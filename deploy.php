<?php
/*
 * This file has been generated automatically.
 * Please change the configuration for correct use deploy.
 */

require 'recipe/symfony.php';

// Set configurations
set('keep_releases', 3);
set('repository', 'https://mod3_manager:sDANxZFrJyNRLTKzXSBq@git.cit-solutions.ru/energoline24/mod3/manager.git');
set('shared_files', ['.env.local']);
set('shared_dirs', ['var/logs', 'var/sessions','var/photo', 'public/photos']);
set('writable_dirs', ['var/cache', 'var/logs', 'var/sessions','var/photo','public/photos']);
set('git_tty', true);
set('bin_dir', 'bin');
set('writable_use_sudo', false);

// App path
env('bin/php', '/opt/php74/bin/php');

// Configure servers
/*server('prod', '185.105.226.136')
    ->user('energoline24_ru')
    ->password('tV2qB8nR')
    ->env('deploy_path', '/var/www/energoline24_ru/data/www/mod3.energoline24.ru')
    ->env('repository_path', '/master')
    ->env('env', 'prod')
    ->env('env_vars', 'SYMFONY_ENV=prod')
    ->env('composer_options', 'install --verbose --prefer-dist --optimize-autoloader --no-progress --no-interaction');*/

server('demo', '185.105.226.136')
    ->user('electroline24')
    ->password('5occKWrp')
    ->env('deploy_path', '~/www/mod3.electroline24.ru')
//    ->env('repository_path', '/master')
    ->env('branch', 'master')
    ->env('env', 'prod')
    ->env('env_vars', 'SYMFONY_ENV=prod')
    ->env('composer_options', 'install --verbose --prefer-dist --optimize-autoloader --no-progress --no-interaction');

server('prod_dagenergi', '185.105.226.136') // electoline.dagenergi.ru
    ->user('energoline24_ru')
    ->password('tV2qB8nR')
    ->env('deploy_path', '~/www/electoline.dagenergi.ru')
    ->env('branch', 'master')
    ->env('env', 'prod')
    ->env('env_vars', 'SYMFONY_ENV=prod')
    ->env('composer_options', 'install --verbose --prefer-dist --optimize-autoloader --no-progress --no-interaction');

///var/www/energoline24_ru/data/


//task('deploy:assetic:dump', function () {
//    if (!get('dump_assets')) {
//        return;
//    }
//
//    run('{{bin/php}} {{release_path}}/' . trim(get('bin_dir'), '/') . '/console assets:install --env={{env}} --no-debug {{release_path}}/public');
//})->desc('Dump assets');

///**
// * Restart php-fpm on success deploy.
// */
//task('php-fpm:restart', function () {
//    // Attention: The user must have rights for restart service
//    // Attention: the command "sudo /bin/systemctl restart php-fpm.service" used only on CentOS system
//    // /etc/sudoers: username ALL=NOPASSWD:/bin/systemctl restart php-fpm.service
//    run('sudo /bin/systemctl restart php-fpm.service');
//})->desc('Restart PHP-FPM service');
//
//after('success', 'php-fpm:restart');


/**
 * Attention: This command is only for for example. Please follow your own migrate strategy.
 * Attention: Commented by default.  
 * Migrate database before symlink new release.
 */
 
// before('deploy:symlink', 'database:migrate');

task('database:migrate', function () {
    run('{{bin/php}} {{release_path}}/' . trim(get('bin_dir'), '/') . '/console doctrine:migrations:migrate --env={{env}} --no-debug --no-interaction');
})->desc('Migrate database');

task('database:schema:drop', function () {
    run('{{bin/php}} {{release_path}}/' . trim(get('bin_dir'), '/') . '/console doctrine:schema:drop --force');
})->desc('Migrate database');

task('database:schema:drop:full', function () {
    run('{{bin/php}} {{release_path}}/' . trim(get('bin_dir'), '/') . '/console doctrine:schema:drop --force --full database');
})->desc('Migrate database');

task('database:schema:update', function () {
    run('{{bin/php}} {{release_path}}/' . trim(get('bin_dir'), '/') . '/console doctrine:schema:update --force');
})->desc('Migrate database');

task('database:fixture', function () {
    run('{{bin/php}} {{release_path}}/' . trim(get('bin_dir'), '/') . '/console doctrine:f:l -n --group=userGroup');
})->desc('Add fixtures');

task('node:install', function () {
    run('cd {{release_path}}');
    run('yarn install');
})->desc('Install node js dep');

task('node:build', function () {
    run('cd {{release_path}}');
    run('yarn run build');
})->desc('Build react app');

task('deploy', [
    'deploy:prepare',
    'deploy:release',
    'deploy:update_code',
    'deploy:create_cache_dir',
    'deploy:shared',
    'deploy:assets',
    'deploy:vendors',
//    'deploy:assetic:dump',
    'deploy:cache:warmup',
    'deploy:writable',
//    'database:migrate',
//    'database:schema:drop',
//    'database:schema:update',
//    'database:fixture',
    'node:install',
//    'node:build',
    'deploy:symlink',
    'cleanup',
])->desc('Deploy your project');