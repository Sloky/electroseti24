<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220331131945 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE address (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', locality VARCHAR(255) DEFAULT NULL, street VARCHAR(255) DEFAULT NULL, house_number VARCHAR(255) DEFAULT NULL, porch_number VARCHAR(255) DEFAULT NULL, apartment_number VARCHAR(255) DEFAULT NULL, lodgers_count INT DEFAULT NULL, rooms_count INT DEFAULT NULL, address_hash VARCHAR(180) NOT NULL, UNIQUE INDEX UNIQ_D4E6F81ACF4B253 (address_hash), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE agreement (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', subscriber_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', company CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', number VARCHAR(100) NOT NULL, payment_number VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_2E655A2496901F54 (number), INDEX IDX_2E655A247808B1AD (subscriber_id), INDEX IDX_2E655A244FBF094F (company), INDEX search_idx (number, payment_number), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bypass_list_record (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', row_position INT NOT NULL, file_name LONGTEXT NOT NULL, status INT NOT NULL, period DATETIME DEFAULT NULL, agreement_number LONGTEXT DEFAULT NULL, address LONGTEXT DEFAULT NULL, subscriber_title LONGTEXT DEFAULT NULL, subscriber_type LONGTEXT DEFAULT NULL, phones LONGTEXT DEFAULT NULL, company_title LONGTEXT DEFAULT NULL, counter_number LONGTEXT DEFAULT NULL, counter_model LONGTEXT DEFAULT NULL, serviceability LONGTEXT DEFAULT NULL, current_counter_value LONGTEXT DEFAULT NULL, current_counter_value_date LONGTEXT DEFAULT NULL, side_seal LONGTEXT DEFAULT NULL, terminal_seal LONGTEXT DEFAULT NULL, antimagnetic_seal LONGTEXT DEFAULT NULL, substation LONGTEXT DEFAULT NULL, feeder LONGTEXT DEFAULT NULL, transformer LONGTEXT DEFAULT NULL, electric_line LONGTEXT DEFAULT NULL, electric_pole LONGTEXT DEFAULT NULL, coords LONGTEXT DEFAULT NULL, controller LONGTEXT DEFAULT NULL, deadline LONGTEXT DEFAULT NULL, rooms_count LONGTEXT DEFAULT NULL, lodgers_count LONGTEXT DEFAULT NULL, counter_initial_date LONGTEXT DEFAULT NULL, counter_initial_value LONGTEXT DEFAULT NULL, errors LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:object)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE company (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', parent CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', title VARCHAR(180) NOT NULL, branch_code VARCHAR(255) DEFAULT NULL, address LONGTEXT DEFAULT NULL, phone VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_4FBF094F2B36786B (title), INDEX IDX_4FBF094F3D8E604F (parent), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE counter (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', agreement CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', address CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', transformer CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', counter_type CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', initial_value CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', final_value CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', title VARCHAR(100) NOT NULL, install_date DATETIME DEFAULT NULL, dismantling_date DATETIME DEFAULT NULL, terminal_seal VARCHAR(255) DEFAULT NULL, anti_magnetic_seal VARCHAR(255) DEFAULT NULL, side_seal VARCHAR(255) DEFAULT NULL, electric_pole_number VARCHAR(255) DEFAULT NULL, electric_line VARCHAR(255) DEFAULT NULL, work_status INT DEFAULT 0 NOT NULL, is_installed_status INT DEFAULT 0 NOT NULL, coordinates JSON DEFAULT NULL, temporary_address VARCHAR(255) DEFAULT NULL, temporary_counter_type VARCHAR(255) DEFAULT NULL, temporary_substation VARCHAR(255) DEFAULT NULL, temporary_transformer VARCHAR(255) DEFAULT NULL, temporary_feeder VARCHAR(255) DEFAULT NULL, temporary_lodgers_count INT DEFAULT NULL, temporary_rooms_count INT DEFAULT NULL, INDEX IDX_C12294782E655A24 (agreement), INDEX IDX_C1229478D4E6F81 (address), INDEX IDX_C1229478664E9BAD (transformer), INDEX IDX_C1229478C3538E4E (counter_type), UNIQUE INDEX UNIQ_C12294781550003D (initial_value), UNIQUE INDEX UNIQ_C12294781C6F0BA4 (final_value), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE counter_type (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', title VARCHAR(180) NOT NULL, electron_current_type INT DEFAULT NULL, number_phases INT DEFAULT NULL, number_tariffs INT DEFAULT NULL, tariff_type VARCHAR(255) DEFAULT NULL, operating_mechanism INT DEFAULT NULL, class_accuracy INT DEFAULT NULL, indicator_device INT DEFAULT NULL, capacity INT DEFAULT NULL, UNIQUE INDEX UNIQ_C3538E4E2B36786B (title), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE counter_value (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', counter CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', inspector CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', date DATETIME NOT NULL, value LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:object)\', INDEX IDX_CEEE2116C1229478 (counter), INDEX IDX_CEEE211672DD518B (inspector), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE failure_factor (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', title VARCHAR(180) NOT NULL, message VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_46AD33482B36786B (title), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE feeder (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', substation CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', title VARCHAR(100) NOT NULL, UNIQUE INDEX UNIQ_DB1A5EE62B36786B (title), INDEX IDX_DB1A5EE6D45B507E (substation), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE photo (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', task CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', file_name VARCHAR(255) NOT NULL, INDEX IDX_14B78418527EDB25 (task), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE subscriber (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', title VARCHAR(255) NOT NULL, name VARCHAR(255) DEFAULT NULL, surname VARCHAR(255) DEFAULT NULL, patronymic VARCHAR(255) DEFAULT NULL, subscriber_type INT NOT NULL, phones JSON NOT NULL, address VARCHAR(255) DEFAULT NULL, tin VARCHAR(255) DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, coordinates JSON DEFAULT NULL, web_site VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE substation (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', title VARCHAR(180) NOT NULL, UNIQUE INDEX UNIQ_D45B507E2B36786B (title), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE task (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', company CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', counter CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', executor CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', operator CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', last_counter_value CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', current_counter_value CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', subscriber CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', agreement CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', changed_counter_type CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', task_type INT NOT NULL, deadline DATETIME DEFAULT NULL, executed_date DATETIME DEFAULT NULL, acceptance_date DATETIME DEFAULT NULL, priority INT NOT NULL, status INT NOT NULL, coordinates JSON DEFAULT NULL, operator_comment LONGTEXT DEFAULT NULL, controller_comment LONGTEXT DEFAULT NULL, actualized_fields JSON DEFAULT NULL, changed_fields JSON DEFAULT NULL, consumption DOUBLE PRECISION DEFAULT NULL, consumption_coefficient DOUBLE PRECISION DEFAULT NULL, counter_physical_status INT NOT NULL, changed_counter_number VARCHAR(255) DEFAULT NULL, changed_counter_type_string VARCHAR(255) DEFAULT NULL, changed_terminal_seal VARCHAR(255) DEFAULT NULL, changed_antimagnetic_seal VARCHAR(255) DEFAULT NULL, changed_side_seal VARCHAR(255) DEFAULT NULL, changed_lodgers_count INT DEFAULT NULL, changed_rooms_count INT DEFAULT NULL, new_coordinates JSON DEFAULT NULL, period DATETIME DEFAULT NULL, INDEX IDX_527EDB254FBF094F (company), INDEX IDX_527EDB25C1229478 (counter), INDEX IDX_527EDB25F7728BD4 (executor), INDEX IDX_527EDB25D7A6A781 (operator), INDEX IDX_527EDB25A2E14281 (last_counter_value), INDEX IDX_527EDB25DF109BF4 (current_counter_value), INDEX IDX_527EDB25AD005B69 (subscriber), INDEX IDX_527EDB252E655A24 (agreement), INDEX IDX_527EDB2561015407 (changed_counter_type), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE task_failure_factors (task_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', failure_factor_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', INDEX IDX_253A3C958DB60186 (task_id), INDEX IDX_253A3C9544C4A36B (failure_factor_id), PRIMARY KEY(task_id, failure_factor_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE transformer (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', feeder CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', company CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', title VARCHAR(180) NOT NULL, coords JSON DEFAULT NULL, UNIQUE INDEX UNIQ_664E9BAD2B36786B (title), INDEX IDX_664E9BADDB1A5EE6 (feeder), INDEX IDX_664E9BAD4FBF094F (company), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users_transformers (transformer_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', user_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', INDEX IDX_E04B78C9B4303423 (transformer_id), INDEX IDX_E04B78C9A76ED395 (user_id), PRIMARY KEY(transformer_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', username VARCHAR(180) NOT NULL, roles JSON NOT NULL, name VARCHAR(255) DEFAULT NULL, surname VARCHAR(255) DEFAULT NULL, patronymic VARCHAR(255) DEFAULT NULL, phone VARCHAR(255) DEFAULT NULL, address VARCHAR(255) DEFAULT NULL, work_status INT NOT NULL, password VARCHAR(255) NOT NULL, user_code VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), INDEX search_idx (username, user_code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users_groups (user_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', user_group_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', INDEX IDX_FF8AB7E0A76ED395 (user_id), INDEX IDX_FF8AB7E01ED93D47 (user_group_id), PRIMARY KEY(user_id, user_group_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE company_users (user_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', company_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', INDEX IDX_5372078CA76ED395 (user_id), INDEX IDX_5372078C979B1AD6 (company_id), PRIMARY KEY(user_id, company_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_group (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', group_name VARCHAR(180) NOT NULL, roles JSON NOT NULL, description LONGTEXT NOT NULL, UNIQUE INDEX UNIQ_8F02BF9D77792576 (group_name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE agreement ADD CONSTRAINT FK_2E655A247808B1AD FOREIGN KEY (subscriber_id) REFERENCES subscriber (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE agreement ADD CONSTRAINT FK_2E655A244FBF094F FOREIGN KEY (company) REFERENCES company (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE company ADD CONSTRAINT FK_4FBF094F3D8E604F FOREIGN KEY (parent) REFERENCES company (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE counter ADD CONSTRAINT FK_C12294782E655A24 FOREIGN KEY (agreement) REFERENCES agreement (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE counter ADD CONSTRAINT FK_C1229478D4E6F81 FOREIGN KEY (address) REFERENCES address (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE counter ADD CONSTRAINT FK_C1229478664E9BAD FOREIGN KEY (transformer) REFERENCES transformer (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE counter ADD CONSTRAINT FK_C1229478C3538E4E FOREIGN KEY (counter_type) REFERENCES counter_type (id)');
        $this->addSql('ALTER TABLE counter ADD CONSTRAINT FK_C12294781550003D FOREIGN KEY (initial_value) REFERENCES counter_value (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE counter ADD CONSTRAINT FK_C12294781C6F0BA4 FOREIGN KEY (final_value) REFERENCES counter_value (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE counter_value ADD CONSTRAINT FK_CEEE2116C1229478 FOREIGN KEY (counter) REFERENCES counter (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE counter_value ADD CONSTRAINT FK_CEEE211672DD518B FOREIGN KEY (inspector) REFERENCES user (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE feeder ADD CONSTRAINT FK_DB1A5EE6D45B507E FOREIGN KEY (substation) REFERENCES substation (id)');
        $this->addSql('ALTER TABLE photo ADD CONSTRAINT FK_14B78418527EDB25 FOREIGN KEY (task) REFERENCES task (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT FK_527EDB254FBF094F FOREIGN KEY (company) REFERENCES company (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT FK_527EDB25C1229478 FOREIGN KEY (counter) REFERENCES counter (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT FK_527EDB25F7728BD4 FOREIGN KEY (executor) REFERENCES user (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT FK_527EDB25D7A6A781 FOREIGN KEY (operator) REFERENCES user (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT FK_527EDB25A2E14281 FOREIGN KEY (last_counter_value) REFERENCES counter_value (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT FK_527EDB25DF109BF4 FOREIGN KEY (current_counter_value) REFERENCES counter_value (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT FK_527EDB25AD005B69 FOREIGN KEY (subscriber) REFERENCES subscriber (id)');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT FK_527EDB252E655A24 FOREIGN KEY (agreement) REFERENCES agreement (id)');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT FK_527EDB2561015407 FOREIGN KEY (changed_counter_type) REFERENCES counter_type (id)');
        $this->addSql('ALTER TABLE task_failure_factors ADD CONSTRAINT FK_253A3C958DB60186 FOREIGN KEY (task_id) REFERENCES task (id)');
        $this->addSql('ALTER TABLE task_failure_factors ADD CONSTRAINT FK_253A3C9544C4A36B FOREIGN KEY (failure_factor_id) REFERENCES failure_factor (id)');
        $this->addSql('ALTER TABLE transformer ADD CONSTRAINT FK_664E9BADDB1A5EE6 FOREIGN KEY (feeder) REFERENCES feeder (id)');
        $this->addSql('ALTER TABLE transformer ADD CONSTRAINT FK_664E9BAD4FBF094F FOREIGN KEY (company) REFERENCES company (id)');
        $this->addSql('ALTER TABLE users_transformers ADD CONSTRAINT FK_E04B78C9B4303423 FOREIGN KEY (transformer_id) REFERENCES transformer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE users_transformers ADD CONSTRAINT FK_E04B78C9A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE users_groups ADD CONSTRAINT FK_FF8AB7E0A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE users_groups ADD CONSTRAINT FK_FF8AB7E01ED93D47 FOREIGN KEY (user_group_id) REFERENCES user_group (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE company_users ADD CONSTRAINT FK_5372078CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE company_users ADD CONSTRAINT FK_5372078C979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE counter DROP FOREIGN KEY FK_C1229478D4E6F81');
        $this->addSql('ALTER TABLE counter DROP FOREIGN KEY FK_C12294782E655A24');
        $this->addSql('ALTER TABLE task DROP FOREIGN KEY FK_527EDB252E655A24');
        $this->addSql('ALTER TABLE agreement DROP FOREIGN KEY FK_2E655A244FBF094F');
        $this->addSql('ALTER TABLE company DROP FOREIGN KEY FK_4FBF094F3D8E604F');
        $this->addSql('ALTER TABLE task DROP FOREIGN KEY FK_527EDB254FBF094F');
        $this->addSql('ALTER TABLE transformer DROP FOREIGN KEY FK_664E9BAD4FBF094F');
        $this->addSql('ALTER TABLE company_users DROP FOREIGN KEY FK_5372078C979B1AD6');
        $this->addSql('ALTER TABLE counter_value DROP FOREIGN KEY FK_CEEE2116C1229478');
        $this->addSql('ALTER TABLE task DROP FOREIGN KEY FK_527EDB25C1229478');
        $this->addSql('ALTER TABLE counter DROP FOREIGN KEY FK_C1229478C3538E4E');
        $this->addSql('ALTER TABLE task DROP FOREIGN KEY FK_527EDB2561015407');
        $this->addSql('ALTER TABLE counter DROP FOREIGN KEY FK_C12294781550003D');
        $this->addSql('ALTER TABLE counter DROP FOREIGN KEY FK_C12294781C6F0BA4');
        $this->addSql('ALTER TABLE task DROP FOREIGN KEY FK_527EDB25A2E14281');
        $this->addSql('ALTER TABLE task DROP FOREIGN KEY FK_527EDB25DF109BF4');
        $this->addSql('ALTER TABLE task_failure_factors DROP FOREIGN KEY FK_253A3C9544C4A36B');
        $this->addSql('ALTER TABLE transformer DROP FOREIGN KEY FK_664E9BADDB1A5EE6');
        $this->addSql('ALTER TABLE agreement DROP FOREIGN KEY FK_2E655A247808B1AD');
        $this->addSql('ALTER TABLE task DROP FOREIGN KEY FK_527EDB25AD005B69');
        $this->addSql('ALTER TABLE feeder DROP FOREIGN KEY FK_DB1A5EE6D45B507E');
        $this->addSql('ALTER TABLE photo DROP FOREIGN KEY FK_14B78418527EDB25');
        $this->addSql('ALTER TABLE task_failure_factors DROP FOREIGN KEY FK_253A3C958DB60186');
        $this->addSql('ALTER TABLE counter DROP FOREIGN KEY FK_C1229478664E9BAD');
        $this->addSql('ALTER TABLE users_transformers DROP FOREIGN KEY FK_E04B78C9B4303423');
        $this->addSql('ALTER TABLE counter_value DROP FOREIGN KEY FK_CEEE211672DD518B');
        $this->addSql('ALTER TABLE task DROP FOREIGN KEY FK_527EDB25F7728BD4');
        $this->addSql('ALTER TABLE task DROP FOREIGN KEY FK_527EDB25D7A6A781');
        $this->addSql('ALTER TABLE users_transformers DROP FOREIGN KEY FK_E04B78C9A76ED395');
        $this->addSql('ALTER TABLE users_groups DROP FOREIGN KEY FK_FF8AB7E0A76ED395');
        $this->addSql('ALTER TABLE company_users DROP FOREIGN KEY FK_5372078CA76ED395');
        $this->addSql('ALTER TABLE users_groups DROP FOREIGN KEY FK_FF8AB7E01ED93D47');
        $this->addSql('DROP TABLE address');
        $this->addSql('DROP TABLE agreement');
        $this->addSql('DROP TABLE bypass_list_record');
        $this->addSql('DROP TABLE company');
        $this->addSql('DROP TABLE counter');
        $this->addSql('DROP TABLE counter_type');
        $this->addSql('DROP TABLE counter_value');
        $this->addSql('DROP TABLE failure_factor');
        $this->addSql('DROP TABLE feeder');
        $this->addSql('DROP TABLE photo');
        $this->addSql('DROP TABLE subscriber');
        $this->addSql('DROP TABLE substation');
        $this->addSql('DROP TABLE task');
        $this->addSql('DROP TABLE task_failure_factors');
        $this->addSql('DROP TABLE transformer');
        $this->addSql('DROP TABLE users_transformers');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE users_groups');
        $this->addSql('DROP TABLE company_users');
        $this->addSql('DROP TABLE user_group');
    }
}
