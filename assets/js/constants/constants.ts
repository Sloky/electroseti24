export const TASK_TYPE = {
    '0': {
        title: 'Снятие показаний узла учёта',
        value: '0'
    },
    '1': {
        title: 'Замена узла учёта',
        value: '1'
    },
    '2': {
        title: 'Отключение узла учёта',
        value: '2'
    },
};

export const TASK_PRIORITY = {
    '0' : {
        title: 'Стандартный',
        value: '0'
    },
    '1' : {
        title: 'Высокий',
        value: '1'
    },
    '2' : {
        title: 'Особый',
        value: '2'
    },
};

export const TASK_STATUS = {
    '0': {
        title: 'К выполнению',
        value: '0'
    },
    '1': {
        title: 'К проверке',
        value: '1'
    },
    '2': {
        title: 'Принято',
        value: '2'
    },
};

export const TASK_COUNTER_PHYSICAL_STATUSES = {
    '0': {
        title: 'Доступен/Исправен',
        value: '0'
    },
    '1': {
        title: 'Недоступен/Неисправен',
        value: '1'
    },
};