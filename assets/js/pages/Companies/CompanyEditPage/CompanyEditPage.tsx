import React, {useEffect, useState} from "react";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import CompanyEditCard from "./CompanyEditCard";
import {useParams} from "react-router";
import {useDispatch, useSelector} from "react-redux";
import {AppAction} from "../../../store/actions/actions";
import {getCompanyEditCardDataRequest} from "../../../requests/Request";
import {appStateInterface} from "../../../store/appState";
import FlashComponent from "../../../components/Flashes/FlashComponent";
import {SimpleFlashData} from "../../../interfaces/interfaces";

export default function CompanyEditPage() {
    const [companyData, setCompanyData] = useState({
        title: '',
        address: '',
        branch_code: '',
        phone: '',
    });
    const [flashError, setFlashError] = useState<SimpleFlashData>({status: false})
    const [flashSuccess, setFlashSuccess] = useState({status: false, message: ''});

    const dispatch = useDispatch();

    let params: any = useParams();
    let id = params.id;
    const loading = useSelector((state: appStateInterface) => state.loading);

    const breadcrumbsItems = [
        {
            itemTitle: 'Отделения',
            itemSrc: '/companies',
        },
        {
            itemTitle: 'Карточка отделения',
            itemSrc: `/company-page/${id}`,
        },
        {
            itemTitle: 'Редактирование отделения',
            // itemSrc: ``,
        },
    ]

    const onCloseErrorFlash = () => {
        setFlashError(prevState => {return {...prevState, status: false}})
    };

    const onCloseSuccessFlash = () => {
        setFlashSuccess(prevState => {return {...prevState, status: false}})
    };

    const showFlash = (type, message, messageHeading) => {
        switch (type) {
            case 'error':
                setFlashError({status: true, messageHeading: messageHeading, message: message});
                break;
            case 'success':
                setFlashSuccess({status: true, message: message});
                break;
            case 'disableFlashes':
                setFlashError({status: false,});
                setFlashSuccess({status: false, message: message});
        }
    }

    useEffect(() => {
        dispatch(AppAction.showLoader());
        getCompanyEditCardDataRequest(id).then(
            result => {
                if (result.type == 'success') {
                    setCompanyData(result.data);
                } else {
                    console.log('Error: ', result.data);
                    showFlash('error', 'Обратитесь к администратору', 'Ошибка.');
                }
            }
        ).finally(() => {
            dispatch(AppAction.hideLoader());
        })
    }, []);

    const changeHandler = (values) => {
        setCompanyData(values);
    }

    return (
        <PageLayout
            breadcrumbsItems={breadcrumbsItems}
        >
            {!loading ?
                <CompanyEditCard
                    companyData={companyData}
                    onFlash={(type, message, messageHeading) => {showFlash(type, message, messageHeading)}}
                    changeHandler={changeHandler}
                >
                    <FlashComponent
                        message={flashError.message}
                        messageHeading={flashError.messageHeading}
                        // onCloseFlash={onCloseErrorFlash}
                        showFlash={flashError.status}
                        flashType="error"
                        // flashWidth="100%"
                    />
                    <FlashComponent
                        message={flashSuccess.message}
                        showFlash={flashSuccess.status}
                        onCloseFlash={onCloseSuccessFlash}
                        flashType={'success'}
                    />
                </CompanyEditCard>
                : <></>
            }
        </PageLayout>
    )
}