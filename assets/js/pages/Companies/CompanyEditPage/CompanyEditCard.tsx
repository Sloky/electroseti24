import React, {useEffect, useState} from "react";
import {Box, Button, Grid, TextField} from "@material-ui/core";
import {CompanyData} from "../../../interfaces/interfaces";
import {useHistory, useParams} from "react-router";
import {ArrowBackIos} from "@material-ui/icons";
import SimpleModal from "../../../components/Modal/SimpleModal/SimpleModal";
import {useDispatch, useSelector} from "react-redux";
import {AppAction} from "../../../store/actions/actions";
import {Formik} from "formik";
import * as Yup from "yup";
import {deleteCompanyRequest, editCompanyRequest} from "../../../requests/Request";
import {appStateInterface} from "../../../store/appState";
import BackButtonComponent from "../../../components/Other/BackButtonComponent/BackButtonComponent";

interface CompanyEditCardProps {
    companyData: CompanyData
    children?: any
    onFlash(type: string, message: string, messageHeading?: string): void
    changeHandler?: any
}

export default function CompanyEditCard(props: CompanyEditCardProps) {
    const [initialValues, setInitialValues] = useState({
        address: props.companyData.address,
        branchCode: props.companyData.branch_code,
        phone: props.companyData.phone,
        title: props.companyData.title,
    });
    const [companyStateStatus, setCompanyStateStatus] = useState(true);

    const history = useHistory();
    const dispatch = useDispatch();

    let params: any = useParams();
    let id = params.id;
    const currentUser = useSelector((state: appStateInterface) => state.user);

    const saveCompanyHandler = (values) => {
        dispatch(AppAction.showLoader());
        editCompanyRequest(id, values).then(
            result => {
                if (result.type == 'success') {
                    props.changeHandler(result.data);
                    // props.onFlash('disableFlashes', '');
                    // props.onFlash('success', 'Данные отделения были успешно изменены');
                    history.push(`/company-page/${id}`);
                } else {
                    props.onFlash('error', 'Пожалуйста, обратитесь к администратору', 'Ошибка.')
                }
            }
        ).finally(() => {
            dispatch(AppAction.hideLoader());
        });
    };

    useEffect(() => {
        setInitialValues({
            address: props.companyData.address,
            branchCode: props.companyData.branch_code,
            phone: props.companyData.phone,
            title: props.companyData.title,
        });
    }, [props]);

    const deleteCompanyHandler = () => {
        dispatch(AppAction.showLoader());
        deleteCompanyRequest(id).then(
            result => {
                if (result.type == 'success') {
                    dispatch(AppAction.hideLoader());
                    history.push('/companies');
                } else {
                    dispatch(AppAction.hideLoader());
                    props.onFlash('error', 'Не удалось удалить отделение. Пожалуйста, обратитесь к администратору', 'Ошибка.');
                }
            }
        );
    };

    const companySchema = Yup.object().shape({
        title: Yup.string().required('Поле с названием отделения не может быть пустым'),
        address: Yup.string().nullable(),
        branchCode: Yup.string().nullable(),
        phone: Yup.string().nullable(),
    })

    return (
        <>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                Редактирование отделения
            </Grid>
            <Grid style={{marginBottom: '30px', display: 'flex', justifyContent: 'space-between',}}>
                <BackButtonComponent changed={!companyStateStatus}/>
                {currentUser.roles.includes('ROLE_SUPER_ADMIN') &&
                    <SimpleModal
                        startButtonTitle={'Удалить отделение'}
                        modalName={'deleteCompanyModal'}
                        startButtonVariant={'text'}
                        closeButtonTitle={'Отменить'}
                        modalTitle={'Удаление отделения'}
                        acceptButtonTitle={'Подтвердить'}
                        onModalSubmit={deleteCompanyHandler}
                    >
                        Удалить отделение?
                    </SimpleModal>
                }
            </Grid>
            {props.children}
            <Formik
                initialValues={initialValues}
                validationSchema={companySchema}
                onSubmit={(values) => saveCompanyHandler(values)}
            >
                {({
                    handleChange,
                    handleSubmit,
                    handleBlur,
                    dirty,
                    isValid,
                    errors,
                    values,
                    touched,
                    setFieldValue,
                    handleReset,
                }) => (
                    <Box
                        component={'form'}
                    >
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={6} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'title'}
                                    value={values.title || ''}
                                    placeholder={'Введите название отделения'}
                                    label={'Название отделения'}
                                    onChange={(e) => {
                                        e.persist()
                                        setFieldValue('title', e.target.value);
                                        setCompanyStateStatus(false);
                                    }}
                                    error={errors.title && touched.title}
                                    helperText={touched.title && errors.title}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={6} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'address'}
                                    value={values.address || ''}
                                    placeholder={'Введите адрес'}
                                    label={'Адрес'}
                                    onChange={(e) => {
                                        e.persist()
                                        setFieldValue('address', e.target.value);
                                        setCompanyStateStatus(false);
                                    }}
                                    error={errors.address && touched.address}
                                    helperText={touched.address && errors.address}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={6} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'branch_code'}
                                    value={values.branchCode || ''}
                                    placeholder={'Введите код отделения'}
                                    label={'Код отделения'}
                                    onChange={(e) => {
                                        e.persist()
                                        setFieldValue('branchCode', e.target.value);
                                        setCompanyStateStatus(false);
                                    }}
                                    error={errors.branchCode && touched.branchCode}
                                    helperText={touched.branchCode && errors.branchCode}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={6} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'phone'}
                                    value={values.phone || ''}
                                    placeholder={'Введите телефон'}
                                    label={'Телефон'}
                                    onChange={(e) => {
                                        e.persist()
                                        setFieldValue('phone', e.target.value);
                                        setCompanyStateStatus(false);
                                    }}
                                    error={errors.phone && touched.phone}
                                    helperText={touched.phone && errors.phone}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={3} style={{paddingRight: '15px',}}>
                                <Button
                                    variant={'outlined'}
                                    fullWidth
                                    onClick={() => {
                                        handleReset();
                                        setCompanyStateStatus(true);
                                    }}
                                    className={'base_styled_btn'}
                                >
                                    Отменить изменения
                                </Button>
                            </Grid>
                            <Grid item xs={3} style={{paddingRight: '15px',}}>
                                <Button
                                    variant={'contained'}
                                    fullWidth
                                    onClick={() => handleSubmit()}
                                    className={'base_styled_btn'}
                                    disabled={companyStateStatus}
                                >
                                    Сохранить изменения
                                </Button>
                            </Grid>
                        </Grid>
                    </Box>
                )}
            </Formik>
            <BackButtonComponent changed={!companyStateStatus}/>
        </>
    )
}