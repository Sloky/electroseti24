import React, {useEffect, useState} from "react";
import {useDispatch} from "react-redux";
import {useHistory} from "react-router";
import {getCompaniesRequest} from "../../../requests/Request";
import {AppAction, EntitiesAction} from "../../../store/actions/actions";
import {
    Box,
    Button,
    Grid,
    Pagination,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow
} from "@material-ui/core";
import FlashComponent from "../../../components/Flashes/FlashComponent";
import {SimpleFlashData} from "../../../interfaces/interfaces";
import {getEntityCollection} from "../../../functions/functions";
import TableItemsCustomizer from "../../Tasks/TasksTable/TableItemsCustomizer";
import {THEME} from "../../../theme";

const listHeader = [
    'Наименование отделения',
    'Адрес',
    'Телефон',
];

export default function CompaniesList() {
    const [flashError, setFlashError] : [SimpleFlashData, any] = useState({status: false});
    const [companiesData, setCompaniesData] = useState([]);

    const [itemsCount, setItemsCount] = useState<number>(100);
    const [pageNumber, setPageNumber] = useState<number>(1);
    const [totalItems, setTotalItems] = useState<number>(0);
    const [pagesCount, setPagesCount] = useState<number>(Math.ceil(totalItems/itemsCount));

    const dispatch = useDispatch();
    const history = useHistory();

    async function getCompanies() {
        let companiesRequest = await getCompaniesRequest({page: pageNumber, maxResult: itemsCount});
        if (companiesRequest.type == 'success') {
            setCompaniesData(companiesRequest.data.companies);
            setTotalItems(companiesRequest.data.companiesCount);
            dispatch(EntitiesAction.changeCompanyBranchesAction(getEntityCollection(companiesRequest.data.companies)));
        } else {
            console.log('Error companies data: ', companiesRequest.data);
            setFlashError({
                status: true,
                message: companiesRequest.data.message,
            });
        }
    }

    useEffect(() => {
        dispatch(AppAction.showLoader());
        getCompanies().then(r => {
            dispatch(AppAction.hideLoader());
        })
    }, []);

    useEffect(() => {
        if (pageNumber > 0) {
            dispatch(AppAction.showLoader());
            getCompaniesRequest({page: pageNumber, maxResult: itemsCount})
                .then(result => {
                    setCompaniesData(result.data.companies);
                    dispatch(AppAction.hideLoader());
                })
        }
    }, [pageNumber, itemsCount])

    useEffect(() => {
        setPagesCount(Math.ceil(totalItems/itemsCount || 1));
    }, [itemsCount, totalItems]);

    useEffect(() => {
        if (pageNumber > pagesCount) {
            setPageNumber(pagesCount ? pagesCount : 1);
        }
    }, [pagesCount]);

    const onPageNumberChange = (value: number) => {
        setPageNumber(value);
    };

    const onTableItemCountChange = (value: number) => {
        setItemsCount(value);
    };

    return (
        <>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                Отделения
            </Grid>
            <Grid container style={{marginBottom: '30px',}}>
                <Grid item xs={3} style={{paddingRight: '15px',}}>
                    <Button
                        onClick={() => history.push('/company/new')}
                        variant={'contained'}
                        fullWidth
                        size={'medium'}
                        className={'base_styled_btn'}
                    >
                        Добавить отделение
                    </Button>
                </Grid>
            </Grid>
            <FlashComponent
                message={flashError.message}
                showFlash={flashError.status}
                messageHeading={flashError.messageHeading}
                flashType={'error'}
            />
            <Grid style={{marginBottom: '20px',}}>
                <TableItemsCustomizer
                    allItems={totalItems}
                    onTableItemCountChange={onTableItemCountChange}
                    initialCount={itemsCount}
                />
            </Grid>
            <TableContainer sx={{bgcolor: 'background.default'}}>
                <Table>
                    <TableHead>
                        <TableRow>
                            {listHeader.map((value, index) => (
                                <TableCell key={index}>{value}</TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {Array.isArray(companiesData) && companiesData.length ? companiesData.map((company, index) => (
                            <TableRow hover key={index} style={{cursor: 'pointer',}} onClick={() => history.push(`/company-page/${company.id}`)}>
                                <TableCell style={{fontSize: '15px',}}>{company.title}</TableCell>
                                <TableCell style={{fontSize: '15px',}}>{company.address}</TableCell>
                                <TableCell style={{fontSize: '15px',}}>{company.phone}</TableCell>
                            </TableRow>
                        )) : (
                            <></>
                        )}
                    </TableBody>
                </Table>
            </TableContainer>
            {!companiesData && (
                <Box style={{padding: '70px 0px', textAlign: 'center', width: '100%', color: THEME.PLACEHOLDER_TEXT, fontSize: '16px',}}>
                    Нет данных
                </Box>
            )}
            <Stack spacing={2} style={{marginTop: '30px',}}>
                <Pagination count={pagesCount} variant="outlined" shape="rounded" color="primary" onChange={(event, page) => onPageNumberChange(page)}/>
            </Stack>
        </>
    )
}