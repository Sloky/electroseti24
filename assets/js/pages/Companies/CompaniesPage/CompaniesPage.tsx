import React from "react";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import CompaniesList from "./CompaniesList";

export default function CompaniesPage() {
    return (
        <PageLayout>
            <CompaniesList/>
        </PageLayout>
    )
}