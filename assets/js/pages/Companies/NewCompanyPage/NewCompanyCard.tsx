import React, {useState} from "react";
import {SimpleFlashData} from "../../../interfaces/interfaces";
import {useHistory} from "react-router";
import {useDispatch} from "react-redux";
import {AppAction} from "../../../store/actions/actions";
import {Formik} from "formik";
import * as Yup from "yup";
import {Box, Button, Grid, TextField} from "@material-ui/core";
import FlashComponent from "../../../components/Flashes/FlashComponent";
import {addNewCompanyRequest} from "../../../requests/Request";

interface companyFormInterface {
    title: string
    address: string
    branchCode: string
    phone: string
}

export default function NewCompanyCard() {
    const [flashError, setFlashError] : [SimpleFlashData, any] = useState({status: false});
    const [flashSuccess, setFlashSuccess] : [SimpleFlashData, any] = useState({status: false});

    const history = useHistory();
    const dispatch = useDispatch();

    const handleSubmit = (values) => {
        dispatch(AppAction.showLoader());
        addNewCompanyRequest(values).then(
            result => {
                if (result.type == 'success') {
                    // setFlashSuccess({
                    //     status: true,
                    //     message: 'Отделение успешно добавлено',
                    // });
                    history.push('/companies');
                } else {
                    setFlashError({
                        status: true,
                        message: 'Произошла ошибка. Обратитесь к администратору',
                    });
                }
            }
        ).finally(() => {
            dispatch(AppAction.hideLoader());
        });
    }

    const onCloseSuccessFlash = () => {
        setFlashSuccess({status: false});
    }

    const onCloseErrorFlash = () => {
        setFlashError({status: false});
    }

    const initialValues: companyFormInterface = {
        title: '',
        address: '',
        branchCode: '',
        phone: '',
    }

    const companySchema = Yup.object().shape({
        title: Yup.string().required('Поле с названием отделения не может быть пустым'),
        address: Yup.string(),
        branchCode: Yup.string(),
        phone: Yup.string(),
    })

    return (
        <>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                Добавление отделения
            </Grid>
            <FlashComponent
                messageHeading={flashError.messageHeading}
                message={flashError.message}
                showFlash={flashError.status}
                onCloseFlash={onCloseErrorFlash}
                flashType={'error'}
            />
            <FlashComponent
                message={flashSuccess.message}
                showFlash={flashSuccess.status}
                onCloseFlash={onCloseSuccessFlash}
                flashType={'success'}
            />
            <Formik
                initialValues={initialValues}
                validationSchema={companySchema}
                onSubmit={(values) => handleSubmit(values)}
            >
                {({
                    handleChange,
                    handleSubmit,
                    handleBlur,
                    dirty,
                    isValid,
                    errors,
                    values,
                    touched,
                    setFieldValue,
                }) => (
                    <Box
                        component={'form'}
                    >
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'title'}
                                    placeholder={'Введите название отделения'}
                                    label={'Название отделения'}
                                    onChange={handleChange}
                                    error={errors.title && touched.title}
                                    helperText={touched.title && errors.title}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'address'}
                                    placeholder={'Введите адрес'}
                                    label={'Адрес'}
                                    onChange={handleChange}
                                    error={errors.address && touched.address}
                                    helperText={touched.address && errors.address}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'branchCode'}
                                    placeholder={'Введите код отделения'}
                                    label={'Код отделения'}
                                    onChange={handleChange}
                                    error={errors.branchCode && touched.branchCode}
                                    helperText={touched.branchCode && errors.branchCode}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'phone'}
                                    placeholder={'Введите телефон'}
                                    label={'Телефон'}
                                    onChange={handleChange}
                                    error={errors.phone && touched.phone}
                                    helperText={touched.phone && errors.phone}
                                />
                            </Grid>
                        </Grid>
                        <Grid container>
                            <Grid item xs={2} style={{paddingRight: '15px',}}>
                                <Button
                                    variant={'outlined'}
                                    fullWidth
                                    onClick={() => history.goBack()}
                                    className={'base_styled_btn'}
                                >
                                    Отменить
                                </Button>
                            </Grid>
                            <Grid item xs={2} style={{paddingRight: '15px',}}>
                                <Button
                                    variant={'contained'}
                                    fullWidth
                                    onClick={() => handleSubmit()}
                                    className={'base_styled_btn'}
                                >
                                    Создать отделение
                                </Button>
                            </Grid>
                        </Grid>
                    </Box>
                )}
            </Formik>
        </>
    )
}