import React from "react";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import NewCompanyCard from "./NewCompanyCard";

export default function NewCompanyPage() {
    const breadcrumbsItems = [
        {
            itemTitle: 'Отделения',
            itemSrc: '/companies',
        },
        {
            itemTitle: 'Добавление отделения',
            // itemSrc: ``,
        },
    ]

    return (
        <PageLayout
            breadcrumbsItems={breadcrumbsItems}
        >
            <NewCompanyCard/>
        </PageLayout>
    );
}