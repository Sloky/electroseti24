import React, {useEffect, useState} from "react";
import {
    CompanyData,
    CompanyTransformersData,
    CompanyTransformerTableRequestData,
    UserData
} from "../../../interfaces/interfaces";
import {
    Box,
    Button,
    Card,
    Grid,
    Pagination,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
} from "@material-ui/core";
import {THEME} from "../../../theme";
import {useHistory} from "react-router";
import SimpleSelectInput from "../../../components/FormInputs/SimpleSelectInput/SimpleSelectInput";
import TableItemsCustomizer from "../../Tasks/TasksTable/TableItemsCustomizer";
import SearchComponent from "../../../components/Other/SearchComponent/SearchComponent";

interface CompanyCardProps {
    companyData: CompanyData
    // companyTransformersData: CompanyTransformersData[]
    // controllers: UserData[]
    // transformersCount: number
    // getTransformers(requestData: CompanyTransformerTableRequestData): void
}

const listHeader = [
    'Трансформатор',
    'Узлы учёта',
    'Контроллер',
];

export default function CompanyCard(props: CompanyCardProps) {
    const [company, setCompany] = useState(props.companyData);
    // const [companyTransformers, setCompanyTransformers] = useState(props.companyTransformersData);

    // const [executorFilter, setExecutorFilter] = useState(null);
    // const [searchValue, setSearchValue] = useState('');

    // const [itemsCount, setItemsCount] = useState(100);
    // const [totalItems, setTotalItems] = useState(props.transformersCount);
    // const [pagesCount, setPagesCount] = useState<number>(Math.ceil(totalItems/itemsCount || 1));
    // const [pageNumber, setPageNumber] = useState<number>(1);

    const history = useHistory();

    // const applyFilters = () => {
    //     let requestData = {
    //         page: pageNumber,
    //         maxResult: itemsCount,
    //         search: searchValue,
    //         executor: executorFilter,
    //     }
    //     if (executorFilter !== 'empty' && executorFilter !== null) {
    //         const currentExecutor = props.controllers.filter(controller => {
    //             if (executorFilter === `${controller.surname} ${controller.name} ${controller.patronymic}`) {
    //                 return controller
    //             }
    //         })
    //         requestData = {
    //             page: pageNumber,
    //             maxResult: itemsCount,
    //             search: searchValue,
    //             executor: currentExecutor[0].id,
    //         }
    //     }
    //     props.getTransformers(requestData);
    // }

    // const controllersForSelect = () => {
    //     let newControllers = props.controllers.map(controller => {
    //         let userFullName = controller.surname+' '+controller.name+' '+controller.patronymic;
    //         return {
    //             title: userFullName,
    //             value: userFullName,
    //         }
    //     })
    //     newControllers.unshift({title: 'Без исполнителя', value: 'empty'});
    //     return newControllers
    // }

    // const onSearchValueChange = (value: string) => {
    //     setSearchValue(value);
    // };
    //
    // const onTableItemCountChange = (value: number) => {
    //     setItemsCount(value);
    // };
    //
    // const onPageNumberChange = (value: number) => {
    //     setPageNumber(value);
    // };

    useEffect(() => {
        setCompany(props.companyData);
        // setTotalItems(props.transformersCount);
        // setCompanyTransformers(props.companyTransformersData);
    }, [props])

    // useEffect(() => {
    //     const requestData = {
    //         page: pageNumber,
    //         maxResult: itemsCount,
    //         search: searchValue,
    //         executor: executorFilter,
    //     }
    //     props.getTransformers(requestData);
    // }, [pageNumber, itemsCount]);

    // useEffect(() => {
    //     setPagesCount(Math.ceil(totalItems/itemsCount || 1));
    // }, [itemsCount, totalItems]);

    // useEffect(() => {
    //     if (pageNumber > pagesCount) {
    //         setPageNumber(pagesCount);
    //     }
    // }, [pagesCount]);

    return (
        <>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                Карточка отделения
            </Grid>
            {company ?
                <>
                    <Card sx={{fontSize: '18px', fontWeight: '500', bgcolor: 'background.default', padding: '20px', marginBottom: '30px',}}>
                        <Grid sx={{color: 'text.primary', marginBottom: '15px',}}>
                            {company.title}
                        </Grid>
                        <Grid container style={{marginBottom: '15px',}}>
                            <Grid item xs={3} sx={{color: THEME.PLACEHOLDER_TEXT,}}>Адрес</Grid>
                            <Grid item xs={9}>{company.address || 'Нет данных'}</Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '15px',}}>
                            <Grid item xs={3} sx={{color: THEME.PLACEHOLDER_TEXT,}}>Код отделения</Grid>
                            <Grid item xs={9}>{company.branch_code || 'Нет данных'}</Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={3} sx={{color: THEME.PLACEHOLDER_TEXT,}}>Телефон</Grid>
                            <Grid item xs={9}>{company.phone || 'Нет данных'}</Grid>
                        </Grid>
                        <Grid item xs={2}>
                            <Button
                                variant={'outlined'}
                                size={'large'}
                                onClick={() => history.push(`/company/edit/${company.id}`)}
                                className={'base_styled_btn'}
                                fullWidth
                            >
                                Редактировать отделение
                            </Button>
                        </Grid>
                    </Card>
                    {/*<Grid container style={{marginBottom: '30px',}}>*/}
                    {/*    <Grid item xs={2}>*/}
                    {/*        <Button*/}
                    {/*            variant={'contained'}*/}
                    {/*            size={'large'}*/}
                    {/*            onClick={() => history.push(`/tp-new/${company.id}`)}*/}
                    {/*            className={'base_styled_btn'}*/}
                    {/*            fullWidth*/}
                    {/*        >*/}
                    {/*            Добавить ТП*/}
                    {/*        </Button>*/}
                    {/*    </Grid>*/}
                    {/*</Grid>*/}
                    {/*<Grid container style={{marginBottom: '30px',}}>*/}
                    {/*    <Grid item xs={3} style={{paddingRight: '20px',}}>*/}
                    {/*        <SimpleSelectInput*/}
                    {/*            label={'Исполнитель'}*/}
                    {/*            allowEmptyValue={true}*/}
                    {/*            emptyValueTitle={'Все исполнители'}*/}
                    {/*            defaultValue={executorFilter}*/}
                    {/*            options={controllersForSelect()}*/}
                    {/*            formControlStyle={{width: '100%',}}*/}
                    {/*            onChange={value => {setExecutorFilter(value)}}*/}
                    {/*        />*/}
                    {/*    </Grid>*/}
                    {/*    <Grid item xs={5}>*/}
                    {/*        <SearchComponent*/}
                    {/*            searchValue={searchValue}*/}
                    {/*            onSearchValueChange={onSearchValueChange}*/}
                    {/*            applyFilters={applyFilters}*/}
                    {/*            searchFieldXs={8}*/}
                    {/*            buttonXs={4}*/}
                    {/*        />*/}
                    {/*    </Grid>*/}
                    {/*</Grid>*/}
                    {/*<Grid style={{marginBottom: '20px',}}>*/}
                    {/*    <TableItemsCustomizer*/}
                    {/*        initialCount={itemsCount}*/}
                    {/*        allItems={totalItems}*/}
                    {/*        onTableItemCountChange={onTableItemCountChange}*/}
                    {/*    />*/}
                    {/*</Grid>*/}
                    {/*<TableContainer sx={{bgcolor: 'background.default', marginBottom: '30px',}}>*/}
                    {/*    <Table>*/}
                    {/*        <TableHead>*/}
                    {/*            <TableRow>*/}
                    {/*                {listHeader.map((value, index) => (*/}
                    {/*                    <TableCell key={index} style={{color: THEME.PLACEHOLDER_TEXT}}>{value}</TableCell>*/}
                    {/*                    ))}*/}
                    {/*            </TableRow>*/}
                    {/*        </TableHead>*/}
                    {/*        <TableBody>*/}
                    {/*            {companyTransformers && companyTransformers.length ? companyTransformers.map((data, index) => (*/}
                    {/*                <TableRow hover style={{cursor: 'pointer',}} key={index} onClick={() => history.push(`/tp-page/${data.id}`)}>*/}
                    {/*                    <TableCell sx={{fontSize: '15px', fontWeight: '500',}}>*/}
                    {/*                        {data.title}*/}
                    {/*                    </TableCell>*/}
                    {/*                    <TableCell sx={{fontSize: '15px', fontWeight: '500',}}>*/}
                    {/*                        {data.counters_count}*/}
                    {/*                    </TableCell>*/}
                    {/*                    <TableCell sx={{fontSize: '15px', fontWeight: '500',}}>*/}
                    {/*                        {data.users && data.users.length ? data.users.map((user, index) => {*/}
                    {/*                            if (index === data.users.length - 1) {*/}
                    {/*                                return `${user.surname} ${user.name} ${user.patronymic}`*/}
                    {/*                            } else {*/}
                    {/*                                return `${user.surname} ${user.name} ${user.patronymic}, `*/}
                    {/*                            }*/}
                    {/*                        }) : 'Нет исполнителя'}*/}
                    {/*                    </TableCell>*/}
                    {/*                </TableRow>*/}
                    {/*            )) : (*/}
                    {/*                <></>*/}
                    {/*            )}*/}
                    {/*        </TableBody>*/}
                    {/*    </Table>*/}
                    {/*</TableContainer>*/}
                    {/*{companyTransformers && !companyTransformers.length && (*/}
                    {/*    <Box style={{padding: '70px 0px', textAlign: 'center', width: '100%', color: THEME.PLACEHOLDER_TEXT, fontSize: '16px',}}>*/}
                    {/*        Нет данных*/}
                    {/*    </Box>*/}
                    {/*)}*/}
                    {/*<Stack spacing={2} style={{marginTop: '30px'}}>*/}
                    {/*    <Pagination*/}
                    {/*        count={pagesCount}*/}
                    {/*        variant={'outlined'}*/}
                    {/*        shape={'rounded'}*/}
                    {/*        color={'primary'}*/}
                    {/*        onChange={(event, page) => onPageNumberChange(page)}*/}
                    {/*    />*/}
                    {/*</Stack>*/}
                </> : <></>
            }
        </>
    )
}