import React, {useEffect, useState} from "react";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import CompanyCard from "./CompanyCard";
import {useDispatch} from "react-redux";
import {AppAction} from "../../../store/actions/actions";
import {
    getCompanyControllersDataRequest,
    getCompanyEditCardDataRequest,
    getCompanyTransformersDataRequest,
} from "../../../requests/Request";
import {useParams} from "react-router";

export default function CompanyPage() {
    const [companyData, setCompanyData] = useState(null);
    // const [companyTransformersData, setCompanyTransformersData] = useState(null);
    // const [controllers, setControllers] = useState([]);

    // const [companyLoaded, setCompanyLoaded] = useState(false);
    // const [companyTransformersLoaded, setCompanyTransformersLoaded] = useState(false);
    // const [controllersLoaded, setControllersLoaded] = useState(false);

    const dispatch = useDispatch();
    let params: any = useParams();
    let id = params.id;

    const breadcrumbsItems = [
        {
            itemTitle: 'Отделения',
            itemSrc: '/companies',
        },
        {
            itemTitle: 'Карточка отделения',
            // itemSrc: ``,
        },
    ]

    useEffect(() => {
        dispatch(AppAction.showLoader());
        getCompanyEditCardDataRequest(id).then(result => {
            if (result.type === 'success') {
                setCompanyData(result.data);
            }
        }).finally(() => {
            // setCompanyLoaded(true);
            dispatch(AppAction.hideLoader());
        })
        // getCompanyTransformersDataRequest(id, {page: 1, maxResult: 100,}).then(result => {
        //     if (result.type === 'success') {
        //         setCompanyTransformersData(result.data);
        //     }
        // }).finally(() => {
        //     setCompanyTransformersLoaded(true);
        // })
        // getCompanyControllersDataRequest(id).then(result => {
        //     if (result.type === 'success') {
        //         setControllers(result.data);
        //     }
        // }).finally(() => {
        //     setControllersLoaded(true);
        // })
    }, [])

    // useEffect(() => {
    //     if (companyLoaded && companyTransformersLoaded && controllersLoaded) {
    //         dispatch(AppAction.hideLoader());
    //     }
    // }, [companyLoaded, companyTransformersLoaded, controllersLoaded])

    // const getTransformers = (requestData) => {
    //     dispatch(AppAction.showLoader());
    //     getCompanyTransformersDataRequest(id, requestData).then(
    //         result => {
    //             if (result.type === 'success') {
    //                 setCompanyTransformersData(result.data);
    //             }
    //         }
    //     ).finally(() => {
    //         dispatch(AppAction.hideLoader());
    //     })
    // };

    return (
        <PageLayout
            breadcrumbsItems={breadcrumbsItems}
        >
            <CompanyCard
                companyData={companyData}
                // companyTransformersData={companyTransformersData && companyTransformersData.transformers}
                // controllers={controllers}
                // transformersCount={companyTransformersData && companyTransformersData.transformersCount}
                // getTransformers={getTransformers}
            />
        </PageLayout>
    )
}