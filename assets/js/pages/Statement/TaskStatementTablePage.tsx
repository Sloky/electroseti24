import React, {useEffect, useState} from "react";
import {
    Box, Button,
    Grid,
    Pagination,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Tooltip
} from "@material-ui/core";
import TableItemsCustomizer from "../Tasks/TasksTable/TableItemsCustomizer";
import {THEME} from "../../theme";
import {ArrowDownwardOutlined} from "@material-ui/icons";

interface TaskStatementTablePageProps {
    taskStatement: any
    taskStatementCount: number
    getTaskStatement(requestData: any): void
    downloadTaskStatement(data: any): void
    children: any
}

const listHeader = [
    'Месяц',
    'Количество узлов учёта',
    '',
];

const months = [
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь',
];

export default function TaskStatementTablePage(props: TaskStatementTablePageProps) {
    const [itemsCount, setItemsCount] = useState(100);
    const [totalItems, setTotalItems] = useState(props.taskStatementCount);
    const [pagesCount, setPagesCount] = useState<number>(Math.ceil(totalItems/itemsCount || 1));
    const [pageNumber, setPageNumber] = useState<number>(1);

    const onTableItemCountChange = (value: number) => {
        setItemsCount(value);
    };

    const onPageNumberChange = (value: number) => {
        setPageNumber(value);
    };

    useEffect(() => {
        props.getTaskStatement({page: pageNumber, maxResult: itemsCount,});
    }, [pageNumber, itemsCount]);

    useEffect(() => {
        setPagesCount(Math.ceil(totalItems/itemsCount || 1));
    }, [itemsCount, totalItems]);

    useEffect(() => {
        if (pageNumber > pagesCount) {
            setPageNumber(pagesCount);
        }
    }, [pagesCount]);

    const downloadTaskStatement = (id) => {
        props.downloadTaskStatement(id);
    }

    return (
        <>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                Ведомость
            </Grid>
            {props.children}
            <Grid style={{marginBottom: '20px',}}>
                <TableItemsCustomizer
                    initialCount={itemsCount}
                    allItems={totalItems}
                    onTableItemCountChange={onTableItemCountChange}
                />
            </Grid>
            <TableContainer sx={{bgcolor: 'background.default', marginBottom: '30px',}}>
                <Table>
                    <TableHead>
                        <TableRow>
                            {listHeader.map((value, index) => (
                                <TableCell key={index} style={{color: THEME.PLACEHOLDER_TEXT}}>{value}</TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {props.taskStatement && props.taskStatement.length ? props.taskStatement.map((data, index) => (
                            <TableRow key={index} onClick={() => {}}>
                                <TableCell sx={{fontSize: '15px', fontWeight: '500',}}>
                                    {months[new Date(data.date).getMonth()] || 'Нет данных'}
                                </TableCell>
                                <TableCell sx={{fontSize: '15px', fontWeight: '500',}}>
                                    {data.countersCount || 'Нет данных'}
                                </TableCell>
                                <TableCell sx={{fontSize: '15px', fontWeight: '500', display: 'flex', justifyContent: 'flex-end',}}>
                                    <Tooltip title={'В разработке'} arrow>
                                        <Button
                                            variant={'text'}
                                            startIcon={<ArrowDownwardOutlined/>}
                                            onClick={() => downloadTaskStatement(data.id)}
                                        >
                                            Скачать
                                        </Button>
                                    </Tooltip>
                                </TableCell>
                            </TableRow>
                        )) : (
                            <></>
                        )}
                    </TableBody>
                </Table>
            </TableContainer>
            {props.taskStatement && !props.taskStatement.length && (
                <Box style={{padding: '70px 0px', textAlign: 'center', width: '100%', color: THEME.PLACEHOLDER_TEXT, fontSize: '16px',}}>
                    Нет данных
                </Box>
            )}
            <Stack spacing={2} style={{marginTop: '30px'}}>
                <Pagination
                    count={pagesCount}
                    variant={'outlined'}
                    shape={'rounded'}
                    color={'primary'}
                    onChange={(event, page) => onPageNumberChange(page)}
                />
            </Stack>
        </>
    )
}