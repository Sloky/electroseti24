import React, {useEffect, useState} from "react";
import TaskStatementTablePage from "./TaskStatementTablePage";
import PageLayout from "../../components/Layouts/PageLayout/PageLayout";
import {useDispatch} from "react-redux";
import {AppAction} from "../../store/actions/actions";
import FlashComponent from "../../components/Flashes/FlashComponent";

export default function TaskStatementPage() {
    const [taskStatementData, setTaskStatementData] = useState([
        {
            id: '1',
            date: new Date(2022, 1, 10),
            countersCount: 10,
        },
        {
            id: '2',
            date: new Date(2022, 2, 10),
            countersCount: 20,
        },
        {
            id: '3',
            date: new Date(2022, 3, 10),
            countersCount: 30,
        },
    ]);
    const [flashError, setFlashError] = useState({status: false, message: ''});

    const dispatch = useDispatch();

    useEffect(() => {
        // dispatch(AppAction.showLoader());
        // getTaskStatementDataRequest({page: 1, maxResult: 100,}).then(
        //     result => {
        //         if (result.type === 'success') {
        //             console.log('result', result);
        //             setTaskStatementData(result.data);
        //         }
        //     }
        // ).finally(() => {
        //     dispatch(AppAction.hideLoader());
        // })
    }, []);

    const getTaskStatement = (requestData) => {
        // dispatch(AppAction.showLoader());
        // getTaskStatementDataRequest(requestData).then(
        //     result => {
        //         if (result.type === 'success') {
        //             console.log('result', result);
        //             setTaskStatementData(result.data);
        //         }
        //     }
        // ).finally(() => {
        //     dispatch(AppAction.hideLoader());
        // })
    }

    const downloadTaskStatement = (id) => {
        // dispatch(AppAction.showLoader());
        // setFlashError({status: false, message: '',});
        // getTaskStatementExcelRequest(id).then(
        //     result => {
        //         if (result.type === 'success') {
        //             let link = document.createElement('a');
        //             link.download = 'TaskStatementList.xlsx';
        //
        //             let blob = new Blob(
        //                 [result.data],
        //                 {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'}
        //             );
        //
        //             link.href = URL.createObjectURL(blob);
        //
        //             link.click();
        //
        //             URL.revokeObjectURL(link.href);
        //         } else {
        //             setFlashError({
        //                 status: true,
        //                 message: 'Во время выгрузки произошла ошибка'
        //             });
        //         }
        //     }
        // ).finally(() => {
        //     dispatch(AppAction.hideLoader());
        // })
    }

    return (
        <PageLayout>
            <TaskStatementTablePage
                taskStatement={taskStatementData}
                taskStatementCount={taskStatementData.length}
                getTaskStatement={getTaskStatement}
                downloadTaskStatement={downloadTaskStatement}
            >
                <FlashComponent
                    message={flashError.message}
                    showFlash={flashError.status}
                    flashType={'error'}
                />
            </TaskStatementTablePage>
        </PageLayout>
    )
}