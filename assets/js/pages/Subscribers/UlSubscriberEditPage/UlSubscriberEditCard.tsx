import React, {useEffect, useState} from "react";
import {Box, Button, Grid, TextField} from "@material-ui/core";
import SimpleModal from "../../../components/Modal/SimpleModal/SimpleModal";
import {Formik} from "formik";
import * as Yup from "yup";
import MapComponent from "../../../components/Other/MapComponent/MapComponent";
import SimpleMapModal from "../../../components/Modal/SimpleMapModal/SimpleMapModal";
import BackButtonComponent from "../../../components/Other/BackButtonComponent/BackButtonComponent";
import {SubscriberPostData} from "../../../interfaces/interfaces";
import PhoneComponent from "../../../components/Other/PhoneComponent/PhoneComponent";
import {useSelector} from "react-redux";
import {appStateInterface} from "../../../store/appState";

interface UlSubscriberEditCardProps {
    subscriberData: SubscriberPostData
    children?: any
    onFlash(type: string, message: string, messageHeading?: string): void
    onSaveHandler?(data: any): void
    onDeleteHandler?(): void
}

export default function UlSubscriberEditCard(props: UlSubscriberEditCardProps) {
    const [initialValues, setInitialValues] = useState({
        title: props.subscriberData.title ? props.subscriberData.title : '',
        TIN: props.subscriberData['_tin'] ? props.subscriberData['_tin'] : '',
        address: props.subscriberData.address ? props.subscriberData.address : '',
        coordinateX: props.subscriberData.coordinates?.length ? props.subscriberData.coordinates[0] : null,
        coordinateY: props.subscriberData.coordinates?.length ? props.subscriberData.coordinates[1] : null,
        phones: props.subscriberData.phones ? props.subscriberData.phones : [],
        email: props.subscriberData.email ? props.subscriberData.email : '',
        webSite: props.subscriberData['web_site'] ? props.subscriberData['web_site'] : '',
    });
    const [subscriberStateStatus, setSubscriberStateStatus] = useState(true);

    const currentUser = useSelector((state: appStateInterface) => state.user);

    const saveSubscriberHandler = (values) => {
        props.onSaveHandler(values);
    };

    const deleteSubscriberHandler = () => {
        props.onDeleteHandler();
    }

    useEffect(() => {
        setInitialValues({
            title: props.subscriberData.title ? props.subscriberData.title : '',
            TIN: props.subscriberData['_tin'] ? props.subscriberData['_tin'] : '',
            address: props.subscriberData.address ? props.subscriberData.address : '',
            coordinateX: props.subscriberData.coordinates?.length ? props.subscriberData.coordinates[0] : null,
            coordinateY: props.subscriberData.coordinates?.length ? props.subscriberData.coordinates[1] : null,
            phones: props.subscriberData.phones ? props.subscriberData.phones : [],
            email: props.subscriberData.email ? props.subscriberData.email : '',
            webSite: props.subscriberData['web_site'] ? props.subscriberData['web_site'] : '',
        })
    }, [props]);

    const subscriberSchema = Yup.object().shape({
        title: Yup.string().required('Поле с наименованием абонента не может быть пустым'),
        TIN: Yup.string(),
        address: Yup.string(),
        coordinateX: Yup.number(),
        coordinateY: Yup.number(),
        phones: Yup.array(),
        email: Yup.string().email('Поле должно содержать email адрес'),
        webSite: Yup.string(),
    })

    return (
        <>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                Редактирование юридического лица
            </Grid>
            <Grid style={{marginBottom: '30px', display: 'flex', justifyContent: 'space-between',}}>
                <BackButtonComponent changed={!subscriberStateStatus}/>
                {currentUser.roles.includes('ROLE_SUPER_ADMIN') &&
                    <SimpleModal
                        startButtonTitle={'Удалить абонента'}
                        modalName={'deleteSubscriberModal'}
                        startButtonVariant={'text'}
                        closeButtonTitle={'Отменить'}
                        modalTitle={'Удаление абонента'}
                        acceptButtonTitle={'Подтвердить'}
                        onModalSubmit={deleteSubscriberHandler}
                    >
                        Удалить абонента?
                    </SimpleModal>
                }
            </Grid>
            {props.children}
            <Formik
                initialValues={initialValues}
                validationSchema={subscriberSchema}
                onSubmit={(values) => saveSubscriberHandler(values)}
            >
                {({
                    handleChange,
                    handleSubmit,
                    handleBlur,
                    dirty,
                    isValid,
                    errors,
                    values,
                    touched,
                    setFieldValue,
                    handleReset,
                }) => (
                    <Box
                        component={'form'}
                    >
                        <Grid container style={{marginBottom: '30px'}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'title'}
                                    value={values.title || ''}
                                    placeholder={'Введите наименование абонента'}
                                    label={'Наименование абонента'}
                                    onChange={(e) => {
                                        e.persist();
                                        setFieldValue('title', e.target.value);
                                        setSubscriberStateStatus(false);
                                    }}
                                    error={errors.title && touched.title}
                                    helperText={touched.title && errors.title}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px'}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'TIN'}
                                    placeholder={'Введите ИНН'}
                                    label={'ИНН'}
                                    value={values.TIN || ''}
                                    onChange={(e) => {
                                        e.persist();
                                        setFieldValue('TIN', e.target.value);
                                        setSubscriberStateStatus(false);
                                    }}
                                    error={errors.TIN && touched.TIN}
                                    helperText={touched.TIN && errors.TIN}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px'}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'address'}
                                    placeholder={'Введите адрес'}
                                    label={'Адрес'}
                                    value={values.address || ''}
                                    onChange={(e) => {
                                        e.persist();
                                        setFieldValue('address', e.target.value);
                                        setSubscriberStateStatus(false);
                                    }}
                                    error={errors.address && touched.address}
                                    helperText={touched.address && errors.address}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <Box sx={{color: 'secondary.main', fontSize: '18px', fontWeight: '500', marginBottom: '20px',}}>
                                    Координаты абонента
                                </Box>
                                <Box sx={{color: 'text.primary', fontSize: '18px', marginBottom: '30px',}}>
                                    {values.coordinateX && values.coordinateY
                                        ? '\[' + values.coordinateX + ', ' + values.coordinateY + '\]'
                                        : 'Нет данных'}
                                </Box>
                                {values.coordinateX && values.coordinateY  &&
                                    <MapComponent
                                        coordX={+values.coordinateX}
                                        coordY={+values.coordinateY}
                                        draggable={false}
                                        mapWidth={'100%'}
                                        mapHeight={'20vh'}
                                        enableSearch={false}
                                    />
                                }
                            </Grid>
                            <Grid item xs={2} style={{paddingRight: '15px',}}>
                                <SimpleMapModal
                                    coordX={+values.coordinateX}
                                    coordY={+values.coordinateY}
                                    modalName={'subscriberCoordinates'}
                                    modalTitle={'GPS координаты абонента'}
                                    startButtonTitle={'Изменить'}
                                    startButtonStyle={{width: '100%',}}
                                    // acceptButtonShow={true}
                                    onModalSubmit={(e) => {
                                        setFieldValue('coordinateX', e.x.toString());
                                        setFieldValue('coordinateY', e.y.toString());
                                        setSubscriberStateStatus(false);
                                    }}
                                    enableSearch={true}
                                    // disableStartButton={!task.new_coordinate_x && !task.new_coordinate_y}
                                />
                            </Grid>
                        </Grid>
                        <PhoneComponent
                            label={'Телефон'}
                            phones={values.phones}
                            onChange={(phones) => {
                                setFieldValue('phones', phones);
                                setSubscriberStateStatus(false);
                            }}
                            lastElemMargin={true}
                        />
                        <Grid container style={{marginBottom: '30px'}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'email'}
                                    value={values.email || ''}
                                    placeholder={'Введите email'}
                                    label={'Email'}
                                    onChange={(e) => {
                                        e.persist();
                                        setFieldValue('email', e.target.value);
                                        setSubscriberStateStatus(false);
                                    }}
                                    error={errors.email && touched.email}
                                    helperText={touched.email && errors.email}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px'}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'webSite'}
                                    value={values.webSite || ''}
                                    placeholder={'Введите сайт'}
                                    label={'Сайт'}
                                    onChange={(e) => {
                                        e.persist();
                                        setFieldValue('webSite', e.target.value);
                                        setSubscriberStateStatus(false);
                                    }}
                                    error={errors.webSite && !!touched.webSite}
                                    helperText={touched.webSite && errors.webSite}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={2} style={{paddingRight: '15px',}}>
                                <Button
                                    variant={'outlined'}
                                    fullWidth
                                    onClick={() => {
                                        handleReset();
                                        setSubscriberStateStatus(true);
                                    }}
                                    className={'base_styled_btn'}
                                >
                                    Отменить изменения
                                </Button>
                            </Grid>
                            <Grid item xs={2} style={{paddingRight: '15px',}}>
                                <Button
                                    variant={'contained'}
                                    fullWidth
                                    onClick={() => handleSubmit()}
                                    className={'base_styled_btn'}
                                    disabled={subscriberStateStatus}
                                >
                                    Сохранить
                                </Button>
                            </Grid>
                        </Grid>
                    </Box>
                )}
            </Formik>
            <BackButtonComponent changed={!subscriberStateStatus}/>
        </>
    )
}