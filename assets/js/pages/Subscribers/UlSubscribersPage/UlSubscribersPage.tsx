import React from "react";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import UlSubscribersList from "./UlSubscribersList";

export default function UlSubscribersPage() {
    return (
        <PageLayout>
            <UlSubscribersList/>
        </PageLayout>
    )
}