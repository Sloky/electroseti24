import React, {useEffect, useState} from "react";
import {SimpleFlashData} from "../../../interfaces/interfaces";
import {useDispatch} from "react-redux";
import {useHistory} from "react-router";
import {
    Box,
    Button,
    Grid,
    Pagination,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Tooltip
} from "@material-ui/core";
import FlashComponent from "../../../components/Flashes/FlashComponent";
import TableItemsCustomizer from "../../Tasks/TasksTable/TableItemsCustomizer";
import {THEME} from "../../../theme";
import {AppAction} from "../../../store/actions/actions";
import {getFlSubscribersRequest} from "../../../requests/Request";
import SearchComponent from "../../../components/Other/SearchComponent/SearchComponent";

const listHeader = [
    'ФИО',
    'Телефон',
    'Рейтинг',
];

export default function FlSubscribersList() {
    const [flashError, setFlashError] : [SimpleFlashData, any] = useState({status: false});
    const [flSubscribersData, setFlSubscribersData] = useState([]);

    const [itemsCount, setItemsCount] = useState<number>(100);
    const [pageNumber, setPageNumber] = useState<number>(1);
    const [totalItems, setTotalItems] = useState<number>(0);
    const [pagesCount, setPagesCount] = useState<number>(Math.ceil(totalItems/itemsCount));
    const [searchValue, setSearchValue] = useState('');

    const dispatch = useDispatch();
    const history = useHistory();

    const onSearchValueChange = (value: string) => {
        setSearchValue(value);
    };

    const applyFilters = () => {
        dispatch(AppAction.showLoader());
        getFlSubscribersRequest({
            page: pageNumber,
            maxResult: itemsCount,
            search: searchValue,
        }).then(result => {
            setTotalItems(result.data.subscribersCount);
            setFlSubscribersData(result.data.subscribers);
            dispatch(AppAction.hideLoader());
        })
    }

    useEffect(() => {
        dispatch(AppAction.showLoader());
        getFlSubscribersRequest({page: pageNumber, maxResult: itemsCount}).then(
            result => {
                if (result.type === 'success') {
                    setFlashError({status: false});
                    setFlSubscribersData(result.data.subscribers);
                    setTotalItems(result.data.subscribersCount);
                } else {
                    setFlashError({status: true, message: 'Обратитесь к администратору', messageHeading: 'Ошибка.'});
                }
            }
        ).finally(() => {
            dispatch(AppAction.hideLoader());
        })
    }, [])

    useEffect(() => {
        if (pageNumber > 0) {
            dispatch(AppAction.showLoader());
            getFlSubscribersRequest({page: pageNumber, maxResult: itemsCount, search: searchValue,}).then(
                result => {
                    setFlSubscribersData(result.data.subscribers);
                    setTotalItems(result.data.subscribersCount);
                    dispatch(AppAction.hideLoader());
                }
            )
        }
    }, [pageNumber, itemsCount]);

    useEffect(() => {
        setPagesCount(Math.ceil(totalItems/itemsCount || 1));
    }, [itemsCount, totalItems]);

    useEffect(() => {
        if (pageNumber > pagesCount) {
            setPageNumber(pagesCount ? pagesCount : 1);
        }
    }, [pagesCount]);

    const onPageNumberChange = (value: number) => {
        setPageNumber(value);
    };

    const onTableItemCountChange = (value: number) => {
        setItemsCount(value);
    };

    return (
        <>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                Абоненты
            </Grid>
            <Grid container style={{marginBottom: '30px',}}>
                <Grid item xs={2} style={{paddingRight: '15px',}}>
                    <Button
                        onClick={() => history.push('/subscriber/fl-new')}
                        variant={'contained'}
                        fullWidth
                        size={'medium'}
                        className={'base_styled_btn'}
                    >
                        Добавить физ. лицо
                    </Button>
                </Grid>
            </Grid>
            <FlashComponent
                message={flashError.message}
                showFlash={flashError.status}
                messageHeading={flashError.messageHeading}
                flashType={'error'}
            />
            <Grid container style={{marginBottom: '30px',}}>
                <Grid item xs={5}>
                    <SearchComponent
                        searchValue={searchValue}
                        onSearchValueChange={onSearchValueChange}
                        applyFilters={applyFilters}
                        searchFieldXs={8}
                        buttonXs={4}
                    />
                </Grid>
            </Grid>
            <Grid style={{marginBottom: '20px',}}>
                <TableItemsCustomizer
                    allItems={totalItems}
                    onTableItemCountChange={onTableItemCountChange}
                    initialCount={itemsCount}
                />
            </Grid>
            <TableContainer sx={{bgcolor: 'background.default'}}>
                <Table>
                    <TableHead>
                        <TableRow>
                            {listHeader.map((value, index) => (
                                <TableCell key={index}>{value}</TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {Array.isArray(flSubscribersData) && flSubscribersData.length ? flSubscribersData.map((subscriber, index) => (
                            <TableRow hover key={index} style={{cursor: 'pointer',}} onClick={() => history.push(`/subscriber/fl/${subscriber.id}`)}>
                                <TableCell style={{fontSize: '15px',}}>{subscriber.title}</TableCell>
                                <TableCell style={{fontSize: '15px',}}>{subscriber.phones.length && subscriber.phones[0].trim() !== '' ? subscriber.phones.join(', ') : 'Нет данных'}</TableCell>
                                <Tooltip title={'В разработке'} arrow>
                                    <TableCell style={{fontSize: '15px',}}>Нет данных</TableCell>
                                </Tooltip>
                            </TableRow>
                        )) : (
                            <></>
                        )}
                    </TableBody>
                </Table>
            </TableContainer>
            {!flSubscribersData || !flSubscribersData.length && (
                <Box style={{padding: '70px 0px', textAlign: 'center', width: '100%', color: THEME.PLACEHOLDER_TEXT, fontSize: '16px',}}>
                    Нет данных
                </Box>
            )}
            <Stack spacing={2} style={{marginTop: '30px',}}>
                <Pagination count={pagesCount} variant="outlined" shape="rounded" color="primary" onChange={(event, page) => onPageNumberChange(page)}/>
            </Stack>
        </>
    )
}