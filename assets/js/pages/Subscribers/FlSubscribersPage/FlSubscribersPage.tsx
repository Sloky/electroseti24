import React from "react";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import FlSubscribersList from "./FlSubscribersList";

export default function FlSubscribersPage() {
    return (
        <PageLayout>
            <FlSubscribersList/>
        </PageLayout>
    )
}