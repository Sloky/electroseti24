import React from "react";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import NewFlSubscriberCard from "./NewFlSubscriberCard";

export default function NewFlSubscriberPage() {
    const breadcrumbsItems = [
        {
            itemTitle: 'Физ. лица',
            itemSrc: '/subscribers/fl',
        },
        {
            itemTitle: 'Добавление физ. лица',
            // itemSrc: ``,
        },
    ]

    return (
        <PageLayout
            breadcrumbsItems={breadcrumbsItems}
        >
            <NewFlSubscriberCard/>
        </PageLayout>
    )
}