import React, {useState} from "react";
import {SimpleFlashData} from "../../../interfaces/interfaces";
import {useHistory} from "react-router";
import {useDispatch} from "react-redux";
import {AppAction} from "../../../store/actions/actions";
import {Formik} from "formik";
import * as Yup from "yup";
import {Box, Button, Grid, TextField} from "@material-ui/core";
import FlashComponent from "../../../components/Flashes/FlashComponent";
import PhoneComponent from "../../../components/Other/PhoneComponent/PhoneComponent";
import {addNewFlSubscriberRequest} from "../../../requests/Request";
import MapComponent from "../../../components/Other/MapComponent/MapComponent";
import SimpleMapModal from "../../../components/Modal/SimpleMapModal/SimpleMapModal";

interface flSubscriberFormInterface {
    surname: string
    name: string
    patronymic: string
    address: string
    coordinateX: number
    coordinateY: number
    phones: string[]
    email: string
}

export default function NewFlSubscriberCard() {
    const [flashError, setFlashError] : [SimpleFlashData, any] = useState({status: false});
    const [flashSuccess, setFlashSuccess] : [SimpleFlashData, any] = useState({status: false});

    const history = useHistory();
    const dispatch = useDispatch();

    const handleSubmit = (values) => {
        dispatch(AppAction.showLoader());
        addNewFlSubscriberRequest(values).then(
            result => {
                if (result.type === 'success') {
                    history.push('/subscribers/fl');
                } else {
                    setFlashError({
                        status: true,
                        message: 'Произошла ошибка. Обратитесь к администратору',
                    });
                }
            }
        ).finally(() => {
            dispatch(AppAction.hideLoader());
        });
    }

    const onCloseSuccessFlash = () => {
        setFlashSuccess({status: false});
    }

    const onCloseErrorFlash = () => {
        setFlashError({status: false});
    }

    const initialValues: flSubscriberFormInterface = {
        surname: '',
        name: '',
        patronymic: '',
        address: '',
        coordinateX: null,
        coordinateY: null,
        phones: [],
        email: '',
    }

    const flSubscriberSchema = Yup.object().shape({
        surname: Yup.string().required('Поле с фамилией не может быть пустым'),
        name: Yup.string().required('Поле с именем не может быть пустым'),
        patronymic: Yup.string(),
        address: Yup.string(),
        coordinateX: Yup.number().nullable(),
        coordinateY: Yup.number().nullable(),
        phones: Yup.array(),
        email: Yup.string().email('Поле должно содержать email адрес').required('Поле с email не может быть пустым'),
    })

    return (
        <>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                Добавление физ. лица
            </Grid>
            <FlashComponent
                messageHeading={flashError.messageHeading}
                message={flashError.message}
                showFlash={flashError.status}
                onCloseFlash={onCloseErrorFlash}
                flashType={'error'}
            />
            <FlashComponent
                message={flashSuccess.message}
                showFlash={flashSuccess.status}
                onCloseFlash={onCloseSuccessFlash}
                flashType={'success'}
            />
            <Formik
                initialValues={initialValues}
                validationSchema={flSubscriberSchema}
                onSubmit={(values) => handleSubmit(values)}
            >
                {({
                    handleChange,
                    handleSubmit,
                    handleBlur,
                    dirty,
                    isValid,
                    errors,
                    values,
                    touched,
                    setFieldValue,
                }) => (
                    <Box
                        component={'form'}
                    >
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={3} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'surname'}
                                    placeholder={'Введите фамилию'}
                                    label={'Фамилия'}
                                    onChange={handleChange}
                                    error={errors.surname && touched.surname}
                                    helperText={touched.surname && errors.surname}
                                />
                            </Grid>
                            <Grid item xs={3} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'name'}
                                    placeholder={'Введите имя'}
                                    label={'Имя'}
                                    onChange={handleChange}
                                    error={errors.name && touched.name}
                                    helperText={touched.name && errors.name}
                                />
                            </Grid>
                            <Grid item xs={3} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'patronymic'}
                                    placeholder={'Введите отчество'}
                                    label={'Отчество'}
                                    onChange={handleChange}
                                    error={errors.patronymic && touched.patronymic}
                                    helperText={touched.patronymic && errors.patronymic}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'address'}
                                    placeholder={'Введите адрес'}
                                    label={'Адрес'}
                                    onChange={handleChange}
                                    error={errors.address && touched.address}
                                    helperText={touched.address && errors.address}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <Box sx={{color: 'secondary.main', fontSize: '18px', fontWeight: '500', marginBottom: '20px',}}>
                                    Координаты абонента
                                </Box>
                                <Box sx={{color: 'text.primary', fontSize: '18px', marginBottom: '30px',}}>
                                    {values.coordinateX && values.coordinateY
                                        ? '\[' + values.coordinateX + ', ' + values.coordinateY + '\]'
                                        : 'Нет данных'}
                                </Box>
                                {values.coordinateX && values.coordinateY  &&
                                    <MapComponent
                                        coordX={values.coordinateX}
                                        coordY={values.coordinateY}
                                        draggable={false}
                                        mapWidth={'100%'}
                                        mapHeight={'20vh'}
                                        enableSearch={false}
                                    />
                                }
                            </Grid>
                            <Grid item xs={2} style={{paddingRight: '15px',}}>
                                <SimpleMapModal
                                    coordX={values.coordinateX}
                                    coordY={values.coordinateY}
                                    modalName={'subscriberCoordinates'}
                                    modalTitle={'GPS координаты абонента'}
                                    startButtonTitle={'Изменить'}
                                    startButtonStyle={{width: '100%',}}
                                    // acceptButtonShow={true}
                                    onModalSubmit={(e) => {
                                        setFieldValue('coordinateX', e.x.toString());
                                        setFieldValue('coordinateY', e.y.toString());
                                    }}
                                    enableSearch={true}
                                    // disableStartButton={!task.new_coordinate_x && !task.new_coordinate_y}
                                />
                            </Grid>
                        </Grid>
                        <PhoneComponent
                            label={'Телефон'}
                            phones={values.phones}
                            onChange={(phones) => setFieldValue('phones', phones)}
                            lastElemMargin={true}
                        />
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'email'}
                                    placeholder={'Введите email'}
                                    label={'Email'}
                                    onChange={handleChange}
                                    error={errors.email && touched.email}
                                    helperText={touched.email && errors.email}
                                />
                            </Grid>
                        </Grid>
                        <Grid container>
                            <Grid item xs={2} style={{paddingRight: '15px',}}>
                                <Button
                                    variant={'outlined'}
                                    fullWidth
                                    onClick={() => history.goBack()}
                                    className={'base_styled_btn'}
                                >
                                    Отменить
                                </Button>
                            </Grid>
                            <Grid item xs={2} style={{paddingRight: '15px',}}>
                                <Button
                                    variant={'contained'}
                                    fullWidth
                                    onClick={() => handleSubmit()}
                                    className={'base_styled_btn'}
                                >
                                    Создать абонента
                                </Button>
                            </Grid>
                        </Grid>
                    </Box>
                )}
            </Formik>
        </>
    )
}