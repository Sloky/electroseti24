import React from "react";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import NewUlSubscriberCard from "./NewUlSubscriberCard";

export default function NewUlSubscriberPage() {
    const breadcrumbsItems = [
        {
            itemTitle: 'Юр. лица',
            itemSrc: '/subscribers/ul',
        },
        {
            itemTitle: 'Добавление юр. лица',
            // itemSrc: ``,
        },
    ]

    return (
        <PageLayout
            breadcrumbsItems={breadcrumbsItems}
        >
            <NewUlSubscriberCard/>
        </PageLayout>
    )
}