import React, {useState} from "react";
import {SimpleFlashData} from "../../../interfaces/interfaces";
import {useHistory} from "react-router";
import {useDispatch} from "react-redux";
import {AppAction} from "../../../store/actions/actions";
import {Formik} from "formik";
import * as Yup from "yup";
import {Box, Button, Grid, TextField} from "@material-ui/core";
import FlashComponent from "../../../components/Flashes/FlashComponent";
import MapComponent from "../../../components/Other/MapComponent/MapComponent";
import SimpleMapModal from "../../../components/Modal/SimpleMapModal/SimpleMapModal";
import PhoneComponent from "../../../components/Other/PhoneComponent/PhoneComponent";
import {addNewUlSubscriberRequest} from "../../../requests/Request";

interface ulSubscriberFormInterface {
    title: string
    TIN: string
    address: string
    coordinateX: number
    coordinateY: number
    phones: string[]
    email: string
    webSite: string
}

export default function NewUlSubscriberCard() {
    const [flashError, setFlashError] : [SimpleFlashData, any] = useState({status: false});
    const [flashSuccess, setFlashSuccess] : [SimpleFlashData, any] = useState({status: false});

    const history = useHistory();
    const dispatch = useDispatch();

    const handleSubmit = (values) => {
        dispatch(AppAction.showLoader());
        addNewUlSubscriberRequest(values).then(
            result => {
                if (result.type === 'success') {
                    history.push('/subscribers/ul');
                } else {
                    setFlashError({
                        status: true,
                        message: 'Произошла ошибка. Обратитесь к администратору',
                    });
                }
            }
        ).finally(() => {
            dispatch(AppAction.hideLoader());
        });
    }

    const onCloseSuccessFlash = () => {
        setFlashSuccess({status: false});
    }

    const onCloseErrorFlash = () => {
        setFlashError({status: false});
    }

    const initialValues: ulSubscriberFormInterface = {
        title: '',
        TIN: '',
        address: '',
        coordinateX: null,
        coordinateY: null,
        phones: [],
        email: '',
        webSite: '',
    }

    const ulSubscriberSchema = Yup.object().shape({
        title: Yup.string().required('Поле с наименованием абонента не может быть пустым'),
        TIN: Yup.string().required('Поле с ИНН не может быть пустым'),
        address: Yup.string().required('Поле с адресом не может быть пустым'),
        coordinateX: Yup.number().nullable(),
        coordinateY: Yup.number().nullable(),
        phones: Yup.array(),
        email: Yup.string().email('Поле должно содержать email адрес'),
        webSite: Yup.string(),
    })

    return (
        <>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                Добавление юр. лица
            </Grid>
            <FlashComponent
                messageHeading={flashError.messageHeading}
                message={flashError.message}
                showFlash={flashError.status}
                onCloseFlash={onCloseErrorFlash}
                flashType={'error'}
            />
            <FlashComponent
                message={flashSuccess.message}
                showFlash={flashSuccess.status}
                onCloseFlash={onCloseSuccessFlash}
                flashType={'success'}
            />
            <Formik
                initialValues={initialValues}
                validationSchema={ulSubscriberSchema}
                onSubmit={(values) => handleSubmit(values)}
            >
                {({
                    handleChange,
                    handleSubmit,
                    handleBlur,
                    dirty,
                    isValid,
                    errors,
                    values,
                    touched,
                    setFieldValue,
                }) => (
                    <Box
                        component={'form'}
                    >
                        <Grid container style={{marginBottom: '30px'}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'title'}
                                    placeholder={'Введите наименование абонента'}
                                    label={'Наименование абонента'}
                                    onChange={handleChange}
                                    error={errors.title && touched.title}
                                    helperText={touched.title && errors.title}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px'}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'TIN'}
                                    placeholder={'Введите ИНН'}
                                    label={'ИНН'}
                                    onChange={handleChange}
                                    error={errors.TIN && touched.TIN}
                                    helperText={touched.TIN && errors.TIN}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px'}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'address'}
                                    placeholder={'Введите адрес'}
                                    label={'Адрес'}
                                    onChange={handleChange}
                                    error={errors.address && touched.address}
                                    helperText={touched.address && errors.address}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <Box sx={{color: 'secondary.main', fontSize: '18px', fontWeight: '500', marginBottom: '20px',}}>
                                    Координаты абонента
                                </Box>
                                <Box sx={{color: 'text.primary', fontSize: '18px', marginBottom: '30px',}}>
                                    {values.coordinateX && values.coordinateY
                                        ? '\[' + values.coordinateX + ', ' + values.coordinateY + '\]'
                                        : 'Нет данных'}
                                </Box>
                                {values.coordinateX && values.coordinateY  &&
                                    <MapComponent
                                        coordX={values.coordinateX}
                                        coordY={values.coordinateY}
                                        draggable={false}
                                        mapWidth={'100%'}
                                        mapHeight={'20vh'}
                                        enableSearch={false}
                                    />
                                }
                            </Grid>
                            <Grid item xs={2} style={{paddingRight: '15px',}}>
                                <SimpleMapModal
                                    coordX={values.coordinateX}
                                    coordY={values.coordinateY}
                                    modalName={'subscriberCoordinates'}
                                    modalTitle={'GPS координаты абонента'}
                                    startButtonTitle={'Изменить'}
                                    startButtonStyle={{width: '100%',}}
                                    // acceptButtonShow={true}
                                    onModalSubmit={(e) => {
                                        setFieldValue('coordinateX', e.x.toString());
                                        setFieldValue('coordinateY', e.y.toString());
                                    }}
                                    enableSearch={true}
                                    // disableStartButton={!task.new_coordinate_x && !task.new_coordinate_y}
                                />
                            </Grid>
                        </Grid>
                        <PhoneComponent
                            label={'Телефон'}
                            phones={values.phones}
                            onChange={(phones) => setFieldValue('phones', phones)}
                            lastElemMargin={true}
                        />
                        <Grid container style={{marginBottom: '30px'}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'email'}
                                    placeholder={'Введите email'}
                                    label={'Email'}
                                    onChange={handleChange}
                                    error={errors.email && touched.email}
                                    helperText={touched.email && errors.email}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px'}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'webSite'}
                                    placeholder={'Введите сайт'}
                                    label={'Сайт'}
                                    onChange={handleChange}
                                    error={errors.webSite && touched.webSite}
                                    helperText={touched.webSite && errors.webSite}
                                />
                            </Grid>
                        </Grid>
                        <Grid container>
                            <Grid item xs={2} style={{paddingRight: '15px',}}>
                                <Button
                                    variant={'outlined'}
                                    fullWidth
                                    onClick={() => history.goBack()}
                                    className={'base_styled_btn'}
                                >
                                    Отменить
                                </Button>
                            </Grid>
                            <Grid item xs={2} style={{paddingRight: '15px',}}>
                                <Button
                                    variant={'contained'}
                                    fullWidth
                                    onClick={() => handleSubmit()}
                                    className={'base_styled_btn'}
                                >
                                    Создать абонента
                                </Button>
                            </Grid>
                        </Grid>
                    </Box>
                )}
            </Formik>
        </>
    )
}