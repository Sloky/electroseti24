import React, {useEffect, useState} from "react";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import FlSubscriberEditCard from "./FlSubscriberEditCard";
import {SimpleFlashData} from "../../../interfaces/interfaces";
import {useDispatch, useSelector} from "react-redux";
import {useHistory, useParams} from "react-router";
import {AppAction} from "../../../store/actions/actions";
import {appStateInterface} from "../../../store/appState";
import FlashComponent from "../../../components/Flashes/FlashComponent";
import {
    deleteSubscriberRequest,
    editFlSubscriberRequest,
    getSubscriberCardDataRequest
} from "../../../requests/Request";

export default function FlSubscriberEditPage() {
    const [subscriberData, setSubscriberData] = useState({
        surname: '',
        name: '',
        patronymic: '',
        address: '',
        coordinateX: null,
        coordinateY: null,
        phones: [],
        email: '',
    });

    const [flashError, setFlashError] = useState<SimpleFlashData>({status: false})
    const [flashSuccess, setFlashSuccess] = useState({status: false, message: ''});

    const dispatch = useDispatch();
    const history = useHistory();

    let params: any = useParams();
    let id = params.id;

    const breadcrumbsItems = [
        {
            itemTitle: 'Физ. лица',
            itemSrc: '/subscribers/fl',
        },
        {
            itemTitle: 'Карточка абонента',
            itemSrc: `/subscriber/fl/${id}`,
        },
        {
            itemTitle: 'Редактирование абонента',
            // itemSrc: ``,
        },
    ]

    const onCloseErrorFlash = () => {
        setFlashError(prevState => {return {...prevState, status: false}})
    };

    const onCloseSuccessFlash = () => {
        setFlashSuccess(prevState => {return {...prevState, status: false}})
    };

    const showFlash = (type, message, messageHeading) => {
        switch (type) {
            case 'error':
                setFlashError({status: true, messageHeading: messageHeading, message: message});
                break;
            case 'success':
                setFlashSuccess({status: true, message: message});
                break;
            case 'disableFlashes':
                setFlashError({status: false,});
                setFlashSuccess({status: false, message: message});
        }
    }

    useEffect(() => {
        dispatch(AppAction.showLoader());
        getSubscriberCardDataRequest(id).then(
            result => {
                if (result.type === 'success') {
                    setSubscriberData(result.data);
                } else {
                    console.log('Error: ', result.data);
                    showFlash('error', 'Обратитесь к администратору', 'Ошибка.');
                }
            }
        ).finally(() => {
            dispatch(AppAction.hideLoader());
        })
    }, [])

    const onSaveHandler = (values) => {
        dispatch(AppAction.showLoader());
        editFlSubscriberRequest(id, values).then(
            result => {
                if (result.type === 'success') {
                    setSubscriberData(result.data);
                    history.push(`/subscriber/fl/${id}`);
                } else {
                    showFlash('error', 'Пожалуйста, обратитесь к администратору', 'Ошибка.')
                }
            }
        ).finally(() => {
            dispatch(AppAction.hideLoader());
        });
    }

    const onDeleteHandler = () => {
        dispatch(AppAction.showLoader());
        deleteSubscriberRequest(id).then(
            result => {
                if (result.type === 'success') {
                    history.push('/subscribers/fl');
                } else {
                    showFlash('error', 'Не удалось удалить абонента. Пожалуйста, обратитесь к администратору', 'Ошибка.')
                }
            }
        ).finally(() => {
            dispatch(AppAction.hideLoader());
        })
    }

    const loading = useSelector((state: appStateInterface) => state.loading);

    return (
        <PageLayout
            breadcrumbsItems={breadcrumbsItems}
        >
            {!loading ?
                <FlSubscriberEditCard
                    subscriberData={subscriberData}
                    onFlash={(type, message, messageHeading) => {showFlash(type, message, messageHeading)}}
                    onSaveHandler={onSaveHandler}
                    onDeleteHandler={onDeleteHandler}
                >
                    <FlashComponent
                        message={flashError.message}
                        messageHeading={flashError.messageHeading}
                        showFlash={flashError.status}
                        flashType={'error'}
                    />
                    <FlashComponent
                        message={flashSuccess.message}
                        showFlash={flashSuccess.status}
                        onCloseFlash={onCloseSuccessFlash}
                        flashType={'success'}
                    />
                </FlSubscriberEditCard> : <></>
            }
        </PageLayout>
    )
}