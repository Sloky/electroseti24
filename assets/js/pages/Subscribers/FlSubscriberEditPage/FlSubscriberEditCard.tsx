import React, {useEffect, useState} from "react";
import {Formik} from "formik";
import * as Yup from "yup";
import {Box, Button, Grid, TextField} from "@material-ui/core";
import SimpleModal from "../../../components/Modal/SimpleModal/SimpleModal";
import BackButtonComponent from "../../../components/Other/BackButtonComponent/BackButtonComponent";
import {SubscriberPostData} from "../../../interfaces/interfaces";
import MapComponent from "../../../components/Other/MapComponent/MapComponent";
import SimpleMapModal from "../../../components/Modal/SimpleMapModal/SimpleMapModal";
import PhoneComponent from "../../../components/Other/PhoneComponent/PhoneComponent";
import {useSelector} from "react-redux";
import {appStateInterface} from "../../../store/appState";

interface FlSubscriberEditCardProps {
    subscriberData: SubscriberPostData
    children?: any
    onFlash(type: string, message: string, messageHeading?: string): void
    onSaveHandler?(data: any): void
    onDeleteHandler?(): void
}

export default function FlSubscriberEditCard(props: FlSubscriberEditCardProps) {
    const [initialValues, setInitialValues] = useState({
        surname: props.subscriberData.surname ? props.subscriberData.surname : '',
        name: props.subscriberData.name ? props.subscriberData.name : '',
        patronymic: props.subscriberData.patronymic ? props.subscriberData.patronymic : '',
        address: props.subscriberData.address ? props.subscriberData.address : '',
        coordinateX: props.subscriberData.coordinates?.length ? props.subscriberData.coordinates[0] : null,
        coordinateY: props.subscriberData.coordinates?.length ? props.subscriberData.coordinates[1] : null,
        phones: props.subscriberData.phones ? props.subscriberData.phones : [],
        email: props.subscriberData.email ? props.subscriberData.email : '',
    });
    const [subscriberStateStatus, setSubscriberStateStatus] = useState(true);

    const currentUser = useSelector((state: appStateInterface) => state.user);

    const saveSubscriberHandler = (values) => {
        props.onSaveHandler(values);
    }

    const deleteSubscriberHandler = () => {
        props.onDeleteHandler();
    }

    useEffect(() => {
        setInitialValues({
            surname: props.subscriberData.surname ? props.subscriberData.surname : '',
            name: props.subscriberData.name ? props.subscriberData.name : '',
            patronymic: props.subscriberData.patronymic ? props.subscriberData.patronymic : '',
            address: props.subscriberData.address ? props.subscriberData.address : '',
            coordinateX: props.subscriberData.coordinates?.length ? props.subscriberData.coordinates[0] : null,
            coordinateY: props.subscriberData.coordinates?.length ? props.subscriberData.coordinates[1] : null,
            phones: props.subscriberData.phones ? props.subscriberData.phones : [],
            email: props.subscriberData.email ? props.subscriberData.email : '',
        })
    }, [props]);

    const subscriberSchema = Yup.object().shape({
        surname: Yup.string().required('Поле с фамилией не может быть пустым'),
        name: Yup.string().required('Поле с именем не может быть пустым'),
        patronymic: Yup.string().required('Поле с отчеством не может быть пустым'),
        address: Yup.string(),
        coordinateX: Yup.number(),
        coordinateY: Yup.number(),
        phone: Yup.string(),
        email: Yup.string().email('Поле должно содержать email адрес'),
    })

    return (
        <>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                Редактирование физического лица
            </Grid>
            <Grid style={{marginBottom: '30px', display: 'flex', justifyContent: 'space-between',}}>
                <BackButtonComponent changed={!subscriberStateStatus}/>
                {currentUser.roles.includes('ROLE_SUPER_ADMIN') &&
                    <SimpleModal
                        startButtonTitle={'Удалить абонента'}
                        modalName={'deleteSubscriberModal'}
                        startButtonVariant={'text'}
                        closeButtonTitle={'Отменить'}
                        modalTitle={'Удаление абонента'}
                        acceptButtonTitle={'Подтвердить'}
                        onModalSubmit={deleteSubscriberHandler}
                    >
                        Удалить абонента?
                    </SimpleModal>
                }
            </Grid>
            {props.children}
            <Formik
                initialValues={initialValues}
                validationSchema={subscriberSchema}
                onSubmit={(values) => saveSubscriberHandler(values)}
            >
                {({
                    handleChange,
                    handleSubmit,
                    handleBlur,
                    dirty,
                    isValid,
                    errors,
                    values,
                    touched,
                    setFieldValue,
                    handleReset,
                }) => (
                    <Box
                        component={'form'}
                    >
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={3} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'surname'}
                                    value={values.surname || ''}
                                    placeholder={'Введите фамилию'}
                                    label={'Фамилия'}
                                    onChange={(e) => {
                                        e.persist();
                                        setFieldValue('surname', e.target.value);
                                        setSubscriberStateStatus(false);
                                    }}
                                    error={errors.surname && !!touched.surname}
                                    helperText={touched.surname && errors.surname}
                                />
                            </Grid>
                            <Grid item xs={3} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'name'}
                                    value={values.name || ''}
                                    placeholder={'Введите имя'}
                                    label={'Имя'}
                                    onChange={(e) => {
                                        e.persist()
                                        setFieldValue('name', e.target.value);
                                        setSubscriberStateStatus(false);
                                    }}
                                    error={errors.name && !!touched.name}
                                    helperText={touched.name && errors.name}
                                />
                            </Grid>
                            <Grid item xs={3} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'patronymic'}
                                    value={values.patronymic || ''}
                                    placeholder={'Введите отчество'}
                                    label={'Отчество'}
                                    onChange={(e) => {
                                        e.persist();
                                        setFieldValue('patronymic', e.target.value);
                                        setSubscriberStateStatus(false);
                                    }}
                                    error={errors.patronymic && !!touched.patronymic}
                                    helperText={touched.patronymic && errors.patronymic}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'address'}
                                    value={values.address}
                                    placeholder={'Введите адрес'}
                                    label={'Адрес'}
                                    onChange={(e) => {
                                        e.persist();
                                        setFieldValue('address', e.target.value);
                                        setSubscriberStateStatus(false);
                                    }}
                                    error={errors.address && !!touched.address}
                                    helperText={touched.address && errors.address}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <Box sx={{color: 'secondary.main', fontSize: '18px', fontWeight: '500', marginBottom: '20px',}}>
                                    Координаты абонента
                                </Box>
                                <Box sx={{color: 'text.primary', fontSize: '18px', marginBottom: '30px',}}>
                                    {values.coordinateX && values.coordinateY
                                        ? '\[' + values.coordinateX + ', ' + values.coordinateY + '\]'
                                        : 'Нет данных'}
                                </Box>
                                {values.coordinateX && values.coordinateY  &&
                                    <MapComponent
                                        coordX={+values.coordinateX}
                                        coordY={+values.coordinateY}
                                        draggable={false}
                                        mapWidth={'100%'}
                                        mapHeight={'20vh'}
                                        enableSearch={false}
                                    />
                                }
                            </Grid>
                            <Grid item xs={2} style={{paddingRight: '15px',}}>
                                <SimpleMapModal
                                    coordX={+values.coordinateX}
                                    coordY={+values.coordinateY}
                                    modalName={'subscriberCoordinates'}
                                    modalTitle={'GPS координаты абонента'}
                                    startButtonTitle={'Изменить'}
                                    startButtonStyle={{width: '100%',}}
                                    // acceptButtonShow={true}
                                    onModalSubmit={(e) => {
                                        setFieldValue('coordinateX', e.x.toString());
                                        setFieldValue('coordinateY', e.y.toString());
                                        setSubscriberStateStatus(false);
                                    }}
                                    enableSearch={true}
                                    // disableStartButton={!task.new_coordinate_x && !task.new_coordinate_y}
                                />
                            </Grid>
                        </Grid>
                        <PhoneComponent
                            label={'Телефон'}
                            phones={values.phones}
                            onChange={(phones) => {
                                setFieldValue('phones', phones);
                                setSubscriberStateStatus(false);
                            }}
                            lastElemMargin={true}
                        />
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'email'}
                                    value={values.email || ''}
                                    placeholder={'Введите email'}
                                    label={'Email'}
                                    onChange={(e) => {
                                        e.persist();
                                        setFieldValue('email', e.target.value);
                                        setSubscriberStateStatus(false);
                                    }}
                                    error={errors.email && !!touched.email}
                                    helperText={touched.email && errors.email}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={2} style={{paddingRight: '15px',}}>
                                <Button
                                    variant={'outlined'}
                                    fullWidth
                                    onClick={() => {
                                        handleReset();
                                        setSubscriberStateStatus(true);
                                    }}
                                    className={'base_styled_btn'}
                                >
                                    Отменить изменения
                                </Button>
                            </Grid>
                            <Grid item xs={2} style={{paddingRight: '15px',}}>
                                <Button
                                    variant={'contained'}
                                    fullWidth
                                    onClick={() => handleSubmit()}
                                    className={'base_styled_btn'}
                                    disabled={subscriberStateStatus}
                                >
                                    Сохранить
                                </Button>
                            </Grid>
                        </Grid>
                    </Box>
                )}
            </Formik>
            <BackButtonComponent changed={!subscriberStateStatus}/>
        </>
    )
}