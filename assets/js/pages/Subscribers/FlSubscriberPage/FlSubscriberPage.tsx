import React, {useEffect, useState} from "react";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import FlSubscriberCard from "./FlSubscriberCard";
import {useDispatch} from "react-redux";
import {useParams} from "react-router";
import {AppAction} from "../../../store/actions/actions";
import {getSubscriberAgreementsDataRequest, getSubscriberCardDataRequest} from "../../../requests/Request";

export default function FlSubscriberPage() {
    const [subscriberData, setSubscriberData] = useState(null);
    const [agreementsData, setAgreementsData] = useState(null);

    const [subscriberLoaded, setSubscriberLoaded] = useState(false);
    const [agreementsLoaded, setAgreementsLoaded] = useState(false);

    const dispatch = useDispatch();
    let params: any = useParams();
    let id = params.id;

    const breadcrumbsItems = [
        {
            itemTitle: 'Физ. лица',
            itemSrc: '/subscribers/fl',
        },
        {
            itemTitle: 'Карточка абонента',
            // itemSrc: `/subscriber/fl/${subscriberData.id}`,
        },
    ]

    useEffect(() => {
        dispatch(AppAction.showLoader());
        getSubscriberCardDataRequest(id).then(
            result => {
                if (result.type === 'success') {
                    setSubscriberData(result.data);
                }
            }
        ).finally(() => {
            setSubscriberLoaded(true);
        })
        getSubscriberAgreementsDataRequest(id, {page: 1, maxResult: 100,}).then(
            result => {
                if (result.type === 'success') {
                    setAgreementsData(result.data);
                }
            }
        ).finally(() => {
            setAgreementsLoaded(true);
        })
    }, []);

    useEffect(() => {
        if (subscriberLoaded && agreementsLoaded) {
            dispatch(AppAction.hideLoader());
        }
    }, [subscriberLoaded, agreementsLoaded]);

    const getAgreements = (requestData) => {
        dispatch(AppAction.showLoader());
        getSubscriberAgreementsDataRequest(id, requestData).then(
            result => {
                if (result.type === 'success') {
                    setAgreementsData(result.data);
                }
            }
        ).finally(() => {
            dispatch(AppAction.hideLoader());
        })
    }

    return (
        <PageLayout
            breadcrumbsItems={breadcrumbsItems}
        >
            <FlSubscriberCard
                subscriberData={subscriberData}
                agreementsData={agreementsData && agreementsData.agreements ? agreementsData.agreements : []}
                agreementsCount={agreementsData && agreementsData.agreementsCount ? agreementsData.agreementsCount : 0}
                getAgreements={getAgreements}
            />
        </PageLayout>
    )
}