import React, {useEffect, useState} from "react";
import {useHistory} from "react-router";
import {
    Box,
    Button,
    Card,
    Grid,
    Pagination, Stack,
    Table, TableBody, TableCell,
    TableContainer,
    TableHead,
    TableRow,
} from "@material-ui/core";
import {THEME} from "../../../theme";
import TableItemsCustomizer from "../../Tasks/TasksTable/TableItemsCustomizer";
import SearchComponent from "../../../components/Other/SearchComponent/SearchComponent";
import {SubscriberAgreementData, SubscriberPostData} from "../../../interfaces/interfaces";
import SimpleMapModal from "../../../components/Modal/SimpleMapModal/SimpleMapModal";
import MapComponent from "../../../components/Other/MapComponent/MapComponent";

interface FlSubscriberCardProps {
    subscriberData: SubscriberPostData
    agreementsData: SubscriberAgreementData[]
    agreementsCount: number
    getAgreements(requestData: any): void
}

const listHeader = [
    'Номер договора',
    'Платежный код',
    'Узлы учёта',
    'Отделение',
];

export default function FlSubscriberCard(props: FlSubscriberCardProps) {
    const [subscriber, setSubscriber] = useState(props.subscriberData);
    const [agreements, setAgreements] = useState(props.agreementsData);

    const [searchValue, setSearchValue] = useState('');

    const [itemsCount, setItemsCount] = useState(100);
    const [totalItems, setTotalItems] = useState(props.agreementsCount);
    const [pagesCount, setPagesCount] = useState<number>(Math.ceil(totalItems/itemsCount || 1));
    const [pageNumber, setPageNumber] = useState<number>(1);

    const history = useHistory();

    const applyFilters = () => {
        props.getAgreements({page: pageNumber, maxResult: itemsCount, search: searchValue,});
    }

    const onSearchValueChange = (value: string) => {
        setSearchValue(value);
    };

    const onTableItemCountChange = (value: number) => {
        setItemsCount(value);
    };

    const onPageNumberChange = (value: number) => {
        setPageNumber(value);
    };

    useEffect(() => {
        setSubscriber(props.subscriberData);
        setTotalItems(props.agreementsCount);
        setAgreements(props.agreementsData);
    }, [props]);

    useEffect(() => {
        props.getAgreements({page: pageNumber, maxResult: itemsCount, search: searchValue,});
    }, [pageNumber, itemsCount]);

    useEffect(() => {
        setPagesCount(Math.ceil(totalItems/itemsCount || 1));
    }, [itemsCount, totalItems]);

    useEffect(() => {
        if (pageNumber > pagesCount) {
            setPageNumber(pagesCount);
        }
    }, [pagesCount]);

    return (
        <>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                Карточка абонента
            </Grid>
            {subscriber ?
                <>
                    <Card sx={{fontSize: '18px', fontWeight: '500', bgcolor: 'background.default', padding: '20px', marginBottom: '30px',}}>
                        <Grid container>
                            <Grid item xs={6}>
                                <Grid sx={{color: 'text.primary', marginBottom: '15px',}}>
                                    {subscriber.title || `${subscriber.surname} ${subscriber.name} ${subscriber.patronymic}`}
                                </Grid>
                                <Grid container style={{marginBottom: '15px',}}>
                                    <Grid item xs={3} sx={{color: THEME.PLACEHOLDER_TEXT,}}>Адрес</Grid>
                                    <Grid item xs={9}>{subscriber.address || 'Нет данных'}</Grid>
                                </Grid>
                                <Grid container style={{marginBottom: '15px',}}>
                                    <Grid item xs={3} sx={{color: THEME.PLACEHOLDER_TEXT,}}>Телефон</Grid>
                                    <Grid item xs={9}>{subscriber.phones.length && subscriber.phones[0].trim() !== '' ? subscriber.phones.join(', ') : 'Нет данных'}</Grid>
                                </Grid>
                                <Grid container style={{marginBottom: '30px',}}>
                                    <Grid item xs={3} sx={{color: THEME.PLACEHOLDER_TEXT,}}>Email</Grid>
                                    <Grid item xs={9}>{subscriber.email || 'Нет данных'}</Grid>
                                </Grid>
                                <Grid item xs={4}>
                                    <Button
                                        variant={'outlined'}
                                        size={'large'}
                                        onClick={() => history.push(`/subscriber-edit/fl/${subscriber.id}`)}
                                        className={'base_styled_btn'}
                                        fullWidth
                                    >
                                        Редактировать абонента
                                    </Button>
                                </Grid>
                            </Grid>
                            <Grid item xs={6}>
                                <Grid container style={{marginBottom: '30px',}}>
                                    <Grid item xs={8} style={{fontSize: '18px',}}>
                                        <Grid sx={{fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                                            Координаты абонента
                                        </Grid>
                                        <Grid sx={{color: 'text.primary',}}>
                                            {subscriber && subscriber.coordinates ?
                                                `[${subscriber.coordinates.join(', ')}]`
                                                : 'Нет данных'
                                            }
                                        </Grid>
                                    </Grid>
                                    <Grid item xs={4}>
                                        <SimpleMapModal
                                            coordX={subscriber && subscriber.coordinates ? subscriber.coordinates[0] : null}
                                            coordY={subscriber && subscriber.coordinates ? subscriber.coordinates[1] : null}
                                            modalName={'coordinates'}
                                            modalTitle={'GPS координаты узла учёта'}
                                            startButtonTitle={'Посмотреть'}
                                            startButtonStyle={{width: '100%',}}
                                            acceptButtonShow={false}
                                            onModalSubmit={() => {}}
                                            enableSearch={true}
                                            disableStartButton={!subscriber && !subscriber.coordinates}
                                        />
                                    </Grid>
                                </Grid>
                                <Grid container>
                                    <Grid item xs={8}>
                                        {subscriber && subscriber.coordinates &&
                                            <MapComponent
                                                coordX={subscriber.coordinates[0]}
                                                coordY={subscriber.coordinates[1]}
                                                draggable={false}
                                                mapWidth={'100%'}
                                                mapHeight={'20vh'}
                                                enableSearch={false}
                                            />
                                        }
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Card>
                    <Grid container style={{marginBottom: '30px',}}>
                        <Grid item xs={2}>
                            <Button
                                variant={'contained'}
                                size={'large'}
                                onClick={() => history.push(`/agreement-new/fl/${subscriber.id}`)}
                                className={'base_styled_btn'}
                                fullWidth
                            >
                                Добавить договор
                            </Button>
                        </Grid>
                    </Grid>
                    <Grid container style={{marginBottom: '30px',}}>
                        <Grid item xs={6}>
                            <SearchComponent
                                searchValue={searchValue}
                                onSearchValueChange={onSearchValueChange}
                                applyFilters={applyFilters}
                                searchFieldXs={8}
                                buttonXs={4}
                            />
                        </Grid>
                    </Grid>
                    <Grid style={{marginBottom: '20px',}}>
                        <TableItemsCustomizer
                            initialCount={itemsCount}
                            allItems={totalItems}
                            onTableItemCountChange={onTableItemCountChange}
                        />
                    </Grid>
                    <TableContainer sx={{bgcolor: 'background.default', marginBottom: '30px',}}>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    {listHeader.map((value, index) => (
                                        <TableCell key={index} style={{color: THEME.PLACEHOLDER_TEXT}}>{value}</TableCell>
                                    ))}
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {agreements && agreements.length ? agreements.map((data, index) => (
                                    <TableRow hover style={{cursor: 'pointer',}} key={index} onClick={() => history.push(`/agreement-page/fl/${data.id}`)}>
                                        <TableCell sx={{fontSize: '15px', fontWeight: '500',}}>
                                            {data.number}
                                        </TableCell>
                                        <TableCell sx={{fontSize: '15px', fontWeight: '500',}}>
                                            {data['payment_number'] || 'Нет данных'}
                                        </TableCell>
                                        <TableCell sx={{fontSize: '15px', fontWeight: '500',}}>
                                            {data['counter_count']}
                                        </TableCell>
                                        <TableCell sx={{fontSize: '15px', fontWeight: '500',}}>
                                            {data.company.title}
                                        </TableCell>
                                    </TableRow>
                                )) : (
                                    <></>
                                )}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    {agreements && !agreements.length && (
                        <Box style={{padding: '70px 0px', textAlign: 'center', width: '100%', color: THEME.PLACEHOLDER_TEXT, fontSize: '16px',}}>
                            Нет данных
                        </Box>
                    )}
                    <Stack spacing={2} style={{marginTop: '30px'}}>
                        <Pagination
                            count={pagesCount}
                            variant={'outlined'}
                            shape={'rounded'}
                            color={'primary'}
                            onChange={(event, page) => onPageNumberChange(page)}
                        />
                    </Stack>
                </> : <></>
            }
        </>
    )
}