import React, {useEffect, useState} from "react";
import PageLayout from "../../components/Layouts/PageLayout/PageLayout";
import {Box, Typography} from "@material-ui/core";
import BypassTablePage from "./BypassTablePage";
import FlashComponent from "../../components/Flashes/FlashComponent";
import {useDispatch, useSelector} from "react-redux";
import {
    CompanyBranch,
    RecordData,
    RecordTableRequestData,
    UserData
} from "../../interfaces/interfaces";
import {AppAction, EntitiesAction, TasksAction} from "../../store/actions/actions";
import {getBypassRecordsCollection, getBypassRecordTableData, getEntityCollection} from "../../functions/functions";
import {
    acceptRecordsToTasksRequest,
    bypassMassEditRequest,
    deleteBypassRecordsRequest,
    getBadgesCount,
    getBypassRecordsRequest,
    getCompaniesForFilterRequest,
    getControllersForTasksRequest,
    uploadBypassListRequest
} from "../../requests/Request";
import {appStateInterface} from "../../store/appState";
import {useHistory} from "react-router";

export default function BypassPage() {
    const [flashError, setFlashError] = useState({status: false, message: undefined, messages: []});
    const [flashSuccess, setFlashSuccess] = useState({status: false, message: ''});
    const [flashInfo, setFlashInfo] = useState({status: false, message: ''});
    const [flashWarning, setFlashWarning] = useState({status: false, message: ''});

    const [recordsLoaded, setRecordsLoaded] = useState(false);
    const [controllersLoaded, setControllersLoaded] = useState(false);
    const [branchesLoaded, setBranchesLoaded] = useState(false);

    const [recordTableData, setRecordTableData] = useState<{
        records: RecordData[]|[]
        controllers: UserData[]|[]
        branches: CompanyBranch[]|[]
        recordsCount: number
    }>({records: [], controllers: [], branches: [], recordsCount: 0});
    const [errorsCollection, setErrorsCollection] = useState([]);

    const dispatch = useDispatch();
    const history = useHistory();
    const filtersData = useSelector((state: appStateInterface) => state.filtersData);

    const onCloseErrorFlash = () => {
        setFlashError(prevState => {return {...prevState, status: false}})
    };

    const onCloseSuccessFlash = () => {
        setFlashSuccess(prevState => {return {...prevState, status: false}})
    };

    const onCloseInfoFlash = () => {
        setFlashInfo(prevState => {return {...prevState, status: false}})
    };

    const onCloseWarningFlash = () => {
        setFlashWarning(prevState => {return {...prevState, status: false}})
    };

    const makeErrorsCollection = (errors) => {
        let errorsCollection = {};
        errors.map(error => {
            if (errorsCollection[error.title]) {
                errorsCollection[error.title][errorsCollection[error.title].length] = [error.message];
            } else {
                errorsCollection[error.title] = [error.message];
            }
        });
        let entries = Object.entries(errorsCollection);
        return entries;
    }

    // Функция очистки всех уведомлений
    const clearFlashes = () => {
        setFlashError({messages: [], message: undefined, status: false});
        setFlashSuccess({message: undefined, status: false});
        setFlashWarning({message: undefined, status: false});
        setFlashInfo({message: undefined, status: false});
    }

    // Функция получения записей обходного листа
    const getBypassRecords = () => {
        setRecordsLoaded(false);
        const requestData = filtersData.filtersLocation !== history.location.pathname ? {page: 1, maxResult: 100} : {
            page: 1,
            maxResult: 100,
            search: filtersData.searchValue,
            companyTitle: filtersData.companyFilter,
            executor: filtersData.executorFilter,
            subscriberType: filtersData.subscriberTypeFilter,
            status: filtersData.statusFilter,
        }
        getBypassRecordsRequest(requestData).then(
            result => {
                if (result.type == 'success') {
                    setRecordTableData(prevState =>
                        {
                            return {
                                ...prevState,
                                records: result.data.records,
                                recordsCount: result.data.recordsCount,
                            }
                        }
                    );
                    dispatch(TasksAction.changeBypassRecordsAction(getBypassRecordsCollection(result.data.records)));
                } else {
                    console.log('Error: ', result.data);
                    if (result.code == 404) {
                        setRecordTableData(prevState =>
                            {
                                return {
                                    ...prevState,
                                    records: []
                                }
                            }
                        );
                        setFlashInfo({
                            status: true,
                            message: 'Нет доступных записей',
                        });
                    } else {
                        setFlashError({
                            messages: [],
                            status: true,
                            message: result.data
                        });
                    }
                }
            }
        ).finally(() => {
            setRecordsLoaded(true);
        });
    }

    const onTaskListLoad = (file, company) => {
        dispatch(AppAction.showTextLoader());
        setRecordsLoaded(false);
        clearFlashes();
        uploadBypassListRequest(file, company).then(
            result => {
                if (result.type == 'success') {
                    setRecordTableData(prevState =>
                        {
                            return {
                                ...prevState,
                                records: result.data.records,
                                branches: result.data.branches,
                                controllers: result.data.controllers,
                                recordsCount: result.data.recordsCount,
                            }
                        }
                    );
                    if (result.data.rowErrors.length !== 0) {
                        setErrorsCollection(makeErrorsCollection(result.data.rowErrors));
                        // setFlashError({
                        //     status: true,
                        //     message: result.data.rowErrors[0].message,
                        //     messages: null,
                        // });
                    } else {
                        setFlashSuccess({
                            status: true,
                            message: 'Список задач был успешно загружен'
                        });
                    }
                    dispatch(TasksAction.changeBypassRecordsAction(getBypassRecordsCollection(result.data.records)));
                    dispatch(EntitiesAction.changeCompanyBranchesAction(getEntityCollection(result.data.branches)));
                } else {
                    console.log('Error: ', result.data);
                    setFlashError({
                        messages: [],
                        status: true,
                        message: result.data.message
                    });
                }
            }
        ).finally(() => {
            setRecordsLoaded(true);
            dispatch(AppAction.hideTextLoader());
        })
    };

    const onBypassRecordsAccept = async (data: RecordTableRequestData) => {
        dispatch(AppAction.showTextLoader());
        setRecordsLoaded(false);
        clearFlashes();
        let status = data.status || '0';
        await acceptRecordsToTasksRequest(data, status).then(
            result => {
                if (result.type == 'success') {
                    if (result.data.rowErrors.length > 0) {
                        setFlashError({
                            status: true,
                            messages: result.data.rowErrors.map((error: {title: string, message: string}) => error.message),
                            message: undefined
                        });
                    } else {
                        setFlashSuccess({
                            status: true,
                            message: 'Успешно отправлено в задачи на выполнение',
                        });
                    }
                    setRecordTableData(prevState =>
                        {
                            return {
                                ...prevState,
                                records: result.data.records,
                                branches: result.data.branches,
                                controllers: result.data.controllers,
                                recordsCount: result.data.recordsCount,
                            }
                        }
                    );
                    dispatch(TasksAction.changeBypassRecordsAction(getBypassRecordsCollection(result.data.records)));
                    dispatch(EntitiesAction.changeCompanyBranchesAction(getEntityCollection(result.data.branches)));
                } else {
                    console.log('Error: ', result.data);
                    setFlashError({
                        messages: [],
                        status: true,
                        message: result.data.message,
                    });
                }
            }
        ).finally(() => {
            setRecordsLoaded(true);
            dispatch(AppAction.hideTextLoader());
        })
    }

    const onBypassRecordsDelete = async (data: RecordTableRequestData) => {
        dispatch(AppAction.showLoader());
        clearFlashes();
        await deleteBypassRecordsRequest(data).then(
            result => {
                if (result.type == 'success') {
                    setFlashSuccess({
                        status: true,
                        message: 'Записи были успешно удалены'
                    })
                } else {
                    console.log('Error: ', result.data);
                    setFlashError({
                        messages: [],
                        status: true,
                        message: result.data.message,
                    });
                }
            }
        ).finally(() => {
            getBypassRecords();
            dispatch(AppAction.hideLoader());
        })
    };

    const onBypassRecordsChange = (params, data) => {
        dispatch(AppAction.showLoader());
        setRecordsLoaded(false);
        clearFlashes();
        bypassMassEditRequest(params, data).then(
            result => {
                if (result.type == 'success') {
                    setFlashSuccess({
                        status: true,
                        message: 'Записи были успешно изменены'
                    })
                    setRecordTableData(prevState =>
                        {
                            return {
                                ...prevState,
                                records: result.data.bypassListRecordList,
                                recordsCount: result.data.bypassListRecordCount,
                            }
                        }
                    );
                    dispatch(TasksAction.changeBypassRecordsAction(getBypassRecordsCollection(result.data.bypassListRecordList)));
                } else {
                    console.log('Error: ', result);
                    setFlashError({
                        messages: [],
                        status: true,
                        message: result.data.message,
                    });
                }
            }
        ).finally(() => {
            setRecordsLoaded(true);
            dispatch(AppAction.hideLoader());
        })
    }

    useEffect(() => {
        dispatch(AppAction.showLoader());
        getBypassRecords();
        getControllersForTasksRequest().then(
            result => {
                if (result.type == 'success') {
                    setRecordTableData( prevState =>
                        {
                            return {
                                ...prevState,
                                controllers: result.data,
                            }
                        }
                    );
                    dispatch(EntitiesAction.changeUsersAction(getEntityCollection(result.data)));
                } else {
                    console.log('Error: ', result);
                    if (result.code == 404) {
                        setFlashInfo({
                            status:true,
                            message: 'Нет доступных исполнителей. Для возможности сортировки по ' +
                                'исполнителям добавьте их в меню "Пользователи"'
                        })
                    } else {
                        setFlashError({
                            messages: [],
                            status:true,
                            message: result.data
                        });
                    }
                }
            }
        ).finally(() => {
            setControllersLoaded(true);
        });
        getCompaniesForFilterRequest().then(
            result => {
                if (result.type == 'success') {
                    setRecordTableData( prevState =>
                        {
                            return {
                                ...prevState,
                                branches: result.data
                            }
                        }
                    );
                    dispatch(EntitiesAction.changeCompanyBranchesAction(getEntityCollection(result.data)));
                    if (result.data.length === 0) {
                        setFlashError({
                            messages: [],
                            status: true,
                            message: 'Нет доступных отделений. Обратитесь к администратору'
                        })
                    }
                } else {
                    console.log('Error: ', result.data);
                    setFlashError({
                        messages: [],
                        status: true,
                        message: result.data
                    });
                }
            }
        ).finally(() => {
            setBranchesLoaded(true);
        });
    }, []);

    useEffect(() => {
        if (recordsLoaded && controllersLoaded && branchesLoaded) {
            dispatch(AppAction.hideLoader());
        }
    }, [recordsLoaded, controllersLoaded, branchesLoaded]);

    useEffect(() => {
        getBadgesCount().then(
            result => {
                if (result.type == 'success') {
                    dispatch(TasksAction.changeBadgesCount(result.data));
                } else {
                    console.log('Error: ', result.data);
                }
            }
        )
    }, [recordTableData.records]);

    return (
        <PageLayout>
            <BypassTablePage
                records={getBypassRecordTableData(recordTableData.records)}
                controllers={recordTableData.controllers || []}
                branches={recordTableData.branches || []}
                recordsCount={recordTableData.recordsCount || 0}
                onTaskListLoad={onTaskListLoad}
                onBypassRecordsDelete={onBypassRecordsDelete}
                onBypassRecordsAccept={onBypassRecordsAccept}
                onBypassRecordsChange={onBypassRecordsChange}
                clearFlashes={clearFlashes}
                recordsLoaded={recordsLoaded}
            >
                {
                    errorsCollection.map((item, index) => <FlashComponent
                            key={index}
                            // messages={flashError.messages}
                            messageHeading={item[0]}
                            message={item[1].map((item, index) => <Box key={index}>{item}</Box>
                            )}
                            // onCloseFlash={onCloseErrorFlash}
                            showFlash={!!errorsCollection.length}
                            flashType="error"
                            flashWidth="100%"
                        />
                    )
                }
                <FlashComponent
                    message={flashError.message}
                    messages={flashError.messages}
                    // onCloseFlash={onCloseErrorFlash}
                    showFlash={flashError.status}
                    flashType="error"
                    flashWidth="100%"
                />
                <FlashComponent
                    message={flashWarning.message}
                    onCloseFlash={onCloseWarningFlash}
                    showFlash={flashWarning.status}
                    flashType="warning"
                />
                <FlashComponent
                    message={flashInfo.message}
                    onCloseFlash={onCloseInfoFlash}
                    showFlash={flashInfo.status}
                    flashType="info"
                />
                <FlashComponent
                    message={flashSuccess.message}
                    onCloseFlash={onCloseSuccessFlash}
                    showFlash={flashSuccess.status}
                    flashType="success"
                />
            </BypassTablePage>
        </PageLayout>
    )
};