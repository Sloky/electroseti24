import React, {useEffect, useState} from "react";
import {
    CompanyBranch,
    RecordTableData,
    RecordTableRequestData,
    UserData
} from "../../interfaces/interfaces";
import {useDispatch, useSelector} from "react-redux";
import {AppAction, TasksAction} from "../../store/actions/actions";
import {Button, Grid, Pagination, Stack, Typography} from "@material-ui/core";
import SimpleLoadFileModal from "../../components/Modal/SimpleLoadFileModal/SimpleLoadFileModal";
import SimpleSelectInput from "../../components/FormInputs/SimpleSelectInput/SimpleSelectInput";
import TableItemsCustomizer from "../Tasks/TasksTable/TableItemsCustomizer";
import SimpleModal from "../../components/Modal/SimpleModal/SimpleModal";
import {DeleteOutline} from "@material-ui/icons";
import BypassTable from "../../components/Tables/BypassTable";
import {getBypassRecordsRequest} from "../../requests/Request";
import {getBypassRecordsCollection, getBypassRecordTableData} from "../../functions/functions";
import MassEditModal from "../../components/Modal/MassEditModal/MassEditModal";
import {useHistory} from "react-router";
import {appStateInterface} from "../../store/appState";
import SearchComponent from "../../components/Other/SearchComponent/SearchComponent";

interface BypassTablePageProps {
    records: RecordTableData[]
    controllers: UserData[]
    branches: CompanyBranch[]
    recordsCount: number
    children?: any
    onTaskListLoad(file: any, value: any): void
    onBypassRecordsDelete?(data: RecordTableRequestData): void
    onBypassRecordsAccept?(data: RecordTableRequestData): void
    onBypassRecordsChange?(params: any, data: any): void // TODO: создать интерфейсы
    clearFlashes?(): void
    recordsLoaded?: boolean
}

const headerItems = [
    'Договор',
    'Наименование абонента',
    'Узел учёта',
    'Отделение',
    'Исполнитель',
    'Срок выполнения',
    'Статус',
];

const selectSubscriberType = [ // TODO: вынести в константы?
    {title: 'Физическое лицо', value: 'Физическое лицо'},
    {title: 'Юридическое лицо', value: 'Юридическое лицо'},
    {title: 'Центральный аппарат', value: 'Центральный аппарат'},
    {title: 'Электрон', value: 'Электрон'},
];

const selectStatus = [
    {title: 'Готово к постановке', value: '0',},
    {title: 'Требует дополнения', value: '1',},
    {title: 'Критические ошибки', value: '2',},
]

export default function BypassTablePage(props: BypassTablePageProps) {
    const [itemsCount, setItemsCount] = useState<number>(100);
    const [pageNumber, setPageNumber] = useState<number>(1);
    const [totalItems, setTotalItems] = useState<number>(props.recordsCount);
    const [pagesCount, setPagesCount] = useState<number>(Math.ceil(totalItems/itemsCount || 1));
    const [records, setRecords] = useState<RecordTableData[]>(props.records);

    const [companyFilter, setCompanyFilter] = useState(null);
    const [executorFilter, setExecutorFilter] = useState(null);
    const [subscriberTypeFilter, setSubscriberTypeFilter] = useState(null);
    const [statusFilter, setStatusFilter] = useState(null);
    const [searchValue, setSearchValue] = useState('');

    const [companyValue, setCompanyValue] = useState(null);
    const [selected, setSelected] = useState([]);

    const dispatch = useDispatch();
    const history = useHistory();

    const filtersData = useSelector((state: appStateInterface) => state.filtersData);
    const currentUser = useSelector((state: appStateInterface) => state.user);

    const onFilterClear = () => {
        setCompanyFilter(null);
        setExecutorFilter(null);
        setSubscriberTypeFilter(null);
        setStatusFilter(null);
        setSearchValue('');
        dispatch(AppAction.showLoader());
        setSelected([]);
        props.clearFlashes();
        dispatch(TasksAction.changeFilters({
            searchValue: '',
            companyFilter: null,
            executorFilter: null,
            subscriberTypeFilter: null,
            statusFilter: null,
            filtersLocation: history.location.pathname,
        }));
        getBypassRecordsRequest({
            page: pageNumber,
            maxResult: itemsCount,
            search: '',
            companyTitle: null,
            executor: null,
            subscriberType: null,
            status: null,
        }).then(result => {
            setTotalItems(result.data.recordsCount);
            setRecords(getBypassRecordTableData(result.data.records));
            dispatch(TasksAction.changeBypassRecordsAction(getBypassRecordsCollection(result.data.records)));
            dispatch(AppAction.hideLoader());
        })
    };

    const onSearchValueChange = (value: string) => {
        setSearchValue(value);
    };

    const onPageNumberChange = (value: number) => {
        setPageNumber(value);
    };

    const onTableItemCountChange = (value: number) => {
        setItemsCount(value);
    };

    const onTaskListFileSubmit = (file) => {
        onFilterClear();
        props.onTaskListLoad(file, companyValue);
    };

    const onTasksAccept = () => {
        props.onBypassRecordsAccept({
            maxResult: itemsCount,
            search: searchValue,
            companyTitle: companyFilter,
            executor: executorFilter,
            subscriberType: subscriberTypeFilter,
            status: statusFilter,
            items: selected,
        });
        setSelected([]);
    }

    const onTaskListClear = () => {
        props.onBypassRecordsDelete({
            search: searchValue,
            companyTitle: companyFilter,
            executor: executorFilter,
            subscriberType: subscriberTypeFilter,
            status: statusFilter,
            items: selected,
        });
        setSelected([]);
    }

    useEffect(() => {
        if (filtersData && filtersData.filtersLocation === history.location.pathname) {
            setCompanyFilter(filtersData.companyFilter);
            setExecutorFilter(filtersData.executorFilter);
            setSearchValue(filtersData.searchValue);
            setSubscriberTypeFilter(filtersData.subscriberTypeFilter);
            setStatusFilter(filtersData.statusFilter);
        }
    }, []);

    useEffect(() => {
        if (props.recordsLoaded) {
            setTotalItems(props.recordsCount);
            setRecords(props.records);
        }
    }, [props.recordsLoaded])

    useEffect(() => {
        dispatch(AppAction.showLoader());
        const requestData = filtersData.filtersLocation !== history.location.pathname ? {page: pageNumber, maxResult: itemsCount} : {
            page: pageNumber,
            maxResult: itemsCount,
            search: filtersData.searchValue,
            companyTitle: filtersData.companyFilter,
            executor: filtersData.executorFilter,
            subscriberType: filtersData.subscriberTypeFilter,
            status: filtersData.statusFilter,
        }
        getBypassRecordsRequest(requestData)
            .then(result => {
                setRecords(getBypassRecordTableData(result.data.records));
                setTotalItems(result.data.recordsCount);
                dispatch(TasksAction.changeBypassRecordsAction(getBypassRecordsCollection(result.data.records)));
                dispatch(AppAction.hideLoader());
            })
    }, [pageNumber, itemsCount])

    useEffect(() => {
        setPagesCount(Math.ceil(totalItems/itemsCount || 1));
    }, [itemsCount, totalItems]);

    useEffect(() => {
        if (pageNumber > pagesCount) {
            setPageNumber(pagesCount);
        }
    }, [pagesCount]);

    const applyFilters = () => {
        dispatch(AppAction.showLoader());
        setSelected([]);
        props.clearFlashes();
        dispatch(TasksAction.changeFilters({
            searchValue,
            companyFilter,
            executorFilter,
            subscriberTypeFilter,
            statusFilter,
            filtersLocation: history.location.pathname,
        }));
        getBypassRecordsRequest({
            page: pageNumber,
            maxResult: itemsCount,
            search: searchValue,
            companyTitle: companyFilter,
            executor: executorFilter,
            subscriberType: subscriberTypeFilter,
            status: statusFilter,
        }).then(result => {
            setTotalItems(result.data.recordsCount);
            setRecords(getBypassRecordTableData(result.data.records));
            dispatch(TasksAction.changeBypassRecordsAction(getBypassRecordsCollection(result.data.records)));
            dispatch(AppAction.hideLoader());
        })
    }

    const onRecordSelect = (items) => {
        setSelected(items);
    }

    const onMassEdit = (values) => {
        let params = {
            page: pageNumber,
            maxResult: itemsCount,
            search: searchValue,
            companyTitle: companyFilter,
            executor: executorFilter,
            subscriberType: subscriberTypeFilter,
            status: statusFilter,
        }
        let data = {
            deadline: values.deadline,
            executor: values.executor,
            items: selected,
        }
        props.onBypassRecordsChange(params, data);
        setSelected([]);
    }

    const controllersForSelect = () => {
        let newControllers = props.controllers.map(controller => {
            let userFullName = controller.surname+' '+controller.name+' '+controller.patronymic;
            return {
                title: userFullName,
                value: userFullName,
            }
        })
        newControllers.unshift({title: 'Без исполнителя', value: 'empty'});
        return newControllers
    }

    const changeModalSubTitle = (modal, selectedCount) => {
        if (modal === 'massEdit') {
            if (selectedCount === 1) {
                return 'Будет изменена 1 запись';
            } else if (selectedCount > 1 && selectedCount < 5) {
                return `Будет изменено ${selectedCount} записи`;
            } else {
                return `Будет изменено ${selectedCount} записей`;
            }
        } else if (modal === 'delete') {
            if (selectedCount === 0) {
                if (totalItems === 1) {
                    return 'Будет удалена 1 запись. Удалить?'
                } else if (totalItems > 1 && totalItems < 5) {
                    return `Будет удалено ${totalItems} записи. Удалить?`
                } else {
                    return `Будет удалено ${totalItems} записей. Удалить?`
                }
            } else if (selectedCount === 1) {
                return 'Будет удалена 1 запись. Удалить?';
            } else if (selectedCount > 1 && selectedCount < 5) {
                return `Будет удалено ${selectedCount} записи. Удалить?`;
            } else {
                return `Будет удалено ${selectedCount} записей. Удалить?`;
            }
        } else if (modal === 'acceptRecords') {
            if (selectedCount === 0) {
                if (totalItems === 1) {
                    return (<>
                        Будет отправлена 1 запись в задачи на выполнение.
                        <Typography component={"span"} sx={{color: 'error.dark', display: 'flex',}}>
                            Отправлены будут только записи со статусом "Готово к постановке".
                        </Typography>
                        Подтвердить?
                    </>)
                } else if (totalItems > 1 && totalItems < 5) {
                    return (<>
                        {`Будет отправлено ${totalItems} записи в задачи на выполнение.`}
                        <Typography component={"span"} sx={{color: 'error.dark', display: 'flex',}}>
                            Отправлены будут только записи со статусом "Готово к постановке".
                        </Typography>
                        Подтвердить?
                    </>)
                } else {
                    return (<>
                        {`Будет отправлено ${totalItems} записей в задачи на выполнение.`}
                        <Typography component={"span"} sx={{color: 'error.dark', display: 'flex',}}>
                            Отправлены будут только записи со статусом "Готово к постановке".
                        </Typography>
                        Подтвердить?
                    </>)
                }
            } else if (selectedCount === 1) {
                return (<>
                    Будет отправлена 1 запись в задачи на выполнение.
                    <Typography component={"span"} sx={{color: 'error.dark', display: 'flex',}}>
                        Отправлены будут только записи со статусом "Готово к постановке".
                    </Typography>
                    Подтвердить?
                </>)
            } else if (selectedCount > 1 && selectedCount < 5) {
                return (<>
                    {`Будет отправлено ${selectedCount} записи в задачи на выполнение.`}
                    <Typography component={"span"} sx={{color: 'error.dark', display: 'flex',}}>
                        Отправлены будут только записи со статусом "Готово к постановке".
                    </Typography>
                    Подтвердить?
                </>)
            } else {
                return (<>
                    {`Будет отправлено ${selectedCount} записей в задачи на выполнение.`}
                    <Typography component={"span"} sx={{color: 'error.dark', display: 'flex',}}>
                        Отправлены будут только записи со статусом "Готово к постановке".
                    </Typography>
                    Подтвердить?
                </>)
            }
        }
    }

    return (
        <>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                Обходной лист
            </Grid>
            <Grid container style={{marginBottom: '30px',}}>
                {currentUser.roles.includes('ROLE_SUPER_ADMIN') &&
                    <Grid item xs={2} style={{paddingRight: '15px',}}>
                        <SimpleLoadFileModal
                            modalName={'bypassList'}
                            modalTitle={'Загрузка обходного листа'}
                            startButtonTitle={'Загрузить обходной лист'}
                            acceptButtonTitle={'Загрузить'}
                            startButtonVariant={'outlined'}
                            onModalSubmit={onTaskListFileSubmit}
                            disableAcceptButton={companyValue}
                            startButtonStyle={{width: '100%',}}
                        >
                            <Grid style={{marginBottom: '30px',}}>
                                <SimpleSelectInput
                                    label={'Выбрать отделение'}
                                    options={props.branches.map(company => {
                                        return {
                                            title: company.title,
                                            value: company.id,
                                        }
                                    })}
                                    onChange={(value) => setCompanyValue(value)}
                                    defaultValue={companyValue}
                                    allowEmptyValue={true}
                                    emptyValueTitle={'Не выбрано'}
                                    formControlStyle={{width: '100%',}}
                                />
                            </Grid>
                        </SimpleLoadFileModal>
                    </Grid>
                }
                <Grid item xs={2} style={{paddingRight: '15px',}}>
                    <SimpleModal
                        modalName={'acceptBypassRecords'}
                        modalTitle={'Отправка записей в задачи'}
                        startButtonTitle={'Отправить в задачи'}
                        acceptButtonTitle={'Подтвердить'}
                        onModalSubmit={onTasksAccept}
                        startButtonVariant={'contained'}
                        closeButtonTitle={'Отменить'}
                        startButtonStyle={{width: '100%',}}
                        disableStartButton={!totalItems}
                    >
                        {changeModalSubTitle('acceptRecords', selected.length)}
                    </SimpleModal>
                </Grid>
            </Grid>
            {props.children}
            <Grid container style={{marginBottom: '30px',}}>
                <Grid item xs={2} style={{paddingRight: '15px',}}>
                    <SimpleSelectInput
                        label={'Отделение'}
                        allowEmptyValue={true}
                        emptyValueTitle={'Все отделения'}
                        defaultValue={companyFilter}
                        options={props.branches.map(branch => {
                            return {
                                title: branch.title,
                                value: branch.title,
                            }
                        })}
                        formControlStyle={{width: '100%',}}
                        onChange={value => {
                            setCompanyFilter(value)
                        }}
                    />
                </Grid>
                <Grid item xs={2} style={{paddingRight: '15px',}}>
                    <SimpleSelectInput
                        label={'Исполнитель'}
                        allowEmptyValue={true}
                        emptyValueTitle={'Все исполнители'}
                        defaultValue={executorFilter}
                        options={controllersForSelect()}
                        formControlStyle={{width: '100%',}}
                        onChange={value => {setExecutorFilter(value)}}
                    />
                </Grid>
                <Grid item xs={2} style={{paddingRight: '15px',}}>
                    <SimpleSelectInput
                        label={'Тип абонента'}
                        allowEmptyValue={true}
                        emptyValueTitle={'Все абоненты'}
                        options={selectSubscriberType}
                        defaultValue={subscriberTypeFilter}
                        formControlStyle={{width: '100%',}}
                        onChange={value => {setSubscriberTypeFilter(value)}}
                    />
                </Grid>
                <Grid item xs={2} style={{paddingRight: '15px',}}>
                    <SimpleSelectInput
                        label={'Статус'}
                        allowEmptyValue={true}
                        emptyValueTitle={'Все статусы'}
                        options={selectStatus}
                        defaultValue={statusFilter}
                        formControlStyle={{width: '100%',}}
                        onChange={value => {setStatusFilter(value)}}
                    />
                </Grid>
            </Grid>
            <Grid container style={{marginBottom: '30px',}}>
                <Grid item xs={8}>
                    <SearchComponent
                        searchValue={searchValue}
                        onSearchValueChange={onSearchValueChange}
                        applyFilters={applyFilters}
                        searchFieldXs={6}
                        buttonXs={3}
                        enableFilters={true}
                        onFilterClear={onFilterClear}
                    />
                </Grid>
                <Grid item xs={2}/>
                <Grid item xs={2}>
                    <MassEditModal
                        onModalSubmit={(values) => onMassEdit(values)}
                        modalName={'massEditModal'}
                        modalTitle={'Изменение записей'}
                        modalSubTitle={changeModalSubTitle('massEdit', selected.length)}
                        startButtonTitle={'Изменить записи'}
                        disableStartButton={!selected.length}
                        closeButtonTitle={'Отменить'}
                        acceptButtonTitle={'Изменить'}
                        startButtonStyle={{width: '100%',}}
                        executorOptions={props.controllers.map(controller => {
                            let userFullName = controller.surname+' '+controller.name+' '+controller.patronymic;
                            return {
                                title: userFullName,
                                value: userFullName,
                            }
                        })}
                    />
                </Grid>
            </Grid>
            <Grid style={{display: 'flex', justifyContent: 'space-between', marginBottom: '20px',}}>
                <TableItemsCustomizer
                    initialCount={itemsCount}
                    onTableItemCountChange={onTableItemCountChange}
                    allItems={totalItems}
                />
                {currentUser.roles.includes('ROLE_SUPER_ADMIN') && (records.length > 0) &&
                    <SimpleModal
                        modalName={'clearBypassRecords'}
                        modalTitle={'Удаление списка записей'}
                        startButtonTitle={'Удалить записи'}
                        acceptButtonTitle={'Подтвердить'}
                        onModalSubmit={onTaskListClear}
                        startIcon={<DeleteOutline/>}
                        startButtonVariant={'text'}
                        closeButtonTitle={'Отменить'}
                    >
                        {changeModalSubTitle('delete', selected.length)}
                    </SimpleModal>
                }
            </Grid>
            <Grid>
                <BypassTable
                    headerItems={headerItems}
                    tableDataRows={records}
                    onRecordSelect={onRecordSelect}
                    selected={selected}
                    itemsCount={itemsCount}
                    totalItems={totalItems}
                />
                <Stack spacing={2} style={{marginTop: '30px',}}>
                    <Pagination count={pagesCount} variant="outlined" shape="rounded" color="primary" onChange={(event, page) => onPageNumberChange(page)}/>
                </Stack>
            </Grid>
        </>
    )
};