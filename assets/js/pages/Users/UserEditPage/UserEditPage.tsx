import React, {useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import UserEditCard from "./UserEditCard";
import "./UserEditPage.css";
import {getUserEditCardDataRequest} from "../../../requests/Request";
import {Box, CircularProgress} from "@material-ui/core";

export default function UserEditPage() {
    const [loadingData, setLoadingData] = useState({
        user: null,
        groups: null,
        companies: null,
        isLoaded: false
    });

    let params: any = useParams();
    let id = params.id;
    let role = params.role;

    const breadcrumbsItems = [
        {
            itemTitle: role === 'all' ? 'Пользователи' : role === 'admins' ? 'Руководители' : role === 'operators' ? 'Операторы' : 'Контроллеры',
            itemSrc: role === 'all' ? '/users-all' : `/users/${role}`,
        },
        {
            itemTitle: 'Карточка пользователя',
            itemSrc: `/${role}/user/${id}`,
        },
        {
            itemTitle: 'Редактирование пользователя',
            // itemSrc: ``,
        },
    ]

    async function loadData(userId) {
        let userEdtCardData = await getUserEditCardDataRequest(userId);
        if (userEdtCardData.type == 'success') {
            let data = userEdtCardData.data;
            return {
                user: data.user,
                groups: data.groups,
                companies: data.companies.companies,
                isLoaded: true
            };
        } else {
            return null;
        }
    }

    useEffect(() => {
        if (!loadingData.isLoaded) {
            loadData(id).then(result => {
                if (result) {
                    setLoadingData(result);
                } else {
                    setLoadingData({
                        user: null,
                        groups: null,
                        companies: null,
                        isLoaded: true
                    });
                }
            });
        }
    });

    return (
        <PageLayout
            breadcrumbsItems={breadcrumbsItems}
        >
            {!loadingData.isLoaded ? (
                <Box style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '500px',}}>
                    <CircularProgress/>
                </Box>
            ) : (
                <UserEditCard
                    userData={loadingData.user}
                    userGroupData={loadingData.groups}
                    companiesData={loadingData.companies}
                />
            )}
        </PageLayout>
    );
}