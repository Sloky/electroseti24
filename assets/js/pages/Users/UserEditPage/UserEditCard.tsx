import React, {useState} from "react";
import "./UserEditCard.css";
import SimpleTextInput from "../../../components/FormInputs/SimpleTextInput/SimpleTextInput";
import {userStatuses} from "../UsersPage/UsersList";
import SimpleModal from "../../../components/Modal/SimpleModal/SimpleModal";
import SimpleSelectInput from "../../../components/FormInputs/SimpleSelectInput/SimpleSelectInput";
import {
    CompanyData,
    IDictionary,
    SimpleFlashData,
    UserData,
    UserDataForSending,
    UserGroupData
} from "../../../interfaces/interfaces";
import {useHistory} from "react-router";
import {deleteUserRequest, editUserRequest} from "../../../requests/Request";
import {Box, Button, Grid} from "@material-ui/core";
import FlashComponent from "../../../components/Flashes/FlashComponent";
import SimpleCheckboxFormGroup from "../../../components/FormInputs/SimpleCheckboxFormGroup/SimpleCheckboxFormGroup";
import SimpleMultiSelectInput from "../../../components/FormInputs/MultiSelectInput/SimpleMultiSelectInput";
import ChangePasswordModal from "../../../components/Modal/ChangePasswordModal/ChangePasswordModal";
import {useDispatch, useSelector} from "react-redux";
import {AppAction} from "../../../store/actions/actions";
import {changeGroupNames} from "../../../functions/functions";
import BackButtonComponent from "../../../components/Other/BackButtonComponent/BackButtonComponent";
import {appStateInterface} from "../../../store/appState";
import {useParams} from "react-router-dom";

interface UserEditFormProps {
    userData: UserData,
    userGroupData: UserGroupData[],
    companiesData: CompanyData[]
}

const componentParameters = {
    resetButtonTitle: 'Отменить изменения',
    submitButtonTitle: 'Сохранить изменения',
    changePasswordButton: 'Изменить пароль',
    deleteUser: 'Удалить пользователя'
};

const getStatuses = () => {
    let statuses = [];
    for (let key in userStatuses) {
        statuses.push({
            value: key, title: userStatuses[key]
        });
    }
    return statuses;
};

const convertUserDataForSending = (userData: UserData, userGroups: string[], userCompanies: string[]): UserDataForSending => {
    let newUserData: UserDataForSending = {
        ...userData,
        companies: userCompanies,
        groups: userGroups,
    };
    newUserData.work_status = Number(newUserData.work_status);
    delete newUserData.id;
    return newUserData;
};

export default function UserEditCard(props: UserEditFormProps) {
    const [user, setUser] = useState(props.userData);
    const [userGroups, setUserGroups] = useState(props.userData.groups.map((group) => (
        group.id
    )));
    const [userCompanies, setUserCompanies] = useState(props.userData.companies.map((company) => (
        company.id
    )));
    const [userStateStatus, setUserStateStatus] = useState(true);
    const [flashSuccess, setFlashSuccess] = useState<SimpleFlashData>({status: false});
    const [flashError, setFlashError] = useState<SimpleFlashData>({status: false});

    let params: any = useParams();
    let role = params.role;

    const history = useHistory();
    const dispatch = useDispatch();
    const currentUser = useSelector((state: appStateInterface) => state.user);

    let userFullName = {
        name: user.name,
        surname: user.surname,
        patronymic: user.patronymic,
    };
    let username = user.username;
    let phone = user.phone;
    let address = user.address;

    const resetUserHandler = () => {
        setFlashError({status: false,});
        setUser({...props.userData});
        setUserCompanies(props.userData.companies.map((company) => (
            company.id
        )));
        setUserGroups(props.userData.groups.map((group) => (
            group.id
        )));
        setUserStateStatus(true);
    };

    const saveUserHandler = () => {
        dispatch(AppAction.showLoader());
        let sendingData = convertUserDataForSending(user, userGroups, userCompanies);

        editUserRequest(user.id, sendingData).then(
            result => {
                if (result.type == 'success') {
                    history.push(`/${role}/user/${user.id}`);
                } else {
                    setFlashError({
                        status: true,
                        messageHeading: 'Ошибка.',
                        message: 'Пожалуйста, обратитесь к администратору'
                    });
                }
            }
        ).finally(() => {
            dispatch(AppAction.hideLoader());
        });
    };

    const deleteUserHandler = () => {
        dispatch(AppAction.showLoader());
        deleteUserRequest(user.id).then(
            result => {
                if (result.type == 'success') {
                    dispatch(AppAction.hideLoader());
                    history.push('/users');
                } else {
                    dispatch(AppAction.showLoader());
                    setFlashError({
                        status: true,
                        messageHeading: 'Ошибка.',
                        message: 'Не удалось удалить пользователя. Пожалуйста, обратитесь к администратору'
                    });
                }
            }
        );
    };

    //-------------------------------------
    // const handleFullNameChange = (e) => {
    //     let paramName = e.target.name;
    //     userFullName[paramName] = e.target.value;
    //     setUser((prevUser) => ({...prevUser, ...userFullName}));
    //     setUserStateStatus(false);
    // };

    const handleSurnameChange = (value) => {
        userFullName.surname = value;
        setUser((prevUser) => ({...prevUser, ...userFullName}));
        setUserStateStatus(false);
    }

    const handleNameChange = (value) => {
        userFullName.name = value;
        setUser((prevUser) => ({...prevUser, ...userFullName}));
        setUserStateStatus(false);
    }

    const handlePatronymicChange = (value) => {
        userFullName.patronymic = value;
        setUser((prevUser) => ({...prevUser, ...userFullName}));
        setUserStateStatus(false);
    }

    //-------------------------------------
    const handleNicknameChange = (value) => {
        setUser((prevUser) => ({...prevUser, username: value}));
        setUserStateStatus(false);
    };

    //-------------------------------------
    const handlePhoneChange = (value) => {
        setUser((prevUser) => ({...prevUser, phone: value}));
        setUserStateStatus(false);
    };

    //-------------------------------------
    const handleGroupChange = (e) => {
        let elem = e.target;
        if (elem.checked) {
            setUserGroups((prevUserGroups) => [...new Set([...prevUserGroups, elem.value])]);
        } else {
            setUserGroups(userGroups.filter(item => item !== elem.value));
        }
        setUserStateStatus(false);
    }

    //-------------------------------------
    const handleAddressChange = (value) => {
        setUser((prevUser) => ({...prevUser, address: value}));
        setUserStateStatus(false);
    };

    //-------------------------------------
    const handleCompanyChange = (e) => {
        let companies = e.target.value;
        setUserCompanies(companies);
        setUserStateStatus(false);
    };

    //-------------------------------------
    const handleWorkStatusChange = (status: number) => {
        setUser((prevUser) => ({...prevUser, work_status: status}));
        setUserStateStatus(false);
    };

    //-------------------------------------
    const handlePasswordSubmit = (e, pass) => {
        setUser((prevUser) => ({...prevUser, password: pass}));
        setUserStateStatus(false);
    };

    const onCloseSuccessFlash = () => {
        setFlashSuccess({status: false});
    }

    const onCloseErrorFlash = () => {
        setFlashError({status: false});
    }

    return (
        <>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                Редактирование пользователя
            </Grid>
            <Grid style={{marginBottom: '30px', display: 'flex', justifyContent: 'space-between',}}>
                <BackButtonComponent changed={!userStateStatus}/>
                {currentUser.roles.includes('ROLE_SUPER_ADMIN') &&
                    <SimpleModal
                        startButtonTitle={'Удалить пользователя'}
                        modalName={'deleteUserModal'}
                        // startIcon={<PersonAddDisabledOutlined/>}
                        startButtonVariant={'text'}
                        closeButtonTitle={'Отменить'}
                        modalTitle={'Удаление пользователя'}
                        acceptButtonTitle={'Подтвердить'}
                        onModalSubmit={deleteUserHandler}
                    >
                        Удалить пользователя?
                    </SimpleModal>
                }
            </Grid>
            <FlashComponent
                messageHeading={flashError.messageHeading}
                message={flashError.message}
                showFlash={flashError.status}
                // onCloseFlash={onCloseErrorFlash}
                flashType={'error'}
                flashWidth={'100%'}
            />
            <FlashComponent
                message={flashSuccess.message}
                showFlash={flashSuccess.status}
                onCloseFlash={onCloseSuccessFlash}
                flashType={'success'}
            />
            <Grid container style={{marginBottom: '30px',}}>
                <Grid item xs={3} style={{paddingRight: '15px',}}>
                    <SimpleTextInput
                        name={'surname'}
                        onChange={() => {}}
                        onBlur={handleSurnameChange}
                        label={'Фамилия'}
                        defaultValue={user.surname}
                    />
                </Grid>
                <Grid item xs={3} style={{paddingRight: '15px',}}>
                    <SimpleTextInput
                        name={'name'}
                        onChange={() => {}}
                        onBlur={handleNameChange}
                        label={'Имя'}
                        defaultValue={user.name}
                    />
                </Grid>
                <Grid item xs={3} style={{paddingRight: '15px',}}>
                    <SimpleTextInput
                        name={'patronymic'}
                        onChange={() => {}}
                        onBlur={handlePatronymicChange}
                        label={'Отчество'}
                        defaultValue={user.patronymic}
                    />
                </Grid>
            </Grid>
            <Box sx={{
                color: 'secondary.main',
                fontSize: '16px',
                fontWeight: '500',
                marginBottom: '30px',
            }}>
                Контактная информация
            </Box>
            <Grid container style={{marginBottom: '30px',}}>
                <Grid item xs={3} style={{paddingRight: '15px',}}>
                    <SimpleTextInput
                        name={'phone'}
                        onChange={() => {}}
                        onBlur={handlePhoneChange}
                        label={'Телефон'}
                        defaultValue={user.phone}
                        // inputStyle={{marginRight: '15px',}}
                    />
                </Grid>
                <Grid item xs={6} style={{paddingRight: '15px',}}>
                    <SimpleTextInput
                        name={'address'}
                        onChange={() => {}}
                        onBlur={handleAddressChange}
                        label={'Адрес'}
                        defaultValue={user.address}
                    />
                </Grid>
            </Grid>
            <Box sx={{
                color: 'secondary.main',
                fontSize: '16px',
                fontWeight: '500',
                marginBottom: '30px',
            }}>
                Рабочая информация
            </Box>
            <Grid container style={{marginBottom: '30px', alignItems: 'end',}}>
                <Grid item xs={3} style={{paddingRight: '15px',}}>
                    <SimpleTextInput
                        name={'username'}
                        onChange={() => {}}
                        onBlur={handleNicknameChange}
                        label={'Логин'}
                        defaultValue={user.username}
                    />
                </Grid>
                <Grid item xs={6} style={{paddingRight: '15px',}}>
                    <SimpleCheckboxFormGroup
                        items={changeGroupNames(props.userGroupData).map((group, index) => (
                            {value: group.id, label: group.group_name}
                        ))}
                        defaultValues={userGroups.map((group, index) => (
                            group
                        ))}
                        labelPlacement={'end'}
                        formGroupLabel={'Группы пользователей'}
                        onChange={handleGroupChange}
                        isRow={true}
                    />
                </Grid>
            </Grid>
            <Grid style={{marginBottom: '30px',}}>
                <SimpleSelectInput
                    label={'Статус'}
                    options={getStatuses()}
                    onChange={(value) => handleWorkStatusChange(value)}
                    defaultValue={user.work_status}
                />
            </Grid>
            <Grid style={{marginBottom: '30px',}}>
                <SimpleMultiSelectInput
                    options={props.companiesData.map(company => ({value: company.id, title: company.title}))}
                    defaultValues={userCompanies.map((company)=> company)}
                    labelId={'companiesLabel'}
                    labelTitle={'Доступ к отделениям'}
                    id={'companies'}
                    selectInputStyle={{width: '100%',}}
                    onChange={handleCompanyChange}
                />
            </Grid>
            <Box sx={{
                color: 'secondary.main',
                fontSize: '16px',
                fontWeight: '500',
                marginBottom: '30px',
            }}>
                Изменить пароль
            </Box>
            <Grid container style={{marginBottom: '30px',}}>
                <Grid item xs={3} style={{paddingRight: '15px',}}>
                    <ChangePasswordModal
                        onModalSubmit={handlePasswordSubmit}
                    />
                </Grid>
            </Grid>
            <Grid container style={{marginBottom: '30px',}}>
                <Grid item xs={3} style={{paddingRight: '15px',}}>
                    <Button
                        variant={'outlined'}
                        onClick={resetUserHandler}
                        fullWidth
                        className={'base_styled_btn'}
                    >
                        {componentParameters.resetButtonTitle}
                    </Button>
                </Grid>
                <Grid item xs={3} style={{paddingRight: '15px',}}>
                    <Button
                        variant={'contained'}
                        onClick={saveUserHandler}
                        fullWidth
                        className={'base_styled_btn'}
                        disabled={userStateStatus}
                    >
                        {componentParameters.submitButtonTitle}
                    </Button>
                </Grid>
            </Grid>
            <BackButtonComponent changed={!userStateStatus}/>
        </>
    );
}