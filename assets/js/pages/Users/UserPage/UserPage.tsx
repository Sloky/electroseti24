import React, {useEffect, useState} from "react";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import UserCard from "./UserCard";
import {useParams} from "react-router-dom";
import {getProfileProgressDataRequest} from "../../../requests/Request";
import {useDispatch} from "react-redux";
import {AppAction} from "../../../store/actions/actions";

export default function UserPage() {
    const [userData, setUserData] = useState(null);

    const dispatch = useDispatch();
    let params: any = useParams();
    let id = params.id;
    let role = params.role;

    const breadcrumbsItems = [
        {
            itemTitle: role === 'all' ? 'Пользователи' : role === 'admins' ? 'Руководители' : role === 'operators' ? 'Операторы' : 'Контроллеры',
            itemSrc: role === 'all' ? '/users-all' : `/users/${role}`,
        },
        {
            itemTitle: 'Карточка пользователя',
            // itemSrc: ``,
        },
    ]

    useEffect(() => {
        dispatch(AppAction.showLoader());
        getProfileProgressDataRequest(id).then(result => {
            if (result.type === 'success') {
                setUserData(result.data);
            }
        }).finally(() => {
            dispatch(AppAction.hideLoader());
        })
    }, [])

    return (
        <PageLayout
            breadcrumbsItems={breadcrumbsItems}
        >
            <UserCard
                userData={userData}
            />
        </PageLayout>
    )
}