import React, {useEffect, useState} from "react";
import {
    Box,
    Button,
    Card,
    Grid,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow
} from "@material-ui/core";
import {PersonOutline, ArrowUpward, ArrowDownward} from "@material-ui/icons";
import {THEME} from "../../../theme";
import {useHistory, useParams} from "react-router";
import {ProfileProgressData} from "../../../interfaces/interfaces";
import {AppAction} from "../../../store/actions/actions";
import {useDispatch} from "react-redux";
import {getProfileProgressExcelRequest} from "../../../requests/Request";
import {changeGroupNames} from "../../../functions/functions";

interface UserCardProps {
    userData: ProfileProgressData,
}

const listHeader = [
    'Месяц',
    'Не выполнено',
    'Нет доступа',
    'Выполнено',
    'Принято',
];

const monthes = [
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь',
]

export default function UserCard(props: UserCardProps) {
    const [user, setUser] = useState(props.userData);

    const history = useHistory();
    const dispatch = useDispatch();
    let params: any = useParams();
    let role = params.role;

    useEffect(() => {
        setUser(props.userData);
    }, [props])

    const showDelta = (delta) => {
        if (delta > 0) {
            return <><ArrowUpward sx={{color: 'success.main'}} /> <Box component={'span'} style={{fontSize: '14px', color: THEME.PLACEHOLDER_TEXT,}}>+{delta}</Box></>
        } else if (delta < 0) {
            return <><ArrowDownward sx={{color: 'error.main'}} /> <Box component={'span'} style={{fontSize: '14px', color: THEME.PLACEHOLDER_TEXT,}}>{delta}</Box></>
        } else {
            return ''
        }
    }

    const lastMonthData = () => {
        return user.statistic[Object.keys(user.statistic)[Object.keys(user.statistic).length - 1]]
    }

    const onUploadExcel = () => {
        dispatch(AppAction.showLoader());
        getProfileProgressExcelRequest(user.user.id)
            .then((result) => {
                if (result.type == 'success') {
                    let link = document.createElement('a');
                    link.download = `${user.user.surname} ${user.user.name} ${user.user.patronymic}.xlsx`;

                    let blob = new Blob(
                        [result.data],
                        {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'}
                    );

                    link.href = URL.createObjectURL(blob);

                    link.click();

                    URL.revokeObjectURL(link.href);
                } else {
                    console.log('Error', result.data);
                }
            })
            .finally(() => {
                dispatch(AppAction.hideLoader());
            });
    }

    return (
        <>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                Карточка пользователя
            </Grid>
            {user ?
                <Card sx={{fontSize: '18px', fontWeight: '500', bgcolor: 'background.default', padding: '20px', marginBottom: '30px',}}>
                    <Grid container>
                        <Grid item xs={11} sx={{color: 'text.primary'}}>
                            {user.user.surname + ' ' + user.user.name + ' ' + user.user.patronymic}
                        </Grid>
                        <Grid item xs={1}>
                            <PersonOutline sx={{color: 'secondary.main', fontSize: 70}}/>
                        </Grid>
                    </Grid>
                    {user && user.user.groups.filter(item => item.group_name === 'Controllers').length ?
                        <Grid container sx={{marginBottom: '15px',}}>
                            <Grid sx={{marginRight: '20px', color: 'text.primary',}}>Средний месячный показатель выполненных задач</Grid>
                            <Grid sx={{marginRight: '10px', color: 'text.primary',}}>{user.averageMonthlyExecutionRatio}</Grid>
                            <Grid>{showDelta(user.delta)}</Grid>
                        </Grid> : <></>
                    }
                    <Grid container style={{marginBottom: '15px',}}>
                        <Grid item xs={3} sx={{color: THEME.PLACEHOLDER_TEXT,}}>Группа пользователей</Grid>
                        <Grid item xs={9}>{changeGroupNames(user.user.groups).map(item => item.group_name).join(', ')}</Grid>
                    </Grid>
                    <Grid container style={{marginBottom: '15px',}}>
                        <Grid item xs={3} sx={{color: THEME.PLACEHOLDER_TEXT,}}>Доступ к отделениям</Grid>
                        <Grid item xs={9}>{user.user.companies.map(item => item.title).join(', ')}</Grid>
                    </Grid>
                    <Grid container style={{marginBottom: '15px',}}>
                        <Grid item xs={3} sx={{color: THEME.PLACEHOLDER_TEXT,}}>Адрес</Grid>
                        <Grid item xs={9}>{user.user.address}</Grid>
                    </Grid>
                    <Grid container style={{marginBottom: '30px',}}>
                        <Grid item xs={3} sx={{color: THEME.PLACEHOLDER_TEXT,}}>Телефон</Grid>
                        <Grid item xs={9}>{user.user.phone}</Grid>
                    </Grid>
                    <Grid container>
                        <Grid item xs={2}>
                            <Button
                                variant={'outlined'}
                                size={'large'}
                                onClick={() => history.push(`/${role}/user-edit/${user.user.id}`)}
                                className={'base_styled_btn'}
                                fullWidth
                            >
                                Редактировать пользователя
                            </Button>
                        </Grid>
                    </Grid>
                </Card> : <></>
            }
            {user && user.user.groups.filter(item => item.group_name === 'Controllers').length ?
                (
                    <>
                        <Grid sx={{fontSize: '18px', fontWeight: '500', marginBottom: '30px',}}>
                            <Grid container style={{marginBottom: '15px',}}>
                                <Grid item xs={2} sx={{color: THEME.PLACEHOLDER_TEXT,}}>Задач в работе</Grid>
                                <Grid item xs={4}>{user && Object.keys(user.statistic).length ? lastMonthData().taskToWorkCount : 'Нет данных'}</Grid>
                                <Grid item xs={2} sx={{color: THEME.PLACEHOLDER_TEXT,}}>Задач к проверке</Grid>
                                <Grid item xs={4}>{user && Object.keys(user.statistic).length ? lastMonthData().executedTaskCount - lastMonthData().acceptedTaskCount : 'Нет данных'}</Grid>
                            </Grid>
                            <Grid container>
                                <Grid item xs={2} sx={{color: THEME.PLACEHOLDER_TEXT,}}>Нет доступа</Grid>
                                <Grid item xs={4}>{user && Object.keys(user.statistic).length ? lastMonthData().unavailableTaskCount : 'Нет данных'}</Grid>
                                <Grid item xs={2} sx={{color: THEME.PLACEHOLDER_TEXT,}}>Принятых задач</Grid>
                                <Grid item xs={4}>{user && Object.keys(user.statistic).length ? lastMonthData().acceptedTaskCount : 'Нет данных'}</Grid>
                            </Grid>
                        </Grid>
                        <TableContainer sx={{bgcolor: 'background.default', marginBottom: '30px',}}>
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        {listHeader.map((value, index) => (
                                            <TableCell key={index} style={{color: THEME.PLACEHOLDER_TEXT}}>{value}</TableCell>
                                        ))}
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {user && Object.keys(user.statistic).length ? Object.keys(user.statistic).map((data, index) => (
                                        <TableRow key={index}>
                                            <TableCell sx={{fontSize: '15px', fontWeight: '500',}}>
                                                {monthes[new Date(data).getMonth()]}
                                            </TableCell>
                                            <TableCell sx={{fontSize: '15px', fontWeight: '500',}}>
                                                {user.statistic[data].taskToWorkCount}
                                            </TableCell>
                                            <TableCell sx={{fontSize: '15px', fontWeight: '500',}}>
                                                {user.statistic[data].unavailableTaskCount}
                                            </TableCell>
                                            <TableCell sx={{fontSize: '15px', fontWeight: '500',}}>
                                                {user.statistic[data].executedTaskCount}
                                            </TableCell>
                                            <TableCell sx={{fontSize: '15px', fontWeight: '500',}}>
                                                {user.statistic[data].acceptedTaskCount}
                                            </TableCell>
                                        </TableRow>
                                    )) : (
                                        <></>
                                    )}
                                </TableBody>
                            </Table>
                        </TableContainer>
                        {user && !Object.keys(user.statistic).length && (
                            <Box style={{padding: '70px 0px', textAlign: 'center', width: '100%', color: THEME.PLACEHOLDER_TEXT, fontSize: '16px',}}>
                                Нет данных
                            </Box>
                        )}
                        <Grid container>
                            <Grid item xs={2}>
                                <Button
                                    variant={'contained'}
                                    size={'large'}
                                    onClick={onUploadExcel}
                                    className={'base_styled_btn'}
                                    fullWidth
                                >
                                    Выгрузить в Excel
                                </Button>
                            </Grid>
                        </Grid>
                    </>
                ) : <></>
            }
        </>
    )
}