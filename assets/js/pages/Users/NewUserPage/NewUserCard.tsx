import React, {useEffect, useState} from "react";
import "./NewUserCard.css";
import SimpleSelectInput from "../../../components/FormInputs/SimpleSelectInput/SimpleSelectInput";
import {userStatuses} from "../UsersPage/UsersList";
import {addNewUserRequest, getNewUserCardRequest} from "../../../requests/Request";
import {CompanyData, SimpleFlashData, UserGroupData} from "../../../interfaces/interfaces";
import {useHistory} from "react-router";
import {Box, Button, Grid, TextField} from "@material-ui/core";
import FlashComponent from "../../../components/Flashes/FlashComponent";
import {Formik} from "formik";
import * as Yup from "yup";
import SimpleCheckboxFormGroup from "../../../components/FormInputs/SimpleCheckboxFormGroup/SimpleCheckboxFormGroup";
import {AppAction} from "../../../store/actions/actions";
import {useDispatch} from "react-redux";
import SimpleMultiSelectInput from "../../../components/FormInputs/MultiSelectInput/SimpleMultiSelectInput";
import {changeGroupNames} from "../../../functions/functions";

interface userFormInterface {
    surname: string
    name: string
    patronymic: string
    phone: string
    address: string
    username: string
    groups: string[]
    work_status: string
    companies: string[]
    password: string
    confirm_password: string
}

const getStatuses = () => {
    let statuses = [];
    for (let key in userStatuses) {
        statuses.push({
            value: key, title: userStatuses[key]
        });
    }
    return statuses;
};

export default function NewUserCard() {
    const [isLoading, setIsLoading] = useState(false);
    const [flashError, setFlashError] : [SimpleFlashData, any] = useState({status:false});
    const [flashSuccess, setFlashSuccess] : [SimpleFlashData, any] = useState({status: false});
    const [groups, setGroups] = useState<UserGroupData[]>([]);
    const [companies, setCompanies] = useState<CompanyData[]>([]);

    const history = useHistory();
    const dispatch = useDispatch();

    useEffect(() => {
        setIsLoading(true);
        dispatch(AppAction.showLoader());
        getNewUserCardRequest().then(
            result => {
                if (result.type == 'success') {
                    setGroups(result.data.groups);
                    setCompanies(result.data.companies.companies);
                } else {
                    setFlashError({
                        status: true,
                        message: 'Произошла ошибка при загрузке страницы. Пожалуйста, обратитесь к администратору'
                    });
                }
            }
        ).finally(() => {
            dispatch(AppAction.hideLoader());
            setIsLoading(false);
        });
    }, []);


    const handleSubmit = (values) => {
        dispatch(AppAction.showLoader());
        addNewUserRequest(values).then(
            result => {
                if (result.type == 'success') {
                    setFlashSuccess({
                        status:true,
                        message: 'Пользователь был успешно добавлен'
                    });
                    history.push('/users-all');
                } else {
                    if (result.data.message.match(/User with username=(\w)+ already exists/).length) {
                        setFlashError({
                            status: true,
                            message: `Пользователь с логином ${result.data.message.match(/User with username=(\w+) already exists/)[1]} уже существует.`
                        })
                    } else {
                        setFlashError({
                            status:true,
                            message: 'Произошла ошибка. Обратитесь к администратору'
                        });
                    }
                }
            }
        ).finally(() => {
            dispatch(AppAction.hideLoader());
        });
    }

    const onCloseSuccessFlash = () => {
        setFlashSuccess({status: false});
    }

    const onCloseErrorFlash = () => {
        setFlashError({status: false});
    }

    const initialValues: userFormInterface = {
        surname: '',
        name: '',
        patronymic: '',
        phone: '',
        address: '',
        username: '',
        groups: [],
        work_status: '1',
        companies: [],
        password: '',
        confirm_password: '',
    }

    const userSchema = Yup.object().shape({
        surname: Yup.string().required('Поле с фамилией не может быть пустым'),
        name: Yup.string().required('Поле с именем не может быть пустым'),
        patronymic: Yup.string().required('Поле с отчеством не может быть пустым'),
        phone: Yup.string().required('Поле с телефоном не может быть пустым'),
        address: Yup.string().required('Поле с адресом не может быть пустым'),
        username: Yup.string().required('Поле с логином не может быть пустым'),
        groups: Yup.array().required('Выберите группу').min(1),
        work_status: Yup.string().required('Поле со статусом не может быть пустым'),
        companies: Yup.array().required('Выберите компанию/отделение').min(1),
        password: Yup.string()
            .required('Поле с паролем не может быть пустым')
            .matches(/[a-zA-Z0-9]/, 'Пароль может содержать только буквы латинского алфавита и цифры'),
        confirm_password: Yup.string()
            .oneOf([Yup.ref('password'), null], 'Пароли не совпадают')
            .required('Пароли не совпадают'),
    })

    return (
        <>
            {!isLoading &&
                <>
                    <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                        Добавление пользователя
                    </Grid>
                    <FlashComponent
                        messageHeading={flashError.messageHeading}
                        message={flashError.message}
                        showFlash={flashError.status}
                        onCloseFlash={onCloseErrorFlash}
                        flashType={'error'}
                        flashWidth={'100%'}
                    />
                    <FlashComponent
                        message={flashSuccess.message}
                        showFlash={flashSuccess.status}
                        onCloseFlash={onCloseSuccessFlash}
                        flashType={'success'}
                    />
                    <Formik
                        initialValues={initialValues}
                        validationSchema={userSchema}
                        onSubmit={(values) => handleSubmit(values)}
                    >
                        {({
                              handleChange,
                              handleSubmit,
                              handleBlur,
                              dirty,
                              isValid,
                              errors,
                              values,
                              touched,
                              setFieldValue,
                        }) => (
                            <Box
                                component={'form'}
                            >
                                <Grid container style={{marginBottom: '30px',}}>
                                    <Grid item xs={3} style={{paddingRight: '15px',}}>
                                        <TextField
                                            fullWidth
                                            size={'small'}
                                            id={'surname'}
                                            placeholder={'Введите фамилию'}
                                            label={'Фамилия'}
                                            onChange={handleChange}
                                            error={errors.surname && touched.surname}
                                            helperText={touched.surname && errors.surname}
                                        />
                                    </Grid>
                                    <Grid item xs={3} style={{paddingRight: '15px',}}>
                                        <TextField
                                            fullWidth
                                            size={'small'}
                                            id={'name'}
                                            placeholder={'Введите имя'}
                                            label={'Имя'}
                                            onChange={handleChange}
                                            error={errors.name && touched.name}
                                            helperText={touched.name && errors.name}
                                        />
                                    </Grid>
                                    <Grid item xs={3} style={{paddingRight: '15px',}}>
                                        <TextField
                                            fullWidth
                                            size={'small'}
                                            id={'patronymic'}
                                            placeholder={'Введите отчество'}
                                            label={'Отчество'}
                                            onChange={handleChange}
                                            error={errors.patronymic && touched.patronymic}
                                            helperText={touched.patronymic && errors.patronymic}
                                        />
                                    </Grid>
                                </Grid>
                                <Box sx={{
                                    color: 'secondary.main',
                                    fontSize: '16px',
                                    fontWeight: '500',
                                    marginBottom: '30px',
                                }}>
                                    Контактная информация
                                </Box>
                                <Grid container style={{marginBottom: '30px',}}>
                                    <Grid item xs={3} style={{paddingRight: '15px',}}>
                                        <TextField
                                            fullWidth
                                            size={'small'}
                                            id={'phone'}
                                            placeholder={'Введите телефон'}
                                            label={'Телефон'}
                                            onChange={handleChange}
                                            error={errors.phone && touched.phone}
                                            helperText={touched.phone && errors.phone}
                                        />
                                    </Grid>
                                    <Grid item xs={6} style={{paddingRight: '15px',}}>
                                        <TextField
                                            fullWidth
                                            size={'small'}
                                            id={'address'}
                                            placeholder={'Введите адрес'}
                                            label={'Адрес'}
                                            onChange={handleChange}
                                            error={errors.address && touched.address}
                                            helperText={touched.address && errors.address}
                                        />
                                    </Grid>
                                </Grid>
                                <Box sx={{
                                    color: 'secondary.main',
                                    fontSize: '16px',
                                    fontWeight: '500',
                                    marginBottom: '30px',
                                }}>
                                    Рабочая информация
                                </Box>
                                <Grid container style={{marginBottom: '30px', alignItems: 'end',}}>
                                    <Grid item xs={3} style={{paddingRight: '15px',}}>
                                        <TextField
                                            fullWidth
                                            size={'small'}
                                            id={'username'}
                                            placeholder={'Введите логин'}
                                            label={'Логин'}
                                            onChange={handleChange}
                                            error={errors.username && touched.username}
                                            helperText={touched.username && errors.username}
                                        />
                                    </Grid>
                                    <Grid item xs={6} style={{paddingRight: '15px',}}>
                                        <SimpleCheckboxFormGroup
                                            name={'groups'}
                                            id={'groups'}
                                            labelPlacement={'end'}
                                            formGroupLabel={'Группы пользователей'}
                                            isRow={true}
                                            defaultValues={values.groups}
                                            // items={groups.length == 0 ? [] : groups.map((group, index) => ({
                                            //     label: group.group_name, value: group.id
                                            // }))}
                                            items={groups.length == 0 ? [] : changeGroupNames(groups).map(group => {
                                                return {
                                                    label: group.group_name,
                                                    value: group.id,
                                                }
                                            })}
                                            error={errors.groups && touched.groups}
                                            onChange={handleChange}
                                        />
                                    </Grid>
                                </Grid>
                                <Grid style={{marginBottom: '30px',}}>
                                    <SimpleSelectInput
                                        id={'work_status'}
                                        label={'Статус'}
                                        options={getStatuses()}
                                        defaultValue={initialValues.work_status}
                                        onChange={(value) => setFieldValue('work_status', value)}
                                    />
                                </Grid>
                                <Grid style={{marginBottom: '30px',}}>
                                    <SimpleMultiSelectInput
                                        id={'companies'}
                                        labelTitle={'Доступ к отделениям'}
                                        defaultValues={values.companies}
                                        options={companies.map(company => {
                                            return {
                                                title: company.title,
                                                value: company.id
                                            };
                                        })}
                                        selectInputStyle={{width: '100%',}}
                                        onChange={(e) => {
                                            setFieldValue('companies', e.target.value)
                                        }}
                                        error={errors.companies && touched.companies}
                                    />
                                </Grid>
                                <Grid container style={{marginBottom: '30px',}}>
                                    <Grid item xs={3} style={{paddingRight: '15px',}}>
                                        <TextField
                                            fullWidth
                                            size={'small'}
                                            type={'password'}
                                            id={'password'}
                                            name={'password'}
                                            label={'Пароль'}
                                            placeholder={'Введите пароль'}
                                            onChange={handleChange}
                                            defaultValue={initialValues.password}
                                            error={errors.password && touched.password}
                                            helperText={touched.password && errors.password}
                                        />
                                    </Grid>
                                    <Grid item xs={3} style={{paddingRight: '15px',}}>
                                        <TextField
                                            fullWidth
                                            size={'small'}
                                            type={'password'}
                                            id={'confirm_password'}
                                            name={'confirm_password'}
                                            label={'Повторите пароль'}
                                            placeholder={'Введите пароль повторно'}
                                            onChange={handleChange}
                                            defaultValue={initialValues.confirm_password}
                                            error={errors.confirm_password && touched.confirm_password}
                                            helperText={touched.confirm_password && errors.confirm_password}
                                        />
                                    </Grid>
                                </Grid>
                                <Grid container>
                                    <Grid item xs={3} style={{paddingRight: '15px',}}>
                                        <Button
                                            variant={'outlined'}
                                            fullWidth
                                            onClick={() => history.goBack()}
                                            className={'base_styled_btn'}
                                        >
                                            Отменить
                                        </Button>
                                    </Grid>
                                    <Grid item xs={3} style={{paddingRight: '15px',}}>
                                        <Button
                                            variant={'contained'}
                                            fullWidth
                                            onClick={() => handleSubmit()}
                                            className={'base_styled_btn'}
                                        >
                                            Создать пользователя
                                        </Button>
                                    </Grid>
                                </Grid>
                            </Box>
                        )}
                    </Formik>
                </>
            }
        </>
    );
}