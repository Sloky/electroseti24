import React from "react";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import NewUserCard from "./NewUserCard";

export default function NewUserPage() {
    return (
        <PageLayout>
            <NewUserCard/>
        </PageLayout>
    );
}