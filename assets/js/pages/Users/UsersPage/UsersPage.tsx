import React from "react";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import UsersList from "./UsersList";

export default function UsersPage() {
    return (
        <PageLayout>
            <UsersList/>
        </PageLayout>
    );
}