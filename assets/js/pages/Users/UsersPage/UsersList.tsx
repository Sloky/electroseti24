import React, {useEffect, useState} from "react";
import "./UserList.css";
import {SimpleFlashData, UserData} from "../../../interfaces/interfaces";
import {getCompaniesRequest, getUserGroupsRequest, getUsersRequest} from "../../../requests/Request";
import {useDispatch, useSelector} from "react-redux";
import {appStateInterface} from "../../../store/appState";
import {AppAction, EntitiesAction} from "../../../store/actions/actions";
import {getEntityCollection, changeGroupNames} from "../../../functions/functions";
import {
    Box,
    Button,
    Grid,
    Pagination,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
} from "@material-ui/core";
import {useHistory} from "react-router";
import FlashComponent from "../../../components/Flashes/FlashComponent";
import {THEME} from "../../../theme";
import TableItemsCustomizer from "../../Tasks/TasksTable/TableItemsCustomizer";
import SimpleSelectInput from "../../../components/FormInputs/SimpleSelectInput/SimpleSelectInput";
import SearchComponent from "../../../components/Other/SearchComponent/SearchComponent";

export interface UsersListProps {
    users?: UserData[]
    usersCount?: number
}

const listHeader = [
    'ФИО',
    'Группа',
    'Отделение',
    'Статус',
    'Количество задач в работе',
];

export const userStatuses = {
    1: 'В работе',
    2: 'Не в работе',
};

export default function UsersList(props: UsersListProps) {
    const [flashError, setFlashError] : [SimpleFlashData, any] = useState({status:false});
    const [usersData, setUsersData] = useState([]);

    const [itemsCount, setItemsCount] = useState<number>(100);
    const [pageNumber, setPageNumber] = useState<number>(1);
    const [totalItems, setTotalItems] = useState<number>(0);
    const [pagesCount, setPagesCount] = useState<number>(Math.ceil(totalItems/itemsCount));

    const [groupsData, setGroupsData] = useState([]);
    const [groupFilter, setGroupFilter] = useState(null);
    const [companiesData, setCompaniesData] = useState([]);
    const [companyFilter, setCompanyFilter] = useState(null);
    const [statusFilter, setStatusFilter] = useState(null);
    const [searchValue, setSearchValue] = useState('');

    const dispatch = useDispatch();
    const history = useHistory();

    const currentUser = useSelector((state: appStateInterface) => state.user);

    async function getUsers() {
        let usersRequest = await getUsersRequest({page: pageNumber, maxResult: itemsCount});
        if (usersRequest.type == 'success') {
            setUsersData(usersRequest.data.users);
            setTotalItems(usersRequest.data.usersCount)
            dispatch(EntitiesAction.changeUsersAction(getEntityCollection(usersRequest.data.users)));
        } else {
            console.log('Error users data: ', usersRequest.data);
            setFlashError({
                status:true,
                message: usersRequest.data.message
            });
        }
    }

    const getStatuses = () => {
        let statuses = [];
        for (let key in userStatuses) {
            statuses.push({
                value: key, title: userStatuses[key]
            });
        }
        return statuses;
    };

    useEffect(() => {
        dispatch(AppAction.showLoader());
        getCompaniesRequest({}).then(r => setCompaniesData(r.data.companies));
        getUserGroupsRequest().then(r => setGroupsData(r.data));
        getUsers().then(r => {
            dispatch(AppAction.hideLoader());
        });
    }, []);

    useEffect(() => {
        if (pageNumber > 0) {
            dispatch(AppAction.showLoader());
            getUsersRequest({
                page: pageNumber,
                maxResult: itemsCount,
                search: searchValue,
                company: companyFilter,
                group: groupFilter,
                status: statusFilter,
            }).then(result => {
                setUsersData(result.data.users)
                dispatch(AppAction.hideLoader());
            })
        }
    }, [pageNumber, itemsCount])

    useEffect(() => {
        setPagesCount(Math.ceil(totalItems/itemsCount || 1));
    }, [itemsCount, totalItems]);

    useEffect(() => {
        if (pageNumber > pagesCount) {
            setPageNumber(pagesCount ? pagesCount : 1);
        }
    }, [pagesCount]);

    const onPageNumberChange = (value: number) => {
        setPageNumber(value);
    };

    const onSearchValueChange = (value: string) => {
        setSearchValue(value);
    };

    const onTableItemCountChange = (value: number) => {
        setItemsCount(value);
    };

    const applyFilters = () => {
        dispatch(AppAction.showLoader());
        getUsersRequest({
            page: pageNumber,
            maxResult: itemsCount,
            search: searchValue,
            company: companyFilter,
            group: groupFilter,
            status: statusFilter,
        }).then(result => {
            setTotalItems(result.data.usersCount);
            setUsersData(result.data.users);
            dispatch(AppAction.hideLoader());
        })
    }

    const closeFlash = () => {
        setFlashError(prevState => ({...prevState, status: false}))
    }

    const companiesToString = (companies: {title: string}[]) => {
        let stringOfCompanies = companies.map((company, index) => {
            return company.title
        })
        return stringOfCompanies.join(', ');
    }

    const onFilterClear = () => {
        setGroupFilter(null);
        setCompanyFilter(null);
        setStatusFilter(null);
        setSearchValue('');
        dispatch(AppAction.showLoader());
        getUsersRequest({
            page: pageNumber,
            maxResult: itemsCount,
            search: '',
            company: null,
            group: null,
            status: null,
        }).then(result => {
            setTotalItems(result.data.usersCount);
            setUsersData(result.data.users);
            dispatch(AppAction.hideLoader());
        })
    }

    const checkRole = (groups, role) => {
        return groups.find(group => group.group_name === role);
    }

    return (
        <>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                Пользователи
            </Grid>
            {currentUser.roles.includes('ROLE_SUPER_ADMIN') &&
                <Grid container style={{marginBottom: '30px',}}>
                    <Grid item xs={3} style={{paddingRight: '15px',}}>
                        <Button
                            onClick={() => history.push('/user/new')}
                            variant={'contained'}
                            fullWidth
                            size={'large'}
                            style={{fontSize: '12px', width: '206px', height: '43px',}}
                        >
                            Добавить пользователя
                        </Button>
                    </Grid>
                </Grid>
            }
            <FlashComponent
                message={flashError.message}
                showFlash={flashError.status}
                messageHeading={flashError.messageHeading}
                flashType={'error'}
                // onCloseFlash={closeFlash}
                flashWidth={'100%'}
            />
            <Grid container>
                <Grid item xs={2} style={{marginBottom: '30px', paddingRight: '15px',}}>
                    <SimpleSelectInput
                        label={'Отделение'}
                        allowEmptyValue={true}
                        emptyValueTitle={'Все отделения'}
                        defaultValue={companyFilter}
                        options={companiesData.map(company => {
                            return {
                                title: company.title,
                                value: company.id
                            }
                        })}
                        formControlStyle={{width: '100%',}}
                        onChange={value => {
                            setCompanyFilter(value)
                        }}
                    />
                </Grid>
                <Grid item xs={2} style={{marginBottom: '30px', paddingRight: '15px',}}>
                    <SimpleSelectInput
                        label={'Группа'}
                        allowEmptyValue={true}
                        emptyValueTitle={'Все группы'}
                        defaultValue={groupFilter}
                        options={changeGroupNames(groupsData).map(group => {
                            return {
                                title: group.group_name,
                                value: group.id,
                            }
                        })}
                        formControlStyle={{width: '100%',}}
                        onChange={value => {
                            setGroupFilter(value)
                        }}
                    />
                </Grid>
                <Grid item xs={2} style={{marginBottom: '30px', paddingRight: '15px',}}>
                    <SimpleSelectInput
                        label={'Статус'}
                        allowEmptyValue={true}
                        emptyValueTitle={'Все статусы'}
                        defaultValue={statusFilter}
                        options={getStatuses()}
                        formControlStyle={{width: '100%',}}
                        onChange={(value) => setStatusFilter(value)}
                    />
                </Grid>
            </Grid>
            <Grid container style={{marginBottom: '30px',}}>
                <Grid item xs={6}>
                    <SearchComponent
                        searchValue={searchValue}
                        onSearchValueChange={onSearchValueChange}
                        onFilterClear={onFilterClear}
                        applyFilters={applyFilters}
                        enableFilters={true}
                        searchFieldXs={6}
                        buttonXs={3}
                    />
                </Grid>
            </Grid>
            <Grid style={{marginBottom: '20px',}}>
                <TableItemsCustomizer
                    initialCount={itemsCount}
                    onTableItemCountChange={onTableItemCountChange}
                    allItems={totalItems}
                />
            </Grid>
            <TableContainer sx={{bgcolor: 'background.default'}}>
                <Table>
                    <TableHead>
                        <TableRow>
                            {listHeader.map((value, index) => (
                                <TableCell key={index}>{value}</TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {Array.isArray(usersData) && usersData.length ? usersData.map((user, index) => (
                            <TableRow hover key={index} style={{cursor: 'pointer',}} onClick={() => history.push(`/all/user/${user.id}`)}>
                                <TableCell style={{fontSize: '15px',}}>
                                    {user.surname + ' ' + user.name + ' ' + user.patronymic}
                                </TableCell>
                                <TableCell style={{fontSize: '15px',}}>
                                    {changeGroupNames(user.groups).map((group, index) => {
                                        return user.groups.length == 1+index ? group.group_name: group.group_name+', '
                                    })}
                                </TableCell>
                                <TableCell style={{fontSize: '15px',}}>{companiesToString(user.companies)}</TableCell>
                                <TableCell style={{fontSize: '15px', minWidth: '132px',}}>
                                    <Box component="span" sx={user.work_status === 1
                                        ? ({backgroundColor: 'success.light', color: 'success.main', borderRadius: '12px', padding: '2px 8px',})
                                        : ({backgroundColor: 'warning.light', color: 'warning.dark', borderRadius: '12px', padding: '2px 8px',})}
                                    >
                                        {userStatuses[user.work_status]}
                                    </Box>
                                </TableCell>
                                <TableCell style={{fontSize: '15px',}}>
                                    {checkRole(user.groups, 'Controllers') ? user.task_to_work_count : checkRole(user.groups, 'Operators') ? user.company_task_to_check_count : 'Нет данных'}
                                </TableCell>
                            </TableRow>
                        )) : (
                            <></>
                        )}
                    </TableBody>
                </Table>
            </TableContainer>
            {(!usersData || !usersData.length) && (
                <Box style={{padding: '70px 0px', textAlign: 'center', width: '100%', color: THEME.PLACEHOLDER_TEXT, fontSize: '16px',}}>
                    Нет данных
                </Box>
            )}
            <Stack spacing={2} style={{marginTop: '30px',}}>
                <Pagination count={pagesCount} variant="outlined" shape="rounded" color="primary" onChange={(event, page) => onPageNumberChange(page)}/>
            </Stack>
        </>
    );
}