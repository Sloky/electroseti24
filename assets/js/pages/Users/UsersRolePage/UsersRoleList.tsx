import React from "react";
import {Grid} from "@material-ui/core";
import {UserData} from "../../../interfaces/interfaces";
import AdminsTable from "../../../components/Tables/AdminsTable";
import OperatorsTable from "../../../components/Tables/OperatorsTable";
import ControllersTable from "../../../components/Tables/ControllersTable";
import {useParams} from "react-router";

interface UsersRoleListProps {
    users: UserData[]
}

export default function UsersRoleList(props: UsersRoleListProps) {
    let params: any = useParams();
    let role = params.role;

    return (
        <>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                {role === 'admins' ? 'Руководители' : role === 'operators' ? 'Операторы' : 'Контроллеры'}
            </Grid>
            {role === 'admins' &&
                <AdminsTable
                    users={props.users}
                />
            }
            {role === 'operators' &&
                <OperatorsTable
                    users={props.users}
                />
            }
            {role === 'controllers' &&
                <ControllersTable
                    users={props.users}
                />
            }
        </>
    )
}