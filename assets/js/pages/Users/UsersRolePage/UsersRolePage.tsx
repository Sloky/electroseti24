import React, {useEffect, useState} from "react";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import UsersRoleList from "./UsersRoleList";
import {useParams} from "react-router";
import {useDispatch} from "react-redux";
import {AppAction} from "../../../store/actions/actions";
import {getUserGroupsRequest, getUsersRequest} from "../../../requests/Request";

export default function UsersRolePage() {
    const [usersData, setUsersData] = useState([]);
    const [groupsData, setGroupsData] = useState(null);

    const [groupsDataLoaded, setGroupsDataLoaded] = useState(false);

    const dispatch = useDispatch();

    let params: any = useParams();
    let role = params.role;

    const getCurrentGroupId = (role) => {
        let id = '';
        groupsData.map(group => {
            if (group['group_name'].toLowerCase() === role) {
                id = group.id
            }
        })
        return id;
    }

    useEffect(() => {
        dispatch(AppAction.showLoader());
        getUserGroupsRequest().then(result => {
            if (result.type === 'success') {
                setGroupsData(result.data);
            }
        }).finally(() => {
            setGroupsDataLoaded(true);
        })
    }, []);

    useEffect(() => {
        dispatch(AppAction.showLoader());
        if (groupsDataLoaded) {
            getUsersRequest({
                page: 1,
                maxResult: 1000,
                group: getCurrentGroupId(role),
            }).then(result => {
                if (result.type === 'success') {
                    setUsersData(result.data.users);
                }
            }).finally(() => {
                dispatch(AppAction.hideLoader());
            })
        }
    }, [groupsDataLoaded, role]);

    return (
        <PageLayout>
            <UsersRoleList
                users={usersData}
            />
        </PageLayout>
    )
}