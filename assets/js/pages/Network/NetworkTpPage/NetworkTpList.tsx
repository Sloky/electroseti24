import React, {useEffect, useState} from "react";
import {
    CompanyBranch,
    CompanyTransformerTableRequestData,
    TransformersData,
    UserData
} from "../../../interfaces/interfaces";
import {
    Box,
    Button,
    Grid, Pagination,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow
} from "@material-ui/core";
import {useHistory} from "react-router";
import SimpleSelectInput from "../../../components/FormInputs/SimpleSelectInput/SimpleSelectInput";
import SearchComponent from "../../../components/Other/SearchComponent/SearchComponent";
import TableItemsCustomizer from "../../Tasks/TasksTable/TableItemsCustomizer";
import {THEME} from "../../../theme";

interface NetworkTpListProps {
    transformers: TransformersData[]
    companies: CompanyBranch[]
    controllers: UserData[]
    transformersCount: number
    getTransformers(requestData: CompanyTransformerTableRequestData): void
}

const listHeader = [
    'Трансформатор',
    'Отделение',
    'Узлы учёта',
    'Контроллер',
];

export default function NetworkTpList(props: NetworkTpListProps) {
    const [transformers, setTransformers] = useState(props.transformers);
    const [controllers, setControllers] = useState(props.controllers);

    const [companyFilter, setCompanyFilter] = useState(null);
    const [executorFilter, setExecutorFilter] = useState(null);
    const [searchValue, setSearchValue] = useState('');

    const [itemsCount, setItemsCount] = useState(100);
    const [totalItems, setTotalItems] = useState(props.transformersCount);
    const [pagesCount, setPagesCount] = useState<number>(Math.ceil(totalItems/itemsCount || 1));
    const [pageNumber, setPageNumber] = useState<number>(1);

    const history = useHistory();

    const applyFilters = () => {
        let requestData = {
            page: pageNumber,
            maxResult: itemsCount,
            search: searchValue,
            company: companyFilter,
            executor: executorFilter,
        }
        if (executorFilter !== 'empty' && executorFilter !== null) {
            const currentExecutor = props.controllers.filter(controller => {
                if (executorFilter === `${controller.surname} ${controller.name} ${controller.patronymic}`) {
                    return controller
                }
            })
            requestData = {
                page: pageNumber,
                maxResult: itemsCount,
                search: searchValue,
                company: companyFilter,
                executor: currentExecutor[0].id,
            }
        }
        props.getTransformers(requestData);
    }

    const controllersForSelect = () => {
        let newControllers = controllers.map(controller => {
            let userFullName = controller.surname+' '+controller.name+' '+controller.patronymic;
            return {
                title: userFullName,
                value: userFullName,
            }
        })
        newControllers.unshift({title: 'Без исполнителя', value: 'empty'});
        return newControllers
    }

    const onSearchValueChange = (value: string) => {
        setSearchValue(value);
    };

    const onTableItemCountChange = (value: number) => {
        setItemsCount(value);
    };

    const onPageNumberChange = (value: number) => {
        setPageNumber(value);
    };

    const onFilterClear = () => {
        setCompanyFilter(null);
        setExecutorFilter(null);
        setSearchValue('');
        props.getTransformers({
            page: pageNumber,
            maxResult: itemsCount,
        });
    }

    useEffect(() => {
        setTransformers(props.transformers);
        setTotalItems(props.transformersCount);
        setControllers(props.controllers);
    }, [props]);

    useEffect(() => {
        const requestData = {
            page: pageNumber,
            maxResult: itemsCount,
            search: searchValue,
            company: companyFilter,
            executor: executorFilter,
        }
        props.getTransformers(requestData);
    }, [pageNumber, itemsCount]);

    useEffect(() => {
        setPagesCount(Math.ceil(totalItems/itemsCount || 1));
    }, [itemsCount, totalItems]);

    useEffect(() => {
        if (pageNumber > pagesCount) {
            setPageNumber(pagesCount);
        }
    }, [pagesCount]);

    return (
        <>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                Объекты сети
            </Grid>
            <Grid container style={{marginBottom: '30px',}}>
                <Grid item xs={2}>
                    <Button
                        variant={'contained'}
                        size={'large'}
                        onClick={() => history.push('/tp-new/')}
                        className={'base_styled_btn'}
                        fullWidth
                    >
                        Добавить ТП
                    </Button>
                </Grid>
            </Grid>
            <Grid container style={{marginBottom: '30px',}}>
                <Grid item xs={4} style={{paddingRight: '15px',}}>
                    <SimpleSelectInput
                        label={'Отделение'}
                        allowEmptyValue={true}
                        emptyValueTitle={'Все отделения'}
                        defaultValue={companyFilter}
                        options={props.companies.map(company => {
                            return {
                                title: company.title,
                                value: company.id
                            }
                        })}
                        formControlStyle={{width: '100%',}}
                        onChange={value => {setCompanyFilter(value)}}
                    />
                </Grid>
                <Grid item xs={4} style={{paddingRight: '15px',}}>
                    <SimpleSelectInput
                        label={'Исполнитель'}
                        allowEmptyValue={true}
                        emptyValueTitle={'Все исполнители'}
                        defaultValue={executorFilter}
                        options={controllersForSelect()}
                        formControlStyle={{width: '100%',}}
                        onChange={value => {setExecutorFilter(value)}}
                    />
                </Grid>
            </Grid>
            <Grid container style={{marginBottom: '30px',}}>
                <Grid item xs={8}>
                    <SearchComponent
                        searchValue={searchValue}
                        onSearchValueChange={onSearchValueChange}
                        applyFilters={applyFilters}
                        searchFieldXs={6}
                        buttonXs={3}
                        enableFilters={true}
                        onFilterClear={onFilterClear}
                    />
                </Grid>
            </Grid>
            <Grid style={{marginBottom: '20px',}}>
                <TableItemsCustomizer
                    initialCount={itemsCount}
                    allItems={totalItems}
                    onTableItemCountChange={onTableItemCountChange}
                />
            </Grid>
            <TableContainer sx={{bgcolor: 'background.default', marginBottom: '30px',}}>
                <Table>
                    <TableHead>
                        <TableRow>
                            {listHeader.map((value, index) => (
                                <TableCell key={index} style={{color: THEME.PLACEHOLDER_TEXT}}>{value}</TableCell>
                                ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {transformers && transformers.length ? transformers.map((data, index) => (
                            <TableRow hover style={{cursor: 'pointer',}} key={index} onClick={() => history.push(`/tp-page/${data.id}`)}>
                                <TableCell sx={{fontSize: '15px', fontWeight: '500',}}>
                                    {data.title}
                                </TableCell>
                                <TableCell sx={{fontSize: '15px', fontWeight: '500',}}>
                                    {data.company.title}
                                </TableCell>
                                <TableCell sx={{fontSize: '15px', fontWeight: '500',}}>
                                    {data.counters_count}
                                </TableCell>
                                <TableCell sx={{fontSize: '15px', fontWeight: '500',}}>
                                    {data.users && data.users.length ? data.users.map((user, index) => {
                                        if (index === data.users.length - 1) {
                                            return `${user.surname} ${user.name} ${user.patronymic}`
                                        } else {
                                            return `${user.surname} ${user.name} ${user.patronymic}, `
                                        }
                                    }) : 'Нет исполнителя'}
                                </TableCell>
                            </TableRow>
                        )) : (
                            <></>
                        )}
                    </TableBody>
                </Table>
            </TableContainer>
            {transformers && !transformers.length && (
                <Box style={{padding: '70px 0px', textAlign: 'center', width: '100%', color: THEME.PLACEHOLDER_TEXT, fontSize: '16px',}}>
                    Нет данных
                </Box>
            )}
            <Stack spacing={2} style={{marginTop: '30px'}}>
                <Pagination
                    count={pagesCount}
                    variant={'outlined'}
                    shape={'rounded'}
                    color={'primary'}
                    onChange={(event, page) => onPageNumberChange(page)}
                />
            </Stack>
        </>
    )
}