import React, {useEffect, useState} from "react";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import NetworkTpList from "./NetworkTpList";
import {useDispatch} from "react-redux";
import {AppAction} from "../../../store/actions/actions";
import {
    getCompaniesForFilterRequest,
    getCompaniesRequest,
    getControllersRequest,
    getTransformersDataRequest
} from "../../../requests/Request";
import {CompanyData, UserData} from "../../../interfaces/interfaces";

export default function NetworkTpPage() {
    const [transformers, setTransformers] = useState([]);
    const [transformersCount, setTransformersCount] = useState(0);
    const [companies, setCompanies] = useState([]);
    const [controllers, setControllers] = useState([]);

    const [transformersLoaded, setTransformersLoaded] = useState(false);
    const [companiesLoaded, setCompaniesLoaded] = useState(false);
    const [controllersLoaded, setControllersLoaded] = useState(false);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(AppAction.showLoader());
        getTransformersDataRequest({page: 1, maxResult: 100,}).then(result => {
            if (result.type === 'success') {
                setTransformers(result.data.transformers);
                setTransformersCount(result.data.transformersCount);
            }
        }).finally(() => {
            setTransformersLoaded(true);
        })
        getCompaniesForFilterRequest().then(result => {
            if (result.type === 'success') {
                setCompanies(result.data);
            }
        }).finally(() => {
            setCompaniesLoaded(true);
        })
        getControllersRequest().then(result => {
            if (result.type === 'success') {
                setControllers(result.data);
            }
        }).finally(() => {
            setControllersLoaded(true);
        })
    }, []);

    useEffect(() => {
        if (companiesLoaded && controllersLoaded && transformersLoaded) {
            dispatch(AppAction.hideLoader());
        }
    }, [companiesLoaded, controllersLoaded, transformersLoaded]);

    const getTransformers = (requestData) => {
        dispatch(AppAction.showLoader());
        getTransformersDataRequest(requestData).then(result => {
            if (result.type === 'success') {
                setTransformers(result.data.transformers);
                setTransformersCount(result.data.transformersCount);
            }
        }).finally(() => {
            dispatch(AppAction.hideLoader());
        })
    };

    return (
        <PageLayout>
            <NetworkTpList
                transformers={transformers}
                companies={companies}
                controllers={controllers}
                transformersCount={transformersCount}
                getTransformers={getTransformers}
            />
        </PageLayout>
    )
}