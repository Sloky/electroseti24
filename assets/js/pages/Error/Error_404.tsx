import React from "react";
import PageLayout from "../../components/Layouts/PageLayout/PageLayout";
import {Box, Typography} from "@material-ui/core";
import "./Error_404.css";

export default function Error_404() {
    return (
        <PageLayout>
            <Box className="error_container" sx={{backgroundColor: 'background.paper',}}>
                <Typography component="h2" className="error_title">
                    Ошибка <Typography component="span" className="text-gradient error_title">404</Typography>
                </Typography>
                <Typography component="span" className="error_subtitle">Такая страница не существует!</Typography>
            </Box>
        </PageLayout>
    );
}
