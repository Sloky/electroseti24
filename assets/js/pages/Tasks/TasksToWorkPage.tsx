import React, {useEffect, useState} from "react";
import PageLayout from "../../components/Layouts/PageLayout/PageLayout";
import TasksToWorkTable from "./TasksTable/TasksToWorkTable";
import {
    deleteTasksToWorkRequest,
    getBadgesCount,
    getCompaniesForFilterRequest,
    getControllersForTasksRequest,
    getPeriodsRequest,
    getTasksToWorkRequest,
    synchronizeMod1DataRequest,
    uploadTaskListRequest
} from "../../requests/Request";
import {useDispatch, useSelector} from "react-redux";
import {CompanyBranch, TaskData, TaskTableRequestData, UserData} from "../../interfaces/interfaces";
import {getEntityCollection, getTasksToWorkCollection, getTaskToWorkTableData} from "../../functions/functions";
import {AppAction, EntitiesAction, TasksAction} from "../../store/actions/actions";
import {Grid} from "@material-ui/core";
import FlashComponent from "../../components/Flashes/FlashComponent";
import {useHistory} from "react-router";
import {appStateInterface} from "../../store/appState";

interface tasksToWorkPageInterface {
    title: string
}

const defaultTasksToWorkPage: tasksToWorkPageInterface = {
    title: 'Задачи к выполнению',
}

export default function TasksToWorkPage(props) {
    const [flashError, setFlashError] = useState({status: false, message: undefined, messages: []});
    const [flashSuccess, setFlashSuccess] = useState({status: false, message: ''});
    const [flashInfo, setFlashInfo] = useState({status: false, message: ''});
    const [flashWarning, setFlashWarning] = useState({status: false, message: ''});
    const [tasksLoaded, setTasksLoaded] = useState(false);
    const [controllersLoaded, setControllersLoaded] = useState(false);
    const [branchesLoaded, setBranchesLoaded] = useState(false);
    const [periodsLoaded, setPeriodsLoaded] = useState(false);
    const [taskTableData, setTaskTableData] = useState<{
        tasks: TaskData[]|[]
        controllers: UserData[]|[]
        branches: CompanyBranch[]|[]
        periods: []
        tasksCount: number
    }>({tasks: [], controllers: [], branches: [], periods: [], tasksCount: 0});

    const dispatch = useDispatch();
    const history = useHistory();
    const filtersData = useSelector((state: appStateInterface) => state.filtersData);

    const onCloseErrorFlash = () => {
        setFlashError(prevState => {return {...prevState, status: false}})
    };

    const onCloseSuccessFlash = () => {
        setFlashSuccess(prevState => {return {...prevState, status: false}})
    };

    const onCloseInfoFlash = () => {
        setFlashInfo(prevState => {return {...prevState, status: false}})
    };

    const onCloseWarningFlash = () => {
        setFlashWarning(prevState => {return {...prevState, status: false}})
    };

    const onTaskListLoad = (file) => {
        dispatch(AppAction.showLoader());
        setTasksLoaded(false);
        setFlashError({messages: [], message: undefined, status: false});
        setFlashSuccess({message: undefined, status: false});
        setFlashWarning({message: undefined, status: false});
        setFlashInfo({message: undefined, status: false});
        uploadTaskListRequest(file).then(
            result => {
                if (result.type == 'success') {
                    setTaskTableData( prevState =>
                        {
                            return {
                                ...prevState,
                                tasks: result.data.tasks,
                                branches: result.data.branches,
                                periods: result.data.periods,
                                controllers: result.data.controllers,
                                tasksCount: result.data.tasksCount,
                            }
                        }
                    );
                    if (result.data.rowErrors.length !== 0) {
                        setFlashError({
                            status: true,
                            message: undefined,
                            messages: result.data.rowErrors
                        });
                    } else {
                        setFlashSuccess({
                            status: true,
                            message: 'Список задач был успешно загружен'
                        });
                    }
                    dispatch(TasksAction.changeTasksToWorkAction(getTasksToWorkCollection(result.data.tasks)));
                    dispatch(EntitiesAction.changeCompanyBranchesAction(getEntityCollection(result.data.branches)));
                } else {
                    console.log('Error: ', result.data);
                    setFlashError({
                        messages: [],
                        status: true,
                        message: result.data.message
                    });
                }
            }
        ).finally(() => {
            setTasksLoaded(true);
            dispatch(AppAction.hideLoader());
        });
    };

    const onSynchronizeData = () => {
        dispatch(AppAction.showLoader());
        setTasksLoaded(false);
        synchronizeMod1DataRequest().then(
            result => {
                if (result.type == 'success') {
                    setFlashSuccess({
                        status: true,
                        message: 'Данные были успешно обновлены'
                    })
                    setTaskTableData( prevState =>
                        {
                            return {
                                ...prevState,
                                tasks: result.data.tasks,
                                branches: result.data.branches
                            }
                        }
                    );
                    dispatch(TasksAction.changeTasksToWorkAction(getTasksToWorkCollection(result.data.tasks)));
                    dispatch(EntitiesAction.changeCompanyBranchesAction(getEntityCollection(result.data.branches)));
                } else {
                    console.log('Error: ', result.data);
                    setFlashError({
                        messages: [],
                        status:true,
                        message: result.data.message
                    });
                }
            }
        ).finally(() => {
            setTasksLoaded(true);
            dispatch(AppAction.hideLoader());
        })
    };

    const onTasksToWorkDelete = async (data: TaskTableRequestData) => {
        dispatch(AppAction.showLoader());
        setTasksLoaded(false);
        await deleteTasksToWorkRequest(data).then(
            result => {
                if (result.type == 'success') {
                    setFlashSuccess({
                        status: true,
                        message: 'Задачи к выполнению были успешно удалены'
                    })
                    setTaskTableData( prevState =>
                        {
                            return {
                                ...prevState,
                                tasks: [],
                                tasksCount: 0,
                            }
                        }
                    );
                    dispatch(TasksAction.changeTasksToWorkAction(getTasksToWorkCollection(result.data.tasks)));
                } else {
                    console.log('Error: ', result.data);
                    setFlashError({
                        messages: [],
                        status:true,
                        message: result.data.message
                    });
                }
            }
        ).finally(() => {
            setTasksLoaded(true);
            dispatch(AppAction.hideLoader());
        })
    };


    useEffect(() => {
        dispatch(AppAction.showLoader());
        setTasksLoaded(false);
        const requestData = filtersData?.filtersLocation !== history.location.pathname ? {page: 1, maxResult: 100} : {
            page: 1,
            maxResult: 100,
            searchField: filtersData.searchField,
            search: filtersData.searchValue,
            company: filtersData.companyFilter,
            user: filtersData.executorFilter,
            subscriberType: filtersData.subscriberTypeFilter,
            period: filtersData.periodFilter,
        }
        getTasksToWorkRequest(requestData).then(
            result => {
                if (result.type == 'success') {
                    setTaskTableData( prevState =>
                        {
                            return {
                                ...prevState,
                                tasks: result.data.tasks,
                                tasksCount: result.data.tasksCount,
                            }
                        }
                    );
                    dispatch(TasksAction.changeTasksToWorkAction(getTasksToWorkCollection(result.data.tasks)));
                } else {
                    console.log('Error: ', result.data);
                    if (result.code == 404) {
                        setTaskTableData( prevState =>
                            {
                                return {
                                    ...prevState,
                                    tasks: []
                                }
                            }
                        );
                        setFlashInfo({
                            status: true,
                            message: 'Нет доступных к исполнению задач'
                        })
                    } else {
                        setFlashError({
                            messages: [],
                            status: true,
                            message: result.data
                        });
                    }
                }
            }
        ).finally(() => {
            setTasksLoaded(true);
        });
        getControllersForTasksRequest().then(
            result => {
                if (result.type == 'success') {
                    setTaskTableData( prevState =>
                        {
                            return {
                                ...prevState,
                                controllers: result.data,
                            }
                        }
                    );
                    dispatch(EntitiesAction.changeUsersAction(getEntityCollection(result.data)));
                } else {
                    console.log('Error: ', result);
                    if (result.code == 404) {
                        setFlashInfo({
                            status:true,
                            message: 'Нет доступных исполнителей. Для возможности сортировки по ' +
                                'исполнителям добавьте их в меню "Пользователи"'
                        })
                    } else {
                        setFlashError({
                            messages: [],
                            status:true,
                            message: result.data
                        });
                    }
                }
            }
        ).finally(() => {
            setControllersLoaded(true);
        });
        getCompaniesForFilterRequest().then(
            result => {
                if (result.type == 'success') {
                    setTaskTableData( prevState =>
                        {
                            return {
                                ...prevState,
                                branches: result.data
                            }
                        }
                    );
                    dispatch(EntitiesAction.changeCompanyBranchesAction(getEntityCollection(result.data)));
                    if (result.data.length === 0) {
                        setFlashError({
                            messages: [],
                            status:true,
                            message: 'Нет доступных отделений. Обратитесь к администратору'
                        })
                    }
                } else {
                    console.log('Error: ', result.data);
                    setFlashError({
                        messages: [],
                        status:true,
                        message: result.data
                    });
                }
            }
        ).finally(() => {
            setBranchesLoaded(true);
        });
        getPeriodsRequest('0').then(
            result => {
                if (result.type == 'success') {
                    setTaskTableData( prevState =>
                        {
                            return {
                                ...prevState,
                                periods: result.data
                            }
                        }
                    );
                } else {
                    console.log('Error: ', result.data);
                }
            }
        ).finally(() => {
            setPeriodsLoaded(true);
        });
    }, []);

    useEffect(() => {
        if (tasksLoaded && controllersLoaded && branchesLoaded && periodsLoaded) {
            dispatch(AppAction.hideLoader());
        }
    }, [tasksLoaded, controllersLoaded, branchesLoaded, periodsLoaded]);

    useEffect(() => {
        getBadgesCount().then(
            result => {
                if (result.type == 'success') {
                    dispatch(TasksAction.changeBadgesCount(result.data));
                } else {
                    console.log('Error: ', result.data);
                }
            }
        )
    }, [taskTableData.tasks]);

    return (
        <PageLayout>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                {defaultTasksToWorkPage.title}
            </Grid>
            <TasksToWorkTable
                tasks={getTaskToWorkTableData(taskTableData.tasks)}
                controllers={taskTableData.controllers || []}
                branches={taskTableData.branches || []}
                periods={taskTableData.periods || []}
                tasksCount={taskTableData.tasksCount || 0}
                onTaskListLoad={onTaskListLoad}
                onSynchronizeData={onSynchronizeData}
                onTasksToWorkDelete={onTasksToWorkDelete}
                tasksLoaded={tasksLoaded}
            >
                <FlashComponent
                    message={flashError.message}
                    messages={flashError.messages}
                    // onCloseFlash={onCloseErrorFlash}
                    showFlash={flashError.status}
                    flashType="error"
                    flashWidth="100%"
                />
                <FlashComponent
                    message={flashWarning.message}
                    onCloseFlash={onCloseWarningFlash}
                    showFlash={flashWarning.status}
                    flashType="warning"
                />
                <FlashComponent
                    message={flashInfo.message}
                    onCloseFlash={onCloseInfoFlash}
                    showFlash={flashInfo.status}
                    flashType="info"
                />
                <FlashComponent
                    message={flashSuccess.message}
                    onCloseFlash={onCloseSuccessFlash}
                    showFlash={flashSuccess.status}
                    flashType="success"
                />
            </TasksToWorkTable>
        </PageLayout>
    );
}