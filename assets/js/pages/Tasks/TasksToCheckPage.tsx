import React, {useEffect, useState} from "react";
import PageLayout from "../../components/Layouts/PageLayout/PageLayout";
import {
    acceptAllTheTaskToCheckRequest,
    getBadgesCount,
    getCompaniesForFilterRequest,
    getControllersForTasksRequest,
    getPeriodsRequest,
    getTasksToCheckRequest,
} from "../../requests/Request";
import {useDispatch, useSelector} from "react-redux";
import {CompanyBranch, TaskToCheckData, UserData} from "../../interfaces/interfaces";
import {getTasksToCheckCollection, getTaskToCheckTableData} from "../../functions/functions";
import TasksToCheckTable from "./TasksTable/TasksToCheckTable";
import {AppAction, TasksAction} from "../../store/actions/actions";
import {Grid} from "@material-ui/core";
import FlashComponent from "../../components/Flashes/FlashComponent";
import {useHistory} from "react-router";
import {appStateInterface} from "../../store/appState";

interface tasksToCheckPageInterface {
    title: string
}

const defaultTasksToCheckPage: tasksToCheckPageInterface = {
    title: 'Задачи к проверке',
}

export default function TasksToCheckPage() {
    const [flashError, setFlashError] = useState({status: false, message: ''});
    const [flashSuccess, setFlashSuccess] = useState({status: false, message: ''});
    const [flashInfo, setFlashInfo] = useState({status: false, message: ''});
    const [flashWarning, setFlashWarning] = useState({status: false, message: ''});
    const [tasksLoaded, setTasksLoaded] = useState(false);
    const [controllersLoaded, setControllersLoaded] = useState(false);
    const [branchesLoaded, setBranchesLoaded] = useState(false);
    const [periodsLoaded, setPeriodsLoaded] = useState(false);
    const [taskTableData, setTaskTableData] = useState<{
        tasks: TaskToCheckData[]|[]
        controllers: UserData[]|[]
        branches: CompanyBranch[]|[]
        periods: []
        tasksCount: number
    }>({tasks: [], controllers: [], branches: [], periods: [], tasksCount: 0});

    const dispatch = useDispatch();
    const history = useHistory();
    const filtersData = useSelector((state: appStateInterface) => state.filtersData);

    const onCloseErrorFlash = () => {
        setFlashError(prevState => {return {...prevState, status: false}})
    };

    const onCloseSuccessFlash = () => {
        setFlashSuccess(prevState => {return {...prevState, status: false}})
    };

    const onCloseInfoFlash = () => {
        setFlashInfo(prevState => {return {...prevState, status: false}})
    };

    const onCloseWarningFlash = () => {
        setFlashWarning(prevState => {return {...prevState, status: false}})
    };

    const acceptAllTheTasks = () => {
        dispatch(AppAction.showLoader());
        setTasksLoaded(false);
        acceptAllTheTaskToCheckRequest().then((result) => {
            if (result.type == 'success') {
                setTaskTableData( prevState => {
                    return {
                        ...prevState,
                        tasks: result.data.tasks,
                        tasksCount: result.data.tasksCount,
                    }
                });
                setFlashSuccess({status: true, message: 'Задачи успешно приняты'})
                dispatch(TasksAction.changeTasksToCheckAction(getTasksToCheckCollection(result.data.tasks)));
                dispatch(AppAction.hideLoader())
            } else {
                console.log('Error task data: ', result.data);
                dispatch(AppAction.hideLoader())
                setFlashError({
                    message: result.data,
                    status: true
                })
            }
        }).finally(() => {
            setTasksLoaded(true);
            dispatch(AppAction.hideLoader());
        });
    }

    useEffect(() => {
        dispatch(AppAction.showLoader());
        setTasksLoaded(false);
        const requestData = filtersData.filtersLocation !== history.location.pathname ? {page: 1, maxResult: 100} : {
            page: 1,
            maxResult: 100,
            searchField: filtersData.searchField,
            search: filtersData.searchValue,
            company: filtersData.companyFilter,
            user: filtersData.executorFilter,
            subscriberType: filtersData.subscriberTypeFilter,
            period: filtersData.periodFilter,
        }
        getTasksToCheckRequest(requestData).then(
            result => {
                if (result.type == 'success') {
                    setTaskTableData( prevState =>
                        {
                            return {
                                ...prevState,
                                tasks: result.data.tasks,
                                tasksCount: result.data.tasksCount,
                            }
                        }
                    );
                    dispatch(TasksAction.changeTasksToCheckAction(getTasksToCheckCollection(result.data.tasks)));
                } else {
                    console.log('Error: ', result.data);
                    if (result.code == 404) {
                        setTaskTableData( prevState =>
                            {
                                return {
                                    ...prevState,
                                    tasks: []
                                }
                            }
                        );
                        setFlashInfo({
                            status:true,
                            message: 'Нет доступных к проверке задач'
                        })
                    } else {
                        setFlashError({
                            status:true,
                            message: result.data
                        });
                    }
                }
            }
        ).finally(() => {
            setTasksLoaded(true);
        });
        getControllersForTasksRequest().then(
            result => {
                if (result.type == 'success') {
                    setTaskTableData( prevState =>
                        {
                            return {
                                ...prevState,
                                controllers: result.data,
                            }
                        }
                    );
                    if (result.data.length === 0) {
                        setFlashInfo({
                            status:true,
                            message: 'Нет доступных исполнителей. Для возможности сортировки по ' +
                                'исполнителям добавьте их в меню "Пользователи"'
                        })
                    }
                } else {
                    console.log('Error: ', result);
                    setFlashError({
                        status:true,
                        message: result.data
                    });
                }
            }
        ).finally(() => {
            setControllersLoaded(true);
        });
        getCompaniesForFilterRequest().then(
            result => {
                if (result.type == 'success') {
                    setTaskTableData( prevState =>
                        {
                            return {
                                ...prevState,
                                branches: result.data
                            }
                        }
                    );
                    if (result.data.length === 0) {
                        setFlashError({
                            status:true,
                            message: 'Нет доступных отделений. Обратитесь к администратору'
                        })
                    }
                } else {
                    console.log('Error: ', result.data);
                    setFlashError({
                        status:true,
                        message: result.data
                    });
                }
            }
        ).finally(() => {
            setBranchesLoaded(true);
        });
        getPeriodsRequest('1').then(
            result => {
                if (result.type == 'success') {
                    setTaskTableData( prevState =>
                        {
                            return {
                                ...prevState,
                                periods: result.data
                            }
                        }
                    );
                } else {
                    console.log('Error: ', result.data);
                }
            }
        ).finally(() => {
            setPeriodsLoaded(true);
        });
    }, []);

    useEffect(() => {
        if (tasksLoaded && controllersLoaded && branchesLoaded && periodsLoaded) {
            dispatch(AppAction.hideLoader())
        }
    }, [tasksLoaded, controllersLoaded, branchesLoaded, periodsLoaded]);

    useEffect(() => {
        getBadgesCount().then(
            result => {
                if (result.type == 'success') {
                    dispatch(TasksAction.changeBadgesCount(result.data));
                } else {
                    console.log('Error: ', result.data);
                }
            }
        )
    }, [taskTableData.tasks]);

    return (
        <PageLayout>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                {defaultTasksToCheckPage.title}
            </Grid>
            <TasksToCheckTable
                tasks={getTaskToCheckTableData(taskTableData.tasks)}
                controllers={taskTableData.controllers || []}
                branches={taskTableData.branches || []}
                periods={taskTableData.periods || []}
                tasksCount={taskTableData.tasksCount || 0}
                onTasksAccept={acceptAllTheTasks}
                tasksLoaded={tasksLoaded}
            >
                <FlashComponent
                    message={flashError.message}
                    // messages={flashError.messages}
                    // onCloseFlash={onCloseErrorFlash}
                    showFlash={flashError.status}
                    flashType="error"
                    flashWidth="100%"
                />
                <FlashComponent
                    message={flashWarning.message}
                    onCloseFlash={onCloseWarningFlash}
                    showFlash={flashWarning.status}
                    flashType="warning"
                />
                <FlashComponent
                    message={flashInfo.message}
                    onCloseFlash={onCloseInfoFlash}
                    showFlash={flashInfo.status}
                    flashType="info"
                />
                <FlashComponent
                    message={flashSuccess.message}
                    onCloseFlash={onCloseSuccessFlash}
                    showFlash={flashSuccess.status}
                    flashType="success"
                />
            </TasksToCheckTable>
        </PageLayout>
    );
}