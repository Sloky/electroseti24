import React, {useEffect, useState} from "react";
import {number} from "prop-types";

interface TablePaginationProps {
    pageCount: number
    currentPage: number
    onPageNumberChange(value: number): void
}

const PageItems = ({pageCount, currentPage, onItemClick}) => {
    let items = [];
    for (let i= 1; i <= pageCount; i++) {
        items.push(
            <li
                key={'pageItem' + i.toString()}
                className={i == currentPage ? 'paginate_button page-item active' : 'paginate_button page-item'}
            >
                <a
                    data-dt-idx="1" tabIndex={0}
                    className="page-link cursor-pointer"
                    onClick={() => onItemClick(i)}
                >
                    {i}
                </a>
            </li>
        );
    }
    return (<>{items}</>);
};

export default function TablePagination(props: TablePaginationProps) {
    const [currentPage, setCurrentPage] = useState<number>(props.currentPage);

    const onItemClick = (item) => {
        setCurrentPage(item);
    };

    const onPreviousClick = () => {
        setCurrentPage(prevState => prevState - 1);
    };

    const onNextClick = () => {
        setCurrentPage(prevState => prevState + 1);
    };

    useEffect(() => {
        setCurrentPage(props.currentPage);
    }, [props.currentPage])

    useEffect(() => {
        props.onPageNumberChange(currentPage);
    }, [currentPage]);

    return (
        <div className="dataTables_paginate paging_simple_numbers float-right d-flex">
            <ul className="pagination">
                <li className={currentPage == 1 ? 'paginate_button page-item previous disabled' : 'paginate_button page-item previous'}>
                    <a
                        data-dt-idx="0"
                        tabIndex={0}
                        className="page-link cursor-pointer"
                        onClick={onPreviousClick}
                    >
                        <i className="fal fa-chevron-left"/>
                    </a>
                </li>
                <PageItems currentPage={currentPage} pageCount={props.pageCount} onItemClick={onItemClick}/>
                <li className={currentPage == props.pageCount ? 'paginate_button page-item next disabled' : 'paginate_button page-item next'}>
                    <a
                        data-dt-idx="7"
                        tabIndex={0}
                        className="page-link cursor-pointer"
                        onClick={onNextClick}
                    >
                        <i className="fal fa-chevron-right"/>
                    </a>
                </li>
            </ul>
        </div>
    );
}