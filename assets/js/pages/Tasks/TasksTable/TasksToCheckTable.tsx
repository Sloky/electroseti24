import React, {useEffect, useState} from "react";
import {CompanyBranch, TaskTableData, UserData} from "../../../interfaces/interfaces";
import "./TasksTable.css"
import TableItemsCustomizer from "./TableItemsCustomizer";
import SimpleSelectInput from "../../../components/FormInputs/SimpleSelectInput/SimpleSelectInput";
import {Box, Grid, Pagination, Stack} from "@material-ui/core";
import CheckTable from "../../../components/Tables/CheckTable";
import {getTasksToCheckRequest} from "../../../requests/Request";
import SimpleModal from "../../../components/Modal/SimpleModal/SimpleModal";
import {getTasksToCheckCollection, getTaskToCheckTableData} from "../../../functions/functions";
import {useDispatch, useSelector} from "react-redux";
import {appStateInterface} from "../../../store/appState";
import {AppAction, TasksAction} from "../../../store/actions/actions";
import {useHistory} from "react-router";
import SearchComponent from "../../../components/Other/SearchComponent/SearchComponent";

interface TaskToCheckTableProps {
    tasks: TaskTableData[]
    controllers: UserData[]
    branches: CompanyBranch[]
    tasksCount: number
    children?: any
    periods: {
        period: string
    }[]
    onTasksAccept(): void
    tasksLoaded?: boolean
}

const headerItems = [
    'Узел учёта',
    'Статус',
    'Отделение',
    'Адрес',
    'Дата выполнения',
    'Исполнитель'
];

const searchFields = [
    {title: 'Абонент', value: 'subscriberTitle'},
    {title: 'Адрес', value: 'address'},
    {title: 'Договор', value: 'agreementNumber'},
    {title: 'Дата выполнения', value: 'executedDate'},
    {title: 'Узел учёта', value: 'counterNumber'},
];

const selectSubscriberType = [
    {title: 'Физическое лицо', value: '0'},
    {title: 'Юридическое лицо', value: '1'},
    {title: 'Центральный аппарат', value: '2'},
    {title: 'Электрон', value: '3'},
];

function TaskToCheckTable(props: TaskToCheckTableProps) {
    const [itemsCount, setItemsCount] = useState<number>(100);
    const [pageNumber, setPageNumber] = useState<number>(1);
    const [totalItems, setTotalItems] = useState<number>(props.tasksCount);
    const [pagesCount, setPagesCount] = useState<number>(Math.ceil(totalItems/itemsCount || 1));
    const [tasks, setTasks] = useState<TaskTableData[]>(props.tasks);

    const [companyFilter, setCompanyFilter] = useState(null);
    const [executorFilter, setExecutorFilter] = useState(null);
    const [subscriberTypeFilter, setSubscriberTypeFilter] = useState(null);
    const [periodFilter, setPeriodFilter] = useState(null);
    const [searchValue, setSearchValue] = useState('');
    const [searchField, setSearchField] = useState(null);

    const dispatch = useDispatch();
    const history = useHistory();

    const filtersData = useSelector((state: appStateInterface) => state.filtersData);

    const onFilterClear = () => {
        setCompanyFilter(null);
        setExecutorFilter(null);
        setSubscriberTypeFilter(null);
        setPeriodFilter(null);
        setSearchValue('');
        setSearchField(null);
        dispatch(AppAction.showLoader());
        dispatch(TasksAction.changeFilters({
            searchValue: '',
            searchField: null,
            companyFilter: null,
            executorFilter: null,
            subscriberTypeFilter: null,
            periodFilter: null,
            filtersLocation: history.location.pathname,
        }));
        getTasksToCheckRequest({
            page: pageNumber,
            maxResult: itemsCount,
            search: '',
            searchField: null,
            company: null,
            user: null,
            subscriberType: null,
            period: null,
        }).then(result => {
            setTotalItems(result.data.tasksCount);
            setTasks(getTaskToCheckTableData(result.data.tasks));
            dispatch(TasksAction.changeTasksToCheckAction(getTasksToCheckCollection(result.data.tasks)))
            dispatch(AppAction.hideLoader());
        })
    };

    const onSearchValueChange = (value: string) => {
        setSearchValue(value);
    };

    const onSearchFieldChange = (value: string) => {
        setSearchField(value);
    };

    const onPageNumberChange = (value: number) => {
        setPageNumber(value);
    };

    const onTableItemCountChange = (value: number) => {
        setItemsCount(value);
    };

    const acceptAllTheTasks = () => {
        props.onTasksAccept();
    }

    useEffect(() => {
        if (filtersData && filtersData.filtersLocation === history.location.pathname) {
            setCompanyFilter(filtersData.companyFilter);
            setExecutorFilter(filtersData.executorFilter);
            setSearchValue(filtersData.searchValue);
            setSubscriberTypeFilter(filtersData.subscriberTypeFilter);
            setSearchField(filtersData.searchField);
            setPeriodFilter(filtersData.periodFilter);
        }
    }, []);

    useEffect(() => {
        if (props.tasksLoaded) {
            setTotalItems(props.tasksCount);
            setTasks(props.tasks);
        }
    }, [props.tasksLoaded])

    useEffect(() => {
        dispatch(AppAction.showLoader());
        const requestData = filtersData.filtersLocation !== history.location.pathname ? {page: pageNumber, maxResult: itemsCount} : {
            page: pageNumber,
            maxResult: itemsCount,
            search: filtersData.searchValue,
            searchField: filtersData.searchField,
            company: filtersData.companyFilter,
            user: filtersData.executorFilter,
            subscriberType: filtersData.subscriberTypeFilter,
            period: filtersData.periodFilter,
        }
        getTasksToCheckRequest(requestData)
            .then(result => {
                setTasks(getTaskToCheckTableData(result.data.tasks));
                setTotalItems(result.data.tasksCount);
                dispatch(TasksAction.changeTasksToCheckAction(getTasksToCheckCollection(result.data.tasks)));
                dispatch(AppAction.hideLoader());
            })
    }, [pageNumber, itemsCount])

    const applyFilters = () => {
        dispatch(AppAction.showLoader());
        dispatch(TasksAction.changeFilters({
            searchValue,
            searchField,
            companyFilter,
            executorFilter,
            subscriberTypeFilter,
            periodFilter,
            filtersLocation: history.location.pathname,
        }));
        getTasksToCheckRequest({
            page: pageNumber,
            maxResult: itemsCount,
            search: searchValue,
            searchField: searchField,
            company: companyFilter,
            user: executorFilter,
            subscriberType: subscriberTypeFilter,
            period: periodFilter,
        }).then(result => {
            setTotalItems(result.data.tasksCount);
            setTasks(getTaskToCheckTableData(result.data.tasks));
            dispatch(TasksAction.changeTasksToCheckAction(getTasksToCheckCollection(result.data.tasks)))
            dispatch(AppAction.hideLoader());
        })
    }

    /*Пагинация. Изменение количества страниц при изменении размера списка*/
    useEffect(() => {
        setPagesCount(Math.ceil(totalItems/itemsCount || 1));
    }, [itemsCount, totalItems]);

    /*Пагинация. Установка номера страницы при изменении количества страниц*/
    useEffect(() => {
        if (pageNumber > pagesCount) {
            setPageNumber(pagesCount);
        }
    }, [pagesCount]);

    return (
        <Box>
            {(tasks.length > 0) &&
                <Grid container style={{marginBottom: '30px',}}>
                    <Grid item xs={2} style={{paddingRight: '15px',}}>
                        <SimpleModal
                            modalName={'AcceptAllTheTasksModal'}
                            modalTitle={'Принять задачи'}
                            startButtonTitle={'Принять все задачи'}
                            startButtonStyle={{width: '100%',}}
                            acceptButtonTitle={'Принять'}
                            onModalSubmit={acceptAllTheTasks}
                            startButtonVariant={'contained'}
                            closeButtonTitle={'Отменить'}
                        >
                            <Box component="p" sx={{color: 'error.main', fontSize: '18px', fontWeight: '500',}}>
                                Не будут приняты
                            </Box>
                            <Box sx={{color: 'error.main', bgcolor: 'error.light', padding: '20px',borderRadius: '4px',}}>
                                Задачи без показаний и задачи с измененными полями
                            </Box>
                        </SimpleModal>
                    </Grid>
                </Grid>
            }
            {props.children}
            <Grid container style={{marginBottom: '30px',}}>
                <Grid item xs={2} style={{paddingRight: '15px',}}>
                    <SimpleSelectInput
                        label={'Отделение'}
                        allowEmptyValue={true}
                        emptyValueTitle={'Все отделения'}
                        defaultValue={companyFilter}
                        options={props.branches.map(branch => {
                            return {
                                title: branch.title,
                                value: branch.id
                            }
                        })}
                        formControlStyle={{width: '100%',}}
                        onChange={value => {
                            setCompanyFilter(value)
                        }}
                    />
                </Grid>
                <Grid item xs={2} style={{paddingRight: '15px',}}>
                    <SimpleSelectInput
                        label={'Исполнитель'}
                        allowEmptyValue={true}
                        emptyValueTitle={'Все исполнители'}
                        defaultValue={executorFilter}
                        options={props.controllers.map(controller => {
                            let userFullName = controller.surname+' '+controller.name+' '+controller.patronymic;
                            return {
                                title: userFullName,
                                value: controller.id
                            }
                        })}
                        formControlStyle={{width: '100%',}}
                        onChange={value => {setExecutorFilter(value)}}
                    />
                </Grid>
                <Grid item xs={2} style={{paddingRight: '15px',}}>
                    <SimpleSelectInput
                        label={'Тип абонента'}
                        allowEmptyValue={true}
                        emptyValueTitle={'Все абоненты'}
                        options={selectSubscriberType}
                        defaultValue={subscriberTypeFilter}
                        formControlStyle={{width: '100%',}}
                        onChange={value => {setSubscriberTypeFilter(value)}}
                    />
                </Grid>
                <Grid item xs={2} style={{paddingRight: '15px',}}>
                    <SimpleSelectInput
                        label={'Период'}
                        allowEmptyValue={true}
                        emptyValueTitle={'Все периоды'}
                        options={props.periods.map(period => {
                            let periodItems = period.period.split('-');
                            let periodTitle = periodItems[0] + '-' + periodItems[1];
                            return {
                                title: periodTitle,
                                value: period.period
                            }
                        })}
                        defaultValue={periodFilter}
                        formControlStyle={{width: '100%',}}
                        onChange={value => {setPeriodFilter(value)}}
                    />
                </Grid>
            </Grid>
            <Grid container style={{marginBottom: '30px',}}>
                <Grid item xs={2} style={{paddingRight: '15px',}}>
                    <SimpleSelectInput
                        label={'Поиск'}
                        allowEmptyValue={true}
                        emptyValueTitle={'Все поля'}
                        options={searchFields}
                        defaultValue={searchField}
                        formControlStyle={{width: '100%',}}
                        onChange={onSearchFieldChange}
                    />
                </Grid>
                <Grid item xs={6}>
                    <SearchComponent
                        searchValue={searchValue}
                        onSearchValueChange={onSearchValueChange}
                        applyFilters={applyFilters}
                        enableFilters={true}
                        onFilterClear={onFilterClear}
                        searchFieldXs={4}
                        buttonXs={4}
                    />
                </Grid>
            </Grid>
            <Grid style={{marginBottom: '20px',}}>
                <TableItemsCustomizer
                    initialCount={itemsCount}
                    onTableItemCountChange={onTableItemCountChange}
                    allItems={totalItems}
                />
            </Grid>
            <Grid>
                <CheckTable headerItems={headerItems} tableDataRows={tasks}/>
                <Stack spacing={2} style={{marginTop: '30px',}}>
                    <Pagination count={pagesCount} variant="outlined" shape="rounded" color="primary" onChange={(event, page) => onPageNumberChange(page)}/>
                </Stack>
            </Grid>
        </Box>
    );
}

export default TaskToCheckTable;