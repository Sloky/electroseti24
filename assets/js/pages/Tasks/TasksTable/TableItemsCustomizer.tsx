import React from "react";
import {Box, FormControl, MenuItem, Select} from "@material-ui/core";

interface TableItemsCustomizerProps {
    initialCount?: number
    allItems: number
    itemsPerPage?: string[]
    onTableItemCountChange(value: number): void
}

export default function TableItemsCustomizer(props: TableItemsCustomizerProps) {
    const onChange = (e) => {
        props.onTableItemCountChange(Number(e.target.value));
    };

    const itemsPerPage = props.itemsPerPage ?? ['10', '50', '100', 'All',];

    return (
        <>
            <Box style={{display: 'flex', alignItems: 'center', fontSize: '15px',}}>
                Показать
                <FormControl>
                    <Select
                        size="small"
                        onChange={onChange}
                        defaultValue={itemsPerPage[2]}
                        style={{
                            margin: '0px 5px',
                            fontSize: '15px',
                        }}
                        label={null}
                        variant="standard"
                    >
                        {itemsPerPage.map((value, index) => (
                            <MenuItem key={index} value={value === 'All' ? '10000' : value}>{value}</MenuItem>
                        ))}
                    </Select>
                </FormControl>
                <Box component="span" style={{fontSize: '15px',}}> из <Box component="span">{props.allItems}</Box> элементов</Box>
            </Box>
        </>
    );
}