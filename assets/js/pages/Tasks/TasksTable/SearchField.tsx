import React, {useEffect, useState} from "react";

interface SearchFieldProps {
    searchFields: {
        title: string
        value: string
    }[]
    allowEmptyField?: boolean
    emptyFieldTitle?: string
    defaultSearchValue?: string|null
    defaultSearchField?: string|null
    onSearchValueChange(value: string): void
    onSearchFieldChange(value: string): void
}

const SearchFieldParams = {
  placeholder: 'Поиск'
};

export default function SearchField(props: SearchFieldProps) {
    const [value, setValue] = useState(props.defaultSearchValue && '');
    const [field, setField] = useState(props.defaultSearchField !== null ? props.defaultSearchField : props.allowEmptyField && 'empty');
    const [options, setOptions] = useState(props.searchFields);

    useEffect(() => {
        if (props.allowEmptyField) {
            setOptions(prevState => [...prevState, {value: 'empty', title: props.emptyFieldTitle}]);
        }
    }, []);

    useEffect(() => {
        setField(props.defaultSearchField !== null ? props.defaultSearchField : props.allowEmptyField && 'empty');
        props.defaultSearchValue === null && setValue('');
    }, [props.defaultSearchField, props.defaultSearchValue]);

    const onSelectChange = (e) => {
        setField(e.target.value);
        props.onSearchFieldChange(e.target.value == 'empty' ? null : e.target.value);
    };

    const onChange = (e) => {
        setValue(e.target.value);
        props.onSearchValueChange(e.target.value);
    };
    return (
        <div className="dataTables_filter">
            <label className={'d-flex flex-row input-group'}>
                <div className="input-group-text d-inline-flex width-3 align-items-center justify-content-center border-bottom-right-radius-0 border-top-right-radius-0 border-right-0">
                    <i className="fal fa-search"/>
                </div>
                <input
                    value={value}
                    type="search"
                    className="form-control border-top-left-radius-0 border-bottom-left-radius-0 ml-0 width-lg shadow-inset-1"
                    placeholder={SearchFieldParams.placeholder}
                    onChange={onChange}
                />
                <select
                    value={field}
                    className={"custom-select form-control"}
                    onChange={onSelectChange}
                >
                    {options.map((option, index) => {
                        return (<option key={index} value={option.value}>{option.title}</option>);
                    })}
                </select>
                {/*<select*/}
                {/*    className="custom-select"*/}
                {/*>*/}
                {/*    <option value="1">Все поля</option>*/}
                {/*    {props.searchFields.map((field) => (*/}
                {/*        <option key={field.title} value={field.value}>{field.title}</option>*/}
                {/*    ))}*/}
                {/*</select>*/}
            </label>
        </div>
    );
}
