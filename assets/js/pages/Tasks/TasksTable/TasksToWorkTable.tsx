import React, {useEffect, useState} from "react";
import {CompanyBranch, TaskTableData, UserData} from "../../../interfaces/interfaces";
import "./TasksTable.css"
import TableItemsCustomizer from "./TableItemsCustomizer";
import SimpleSelectInput from "../../../components/FormInputs/SimpleSelectInput/SimpleSelectInput";
import {useDispatch, useSelector} from "react-redux";
import {appStateInterface} from "../../../store/appState";
import SimpleModal from "../../../components/Modal/SimpleModal/SimpleModal";
import {
    Box,
    Grid,
    Stack,
    Pagination,
} from "@material-ui/core";
import {DeleteOutline} from "@material-ui/icons";
import WorkTable from "../../../components/Tables/WorkTable";
import {getTasksToWorkRequest} from "../../../requests/Request";
import {getTasksToWorkCollection, getTaskToWorkTableData} from "../../../functions/functions";
import {TaskTableRequestData} from "../../../interfaces/interfaces";
import {AppAction, TasksAction} from "../../../store/actions/actions";
import {useHistory} from "react-router";
import SearchComponent from "../../../components/Other/SearchComponent/SearchComponent";

interface TaskTableProps {
    tasks: TaskTableData[]
    controllers: UserData[]
    branches: CompanyBranch[]
    tasksCount: number
    children?: any
    periods: {
        period: string
    }[]
    onTaskListLoad(file: any): void
    onSynchronizeData?(): void
    onTasksToWorkDelete?(data: TaskTableRequestData): void
    tasksLoaded?: boolean
}

const headerItems = [
    'Узел учёта',
    'Отделение',
    'Адрес',
    'Срок выполнения',
    'Исполнитель'
];

const searchFields = [
    {title: 'Абонент', value: 'subscriberTitle'},
    {title: 'Адрес', value: 'address'},
    {title: 'Договор', value: 'agreementNumber'},
    {title: 'Срок выполнения', value: 'deadline'},
    {title: 'Узел учёта', value: 'counterNumber'},
];

const selectSubscriberType = [
    {title: 'Физическое лицо', value: '0'},
    {title: 'Юридическое лицо', value: '1'},
    {title: 'Центральный аппарат', value: '2'},
    {title: 'Электрон', value: '3'},
];

function TasksToWorkTable(props: TaskTableProps) {
    const [itemsCount, setItemsCount] = useState<number>(100);
    const [pageNumber, setPageNumber] = useState<number>(1);
    const [totalItems, setTotalItems] = useState<number>(props.tasksCount);
    const [pagesCount, setPagesCount] = useState<number>(Math.ceil(totalItems/itemsCount || 1));
    const [tasks, setTasks] = useState<TaskTableData[]>(props.tasks);

    const [companyFilter, setCompanyFilter] = useState(null);
    const [executorFilter, setExecutorFilter] = useState(null);
    const [subscriberTypeFilter, setSubscriberTypeFilter] = useState(null);
    const [periodFilter, setPeriodFilter] = useState(null);
    const [searchValue, setSearchValue] = useState('');
    const [searchField, setSearchField] = useState(null);

    const dispatch = useDispatch();
    const history = useHistory();

    const filtersData = useSelector((state: appStateInterface) => state.filtersData);

    const currentUser = useSelector((state: appStateInterface) => state.user);

    const onFilterClear = () => {
        setCompanyFilter(null);
        setExecutorFilter(null);
        setSubscriberTypeFilter(null);
        setPeriodFilter(null);
        setSearchValue('');
        setSearchField(null);
        dispatch(AppAction.showLoader());
        dispatch(TasksAction.changeFilters({
            searchValue: '',
            companyFilter: null,
            executorFilter: null,
            subscriberTypeFilter: null,
            searchField: null,
            periodFilter: null,
            filtersLocation: history.location.pathname,
        }));
        getTasksToWorkRequest({
            page: pageNumber,
            maxResult: itemsCount,
            searchField: null,
            search: '',
            company: null,
            user: null,
            subscriberType: null,
            period: null,
        }).then(result => {
            setTotalItems(result.data.tasksCount);
            setTasks(getTaskToWorkTableData(result.data.tasks));
            dispatch(TasksAction.changeTasksToWorkAction(getTasksToWorkCollection(result.data.tasks)));
            dispatch(AppAction.hideLoader());
        })
    };

    const onSynchronizeClick = () => {
        // props.onSynchronizeData();
    };

    const onSearchValueChange = (value: string) => {
        setSearchValue(value);
    };

    const onSearchFieldChange = (value: string) => {
        setSearchField(value);
    };

    const onPageNumberChange = (value: number) => {
        setPageNumber(value);
    };

    const onTableItemCountChange = (value: number) => {
        setItemsCount(value);
    };

    const onTaskListClear = () => {
        props.onTasksToWorkDelete({
            searchField: searchField,
            search: searchValue,
            company: companyFilter,
            user: executorFilter,
            subscriberType: subscriberTypeFilter,
            period: periodFilter,
        });
    }

    useEffect(() => {
        if (filtersData && filtersData.filtersLocation === history.location.pathname) {
            setCompanyFilter(filtersData.companyFilter);
            setExecutorFilter(filtersData.executorFilter);
            setSearchValue(filtersData.searchValue);
            setSubscriberTypeFilter(filtersData.subscriberTypeFilter);
            setSearchField(filtersData.searchField);
            setPeriodFilter(filtersData.periodFilter);
        }
    }, []);

    useEffect(() => {
        if (props.tasksLoaded) {
            setTotalItems(props.tasksCount);
            setTasks(props.tasks);
        }
    }, [props.tasksLoaded])

    useEffect(() => {
        dispatch(AppAction.showLoader());
        const requestData = filtersData?.filtersLocation !== history.location.pathname ? {page: pageNumber, maxResult: itemsCount} : {
            page: pageNumber,
            maxResult: itemsCount,
            searchField: filtersData.searchField,
            search: filtersData.searchValue,
            company: filtersData.companyFilter,
            user: filtersData.executorFilter,
            subscriberType: filtersData.subscriberTypeFilter,
            period: filtersData.periodFilter,
        }
        getTasksToWorkRequest(requestData)
            .then(result => {
                setTasks(getTaskToWorkTableData(result.data.tasks));
                setTotalItems(result.data.tasksCount);
                dispatch(TasksAction.changeTasksToWorkAction(getTasksToWorkCollection(result.data.tasks)));
                dispatch(AppAction.hideLoader());
            })
    }, [pageNumber, itemsCount])

    const applyFilters = () => {
        dispatch(AppAction.showLoader());
        dispatch(TasksAction.changeFilters({
            searchValue,
            companyFilter,
            executorFilter,
            subscriberTypeFilter,
            searchField,
            periodFilter,
            filtersLocation: history.location.pathname,
        }));
        getTasksToWorkRequest({
            page: pageNumber,
            maxResult: itemsCount,
            searchField: searchField,
            search: searchValue,
            company: companyFilter,
            user: executorFilter,
            subscriberType: subscriberTypeFilter,
            period: periodFilter,
        }).then(result => {
            setTotalItems(result.data.tasksCount);
            setTasks(getTaskToWorkTableData(result.data.tasks));
            dispatch(TasksAction.changeTasksToWorkAction(getTasksToWorkCollection(result.data.tasks)));
            dispatch(AppAction.hideLoader());
        })
    }

    /*Пагинация. Изменение количества страниц при изменении размера списка*/
    useEffect(() => {
        setPagesCount(Math.ceil(totalItems/itemsCount || 1));
    }, [itemsCount, totalItems]);

    /*Пагинация. Установка номера страницы при изменении количества страниц*/
    useEffect(() => {
        if (pageNumber > pagesCount) {
            setPageNumber(pagesCount);
        }
    }, [pagesCount]);

    return (
        <>
            <Box>
                {/*{currentUser.roles.includes('ROLE_ADMIN') &&*/}
                {/*<Grid style={{marginBottom: '30px', display: 'flex',}}>*/}
                {/*    <Tooltip title={'Функционал пока недоступен'} arrow>*/}
                {/*        <Button*/}
                {/*            variant="outlined"*/}
                {/*            startIcon={<Sync/>}*/}
                {/*            onClick={onSynchronizeClick}*/}
                {/*            style={{width: '206px', height: '43px', fontSize: '12px',}}*/}
                {/*        >*/}
                {/*            Синхронизация*/}
                {/*        </Button>*/}
                {/*    </Tooltip>*/}
                {/*</Grid>*/}
                {/*}*/}
                {props.children}
                <Grid container style={{marginBottom: '30px',}}>
                    <Grid item xs={2} style={{paddingRight: '15px',}}>
                        <SimpleSelectInput
                            label={'Отделение'}
                            allowEmptyValue={true}
                            emptyValueTitle={'Все отделения'}
                            defaultValue={companyFilter}
                            options={props.branches.map(branch => {
                                return {
                                    title: branch.title,
                                    value: branch.id
                                }
                            })}
                            formControlStyle={{width: '100%',}}
                            onChange={value => {
                                setCompanyFilter(value)
                            }}
                        />
                    </Grid>
                    <Grid item xs={2} style={{paddingRight: '15px',}}>
                        <SimpleSelectInput
                            label={'Исполнитель'}
                            allowEmptyValue={true}
                            emptyValueTitle={'Все исполнители'}
                            defaultValue={executorFilter}
                            options={props.controllers.map(controller => {
                                let userFullName = controller.surname+' '+controller.name+' '+controller.patronymic;
                                return {
                                    title: userFullName,
                                    value: controller.id
                                }
                            })}
                            formControlStyle={{width: '100%',}}
                            onChange={value => {setExecutorFilter(value)}}
                        />
                    </Grid>
                    <Grid item xs={2} style={{paddingRight: '15px',}}>
                        <SimpleSelectInput
                            label={'Тип абонента'}
                            allowEmptyValue={true}
                            emptyValueTitle={'Все абоненты'}
                            options={selectSubscriberType}
                            defaultValue={subscriberTypeFilter}
                            formControlStyle={{width: '100%',}}
                            onChange={value => {setSubscriberTypeFilter(value)}}
                        />
                    </Grid>
                    <Grid item xs={2} style={{paddingRight: '15px',}}>
                        <SimpleSelectInput
                            label={'Период'}
                            allowEmptyValue={true}
                            emptyValueTitle={'Все периоды'}
                            options={props.periods.map(period => {
                                let periodItems = period.period.split('-');
                                let periodTitle = periodItems[0] + '-' + periodItems[1];
                                return {
                                    title: periodTitle,
                                    value: period.period
                                }
                            })}
                            defaultValue={periodFilter}
                            formControlStyle={{width: '100%',}}
                            onChange={value => {setPeriodFilter(value)}}
                        />
                    </Grid>
                </Grid>
                <Grid container style={{marginBottom: '30px',}}>
                    <Grid item xs={2} style={{paddingRight: '15px',}}>
                        <SimpleSelectInput
                            label={'Поиск'}
                            allowEmptyValue={true}
                            emptyValueTitle={'Все поля'}
                            options={searchFields}
                            defaultValue={searchField}
                            formControlStyle={{width: '100%',}}
                            onChange={onSearchFieldChange}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <SearchComponent
                            searchValue={searchValue}
                            onSearchValueChange={onSearchValueChange}
                            applyFilters={applyFilters}
                            searchFieldXs={4} buttonXs={4}
                            enableFilters={true}
                            onFilterClear={onFilterClear}
                        />
                    </Grid>
                </Grid>
                <Grid style={{display: 'flex', justifyContent: 'space-between', marginBottom: '20px',}}>
                    <TableItemsCustomizer
                        initialCount={itemsCount}
                        onTableItemCountChange={onTableItemCountChange}
                        allItems={totalItems}
                        // itemsPerPage={itemsPerPage}
                    />
                    {currentUser.roles.includes('ROLE_SUPER_ADMIN') && (tasks.length > 0) && <SimpleModal
                        modalName={'clearTasksToWork'}
                        modalTitle={'Удаление списка задач к выполнению'}
                        startButtonTitle={'Удалить задачи'}
                        // startButtonStyle={{display: "flex", marginLeft: "5px"}}
                        acceptButtonTitle={'Подтвердить'}
                        onModalSubmit={onTaskListClear}
                        startIcon={<DeleteOutline/>}
                        startButtonVariant={'text'}
                        closeButtonTitle={'Отменить'}
                    >
                        Удалить список задач к выполнению?
                    </SimpleModal>
                    }
                </Grid>
                <Grid>
                    <WorkTable headerItems={headerItems} tableDataRows={tasks}/>
                    <Stack spacing={2} style={{marginTop: '30px',}}>
                        <Pagination count={pagesCount} variant="outlined" shape="rounded" color="primary" onChange={(event, page) => onPageNumberChange(page)}/>
                    </Stack>
                </Grid>
            </Box>
        </>
    );
}

export default TasksToWorkTable;