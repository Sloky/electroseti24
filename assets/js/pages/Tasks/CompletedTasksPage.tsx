import React, {useEffect, useState} from "react";
import PageLayout from "../../components/Layouts/PageLayout/PageLayout";
import {
    getBadgesCount,
    getCompaniesForFilterRequest,
    getCompletedTasksExcelRequest,
    getCompletedTasksPhotosRequest,
    getCompletedTasksRequest,
    getControllersForTasksRequest,
    getPeriodsRequest,
} from "../../requests/Request";
import {useDispatch, useSelector} from "react-redux";
import {CompanyBranch, CompleteTaskData, ExcelAndPhotosUploadRequestData, UserData} from "../../interfaces/interfaces";
import {getCompletedTasksCollection, getCompletedTaskTableData,} from "../../functions/functions";
import {AppAction, TasksAction} from "../../store/actions/actions";
import CompletedTasksTable from "./TasksTable/CompletedTasksTable";
import {Grid} from "@material-ui/core";
import FlashComponent from "../../components/Flashes/FlashComponent";
import {useHistory} from "react-router";
import {appStateInterface} from "../../store/appState";

interface completedTasksPageInterface {
    title: string
}

const defaultCompletedTasksPage: completedTasksPageInterface = {
    title: 'Принятые задачи',
}

export default function CompletedTasksPage() {
    const [flashError, setFlashError] = useState({status: false, message: ''});
    const [flashSuccess, setFlashSuccess] = useState({status: false, message: ''});
    const [flashInfo, setFlashInfo] = useState({status: false, message: ''});
    const [flashWarning, setFlashWarning] = useState({status: false, message: ''});
    const [tasksLoaded, setTasksLoaded] = useState(false);
    const [controllersLoaded, setControllersLoaded] = useState(false);
    const [branchesLoaded, setBranchesLoaded] = useState(false);
    const [periodsLoaded, setPeriodsLoaded] = useState(false);
    const [taskTableData, setTaskTableData] = useState<{
        tasks: CompleteTaskData[]|[]
        controllers: UserData[]|[]
        branches: CompanyBranch[]|[]
        periods: {period: string}[]|[]
        tasksCount: number
    }>({tasks: [], controllers: [], branches: [], periods: [], tasksCount: 0});

    const dispatch = useDispatch();
    const history = useHistory();
    const filtersData = useSelector((state: appStateInterface) => state.filtersData);

    const onCloseErrorFlash = () => {
        setFlashError(prevState => {return {...prevState, status: false}})
    };

    const onCloseSuccessFlash = () => {
        setFlashSuccess(prevState => {return {...prevState, status: false}})
    };

    const onCloseInfoFlash = () => {
        setFlashInfo(prevState => {return {...prevState, status: false}})
    };

    const onCloseWarningFlash = () => {
        setFlashWarning(prevState => {return {...prevState, status: false}})
    };

    const getCompletedTasks = () => {
        setTasksLoaded(false);
        const requestData = filtersData.filtersLocation !== history.location.pathname ?
            {
                page: 1,
                maxResult: 100,
                period: taskTableData.periods.length ? taskTableData.periods[taskTableData.periods.length - 1].period : null
            } : {
                page: 1,
                maxResult: 100,
                searchField: filtersData.searchField,
                search: filtersData.searchValue,
                company: filtersData.companyFilter,
                user: filtersData.executorFilter,
                subscriberType: filtersData.subscriberTypeFilter,
                period: filtersData.periodFilter,
            }
        getCompletedTasksRequest(requestData).then(
            result => {
                if (result.type == 'success') {
                    setTaskTableData( prevState =>
                        {
                            return {
                                ...prevState,
                                tasks: result.data.tasks,
                                tasksCount: result.data.tasksCount,
                            }
                        }
                    );
                    dispatch(TasksAction.changeCompletedTasksAction(getCompletedTasksCollection(result.data.tasks)));
                } else {
                    console.log('Error: ', result.data);
                    if (result.code == 404) {
                        setTaskTableData( prevState =>
                            {
                                return {
                                    ...prevState,
                                    tasks: []
                                }
                            }
                        );
                        setFlashInfo({
                            status:true,
                            message: 'Нет доступных для просмотра задач'
                        })
                    } else {
                        setFlashError({
                            status:true,
                            message: result.data
                        });
                    }
                }
            }
        ).finally(() => {
            setTasksLoaded(true);
        });
    }

    useEffect(() => {
        dispatch(AppAction.showLoader());
        getControllersForTasksRequest().then(
            result => {
                if (result.type == 'success') {
                    setTaskTableData( prevState =>
                        {
                            return {
                                ...prevState,
                                controllers: result.data,
                            }
                        }
                    );
                    if (result.data.length === 0) {
                        setFlashInfo({
                            status:true,
                            message: 'Нет доступных исполнителей. Для возможности сортировки по ' +
                                'исполнителям добавьте их в меню "Пользователи"'
                        })
                    }
                } else {
                    console.log('Error: ', result);
                    setFlashError({
                        status:true,
                        message: result.data
                    });
                }
            }
        ).finally(() => {
            setControllersLoaded(true);
        });
        getCompaniesForFilterRequest().then(
            result => {
                if (result.type == 'success') {
                    setTaskTableData( prevState =>
                        {
                            return {
                                ...prevState,
                                branches: result.data
                            }
                        }
                    );
                    if (result.data.length === 0) {
                        setFlashError({
                            status:true,
                            message: 'Нет доступных отделений. Обратитесь к администратору'
                        })
                    }
                } else {
                    console.log('Error: ', result.data);
                    setFlashError({
                        status:true,
                        message: result.data
                    });
                }
            }
        ).finally(() => {
            setBranchesLoaded(true);
        });
        getPeriodsRequest('2').then(
            result => {
                if (result.type == 'success') {
                    setTaskTableData( prevState =>
                        {
                            return {
                                ...prevState,
                                periods: result.data
                            }
                        }
                    );
                } else {
                    console.log('Error: ', result.data);
                }
            }
        ).finally(() => {
            setPeriodsLoaded(true);
        });
    }, []);

    useEffect(() => {
        if (periodsLoaded) {
            getCompletedTasks();
        }
    }, [periodsLoaded]);

    const onUploadAllThePhotos = (data: ExcelAndPhotosUploadRequestData) => {
        dispatch(AppAction.showTextLoader());
        getCompletedTasksPhotosRequest(data)
            .then((result) => {
                if (result.type == 'success') {
                    let link = document.createElement('a');
                    // let dateTimeFormat = Intl.DateTimeFormat('ru');
                    // let date = new Date(task.executed_date);
                    link.download = `photos.zip`;

                    let blob = new Blob([result.data], {type: 'application/zip'});

                    link.href = URL.createObjectURL(blob);

                    link.click();

                    URL.revokeObjectURL(link.href);
                } else {
                    setFlashError({
                        status: true,
                        message: 'Во время выгрузки фотографий произошла ошибка'
                    });
                }
            })
            .finally(() => {
                dispatch(AppAction.hideTextLoader());
            });
    };

    const onUploadExcel = (data: ExcelAndPhotosUploadRequestData) => {
        dispatch(AppAction.showTextLoader());
        getCompletedTasksExcelRequest(data)
            .then((result) => {
                if (result.type == 'success') {
                    let link = document.createElement('a');
                    link.download = `TasksList.xlsx`;

                    let blob = new Blob(
                        [result.data],
                        {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'}
                    );

                    link.href = URL.createObjectURL(blob);

                    link.click();

                    URL.revokeObjectURL(link.href);
                } else {
                    setFlashError({
                        status: true,
                        message: 'Во время выгрузки обходного листа произошла ошибка'
                    });
                }
            })
            .finally(() => {
                dispatch(AppAction.hideTextLoader());
            });
    };

    useEffect(() => {
        if (tasksLoaded && controllersLoaded && branchesLoaded && periodsLoaded) {
            dispatch(AppAction.hideLoader());
        }
    }, [tasksLoaded, controllersLoaded, branchesLoaded, periodsLoaded]);

    useEffect(() => {
        getBadgesCount().then(
            result => {
                if (result.type == 'success') {
                    dispatch(TasksAction.changeBadgesCount(result.data));
                } else {
                    console.log('Error: ', result.data);
                }
            }
        )
    }, [taskTableData.tasks]);

    return (
        <PageLayout>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                {defaultCompletedTasksPage.title}
            </Grid>
            <CompletedTasksTable
                tasks={getCompletedTaskTableData(taskTableData.tasks)}
                controllers={taskTableData.controllers || []}
                branches={taskTableData.branches || []}
                periods={taskTableData.periods || []}
                tasksCount={taskTableData.tasksCount || 0}
                onUploadAllThePhotos={onUploadAllThePhotos}
                onUploadExcel={onUploadExcel}
                getCompletedTasks={getCompletedTasks}
                tasksLoaded={tasksLoaded}
                periodsLoaded={periodsLoaded}
            >
                <FlashComponent
                    message={flashError.message}
                    // onCloseFlash={onCloseErrorFlash}
                    showFlash={flashError.status}
                    flashType="error"
                    flashWidth="100%"
                />
                <FlashComponent
                    message={flashWarning.message}
                    onCloseFlash={onCloseWarningFlash}
                    showFlash={flashWarning.status}
                    flashType="warning"
                />
                <FlashComponent
                    message={flashInfo.message}
                    onCloseFlash={onCloseInfoFlash}
                    showFlash={flashInfo.status}
                    flashType="info"
                />
                <FlashComponent
                    message={flashSuccess.message}
                    onCloseFlash={onCloseSuccessFlash}
                    showFlash={flashSuccess.status}
                    flashType="success"
                />
            </CompletedTasksTable>
        </PageLayout>
    );
}