import React from "react";
import LoginLayout from "../../components/Layouts/LoginLayout/LoginLayout";
import LoginForm from "../../components/Forms/LoginForm/LoginForm";

export default function LoginPage() {
    return (
        <LoginLayout>
            <LoginForm
                submitTitle={'Войти'}
                usernameTitle={'Имя пользователя'}
                passwordTitle={'Пароль'}
                rememberMeTitle={'Запомнить'}
            />
        </LoginLayout>
    );
}