import React, {Dispatch, useEffect, useState} from "react";
import {SimpleFlashData, UserGroupData} from "../../../interfaces/interfaces";
import {getUserGroupsRequest} from "../../../requests/Request";
import {
    Box,
    Button,
    Grid,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow
} from "@material-ui/core";
import FlashComponent from "../../../components/Flashes/FlashComponent";
import {useHistory} from "react-router";
import {THEME} from "../../../theme";
import {useDispatch} from "react-redux";
import {AppAction} from "../../../store/actions/actions";

const componentParams = {
    addGroupLinkTitle: 'Добавить группу',
    addGroupLinkSource: '/group/new'
};

let listHeader = [
    'Наименование',
    'Описание',
    'Роли'
];

export default function GroupsList() {
    const [mount, setMount] = useState(false);
    const [flashError, setFlashError] : [SimpleFlashData, any] = useState({status:false});
    const [groupsData, setGroupsData] : [UserGroupData[], Dispatch<UserGroupData[]>] = useState();

    const history = useHistory();
    const dispatch = useDispatch();

    async function getGroups() {
        let groupsRequest = await getUserGroupsRequest();
        if (groupsRequest.type == 'success') {
            setGroupsData(groupsRequest.data);
            if (groupsRequest.data?.length === 0) {
                setFlashError({
                    status:true,
                    message: 'Нет доступных групп'
                });
            }
        } else {
            setFlashError({
                status:true,
                message: groupsRequest.data.message
            });
        }
    }

    useEffect(() => {
        if (!mount) {
            dispatch(AppAction.showLoader());
            getGroups().then(r => {
                dispatch(AppAction.hideLoader());
            });
            setMount(true);
        }
    });

    const closeFlash = () => {
        setFlashError(prevState => ({...prevState, status: false}))
    }

    return (
        <>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                Группы пользователей
            </Grid>
            <Grid container style={{marginBottom: '30px',}}>
                <Grid item xs={3} style={{paddingRight: '15px',}}>
                    <Button
                        onClick={() => history.push(componentParams.addGroupLinkSource)}
                        variant={'contained'}
                        fullWidth
                        size={'medium'}
                        style={{fontSize: '12px',}}
                    >
                        {componentParams.addGroupLinkTitle}
                    </Button>
                </Grid>
            </Grid>
            <FlashComponent
                message={flashError.message}
                messageHeading={flashError.messageHeading}
                showFlash={flashError.status}
                flashType={'error'}
                // onCloseFlash={closeFlash}
                flashWidth={'100%'}
            />
            <TableContainer sx={{bgcolor: 'background.default'}}>
                <Table>
                    <TableHead>
                        <TableRow>
                            {listHeader.map((value, index) => (
                                <TableCell key={index}>{value}</TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {Array.isArray(groupsData) && groupsData.length ? groupsData.map((group, index) => (
                            <TableRow hover key={index} style={{cursor: 'pointer',}} onClick={() => history.push(`/group/edit/${group.id}`)}>
                                <TableCell style={{fontSize: '15px',}}>
                                    {group.group_name}
                                </TableCell>
                                <TableCell style={{fontSize: '15px',}}>
                                    {group.description}
                                </TableCell>
                                <TableCell style={{fontSize: '15px',}}>
                                    {group.roles.join(', ')}
                                </TableCell>
                            </TableRow>
                        )) : (
                            <></>
                        )}
                    </TableBody>
                </Table>
            </TableContainer>
            {!groupsData && (
                <Box style={{padding: '70px 0px', textAlign: 'center', width: '100%', color: THEME.PLACEHOLDER_TEXT, fontSize: '16px',}}>
                    Нет данных
                </Box>
            )}
        </>
    );
}