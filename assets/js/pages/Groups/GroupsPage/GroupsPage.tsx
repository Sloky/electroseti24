import React from "react";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import GroupsList from "./GroupsList";

export default function GroupsPage() {
    return (
        <PageLayout>
            <GroupsList/>
        </PageLayout>
    );
}