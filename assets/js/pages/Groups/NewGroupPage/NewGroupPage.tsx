import React from "react";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import NewGroupCard from "./NewGroupCard";

export default function NewGroupPage() {
    return (
        <PageLayout>
            <NewGroupCard/>
        </PageLayout>
    );
}