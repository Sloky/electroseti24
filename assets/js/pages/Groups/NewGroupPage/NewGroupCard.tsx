import React, {Dispatch, useEffect, useState} from "react";
import "./NewGroupCard.css";
import {SimpleFlashData} from "../../../interfaces/interfaces";
import {
    addNewGroupRequest,
    getAllRolesRequest,
} from "../../../requests/Request";
import {Box, Button, Grid, TextField} from "@material-ui/core";
import FlashComponent from "../../../components/Flashes/FlashComponent";
import {Formik} from "formik";
import * as Yup from "yup";
import {useHistory} from "react-router";
import CheckboxGroup from "../../../components/FormInputs/SimpleCheckboxGroup/CheckboxGroup";
import {useDispatch} from "react-redux";
import {AppAction} from "../../../store/actions/actions";

interface groupFormInterface {
    groupName: string
    roles: string[]
    description: string
}

/**
 * Преобразование объекта ключ => роль в массив ролей
 * @param roles
 */
const getRolesAsArray = (roles: {[id: string] : string}) => {
    let arr = [];
    for (let id in roles) {
        arr.push(roles[id]);
    }
    return arr;
};

export default function NewGroupCard() {
    const [mount, setMount] = useState(false);
    const [flashError, setFlashError] : [SimpleFlashData, any] = useState({status:false});
    const [flashSuccess, setFlashSuccess] : [SimpleFlashData, any] = useState({status: false});
    const [roles, setRoles] : [string[], Dispatch<string[]>] = useState([]);

    const dispatch = useDispatch();
    const history = useHistory();

    async function getRoles() {
        let rolesRequest = await getAllRolesRequest();
        if (rolesRequest.type == 'success') {
            setRoles(getRolesAsArray(rolesRequest.data));
        } else {
            setFlashError({
                status:true,
                message: rolesRequest.data.message
            });
        }
    }

    useEffect(() => {
        if (!mount) {
            getRoles().then(r => setMount(true));
        }
    });

    const handleSubmit = (values) => {
        dispatch(AppAction.showLoader());
        addNewGroupRequest(values).then(
            result => {
                if (result.type == 'success') {
                    setFlashSuccess({
                        status:true,
                        message: 'Группа была успешно добавлена'
                    });
                    history.push('/groups');
                } else {
                    setFlashError({
                        status:true,
                        message: 'Произошла ошибка. Обратитесь к администратору'
                    });
                }
            }
        ).finally(() => {
            dispatch(AppAction.hideLoader());
        });
    }

    const onCloseSuccessFlash = () => {
        setFlashSuccess({status: false});
    }

    const onCloseErrorFlash = () => {
        setFlashError({status: false});
    }

    const initialValues: groupFormInterface = {
        groupName: '',
        roles: [],
        description: '',
    }

    const groupSchema = Yup.object().shape({
        groupName: Yup.string().required('Поле с названием группы не может быть пустым'),
        roles: Yup.array().required('Выберите минимум одну роль'),
        description: Yup.string().required('Поле с описанием не может быть пустым'),
    })

    return (
        <>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                Добавление группы
            </Grid>
            <FlashComponent
                messageHeading={flashError.messageHeading}
                message={flashError.message}
                showFlash={flashError.status}
                // onCloseFlash={onCloseErrorFlash}
                flashType={'error'}
                flashWidth={'100%'}
            />
            <FlashComponent
                message={flashSuccess.message}
                showFlash={flashSuccess.status}
                onCloseFlash={onCloseSuccessFlash}
                flashType={'success'}
            />
            <Formik
                initialValues={initialValues}
                validationSchema={groupSchema}
                onSubmit={(values) => handleSubmit(values)}
            >
                {({
                    handleChange,
                    handleSubmit,
                    handleBlur,
                    dirty,
                    isValid,
                    errors,
                    values,
                    touched,
                }) => (
                    <Box component={"form"}>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={3} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'groupName'}
                                    placeholder={'Введите название группы'}
                                    label={'Название группы'}
                                    onChange={handleChange}
                                    error={errors.groupName && touched.groupName}
                                    helperText={touched.groupName && errors.groupName}
                                />
                            </Grid>
                        </Grid>
                        <Box sx={{
                            color: 'secondary.main',
                            fontSize: '16px',
                            fontWeight: '500',
                            marginBottom: '30px',
                        }}>
                            Роли пользователей
                        </Box>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={3}>
                                <CheckboxGroup
                                    name={'roles'}
                                    id={'roles'}
                                    defaultValues={[]}
                                    items={roles.length == 0 ? [] : roles.map((role) => ({
                                        label: role, value: role
                                    }))}
                                    onChange={handleChange}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={6} style={{paddingRight: '15px',}}>
                                <TextField
                                    label={'Описание группы'}
                                    name={'description'}
                                    id={'description'}
                                    fullWidth
                                    multiline
                                    placeholder={'Введите описание группы'}
                                    onChange={handleChange}
                                />
                            </Grid>
                        </Grid>
                        <Grid container>
                            <Grid item xs={3} style={{paddingRight: '15px',}}>
                                <Button
                                    variant={'contained'}
                                    fullWidth
                                    onClick={() => handleSubmit()}
                                    style={{fontSize: '12px',}}
                                >
                                    Создать группу
                                </Button>
                            </Grid>
                            <Grid item xs={3} style={{paddingRight: '15px',}}>
                                <Button
                                    variant={'outlined'}
                                    fullWidth
                                    onClick={() => history.goBack()}
                                    style={{fontSize: '12px',}}
                                >
                                    Отменить
                                </Button>
                            </Grid>
                        </Grid>
                    </Box>
                )}
            </Formik>
        </>
    );
}