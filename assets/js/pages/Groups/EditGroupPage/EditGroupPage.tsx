import React, {useLayoutEffect, useState} from "react";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import {useParams} from "react-router";
import {getAllRolesRequest, getGroupRequest} from "../../../requests/Request";
import EditGroupCard from "./EditGroupCard";
import {Box, CircularProgress} from "@material-ui/core";

/**
 * Преобразование объекта ключ => роль в массив ролей
 * @param roles
 */
const getRolesAsArray = (roles: {[id: string] : string}) => {
    let arr = [];
    for (let id in roles) {
        arr.push(roles[id]);
    }
    return arr;
};

export default function EditGroupPage() {
    const [loadingData, setLoadingData] = useState({
        group: null,
        roles: [],
        isLoaded: false
    });

    let params: any = useParams();
    let id = params.id;

    async function loadData(groupId) {
        let groupRequest = await getGroupRequest(groupId);
        let rolesRequest = await getAllRolesRequest();
        if (groupRequest.type == 'success' && rolesRequest.type == 'success') {
            return {
                group: groupRequest.data,
                roles: getRolesAsArray(rolesRequest.data),
                isLoaded: true
            };
        } else {
            return null;
        }
    }

    useLayoutEffect(() => {
        if (!loadingData.isLoaded) {
            loadData(id).then(result => {
                if (result) {
                    setLoadingData(result);
                } else {
                    setLoadingData({
                        group: null,
                        roles: [],
                        isLoaded: true
                    });
                }
            });
        }
    });

    return (
        <PageLayout>
            {!loadingData.isLoaded ? (
                <Box style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '500px',}}>
                    <CircularProgress/>
                </Box>
            ) : (
                <EditGroupCard group={loadingData.group} roles={loadingData.roles}/>
            )}
        </PageLayout>
    );
}