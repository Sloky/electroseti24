import React, {Dispatch, useLayoutEffect, useState} from "react";
import {SimpleFlashData, UserGroupData} from "../../../interfaces/interfaces";
import {deleteGroupRequest, editGroupRequest} from "../../../requests/Request";
import {useHistory} from "react-router-dom";
import {Box, Button, Grid, TextField} from "@material-ui/core";
import FlashComponent from "../../../components/Flashes/FlashComponent";
import {Formik} from "formik";
import * as Yup from "yup";
import SimpleModal from "../../../components/Modal/SimpleModal/SimpleModal";
import CheckboxGroup from "../../../components/FormInputs/SimpleCheckboxGroup/CheckboxGroup";
import {ArrowBackIos} from "@material-ui/icons";
import {useDispatch} from "react-redux";
import {AppAction} from "../../../store/actions/actions";

interface EditGroupCardProps {
    group: UserGroupData,
    roles: string[]
}

export default function EditGroupCard(props: EditGroupCardProps) {
    const [mount, setMount] = useState(false);
    const [flashError, setFlashError]: [SimpleFlashData, any] = useState({status: false});
    const [flashSuccess, setFlashSuccess]: [SimpleFlashData, any] = useState({status: false});
    const [roles, setRoles]: [string[], Dispatch<string[]>] = useState(props.roles);
    const [group, setGroup]: [UserGroupData, Dispatch<UserGroupData>] = useState(props.group);
    const [initialValues, setInitialValues] = useState({
        groupName: group ? group.group_name : '',
        roles: group ? group.roles : [],
        description: group ? group.description : ''
    });

    const history = useHistory();
    const dispatch = useDispatch();

    useLayoutEffect(() => {
        if (!mount) {
            if (!(props.roles && props.group)) {
                setFlashError({
                    status: true,
                    message: 'Произошла ошибка. Обратитесь к администратору.'
                })
            }
            setMount(true);
        }
    });

    const onDeleteHandler = () => {
        dispatch(AppAction.showLoader());
        deleteGroupRequest(group.id)
            .then(r => {
                if (r.type == 'success') {
                    history.push('/groups');
                } else {
                    dispatch(AppAction.hideLoader());
                    setFlashError({
                        status: true,
                        message: 'Произошла ошибка. Обратитесь к администратору.'
                    });
                }
            })
    };

    const handleSubmit = (values) => {
        dispatch(AppAction.showLoader());
        editGroupRequest(group.id, values).then(
            result => {
                if (result.type == 'success') {
                    setFlashSuccess({
                        status:true,
                        message: 'Группа была успешно изменена'
                    });
                } else {
                    setFlashError({
                        status:true,
                        message: 'Произошла ошибка. Обратитесь к администратору'
                    });
                }
            }
        ).finally(() => {
            dispatch(AppAction.hideLoader());
        });
    }

    const onCloseSuccessFlash = () => {
        setFlashSuccess({status: false});
    }

    const onCloseErrorFlash = () => {
        setFlashError({status: false});
    }

    const groupSchema = Yup.object().shape({
        groupName: Yup.string().required('Поле с названием группы не может быть пустым'),
        roles: Yup.array().required('Выберите минимум одну роль'),
        description: Yup.string().required('Поле с описанием не может быть пустым'),
    })

    return (
        <>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                Редактирование группы
            </Grid>
            <FlashComponent
                messageHeading={flashError.messageHeading}
                message={flashError.message}
                showFlash={flashError.status}
                // onCloseFlash={onCloseErrorFlash}
                flashType={'error'}
                flashWidth={'100%'}
            />
            <FlashComponent
                message={flashSuccess.message}
                showFlash={flashSuccess.status}
                onCloseFlash={onCloseSuccessFlash}
                flashType={'success'}
            />
            <Grid style={{marginBottom: '30px', display: 'flex', justifyContent: 'space-between',}}>
                <Button
                    onClick={() => history.goBack()}
                    startIcon={<ArrowBackIos/>}
                    sx={{fontSize: '14px', fontWeight: '500',}}
                    size="small"
                >
                    Назад
                </Button>
                <SimpleModal
                    startButtonTitle={'Удалить группу'}
                    modalName={'deleteGroupModal'}
                    // startIcon={}
                    startButtonVariant={'text'}
                    closeButtonTitle={'Отменить'}
                    modalTitle={'Удаление группы'}
                    acceptButtonTitle={'Подтвердить'}
                    onModalSubmit={onDeleteHandler}
                >
                    Удалить группу?
                </SimpleModal>
            </Grid>
            <Grid style={{marginBottom: '30px',}}>
                <Formik
                    initialValues={initialValues}
                    validationSchema={groupSchema}
                    onSubmit={(values) => handleSubmit(values)}
                >
                    {({
                          handleChange,
                          handleSubmit,
                          handleBlur,
                          dirty,
                          isValid,
                          errors,
                          values,
                          touched,
                      }) => (
                        <Box component={"form"}>
                            <Grid container style={{marginBottom: '30px',}}>
                                <Grid item xs={3} style={{paddingRight: '15px',}}>
                                    <TextField
                                        defaultValue={initialValues.groupName}
                                        fullWidth
                                        size={'small'}
                                        id={'groupName'}
                                        placeholder={'Введите название группы'}
                                        label={'Название группы'}
                                        onChange={handleChange}
                                        error={errors.groupName && touched.groupName}
                                        helperText={touched.groupName && errors.groupName}
                                    />
                                </Grid>
                            </Grid>
                            <Box sx={{
                                color: 'secondary.main',
                                fontSize: '16px',
                                fontWeight: '500',
                                marginBottom: '30px',
                            }}>
                                Роли пользователей
                            </Box>
                            <Grid container style={{marginBottom: '30px',}}>
                                <Grid item xs={3}>
                                    <CheckboxGroup
                                        name={'roles'}
                                        id={'roles'}
                                        defaultValues={initialValues.roles}
                                        items={roles.length == 0 ? [] : roles.map((role) => ({
                                            label: role, value: role
                                        }))}
                                        onChange={handleChange}
                                    />
                                </Grid>
                            </Grid>
                            <Grid container style={{marginBottom: '30px',}}>
                                <Grid item xs={6} style={{paddingRight: '15px',}}>
                                    <TextField
                                        defaultValue={initialValues.description}
                                        label={'Описание группы'}
                                        name={'description'}
                                        id={'description'}
                                        fullWidth
                                        multiline
                                        placeholder={'Введите описание группы'}
                                        onChange={handleChange}
                                    />
                                </Grid>
                            </Grid>
                            <Grid container>
                                <Grid item xs={3} style={{paddingRight: '15px',}}>
                                    <Button
                                        variant={'contained'}
                                        fullWidth
                                        onClick={() => handleSubmit()}
                                        style={{fontSize: '12px',}}
                                    >
                                        Сохранить
                                    </Button>
                                </Grid>
                                <Grid item xs={3} style={{paddingRight: '15px',}}>
                                    <Button
                                        variant={'outlined'}
                                        fullWidth
                                        onClick={() => history.goBack()}
                                        style={{fontSize: '12px',}}
                                    >
                                        Отменить
                                    </Button>
                                </Grid>
                            </Grid>
                        </Box>
                    )}
                </Formik>
            </Grid>
            <Button
                onClick={() => history.goBack()}
                startIcon={<ArrowBackIos/>}
                sx={{fontSize: '14px', fontWeight: '500',}}
                size="small"
            >
                Назад
            </Button>
        </>
    );
}