import React, {useEffect, useState} from "react";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import AgreementEditCard from "./AgreementEditCard";
import {SimpleFlashData} from "../../../interfaces/interfaces";
import {useDispatch, useSelector} from "react-redux";
import {useHistory, useParams} from "react-router";
import {appStateInterface} from "../../../store/appState";
import FlashComponent from "../../../components/Flashes/FlashComponent";
import {AppAction} from "../../../store/actions/actions";
import {
    deleteAgreementRequest,
    editAgreementRequest,
    getAgreementCardDataRequest,
    getCompaniesRequest
} from "../../../requests/Request";

export default function AgreementEditPage() {
    const [agreementData, setAgreementData] = useState({
        number: '',
        paymentCode: '',
        company: null,
        subscriber: null,
    });
    const [companies, setCompanies] = useState([]);

    const [agreementLoaded, setAgreementLoaded] = useState(false);
    const [companiesLoaded, setCompaniesLoaded] = useState(false);

    const [flashError, setFlashError] = useState<SimpleFlashData>({status: false})
    const [flashSuccess, setFlashSuccess] = useState({status: false, message: ''});

    const dispatch = useDispatch();
    const history = useHistory();

    let params: any = useParams();
    let id = params.id;
    let type = params.type;

    const breadcrumbsItems = [
        {
            itemTitle: type === 'ul' ? 'Юр. лица' : 'Физ. лица',
            itemSrc: `/subscribers/${type}`,
        },
        {
            itemTitle: 'Карточка абонента',
            itemSrc: `/subscriber/${type}/${agreementData.subscriber ? agreementData.subscriber.id : ''}`,
        },
        {
            itemTitle: 'Карточка договора',
            itemSrc: `/agreement-page/${type}/${id}`,
        },
        {
            itemTitle: 'Редактирование договора',
            // itemSrc: ``,
        },
    ]

    const onCloseErrorFlash = () => {
        setFlashError(prevState => {return {...prevState, status: false}})
    };

    const onCloseSuccessFlash = () => {
        setFlashSuccess(prevState => {return {...prevState, status: false}})
    };

    const showFlash = (type, message, messageHeading) => {
        switch (type) {
            case 'error':
                setFlashError({status: true, messageHeading: messageHeading, message: message});
                break;
            case 'success':
                setFlashSuccess({status: true, message: message});
                break;
            case 'disableFlashes':
                setFlashError({status: false,});
                setFlashSuccess({status: false, message: message});
        }
    }

    useEffect(() => {
        dispatch(AppAction.showLoader());
        getAgreementCardDataRequest(id).then(
            result => {
                if (result.type === 'success') {
                    setAgreementData(result.data);
                } else {
                    console.log('Error: ', result.data);
                    showFlash('error', 'Обратитесь к администратору', 'Ошибка.');
                }
            }
        ).finally(() => {
            setAgreementLoaded(true);
        })
        getCompaniesRequest({}).then(result => {
            if (result.type === 'success') {
                setCompanies(result.data.companies);
            }
        }).finally(() => {
            setCompaniesLoaded(true);
        })
    }, []);

    useEffect(() => {
        if (agreementLoaded && companiesLoaded) {
            dispatch(AppAction.hideLoader());
        }
    }, [agreementLoaded, companiesLoaded])

    const onSaveHandler = (values) => {
        dispatch(AppAction.showLoader());
        editAgreementRequest(id, values).then(
            result => {
                if (result.type === 'success') {
                    setAgreementData(result.data);
                    history.push(`/agreement-page/${type}/${id}`)
                } else {
                    showFlash('error', 'Пожалуйста, обратитесь к администратору', 'Ошибка.')
                }
            }
        ).finally(() => {
            dispatch(AppAction.hideLoader());
        })
    }

    const onDeleteHandler = () => {
        dispatch(AppAction.showLoader());
        deleteAgreementRequest(id).then(
            result => {
                if (result.type === 'success') {
                    history.go(-2);
                } else {
                    showFlash('error', 'Не удалось удалить договор. Пожалуйста, обратитесь к администратору', 'Ошибка.')
                }
            }
        ).finally(() => {
            dispatch(AppAction.hideLoader());
        })
    }

    const loading = useSelector((state: appStateInterface) => state.loading);

    return (
        <PageLayout
            breadcrumbsItems={breadcrumbsItems}
        >
            {!loading ?
                <AgreementEditCard
                    agreementData={agreementData}
                    companies={companies}
                    onFlash={(type, message, messageHeading) => {showFlash(type, message, messageHeading)}}
                    onSaveHandler={onSaveHandler}
                    onDeleteHandler={onDeleteHandler}
                >
                    <FlashComponent
                        message={flashError.message}
                        messageHeading={flashError.messageHeading}
                        showFlash={flashError.status}
                        flashType={'error'}
                    />
                    <FlashComponent
                        message={flashSuccess.message}
                        showFlash={flashSuccess.status}
                        onCloseFlash={onCloseSuccessFlash}
                        flashType={'success'}
                    />
                </AgreementEditCard> : <></>
            }
        </PageLayout>
    )
}