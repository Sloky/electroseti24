import React, {useEffect, useState} from "react";
import {Formik} from "formik";
import * as Yup from "yup";
import {Box, Button, Grid, TextField} from "@material-ui/core";
import SimpleModal from "../../../components/Modal/SimpleModal/SimpleModal";
import {CompanyData, SubscriberAgreementData} from "../../../interfaces/interfaces";
import BackButtonComponent from "../../../components/Other/BackButtonComponent/BackButtonComponent";
import SimpleSelectInput from "../../../components/FormInputs/SimpleSelectInput/SimpleSelectInput";
import {useSelector} from "react-redux";
import {appStateInterface} from "../../../store/appState";

interface AgreementEditCardProps {
    agreementData: SubscriberAgreementData
    companies: CompanyData[]
    children?: any
    onFlash(type: string, message: string, messageHeading?: string): void
    onSaveHandler?(data: any): void
    onDeleteHandler?(): void
}

export default function AgreementEditCard(props: AgreementEditCardProps) {
    const [initialValues, setInitialValues] = useState({
        number: props.agreementData.number || '',
        paymentCode: props.agreementData['payment_number'] || '',
        company: props.agreementData.company ? props.agreementData.company.id : '',
        subscriber: props.agreementData.subscriber ? props.agreementData.subscriber.id : '',
    });
    const [agreementStateStatus, setAgreementStateStatus] = useState(true);

    const currentUser = useSelector((state: appStateInterface) => state.user);

    const saveSubscriberHandler = (values) => {
        props.onSaveHandler(values);
    };

    const deleteSubscriberHandler = () => {
        props.onDeleteHandler();
    }

    useEffect(() => {
        setInitialValues({
            number: props.agreementData.number || '',
            paymentCode: props.agreementData['payment_number'] || '',
            company: props.agreementData.company ? props.agreementData.company.id : '',
            subscriber: props.agreementData.subscriber ? props.agreementData.subscriber.id : '',
        })
    }, [props]);

    const agreementSchema = Yup.object().shape({
        number: Yup.string().required('Поле с номером договора не может быть пустым'),
        paymentCode: Yup.string(),
        company: Yup.string().required('Отделение не выбрано'),
    })

    return (
        <>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                Редактировать договор
            </Grid>
            <Grid style={{marginBottom: '30px', display: 'flex', justifyContent: 'space-between',}}>
                <BackButtonComponent changed={!agreementStateStatus}/>
                {currentUser.roles.includes('ROLE_SUPER_ADMIN') &&
                    <SimpleModal
                        startButtonTitle={'Удалить договор'}
                        modalName={'deleteAgreementModal'}
                        startButtonVariant={'text'}
                        closeButtonTitle={'Отменить'}
                        modalTitle={'Удаление договора'}
                        acceptButtonTitle={'Подтвердить'}
                        onModalSubmit={deleteSubscriberHandler}
                    >
                        Удалить договор?
                    </SimpleModal>
                }
            </Grid>
            {props.children}
            <Formik
                initialValues={initialValues}
                validationSchema={agreementSchema}
                onSubmit={(values) => saveSubscriberHandler(values)}
            >
                {({
                    handleChange,
                    handleSubmit,
                    handleBlur,
                    dirty,
                    isValid,
                    errors,
                    values,
                    touched,
                    setFieldValue,
                    handleReset,
                }) => (
                    <Box
                        component={'form'}
                    >
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'number'}
                                    value={values.number}
                                    placeholder={'Введите номер договора'}
                                    label={'Номер договора'}
                                    onChange={(e) => {
                                        e.persist();
                                        setFieldValue('number', e.target.value);
                                        setAgreementStateStatus(false);
                                    }}
                                    error={errors.number && touched.number}
                                    helperText={touched.number && errors.number}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'paymentCode'}
                                    value={values.paymentCode}
                                    placeholder={'Введите платежный код'}
                                    label={'Платежный код'}
                                    onChange={(e) => {
                                        e.persist();
                                        setFieldValue('paymentCode', e.target.value);
                                        setAgreementStateStatus(false);
                                    }}
                                    error={errors.paymentCode && touched.paymentCode}
                                    helperText={touched.paymentCode && errors.paymentCode}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleSelectInput
                                    label={'Отделение'}
                                    options={props.companies.map(item => {
                                        return {
                                            title: item.title,
                                            value: item.id,
                                        }
                                    })}
                                    defaultValue={values.company}
                                    onChange={(value) => {
                                        setFieldValue('company', value)
                                        setAgreementStateStatus(false);
                                    }}
                                    formControlStyle={{width: '100%',}}
                                    allowEmptyValue={true}
                                    emptyValueTitle={'Не выбрано'}
                                    error={!!touched.company && !!!values.company}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={2} style={{paddingRight: '15px',}}>
                                <Button
                                    variant={'outlined'}
                                    fullWidth
                                    onClick={() => {
                                        handleReset();
                                        setAgreementStateStatus(true);
                                    }}
                                    className={'base_styled_btn'}
                                >
                                    Отменить изменения
                                </Button>
                            </Grid>
                            <Grid item xs={2} style={{paddingRight: '15px',}}>
                                <Button
                                    variant={'contained'}
                                    fullWidth
                                    onClick={() => handleSubmit()}
                                    className={'base_styled_btn'}
                                    disabled={agreementStateStatus}
                                >
                                    Сохранить
                                </Button>
                            </Grid>
                        </Grid>
                    </Box>
                )}
            </Formik>
            <BackButtonComponent changed={!agreementStateStatus}/>
        </>
    )
}