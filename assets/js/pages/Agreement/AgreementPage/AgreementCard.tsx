import React, {useEffect, useState} from "react";
import {useHistory, useParams} from "react-router";
import {
    Box,
    Button,
    Card,
    Grid,
    Pagination, Stack,
    Table, TableBody, TableCell,
    TableContainer,
    TableHead,
    TableRow,
} from "@material-ui/core";
import {THEME} from "../../../theme";
import TableItemsCustomizer from "../../Tasks/TasksTable/TableItemsCustomizer";
import SearchComponent from "../../../components/Other/SearchComponent/SearchComponent";
import {AgreementCounterTableData, SubscriberAgreementData} from "../../../interfaces/interfaces";

interface AgreementCardProps {
    agreementData: SubscriberAgreementData
    countersData: AgreementCounterTableData[]
    countersCount: number
    getCounters(requestData: any): void
}

const listHeader = [
    'Номер узла учёта',
    'Адрес',
    'Модель узла учёта',
    'Состояние / доступ',
]

export default function AgreementCard(props: AgreementCardProps) {
    const [agreementData, setAgreementData] = useState(props.agreementData);
    const [counters, setCounters] = useState(props.countersData);

    const [searchValue, setSearchValue] = useState('');

    const [itemsCount, setItemsCount] = useState(100);
    const [totalItems, setTotalItems] = useState(props.countersCount);
    const [pagesCount, setPagesCount] = useState<number>(Math.ceil(totalItems/itemsCount || 1));
    const [pageNumber, setPageNumber] = useState<number>(1);

    const history = useHistory();

    let params: any = useParams();
    let type = params.type;

    const applyFilters = () => {
        props.getCounters({page: pageNumber, maxResult: itemsCount, search: searchValue,});
    }

    const onSearchValueChange = (value: string) => {
        setSearchValue(value);
    };

    const onTableItemCountChange = (value: number) => {
        setItemsCount(value);
    };

    const onPageNumberChange = (value: number) => {
        setPageNumber(value);
    };

    useEffect(() => {
        setAgreementData(props.agreementData);
        setTotalItems(props.countersCount);
        setCounters(props.countersData);
    }, [props]);

    useEffect(() => {
        props.getCounters({page: pageNumber, maxResult: itemsCount, search: searchValue,});
    }, [pageNumber, itemsCount]);

    useEffect(() => {
        setPagesCount(Math.ceil(totalItems/itemsCount || 1));
    }, [itemsCount, totalItems]);

    useEffect(() => {
        if (pageNumber > pagesCount) {
            setPageNumber(pagesCount);
        }
    }, [pagesCount]);

    return (
        <>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                Карточка договора
            </Grid>
            {agreementData ?
                <>
                    <Card sx={{fontSize: '18px', fontWeight: '500', bgcolor: 'background.default', padding: '20px', marginBottom: '30px',}}>
                        <Grid container style={{marginBottom: '15px',}}>
                            <Grid item xs={3} sx={{color: THEME.PLACEHOLDER_TEXT,}}>Номер договора</Grid>
                            <Grid item xs={9}>{agreementData.number || 'Нет данных'}</Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '15px',}}>
                            <Grid item xs={3} sx={{color: THEME.PLACEHOLDER_TEXT,}}>Платежный код</Grid>
                            <Grid item xs={9}>{agreementData['payment_number'] || 'Нет данных'}</Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={3} sx={{color: THEME.PLACEHOLDER_TEXT,}}>Отделение</Grid>
                            <Grid item xs={9}>{agreementData.company.title || 'Нет данных'}</Grid>
                        </Grid>
                        <Grid item xs={2}>
                            <Button
                                variant={'outlined'}
                                size={'large'}
                                onClick={() => history.push(`/agreement-edit/${type}/${agreementData.id}`)}
                                className={'base_styled_btn'}
                                fullWidth
                            >
                                Редактировать договор
                            </Button>
                        </Grid>
                    </Card>
                    <Grid container style={{marginBottom: '30px',}}>
                        <Grid item xs={2}>
                            <Button
                                variant={'contained'}
                                size={'large'}
                                onClick={() => history.push(`/counter-new/${type}/${agreementData.id}`)}
                                className={'base_styled_btn'}
                                fullWidth
                            >
                                Добавить узел учёта
                            </Button>
                        </Grid>
                    </Grid>
                    <Grid container style={{marginBottom: '30px',}}>
                        <Grid item xs={5}>
                            <SearchComponent
                                searchValue={searchValue}
                                onSearchValueChange={onSearchValueChange}
                                applyFilters={applyFilters}
                                searchFieldXs={8}
                                buttonXs={4}
                            />
                        </Grid>
                    </Grid>
                    <Grid style={{marginBottom: '20px',}}>
                        <TableItemsCustomizer
                            initialCount={itemsCount}
                            allItems={totalItems}
                            onTableItemCountChange={onTableItemCountChange}
                        />
                    </Grid>
                    <TableContainer sx={{bgcolor: 'background.default', marginBottom: '30px',}}>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    {listHeader.map((value, index) => (
                                        <TableCell key={index} style={{color: THEME.PLACEHOLDER_TEXT}}>{value}</TableCell>
                                    ))}
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {counters && counters.length ? counters.map((data, index) => (
                                    <TableRow hover style={{cursor: 'pointer',}} key={index} onClick={() => history.push(`/counter-page/${type}/${data.id}`)}>
                                        <TableCell sx={{fontSize: '15px', fontWeight: '500',}}>
                                            {data.title}
                                        </TableCell>
                                        <TableCell sx={{fontSize: '15px', fontWeight: '500',}}>
                                            {data.address || data.temporary_address || 'Нет данных'}
                                        </TableCell>
                                        <TableCell sx={{fontSize: '15px', fontWeight: '500',}}>
                                            {data.counter_type || data.temporary_counter_type || 'Нет данных'}
                                        </TableCell>
                                        <TableCell sx={{fontSize: '15px', fontWeight: '500',}}>
                                            {data['work_status'] ? 'Недоступен / неисправен' : 'Доступен / исправен'}
                                        </TableCell>
                                    </TableRow>
                                )) : (
                                    <></>
                                )}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    {counters && !counters.length && (
                        <Box style={{padding: '70px 0px', textAlign: 'center', width: '100%', color: THEME.PLACEHOLDER_TEXT, fontSize: '16px',}}>
                            Нет данных
                        </Box>
                    )}
                    <Stack spacing={2} style={{marginTop: '30px'}}>
                        <Pagination
                            count={pagesCount}
                            variant={'outlined'}
                            shape={'rounded'}
                            color={'primary'}
                            onChange={(event, page) => onPageNumberChange(page)}
                        />
                    </Stack>
                </> : <></>
            }
        </>
    )
}