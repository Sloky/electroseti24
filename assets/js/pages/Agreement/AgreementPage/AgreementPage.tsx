import React, {useEffect, useState} from "react";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import AgreementCard from "./AgreementCard";
import {useDispatch} from "react-redux";
import {useParams} from "react-router";
import {AppAction} from "../../../store/actions/actions";
import {getAgreementCardDataRequest, getAgreementCountersDataRequest} from "../../../requests/Request";

export default function AgreementPage() {
    const [agreementData, setAgreementData] = useState(null);
    const [countersData, setCountersData] = useState(null)

    const [agreementLoaded, setAgreementLoaded] = useState(false);
    const [countersLoaded, setCountersLoaded] = useState(false);

    const dispatch = useDispatch();
    let params: any = useParams();
    let id = params.id;
    let type = params.type;

    const breadcrumbsItems = [
        {
            itemTitle: type === 'ul' ? 'Юр. лица' : 'Физ. лица',
            itemSrc: `/subscribers/${type}`,
        },
        {
            itemTitle: 'Карточка абонента',
            itemSrc: `/subscriber/${type}/${agreementData ? agreementData.subscriber.id : ''}`,
        },
        {
            itemTitle: 'Карточка договора',
            // itemSrc: ``,
        },
    ]

    useEffect(() => {
        dispatch(AppAction.showLoader());
        getAgreementCardDataRequest(id).then(
            result => {
                if (result.type === 'success') {
                    setAgreementData(result.data);
                }
            }
        ).finally(() => {
            setAgreementLoaded(true);
        })
        getAgreementCountersDataRequest(id, {page: 1, maxResult: 100,}).then(
            result => {
                if (result.type === 'success') {
                    setCountersData(result.data);
                }
            }
        ).finally(() => {
            setCountersLoaded(true);
        })
    }, []);

    useEffect(() => {
        if (agreementLoaded && countersLoaded) {
            dispatch(AppAction.hideLoader());
        }
    }, [agreementLoaded, countersLoaded]);

    const getCounters = (requestData) => {
        dispatch(AppAction.showLoader());
        getAgreementCountersDataRequest(id, requestData).then(
            result => {
                if (result.type === 'success') {
                    setCountersData(result.data);
                }
            }
        ).finally(() => {
            dispatch(AppAction.hideLoader());
        })
    }

    return (
        <PageLayout
            breadcrumbsItems={breadcrumbsItems}
        >
            <AgreementCard
                agreementData={agreementData}
                countersData={countersData && countersData.counters}
                countersCount={countersData && countersData.countersCount}
                getCounters={getCounters}
            />
        </PageLayout>
    )
}