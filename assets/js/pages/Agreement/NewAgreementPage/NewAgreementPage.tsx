import React, {useEffect, useState} from "react";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import NewAgreementCard from "./NewAgreementCard";
import {useDispatch} from "react-redux";
import {AppAction} from "../../../store/actions/actions";
import {getCompaniesRequest} from "../../../requests/Request";
import {useParams} from "react-router";

export default function NewAgreementPage() {
    const [companies, setCompanies] = useState([]);

    const dispatch = useDispatch();

    let params: any = useParams();
    let type = params.type;
    let subscriberId = params.id;

    const breadcrumbsItems = [
        {
            itemTitle: type === 'ul' ? 'Юр. лица' : 'Физ. лица',
            itemSrc: `/subscribers/${type}`,
        },
        {
            itemTitle: 'Карточка абонента',
            itemSrc: `/subscriber/${type}/${subscriberId}`,
        },
        {
            itemTitle: 'Добавление договора',
            // itemSrc: ``,
        },
    ]

    useEffect(() => {
        dispatch(AppAction.showLoader());
        getCompaniesRequest({}).then(result => {
            if (result.type === 'success') {
                setCompanies(result.data.companies);
            }
        }).finally(() => {
            dispatch(AppAction.hideLoader());
        })
    }, []);

    return (
        <PageLayout
            breadcrumbsItems={breadcrumbsItems}
        >
            <NewAgreementCard
                companies={companies}
                subscriberId={subscriberId}
            />
        </PageLayout>
    )
}