import React, {useState} from "react";
import {CompanyData, SimpleFlashData} from "../../../interfaces/interfaces";
import {useHistory} from "react-router";
import {useDispatch} from "react-redux";
import {AppAction} from "../../../store/actions/actions";
import {Formik} from "formik";
import * as Yup from "yup";
import {Box, Button, Grid, TextField} from "@material-ui/core";
import FlashComponent from "../../../components/Flashes/FlashComponent";
import SimpleSelectInput from "../../../components/FormInputs/SimpleSelectInput/SimpleSelectInput";
import {addNewAgreementRequest} from "../../../requests/Request";

interface AgreementFormInterface {
    number: string
    paymentCode: string
    company: string
    subscriber: string
}

interface NewAgreementCardProps {
    companies: CompanyData[]
    subscriberId: string
}

export default function NewAgreementCard(props: NewAgreementCardProps) {
    const [company, setCompany] = useState(null);
    const [flashError, setFlashError] : [SimpleFlashData, any] = useState({status: false});
    const [flashSuccess, setFlashSuccess] : [SimpleFlashData, any] = useState({status: false});

    const history = useHistory();
    const dispatch = useDispatch();

    const handleSubmit = (values) => {
        dispatch(AppAction.showLoader());
        addNewAgreementRequest(values).then(
            result => {
                if (result.type === 'success') {
                    history.goBack();
                } else {
                    if (result.type === 'error' && result.data.message.match(/An agreement with number (\S+) already exists/).length) {
                        setFlashError({
                            status: true,
                            message: `Произошла ошибка. Договор с номером ${result.data.message.match(/An agreement with number (\S+) already exists/)[1]} уже существует.`,
                        });
                    } else {
                        setFlashError({
                            status: true,
                            message: 'Произошла ошибка. Обратитесь к администратору.',
                        });
                    }
                }
            }
        ).finally(() => {
            dispatch(AppAction.hideLoader());
        })
    }

    const onCloseErrorFlash = () => {
        setFlashError({status: false});
    }

    const onCloseSuccessFlash = () => {
        setFlashSuccess({status: false});
    }

    const initialValues: AgreementFormInterface = {
        number: '',
        paymentCode: '',
        company: '',
        subscriber: props.subscriberId,
    }

    const agreementSchema = Yup.object().shape({
        number: Yup.string().required('Поле с номером договора не может быть пустым'),
        paymentCode: Yup.string(),
        company: Yup.string().required('Отделение не выбрано'),
    })

    return (
        <>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                Добавление договора
            </Grid>
            <FlashComponent
                messageHeading={flashError.messageHeading}
                message={flashError.message}
                showFlash={flashError.status}
                onCloseFlash={onCloseErrorFlash}
                flashType={'error'}
            />
            <FlashComponent
                message={flashSuccess.message}
                showFlash={flashSuccess.status}
                onCloseFlash={onCloseSuccessFlash}
                flashType={'success'}
            />
            <Formik
                initialValues={initialValues}
                validationSchema={agreementSchema}
                onSubmit={(values) => handleSubmit(values)}
            >
                {({
                    handleChange,
                    handleSubmit,
                    handleBlur,
                    dirty,
                    isValid,
                    errors,
                    values,
                    touched,
                    setFieldValue,
                }) => (
                    <Box
                        component={'form'}
                    >
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'number'}
                                    placeholder={'Введите номер договора'}
                                    label={'Номер договора'}
                                    onChange={handleChange}
                                    error={errors.number && touched.number}
                                    helperText={touched.number && errors.number}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'paymentCode'}
                                    placeholder={'Введите платежный код'}
                                    label={'Платежный код'}
                                    onChange={handleChange}
                                    error={errors.paymentCode && touched.paymentCode}
                                    helperText={touched.paymentCode && errors.paymentCode}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleSelectInput
                                    label={'Отделение'}
                                    options={props.companies.map(item => {
                                        return {
                                            title: item.title,
                                            value: item.id,
                                        }
                                    })}
                                    defaultValue={company}
                                    onChange={(value) => {
                                        setCompany(value);
                                        setFieldValue('company', value)
                                    }}
                                    formControlStyle={{width: '100%',}}
                                    allowEmptyValue={true}
                                    emptyValueTitle={'Не выбрано'}
                                    error={!!touched.company && !!!company}
                                />
                            </Grid>
                        </Grid>
                        <Grid container>
                            <Grid item xs={2} style={{paddingRight: '15px',}}>
                                <Button
                                    variant={'outlined'}
                                    fullWidth
                                    onClick={() => history.goBack()}
                                    className={'base_styled_btn'}
                                >
                                    Отменить
                                </Button>
                            </Grid>
                            <Grid item xs={2} style={{paddingRight: '15px',}}>
                                <Button
                                    variant={'contained'}
                                    fullWidth
                                    onClick={() => handleSubmit()}
                                    className={'base_styled_btn'}
                                >
                                    Создать
                                </Button>
                            </Grid>
                        </Grid>
                    </Box>
                )}
            </Formik>
        </>
    )
}