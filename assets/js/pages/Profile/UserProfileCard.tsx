import React from "react";
import {Box, Button, Card, Grid} from "@material-ui/core";
import Store from "../../store/store.";
import {useHistory} from "react-router";
import {ExitToApp, PersonOutline, MailOutline, HelpOutline} from "@material-ui/icons";
import {isObjEmpty} from "../../functions/functions";
import "./UserProfileCardStyle.css";
import SimpleModal from "../../components/Modal/SimpleModal/SimpleModal";

export default function UserProfileCard() {
    const user = Store.getState().user;
    const history = useHistory();

    if (isObjEmpty(user)) {
        history.push('/login');
    }

    const handleLogoutClick = () => {
        Store.dispatch({type: 'APP_CLEAR_STATE',});
        localStorage.clear();
        sessionStorage.clear()
        history.push("/login")
    };

    const changeCompaniesToString = (companies: {id: string, title: string}[]) => {
        if (companies) {
            return companies.map((item) => {
                return item.title;
            }).join('_');
        }
    }

    return (
        <>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                Личный кабинет
            </Grid>
            <Grid container style={{justifyContent: 'flex-end', marginBottom: '30px',}}>
                <Grid item>
                    <SimpleModal
                        modalName={'profileLogout'}
                        modalTitle={'Выход из аккаунта'}
                        startButtonTitle={'Выйти из аккаунта'}
                        acceptButtonTitle={'Подтвердить'}
                        onModalSubmit={handleLogoutClick}
                        startIcon={<ExitToApp/>}
                        startButtonVariant={'text'}
                        closeButtonTitle={'Отменить'}
                    >
                        Выйти из акканута?
                    </SimpleModal>
                </Grid>
            </Grid>
            <Card sx={{bgcolor: 'background.default', padding: '20px', marginBottom: '30px',}}>
                <Grid container>
                    <Grid item xs={11} sx={{color: 'text.primary', fontSize: '18px', fontWeight: '500',}}>
                        {user.surname + ' ' + user.name + ' ' + user.patronymic}
                    </Grid>
                    <Grid item xs={1}>
                        <PersonOutline sx={{color: 'secondary.main', fontSize: 70}}/>
                    </Grid>
                </Grid>
            </Card>
            <Grid container>
                <Grid item xs={3} style={{paddingRight: '15px',}}>
                    <Button
                        // href={`http://support.electroline24.ru?user_id=${user.id}&branch_name=${changeCompaniesToString(user.companies)}`}
                        href={'https://t.me/electroseti24'}
                        target={'_blank'}
                        variant={'outlined'}
                        size={'small'}
                        fullWidth
                        startIcon={<MailOutline/>}
                        sx={{bgcolor: 'background.default',}}
                        style={{textDecoration: "none"}}
                        className={'btn profile_btn base_styled_btn'}
                    >
                        Написать в поддержку
                    </Button>
                </Grid>
                <Grid item xs={3} style={{paddingRight: '15px',}}>
                    <Button
                        href={`http://faq.electroline24.ru?user_id=${user.id}&branch_name=${changeCompaniesToString(user.companies)}`}
                        target={'_blank'}
                        variant={'outlined'}
                        size={'small'}
                        fullWidth
                        startIcon={<HelpOutline/>}
                        sx={{bgcolor: 'background.default',}}
                        className={'btn profile_btn base_styled_btn'}
                    >
                        Посмотреть инструкцию
                    </Button>
                </Grid>
            </Grid>
        </>
    )
}