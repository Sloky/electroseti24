import React from "react";
import PageLayout from "../../components/Layouts/PageLayout/PageLayout";
import UserProfileCard from "./UserProfileCard";

export default function ProfilePage() {
    return (
        <PageLayout>
            <UserProfileCard/>
        </PageLayout>
    )
}