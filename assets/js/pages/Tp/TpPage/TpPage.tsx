import React, {useEffect, useState} from "react";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import TpCard from "./TpCard";
import {AppAction} from "../../../store/actions/actions";
import {useDispatch} from "react-redux";
import {
    getTransformerCardDataRequest,
    getTransformerCountersDataRequest,
    uploadCounterListRequest
} from "../../../requests/Request";
import {useParams} from "react-router";
import FlashComponent from "../../../components/Flashes/FlashComponent";
import {Box, Grid} from "@material-ui/core";

export default function TpPage() {
    const [transformerData, setTransformerData] = useState(null);
    const [countersData, setCountersData] = useState(null);

    const [transformerLoaded, setTransformerLoaded] = useState(false);
    const [countersLoaded, setCountersLoaded] = useState(false);

    const [errorsCollection, setErrorsCollection] = useState([]);
    const [flashError, setFlashError] = useState({status: false, message: undefined, messages: []});
    const [flashSuccess, setFlashSuccess] = useState({status: false, message: ''});
    const [flashInfo, setFlashInfo] = useState({status: false, message: ''});
    const [flashWarning, setFlashWarning] = useState({status: false, message: ''});

    const dispatch = useDispatch();
    let params: any = useParams();
    let id = params.id;

    const breadcrumbsItems = [
        {
            itemTitle: 'Объекты сети',
            itemSrc: '/network',
        },
        {
            itemTitle: 'Карточка ТП',
            // itemSrc: ``,
        },
    ]

    const onCloseErrorFlash = () => {
        setFlashError(prevState => {return {...prevState, status: false}})
    };

    const onCloseSuccessFlash = () => {
        setFlashSuccess(prevState => {return {...prevState, status: false}})
    };

    const onCloseInfoFlash = () => {
        setFlashInfo(prevState => {return {...prevState, status: false}})
    };

    const onCloseWarningFlash = () => {
        setFlashWarning(prevState => {return {...prevState, status: false}})
    };

    const makeErrorsCollection = (errors) => {
        let errorsCollection = {};
        errors.map(error => {
            if (errorsCollection[error.title]) {
                errorsCollection[error.title][errorsCollection[error.title].length] = [error.message];
            } else {
                errorsCollection[error.title] = [error.message];
            }
        });
        let entries = Object.entries(errorsCollection);
        return entries;
    }

    const onCounterListLoad = (file) => {
        dispatch(AppAction.showLoader());
        setFlashError({messages: [], message: undefined, status: false});
        setFlashSuccess({message: undefined, status: false});
        setFlashWarning({message: undefined, status: false});
        setFlashInfo({message: undefined, status: false});
        setErrorsCollection([]);
        uploadCounterListRequest(file, id).then(result => {
            if (result.type === 'success') {
                setCountersData(result.data);
                if (result.data.rowErrors.length !== 0) {
                    setErrorsCollection(makeErrorsCollection(result.data.rowErrors));
                } else {
                    setFlashSuccess({
                        status: true,
                        message: 'Список узлов учёта был успешно загружен',
                    });
                }
            } else {
                console.log('Error: ', result.data);
                setFlashError({
                    messages: [],
                    status: true,
                    message: result.data.message,
                });
            }
        }).finally(() => {
            dispatch(AppAction.hideLoader());
        })
    };

    const getCounters = (requestData) => {
        dispatch(AppAction.showLoader());
        setFlashError({messages: [], message: undefined, status: false});
        setFlashSuccess({message: undefined, status: false});
        setFlashWarning({message: undefined, status: false});
        setFlashInfo({message: undefined, status: false});
        setErrorsCollection([]);
        getTransformerCountersDataRequest(id, requestData).then(result => {
            if (result.type === 'success') {
                setCountersData(result.data);
            } else {
                setFlashError({status: true, message: 'Произошла ошибка. Обратитесь к администратору.', messages: []})
            }
        }).finally(() => {
            dispatch(AppAction.hideLoader());
        })
    }

    useEffect(() => {
        dispatch(AppAction.showLoader());
        getTransformerCardDataRequest(id).then(result => {
            if (result.type === 'success') {
                setTransformerData(result.data);
            }
        }).finally(() => {
            setTransformerLoaded(true);
        })
        getTransformerCountersDataRequest(id, {page: 1, maxResult: 100,}).then(result => {
            if (result.type === 'success') {
                setCountersData(result.data);
            }
        }).finally(() => {
            setCountersLoaded(true);
        })
    }, []);

    useEffect(() => {
        if (transformerLoaded && countersLoaded) {
            dispatch(AppAction.hideLoader());
        }
    }, [transformerLoaded, countersLoaded]);

    return (
        <PageLayout
            breadcrumbsItems={breadcrumbsItems}
        >
            <TpCard
                transformerData={transformerData}
                countersData={countersData && countersData.counters}
                countersCount={countersData && countersData.countersCount}
                onCounterListLoad={onCounterListLoad}
                getCounters={getCounters}
            >
                <Grid>
                    {
                        errorsCollection.map((item, index) =>
                            <FlashComponent
                                key={index}
                                messageHeading={item[0]}
                                message={item[1].map((item, index) =>
                                    <Box key={index}>{item}</Box>
                                )}
                                showFlash={!!errorsCollection.length}
                                flashType={'error'}
                                flashWidth={'100%'}
                            />
                        )
                    }
                    <FlashComponent
                        message={flashError.message}
                        messages={flashError.messages}
                        // onCloseFlash={onCloseErrorFlash}
                        showFlash={flashError.status}
                        flashType="error"
                        flashWidth="100%"
                    />
                    <FlashComponent
                        message={flashWarning.message}
                        onCloseFlash={onCloseWarningFlash}
                        showFlash={flashWarning.status}
                        flashType="warning"
                    />
                    <FlashComponent
                        message={flashInfo.message}
                        onCloseFlash={onCloseInfoFlash}
                        showFlash={flashInfo.status}
                        flashType="info"
                    />
                    <FlashComponent
                        message={flashSuccess.message}
                        onCloseFlash={onCloseSuccessFlash}
                        showFlash={flashSuccess.status}
                        flashType="success"
                    />
                </Grid>
            </TpCard>
        </PageLayout>
    )
}