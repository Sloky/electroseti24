import React, {useEffect, useState} from "react";
import {Button, Card, Grid, Pagination, Stack} from "@material-ui/core";
import {CounterPostData, TransformerCounterTableRequestData, TransformerPostData} from "../../../interfaces/interfaces";
import {THEME} from "../../../theme";
import {useHistory} from "react-router";
import TableItemsCustomizer from "../../Tasks/TasksTable/TableItemsCustomizer";
import TpTable from "../../../components/Tables/TpTable";
import SimpleLoadFileModal from "../../../components/Modal/SimpleLoadFileModal/SimpleLoadFileModal";
import SearchComponent from "../../../components/Other/SearchComponent/SearchComponent";
import {useSelector} from "react-redux";
import {appStateInterface} from "../../../store/appState";

interface TpCardProps {
    transformerData: TransformerPostData
    countersData: CounterPostData[]
    countersCount: number
    onCounterListLoad(file: any): void
    children?: any
    getCounters(requestData: TransformerCounterTableRequestData): void
}

const headerItems = [
    'Договор',
    'Наименование абонента',
    'Узел учёта',
    'Дата последних снятых показаний',
    'Показания',
];

export default function TpCard(props: TpCardProps) {
    const [transformerData, setTransformerData] = useState(props.transformerData);
    const [countersData, setCountersData] = useState(props.countersData);

    const [itemsCount, setItemsCount] = useState(100);
    const [pageNumber, setPageNumber] = useState<number>(1);
    const [totalItems, setTotalItems] = useState(props.countersCount);
    const [pagesCount, setPagesCount] = useState<number>(Math.ceil(totalItems/itemsCount || 1));
    const [searchValue, setSearchValue] = useState('');

    const history = useHistory();
    const currentUser = useSelector((state: appStateInterface) => state.user);

    const applyFilters = () => {
        const requestData = {
            page: pageNumber,
            maxResult: itemsCount,
            search: searchValue,
        }
        props.getCounters(requestData);
    }

    const onTableItemCountChange = (value: number) => {
        setItemsCount(value);
    };

    const onPageNumberChange = (value: number) => {
        setPageNumber(value);
    };

    const onSearchValueChange = (value: string) => {
        setSearchValue(value);
    };

    const onCounterListFileSubmit = (file) => {
        props.onCounterListLoad(file);
    }

    useEffect(() => {
        setTransformerData(props.transformerData);
        setTotalItems(props.countersCount);
        setCountersData(props.countersData);
    }, [props]);

    useEffect(() => {
        const requestData = {
            page: pageNumber,
            maxResult: itemsCount,
            search: searchValue,
        }
        props.getCounters(requestData);
    }, [pageNumber, itemsCount])

    useEffect(() => {
        setPagesCount(Math.ceil(totalItems/itemsCount || 1));
    }, [itemsCount, totalItems]);

    useEffect(() => {
        if (pageNumber > pagesCount) {
            setPageNumber(pagesCount);
        }
    }, [pagesCount]);

    return (
        <>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                Карточка ТП
            </Grid>
            {transformerData ?
                <>
                    <Card sx={{fontSize: '18px', fontWeight: '500', bgcolor: 'background.default', padding: '20px', marginBottom: '30px',}}>
                        <Grid sx={{color: 'text.primary', marginBottom: '15px',}}>
                            {transformerData.title}
                        </Grid>
                        <Grid container style={{marginBottom: '15px',}}>
                            <Grid item xs={3} sx={{color: THEME.PLACEHOLDER_TEXT,}}>Отделение</Grid>
                            <Grid item xs={9}>{transformerData.company ? transformerData.company.title : 'Нет данных'}</Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '15px',}}>
                            <Grid item xs={3} sx={{color: THEME.PLACEHOLDER_TEXT,}}>Подстанция</Grid>
                            <Grid item xs={9}>{transformerData.feeder && transformerData.feeder.substation ? transformerData.feeder.substation.title : 'Нет данных'}</Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '15px',}}>
                            <Grid item xs={3} sx={{color: THEME.PLACEHOLDER_TEXT,}}>Фидер</Grid>
                            <Grid item xs={9}>{transformerData.feeder ? transformerData.feeder.title : 'Нет данных'}</Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={3} sx={{fontWeight: '500', color: THEME.PLACEHOLDER_TEXT,}}>Контроллер</Grid>
                            <Grid item xs={9}>{transformerData.users && transformerData.users.length ? transformerData.users.map((user, index) => {
                                if (index === transformerData.users.length - 1) {
                                    return `${user.surname} ${user.name} ${user.patronymic}`
                                } else {
                                    return `${user.surname} ${user.name} ${user.patronymic}, `
                                }
                            }) : 'Нет данных'}</Grid>
                        </Grid>
                        <Grid item xs={2}>
                            <Button
                                variant={'outlined'}
                                size={'large'}
                                onClick={() => history.push(`/tp-edit/${transformerData.id}`)}
                                style={{fontSize: '12px',}}
                                fullWidth
                            >
                                Редактировать ТП
                            </Button>
                        </Grid>
                    </Card>

                </> : <></>
            }
            {currentUser.roles.includes('ROLE_SUPER_ADMIN') &&
                <Grid container style={{marginBottom: '30px',}}>
                    <Grid item xs={2}>
                        <SimpleLoadFileModal
                            modalName={'countersList'}
                            modalTitle={'Загрузить узлы учёта'}
                            startButtonTitle={'Загрузить узлы учёта'}
                            acceptButtonTitle={'Загрузить'}
                            startButtonVariant={'contained'}
                            onModalSubmit={onCounterListFileSubmit}
                            startButtonStyle={{width: '100%'}}
                            disableAcceptButton={true}
                        />
                    </Grid>
                </Grid>
            }
            {props.children}
            <Grid container style={{marginBottom: '30px',}}>
                <Grid item xs={6}>
                    <SearchComponent
                        searchValue={searchValue}
                        onSearchValueChange={onSearchValueChange}
                        applyFilters={applyFilters}
                        searchFieldXs={8}
                        buttonXs={4}
                    />
                </Grid>
            </Grid>
            <Grid style={{marginBottom: '20px',}}>
                <TableItemsCustomizer
                    initialCount={itemsCount}
                    allItems={totalItems}
                    onTableItemCountChange={onTableItemCountChange}
                />
            </Grid>
            <Grid>
                <TpTable
                    headerItems={headerItems}
                    tableDataRows={countersData}
                />
                <Stack spacing={2} style={{marginTop: '30px',}}>
                    <Pagination
                        count={pagesCount}
                        variant={'outlined'}
                        shape={'rounded'}
                        color={'primary'}
                        onChange={(event, page) => onPageNumberChange(page)}
                    />
                </Stack>
            </Grid>
        </>
    )
}