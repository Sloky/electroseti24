import React, {useEffect, useState} from "react";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import NewTpCard from "./NewTpCard";
import {useDispatch} from "react-redux";
import {getCompaniesForFilterRequest, getCompanyControllersDataRequest} from "../../../requests/Request";
import {AppAction} from "../../../store/actions/actions";

export default function NewTpPage() {
    const [companies, setCompanies] = useState([]);
    const [controllers, setControllers] = useState([]);

    const dispatch = useDispatch();

    const breadcrumbsItems = [
        {
            itemTitle: 'Объекты сети',
            itemSrc: '/network',
        },
        {
            itemTitle: 'Добавление ТП',
            // itemSrc: ``,
        },
    ]

    const getControllers = (companyId: string) => {
        dispatch(AppAction.showLoader());
        getCompanyControllersDataRequest(companyId).then(result => {
            if (result.type === 'success') {
                setControllers(result.data);
            }
        })
        dispatch(AppAction.hideLoader());
    }

    useEffect(() => {
        dispatch(AppAction.showLoader());
        getCompaniesForFilterRequest().then(result => {
            if (result.type === 'success') {
                setCompanies(result.data);
            }
        }).finally(() => {
            dispatch(AppAction.hideLoader());
        })
    }, [])

    return (
        <PageLayout
            breadcrumbsItems={breadcrumbsItems}
        >
            <NewTpCard
                companies={companies}
                controllers={controllers}
                getControllers={getControllers}
            />
        </PageLayout>
    )
}