import React, {useEffect, useState} from "react";
import {Box, Button, Grid, TextField} from "@material-ui/core";
import FlashComponent from "../../../components/Flashes/FlashComponent";
import {CompanyBranch, SimpleFlashData, UserData} from "../../../interfaces/interfaces";
import {Formik} from "formik";
import * as Yup from "yup";
import {useDispatch} from "react-redux";
import {AppAction} from "../../../store/actions/actions";
import {useHistory} from "react-router";
import {addNewTransformerRequest} from "../../../requests/Request";
import SimpleMultiSelectInput from "../../../components/FormInputs/MultiSelectInput/SimpleMultiSelectInput";
import SimpleMapModal from "../../../components/Modal/SimpleMapModal/SimpleMapModal";
import MapComponent from "../../../components/Other/MapComponent/MapComponent";
import SimpleSelectInput from "../../../components/FormInputs/SimpleSelectInput/SimpleSelectInput";

interface transformerFormInterface {
    transformer: string
    substation: string
    feeder: string
    executors: string[]
    coordinateX: number | null
    coordinateY: number | null
    company: string
}

interface NewTransformerCardProps {
    companies: CompanyBranch[]
    controllers: UserData[]
    getControllers(companyId: string): void
}

export default function NewTpCard(props: NewTransformerCardProps) {
    const [flashError, setFlashError] : [SimpleFlashData, any] = useState({status: false});
    const [flashSuccess, setFlashSuccess] : [SimpleFlashData, any] = useState({status: false});

    const [company, setCompany] = useState(null);

    const history = useHistory();
    const dispatch = useDispatch();

    const handleSubmit = (values) => {
        dispatch(AppAction.showLoader());
        addNewTransformerRequest(values).then(
            result => {
                if (result.type == 'success') {
                    history.goBack()
                } else {
                    if (result.type === 'error' && result.data.message.match(/Transformer with title = (\S+) already exists/).length) {
                        setFlashError({
                            status: true,
                            message: `Произошла ошибка. Трансформатор с именем "${result.data.message.match(/Transformer with title = (\S+) already exists/)[1]}" уже существует.`,
                        });
                    } else {
                        setFlashError({
                            status: true,
                            message: 'Произошла ошибка. Обратитесь к администратору.',
                        });
                    }
                }
            }
        ).finally(() => {
            dispatch(AppAction.hideLoader());
        });
    }

    const onCloseErrorFlash = () => {
        setFlashError({status: false});
    }

    const onCloseSuccessFlash = () => {
        setFlashSuccess({status: false});
    }

    useEffect(() => {
        if (company) {
            props.getControllers(company);
        }
    }, [company]);

    const initialValues: transformerFormInterface = {
        transformer: '',
        substation: '',
        feeder: '',
        executors: [],
        coordinateX: null,
        coordinateY: null,
        company: null,
    }

    const tpSchema = Yup.object().shape({
        transformer: Yup.string().required('Поле с названием трансформатора не может быть пустым'),
        substation: Yup.string().required('Поле с названием подстанции не может быть пустым'),
        feeder: Yup.string().required('Поле с названием фидера не может быть пустым'),
        coordinateX: Yup.number().nullable(),
        coordinateY: Yup.number().nullable(),
        executors: Yup.array(),
        company: Yup.string().required('Поле с отделением не может быть пустым'),
    })

    return (
        <>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                Добавление ТП
            </Grid>
            <FlashComponent
                messageHeading={flashError.messageHeading}
                message={flashError.message}
                showFlash={flashError.status}
                onCloseFlash={onCloseErrorFlash}
                flashType={'error'}
            />
            <FlashComponent
                message={flashSuccess.message}
                showFlash={flashSuccess.status}
                onCloseFlash={onCloseSuccessFlash}
                flashType={'success'}
            />
            <Formik
                initialValues={initialValues}
                validationSchema={tpSchema}
                onSubmit={(values) => handleSubmit(values)}
            >
                {({
                    handleChange,
                    handleSubmit,
                    handleBlur,
                    dirty,
                    isValid,
                    errors,
                    values,
                    touched,
                    setFieldValue,
                }) => (
                    <Box
                        component={'form'}
                    >
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={6}>
                                <Grid container style={{marginBottom: '30px',}}>
                                    <Grid item xs={8}>
                                        <TextField
                                            fullWidth
                                            size={'small'}
                                            id={'transformer'}
                                            placeholder={'Введите название трансформатора'}
                                            label={'Трансформатор (ТП)'}
                                            onChange={handleChange}
                                            error={errors.transformer && touched.transformer}
                                            helperText={touched.transformer && errors.transformer}
                                        />
                                    </Grid>
                                </Grid>
                                <Grid container style={{marginBottom: '30px',}}>
                                    <Grid item xs={8}>
                                        <TextField
                                            fullWidth
                                            size={'small'}
                                            id={'substation'}
                                            placeholder={'Введите название подстанции'}
                                            label={'Подстанция'}
                                            onChange={handleChange}
                                            error={errors.substation && touched.substation}
                                            helperText={touched.substation && errors.substation}
                                        />
                                    </Grid>
                                </Grid>
                                <Grid container style={{marginBottom: '30px',}}>
                                    <Grid item xs={8}>
                                        <TextField
                                            fullWidth
                                            size={'small'}
                                            id={'feeder'}
                                            placeholder={'Введите название фидера'}
                                            label={'Фидер'}
                                            onChange={handleChange}
                                            error={errors.feeder && touched.feeder}
                                            helperText={touched.feeder && errors.feeder}
                                        />
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item xs={6}>
                                <Grid container>
                                    <Grid item xs={8}>
                                        <Box sx={{color: 'secondary.main', fontSize: '18px', fontWeight: '500', marginBottom: '20px',}}>
                                            Координаты объекта
                                        </Box>
                                        <Box sx={{color: 'text.primary', fontSize: '18px', marginBottom: '30px',}}>
                                            {values.coordinateX && values.coordinateY
                                                ? '\[' + values.coordinateX + ', ' + values.coordinateY + '\]'
                                                : 'Нет данных'}
                                        </Box>
                                        {values.coordinateX && values.coordinateY  &&
                                            <MapComponent
                                                coordX={values.coordinateX}
                                                coordY={values.coordinateY}
                                                draggable={false}
                                                mapWidth={'100%'}
                                                mapHeight={'20vh'}
                                                enableSearch={false}
                                            />
                                        }
                                    </Grid>
                                    <Grid item xs={4}>
                                        <SimpleMapModal
                                            coordX={values.coordinateX}
                                            coordY={values.coordinateY}
                                            modalName={'transformerCoordinates'}
                                            modalTitle={'GPS координаты трансформаторной подстанции'}
                                            startButtonTitle={'Изменить'}
                                            startButtonStyle={{width: '100%',}}
                                            // acceptButtonShow={true}
                                            onModalSubmit={(e) => {
                                                setFieldValue('coordinateX', e.x);
                                                setFieldValue('coordinateY', e.y);
                                            }}
                                            enableSearch={true}
                                            // disableStartButton={!task.new_coordinate_x && !task.new_coordinate_y}
                                        />
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={6} style={{paddingRight: '15px',}}>
                                <SimpleSelectInput
                                    label={'Отделение'}
                                    allowEmptyValue={true}
                                    emptyValueTitle={'Выберите отделение'}
                                    defaultValue={values.company}
                                    options={props.companies.map(company => ({
                                        title: company.title,
                                        value: company.id,
                                    }))}
                                    formControlStyle={{width: '100%',}}
                                    onChange={value => {
                                        setFieldValue('company', value);
                                        setCompany(value)
                                    }}
                                    error={!values.company && touched.company}
                                />
                            </Grid>
                            <Grid item xs={6} style={{paddingRight: '15px',}}>
                                <SimpleMultiSelectInput
                                    id={'executors'}
                                    labelTitle={'Контроллеры'}
                                    defaultValues={values.executors}
                                    options={props.controllers.map(controller => {
                                        return {
                                            title: `${controller.surname} ${controller.name} ${controller.patronymic}`,
                                            value: controller.id
                                        };
                                    })}
                                    selectInputStyle={{width: '100%',}}
                                    onChange={(e) => {
                                        setFieldValue('executors', e.target.value)
                                    }}
                                    error={errors.executors && touched.executors}
                                    disabled={!values.company}
                                />
                            </Grid>
                        </Grid>
                        <Grid container>
                            <Grid item xs={2} style={{paddingRight: '15px',}}>
                                <Button
                                    variant={'outlined'}
                                    fullWidth
                                    onClick={() => history.goBack()}
                                    className={'base_styled_btn'}
                                >
                                    Отменить
                                </Button>
                            </Grid>
                            <Grid item xs={2} style={{paddingRight: '15px',}}>
                                <Button
                                    variant={'contained'}
                                    fullWidth
                                    onClick={() => handleSubmit()}
                                    className={'base_styled_btn'}
                                >
                                    Создать
                                </Button>
                            </Grid>
                        </Grid>
                    </Box>
                )}
            </Formik>
        </>
    )
}