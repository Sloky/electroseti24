import React, {useEffect, useState} from "react";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import TpEditCard from "./TpEditCard";
import {useDispatch, useSelector} from "react-redux";
import {AppAction} from "../../../store/actions/actions";
import {useHistory, useParams} from "react-router";
import FlashComponent from "../../../components/Flashes/FlashComponent";
import {SimpleFlashData} from "../../../interfaces/interfaces";
import {
    deleteTransformerRequest,
    editTransformerRequest, getCompaniesForFilterRequest,
    getCompanyControllersDataRequest,
    getTransformerCardDataRequest
} from "../../../requests/Request";
import {appStateInterface} from "../../../store/appState";

export default function TpEditPage() {
    const [transformerData, setTransformerData] = useState(null);
    const [companies, setCompanies] = useState([]);
    const [controllers, setControllers] = useState([]);

    const [transformerLoaded, setTransformerLoaded] = useState(false);
    const [companiesLoaded, setCompaniesLoaded] = useState(false);
    const [controllersLoaded, setControllersLoaded] = useState(false);

    const [flashError, setFlashError] = useState<SimpleFlashData>({status: false})
    const [flashSuccess, setFlashSuccess] = useState({status: false, message: ''});

    const dispatch = useDispatch();
    const history = useHistory();

    let params: any = useParams();
    let id = params.id;

    const breadcrumbsItems = [
        {
            itemTitle: 'Объекты сети',
            itemSrc: '/network',
        },
        {
            itemTitle: 'Карточка ТП',
            itemSrc: `/tp-page/${id}`,
        },
        {
            itemTitle: 'Редактирование ТП',
            // itemSrc: ``,
        },
    ]

    const onCloseErrorFlash = () => {
        setFlashError(prevState => {return {...prevState, status: false}})
    };

    const onCloseSuccessFlash = () => {
        setFlashSuccess(prevState => {return {...prevState, status: false}})
    };

    const showFlash = (type, message, messageHeading) => {
        switch (type) {
            case 'error':
                setFlashError({status: true, messageHeading: messageHeading, message: message});
                break;
            case 'success':
                setFlashSuccess({status: true, message: message});
                break;
            case 'disableFlashes':
                setFlashError({status: false,});
                setFlashSuccess({status: false, message: message});
        }
    }

    useEffect(() => {
        dispatch(AppAction.showLoader());
        getTransformerCardDataRequest(id).then(
            result => {
                if (result.type == 'success') {
                    setTransformerData(result.data);
                } else {
                    console.log('Error: ', result.data);
                    showFlash('error', 'Обратитесь к администратору', 'Ошибка.');
                }
            }
        ).finally(() => {
            setTransformerLoaded(true);
        })
        getCompaniesForFilterRequest().then(result => {
            if (result.type === 'success') {
                setCompanies(result.data);
            }
        }).finally(() => {
            setCompaniesLoaded(true);
        })
    }, []);

    useEffect(() => {
        if (transformerLoaded) {
            getCompanyControllersDataRequest(transformerData.company.id).then(
                result => {
                    if (result.type === 'success') {
                        setControllers(result.data);
                    } else {
                        console.log('Error: ', result.data);
                        showFlash('error', 'Обратитесь к администратору', 'Ошибка.');
                    }
                }
            ).finally(() => {
                setControllersLoaded(true);
            })
        }
    }, [transformerLoaded]);

    useEffect(() => {
        if (transformerLoaded && controllersLoaded && companiesLoaded) {
            dispatch(AppAction.hideLoader());
        }
    }, [transformerLoaded, controllersLoaded, companiesLoaded]);

    const getControllers = (companyId: string) => {
        dispatch(AppAction.showLoader());
        getCompanyControllersDataRequest(companyId).then(result => {
            if (result.type === 'success') {
                setControllers(result.data);
            }
        })
        dispatch(AppAction.hideLoader());
    }

    const onSaveHandler = (values) => {
        dispatch(AppAction.showLoader());
        editTransformerRequest(id, values).then(
            result => {
                if (result.type === 'success') {
                    setTransformerData(result.data);
                    history.push(`/tp-page/${result.data.id}`);
                } else {
                    showFlash('error', 'Пожалуйста, обратитесь к администратору', 'Ошибка.')
                }
            }
        ).finally(() => {
            dispatch(AppAction.hideLoader());
        });
    }

    const onDeleteHandler = () => {
        dispatch(AppAction.showLoader());
        deleteTransformerRequest(id).then(
            result => {
                if (result.type === 'success') {
                    history.push('/network');
                } else {
                    showFlash('error', 'Не удалось удалить ТП. Пожалуйста, обратитесь к администратору', 'Ошибка.')
                }
            }
        ).finally(() => {
            dispatch(AppAction.hideLoader());
        })
    }

    const loading = useSelector((state: appStateInterface) => state.loading);

    return (
        <PageLayout
            breadcrumbsItems={breadcrumbsItems}
        >
            {!loading ?
                <TpEditCard
                    transformerData={transformerData}
                    companies={companies}
                    controllers={controllers}
                    onFlash={(type, message, messageHeading) => {showFlash(type, message, messageHeading)}}
                    onSaveHandler={onSaveHandler}
                    onDeleteHandler={onDeleteHandler}
                    getControllers={getControllers}
                >
                    <FlashComponent
                        message={flashError.message}
                        messageHeading={flashError.messageHeading}
                        showFlash={flashError.status}
                        flashType={'error'}
                    />
                    <FlashComponent
                        message={flashSuccess.message}
                        showFlash={flashSuccess.status}
                        onCloseFlash={onCloseSuccessFlash}
                        flashType={'success'}
                    />
                </TpEditCard> : <></>
            }
        </PageLayout>
    )
}