import React, {useEffect, useState} from "react";
import {Formik} from "formik";
import {Box, Button, Grid, TextField} from "@material-ui/core";
import {CompanyBranch, TransformerPostData, UserData} from "../../../interfaces/interfaces";
import * as Yup from "yup";
import SimpleModal from "../../../components/Modal/SimpleModal/SimpleModal";
import {useHistory} from "react-router";
import MapComponent from "../../../components/Other/MapComponent/MapComponent";
import SimpleMapModal from "../../../components/Modal/SimpleMapModal/SimpleMapModal";
import SimpleMultiSelectInput from "../../../components/FormInputs/MultiSelectInput/SimpleMultiSelectInput";
import {useSelector} from "react-redux";
import {appStateInterface} from "../../../store/appState";
import BackButtonComponent from "../../../components/Other/BackButtonComponent/BackButtonComponent";
import SimpleSelectInput from "../../../components/FormInputs/SimpleSelectInput/SimpleSelectInput";

interface transformerEditCardProps {
    transformerData: TransformerPostData
    controllers: UserData[]
    companies: CompanyBranch[]
    children?: any
    onFlash(type: string, message: string, messageHeading?: string): void
    onSaveHandler?(data: TransformerPostData): void
    onDeleteHandler?(): void
    getControllers(companyId: string): void
}

export default function TpEditCard(props: transformerEditCardProps) {
    const [initialValues, setInitialValues] = useState({
        transformer: props.transformerData?.title || '',
        substation: props.transformerData?.feeder?.substation?.title || '',
        feeder: props.transformerData?.feeder?.title || '',
        executors: props.transformerData?.users?.map(user => user.id) || [],
        coordinateX: props.transformerData && props.transformerData.coords ? props.transformerData.coords[0] : '',
        coordinateY: props.transformerData && props.transformerData.coords ? props.transformerData.coords[1] : '',
        company: props.transformerData?.company?.id || '',
    });
    const [transformerStateStatus, setTransformerStateStatus] = useState(true);

    const [company, setCompany] = useState(null);

    const history = useHistory();
    const currentUser = useSelector((state: appStateInterface) => state.user);

    const saveTransformerHandler = (values) => {
        props.onSaveHandler(values);
    };

    useEffect(() => {
        setInitialValues({
            transformer: props.transformerData?.title || '',
            substation: props.transformerData?.feeder?.substation?.title || '',
            feeder: props.transformerData?.feeder?.title || '',
            executors: props.transformerData?.users?.map(user => user.id) || [],
            coordinateX: props.transformerData && props.transformerData.coords ? props.transformerData.coords[0] : '',
            coordinateY: props.transformerData && props.transformerData.coords ? props.transformerData.coords[1] : '',
            company: props.transformerData?.company?.id || '',
        });
    }, [props]);

    useEffect(() => {
        if (company) {
            props.getControllers(company);
        }
    }, [company]);

    const deleteTransformerHandler = () => {
        props.onDeleteHandler();
    }

    const transformerSchema = Yup.object().shape({
        transformer: Yup.string().required('Поле с названием трансформатора не может быть пустым'),
        substation: Yup.string(),
        feeder: Yup.string(),
    })

    return (
        <>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                Редактирование ТП
            </Grid>
            <Grid style={{marginBottom: '30px', display: 'flex', justifyContent: 'space-between',}}>
                <BackButtonComponent changed={!transformerStateStatus}/>
                {currentUser.roles.includes('ROLE_SUPER_ADMIN') &&
                    <SimpleModal
                        startButtonTitle={'Удалить ТП'}
                        modalName={'deleteTpModal'}
                        startButtonVariant={'text'}
                        closeButtonTitle={'Отменить'}
                        modalTitle={'Удаление ТП'}
                        acceptButtonTitle={'Подтвердить'}
                        onModalSubmit={deleteTransformerHandler}
                    >
                        Удалить ТП?
                    </SimpleModal>
                }
            </Grid>
            {props.children}
            <Formik
                initialValues={initialValues}
                validationSchema={transformerSchema}
                onSubmit={(values) => saveTransformerHandler(values)}
            >
                {({
                      handleChange,
                      handleSubmit,
                      handleBlur,
                      dirty,
                      isValid,
                      errors,
                      values,
                      touched,
                      setFieldValue,
                      handleReset,
                }) => (
                    <Box
                        component={'form'}
                    >
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={6}>
                                <Grid container style={{marginBottom: '30px',}}>
                                    <Grid item xs={8}>
                                        <TextField
                                            fullWidth
                                            size={'small'}
                                            id={'transformer'}
                                            value={values.transformer}
                                            placeholder={'Введите название трансформатора'}
                                            label={'Трансформатор (ТП)'}
                                            onChange={(e) => {
                                                e.persist();
                                                setFieldValue('transformer', e.target.value);
                                                setTransformerStateStatus(false);
                                            }}
                                            error={errors.transformer && touched.transformer}
                                            helperText={touched.transformer && errors.transformer}
                                        />
                                    </Grid>
                                </Grid>
                                <Grid container style={{marginBottom: '30px',}}>
                                    <Grid item xs={8}>
                                        <TextField
                                            fullWidth
                                            size={'small'}
                                            id={'substation'}
                                            value={values.substation}
                                            placeholder={'Введите название подстанции'}
                                            label={'Подстанция'}
                                            onChange={(e) => {
                                                e.persist();
                                                setFieldValue('substation', e.target.value);
                                                setTransformerStateStatus(false);
                                            }}
                                            error={errors.substation && touched.substation}
                                            helperText={touched.substation && errors.substation}
                                        />
                                    </Grid>
                                </Grid>
                                <Grid container style={{marginBottom: '30px',}}>
                                    <Grid item xs={8}>
                                        <TextField
                                            fullWidth
                                            size={'small'}
                                            id={'feeder'}
                                            value={values.feeder}
                                            placeholder={'Введите название фидера'}
                                            label={'Фидер'}
                                            onChange={(e) => {
                                                e.persist();
                                                setFieldValue('feeder', e.target.value);
                                                setTransformerStateStatus(false);
                                            }}
                                            // error={errors.feeder && touched.feeder}
                                            // helperText={touched.feeder && errors.feeder}
                                        />
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item xs={6}>
                                <Grid container>
                                    <Grid item xs={8}>
                                        <Box sx={{color: 'secondary.main', fontSize: '18px', fontWeight: '500', marginBottom: '20px',}}>
                                            Координаты объекта
                                        </Box>
                                        <Box sx={{color: 'text.primary', fontSize: '18px', marginBottom: '30px',}}>
                                            {values.coordinateX && values.coordinateY
                                                ? `[${values.coordinateX}, ${values.coordinateY}]`
                                                : 'Нет данных'
                                            }
                                        </Box>
                                        {values.coordinateX && values.coordinateY &&
                                            <MapComponent
                                                coordX={+values.coordinateX}
                                                coordY={+values.coordinateY}
                                                draggable={false}
                                                mapWidth={'100%'}
                                                mapHeight={'20vh'}
                                                enableSearch={false}
                                            />
                                        }
                                    </Grid>
                                    <Grid item xs={4}>
                                        <SimpleMapModal
                                            coordX={+values.coordinateX}
                                            coordY={+values.coordinateY}
                                            modalName={'transformerCoordinates'}
                                            modalTitle={'GPS координаты трансформаторной подстанции'}
                                            startButtonTitle={'Посмотреть'}
                                            startButtonStyle={{width: '100%',}}
                                            // acceptButtonShow={true}
                                            onModalSubmit={(e) => {
                                                setFieldValue('coordinateX', e.x);
                                                setFieldValue('coordinateY', e.y);
                                                setTransformerStateStatus(false);
                                            }}
                                            enableSearch={true}
                                            // disableStartButton={!task.new_coordinate_x && !task.new_coordinate_y}
                                        />
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={6} style={{paddingRight: '15px',}}>
                                <SimpleSelectInput
                                    label={'Отделение'}
                                    defaultValue={values.company}
                                    options={props.companies.map(company => ({
                                        title: company.title,
                                        value: company.id,
                                    }))}
                                    formControlStyle={{width: '100%',}}
                                    onChange={value => {
                                        setTransformerStateStatus(false);
                                        setFieldValue('company', value);
                                        setFieldValue('executors', []);
                                        setCompany(value);
                                    }}
                                    error={!values.company && touched.company}
                                />
                            </Grid>
                            <Grid item xs={6} style={{paddingRight: '15px',}}>
                                <SimpleMultiSelectInput
                                    id={'executors'}
                                    labelTitle={'Контроллеры'}
                                    defaultValues={values.executors.map(executor => executor)}
                                    options={props.controllers.map(controller => {
                                        return {
                                            title: `${controller.surname} ${controller.name} ${controller.patronymic}`,
                                            value: controller.id
                                        };
                                    })}
                                    selectInputStyle={{width: '100%',}}
                                    onChange={(e) => {
                                        setInitialValues(prevState => ({...prevState, executors: e.target.value}))
                                        setFieldValue('executors', e.target.value);
                                        setTransformerStateStatus(false);
                                    }}
                                    // error={errors.executors && touched.executors}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={2} style={{paddingRight: '15px',}}>
                                <Button
                                    variant={'outlined'}
                                    fullWidth
                                    onClick={() => {
                                        handleReset();
                                        setTransformerStateStatus(true);
                                    }}
                                    className={'base_styled_btn'}
                                >
                                    Отменить изменения
                                </Button>
                            </Grid>
                            <Grid item xs={2} style={{paddingRight: '15px',}}>
                                <Button
                                    variant={'contained'}
                                    fullWidth
                                    onClick={() => handleSubmit()}
                                    className={'base_styled_btn'}
                                    disabled={transformerStateStatus}
                                >
                                    Сохранить
                                </Button>
                            </Grid>
                        </Grid>
                    </Box>
                )}
            </Formik>
            <BackButtonComponent changed={!transformerStateStatus}/>
        </>
    )
}