import React, {useEffect, useState} from "react";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import {useDispatch, useSelector} from "react-redux";
import {useHistory, useParams} from "react-router";
import {AppAction} from "../../../store/actions/actions";
import {CounterPostData, SimpleFlashData} from "../../../interfaces/interfaces";
import {
    deleteTransformerCounterRequest,
    editTransformerCounterRequest,
    getTransformerCounterRequest
} from "../../../requests/Request";
import FlashComponent from "../../../components/Flashes/FlashComponent";
import TpCounterCard from "./TpCounterCard";
import {appStateInterface} from "../../../store/appState";
import {Grid} from "@material-ui/core";

export default function TpCounterPage() {
    const [counter, setCounter] = useState<CounterPostData>({
        agreement_number: '',
        antimagnetic_seal: '',
        coordinates: [],
        counter_id: '',
        counter_model: '',
        counter_physical_status: 0,
        counter_title: '',
        initial_counter_value: null,
        initial_counter_value_date: '',
        last_counter_value: null,
        last_counter_value_date: '',
        lodgers_count: '',
        phones: [],
        rooms_count: '',
        side_seal: '',
        subscriber_title: '',
        terminal_seal: '',
    });

    const [flashError, setFlashError] = useState<SimpleFlashData>({status: false});
    const [flashWarning, setFlashWarning] = useState({status: false, message: ''});
    const [flashInfo, setFlashInfo] = useState({status: false, message: ''});
    const [flashSuccess, setFlashSuccess] = useState({status: false, message: ''});

    const dispatch = useDispatch();
    const history = useHistory();
    let params: any = useParams();
    let id = params.id;

    const breadcrumbsItems = [
        {
            itemTitle: 'Объекты сети',
            itemSrc: '/network',
        },
        {
            itemTitle: 'Карточка ТП',
            itemSrc: `/tp-page/${counter.transformer ? counter.transformer.id : ''}`,
        },
        {
            itemTitle: 'Узел учёта',
            // itemSrc: ``,
        },
    ]

    const loading = useSelector((state: appStateInterface) => state.loading);

    const onCloseErrorFlash = () => {
        setFlashError(prevState => {return {...prevState, status: false}})
    };

    const onCloseWarningFlash = () => {
        setFlashWarning(prevState => {return {...prevState, status: false}})
    };

    const onCloseInfoFlash = () => {
        setFlashInfo(prevState => {return {...prevState, status: false}})
    };

    const onCloseSuccessFlash = () => {
        setFlashSuccess(prevState => {return {...prevState, status: false}})
    };

    useEffect(() => {
        dispatch(AppAction.showLoader());
        getTransformerCounterRequest(id).then(
            result => {
                if (result.type === 'success') {
                    setCounter(result.data.counterCardData);
                } else {
                    console.log('Error: ', result.data);
                    showFlash('error', result.data);
                }
            }
        ).finally(() => {
            dispatch(AppAction.hideLoader());
        })
    }, []);

    const showFlash = (type, message, messageHeading?) => {
        switch (type) {
            case 'error':
                setFlashError({status: true, messageHeading: messageHeading, message: message});
                break;
            case 'warning':
                setFlashWarning({status: true, message: message});
                break;
            case 'info':
                setFlashInfo({status: true, message: message});
                break;
            case 'success':
                setFlashSuccess({status: true, message: message});
                break;
            case 'disableFlashes':
                setFlashSuccess({status: false, message: message,});
                setFlashError({status: false,});
                setFlashInfo({status: false, message: message,});
                setFlashWarning({status: false, message: message,});
        }
    }

    const saveCounter = (data) => {
        dispatch(AppAction.showLoader());
        editTransformerCounterRequest(id, data).then(
            result => {
                if (result.type === 'success') {
                    setCounter(result.data.counterCardData);
                    showFlash('disableFlashes', '');
                    showFlash('success', 'Запись была успешно изменена')
                } else {
                    console.log('Error task data: ', result.data);
                    showFlash('error', 'Не удалось записать изменения')
                }
            }
        ).finally(() => {
            dispatch(AppAction.hideLoader());
        });
    }

    const deleteCounter = () => {
        dispatch(AppAction.showLoader());
        deleteTransformerCounterRequest(id).then(
            result => {
                if (result.type === 'success') {
                    history.goBack();
                } else {
                    showFlash('error', 'Не удалось удалить. Пожалуйста, обратитесь к администратору', 'Ошибка.');
                }
            }
        ).finally(() => {
            dispatch(AppAction.hideLoader());
        })
    }

    return (
        <PageLayout
            breadcrumbsItems={breadcrumbsItems}
        >
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                {`Узел учёта ${counter ? counter.counter_title : ''}`}
            </Grid>
            {!loading ?
                <TpCounterCard
                    counter={counter}
                    onSaveCounter={saveCounter}
                    onDeleteCounter={deleteCounter}
                >
                    <FlashComponent
                        message={flashError.message}
                        messageHeading={flashError.messageHeading}
                        // onCloseFlash={onCloseErrorFlash}
                        showFlash={flashError.status}
                        flashType={'error'}
                        flashWidth={'100%'}
                    />
                    <FlashComponent
                        message={flashWarning.message}
                        onCloseFlash={onCloseWarningFlash}
                        showFlash={flashWarning.status}
                        flashType={'warning'}
                    />
                    <FlashComponent
                        message={flashInfo.message}
                        onCloseFlash={onCloseInfoFlash}
                        showFlash={flashInfo.status}
                        flashType={'info'}
                    />
                    <FlashComponent
                        message={flashSuccess.message}
                        showFlash={flashSuccess.status}
                        onCloseFlash={onCloseSuccessFlash}
                        flashType={'success'}
                    />
                </TpCounterCard> : <></>
            }
        </PageLayout>
    )
}