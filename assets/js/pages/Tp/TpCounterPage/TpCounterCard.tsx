import React, {useEffect, useState} from "react";
import {Box, Button, Grid} from "@material-ui/core";
import SimpleModal from "../../../components/Modal/SimpleModal/SimpleModal";
import {useHistory} from "react-router";
import {Formik} from "formik";
import * as Yup from "yup";
import SimpleTextInput from "../../../components/FormInputs/SimpleTextInput/SimpleTextInput";
import SimpleSelectInput from "../../../components/FormInputs/SimpleSelectInput/SimpleSelectInput";
import ChangeAddressModal from "../../../components/Modal/ChangeAddressModal/ChangeAddressModal";
import SimpleMapModal from "../../../components/Modal/SimpleMapModal/SimpleMapModal";
import MapComponent from "../../../components/Other/MapComponent/MapComponent";
import {CounterPostData} from "../../../interfaces/interfaces";
import BasicDateTimePicker from "../../../components/DatePicker/BasicDateTimePicker";
import PhoneComponent from "../../../components/Other/PhoneComponent/PhoneComponent";
import {useSelector} from "react-redux";
import {appStateInterface} from "../../../store/appState";
import BackButtonComponent from "../../../components/Other/BackButtonComponent/BackButtonComponent";

interface TpCounterCardProps {
    children?: any
    counter?: any
    onSaveCounter(data: CounterPostData): void
    onDeleteCounter(): void
}

const selectServiceability = [
    {title: 'Доступен/Исправен', value: 'Доступен/Исправен'},
    {title: 'Недоступен/Неисправен', value: 'Недоступен/Неисправен'},
];

const transformFormData = (data) => {
    return {
        transformer: data.transformer ? data.transformer.id : '',
        counterTitle: data.counter_title,
        counterPhysicalStatus: data.counter_physical_status,
        counterModel: data.counter_model,
        terminalSeal: data.terminal_seal,
        antimagneticSeal: data.antimagnetic_seal,
        sideSeal: data.side_seal,
        address: data.address || '',
        coordinateX: data.coordinates && data.coordinates.length > 1 ? data.coordinates[0] : null,
        coordinateY: data.coordinates && data.coordinates.length > 1 ? data.coordinates[1] : null,
        initialCounterValueId: data.initial_counter_value_id,
        initialCounterValue: data.initial_counter_value,
        initialCounterValueDate: data.initial_counter_value_date,
        lastCounterValueId: data.last_counter_value_id,
        lastCounterValue: data.last_counter_value,
        lastCounterValueDate: data.last_counter_value_date,
        subscriberTitle: data.subscriber_title,
        agreementNumber: data.agreement_number,
        lodgersCount: data.lodgers_count,
        roomsCount: data.rooms_count,
        phones: data.phones,
    }
}

export default function TpCounterCard(props: TpCounterCardProps) {
    const [counter, setCounter] = useState(transformFormData(props.counter));
    const [changed, setChanged] = useState(false);

    const history = useHistory();
    const currentUser = useSelector((state: appStateInterface) => state.user);

    useEffect(() => {
        setCounter(transformFormData(props.counter));
    }, [props]);

    const saveTransformerCounterChanges = (values) => {
        setChanged(false);
        props.onSaveCounter(values);
    };

    const deleteTpCounterHandler = () => {
        props.onDeleteCounter();
    }

    const transformerCounterSchema = Yup.object().shape({
        agreement_number: Yup.string(),
        antimagnetic_seal: Yup.string(),
        coordinateX: Yup.number(),
        coordinateY: Yup.number(),
        counter_id: Yup.string(),
        counter_model: Yup.string(),
        counter_physical_status: Yup.string(),
        counter_title: Yup.string(),
        initial_counter_value: Yup.string(),
        initial_counter_value_date: Yup.string(),
        last_counter_value: Yup.string(),
        last_counter_value_date: Yup.string(),
        lodgers_count: Yup.string(),
        phones: Yup.array(),
        rooms_count: Yup.string(),
        side_seal: Yup.string(),
        subscriber_title: Yup.string(),
        terminal_seal: Yup.string(),
    })

    const changeServiceabilityValue = (value) => value === 'Доступен/Исправен' ? 0 : 1

    return (
        <>
            <Grid style={{marginBottom: '30px', display: 'flex', justifyContent: 'space-between',}}>
                <BackButtonComponent changed={changed}/>
                {currentUser.roles.includes('ROLE_SUPER_ADMIN') &&
                    <SimpleModal
                        startButtonTitle={'Удалить узел учёта'}
                        modalName={'deleteTpCounterModal'}
                        startButtonVariant={'text'}
                        closeButtonTitle={'Отменить'}
                        modalTitle={'Удаление узла учёта'}
                        acceptButtonTitle={'Подтвердить'}
                        onModalSubmit={deleteTpCounterHandler}
                    >
                        Удалить узел учёта?
                    </SimpleModal>
                }
            </Grid>
            {props.children}
            <Formik
                initialValues={counter}
                validationSchema={transformerCounterSchema}
                onSubmit={(values) => saveTransformerCounterChanges(values)}
            >
                {({
                    handleChange,
                    handleSubmit,
                    handleBlur,
                    dirty,
                    isValid,
                    errors,
                    values,
                    touched,
                    setFieldValue,
                    handleReset,
                    validateForm,
                }) => (
                    <Box component={'form'}>
                        <Box sx={{
                            marginBottom: '30px',
                            padding: '30px 20px',
                            bgcolor: 'background.default',
                            borderRadius: '10px',
                            boxShadow: '0px 2px 5px 0px rgba(138, 193, 233, 0.2)',
                        }}>
                            <Grid sx={{fontSize: '18px', fontWeight: '500', color: 'text.primary', marginBottom: '30px',}}>
                                Данные узла учёта
                            </Grid>
                            <Grid container style={{marginBottom: '30px',}}>
                                <Grid item xs={4} style={{paddingRight: '15px',}}>
                                    <SimpleTextInput
                                        name={'counterTitle'}
                                        label={'Номер счётчика'}
                                        defaultValue={values.counterTitle}
                                        onBlur={(value) => {
                                            setFieldValue('counterTitle', value);
                                            setChanged(true);
                                        }}
                                        onChange={() => {}}
                                        id={'counterTitle'}
                                    />
                                </Grid>
                            </Grid>
                            <Grid container style={{marginBottom: '30px',}}>
                                <Grid item xs={4} style={{paddingRight: '15px',}}>
                                    <SimpleSelectInput
                                        label={'Состояние / Доступ узла учёта'}
                                        options={selectServiceability}
                                        defaultValue={values.counterPhysicalStatus >= 0 && selectServiceability[values.counterPhysicalStatus].value}
                                        onChange={(value) => {
                                            setFieldValue('counterPhysicalStatus', changeServiceabilityValue(value));
                                            setChanged(true);
                                        }}
                                        id={'counterPhysicalStatus'}
                                        formControlStyle={{width: '100%',}}
                                    />
                                </Grid>
                            </Grid>
                            <Grid container style={{marginBottom: '30px',}}>
                                <Grid item xs={4} style={{paddingRight: '15px',}}>
                                    <SimpleTextInput
                                        name={'counterModel'}
                                        label={'Модель узла учёта'}
                                        defaultValue={values.counterModel}
                                        onBlur={(value) => {
                                            setFieldValue('counterModel', value);
                                            setChanged(true);
                                        }}
                                        onChange={() => {}}
                                        id={'counterModel'}
                                    />
                                </Grid>
                            </Grid>
                            <Grid container style={{marginBottom: '30px',}}>
                                <Grid item xs={4} style={{paddingRight: '15px',}}>
                                    <SimpleTextInput
                                        name={'terminalSeal'}
                                        label={'Клеммная пломба'}
                                        defaultValue={values.terminalSeal}
                                        onBlur={(value) => {
                                            setFieldValue('terminalSeal', value);
                                            setChanged(true);
                                        }}
                                        onChange={() => {}}
                                        id={'terminalSeal'}
                                    />
                                </Grid>
                            </Grid>
                            <Grid container style={{marginBottom: '30px',}}>
                                <Grid item xs={4} style={{paddingRight: '15px',}}>
                                    <SimpleTextInput
                                        name={'antimagneticSeal'}
                                        label={'Антимагнитная пломба'}
                                        defaultValue={values.antimagneticSeal}
                                        onBlur={(value) => {
                                            setFieldValue('antimagneticSeal', value);
                                            setChanged(true);
                                        }}
                                        onChange={() => {}}
                                        id={'antimagneticSeal'}
                                    />
                                </Grid>
                            </Grid>
                            <Grid container>
                                <Grid item xs={4} style={{paddingRight: '15px',}}>
                                    <SimpleTextInput
                                        name={'sideSeal'}
                                        label={'Боковая пломба'}
                                        defaultValue={values.sideSeal}
                                        onBlur={(value) => {
                                            setFieldValue('sideSeal', value);
                                            setChanged(true);
                                        }}
                                        onChange={() => {}}
                                        id={'sideSeal'}
                                    />
                                </Grid>
                            </Grid>
                        </Box>
                        <Box sx={{
                            marginBottom: '30px',
                            padding: '30px 20px',
                            bgcolor: 'background.default',
                            borderRadius: '10px',
                            boxShadow: '0px 2px 5px 0px rgba(138, 193, 233, 0.2)',
                        }}>
                            <Grid container style={{marginBottom: '30px',}}>
                                <Grid item xs={4} style={{fontSize: '18px',}}>
                                    <Grid sx={{fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                                        Адрес
                                    </Grid>
                                    <Grid sx={{color: 'text.primary',}}>
                                        {values.address || 'Нет данных'}
                                    </Grid>
                                </Grid>
                                <Grid item xs={2}>
                                    <ChangeAddressModal
                                        modalName={'address'}
                                        defaultLocality={''}
                                        defaultStreet={''}
                                        defaultHouse={''}
                                        defaultPorch={''}
                                        defaultApartment={''}
                                        modalTitle={'Редактировать адрес'}
                                        startButtonStyle={{width: '100%',}}
                                        onModalSubmit={(address) => {
                                            setFieldValue('address', `${address.locality}, ${address.street}, ${address.house}, ${address.apartment}`)
                                        }}
                                        fullAddress={values.address}
                                    />
                                </Grid>
                            </Grid>
                            <Grid container style={{marginBottom: '30px',}}>
                                <Grid item xs={4} style={{fontSize: '18px',}}>
                                    <Grid sx={{fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                                        Координаты объекта
                                    </Grid>
                                    <Grid sx={{color: 'text.primary',}}>
                                        {values.coordinateX && values.coordinateY ?
                                            `[${values.coordinateX}, ${values.coordinateY}]`
                                            : 'Нет данных'
                                        }
                                    </Grid>
                                </Grid>
                                <Grid item xs={2}>
                                    <SimpleMapModal
                                        coordX={values.coordinateX}
                                        coordY={values.coordinateY}
                                        modalName={'coordinates'}
                                        modalTitle={'GPS координаты трансформаторной подстанции'}
                                        startButtonTitle={'Посмотреть'}
                                        startButtonStyle={{width: '100%',}}
                                        // acceptButtonShow={false}
                                        onModalSubmit={(e) => {
                                            setFieldValue('coordinateX', e.x);
                                            setFieldValue('coordinateY', e.y);
                                        }}
                                        enableSearch={true}
                                        // disableStartButton={!values.coordinate_x && !values.coordinate_y}
                                    />
                                </Grid>
                            </Grid>
                            <Grid container>
                                <Grid item xs={4}>
                                    {values.coordinateX && values.coordinateY &&
                                        <MapComponent
                                            coordX={values.coordinateX}
                                            coordY={values.coordinateY}
                                            draggable={false}
                                            mapWidth={'100%'}
                                            mapHeight={'20vh'}
                                            enableSearch={false}
                                        />
                                    }
                                </Grid>
                            </Grid>
                        </Box>
                        <Box sx={{
                            marginBottom: '30px',
                            padding: '30px 20px',
                            bgcolor: 'background.default',
                            borderRadius: '10px',
                            boxShadow: '0px 2px 5px 0px rgba(138, 193, 233, 0.2)',
                        }}>
                            <Grid sx={{fontSize: '18px', fontWeight: '500', color: 'text.primary', marginBottom: '30px',}}>
                                Показания узла учёта
                            </Grid>
                            <Grid container style={{marginBottom: '30px',}}>
                                <Grid item xs={3} style={{paddingRight: '15px',}}>
                                    <SimpleTextInput
                                        name={'initialCounterValue'}
                                        label={'Показания счётчика при установке'}
                                        defaultValue={values.initialCounterValue}
                                        onBlur={(value) => {
                                            setFieldValue('initialCounterValue', +value);
                                            setChanged(true);
                                        }}
                                        onChange={() => {}}
                                        id={'initialCounterValue'}
                                    />
                                </Grid>
                                <Grid item xs={1}/>
                                <Grid item xs={3} style={{paddingRight: '15px',}}>
                                    <BasicDateTimePicker
                                        id={'initialCounterValueDate'}
                                        label={'Дата установки'}
                                        defaultValue={values.initialCounterValueDate}
                                        onChange={(value) => {
                                            setFieldValue('initialCounterValueDate', value);
                                            setChanged(true);
                                        }}
                                        maxDate={new Date()}
                                        inputVariant={'outlined'}
                                        style={{width: '100%',}}
                                    />
                                </Grid>
                            </Grid>
                            <Grid container>
                                <Grid item xs={3} style={{paddingRight: '15px',}}>
                                    <SimpleTextInput
                                        name={'lastCounterValue'}
                                        label={'Текущее показание счётчика'}
                                        defaultValue={values.lastCounterValue}
                                        onBlur={(value) => {
                                            setFieldValue('lastCounterValue', +value);
                                            setChanged(true);
                                        }}
                                        onChange={() => {}}
                                        id={'lastCounterValue'}
                                    />
                                </Grid>
                                <Grid item xs={1}/>
                                <Grid item xs={3} style={{paddingRight: '15px',}}>
                                    <BasicDateTimePicker
                                        id={'lastCounterValueDate'}
                                        label={'Дата снятия показания'}
                                        defaultValue={values.lastCounterValueDate}
                                        onChange={(value) => {
                                            setFieldValue('lastCounterValueDate', value);
                                            setChanged(true);
                                        }}
                                        maxDate={new Date()}
                                        inputVariant={'outlined'}
                                        style={{width: '100%',}}
                                    />
                                </Grid>
                            </Grid>
                        </Box>
                        <Box sx={{
                            marginBottom: '30px',
                            padding: '30px 20px',
                            bgcolor: 'background.default',
                            borderRadius: '10px',
                            boxShadow: '0px 2px 5px 0px rgba(138, 193, 233, 0.2)',
                        }}>
                            <Grid sx={{fontSize: '18px', fontWeight: '500', color: 'text.primary', marginBottom: '30px',}}>
                                Данные абонента
                            </Grid>
                            <Grid container style={{marginBottom: '30px',}}>
                                <Grid item xs={4} style={{paddingRight: '15px',}}>
                                    <SimpleTextInput
                                        name={'subscriberTitle'}
                                        label={'Наименование абонента'}
                                        defaultValue={values.subscriberTitle}
                                        onBlur={(value) => {
                                            setFieldValue('subscriberTitle', value);
                                            setChanged(true);
                                        }}
                                        onChange={() => {}}
                                        id={'subscriberTitle'}
                                    />
                                </Grid>
                            </Grid>
                            <Grid container style={{marginBottom: '30px',}}>
                                <Grid item xs={4} style={{paddingRight: '15px',}}>
                                    <SimpleTextInput
                                        name={'agreementNumber'}
                                        label={'Договор'}
                                        defaultValue={values.agreementNumber}
                                        onBlur={(value) => {
                                            setFieldValue('agreementNumber', value);
                                            setChanged(true);
                                        }}
                                        onChange={() => {}}
                                        id={'agreementNumber'}
                                    />
                                </Grid>
                            </Grid>
                            <Grid container style={{marginBottom: '30px',}}>
                                <Grid item xs={4} style={{paddingRight: '15px',}}>
                                    <SimpleTextInput
                                        name={'lodgersCount'}
                                        label={'Количество проживающих'}
                                        defaultValue={values.lodgersCount}
                                        onBlur={(value) => {
                                            setFieldValue('lodgersCount', +value);
                                            setChanged(true);
                                        }}
                                        onChange={() => {}}
                                        id={'lodgersCount'}
                                    />
                                </Grid>
                            </Grid>
                            <Grid container style={{marginBottom: '30px',}}>
                                <Grid item xs={4} style={{paddingRight: '15px',}}>
                                    <SimpleTextInput
                                        name={'roomsCount'}
                                        label={'Количество комнат'}
                                        defaultValue={values.roomsCount}
                                        onBlur={(value) => {
                                            setFieldValue('roomsCount', +value);
                                            setChanged(true);
                                        }}
                                        onChange={() => {}}
                                        id={'roomsCount'}
                                    />
                                </Grid>
                            </Grid>
                            <PhoneComponent
                                label={'Телефон'}
                                phones={values.phones}
                                onChange={(phones) => setFieldValue('phones', phones)}
                            />
                        </Box>
                        <Grid container style={{marginBottom: '20px',}}>
                            <Grid item xs={2} style={{paddingRight: '20px',}}>
                                <Button
                                    onClick={() => {
                                        handleReset();
                                        setChanged(false);
                                    }}
                                    variant={'outlined'}
                                    size={'small'}
                                    style={{width: '100%', height: '43px', fontSize: '12px',}}
                                >
                                    Отменить изменения
                                </Button>
                            </Grid>
                            <Grid item xs={2} style={{paddingRight: '20px',}}>
                                <Button
                                    onClick={() => props.onSaveCounter(values)}
                                    variant={'contained'}
                                    size={'small'}
                                    style={{width: '100%', height: '43px', fontSize: '12px',}}
                                >
                                    Сохранить изменения
                                </Button>
                            </Grid>
                        </Grid>
                    </Box>
                )}
            </Formik>
            <BackButtonComponent changed={changed}/>
        </>
    )
}