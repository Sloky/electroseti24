import React, {useEffect, useState} from "react";
import {
    AddressCompilation,
    CounterTypeData,
    TaskToCheckData,
    TaskToCheckPostData,
    TaskWarning,
    TaskElementInterface,
    SimpleFlashData,
} from "../../../interfaces/interfaces";
import "./TaskToCheckEditCard.css";
import {TASK_COUNTER_PHYSICAL_STATUSES, TASK_PRIORITY, TASK_STATUS, TASK_TYPE} from "../../../constants/constants";
import {acceptTaskToCheckRequest, editTaskToCheckRequest, returnTaskToCheckRequest} from "../../../requests/Request";
import SimpleMapModal from "../../../components/Modal/SimpleMapModal/SimpleMapModal";
import SimplePhotoGalleryModal from "../../../components/Modal/SimplePhotoGalleryModal/SimplePhotoGalleryModal";
import {changeView, getFullAddress, getTaskCheckPhotos} from "../../../functions/functions";
import {useDispatch} from "react-redux";
import {AppAction, TasksAction} from "../../../store/actions/actions";
import {useHistory} from "react-router";
import ChangeAddressModal from "../../../components/Modal/ChangeAddressModal/ChangeAddressModal";
import {Box, Button, Grid, Tooltip} from "@material-ui/core";
import FlashComponent from "../../../components/Flashes/FlashComponent";
import {THEME} from "../../../theme";
import MapComponent from "../../../components/Other/MapComponent/MapComponent";
import SimpleTextareaInput from "../../../components/FormInputs/SimpleTextareaInput/SimpleTextareaInput";
import SimpleSelectInput from "../../../components/FormInputs/SimpleSelectInput/SimpleSelectInput";
import SimpleTextInput from "../../../components/FormInputs/SimpleTextInput/SimpleTextInput";
import BackButtonComponent from "../../../components/Other/BackButtonComponent/BackButtonComponent";

interface TaskToCheckEditPageProps {
    task: TaskToCheckData
    counterTypes?: CounterTypeData[]
    children?: any
    onErrorOccured(message: string): void
}

function TaskDataItem(props: TaskElementInterface) {
    return (
        <Grid item xs={6}>
            <Grid container style={{marginBottom: '15px', display: 'flex', alignItems: 'center',}}>
                {props.title &&
                <Grid item xs={6} sx={{fontSize: '18px', fontWeight: '500', color: props.editable ? 'secondary.main' : THEME.PLACEHOLDER_TEXT}}>
                    <Box style={{width: '90%',}}>
                        {props.title}
                    </Box>
                </Grid>
                }
                {props.value &&
                <Grid item xs={6}>
                    <Box sx={{width: '90%', fontSize: '18px', color: 'text.primary', ...changeView(props.taskTextColor, props.taskBackgroundColor)}}>
                        {props.value}
                    </Box>
                </Grid>
                }
                {props.modal &&
                <Grid item xs={6}>
                    <Box>
                        {props.modal}
                    </Box>
                </Grid>
                }
            </Grid>
        </Grid>
    );
}

// const getLodgersCountData = (task: TaskToCheckData) => {
//     let oldValue = task.lodgers_count ? task.lodgers_count.toString() : 'Нет данных';
//     return task.changed_lodgers_count
//         ? `${task.changed_lodgers_count.toString()} (Предыдущее значение: ${oldValue})`
//         : oldValue;
// };
//
// const getRoomsCountData = (task: TaskToCheckData) => {
//     let oldValue = task.rooms_count ? task.rooms_count.toString() : 'Нет данных';
//     return task.changed_rooms_count
//         ? `${task.changed_rooms_count.toString()} (Предыдущее значение: ${oldValue})`
//         : oldValue;
// };

export default function TaskToCheckEditCard(props: TaskToCheckEditPageProps) {
    const [task, setTask] = useState<TaskToCheckData>(props.task);
    const [counterTypes, setCounterTypes] = useState<CounterTypeData[]>(props.counterTypes);
    const [warnings, setWarnings] = useState<TaskWarning[]>([]);
    const [flashSuccess, setFlashSuccess] = useState<SimpleFlashData>({status: false});
    const [flashError, setFlashError] = useState<SimpleFlashData>({status: false});

    const [editData, setEditData] = useState<TaskToCheckPostData>({});

    const [changed, setChanged] = useState(false);

    const dispatch = useDispatch();
    const history = useHistory();

    useEffect(() => {
        setTask(props.task);
    }, [props.task]);

    useEffect(() => {
        setCounterTypes(props.counterTypes);
    }, [props.counterTypes]);

    useEffect(() => {
        let warnings: TaskWarning[] = [];
        (task.changed_lodgers_count
            || task.changed_rooms_count
            || task.changed_side_seal
            || task.changed_antimagnetic_seal
            || task.changed_terminal_seal
            || task.changed_counter_type
            || task.changed_counter_number)
        && warnings.push({
            type: 'warning',
            title: 'Внимание',
            message: 'В задаче есть поля изменённые контролёром',
            status: true,
        });

        task.consumption < 0 && warnings.push({
            type: 'error',
            title: 'Внимание',
            message: 'Новые показания прибора учёта меньше предыдущих',
            status: true,
        });

        (!task.changed_counter_type && !!task.changed_counter_type_string) &&  warnings.push({
            type: 'warning',
            title: 'Внимание',
            message: 'Была изменена модель прибора учета. Выберите соответствующую модель',
            status: true,
        });

        task.counter_physical_status == 1 && warnings.push({
            type: 'error',
            title: 'Внимание',
            message: 'Узел учёта недоступен/неисправен',
            status: true,
        });
        setWarnings(warnings);

        if (!task.current_counter_value) {
            console.log('Error current_counter_value')
            setFlashError({status: true, messageHeading: 'Ошибка.', message: 'Отсутствуют показания счётчика'})
        }
    }, [task]);

    const saveTaskChanges = (taskEditData: TaskToCheckPostData) => {
        dispatch(AppAction.showLoader());
        setChanged(false);
        editTaskToCheckRequest(task.id, taskEditData).then(
            result => {
                if (result.type == 'success') {
                    setTask(result.data.task);
                    dispatch(TasksAction.changeTaskToCheckAction(result.data.task))
                    setFlashSuccess({message: 'Задача была успешно изменена', status: true});
                    window.scroll(0, 0);
                } else {
                    console.log('Error task data: ', result.data);
                    props.onErrorOccured(result.data.message);
                }
            }
        ).finally(() => {
            dispatch(AppAction.hideLoader());
        });
    };

    const onAddressChange = (address: AddressCompilation) => {
        let taskData = {
            ...task,
            locality: address.locality,
            street: address.street,
            house_number: address.house,
            porch_number: address.porch,
            apartment_number: address.apartment
        };
        let taskToCheckPostData: TaskToCheckPostData = {
            locality: address.locality,
            street: address.street,
            houseNumber: address.house,
            porchNumber: address.porch,
            apartmentNumber: address.apartment
        };
        setTask(taskData);
        setEditData(prevState => ({...prevState, ...taskToCheckPostData}));
    };

    const onOperatorCommentChange = (value: string) => {
        setTask(prevState => ({...prevState, operator_comment: value}));
        setEditData(prevState => ({...prevState, operatorComment: value}));
        setChanged(true);
    }

    const onCounterTypeChange = (value: string) => { // TODO: изменение counterTypes
        setChanged(true);
        // setTask(prevState => ({...prevState, changed_counter_type: value}));
    }

    const onCounterNumberChange = (value) => {
        setTask(prevState => ({...prevState, changed_counter_number: value}));
        setEditData(prevState => ({...prevState, newCounterNumber: value}));
        setChanged(true);
    }

    const onTerminalSealChange = (value) => {
        setTask(prevState => ({...prevState, changed_terminal_seal: value}));
        setEditData(prevState => ({...prevState, newTerminalSeal: value}));
        setChanged(true);
    }

    const onAntimagneticSealChange = (value) => {
        setTask(prevState => ({...prevState, changed_antimagnetic_seal: value}));
        setEditData(prevState => ({...prevState, newAntimagneticSeal: value}));
        setChanged(true);
    }

    const onSideSealChange = (value) => {
        setTask(prevState => ({...prevState, changed_side_seal: value}));
        setEditData(prevState => ({...prevState, newSideSeal: value}));
        setChanged(true);
    }

    const onCounterValueChange = (value) => {
        setTask(prevState => ({...prevState, current_counter_value: [+value]}));
        setEditData(prevState => ({...prevState, newCounterValue: [+value]}));
        setChanged(true);
    }

    const onLodgersCountChange = (value) => {
        setTask(prevState => ({...prevState, changed_lodgers_count: +value}));
        setEditData(prevState => ({...prevState, lodgersCount: +value}));
        setChanged(true);
    }

    const onRoomsCountChange = (value) => {
        setTask(prevState => ({...prevState, changed_rooms_count: +value}));
        setEditData(prevState => ({...prevState, roomsCount: +value}));
        setChanged(true);
    }

    const onCounterPhysicalStatusChange = (value: string) => {
        setTask(prevState => ({...prevState, counter_physical_status: +value}));
        setEditData(prevState => ({...prevState, counterPhysicalStatus: +value}));
        setChanged(true);
    }

    const resetTaskHandler = () => {
        setTask({...props.task});
        setEditData({});
        setChanged(false);
    }

    const onAcceptTask = () => {
        dispatch(AppAction.showLoader());
        acceptTaskToCheckRequest(task.id)
            .then((result) => {
                if (result.type == 'success') {
                    history.push('/tasks-to-check');
                } else {
                    console.log('Error task data: ', result.data);
                    dispatch(AppAction.hideLoader());
                    props.onErrorOccured(result.data.message);
                }
            });
    }

    const onReturnTask = () => {
        dispatch(AppAction.showLoader());
        returnTaskToCheckRequest(task.id)
            .then((result) => {
                if (result.type == 'success') {
                    history.push('/tasks-to-check');
                } else {
                    console.log('Error task data: ', result.data);
                    dispatch(AppAction.hideLoader());
                    props.onErrorOccured(result.data.message);
                }
            });
    }

    const onCloseSuccessFlash = () => {
        setFlashSuccess({status: false});
    }

    const onCloseErrorFlash = () => {
        setFlashError({status: false});
    }

    let address = getFullAddress(task);
    let date = new Date(task.deadline).toLocaleDateString('ru');
    let executedDate = new Date(task.executed_date).toLocaleString('ru');
    let executorFullName = task.executor !== null
        ? task.executor.surname + ' ' + task.executor.name + ' ' + task.executor.patronymic
        : 'Не назначен';
    let coords = task.coordinate_x && task.coordinate_y
        ? '\[' + task.coordinate_x + ', ' + task.coordinate_y + '\]'
        : 'Нет данных';
    let newCoords = task.new_coordinate_x && task.new_coordinate_y
        ? '\[' + task.new_coordinate_x + ', ' + task.new_coordinate_y + '\]'
        : 'Нет данных';
    let controllerComment = task.controller_comment || 'Нет данных';

    let counterPhotos = getTaskCheckPhotos(task, 'counter_photo');

    const taskDataElements: TaskElementInterface[] = [
        {
            title: 'Тип задачи',
            value: TASK_TYPE[task.task_type].title,
            modal: null
        },
        {
            title: 'Узел учёта',
            value: task.counter_number,
            modal: null
        },
        {
            title: 'Отделение',
            value: task.company.title,
            modal: null
        },
        {
            title: 'Исполнитель',
            value: executorFullName,
            modal: null
        },
        {
            title: 'Приоритет',
            value: TASK_PRIORITY[task.priority].title,
            modal: null
        },
        {
            title: 'Статус',
            value: TASK_STATUS[task.status].title,
            modal: null
        },
        {
            title: 'Срок выполнения',
            value: date,
            modal: null
        },
        {
            title: 'Дата и время исполнения',
            value: executedDate,
            modal: null
        },
        {
            title: 'Примечание контролёра',
            value: controllerComment,
            modal: null,
            taskTextColor: task.controller_comment ? 'warning' : undefined,
            taskBackgroundColor: null,
        },

    ];

    const networkDataElements: TaskElementInterface[] = [
        {
            title: 'Подстанция',
            value: task.substation ? task.substation.title : task.temporary_substation || 'Нет данных',
            modal: null
        },
        {
            title: 'Трансформатор',
            value: task.transformer ? task.transformer.title : task.temporary_transformer || 'Нет данных',
            modal: null
        },
        {
            title: 'Фидер',
            value: task.feeder ? task.feeder.title : task.temporary_feeder || 'Нет данных',
            modal: null
        },
        {
            title: '№ Линии',
            value: task.line_number || 'Нет данных',
            modal: null
        },
        {
            title: '№ Столба',
            value: task.electric_pole_number || 'Нет данных',
            modal: null
        },
    ];

    const counterMainDataElements: TaskElementInterface[] = [
        {
            title: 'Модель узла учёта',
            value: task.counter_type ? task.counter_type.title : task.temporary_counter_type || 'Нет данных',
            modal: null,
        },
        {
            // title: 'Новая модель узла учёта',
            title: null,
            // value: task.changed_counter_type ? task.changed_counter_type.title : task.changed_counter_type_string,
            value: <Tooltip title={'Функционал временно недоступен'} arrow>
                <Box>
                    <SimpleSelectInput
                        label={'Новая модель узла учёта'}
                        options={
                            counterTypes.map((counterType) => ({
                                title: counterType.title,
                                value: counterType.id,
                            }))
                        }
                        // defaultValue={task.changed_counter_type?.id}
                        onChange={onCounterTypeChange}
                        formControlStyle={{width: '100%',}}
                        disabled={true}
                    />
                </Box>
            </Tooltip>,
            editable: true,
            modal:
                <SimplePhotoGalleryModal
                    modalName={'counterTypeGalleryModal'}
                    modalTitle={'Фотографии прибора учёта'}
                    photos={getTaskCheckPhotos(task, 'counter_photo')}
                />,
            show: !!task.changed_counter_type_string,
            taskTextColor: null,
            // taskBackgroundColor: 'warning',
            taskBackgroundColor: null,
        },
        {
            title: 'Номер узла учёта',
            value: task.counter_number,
            modal: null,
        },
        {
            // title: 'Новый номер узла учёта',
            title: null,
            // value: task.changed_counter_number,
            value: <SimpleTextInput
                name={'counterNumber'}
                label={'Новый номер узла учёта'}
                defaultValue={task.changed_counter_number}
                onChange={() => {}}
                onBlur={onCounterNumberChange}
            />,
            editable: true,
            modal:
                <SimplePhotoGalleryModal
                    modalName={'counterGalleryModal'}
                    modalTitle={'Фотографии прибора учёта'}
                    photos={getTaskCheckPhotos(task, 'counter_photo')}
                />,
            show: !!task.changed_counter_number,
            taskTextColor: null,
            taskBackgroundColor: 'warning',
        },
        {
            title: 'Клеммная пломба',
            value: task.terminal_seal || 'Нет данных',
            modal: null
        },
        {
            // title: 'Новая клеммная пломба',
            title: null,
            // value: task.changed_terminal_seal,
            value: <SimpleTextInput
                label={'Новая клеммная пломба'}
                name={'terminalSeal'}
                defaultValue={task.changed_terminal_seal}
                onChange={() => {}}
                onBlur={onTerminalSealChange}
            />,
            editable: true,
            modal:
                <SimplePhotoGalleryModal
                    modalName={'terminalSealGalleryModal'}
                    modalTitle={'Фотографии клеммной пломбы'}
                    photos={getTaskCheckPhotos(task, 'terminal_seal')}
                />,
            show: !!task.changed_terminal_seal,
            taskTextColor: null,
            taskBackgroundColor: 'warning',
        },
        {
            title: 'Антимагнитная пломба',
            value: task.anti_magnetic_seal || 'Нет данных',
            modal: null
        },
        {
            // title: 'Новая антимагнитная пломба',
            title: null,
            // value: task.changed_antimagnetic_seal,
            value: <SimpleTextInput
                name={'antimagneticSeal'}
                label={'Новая антимагнитная пломба'}
                defaultValue={task.changed_antimagnetic_seal}
                onChange={() => {}}
                onBlur={onAntimagneticSealChange}
            />,
            editable: true,
            modal:
                <SimplePhotoGalleryModal
                    modalName={'antimagneticSealGalleryModal'}
                    modalTitle={'Фотографии антимагнитной пломбы'}
                    photos={getTaskCheckPhotos(task, 'antimagnetic_seal')}
                />,
            show: !!task.changed_antimagnetic_seal,
            taskTextColor: null,
            taskBackgroundColor: 'warning',
        },
        {
            title: 'Боковая пломба',
            value: task.side_seal || 'Нет данных',
            modal: null
        },
        {
            // title: 'Новая боковая пломба',
            title: null,
            // value: task.changed_side_seal,
            value: <SimpleTextInput
                label={'Новая боковая пломба'}
                name={'sideSeal'}
                defaultValue={task.changed_side_seal}
                onChange={() => {}}
                onBlur={onSideSealChange}
            />,
            editable: true,
            modal:
                <SimplePhotoGalleryModal
                    modalName={'sideSealGalleryModal'}
                    modalTitle={'Фотографии боковой пломбы'}
                    photos={getTaskCheckPhotos(task, 'side_seal')}
                />,
            show: !!task.changed_side_seal,
            taskTextColor: null,
            taskBackgroundColor: 'warning',
        },
    ];

    const counterDataElements: TaskElementInterface[] = [
        {
            title: 'Начальные показания узла учёта (кВч)',
            value: task.last_counter_value.toString(),
            modal: null
        },
        {
            // title: 'Конечные показания узла учёта (кВч)',
            title: null,
            // value: task.current_counter_value ? task.current_counter_value.toString() : 'Отсутствуют',
            value: <SimpleTextInput
                name={'currentCounterValue'}
                label={'Конечные показания узла учёта'}
                defaultValue={task.current_counter_value ? task.current_counter_value.toString() : 'Отсутствует'}
                onChange={() => {}}
                onBlur={onCounterValueChange}
            />,
            taskTextColor: null,
            taskBackgroundColor: task.consumption < 0 ? 'danger' : undefined,
            editable: true,
            modal: task.current_counter_value ?
                <SimplePhotoGalleryModal
                    modalName={'counterValueGalleryModal'}
                    modalTitle={'Фотографии показаний прибора учёта'}
                    photos={getTaskCheckPhotos(task, 'counter_value')}
                />
                : null
        },
        {
            title: 'Разность показаний (кВч)',
            value: task.consumption ? task.consumption.toString() : 'Нет данных',
            taskTextColor: task.consumption < 0 ? 'danger' : undefined,
            taskBackgroundColor: null,
            modal: null
        },
    ];

    const subscriberDataElements: TaskElementInterface[] = [
        {
            title: 'Наименование абонента',
            value: task.subscriber_title,
            modal: null
        },
        {
            title: 'Платежный код/Номер договора',
            value: task.agreement_number,
            modal: null
        },
        {
            title: 'Количество жильцов',
            value: task.lodgers_count || 'Нет данных',
            modal: null,
        },
        {
            // title: 'Количество жильцов',
            title: null,
            // value: getLodgersCountData(task),
            value: task.changed_lodgers_count ? <SimpleTextInput
                name={'lodgersCount'}
                label={'Количество жильцов'}
                defaultValue={task.changed_lodgers_count.toString()}
                onChange={() => {}}
                onBlur={onLodgersCountChange}
            /> : null,
            taskTextColor: null,
            taskBackgroundColor: task.changed_lodgers_count ? 'warning' : undefined,
            editable: true,
            modal: null,
            show: !!task.changed_lodgers_count,
        },
        {
            title: 'Количество комнат',
            value: task.rooms_count || 'Нет данных',
            modal: null,
        },
        {
            // title: 'Количество комнат',
            title: null,
            // value: getRoomsCountData(task),
            value: task.changed_rooms_count ? <SimpleTextInput
                name={'roomsCount'}
                label={'Количество комнат'}
                defaultValue={task.changed_rooms_count.toString()}
                onChange={() => {}}
                onBlur={onRoomsCountChange}
            /> : null,
            taskTextColor: null,
            taskBackgroundColor: task.changed_rooms_count ? 'warning' : undefined,
            editable: true,
            modal: null,
            show: !!task.changed_rooms_count,
        },
        {
            title: 'Телефон',
            value: task.subscriber_phones.length !== 0 ? task.subscriber_phones[0] : 'Нет данных',
            modal: null
        },
    ];

    return (
        <>
            <Box style={{marginBottom: '30px',}}>
                <BackButtonComponent changed={changed}/>
            </Box>
            {props.children}
            <FlashComponent
                messageHeading={flashError.messageHeading}
                message={flashError.message}
                showFlash={flashError.status}
                flashType={'error'}
                // onCloseFlash={onCloseErrorFlash}
            />
            {warnings.length > 0 && (
                warnings.map((warning, index) => (
                    <FlashComponent
                        key={index}
                        flashType={warning.type}
                        messageHeading={warning.title}
                        message={warning.message}
                        showFlash={warning.status}
                    />
                ))
            )}
            <FlashComponent
                messageHeading={flashSuccess.messageHeading}
                message={flashSuccess.message}
                showFlash={flashSuccess.status}
                flashType={'success'}
                onCloseFlash={onCloseSuccessFlash}
            />
            <>
                <Box sx={{
                    marginBottom: '30px',
                    padding: '30px 30px 15px 30px',
                    bgcolor: 'background.default',
                    borderRadius: '10px',
                    boxShadow: '0px 2px 5px 0px rgba(138, 193, 233, 0.2)',
                }}>
                    {taskDataElements.map((element, index) => (
                        element.show !== false && (
                            <Grid container key={index} style={{marginBottom: '15px',}}>
                                <Grid item xs={3} sx={{color: THEME.PLACEHOLDER_TEXT, fontSize: '18px', fontWeight: '500',}}>
                                    <Box style={{width: '90%',}}>
                                        {element.title}
                                    </Box>
                                </Grid>
                                <Grid item xs={9} sx={{color: 'text.primary', fontSize: '18px', ...changeView(element.taskTextColor, element.taskBackgroundColor)}}>
                                    {element.value}
                                </Grid>
                            </Grid>
                        )
                    ))}
                </Box>
                <Grid sx={{color: 'secondary.main', fontSize: '18px', fontWeight: '500', marginBottom: '20px',}}>Адрес</Grid>
                <Grid style={{marginBottom: '30px', display: 'flex', justifyContent: 'space-between',}}>
                    <Box sx={{color: 'text.primary', fontSize: '18px',}}>
                        {address}
                    </Box>
                    <Box>
                        <ChangeAddressModal
                            modalName={'address'}
                            defaultLocality={task.locality}
                            defaultStreet={task.street}
                            defaultHouse={task.house_number}
                            defaultPorch={task.porch_number}
                            defaultApartment={task.apartment_number}
                            modalTitle={'Редактировать адрес'}
                            onModalSubmit={onAddressChange}
                        />
                    </Box>
                </Grid>
                <Grid container style={{marginBottom: '30px',}}>
                    <Grid item xs={6} style={{paddingRight: '50px',}}>
                        <Box sx={{color: 'secondary.main', fontSize: '18px', fontWeight: '500', marginBottom: '10px',}}>
                            Координаты объекта
                        </Box>
                        <Box style={{display: 'flex', justifyContent: 'space-between', marginBottom: '15px',}}>
                            <Box sx={{color: 'text.primary', fontSize: '18px',}}>{coords}</Box>
                            <SimpleMapModal
                                coordX={task.coordinate_x}
                                coordY={task.coordinate_y}
                                modalName={'coordinates'}
                                modalTitle={'GPS координаты объекта задачи'}
                                startButtonTitle={'Посмотреть'}
                                acceptButtonShow={false}
                                onModalSubmit={() => {}}
                                enableSearch={false}
                                disableStartButton={!task.coordinate_x && !task.coordinate_y}
                            />
                        </Box>
                        {task.coordinate_x && task.coordinate_y && (
                            <MapComponent
                                coordX={task.coordinate_x}
                                coordY={task.coordinate_y}
                                draggable={false}
                                mapWidth={'100%'}
                                mapHeight={'20vh'}
                                enableSearch={false}
                            />
                        )}
                    </Grid>
                    <Grid item xs={6} style={{paddingLeft: '50px',}}>
                        <Box sx={{color: 'secondary.main', fontSize: '18px', fontWeight: '500', marginBottom: '10px',}}>
                            Координаты места выполнения задачи
                        </Box>
                        <Box style={{display: 'flex', justifyContent: 'space-between', marginBottom: '15px',}}>
                            <Box sx={{color: 'text.primary', fontSize: '18px',}}>{newCoords}</Box>
                            <SimpleMapModal
                                coordX={task.new_coordinate_x}
                                coordY={task.new_coordinate_y}
                                modalName={'newCoordinates'}
                                modalTitle={'GPS координаты места выполнения задачи'}
                                startButtonTitle={'Посмотреть'}
                                acceptButtonShow={false}
                                onModalSubmit={() => {}}
                                enableSearch={false}
                                disableStartButton={!task.new_coordinate_x && !task.new_coordinate_y}
                            />
                        </Box>
                        {task.new_coordinate_x && task.new_coordinate_y && (
                            <MapComponent
                                coordX={task.new_coordinate_x}
                                coordY={task.new_coordinate_y}
                                draggable={false}
                                mapWidth={'100%'}
                                mapHeight={'20vh'}
                                enableSearch={false}
                            />
                        )}
                    </Grid>
                </Grid>
                <Box style={{marginBottom: '30px',}}>
                    <Grid sx={{color: 'secondary.main', fontSize: '18px', fontWeight: '500', marginBottom: '20px',}}>Данные сети</Grid>
                    <Grid container>
                        {networkDataElements.map((element, index) => (
                            <Grid item xs={6} key={index} style={{marginBottom: '10px', display: 'flex',}}>
                                <Grid container>
                                    <Grid item xs={4} sx={{color: THEME.PLACEHOLDER_TEXT, fontSize: '18px', fontWeight: '500',}}>{element.title}</Grid>
                                    <Grid item xs={8} sx={{color: 'text.primary', fontSize: '18px', width: '90%',}}>{element.value}</Grid>
                                </Grid>
                            </Grid>
                        ))}
                    </Grid>
                </Box>
                <Box sx={{
                    marginBottom: '1px',
                    padding: '30px 30px 15px 30px',
                    bgcolor: 'background.default',
                    borderTopRightRadius: 10,
                    borderTopLeftRadius: 10,
                    boxShadow: '0px 2px 5px 0px rgba(138, 193, 233, 0.2)',
                }}>
                    <Grid sx={{marginBottom: '20px', color: 'text.primary', fontSize: '18px', fontWeight: '500',}}>Данные узла учёта</Grid>
                    <Grid container>
                        <Grid item xs={12} style={{marginBottom: '15px', display: 'flex', alignItems: 'center',}}>
                            <Box style={{marginRight: '15px',}}>
                                <SimpleSelectInput
                                    label={'Состояние/доступ узла учёта'}
                                    options={Object.values(TASK_COUNTER_PHYSICAL_STATUSES)}
                                    defaultValue={task.counter_physical_status.toString()}
                                    onChange={onCounterPhysicalStatusChange}
                                    style={{height: '50px', paddingRight: '15px',}}
                                />
                            </Box>
                            {
                                counterPhotos.length > 0
                                &&  <SimplePhotoGalleryModal
                                    modalName={'counterTypeGalleryModal'}
                                    modalTitle={'Фотографии прибора учёта'}
                                    photos={counterPhotos}
                                />
                            }
                        </Grid>
                        {counterMainDataElements.map((element, index) => (
                            element.show !== false ? <TaskDataItem
                                title={element.title}
                                taskBackgroundColor={element.taskBackgroundColor}
                                taskTextColor={element.taskTextColor}
                                key={index}
                                value={element.value}
                                modal={element.modal}
                                editable={element.editable}
                            /> : <Grid key={index} item xs={6} style={{width: '100%',}}/>
                        ))}
                    </Grid>
                    <Grid sx={{marginBottom: '20px', color: 'text.primary', fontSize: '18px', fontWeight: '500',}}>Показания узла учёта</Grid>
                    <Grid container>
                        {counterDataElements.map((element, index) => (
                            <TaskDataItem
                                title={element.title}
                                taskBackgroundColor={element.taskBackgroundColor}
                                taskTextColor={element.taskTextColor}
                                key={index}
                                value={element.value}
                                modal={element.modal}
                                editable={element.editable}
                            />
                        ))}
                    </Grid>
                </Box>
                <Box sx={{
                    marginBottom: '30px',
                    padding: '30px 30px 15px 30px',
                    bgcolor: 'background.default',
                    borderBottomLeftRadius: 10,
                    borderBottomRightRadius: 10,
                    boxShadow: '0px 2px 5px 0px rgba(138, 193, 233, 0.2)',
                }}>
                    {subscriberDataElements.map((element, index) => (
                        element.show !== false ? <TaskDataItem
                            title={element.title}
                            taskBackgroundColor={element.taskBackgroundColor}
                            taskTextColor={element.taskTextColor}
                            key={index}
                            value={element.value}
                            modal={element.modal}
                        /> : <Grid key={index} item xs={6} style={{width: '100%',}}/>
                    ))}
                </Box>
                <Box style={{marginBottom: '30px',}}>
                    <Grid sx={{color: 'secondary.main', fontSize: '18px', fontWeight: '500', marginBottom: '20px',}}>Примечание оператора</Grid>
                    <SimpleTextareaInput
                        label={'Примечание оператора'}
                        name={'operator_comment'}
                        defaultValue={task.operator_comment || ''}
                        onChange={() => {}}
                        onBlur={onOperatorCommentChange}
                        style={{width: '570px', backgroundColor: THEME.BACKGROUND_COLOR,}}
                    />
                </Box>
                <Grid container style={{marginBottom: '30px',}}>
                    <Grid item xs={2} style={{paddingRight: '15px',}}>
                        <Button
                            onClick={resetTaskHandler}
                            variant={'outlined'}
                            size={'small'}
                            className={'base_styled_btn'}
                            fullWidth
                        >
                            Отменить изменения
                        </Button>
                    </Grid>
                    <Grid item xs={2} style={{paddingRight: '15px',}}>
                        <Button
                            onClick={() => saveTaskChanges(editData)}
                            variant={'contained'}
                            size={'small'}
                            className={'base_styled_btn'}
                            fullWidth
                        >
                            Сохранить изменения
                        </Button>
                    </Grid>
                </Grid>
                <Grid container style={{marginBottom: '20px',}}>
                    <Grid item xs={2} style={{paddingRight: '15px',}}>
                        <Button
                            onClick={onReturnTask}
                            variant={'outlined'}
                            size={'small'}
                            className={'base_styled_btn'}
                            fullWidth
                        >
                            Отправить на доработку
                        </Button>
                    </Grid>
                    <Grid item xs={2} style={{paddingRight: '15px',}}>
                        <Button
                            onClick={onAcceptTask}
                            variant={'contained'}
                            size={'small'}
                            className={'base_styled_btn'}
                            fullWidth
                        >
                            Принять
                        </Button>
                    </Grid>
                </Grid>
            </>
            <BackButtonComponent changed={changed}/>
        </>
    );
}