import React, {useEffect, useState} from "react";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import {CounterTypeData, SimpleFlashData, TaskToCheckData} from "../../../interfaces/interfaces";
import {useParams} from "react-router";
import {getTaskToCheckRequest} from "../../../requests/Request";
import TaskToCheckEditCard from "./TaskToCheckEditCard";
import {useDispatch, useSelector} from "react-redux";
import {appStateInterface} from "../../../store/appState";
import {EntitiesAction} from "../../../store/actions/actions";
import {getEntityCollection, getFullAddress} from "../../../functions/functions";
import {Grid} from "@material-ui/core";
import FlashComponent from "../../../components/Flashes/FlashComponent";

export default function TaskToCheckPage() {
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [task, setTask] = useState<TaskToCheckData>();
    const [counterModels, setCounterModels] = useState<CounterTypeData[]>();
    const [flashError, setFlashError] = useState<SimpleFlashData>({status: false});
    const [flashInfo, setFlashInfo] = useState<SimpleFlashData>({status: false, message: ''});
    const params: {id: string} = useParams();

    const dispatch = useDispatch();

    const tasksToCheck = useSelector((state: appStateInterface) => state.tasksToCheck);
    const counterTypes = useSelector((state: appStateInterface) => state.counterTypes);

    const breadcrumbsItems = [
        {
            itemTitle: 'Задачи к проверке',
            itemSrc: '/tasks-to-check',
        },
        {
            itemTitle: task && task.counter_number ? `Задача ${task.counter_number}` : 'Задача',
            // itemSrc: ``,
        },
    ]

    const onCloseErrorFlash = () => {
        setFlashError(prevState =>  ({...prevState, status: false}))
    };

    const onCloseInfoFlash = () => {
        setFlashInfo(prevState => ({...prevState, status: false}))
    };

    useEffect(() => {
        setTask(tasksToCheck[params.id]);
        let models = Object.values(counterTypes);
        if (models.length > 0) {
            setCounterModels(models);
            setIsLoading(false);
        }
        getTaskToCheckRequest(params.id).then(
            result => {
                if (result.type == 'success') {
                    setTask(result.data.task);
                    setCounterModels(result.data.counterModels);
                    dispatch(EntitiesAction.changeCounterTypesAction(getEntityCollection(result.data.counterModels)));
                    if (result.data?.counterModels?.length === 0) {
                        setFlashInfo({
                            status:true,
                            message: 'Нет доступных моделей приборов учета'
                        });
                    }
                } else {
                    console.log('Error: ', result.data);
                    setFlashError({
                        status:true,
                        message: result.data
                    });
                }
            }
        ).finally(() => {
            setIsLoading(false);
        });

    }, []);

    return (
        <PageLayout
            dataLoading={isLoading}
            breadcrumbsItems={breadcrumbsItems}
        >
            {!isLoading &&
            <>
                <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                    {task ? `Задача "${getFullAddress(task)}, узел учёта ${task.counter_number}"` : 'Задача'}
                </Grid>
                <TaskToCheckEditCard
                    task={task}
                    counterTypes={counterModels || []}
                    onErrorOccured={(message) => {setFlashError({status: true, message: message})}}
                >
                    <FlashComponent
                        message={flashError.message}
                        messageHeading={flashError.messageHeading}
                        // onCloseFlash={onCloseErrorFlash}
                        showFlash={flashError.status}
                        flashType="error"
                        flashWidth="100%"
                    />
                    <FlashComponent
                        message={flashInfo.message}
                        onCloseFlash={onCloseInfoFlash}
                        showFlash={flashInfo.status}
                        flashType="info"
                    />
                </TaskToCheckEditCard>
            </>
            }
        </PageLayout>
    );
}