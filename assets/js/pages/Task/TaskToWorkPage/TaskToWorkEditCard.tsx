import React, {useEffect, useState} from "react";
import {
    AddressCompilation,
    TaskData,
    TaskToWorkPostData,
    UserData,
    TaskElementInterface,
    SimpleFlashData
} from "../../../interfaces/interfaces";
import "./TaskToWorkEditCard.css";
import {TASK_COUNTER_PHYSICAL_STATUSES, TASK_PRIORITY, TASK_STATUS, TASK_TYPE} from "../../../constants/constants";
import {editTaskToWorkRequest, deleteTaskToWorkRequest} from "../../../requests/Request";
import {useDispatch, useSelector} from "react-redux";
import ChangeAddressModal from "../../../components/Modal/ChangeAddressModal/ChangeAddressModal";
import SimpleMapModal from "../../../components/Modal/SimpleMapModal/SimpleMapModal";
import {AppAction, TasksAction} from "../../../store/actions/actions";
import {getFullAddress} from "../../../functions/functions";
import {Box, Button, Grid} from "@material-ui/core";
import {useHistory} from "react-router";
import {THEME} from "../../../theme";
import SimpleSelectInput from "../../../components/FormInputs/SimpleSelectInput/SimpleSelectInput";
import BasicDatePicker from "../../../components/DatePicker/BasicDatePicker";
import SimpleRadioGroup from "../../../components/FormInputs/SimpleRadioGroup/SimpleRadioGroup";
import SimpleTextareaInput from "../../../components/FormInputs/SimpleTextareaInput/SimpleTextareaInput";
import FlashComponent from "../../../components/Flashes/FlashComponent";
import SimpleModal from "../../../components/Modal/SimpleModal/SimpleModal";
import BackButtonComponent from "../../../components/Other/BackButtonComponent/BackButtonComponent";
import {appStateInterface} from "../../../store/appState";

interface TaskToWorkEditPageProps {
    task: TaskData
    controllers?: UserData[]
    children?: any
    onErrorOccured(message: string): void
}

const getController = (controllers: UserData[], controllerId: string) => {
    return controllers.filter(controller => controller.id === controllerId);
};

export default function TaskToWorkEditCard(props: TaskToWorkEditPageProps) {
    const [task, setTask] = useState<TaskData>(props.task);
    const [controllers, setControllers] = useState<UserData[]>(props.controllers);
    const [flashError, setFlashError] = useState<SimpleFlashData>({status: false});
    const [flashSuccess, setFlashSuccess] = useState<SimpleFlashData>({status: false});

    const [editData, setEditData] = useState<TaskToWorkPostData>({
        taskType: task.task_type,
        deadline: task.deadline,
        executor: task.executor && task.executor.id,
        priority: task.priority,
        status: task.status,
        coordinateX: task.coordinate_x,
        coordinateY: task.coordinate_y,
        operatorComment: task.operator_comment,
        locality: task.locality,
        street: task.street,
        houseNumber: task.house_number,
        porchNumber: task.porch_number,
        apartmentNumber: task.apartment_number,
        lodgersCount: task.lodgers_count,
        roomsCount: task.rooms_count
    });

    const [changed, setChanged] = useState(false);

    const dispatch = useDispatch();
    const history = useHistory();
    const currentUser = useSelector((state: appStateInterface) => state.user);

    useEffect(() => {
        setTask(props.task);
    }, [props.task]);

    useEffect(() => {
        setControllers(props.controllers);
    }, [props.controllers]);

    const saveTaskChanges = (taskData: TaskToWorkPostData) => {
        dispatch(AppAction.showLoader());
        setChanged(false);
        editTaskToWorkRequest(task.id, taskData).then(
            result => {
                if (result.type == 'success') {
                    setTask(result.data.task);
                    dispatch(TasksAction.changeTaskToWorkAction(result.data.task));
                    history.push('/tasks-to-work');
                    // setFlashSuccess({message: 'Задача была успешно изменена', status: true})
                } else {
                    console.log('Error task data: ', result.data);
                    props.onErrorOccured(result.data.message);
                }
            }
        ).finally(() => {
            dispatch(AppAction.hideLoader());
        });
    };

    const resetTaskHandler = () => {
        setTask({...props.task});
        setEditData({
            taskType: task.task_type,
            deadline: task.deadline,
            executor: task.executor && task.executor.id,
            priority: task.priority,
            status: task.status,
            coordinateX: task.coordinate_x,
            coordinateY: task.coordinate_y,
            operatorComment: task.operator_comment,
            locality: task.locality,
            street: task.street,
            houseNumber: task.house_number,
            porchNumber: task.porch_number,
            apartmentNumber: task.apartment_number,
            lodgersCount: task.lodgers_count,
            roomsCount: task.rooms_count
        });
        setChanged(false);
    }

    function onTaskTypeChange(taskType: number) {
        setTask(prevState => ({...prevState, task_type: taskType}));
        setEditData(prevState => ({...prevState, taskType: taskType}));
        setChanged(true);
    }

    const onExecutorChange = (userId: string|null) => {
        let controllersData = getController(controllers, userId);
        let executor = controllersData.length !== 0 ? controllersData[0] : null;
        setTask(prevState => ({...prevState, executor: executor}));
        setEditData(prevState => ({...prevState, executor: userId}));
        setChanged(true);
    };

    const onPriorityChange = (priority: number) => {
        setTask(prevState => ({...prevState, priority: priority}));
        setEditData(prevState => ({...prevState, priority: priority}));
        setChanged(true);
    };

    const onTextAreaChange = (comment: string) => {
        setTask(prevState => ({...prevState, operator_comment: comment}));
        setEditData(prevState => ({...prevState, operatorComment: comment}));
        setChanged(true);
    };

    const onAddressChange = (address: AddressCompilation) => {
        let taskData = {
            ...task,
            locality: address.locality,
            street: address.street,
            house_number: address.house,
            porch_number: address.porch,
            apartment_number: address.apartment
        };
        let taskToWorkPostData: TaskToWorkPostData = {
            locality: address.locality,
            street: address.street,
            houseNumber: address.house,
            porchNumber: address.porch,
            apartmentNumber: address.apartment,
        }
        setTask(taskData);
        setEditData(prevState => ({...prevState, ...taskToWorkPostData}));
        setChanged(true);
    };

    const onCoordinatesChange = (coords: {x: number, y: number}) => {
        let taskData = {
            ...task,
            coordinate_x: coords.x,
            coordinate_y: coords.y
        };
        let taskPostData: TaskToWorkPostData = {
            coordinateX: coords.x,
            coordinateY: coords.y,
        }
        setTask(taskData);
        setEditData(prevState => ({...prevState, ...taskPostData}));
        setChanged(true);
    };

    const onDeadlineChange = (date: string) => {
        setTask(prevState => ({...prevState, deadline: date}));
        setEditData(prevState => ({...prevState, deadline: date}));
        setChanged(true);
    };

    const deleteTaskHandler = () => {
        dispatch(AppAction.showLoader());
        deleteTaskToWorkRequest(task.id).then(
            result => {
                if (result.type == 'success') {
                    dispatch(AppAction.hideLoader());
                    history.push("/tasks-to-work");
                } else {
                    dispatch(AppAction.hideLoader());
                    setFlashError({
                        status: true,
                        messageHeading: 'Ошибка.',
                        message: 'Не удалось удалить пользователя. Пожалуйста, обратитесь к администратору',
                    });
                }
            }
        );
    };

    const onCloseSuccessFlash = () => {
        setFlashSuccess({status: false});
    }

    let address = getFullAddress(task);
    // let date = task ? new Date(task.deadline).toLocaleDateString('ru') : '';
    // let executorFullName = task.executor !== null
    //     ? task.executor.surname + ' ' + task.executor.name + ' ' + task.executor.patronymic
    //     : 'Не назначен';
    let coords = task.coordinate_x && task.coordinate_y
        ? '\[' + task.coordinate_x + ', ' + task.coordinate_y + '\]'
        : 'Нет данных';
    // let comment = task.operator_comment || 'Нет данных';

    const taskMainDataElements: TaskElementInterface[] = [
        {
            title: 'Узел учёта',
            value: task.counter_number || 'Нет данных',
            modal: null,
        },
        {
            title: 'Отделение',
            value: task.company.title || 'Нет данных',
            modal: null,
        },
        {
            title: 'Статус',
            value: TASK_STATUS[task.status].title || 'Нет данных',
            modal: null,
        },
    ];

    const taskDataElements: TaskElementInterface[] = [
        {
            title: 'Тип задачи',
            // value: TASK_TYPE[task.task_type].title,
            value: '',
            modal: (
                <SimpleSelectInput
                    label={'Тип задачи'}
                    options={Object.values(TASK_TYPE)}
                    onChange={onTaskTypeChange}
                    style={{width: '570px', height: '48px',}}
                    defaultValue={task.task_type}
                />
            ),
        },
        {
            title: 'Адрес',
            value: address,
            modal: (
                <ChangeAddressModal
                    modalName={'address'}
                    defaultLocality={task.locality}
                    defaultStreet={task.street}
                    defaultHouse={task.house_number}
                    defaultPorch={task.porch_number}
                    defaultApartment={task.apartment_number}
                    modalTitle={'Редактировать адрес'}
                    onModalSubmit={onAddressChange}
                    startButtonStyle={{height: '48px',}}
                />
            )
        },
        {
            title: 'Исполнитель',
            // value: executorFullName,
            value: '',
            modal: (
                <SimpleSelectInput
                    label={'Исполнитель'}
                    options={controllers.map(controller => {
                        return {
                            title: controller.surname + ' ' + controller.name + ' ' + controller.patronymic,
                            value: controller.id,
                        }
                    })}
                    onChange={onExecutorChange}
                    style={{width: '570px', height: '48px',}}
                    defaultValue={task.executor && task.executor.id}
                    allowEmptyValue={true}
                    emptyValueTitle={'Не выбран'}
                />
            ),
        },
        {
            title: 'Срок выполнения',
            // value: date,
            value: '',
            modal: (
                <BasicDatePicker
                    label={'Срок выполнения'}
                    defaultValue={new Date(task.deadline)}
                    onChange={onDeadlineChange}
                />
            ),
        },
        {
            title: 'Приоритет',
            // value: TASK_PRIORITY[task.priority].title,
            value: '',
            modal: (
                <SimpleRadioGroup
                    onChange={onPriorityChange}
                    options={Object.values(TASK_PRIORITY)}
                    defaultValue={task.priority}
                    style={{width: '570px',}}
                />
            ),
        },
        {
            title: 'GPS координаты',
            value: coords,
            modal: (
                <SimpleMapModal
                    coordX={task.coordinate_x}
                    coordY={task.coordinate_y}
                    modalName={'coordinates'}
                    modalTitle={'Изменение координат'}
                    onModalSubmit={onCoordinatesChange}
                    enableSearch={true}
                    startButtonStyle={{height: '48px',}}
                />
            )
        },
        {
            title: 'Примечание',
            // value: comment,
            value: '',
            modal: (
                <SimpleTextareaInput
                    label={'Примечание'}
                    name={'operator_comment'}
                    defaultValue={task.operator_comment || ''}
                    onChange={() => {}}
                    onBlur={onTextAreaChange}
                    style={{width: '570px',}}
                />
            ),
        },
    ];

    const networkDataElements: TaskElementInterface[] = [
        {
            title: 'Подстанция',
            value: task.substation ? task.substation.title : task.temporary_substation || 'Нет данных',
            modal: null
        },
        {
            title: 'Трансформатор',
            value: task.transformer ? task.transformer.title : task.temporary_transformer || 'Нет данных',
            modal: null
        },
        {
            title: 'Фидер',
            value: task.feeder ? task.feeder.title : task.temporary_feeder || 'Нет данных',
            modal: null
        },
        {
            title: '№ Линии',
            value: task.line_number || 'Нет данных',
            modal: null
        },
        {
            title: '№ Столба',
            value: task.electric_pole_number || 'Нет данных',
            modal: null
        },
    ];

    const counterMainDataElements: TaskElementInterface[] = [
        {
            title: 'Модель узла учёта',
            value: task.counter_type ? task.counter_type.title : task.temporary_counter_type || 'Нет данных',
            modal: null
        },
        {
            title: 'Номер узла учёта',
            value: task.counter_number || 'Нет данных',
            modal: null
        },
        {
            title: 'Состояние/доступ узла учёта',
            value: TASK_COUNTER_PHYSICAL_STATUSES[task.counter_physical_status].title,
            modal: null
        },
        {
            title: 'Клеммная пломба',
            value: task.terminal_seal || 'Нет данных',
            modal: null
        },
        {
            title: 'Антимагнитная пломба',
            value: task.anti_magnetic_seal || 'Нет данных',
            modal: null
        },
        {
            title: 'Боковая пломба',
            value: task.side_seal || 'Нет данных',
            modal: null
        },
    ];

    const counterDataElements: TaskElementInterface[] = [
        {
            title: 'Начальные показания узла учёта (кВч)',
            value: task.last_counter_value.toString(),
            modal: null
        },
    ];

    const subscriberDataElements: TaskElementInterface[] = [
        {
            title: 'ФИО/Наименование ЮЛ',
            value: task.subscriber_title,
            modal: null
        },
        {
            title: 'Платежный код/Номер договора',
            value: task.agreement_number,
            modal: null
        },
        {
            title: 'Количество проживающих',
            value: task.lodgers_count ? task.lodgers_count.toString() : 'Нет данных',
            modal: null
        },
        {
            title: 'Количество комнат',
            value: task.rooms_count ? task.rooms_count.toString() : 'Нет данных',
            modal: null
        },
        {
            title: 'Телефон',
            value: task.subscriber_phones.length !== 0 ? task.subscriber_phones[0] : 'Нет данных',
            modal: null
        },
    ];

    return (
        <>
            <Grid style={{marginBottom: '30px', display: 'flex', justifyContent: 'space-between',}}>
                <BackButtonComponent changed={changed}/>
                {currentUser.roles.includes('ROLE_SUPER_ADMIN') &&
                    <SimpleModal
                        startButtonTitle={'Удалить задачу'}
                        modalName={'deleteTaskModal'}
                        // startIcon={<PersonAddDisabledOutlined/>}
                        startButtonVariant={'text'}
                        closeButtonTitle={'Отменить'}
                        modalTitle={'Удаление задачи'}
                        acceptButtonTitle={'Подтвердить'}
                        onModalSubmit={deleteTaskHandler}
                    >
                        Удалить задачу?
                    </SimpleModal>
                }
            </Grid>
            <Box style={{marginBottom: '30px',}}>
                <FlashComponent
                    messageHeading={flashError.messageHeading}
                    message={flashError.message}
                    showFlash={flashError.status}
                    flashType={'error'}
                    flashWidth={'100%'}
                />
                <FlashComponent
                    messageHeading={flashSuccess.messageHeading}
                    message={flashSuccess.message}
                    showFlash={flashSuccess.status}
                    flashType={'success'}
                    onCloseFlash={onCloseSuccessFlash}
                />
                {taskMainDataElements.map((element, index) => (
                    <Grid container key={index} sx={{marginBottom: '15px',}}>
                        <Grid item xs={2} sx={{
                            color: THEME.PLACEHOLDER_TEXT,
                            fontSize: '18px',
                        }}>
                            {element.title}
                        </Grid>
                        <Grid item xs={10} sx={{color: 'text.primary', fontSize: '18px', fontWeight: '500',}}>{element.value}</Grid>
                    </Grid>
                ))}
            </Box>
            {props.children}
            <Box
                sx={{
                    marginBottom: '30px',
                    padding: '30px 30px 15px 30px',
                    bgcolor: 'background.default',
                    borderTopRightRadius: 10,
                    borderBottomRightRadius: 10,
                    boxShadow: '0px 2px 5px 0px rgba(138, 193, 233, 0.2)',
                }}>
                {taskDataElements.map((element, index) => (
                    <Grid container key={index} sx={{marginBottom: '15px', display: 'flex', alignItems: 'center',}}>
                        <Grid item xs={3} sx={{color: 'secondary.main', fontSize: '18px', fontWeight: '500',}}>{element.title}</Grid>
                        <Grid item xs={6}>
                            <Box style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center', width: '570px',}}>
                                <Box sx={{fontSize: '18px', color: 'text.primary',}}>
                                    {element.value}
                                </Box>
                                <Box>
                                    {element.modal}
                                </Box>
                            </Box>
                        </Grid>
                    </Grid>
                ))}
            </Box>
            <Box style={{marginBottom: '30px',}}>
                <Grid sx={{color: 'secondary.main', fontSize: '18px', fontWeight: '500', marginBottom: '20px',}}>Данные сети</Grid>
                <Grid container>
                    {networkDataElements.map((element, index) => (
                        <Grid item xs={6} key={index} style={{marginBottom: '10px', display: 'flex',}}>
                            <Grid container>
                                <Grid item xs={4} sx={{color: THEME.PLACEHOLDER_TEXT, fontSize: '18px', fontWeight: '500',}}>{element.title}</Grid>
                                <Grid item xs={8} sx={{color: 'text.primary', fontSize: '18px', width: '90%',}}>{element.value}</Grid>
                            </Grid>
                        </Grid>
                    ))}
                </Grid>
            </Box>
            <Box
                sx={{
                    marginBottom: '1px',
                    padding: '30px 30px 15px 30px',
                    bgcolor: 'background.default',
                    borderTopRightRadius: 10,
                    boxShadow: '0px 2px 5px 0px rgba(138, 193, 233, 0.2)',
                }}>
                <Grid sx={{marginBottom: '20px', color: 'text.primary', fontSize: '18px', fontWeight: '500',}}>Данные узла учёта</Grid>
                <Box style={{marginBottom: '30px',}}>
                    {counterMainDataElements.map((element, index) => (
                        <Grid container key={index} style={{marginBottom: '15px',}}>
                            <Grid item xs={3} sx={{color: THEME.PLACEHOLDER_TEXT, fontSize: '18px', fontWeight: '500',}}>
                                <Box style={{width: '90%',}}>
                                    {element.title}
                                </Box>
                            </Grid>
                            <Grid item xs={9} sx={{color: 'text.primary', fontSize: '18px',}}>
                                {element.value}
                            </Grid>
                        </Grid>
                    ))}
                </Box>
                <Grid sx={{marginBottom: '20px', color: 'text.primary', fontSize: '18px', fontWeight: '500',}}>Показания узла учёта</Grid>
                {counterDataElements.map((element, index) => (
                    <Grid container key={index} style={{marginBottom: '15px',}}>
                        <Grid item xs={3} sx={{color: THEME.PLACEHOLDER_TEXT, fontSize: '18px', fontWeight: '500',}}>
                            <Box style={{width: '90%',}}>
                                {element.title}
                            </Box>
                        </Grid>
                        <Grid item xs={9} sx={{color: 'text.primary', fontSize: '18px',}}>
                            {element.value}
                        </Grid>
                    </Grid>
                ))}
            </Box>
            <Box
                sx={{
                    marginBottom: '30px',
                    padding: '30px 30px 15px 30px',
                    bgcolor: 'background.default',
                    borderBottomRightRadius: 10,
                    boxShadow: '0px 2px 5px 0px rgba(138, 193, 233, 0.2)',
                }}>
                <Grid sx={{marginBottom: '20px', color: 'text.primary', fontSize: '18px', fontWeight: '500',}}>Данные абонента</Grid>
                {subscriberDataElements.map((element, index) => (
                    <Grid container key={index} style={{marginBottom: '15px',}}>
                        <Grid item xs={3} sx={{color: THEME.PLACEHOLDER_TEXT, fontSize: '18px', fontWeight: '500',}}>
                            <Box style={{width: '90%',}}>
                                {element.title}
                            </Box>
                        </Grid>
                        <Grid item xs={9} sx={{color: 'text.primary', fontSize: '18px',}}>
                            {element.value}
                        </Grid>
                    </Grid>
                ))}
            </Box>
            <Grid container style={{marginBottom: '20px',}}>
                <Grid item xs={2} style={{paddingRight: '15px',}}>
                    <Button
                        onClick={resetTaskHandler}
                        variant={'outlined'}
                        size={'small'}
                        className={'base_styled_btn'}
                        fullWidth
                    >
                        Отменить изменения
                    </Button>
                </Grid>
                <Grid item xs={2} style={{paddingRight: '15px',}}>
                    <Button
                        onClick={() => saveTaskChanges(editData)}
                        variant={'contained'}
                        size={'small'}
                        className={'base_styled_btn'}
                        fullWidth
                    >
                        Сохранить изменения
                    </Button>
                </Grid>
            </Grid>
            <BackButtonComponent changed={changed}/>
        </>
    );
}