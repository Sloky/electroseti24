import React, {useEffect, useState} from "react";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import {useParams} from "react-router";
import TaskToWorkEditCard from "./TaskToWorkEditCard";
import {SimpleFlashData, TaskData, UserData} from "../../../interfaces/interfaces";
import {getTaskToWorkRequest} from "../../../requests/Request";
import {useDispatch, useSelector} from "react-redux";
import {appStateInterface} from "../../../store/appState";
import {TASK_TYPE} from "../../../constants/constants";
import FlashComponent from "../../../components/Flashes/FlashComponent";
import {Grid} from "@material-ui/core";

export default function TaskToWorkPage() {
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [task, setTask] = useState<TaskData>();
    const [controllers, setControllers] = useState<UserData[]>();
    const [flashError, setFlashError] = useState<SimpleFlashData>({status: false});
    const [flashInfo, setFlashInfo] = useState({status: false, message: ''});
    const params: {id: string} = useParams();

    const tasksToWork = useSelector((state: appStateInterface) => state.tasksToWork);
    const users = [];

    const breadcrumbsItems = [
        {
            itemTitle: 'Задачи к выполнению',
            itemSrc: '/tasks-to-work',
        },
        {
            itemTitle: task && task.counter_number ? `Задача ${task.counter_number}` : 'Задача',
            // itemSrc: ``,
        },
    ]

    const onCloseErrorFlash = () => {
        setFlashError(prevState => {return {...prevState, status: false}})
    };

    const onCloseInfoFlash = () => {
        setFlashInfo(prevState => {return {...prevState, status: false}})
    };

    useEffect(() => {
        if (tasksToWork[params.id]) {
            setTask(tasksToWork[params.id]);
        }
    }, [tasksToWork]);

    useEffect(() => {
        setTask(tasksToWork[params.id]);
        let controllers = Object.values(users);
        if (controllers.length > 0) {
            setControllers(controllers);
            setIsLoading(false);
        }
        getTaskToWorkRequest(params.id).then(
            result => {
                if (result.type == 'success') {
                    setTask(result.data.task);
                    setControllers(result.data.users);
                    if (result.data.users.length === 0 ) {
                        setFlashInfo({
                            status:true,
                            message: 'Нет доступных исполнителей для этого отделения. ' +
                                'Для возможности выбора исполнителя добавьте его через меню "Пользователи"'
                        });
                    }
                } else {
                    console.log('Error: ', result.data);
                    setFlashError({
                        status:true,
                        message: result.data
                    });
                }
            }
        ).finally(() => {
            setIsLoading(false);
        });
    }, []);

    return (
        <PageLayout
            breadcrumbsItems={breadcrumbsItems}
        >
            {!isLoading &&
            <>
                <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                    {task ? `Задача "${TASK_TYPE[task.task_type].title} ${task.counter_number}"` : 'Задача'}
                </Grid>
                <TaskToWorkEditCard
                    task={task}
                    controllers={controllers || []}
                    onErrorOccured={(message) => {setFlashError({status: true, message: message})}}
                >
                    <FlashComponent
                        message={flashError.message}
                        messageHeading={flashError.messageHeading}
                        // onCloseFlash={onCloseErrorFlash}
                        showFlash={flashError.status}
                        flashType="error"
                        flashWidth="100%"
                    />
                    <FlashComponent
                        message={flashInfo.message}
                        onCloseFlash={onCloseInfoFlash}
                        showFlash={flashInfo.status}
                        flashType="info"
                    />
                </TaskToWorkEditCard>
            </>
            }
        </PageLayout>
    );
}