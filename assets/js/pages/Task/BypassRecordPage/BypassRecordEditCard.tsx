import React, {useEffect, useState} from "react";
import {
    BypassRecordData,
    BypassRecordPostData,
    CompanyData,
    UserData
} from "../../../interfaces/interfaces";
import {useDispatch, useSelector} from "react-redux";
import {useHistory} from "react-router";
import {AppAction} from "../../../store/actions/actions";
import {Box, Button, Grid, Tooltip} from "@material-ui/core";
import SimpleModal from "../../../components/Modal/SimpleModal/SimpleModal";
import {deleteBypassRecordRequest} from "../../../requests/Request";
import SimpleSelectInput from "../../../components/FormInputs/SimpleSelectInput/SimpleSelectInput";
import {Formik} from "formik";
import * as Yup from "yup";
import SimpleTextInput from "../../../components/FormInputs/SimpleTextInput/SimpleTextInput";
import SimpleMapModal from "../../../components/Modal/SimpleMapModal/SimpleMapModal";
import BasicDatePicker from "../../../components/DatePicker/BasicDatePicker";
import BackButtonComponent from "../../../components/Other/BackButtonComponent/BackButtonComponent";
import {appStateInterface} from "../../../store/appState";

interface BypassRecordEditPageProps {
    record: BypassRecordData
    companiesData: CompanyData[]
    controllersData?: UserData[]
    children?: any
    onFlash(type: string, message: string, messageHeading?: string): void
    onSaveRecord(data: BypassRecordPostData): void
    onAcceptRecord(data: BypassRecordPostData): void
}

const selectSubscriberType = [
    {title: 'Физическое лицо', value: 'Физическое лицо'},
    {title: 'Юридическое лицо', value: 'Юридическое лицо'},
    {title: 'Центральный аппарат', value: 'Центральный аппарат'},
    {title: 'Электрон', value: 'Электрон'},
];

const selectServiceability = [
    {title: 'Доступен / Исправен', value: 'Доступен / Исправен'},
    {title: 'Недоступен / Неисправен', value: 'Недоступен / Неисправен'},
];

export default function BypassRecordEditCard(props: BypassRecordEditPageProps) {
    const [record, setRecord] = useState({...props.record});
    const [companiesData, setCompaniesData] = useState(props.companiesData);
    const [controllersData, setControllersData] = useState(props.controllersData);
    const [controllersOption, setControllersOption] = useState([]);

    const [changed, setChanged] = useState(false);

    const dispatch = useDispatch();
    const history = useHistory();
    const currentUser = useSelector((state: appStateInterface) => state.user);

    useEffect(() => {
        setRecord(props.record);
        setCompaniesData(props.companiesData);
        setControllersData(props.controllersData);
    }, [props]);

    useEffect(() => {
        setControllersOption(filterControllers(record.company_title));
    }, [record.company_title])

    const changeDataFormat = (values) => {
        let editData = {
            subscriberType: values.subscriber_type,
            subscriberTitle: values.subscriber_title,
            agreementNumber: values.agreement_number,
            counterNumber: values.counter_number,
            companyTitle: values.company_title,
            controller: values.controller,
            address: values.address,
            coords: values.coords,
            deadline: values.deadline,
            serviceability: values.serviceability,
            counterModel: values.counter_model,
            counterInitialValue: values.counter_initial_value,
            counterInitialDate: values.counter_initial_date,
            currentCounterValue: values.current_counter_value,
            currentCounterValueDate: values.current_counter_value_date,
            terminalSeal: values.terminal_seal,
            antimagneticSeal: values.antimagnetic_seal,
            sideSeal: values.side_seal,
            substation: values.substation,
            feeder: values.feeder,
            transformer: values.transformer,
            electricLine: values.electric_line,
            electricPole: values.electric_pole,
            lodgersCount: values.lodgers_count,
            roomsCount: values.rooms_count,
            phones: values.phones,
            // taskType: values.task_type, // TODO: тип задачи
        };
        return editData;
    }

    const acceptRecord = (values) => {
        props.onAcceptRecord(changeDataFormat(values));
    };

    const saveRecordChanges = (values) => {
        setChanged(false);
        props.onSaveRecord(changeDataFormat(values));
    };

    const deleteRecordHandler = () => {
        dispatch(AppAction.showLoader());
        deleteBypassRecordRequest(record.id).then(
            result => {
                if (result.type == 'success') {
                    dispatch(AppAction.hideLoader());
                    history.push("/bypass");
                } else {
                    dispatch(AppAction.hideLoader());
                    props.onFlash('error', 'Не удалось удалить запись. Пожалуйста, обратитесь к администратору', 'Ошибка.');
                }
            }
        );
    };

    const checkCompanyTitle = (company) => {
        return companiesData.filter(item => item.title === company).length
    }

    const filterControllers = (companyValue) => {
        let company = companiesData.filter(company => company.title === companyValue);
        if (company.length) {
            let controllers = controllersData.filter(i => {
                let result = i.companies.filter(item => item.id == company[0].id)
                if (result[0]) {
                    return i;
                }
            });
            return controllers.map(item => ({
                title: `${item.surname} ${item.name} ${item.patronymic}`,
                value: `${item.surname} ${item.name} ${item.patronymic}`,
            }));
        }
        return [];
    }

    const recordSchema = Yup.object().shape({
        subscriber_type: Yup.string(),
        subscriber_title: Yup.string(),
        agreement_number: Yup.string(),
        counter_number: Yup.string(),
        company_title: Yup.string(),
        controller: Yup.string(),
        address: Yup.string(),
        coords: Yup.string(),
        deadline: Yup.string(),
        serviceability: Yup.string(),
        counter_model: Yup.string(),
        counter_initial_value: Yup.string(),
        counter_initial_date: Yup.string().matches(/^\d{2}.\d{2}.\d{4}$/),
        current_counter_value: Yup.string(),
        current_counter_value_date: Yup.string().matches(/^\d{2}.\d{2}.\d{4}$/),
        terminal_seal: Yup.string(),
        antimagnetic_seal: Yup.string(),
        side_seal: Yup.string(),
        substation: Yup.string(),
        feeder: Yup.string(),
        transformer: Yup.string(),
        electric_line: Yup.string(),
        electric_pole: Yup.string(),
        lodgers_count: Yup.string(),
        rooms_count: Yup.string(),
        phones: Yup.string(),
    });

    return (
        <>
            <Grid style={{marginBottom: '30px', display: 'flex', justifyContent: 'space-between',}}>
                <BackButtonComponent changed={changed}/>
                {currentUser.roles.includes('ROLE_SUPER_ADMIN') &&
                    <SimpleModal
                        startButtonTitle={'Удалить запись'}
                        modalName={'deleteBypassRecordModal'}
                        // startIcon={<PersonAddDisabledOutlined/>}
                        startButtonVariant={'text'}
                        closeButtonTitle={'Отменить'}
                        modalTitle={'Удаление запись'}
                        acceptButtonTitle={'Подтвердить'}
                        onModalSubmit={deleteRecordHandler}
                    >
                        Удалить запись?
                    </SimpleModal>
                }
            </Grid>
            {props.children}
            <Formik
                initialValues={record}
                validationSchema={recordSchema}
                onSubmit={() => {}}
            >
                {({
                      handleChange,
                      handleSubmit,
                      handleBlur,
                      dirty,
                      isValid,
                      errors,
                      values,
                      touched,
                      setFieldValue,
                      handleReset,
                      validateForm,
                }) => (
                    <Box component={'form'}>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Tooltip title={'Функционал временно недоступен'} arrow>
                                <Grid item xs={4} style={{paddingRight: '15px',}}>
                                    <SimpleSelectInput
                                        name={'taskType'}
                                        label={'Тип задачи'}
                                        options={[{title: 'Снятие показаний', value: 'Снятие показаний',}]}
                                        defaultValue={'Снятие показаний'}
                                        // onChange={}
                                        id={'taskType'}
                                        formControlStyle={{width: '100%',}}
                                        disabled
                                    />
                                </Grid>
                            </Tooltip>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            {!selectSubscriberType.filter(item => item.value === values.subscriber_type).length ?
                                <Grid item xs={4} style={{paddingRight: '15px',}}>
                                    <SimpleTextInput
                                        error={true}
                                        helperText={'Указан несуществующий тип абонента'}
                                        name={'subscriberType'}
                                        label={'Тип абонента'}
                                        defaultValue={values.subscriber_type}
                                        // onChange={}
                                        id={'subscriberType'}
                                        disabled
                                    />
                                </Grid> : null
                            }
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleSelectInput
                                    label={'Тип абонента'}
                                    options={selectSubscriberType}
                                    defaultValue={selectSubscriberType.filter(item => item.value === values.subscriber_type)[0] ? values.subscriber_type : null}
                                    onChange={(value) => {
                                        setFieldValue('subscriber_type', value);
                                        setChanged(true);
                                    }}
                                    id={'subscriberType'}
                                    allowEmptyValue={true}
                                    formControlStyle={{width: '100%',}}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleTextInput
                                    name={'subscriberTitle'}
                                    label={'Наименование абонента'}
                                    defaultValue={values.subscriber_title}
                                    onBlur={(value) => {
                                        setFieldValue('subscriber_title', value);
                                        setChanged(true);
                                    }}
                                    onChange={() => {}}
                                    id={'subscriberTitle'}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleTextInput
                                    name={'agreementNumber'}
                                    label={'Договор'}
                                    defaultValue={values.agreement_number}
                                    onBlur={(value) => {
                                        setFieldValue('agreement_number', value);
                                        setChanged(true);
                                    }}
                                    onChange={() => {}}
                                    id={'agreementNumber'}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleTextInput
                                    name={'counterNumber'}
                                    label={'Номер счётчика'}
                                    defaultValue={values.counter_number}
                                    onBlur={(value) => {
                                        setFieldValue('counter_number', value);
                                        setChanged(true);
                                    }}
                                    onChange={() => {}}
                                    id={'counterNumber'}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            {!companiesData.filter(item => item.title === values.company_title).length ?
                                <Grid item xs={4} style={{paddingRight: '15px',}}>
                                    <SimpleTextInput
                                        error={true}
                                        helperText={'Отделение не существует'}
                                        name={'companyTitle'}
                                        label={'Отделение'}
                                        defaultValue={values.company_title}
                                        // onChange={}
                                        id={'companyTitle'}
                                        disabled={true}
                                    />
                                </Grid> : null
                            }
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleSelectInput
                                    label={'Отделение'}
                                    options={companiesData.map(company => {
                                        return {
                                            title: company.title,
                                            value: company.title,
                                        }
                                    })}
                                    defaultValue={checkCompanyTitle(values.company_title) ? values.company_title : null}
                                    onChange={(value) => {
                                        setFieldValue('company_title', value);
                                        setControllersOption(filterControllers(value));
                                        setChanged(true);
                                    }}
                                    allowEmptyValue={true}
                                    id={'companyTitle'}
                                    formControlStyle={{width: '100%',}}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            {!controllersOption.filter(item => item.value === values.controller).length ?
                                <Grid item xs={4} style={{paddingRight: '15px',}}>
                                    <SimpleTextInput
                                        // error={true}
                                        // helperText={'Исполнитель не существует'}
                                        name={'controller'}
                                        label={'Исполнитель'}
                                        defaultValue={values.controller}
                                        // onChange={}
                                        id={'controller'}
                                        disabled={true}
                                    />
                                </Grid> : null
                            }
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleSelectInput
                                    label={'Исполнитель'}
                                    options={controllersOption}
                                    defaultValue={controllersOption.filter(item => item.value === values.controller)[0] ? values.controller : null}
                                    onChange={(value) => {
                                        setFieldValue('controller', value);
                                        setChanged(true);
                                    }}
                                    id={'controller'}
                                    formControlStyle={{width: '100%',}}
                                    allowEmptyValue={true}
                                    emptyValueTitle={'Не выбран'}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={8} style={{paddingRight: '15px',}}>
                                <SimpleTextInput
                                    error={!values.address}
                                    helperText={values.address ? '' : 'Адрес объекта не может быть пустым'}
                                    name={'address'}
                                    label={'Адрес объекта'}
                                    defaultValue={values.address}
                                    onBlur={(value) => {
                                        setFieldValue('address', value);
                                        setChanged(true);
                                    }}
                                    onChange={() => {}}
                                    id={'address'}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleTextInput
                                    name={'coords'}
                                    label={'Координаты объекта'}
                                    defaultValue={values.coords}
                                    // onChange={}
                                    id={'coords'}
                                    disabled
                                />
                            </Grid>
                            <Grid item xs={2} style={{paddingRight: '15px',}}>
                                <SimpleMapModal
                                    modalName={'coords'}
                                    onModalSubmit={(value) => {
                                        setFieldValue('coords', `[${value.x}, ${value.y}]`,);
                                        setChanged(true);
                                    }}
                                    coordX={values.coords ? +values.coords.match(/^\[(.+)\,\s(.+)\]$/)[1] : 0}
                                    coordY={values.coords ? +values.coords.match(/^\[(.+)\,\s(.+)\]$/)[2] : 0}
                                    modalTitle={'GPS координаты объекта'}
                                    startButtonTitle={'Изменить'}
                                    enableSearch={true}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <BasicDatePicker
                                    label={'Срок выполнения'}
                                    defaultValue={values.deadline ? new Date(values.deadline) : null}
                                    onChange={(value) => {
                                        setFieldValue('deadline', value);
                                        setChanged(true);
                                    }}
                                    minDate={new Date()}
                                    inputVariant={'outlined'}
                                    style={{width: '100%',}}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleSelectInput
                                    label={'Состояние / Доступ узла учёта'}
                                    options={selectServiceability}
                                    defaultValue={values.serviceability || 'Доступен / Исправен'}
                                    onChange={(value) => {
                                        setFieldValue('serviceability', value);
                                        setChanged(true);
                                    }}
                                    id={'serviceability'}
                                    formControlStyle={{width: '100%',}}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleTextInput
                                    name={'counterModel'}
                                    label={'Модель узла учёта'}
                                    defaultValue={values.counter_model}
                                    onBlur={(value) => {
                                        setFieldValue('counter_model', value);
                                        setChanged(true);
                                    }}
                                    onChange={() => {}}
                                    id={'counterModel'}
                                />
                            </Grid>
                            <Tooltip title={'Функционал временно недоступен'} arrow>
                                <Grid item xs={4} style={{paddingRight: '15px',}}>
                                    <SimpleSelectInput
                                        label={'Модель узла учёта'}
                                        options={[]}
                                        defaultValue={null}
                                        // onChange={}
                                        id={'counterModel'}
                                        formControlStyle={{width: '100%',}}
                                        disabled
                                    />
                                </Grid>
                            </Tooltip>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleTextInput
                                    name={'counterInitialValue'}
                                    label={'Показания счётчика при установке'}
                                    defaultValue={values.counter_initial_value}
                                    onBlur={(value) => {
                                        setFieldValue('counter_initial_value', value);
                                        setChanged(true);
                                    }}
                                    onChange={() => {}}
                                    id={'counterInitialValue'}
                                />
                            </Grid>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleTextInput
                                    name={'counterInitialDate'}
                                    label={'Дата установки'}
                                    defaultValue={values.counter_initial_date}
                                    onBlur={(value) => {
                                        setFieldValue('counter_initial_date', value);
                                        setChanged(true);
                                    }}
                                    onChange={() => {}}
                                    id={'counterInitialValue'}
                                    error={errors.counter_initial_date && touched.counter_initial_date}
                                    helperText={touched.counter_initial_date && errors.counter_initial_date && 'Дата должна быть в формате: дд.мм.гггг'}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleTextInput
                                    name={'currentCounterValue'}
                                    label={'Текущее показание счётчика'}
                                    defaultValue={values.current_counter_value}
                                    onBlur={(value) => {
                                        setFieldValue('current_counter_value', value);
                                        setChanged(true);
                                    }}
                                    onChange={() => {}}
                                    id={'currentCounterValue'}
                                />
                            </Grid>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleTextInput
                                    name={'currentCounterValueDate'}
                                    label={'Дата снятия показания'}
                                    defaultValue={values.current_counter_value_date}
                                    onBlur={(value) => {
                                        setFieldValue('current_counter_value_date', value);
                                        setChanged(true);
                                    }}
                                    onChange={() => {}}
                                    id={'currentCounterValueDate'}
                                    error={errors.current_counter_value_date && touched.current_counter_value_date}
                                    helperText={touched.current_counter_value_date && errors.current_counter_value_date && 'Дата должна быть в формате: дд.мм.гггг'}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleTextInput
                                    name={'terminalSeal'}
                                    label={'Клеммная пломба'}
                                    defaultValue={values.terminal_seal}
                                    onBlur={(value) => {
                                        setFieldValue('terminal_seal', value);
                                        setChanged(true);
                                    }}
                                    onChange={() => {}}
                                    id={'terminalSeal'}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleTextInput
                                    name={'antimagneticSeal'}
                                    label={'Антимагнитная пломба'}
                                    defaultValue={values.antimagnetic_seal}
                                    onBlur={(value) => {
                                        setFieldValue('antimagnetic_seal', value);
                                        setChanged(true);
                                    }}
                                    onChange={() => {}}
                                    id={'antimagneticSeal'}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleTextInput
                                    name={'sideSeal'}
                                    label={'Боковая пломба'}
                                    defaultValue={values.side_seal}
                                    onBlur={(value) => {
                                        setFieldValue('side_seal', value);
                                        setChanged(true);
                                    }}
                                    onChange={() => {}}
                                    id={'sideSeal'}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleTextInput
                                    name={'substation'}
                                    label={'Подстанция'}
                                    defaultValue={values.substation}
                                    onBlur={(value) => {
                                        setFieldValue('substation', value);
                                        setChanged(true);
                                    }}
                                    onChange={() => {}}
                                    id={'substation'}
                                />
                            </Grid>
                            <Tooltip title={'Функционал временно недоступен'} arrow>
                                <Grid item xs={4} style={{paddingRight: '15px',}}>
                                    <SimpleSelectInput
                                        label={'Подстанция'}
                                        options={[]}
                                        defaultValue={null}
                                        // onChange={}
                                        id={'substation'}
                                        formControlStyle={{width: '100%',}}
                                        disabled
                                    />
                                </Grid>
                            </Tooltip>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleTextInput
                                    name={'feeder'}
                                    label={'Фидер'}
                                    defaultValue={values.feeder}
                                    onBlur={(value) => {
                                        setFieldValue('feeder', value);
                                        setChanged(true);
                                    }}
                                    onChange={() => {}}
                                    id={'feeder'}
                                />
                            </Grid>
                            <Tooltip title={'Функционал временно недоступен'} arrow>
                                <Grid item xs={4} style={{paddingRight: '15px',}}>
                                    <SimpleSelectInput
                                        label={'Фидер'}
                                        options={[]}
                                        defaultValue={null}
                                        // onChange={}
                                        id={'feeder'}
                                        formControlStyle={{width: '100%',}}
                                        disabled
                                    />
                                </Grid>
                            </Tooltip>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleTextInput
                                    name={'transformer'}
                                    label={'Трансформатор'}
                                    defaultValue={values.transformer}
                                    onBlur={(value) => {
                                        setFieldValue('transformer', value);
                                        setChanged(true);
                                    }}
                                    onChange={() => {}}
                                    id={'transformer'}
                                />
                            </Grid>
                            <Tooltip title={'Функционал временно недоступен'} arrow>
                                <Grid item xs={4} style={{paddingRight: '15px',}}>
                                    <SimpleSelectInput
                                        label={'Трансформатор'}
                                        options={[]}
                                        defaultValue={null}
                                        // onChange={}
                                        id={'transformer'}
                                        formControlStyle={{width: '100%',}}
                                        disabled
                                    />
                                </Grid>
                            </Tooltip>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleTextInput
                                    name={'electricLine'}
                                    label={'Линия'}
                                    defaultValue={values.electric_line}
                                    onBlur={(value) => {
                                        setFieldValue('electric_line', value);
                                        setChanged(true);
                                    }}
                                    onChange={() => {}}
                                    id={'electricLine'}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleTextInput
                                    name={'electricPole'}
                                    label={'Столб'}
                                    defaultValue={values.electric_pole}
                                    onBlur={(value) => {
                                        setFieldValue('electric_pole', value);
                                        setChanged(true);
                                    }}
                                    onChange={() => {}}
                                    id={'electricPole'}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleTextInput
                                    name={'lodgersCount'}
                                    label={'Количество проживающих'}
                                    defaultValue={values.lodgers_count ? values.lodgers_count.toString() : ''}
                                    onBlur={(value) => {
                                        setFieldValue('lodgers_count', value);
                                        setChanged(true);
                                    }}
                                    onChange={() => {}}
                                    id={'lodgersCount'}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleTextInput
                                    name={'roomsCount'}
                                    label={'Количество комнат'}
                                    defaultValue={values.rooms_count ? values.rooms_count.toString() : ''}
                                    onBlur={(value) => {
                                        setFieldValue('rooms_count', value);
                                        setChanged(true);
                                    }}
                                    onChange={() => {}}
                                    id={'roomsCount'}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleTextInput
                                    name={'phones'}
                                    label={'Телефон'}
                                    defaultValue={values.phones}
                                    onBlur={(value) => {
                                        setFieldValue('phones', value);
                                        setChanged(true);
                                    }}
                                    onChange={() => {}}
                                    id={'phones'}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '20px',}}>
                            <Grid item xs={2} style={{paddingRight: '15px',}}>
                                <Button
                                    onClick={() => {
                                        handleReset();
                                        setChanged(false);
                                    }}
                                    variant={'outlined'}
                                    size={'small'}
                                    className={'base_styled_btn'}
                                    fullWidth
                                >
                                    Отменить изменения
                                </Button>
                            </Grid>
                            <Grid item xs={2} style={{paddingRight: '15px',}}>
                                <Button
                                    onClick={() => {
                                        handleSubmit();
                                        saveRecordChanges(values)
                                    }}
                                    variant={'contained'}
                                    size={'small'}
                                    className={'base_styled_btn'}
                                    fullWidth
                                >
                                    Сохранить изменения
                                </Button>
                            </Grid>
                            <Grid item xs={2} style={{paddingRight: '15px',}}>
                                <Button
                                    onClick={() => {
                                        handleSubmit();
                                        if (isValid) {
                                            acceptRecord(values);
                                        }
                                    }}
                                    variant={'contained'}
                                    size={'small'}
                                    className={'base_styled_btn'}
                                    fullWidth
                                >
                                    Отправить в задачи
                                </Button>
                            </Grid>
                        </Grid>
                    </Box>
                )}
            </Formik>
            <BackButtonComponent changed={changed}/>
        </>
    )
}