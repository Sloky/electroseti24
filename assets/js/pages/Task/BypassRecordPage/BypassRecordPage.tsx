import React, {useEffect, useState} from "react";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import {RecordData, SimpleFlashData} from "../../../interfaces/interfaces";
import {useHistory, useParams} from "react-router";
import BypassRecordEditCard from "./BypassRecordEditCard";
import {Grid} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {appStateInterface} from "../../../store/appState";
import FlashComponent from "../../../components/Flashes/FlashComponent";
import {
    acceptRecordToTasksRequest,
    editBypassRecordRequest,
    getBypassRecordRequest,
    getCompaniesForFilterRequest,
    getControllersForTasksRequest
} from "../../../requests/Request";
import {AppAction, TasksAction} from "../../../store/actions/actions";

export default function BypassRecordPage() {
    const [isLoading, setIsLoading] = useState(true);
    const [record, setRecord] = useState<RecordData>();
    const [companiesData, setCompaniesData] = useState();
    const [controllersData, setControllersData] = useState();

    const [recordLoaded, setRecordLoaded] = useState(false);
    const [companiesLoaded, setCompaniesLoaded] = useState(false);
    const [controllersLoaded, setControllersLoaded] = useState(false);

    const [flashError, setFlashError] = useState<SimpleFlashData>({status: false})
    const [flashWarning, setFlashWarning] = useState({status: false, message: ''});
    const [flashInfo, setFlashInfo] = useState({status: false, message: ''});
    const [flashSuccess, setFlashSuccess] = useState({status: false, message: ''});

    const params: {id: string} = useParams();

    const dispatch = useDispatch();
    const history = useHistory();

    const bypassRecords = useSelector((state: appStateInterface) => state.bypassRecords);

    const breadcrumbsItems = [
        {
            itemTitle: 'Обходной лист',
            itemSrc: '/bypass',
        },
        {
            itemTitle: record && record.counter_number ? `Запись ${record.counter_number}` : 'Запись',
            // itemSrc: ``,
        },
    ]

    const onCloseErrorFlash = () => {
        setFlashError(prevState => {return {...prevState, status: false}})
    };

    const onCloseWarningFlash = () => {
        setFlashWarning(prevState => {return {...prevState, status: false}})
    };

    const onCloseInfoFlash = () => {
        setFlashInfo(prevState => {return {...prevState, status: false}})
    };

    const onCloseSuccessFlash = () => {
        setFlashSuccess(prevState => {return {...prevState, status: false}})
    };

    useEffect(() => {
        if (bypassRecords[params.id]) {
            setRecord(bypassRecords[params.id]);
        }
    }, [bypassRecords]);

    useEffect(() => {
        dispatch(AppAction.showLoader());
        setRecord(bypassRecords[params.id]);
        getBypassRecordRequest(params.id).then(
            result => {
                if (result.type == 'success') {
                    setRecord(result.data);
                } else {
                    console.log('Error: ', result.data);
                    setFlashError({
                        status: true,
                        message: result.data.message,
                    });
                }
            }
        ).finally(() => {
            setRecordLoaded(true);
        });
        getCompaniesForFilterRequest().then(
            result => {
                if (result.type == 'success') {
                    setCompaniesData(result.data);
                } else {
                    console.log('Error: ', result.data);
                    setFlashError({
                        status: true,
                        message: result.data,
                    });
                }
            }
        ).finally(() => {
            setCompaniesLoaded(true);
        });
        getControllersForTasksRequest().then(
            result => {
                if (result.type == 'success') {
                    setControllersData(result.data);
                } else {
                    console.log('Error: ', result.data);
                    setFlashError({
                        status: true,
                        message: result.data,
                    });
                }
            }
        ).finally(() => {
            setControllersLoaded(true);
        })
    }, []);

    useEffect(() => {
        if (recordLoaded && companiesLoaded && controllersLoaded) {
            dispatch(AppAction.hideLoader());
            setIsLoading(false);
        }
    }, [recordLoaded, companiesLoaded, controllersLoaded]);

    const showFlash = (type, message, messageHeading?) => {
        switch (type) {
            case 'error':
                setFlashError({status: true, messageHeading: messageHeading, message: message});
                break;
            case 'warning':
                setFlashWarning({status: true, message: message});
                break;
            case 'info':
                setFlashInfo({status: true, message: message});
                break;
            case 'success':
                setFlashSuccess({status: true, message: message});
                break;
            case 'disableFlashes':
                setFlashSuccess({status: false, message: message,});
                setFlashError({status: false,});
                // setFlashInfo({status: false, message: message,});
                // setFlashWarning({status: false, message: message,});
        }
    }

    const saveRecord = (data) => {
        dispatch(AppAction.showLoader());
        editBypassRecordRequest(record.id, data).then(
            result => {
                if (result.type == 'success') {
                    setRecord(result.data);
                    dispatch(TasksAction.changeBypassRecordAction(result.data.task));
                    // history.push('/bypass');
                    showFlash('disableFlashes', '');
                    showFlash('success', 'Запись была успешно изменена');
                } else {
                    console.log('Error task data: ', result.data);
                    showFlash('error', 'Не удалось записать изменения');
                }
            }
        ).finally(() => {
            dispatch(AppAction.hideLoader());
        });
    }

    const acceptRecord = (data) => {
        setFlashSuccess({status: false, message: ''});
        dispatch(AppAction.showLoader());
        editBypassRecordRequest(record.id, data).then(
            result => {
                if (result.type == 'success') {
                    setRecord(result.data);
                    dispatch(TasksAction.changeBypassRecordAction(result.data.task));
                } else {
                    console.log('Error task data: ', result.data);
                    showFlash('error', 'Не удалось записать изменения', 'Ошибка.');
                }
            }
        ).then(() => {
            acceptRecordToTasksRequest(record.id)
                .then(result => {
                    if (result.type == 'success') {
                        history.push('/bypass');
                    } else {
                        console.log('Error record data: ', result.data);
                        showFlash('error', result.data, 'Ошибка.');
                    }
                })
        }).finally(() => {
            dispatch(AppAction.hideLoader());
        })
    }

    return (
        <PageLayout
            breadcrumbsItems={breadcrumbsItems}
        >
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                {record ? `Запись "${record.address ? record.address + ', ' : ''}узел учёта ${record.counter_number || 'Нет данных'}"` : `Запись № ${params.id}`}
            </Grid>
            {!isLoading && <BypassRecordEditCard
                record={record}
                companiesData={companiesData}
                controllersData={controllersData}
                onFlash={(type, message, messageHeading) => {showFlash(type, message, messageHeading)}}
                onSaveRecord={saveRecord}
                onAcceptRecord={acceptRecord}
            >
                {record.errors.map((item, index) => <FlashComponent
                        key={index}
                        message={item.message}
                        messageHeading={item.title}
                        // onCloseFlash={onCloseErrorFlash}
                        showFlash={!!record.errors.length}
                        flashType="error"
                        flashWidth="100%"
                    />
                )}
                <FlashComponent
                    message={flashError.message}
                    messageHeading={flashError.messageHeading}
                    // onCloseFlash={onCloseErrorFlash}
                    showFlash={flashError.status}
                    flashType="error"
                    flashWidth="100%"
                />
                <FlashComponent
                    message={flashWarning.message}
                    onCloseFlash={onCloseWarningFlash}
                    showFlash={flashWarning.status}
                    flashType="warning"
                />
                <FlashComponent
                    message={flashInfo.message}
                    onCloseFlash={onCloseInfoFlash}
                    showFlash={flashInfo.status}
                    flashType="info"
                />
                <FlashComponent
                    message={flashSuccess.message}
                    showFlash={flashSuccess.status}
                    onCloseFlash={onCloseSuccessFlash}
                    flashType={'success'}
                />
            </BypassRecordEditCard>
            }
        </PageLayout>
    )
}