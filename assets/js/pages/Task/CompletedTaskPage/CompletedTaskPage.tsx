import React, {useEffect, useState} from "react";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import {CompleteTaskData, CounterTypeData, SimpleFlashData} from "../../../interfaces/interfaces";
import {useParams} from "react-router";
import {getCompletedTaskRequest} from "../../../requests/Request";
import {useSelector} from "react-redux";
import {appStateInterface} from "../../../store/appState";
import {getFullAddress} from "../../../functions/functions";
import CompletedTaskCard from "./CompletedTaskCard";
import {Grid} from "@material-ui/core";
import FlashComponent from "../../../components/Flashes/FlashComponent";

export default function CompletedTaskPage() {
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [task, setTask] = useState<CompleteTaskData>();
    const [counterModels, setCounterModels] = useState<CounterTypeData[]>();
    const [flashError, setFlashError] = useState<SimpleFlashData>({status: false});
    const [flashInfo, setFlashInfo] = useState({status: false, message: ''});
    const params: {id: string} = useParams();

    const completedTasks = useSelector((state: appStateInterface) => state.completedTasks);
    const counterTypes = useSelector((state: appStateInterface) => state.counterTypes);

    const breadcrumbsItems = [
        {
            itemTitle: 'Принятые задачи',
            itemSrc: '/completed-tasks',
        },
        {
            itemTitle: task && task.counter_number ? `Задача ${task.counter_number}` : 'Задача',
            // itemSrc: ``,
        },
    ]

    const onCloseErrorFlash = () => {
        setFlashError(prevState => ({...prevState, status: false}))
    }

    const onCloseInfoFlash = () => {
        setFlashInfo(prevState => ({...prevState, status: false}))
    };

    useEffect(() => {
        setTask(completedTasks[params.id]);
        let models = Object.values(counterTypes);
        if (models.length > 0) {
            setCounterModels(models);
            setIsLoading(false);
        }
        getCompletedTaskRequest(params.id).then(
            result => {
                if (result.type == 'success') {
                    setTask(result.data.task);
                } else {
                    console.log('Error users data: ', result.data);
                    setFlashError({
                        status:true,
                        message: result.data
                    });
                }
            }
        ).finally(() => {
            setIsLoading(false);
        });
    }, []);

    return (
        <PageLayout
            breadcrumbsItems={breadcrumbsItems}
        >
            {!isLoading &&
                <>
                    <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                        {task ? `Задача "${getFullAddress(task)}, узел учёта ${task.counter_number}"` : 'Задача'}
                    </Grid>
                    <CompletedTaskCard
                        task={task}
                        counterTypes={counterModels || []}
                        onErrorOccured={(message) => {setFlashError({status: true, message: message})}}
                    >
                        <FlashComponent
                            message={flashError.message}
                            messageHeading={flashError.messageHeading}
                            // onCloseFlash={onCloseErrorFlash}
                            showFlash={flashError.status}
                            flashType="error"
                            flashWidth="100%"
                        />
                        <FlashComponent
                            message={flashInfo.message}
                            onCloseFlash={onCloseInfoFlash}
                            showFlash={flashInfo.status}
                            flashType="info"
                        />
                    </CompletedTaskCard>
                </>
            }
        </PageLayout>
    );
}