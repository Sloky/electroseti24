import React, {useEffect, useState} from "react";
import {
    CompleteTaskData,
    CounterTypeData,
    TaskToCheckData,
    TaskElementInterface,
} from "../../../interfaces/interfaces";
import "./CompletedTaskCard.css";
import {TASK_COUNTER_PHYSICAL_STATUSES, TASK_PRIORITY, TASK_STATUS, TASK_TYPE} from "../../../constants/constants";
import {
    getCompletedTaskExcelRequest,
    getCompletedTaskPhotosRequest,
} from "../../../requests/Request";
import SimpleMapModal from "../../../components/Modal/SimpleMapModal/SimpleMapModal";
import SimplePhotoGalleryModal from "../../../components/Modal/SimplePhotoGalleryModal/SimplePhotoGalleryModal";
import {changeView, getFullAddress, getTaskCheckPhotos} from "../../../functions/functions";
import {Box, Button, Grid} from "@material-ui/core";
import {ArrowBackIos} from "@material-ui/icons";
import {useHistory} from "react-router";
import {THEME} from "../../../theme";
import MapComponent from "../../../components/Other/MapComponent/MapComponent";
import {useDispatch} from "react-redux";
import {AppAction} from "../../../store/actions/actions";

interface TaskToCheckEditPageProps {
    task: CompleteTaskData
    counterTypes?: CounterTypeData[]
    children?: any
    onErrorOccured(message: string): void
}

function TaskDataItem(props: TaskElementInterface) {
    return (
        <Grid container style={{marginBottom: '15px', display: 'flex', alignItems: 'center',}}>
            <Grid item xs={3} sx={{color: THEME.PLACEHOLDER_TEXT, fontSize: '18px', fontWeight: '500',}}>
                <Box style={{width: '90%',}}>
                    {props.title}
                </Box>
            </Grid>
            <Grid item xs={6}>
                <Box style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center', width: '570px',}}>
                    <Box sx={{fontSize: '18px', color: 'text.primary', ...changeView(props.taskTextColor, props.taskBackgroundColor)}}>
                        {props.value}
                    </Box>
                    <Box>
                        {props.modal}
                    </Box>
                </Box>
            </Grid>
        </Grid>
    );
}

const getLodgersCountData = (task: TaskToCheckData) => {
    let oldValue = task.lodgers_count ? task.lodgers_count.toString() : 'Нет данных';
    return task.changed_lodgers_count
        ? `${task.changed_lodgers_count.toString()} (Предыдущее значение: ${oldValue})`
        : oldValue;
};

const getRoomsCountData = (task: TaskToCheckData) => {
    let oldValue = task.rooms_count ? task.rooms_count.toString() : 'Нет данных';
    return task.changed_rooms_count
        ? `${task.changed_rooms_count.toString()} (Предыдущее значение: ${oldValue})`
        : oldValue;
};

export default function CompletedTaskCard(props: TaskToCheckEditPageProps) {
    const [task, setTask] = useState<CompleteTaskData>(props.task);

    const history = useHistory();
    const dispatch = useDispatch();

    useEffect(() => {
        setTask(props.task);
    }, [props.task]);

    let address = getFullAddress(task);
    let date = new Date(task.deadline).toLocaleDateString('ru');
    let executedDate = new Date(task.executed_date).toLocaleString('ru');
    let acceptanceDate = new Date(task.acceptance_date).toLocaleString('ru');
    let executorFullName = task.executor !== null
        ? task.executor.surname + ' ' + task.executor.name + ' ' + task.executor.patronymic
        : 'Не назначен';
    let operatorFullName = task.operator !== null
        ? task.operator.surname + ' ' + task.operator.name + ' ' + task.operator.patronymic
        : 'Не назначен';
    let coords = task.coordinate_x && task.coordinate_y
        ? '\[' + task.coordinate_x + ', ' + task.coordinate_y + '\]'
        : 'Нет данных';
    let newCoords = task.new_coordinate_x && task.new_coordinate_y
        ? '\[' + task.new_coordinate_x + ', ' + task.new_coordinate_y + '\]'
        : 'Нет данных';
    let operatorComment = task.operator_comment || 'Нет данных';
    let controllerComment = task.controller_comment || 'Нет данных';

    let counterPhotos = getTaskCheckPhotos(task, 'counter_photo');

    const onUploadPhotosClick = () => {
        dispatch(AppAction.showLoader());
        getCompletedTaskPhotosRequest(task.id)
            .then((result) => {
                if (result.type == 'success') {
                    let link = document.createElement('a');
                    let periodDate = new Date(task.period);
                    let month = periodDate.getMonth() < 10 ? '0' + periodDate.getMonth() : periodDate.getMonth();
                    let period = periodDate.getFullYear() + '-' + month;
                    link.download = `${period + '_' + task.agreement_number + '_' + task.counter_number}.zip`;

                    let blob = new Blob([result.data], {type: 'application/zip'});

                    link.href = URL.createObjectURL(blob);

                    link.click();

                    URL.revokeObjectURL(link.href);
                }
            })
            .finally(() => {
                dispatch(AppAction.hideLoader());
            });
    };

    const onUploadExcelClick = () => {
        dispatch(AppAction.showLoader());
        getCompletedTaskExcelRequest(task.id)
            .then((result) => {
                if (result.type == 'success') {
                    let link = document.createElement('a');
                    let periodDate = new Date(task.period);
                    let month = periodDate.getMonth() < 10 ? '0' + periodDate.getMonth() : periodDate.getMonth();
                    let period = periodDate.getFullYear() + '-' + month;
                    link.download = `${period + '_' + task.agreement_number + '_' + task.counter_number}.xlsx`;

                    let blob = new Blob(
                        [result.data],
                        {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'}
                    );

                    link.href = URL.createObjectURL(blob);

                    link.click();

                    URL.revokeObjectURL(link.href);
                }
            })
            .finally(() => {
                dispatch(AppAction.hideLoader());
            });
    };

    const taskDataElements: TaskElementInterface[] = [
        {
            title: 'Тип задачи',
            value: TASK_TYPE[task.task_type].title,
            modal: null
        },
        {
            title: 'Узел учёта',
            value: task.counter_number,
            modal: null
        },
        {
            title: 'Отделение',
            value: task.company.title,
            modal: null
        },
        {
            title: 'Адрес',
            value: address,
            modal: null
        },
        {
            title: 'Приоритет',
            value: TASK_PRIORITY[task.priority].title,
            modal: null
        },
        {
            title: 'Статус',
            value: TASK_STATUS[task.status].title,
            modal: null
        },
        {
            title: 'Исполнитель',
            value: executorFullName,
            modal: null
        },
        {
            title: 'Принимающий',
            value: operatorFullName,
            modal: null
        },
        {
            title: 'Срок выполнения',
            value: date,
            modal: null
        },
        {
            title: 'Исполнено (дата и время)',
            value: executedDate,
            modal: null
        },
        {
            title: 'Принято (дата и время)',
            value: acceptanceDate,
            modal: null
        },
        {
            title: 'Примечание оператора',
            value: operatorComment,
            modal: null
        },
        {
            title: 'Примечание исполнителя',
            value: controllerComment,
            modal: null,
            taskTextColor: task.controller_comment ? 'warning' : undefined,
            taskBackgroundColor: null,
        },
    ];

    const networkDataElements: TaskElementInterface[] = [
        {
            title: 'Подстанция',
            value: task.substation ? task.substation.title : task.temporary_substation || 'Нет данных',
            modal: null
        },
        {
            title: 'Трансформатор',
            value: task.transformer ? task.transformer.title : task.temporary_transformer || 'Нет данных',
            modal: null
        },
        {
            title: 'Фидер',
            value: task.feeder ? task.feeder.title : task.temporary_feeder || 'Нет данных',
            modal: null
        },
        {
            title: '№ Линии',
            value: task.line_number || 'Нет данных',
            modal: null
        },
        {
            title: '№ Столба',
            value: task.electric_pole_number || 'Нет данных',
            modal: null
        },
    ]

    const counterMainDataElements: TaskElementInterface[] = [
        {
            title: 'Модель узла учёта',
            value: task.counter_type ? task.counter_type.title : task.temporary_counter_type || 'Нет данных',
            modal: null
        },
        {
            title: 'Новая модель узла учёта',
            value: task.changed_counter_type ? task.changed_counter_type.title : task.changed_counter_type_string,
            modal:
                <Box style={{display: 'flex'}}>
                    <SimplePhotoGalleryModal
                        modalName={'counterTypeGalleryModal'}
                        modalTitle={'Фотографии прибора учёта'}
                        photos={getTaskCheckPhotos(task, 'counter_photo')}
                    />
                </Box>,
            show: !!task.changed_counter_type,
            taskTextColor: 'warning',
            taskBackgroundColor: null,
        },
        // {
        //     title: 'Номер узла учёта',
        //     value: task.counter_number,
        //     modal: null,
        // },
        {
            // title: 'Новый номер узла учёта',
            title: 'Номер узла учёта',
            value: task.changed_counter_number,
            modal:
                <Box style={{display: 'flex'}}>
                    <SimplePhotoGalleryModal
                        modalName={'counterGalleryModal'}
                        modalTitle={'Фотографии прибора учёта'}
                        photos={getTaskCheckPhotos(task, 'counter_photo')}
                    />
                </Box>,
            show: !!task.changed_counter_number,
            taskTextColor: 'warning',
            taskBackgroundColor: null,
        },
        {
            title: 'Состояние/доступ узла учёта',
            value: TASK_COUNTER_PHYSICAL_STATUSES[task.counter_physical_status].title,
            modal:
                <Box style={{display: 'flex'}}>
                    {
                        counterPhotos.length > 0
                        &&  <SimplePhotoGalleryModal
                            modalName={'counterTypeGalleryModal'}
                            modalTitle={'Фотографии прибора учёта'}
                            photos={counterPhotos}
                        />
                    }
                </Box>
        },
        // {
        //     title: 'Клеммная пломба',
        //     value: task.terminal_seal || 'Нет данных',
        //     modal: null
        // },
        {
            // title: 'Новая клеммная пломба',
            title: 'Клеммная пломба',
            value: task.changed_terminal_seal,
            modal:
                <Box style={{display: 'flex'}}>
                    <SimplePhotoGalleryModal
                        modalName={'terminalSealGalleryModal'}
                        modalTitle={'Фотографии клеммной пломбы'}
                        photos={getTaskCheckPhotos(task, 'terminal_seal')}
                    />
                </Box>,
            show: !!task.changed_terminal_seal,
            taskTextColor: 'warning',
            taskBackgroundColor: null,
        },
        // {
        //     title: 'Антимагнитная пломба',
        //     value: task.anti_magnetic_seal || 'Нет данных',
        //     modal: null
        // },
        {
            // title: 'Новая антимагнитная пломба',
            title: 'Антимагнитная пломба',
            value: task.changed_antimagnetic_seal,
            modal:
                <Box style={{display: 'flex'}}>
                    <SimplePhotoGalleryModal
                        modalName={'antimagneticSealGalleryModal'}
                        modalTitle={'Фотографии антимагнитной пломбы'}
                        photos={getTaskCheckPhotos(task, 'antimagnetic_seal')}
                    />
                </Box>,
            show: !!task.changed_antimagnetic_seal,
            taskTextColor: 'warning',
            taskBackgroundColor: null,
        },
        // {
        //     title: 'Боковая пломба',
        //     value: task.side_seal || 'Нет данных',
        //     modal: null
        // },
        {
            // title: 'Новая боковая пломба',
            title: 'Боковая пломба',
            value: task.changed_side_seal,
            modal:
                <Box style={{display: 'flex'}}>
                    <SimplePhotoGalleryModal
                        modalName={'sideSealGalleryModal'}
                        modalTitle={'Фотографии боковой пломбы'}
                        photos={getTaskCheckPhotos(task, 'side_seal')}
                    />
                </Box>,
            show: !!task.changed_side_seal,
            taskTextColor: 'warning',
            taskBackgroundColor: null,
        },
    ];

    const counterDataElements: TaskElementInterface[] = [
        {
            title: 'Начальные показания узла учёта (кВч)',
            value: task.last_counter_value.toString(),
            modal: null
        },
        {
            title: 'Конечные показания узла учёта (кВч)',
            value: task.current_counter_value ? task.current_counter_value.toString() : 'Отсутствуют',
            taskTextColor: task.consumption < 0 ? 'danger' : undefined,
            taskBackgroundColor: null,
            modal: task.current_counter_value ?
                <Box style={{display: 'flex'}}>
                    <SimplePhotoGalleryModal
                        modalName={'counterValueGalleryModal'}
                        modalTitle={'Фотографии показаний прибора учёта'}
                        photos={getTaskCheckPhotos(task, 'counter_value')}
                    />
                </Box>
                : null
        },
        {
            title: 'Разность показаний (кВч)',
            value: task.consumption ? task.consumption.toString() : 'Нет данных',
            taskTextColor: task.consumption < 0 ? 'danger' : undefined,
            taskBackgroundColor: null,
            modal: null
        },
    ];

    const subscriberDataElements: TaskElementInterface[] = [
        {
            title: 'Наименование абонента',
            value: task.subscriber_title,
            modal: null
        },
        {
            title: 'Платежный код/Номер договора',
            value: task.agreement_number,
            modal: null
        },
        {
            title: 'Количество проживающих',
            value: getLodgersCountData(task),
            taskTextColor: task.changed_lodgers_count ? 'warning' : undefined,
            taskBackgroundColor: null,
            modal: null
        },
        {
            title: 'Количество комнат',
            value: getRoomsCountData(task),
            taskTextColor: task.changed_rooms_count ? 'warning' : undefined,
            taskBackgroundColor: null,
            modal: null
        },
        {
            title: 'Телефон',
            value: task.subscriber_phones.length !== 0 ? task.subscriber_phones[0] : 'Нет данных',
            modal: null
        },
    ];

    return (
        <>
            <Button
                onClick={() => history.goBack()}
                startIcon={<ArrowBackIos/>}
                sx={{fontSize: '14px', fontWeight: '500', marginBottom: '20px',}}
                size="small"
            >
                Назад
            </Button>
            {props.children}
                <>
                    <Grid container style={{marginBottom: '30px',}}>
                        <Grid item xs={2} style={{paddingRight: '15px',}}>
                            <Button
                                onClick={onUploadExcelClick}
                                variant={'contained'}
                                className={'base_styled_btn'}
                                fullWidth
                            >
                                Выгрузить в Excel
                            </Button>
                        </Grid>
                        <Grid item xs={2} style={{paddingRight: '15px',}}>
                            <Button
                                onClick={onUploadPhotosClick}
                                variant={'outlined'}
                                className={'base_styled_btn'}
                                fullWidth
                            >
                                Выгрузить фотографии
                            </Button>
                        </Grid>
                    </Grid>
                    <Box sx={{
                        marginBottom: '30px',
                        padding: '30px 30px 15px 30px',
                        bgcolor: 'background.default',
                        borderTopRightRadius: 10,
                        borderBottomRightRadius: 10,
                        boxShadow: '0px 2px 5px 0px rgba(138, 193, 233, 0.2)',
                    }}>
                        <Grid container>
                            <Grid item xs={6}>
                                {taskDataElements.map((element, index) => (
                                    <Grid container key={index} style={{marginBottom: '15px',}}>
                                        <Grid item xs={4} sx={{color: THEME.PLACEHOLDER_TEXT, fontSize: '18px', fontWeight: '500',}}>
                                            <Box style={{width: '90%',}}>
                                                {element.title}
                                            </Box>
                                        </Grid>
                                        <Grid item xs={8} sx={{color: 'text.primary', fontSize: '18px',}}>
                                            <Box style={{width: '80%', ...changeView(element.taskTextColor, element.taskBackgroundColor)}}>
                                                {element.value}
                                            </Box>
                                        </Grid>
                                    </Grid>
                                ))}
                            </Grid>
                            <Grid item xs={6}>
                                <Box style={{marginBottom: '30px',}}>
                                    <Box sx={{color: THEME.PLACEHOLDER_TEXT, fontSize: '18px', fontWeight: '500', marginBottom: '10px',}}>
                                        Координаты объекта
                                    </Box>
                                    <Box style={{display: 'flex', justifyContent: 'space-between', marginBottom: '15px',}}>
                                        <Box sx={{color: 'text.primary', fontSize: '18px', width: '90%',}}>{coords}</Box>
                                        <SimpleMapModal
                                            coordX={task.coordinate_x}
                                            coordY={task.coordinate_y}
                                            modalName={'coordinates'}
                                            modalTitle={'GPS координаты объекта задачи'}
                                            startButtonTitle={'Посмотреть'}
                                            acceptButtonShow={false}
                                            onModalSubmit={() => {}}
                                            enableSearch={false}
                                            disableStartButton={!task.coordinate_x && !task.coordinate_y}
                                        />
                                    </Box>
                                    {task.coordinate_x && task.coordinate_y && (
                                        <MapComponent
                                            coordX={task.coordinate_x}
                                            coordY={task.coordinate_y}
                                            draggable={false}
                                            mapWidth={'100%'}
                                            mapHeight={'20vh'}
                                            enableSearch={false}
                                        />
                                    )}
                                </Box>
                                <Box>
                                    <Box sx={{color: THEME.PLACEHOLDER_TEXT, fontSize: '18px', fontWeight: '500', marginBottom: '10px',}}>
                                        Координаты места выполнения задачи
                                    </Box>
                                    <Box style={{display: 'flex', justifyContent: 'space-between', marginBottom: '15px',}}>
                                        <Box sx={{color: 'text.primary', fontSize: '18px', width: '90%',}}>{newCoords}</Box>
                                        <SimpleMapModal
                                            coordX={task.new_coordinate_x}
                                            coordY={task.new_coordinate_y}
                                            modalName={'newCoordinates'}
                                            modalTitle={'GPS координаты места выполнения задачи'}
                                            startButtonTitle={'Посмотреть'}
                                            acceptButtonShow={false}
                                            onModalSubmit={() => {}}
                                            enableSearch={false}
                                            disableStartButton={!task.new_coordinate_x && !task.new_coordinate_y}
                                        />
                                    </Box>
                                    {task.new_coordinate_x && task.new_coordinate_y && (
                                        <MapComponent
                                            coordX={task.new_coordinate_x}
                                            coordY={task.new_coordinate_y}
                                            draggable={false}
                                            mapWidth={'100%'}
                                            mapHeight={'20vh'}
                                            enableSearch={false}
                                        />
                                    )}
                                </Box>
                            </Grid>
                        </Grid>
                    </Box>
                    <Box style={{marginBottom: '30px',}}>
                        <Grid sx={{color: 'secondary.main', fontSize: '18px', fontWeight: '500', marginBottom: '20px',}}>Данные сети</Grid>
                        <Grid container>
                            {networkDataElements.map((element, index) => (
                                <Grid item xs={6} key={index} style={{marginBottom: '10px', display: 'flex',}}>
                                    <Grid container>
                                        <Grid item xs={4} sx={{color: THEME.PLACEHOLDER_TEXT, fontSize: '18px', fontWeight: '500',}}>{element.title}</Grid>
                                        <Grid item xs={8} sx={{color: 'text.primary', fontSize: '18px', width: '90%',}}>{element.value}</Grid>
                                    </Grid>
                                </Grid>
                            ))}
                        </Grid>
                    </Box>
                    <Box sx={{
                        marginBottom: '1px',
                        padding: '30px 30px 15px 30px',
                        bgcolor: 'background.default',
                        borderTopRightRadius: 10,
                        // borderBottomRightRadius: 10,
                        boxShadow: '0px 2px 5px 0px rgba(138, 193, 233, 0.2)',
                    }}>
                        <Grid sx={{marginBottom: '20px', color: 'text.primary', fontSize: '18px', fontWeight: '500',}}>Данные узла учёта</Grid>
                        {counterMainDataElements.map((element, index) => (
                            element.show !== false && <TaskDataItem
                                title={element.title}
                                taskBackgroundColor={element.taskBackgroundColor}
                                taskTextColor={element.taskTextColor}
                                key={index}
                                value={element.value}
                                modal={element.modal}
                            />
                        ))}
                        <Grid sx={{marginBottom: '20px', color: 'text.primary', fontSize: '18px', fontWeight: '500',}}>Показания узла учёта</Grid>
                        {counterDataElements.map((element, index) => (
                            element.show !== false && <TaskDataItem
                                title={element.title}
                                taskBackgroundColor={element.taskBackgroundColor}
                                taskTextColor={element.taskTextColor}
                                key={index}
                                value={element.value}
                                modal={element.modal}
                            />
                        ))}
                    </Box>
                    <Box sx={{
                        marginBottom: '30px',
                        padding: '30px 30px 15px 30px',
                        bgcolor: 'background.default',
                        // borderTopRightRadius: 10,
                        borderBottomRightRadius: 10,
                        boxShadow: '0px 2px 5px 0px rgba(138, 193, 233, 0.2)',
                    }}>
                        {subscriberDataElements.map((element, index) => (
                            element.show !== false && <TaskDataItem
                                title={element.title}
                                taskBackgroundColor={element.taskBackgroundColor}
                                taskTextColor={element.taskTextColor}
                                key={index}
                                value={element.value}
                                modal={element.modal}
                            />
                        ))}
                    </Box>
                </>
            <Button
                onClick={() => history.goBack()}
                startIcon={<ArrowBackIos/>}
                sx={{fontSize: '14px', fontWeight: '500',}}
                size="small"
            >
                Назад
            </Button>
        </>
    );
}