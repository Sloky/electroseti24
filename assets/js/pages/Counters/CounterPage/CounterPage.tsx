import React, {useEffect, useState} from "react";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import CounterCard from "./CounterCard";
import {useDispatch} from "react-redux";
import {useParams} from "react-router";
import {AppAction} from "../../../store/actions/actions";
import {getCounterCardDataRequest, getCounterValuesDataRequest} from "../../../requests/Request";

export default function CounterPage() {
    const [counterData, setCounterData] = useState(null);
    const [counterValues, setCounterValues] = useState(null);

    const [counterLoaded, setCounterLoaded] = useState(false);
    const [counterValuesLoaded, setCounterValuesLoaded] = useState(false);

    const dispatch = useDispatch();
    let params: any = useParams();
    let id = params.id;
    let type = params.type;

    const breadcrumbsItems = [
        {
            itemTitle: type === 'ul' ? 'Юр. лица' : 'Физ. лица',
            itemSrc: `/subscribers/${type}`,
        },
        {
            itemTitle: 'Карточка абонента',
            itemSrc: `/subscriber/${type}/${counterData ? counterData.subscriber.id : ''}`,
        },
        {
            itemTitle: 'Карточка договора',
            itemSrc: `/agreement-page/${type}/${counterData && counterData.agreement.id}`,
        },
        {
            itemTitle: 'Карточка узла учёта',
            // itemSrc: ``,
        },
    ]

    useEffect(() => {
        dispatch(AppAction.showLoader());
        getCounterCardDataRequest(id).then(
            result => {
                if (result.type === 'success') {
                    setCounterData(result.data[0]);
                }
            }
        ).finally(() => {
            setCounterLoaded(true);
        })
        getCounterValuesDataRequest(id, {page: 1, maxResult: 100,}).then(
            result => {
                if (result.type === 'success') {
                    setCounterValues(result.data[0].reverse());
                }
            }
        ).finally(() => {
            setCounterValuesLoaded(true);
        })
    }, []);

    useEffect(() => {
        if (counterLoaded && counterValuesLoaded) {
            dispatch(AppAction.hideLoader());
        }
    }, [counterLoaded, counterValuesLoaded]);

    const getCounterValues = (requestData) => {
        dispatch(AppAction.showLoader());
        getCounterValuesDataRequest(id, requestData).then(
            result => {
                if (result.type === 'success') {
                    setCounterValues(result.data[0].reverse());
                }
            }
        ).finally(() => {
            dispatch(AppAction.hideLoader());
        })
    }

    return (
        <PageLayout
            breadcrumbsItems={breadcrumbsItems}
        >
            <CounterCard
                counter={counterData}
                counterValues={counterValues}
                counterValuesCount={counterValues && counterValues.length}
                getCounterValues={getCounterValues}
            />
        </PageLayout>
    )
}