import React, {useEffect, useState} from "react";
import {useHistory, useParams} from "react-router";
import {
    Box,
    Button,
    Card,
    Grid,
    Pagination, Stack,
    Table, TableBody, TableCell,
    TableContainer,
    TableHead,
    TableRow,
} from "@material-ui/core";
import {THEME} from "../../../theme";
import SimpleMapModal from "../../../components/Modal/SimpleMapModal/SimpleMapModal";
import MapComponent from "../../../components/Other/MapComponent/MapComponent";
import TableItemsCustomizer from "../../Tasks/TasksTable/TableItemsCustomizer";
import {AgreementCounterData, CounterValuesData} from "../../../interfaces/interfaces";

interface CounterCardProps {
    counter: AgreementCounterData
    counterValues: CounterValuesData[]
    counterValuesCount: number
    getCounterValues(requestData: any): void
}

const listHeader = [
    'Месяц',
    'Показания счётчика',
    'Дата снятия показания',
]

const months = [
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь',
]

export default function CounterCard(props: CounterCardProps) {
    const [counter, setCounter] = useState(props.counter);
    const [counterValues, setCounterValues] = useState(props.counterValues);

    const [itemsCount, setItemsCount] = useState(100);
    const [totalItems, setTotalItems] = useState(props.counterValuesCount);
    const [pagesCount, setPagesCount] = useState<number>(Math.ceil(totalItems/itemsCount || 1));
    const [pageNumber, setPageNumber] = useState<number>(1);

    const history = useHistory();
    let params: any = useParams();
    let type = params.type;

    const onTableItemCountChange = (value: number) => {
        setItemsCount(value);
    };

    const onPageNumberChange = (value: number) => {
        setPageNumber(value);
    };

    useEffect(() => {
        setCounter(props.counter);
        setCounterValues(props.counterValues);
        setTotalItems(props.counterValuesCount);
    }, [props]);

    useEffect(() => {
        props.getCounterValues({page: pageNumber, maxResult: itemsCount,});
    }, [pageNumber, itemsCount]);

    useEffect(() => {
        setPagesCount(Math.ceil(totalItems/itemsCount || 1));
    }, [itemsCount, totalItems]);

    useEffect(() => {
        if (pageNumber > pagesCount) {
            setPageNumber(pagesCount);
        }
    }, [pagesCount]);

    return (
        <>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                Карточка узла учёта
            </Grid>
            {counter ?
                <>
                    <Card sx={{fontSize: '18px', fontWeight: '500', bgcolor: 'background.default', padding: '20px', marginBottom: '30px',}}>
                        <Grid container>
                            <Grid item xs={6}>
                                <Grid container style={{marginBottom: '15px',}}>
                                    <Grid item xs={5} sx={{color: THEME.PLACEHOLDER_TEXT, paddingRight: '15px',}}>Номер узла учёта</Grid>
                                    <Grid item xs={7}>{counter && counter['counter_title'] ? counter['counter_title'] : 'Нет данных'}</Grid>
                                </Grid>
                                <Grid container style={{marginBottom: '15px',}}>
                                    <Grid item xs={5} sx={{color: THEME.PLACEHOLDER_TEXT, paddingRight: '15px',}}>Модель узла учёта</Grid>
                                    <Grid item xs={7}>{counter && counter['counter_model'] ? counter['counter_model'] : 'Нет данных'}</Grid>
                                </Grid>
                                <Grid container style={{marginBottom: '15px',}}>
                                    <Grid item xs={5} sx={{color: THEME.PLACEHOLDER_TEXT, paddingRight: '15px',}}>Состояние / Доступ</Grid>
                                    <Grid item xs={7}>{counter ? (counter['counter_physical_status'] ? 'Недоступен / неисправен' : 'Доступен / исправен') : 'Нет данных'}</Grid>
                                </Grid>
                                <Grid container style={{marginBottom: '15px',}}>
                                    <Grid item xs={5} sx={{color: THEME.PLACEHOLDER_TEXT, paddingRight: '15px',}}>Адрес</Grid>
                                    <Grid item xs={7}>{counter && counter.address ? counter.address : 'Нет данных'}</Grid>
                                </Grid>
                                <Grid container style={{marginBottom: '15px',}}>
                                    <Grid item xs={5} sx={{color: THEME.PLACEHOLDER_TEXT, paddingRight: '15px',}}>Клеммная пломба</Grid>
                                    <Grid item xs={7}>{counter && counter['terminal_seal'] ? counter['terminal_seal'] : 'Нет данных'}</Grid>
                                </Grid>
                                <Grid container style={{marginBottom: '15px',}}>
                                    <Grid item xs={5} sx={{color: THEME.PLACEHOLDER_TEXT, paddingRight: '15px',}}>Антимагнитная пломба</Grid>
                                    <Grid item xs={7}>{counter && counter['antimagnetic_seal'] ? counter['antimagnetic_seal'] : 'Нет данных'}</Grid>
                                </Grid>
                                <Grid container style={{marginBottom: '15px',}}>
                                    <Grid item xs={5} sx={{color: THEME.PLACEHOLDER_TEXT, paddingRight: '15px',}}>Боковая пломба</Grid>
                                    <Grid item xs={7}>{counter && counter['side_seal'] ? counter['side_seal'] : 'Нет данных'}</Grid>
                                </Grid>
                                <Grid container style={{marginBottom: '15px',}}>
                                    <Grid item xs={5} sx={{color: THEME.PLACEHOLDER_TEXT, paddingRight: '15px',}}>Трансформатор</Grid>
                                    <Grid item xs={7}>{counter && counter.transformer ? counter.transformer.title : 'Нет данных'}</Grid>
                                </Grid>
                                <Grid container style={{marginBottom: '15px',}}>
                                    <Grid item xs={5} sx={{color: THEME.PLACEHOLDER_TEXT, paddingRight: '15px',}}>Показания счётчика при установке</Grid>
                                    <Grid item xs={7}>{counter && counter['initial_counter_value'] ? counter['initial_counter_value'] : 'Нет данных'}</Grid>
                                </Grid>
                                <Grid container style={{marginBottom: '15px',}}>
                                    <Grid item xs={5} sx={{color: THEME.PLACEHOLDER_TEXT, paddingRight: '15px',}}>Дата установки</Grid>
                                    <Grid item xs={7}>{counter && counter['initial_counter_value_date'] ? new Date(counter['initial_counter_value_date']).toLocaleString('ru') : 'Нет данных'}</Grid>
                                </Grid>
                                <Grid container style={{marginBottom: '15px',}}>
                                    <Grid item xs={5} sx={{color: THEME.PLACEHOLDER_TEXT, paddingRight: '15px',}}>Текущее показание счётчика</Grid>
                                    <Grid item xs={7}>{counter && counter['last_counter_value'] ? counter['last_counter_value'] : 'Нет данных'}</Grid>
                                </Grid>
                                <Grid container style={{marginBottom: '15px',}}>
                                    <Grid item xs={5} sx={{color: THEME.PLACEHOLDER_TEXT, paddingRight: '15px',}}>Дата снятия показания</Grid>
                                    <Grid item xs={7}>{counter && counter['last_counter_value_date'] ? new Date(counter['last_counter_value_date']).toLocaleString('ru') : 'Нет данных'}</Grid>
                                </Grid>
                                <Grid container style={{marginBottom: '15px',}}>
                                    <Grid item xs={5} sx={{color: THEME.PLACEHOLDER_TEXT, paddingRight: '15px',}}>Столб</Grid>
                                    <Grid item xs={7}>{counter && counter['electric_pole'] ? counter['electric_pole'] : 'Нет данных'}</Grid>
                                </Grid>
                                <Grid container style={{marginBottom: '15px',}}>
                                    <Grid item xs={5} sx={{color: THEME.PLACEHOLDER_TEXT, paddingRight: '15px',}}>Линия</Grid>
                                    <Grid item xs={7}>{counter && counter['electric_line'] ? counter['electric_line'] : 'Нет данных'}</Grid>
                                </Grid>
                                <Grid container style={{marginBottom: '15px',}}>
                                    <Grid item xs={5} sx={{color: THEME.PLACEHOLDER_TEXT, paddingRight: '15px',}}>Количество проживающих</Grid>
                                    <Grid item xs={7}>{counter && counter['lodgers_count'] ? counter['lodgers_count'] : 'Нет данных'}</Grid>
                                </Grid>
                                <Grid container style={{marginBottom: '30px',}}>
                                    <Grid item xs={5} sx={{color: THEME.PLACEHOLDER_TEXT, paddingRight: '15px',}}>Количество комнат</Grid>
                                    <Grid item xs={7}>{counter && counter['rooms_count'] ? counter['rooms_count'] : 'Нет данных'}</Grid>
                                </Grid>
                                <Grid item xs={4}>
                                    <Button
                                        variant={'outlined'}
                                        size={'large'}
                                        onClick={() => history.push(`/counter-edit/${type}/${counter['counter_id']}`)}
                                        className={'base_styled_btn'}
                                        fullWidth
                                    >
                                        Редактировать узел учёта
                                    </Button>
                                </Grid>
                            </Grid>
                            <Grid item xs={6}>
                                <Grid container style={{marginBottom: '30px',}}>
                                    <Grid item xs={8} style={{fontSize: '18px',}}>
                                        <Grid sx={{fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                                            Координаты узла учёта
                                        </Grid>
                                        <Grid sx={{color: 'text.primary',}}>
                                            {counter && counter['coordinate_x'] && counter['coordinate_y'] ?
                                                `[${counter['coordinate_x']}, ${counter['coordinate_y']}]`
                                                : 'Нет данных'
                                            }
                                        </Grid>
                                    </Grid>
                                    <Grid item xs={4}>
                                        <SimpleMapModal
                                            coordX={counter && counter['coordinate_x']}
                                            coordY={counter && counter['coordinate_y']}
                                            modalName={'coordinates'}
                                            modalTitle={'GPS координаты узла учёта'}
                                            startButtonTitle={'Посмотреть'}
                                            startButtonStyle={{width: '100%',}}
                                            acceptButtonShow={false}
                                            onModalSubmit={() => {}}
                                            enableSearch={true}
                                            disableStartButton={!counter && !counter['coordinate_x'] && !counter['coordinate_y']}
                                        />
                                    </Grid>
                                </Grid>
                                <Grid container>
                                    <Grid item xs={8}>
                                        {counter && counter['coordinate_x'] && counter['coordinate_y'] &&
                                            <MapComponent
                                                coordX={counter['coordinate_x']}
                                                coordY={counter['coordinate_y']}
                                                draggable={false}
                                                mapWidth={'100%'}
                                                mapHeight={'20vh'}
                                                enableSearch={false}
                                            />
                                        }
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Card>
                    <Grid style={{marginBottom: '20px',}}>
                        <TableItemsCustomizer
                            initialCount={itemsCount}
                            allItems={totalItems}
                            onTableItemCountChange={onTableItemCountChange}
                        />
                    </Grid>
                    <TableContainer sx={{bgcolor: 'background.default', marginBottom: '30px',}}>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    {listHeader.map((value, index) => (
                                        <TableCell key={index} style={{color: THEME.PLACEHOLDER_TEXT}}>{value}</TableCell>
                                    ))}
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {counterValues && counterValues.length ? counterValues.map((data, index) => (
                                    <TableRow key={index} onClick={() => {}}>
                                        <TableCell sx={{fontSize: '15px', fontWeight: '500',}}>
                                            {months[new Date(data.date).getMonth()] || 'Нет данных'}
                                        </TableCell>
                                        <TableCell sx={{fontSize: '15px', fontWeight: '500',}}>
                                            {data.value.value || 'Нет данных'}
                                        </TableCell>
                                        <TableCell sx={{fontSize: '15px', fontWeight: '500',}}>
                                            {new Date(data.date).toLocaleString('ru') || 'Нет данных'}
                                        </TableCell>
                                    </TableRow>
                                )) : (
                                    <></>
                                )}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    {counterValues && !counterValues.length && (
                        <Box style={{padding: '70px 0px', textAlign: 'center', width: '100%', color: THEME.PLACEHOLDER_TEXT, fontSize: '16px',}}>
                            Нет данных
                        </Box>
                    )}
                    <Stack spacing={2} style={{marginTop: '30px'}}>
                        <Pagination
                            count={pagesCount}
                            variant={'outlined'}
                            shape={'rounded'}
                            color={'primary'}
                            onChange={(event, page) => onPageNumberChange(page)}
                        />
                    </Stack>
                </> : <></>
            }
        </>
    )
}