import React, {useEffect, useState} from "react";
import {Formik} from "formik";
import * as Yup from "yup";
import {Box, Button, Grid, TextField} from "@material-ui/core";
import SimpleModal from "../../../components/Modal/SimpleModal/SimpleModal";
import SimpleSelectInput from "../../../components/FormInputs/SimpleSelectInput/SimpleSelectInput";
import MapComponent from "../../../components/Other/MapComponent/MapComponent";
import SimpleMapModal from "../../../components/Modal/SimpleMapModal/SimpleMapModal";
import SimpleTextInput from "../../../components/FormInputs/SimpleTextInput/SimpleTextInput";
import BasicDatePicker from "../../../components/DatePicker/BasicDatePicker";
import BackButtonComponent from "../../../components/Other/BackButtonComponent/BackButtonComponent";
import {AgreementCounterData, CompanyTransformersData} from "../../../interfaces/interfaces";
import BasicDateTimePicker from "../../../components/DatePicker/BasicDateTimePicker";
import {useSelector} from "react-redux";
import {appStateInterface} from "../../../store/appState";

const selectServiceability = [
    {title: 'Доступен / Исправен', value: 0},
    {title: 'Недоступен / Неисправен', value: 1},
];

interface CounterEditCardProps {
    counterData: AgreementCounterData
    transformers: CompanyTransformersData[]
    children?: any
    onFlash(type: string, message: string, messageHeading?: string): void
    onSaveHandler?(data: any): void
    onDeleteHandler?(): void
}

export default function CounterEditCard(props: CounterEditCardProps) {
    const [initialValues, setInitialValues] = useState({
        counterTitle: props.counterData && props.counterData['counter_title'] ? props.counterData['counter_title'] : '',
        counterModel: props.counterData && props.counterData['counter_model'] ? props.counterData['counter_model'] : '',
        counterPhysicalStatus: props.counterData && props.counterData['counter_physical_status'] ? props.counterData['counter_physical_status'] : 0,
        address: props.counterData && props.counterData.address ? props.counterData.address : '',
        coordinateX: props.counterData && props.counterData['coordinate_x'] ? props.counterData['coordinate_x'] : null,
        coordinateY: props.counterData && props.counterData['coordinate_y'] ? props.counterData['coordinate_y'] : null,
        terminalSeal: props.counterData && props.counterData['terminal_seal'] ? props.counterData['terminal_seal'] : '',
        antimagneticSeal: props.counterData && props.counterData['antimagnetic_seal'] ? props.counterData['antimagnetic_seal'] : '',
        sideSeal: props.counterData && props.counterData['side_seal'] ? props.counterData['side_seal'] : '',
        transformer: props.counterData && props.counterData.transformer ? props.counterData.transformer.id : '',
        initialCounterValue: props.counterData && props.counterData['initial_counter_value'] ? props.counterData['initial_counter_value'] : '',
        initialCounterValueDate: props.counterData && props.counterData['initial_counter_value_date'] ? props.counterData['initial_counter_value_date'] : '',
        lastCounterValue: props.counterData && props.counterData['last_counter_value'] ? props.counterData['last_counter_value'] : '',
        lastCounterValueDate: props.counterData && props.counterData['last_counter_value_date'] ? props.counterData['last_counter_value_date'] : '',
        electricPoleTitle: props.counterData && props.counterData['electric_pole'] ? props.counterData['electric_pole'] : '',
        electricLineTitle: props.counterData && props.counterData['electric_line'] ? props.counterData['electric_line'] : '',
        lodgersCount: props.counterData && props.counterData['lodgers_count'] ? props.counterData['lodgers_count'].toString() : '',
        roomsCount: props.counterData && props.counterData['rooms_count'] ? props.counterData['rooms_count'].toString() : '',
    })
    const [counterStateStatus, setCounterStateStatus] = useState(true);

    const currentUser = useSelector((state: appStateInterface) => state.user);

    const saveCounterHandler = (values) => {
        props.onSaveHandler(values);
    }

    const deleteCounterHandler = () => {
        props.onDeleteHandler();
    }

    useEffect(() => {
        setInitialValues({
            counterTitle: props.counterData && props.counterData['counter_title'] ? props.counterData['counter_title'] : '',
            counterModel: props.counterData && props.counterData['counter_model'] ? props.counterData['counter_model'] : '',
            counterPhysicalStatus: props.counterData && props.counterData['counter_physical_status'] ? props.counterData['counter_physical_status'] : 0,
            address: props.counterData && props.counterData.address ? props.counterData.address : '',
            coordinateX: props.counterData && props.counterData['coordinate_x'] ? props.counterData['coordinate_x'] : null,
            coordinateY: props.counterData && props.counterData['coordinate_y'] ? props.counterData['coordinate_y'] : null,
            terminalSeal: props.counterData && props.counterData['terminal_seal'] ? props.counterData['terminal_seal'] : '',
            antimagneticSeal: props.counterData && props.counterData['antimagnetic_seal'] ? props.counterData['antimagnetic_seal'] : '',
            sideSeal: props.counterData && props.counterData['side_seal'] ? props.counterData['side_seal'] : '',
            transformer: props.counterData && props.counterData.transformer ? props.counterData.transformer.id : '',
            initialCounterValue: props.counterData && props.counterData['initial_counter_value'] ? props.counterData['initial_counter_value'] : '',
            initialCounterValueDate: props.counterData && props.counterData['initial_counter_value_date'] ? props.counterData['initial_counter_value_date'] : '',
            lastCounterValue: props.counterData && props.counterData['last_counter_value'] ? props.counterData['last_counter_value'] : '',
            lastCounterValueDate: props.counterData && props.counterData['last_counter_value_date'] ? props.counterData['last_counter_value_date'] : '',
            electricPoleTitle: props.counterData && props.counterData['electric_pole'] ? props.counterData['electric_pole'] : '',
            electricLineTitle: props.counterData && props.counterData['electric_line'] ? props.counterData['electric_line'] : '',
            lodgersCount: props.counterData && props.counterData['lodgers_count'] ? props.counterData['lodgers_count'].toString() : '',
            roomsCount: props.counterData && props.counterData['rooms_count'] ? props.counterData['rooms_count'].toString() : '',        })
    }, [props]);

    const counterSchema = Yup.object().shape({
        transformer: Yup.string(),
        counterTitle: Yup.string().required('Поле с номером узла учёта не может быть пустым'),
        counterPhysicalStatus: Yup.number(),
        counterModel: Yup.string(),
        terminalSeal: Yup.string(),
        antimagneticSeal: Yup.string(),
        sideSeal: Yup.string(),
        address: Yup.string(),
        coordinateX: Yup.number().nullable(),
        coordinateY: Yup.number().nullable(),
        initialCounterValue: Yup.number().nullable(),
        initialCounterValueDate: Yup.date().nullable(),
        electricPoleTitle: Yup.string(),
        electricLineTitle: Yup.string(),
        lodgersCount: Yup.number().nullable(),
        roomsCount: Yup.number().nullable(),
    })

    return (
        <>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                Редактирование узла учёта
            </Grid>
            <Grid style={{marginBottom: '30px', display: 'flex', justifyContent: 'space-between',}}>
                <BackButtonComponent changed={!counterStateStatus}/>
                {currentUser.roles.includes('ROLE_SUPER_ADMIN') &&
                    <SimpleModal
                        startButtonTitle={'Удалить узел учёта'}
                        modalName={'deleteCounterModal'}
                        startButtonVariant={'text'}
                        closeButtonTitle={'Отменить'}
                        modalTitle={'Удаление узла учёта'}
                        acceptButtonTitle={'Подтвердить'}
                        onModalSubmit={deleteCounterHandler}
                    >
                        Удалить узел учёта?
                    </SimpleModal>
                }
            </Grid>
            {props.children}
            <Formik
                initialValues={initialValues}
                validationSchema={counterSchema}
                onSubmit={(values) => saveCounterHandler(values)}
            >
                {({
                    handleChange,
                    handleSubmit,
                    handleBlur,
                    dirty,
                    isValid,
                    errors,
                    values,
                    touched,
                    setFieldValue,
                    handleReset,
                }) => (
                    <Box
                        component={'form'}
                    >
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'counterTitle'}
                                    value={values.counterTitle}
                                    placeholder={'Введите номер узла учёта'}
                                    label={'Номер узла учёта'}
                                    onChange={(e) => {
                                        e.persist();
                                        setFieldValue('counterTitle', e.target.value);
                                        setCounterStateStatus(false);
                                    }}
                                    error={errors.counterTitle && !!touched.counterTitle}
                                    helperText={touched.counterTitle && errors.counterTitle}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'counterModel'}
                                    value={values.counterModel}
                                    placeholder={'Введите модель узла учёта'}
                                    label={'Модель узла учёта'}
                                    onChange={(e) => {
                                        e.persist();
                                        setFieldValue('counterModel', e.target.value);
                                        setCounterStateStatus(false);
                                    }}
                                    error={errors.counterModel && !!touched.counterModel}
                                    helperText={touched.counterModel && errors.counterModel}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleSelectInput
                                    label={'Состояние / Доступ узла учёта'}
                                    options={selectServiceability}
                                    defaultValue={values.counterPhysicalStatus || 0}
                                    onChange={(value) => {
                                        setFieldValue('counterPhysicalStatus', value);
                                        setCounterStateStatus(false);
                                    }}
                                    id={'counterPhysicalStatus'}
                                    formControlStyle={{width: '100%',}}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'address'}
                                    value={values.address}
                                    placeholder={'Введите адрес узла учёта'}
                                    label={'Адрес'}
                                    onChange={(e) => {
                                        e.persist();
                                        setFieldValue('address', e.target.value);
                                        setCounterStateStatus(false);
                                    }}
                                    error={errors.address && !!touched.address}
                                    helperText={touched.address && errors.address}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <Box sx={{color: 'secondary.main', fontSize: '18px', fontWeight: '500', marginBottom: '20px',}}>
                                    Координаты узла учёта
                                </Box>
                                <Box sx={{color: 'text.primary', fontSize: '18px', marginBottom: '30px',}}>
                                    {values.coordinateX && values.coordinateY
                                        ? '\[' + values.coordinateX + ', ' + values.coordinateY + '\]'
                                        : 'Нет данных'}
                                </Box>
                                {values.coordinateX && values.coordinateY  &&
                                    <MapComponent
                                        coordX={values.coordinateX}
                                        coordY={values.coordinateY}
                                        draggable={false}
                                        mapWidth={'100%'}
                                        mapHeight={'20vh'}
                                        enableSearch={false}
                                    />
                                }
                            </Grid>
                            <Grid item xs={2} style={{paddingRight: '15px',}}>
                                <SimpleMapModal
                                    coordX={values.coordinateX}
                                    coordY={values.coordinateY}
                                    modalName={'counterCoordinates'}
                                    modalTitle={'GPS координаты узла учёта'}
                                    startButtonTitle={'Изменить'}
                                    startButtonStyle={{width: '100%',}}
                                    // acceptButtonShow={true}
                                    onModalSubmit={(e) => {
                                        setFieldValue('coordinateX', e.x);
                                        setFieldValue('coordinateY', e.y);
                                        setCounterStateStatus(false);
                                    }}
                                    enableSearch={true}
                                    // disableStartButton={!task.new_coordinate_x && !task.new_coordinate_y}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'terminalSeal'}
                                    value={values.terminalSeal}
                                    label={'Клеммная пломба'}
                                    onChange={(e) => {
                                        e.persist();
                                        setFieldValue('terminalSeal', e.target.value);
                                        setCounterStateStatus(false);
                                    }}
                                    error={errors.terminalSeal && !!touched.terminalSeal}
                                    helperText={touched.terminalSeal && errors.terminalSeal}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'antimagneticSeal'}
                                    value={values.antimagneticSeal}
                                    label={'Антимагнитная пломба'}
                                    onChange={(e) => {
                                        e.persist();
                                        setFieldValue('antimagneticSeal', e.target.value);
                                        setCounterStateStatus(false);
                                    }}
                                    error={errors.antimagneticSeal && !!touched.antimagneticSeal}
                                    helperText={touched.antimagneticSeal && errors.antimagneticSeal}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'sideSeal'}
                                    value={values.sideSeal}
                                    label={'Боковая пломба'}
                                    onChange={(e) => {
                                        e.persist();
                                        setFieldValue('sideSeal', e.target.value);
                                        setCounterStateStatus(false);
                                    }}
                                    error={errors.sideSeal && !!touched.sideSeal}
                                    helperText={touched.sideSeal && errors.sideSeal}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleSelectInput
                                    label={'Трансформатор'}
                                    options={props.transformers ? props.transformers.map(item => {
                                        return {
                                            title: item.title,
                                            value: item.id,
                                        }
                                    }) : []}
                                    defaultValue={values.transformer}
                                    onChange={(value) => {
                                        setFieldValue('transformer', value);
                                        setCounterStateStatus(false);
                                    }}
                                    formControlStyle={{width: '100%',}}
                                    allowEmptyValue={true}
                                    emptyValueTitle={'Не выбрано'}
                                    error={!!touched.transformer && !!!values.transformer}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleTextInput
                                    name={'initialCounterValue'}
                                    label={'Показания счётчика при установке'}
                                    defaultValue={values.initialCounterValue}
                                    onBlur={(value) => {
                                        setFieldValue('initialCounterValue', +value);
                                        setCounterStateStatus(false);
                                    }}
                                    onChange={() => {}}
                                    id={'initialCounterValue'}
                                />
                            </Grid>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <BasicDateTimePicker
                                    id={'initialCounterValueDate'}
                                    label={'Дата установки'}
                                    defaultValue={values.initialCounterValueDate ? new Date(values.initialCounterValueDate) : null}
                                    maxDate={new Date()}
                                    onChange={(value) => {
                                        setFieldValue('initialCounterValueDate', value);
                                        setCounterStateStatus(false);
                                    }}
                                    inputVariant={'outlined'}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleTextInput
                                    name={'lastCounterValue'}
                                    label={'Текущее показание счётчика'}
                                    defaultValue={values.lastCounterValue}
                                    onBlur={() => {}}
                                    onChange={() => {}}
                                    id={'lastCounterValue'}
                                    disabled
                                />
                            </Grid>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <BasicDateTimePicker
                                    id={'lastCounterValueDate'}
                                    label={'Дата снятия показания'}
                                    defaultValue={values.lastCounterValueDate ? new Date(values.lastCounterValueDate) : null}
                                    maxDate={new Date()}
                                    onChange={(value) => {
                                        setFieldValue('lastCounterValueDate', value);
                                    }}
                                    inputVariant={'outlined'}
                                    disabled
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'electricPoleTitle'}
                                    value={values.electricPoleTitle}
                                    label={'Столб'}
                                    onChange={(e) => {
                                        e.persist();
                                        setFieldValue('electricPoleTitle', e.target.value);
                                        setCounterStateStatus(false);
                                    }}
                                    error={errors.electricPoleTitle && !!touched.electricPoleTitle}
                                    helperText={touched.electricPoleTitle && errors.electricPoleTitle}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'electricLineTitle'}
                                    value={values.electricLineTitle}
                                    label={'Линия'}
                                    onChange={(e) => {
                                        e.persist();
                                        setFieldValue('electricLineTitle', e.target.value);
                                        setCounterStateStatus(false);
                                    }}
                                    error={errors.electricLineTitle && !!touched.electricLineTitle}
                                    helperText={touched.electricLineTitle && errors.electricLineTitle}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'lodgersCount'}
                                    value={values.lodgersCount}
                                    label={'Количество проживающих'}
                                    onChange={(e) => {
                                        e.persist();
                                        setFieldValue('lodgersCount', +e.target.value);
                                        setCounterStateStatus(false);
                                    }}
                                    error={errors.lodgersCount && !!touched.lodgersCount}
                                    helperText={touched.lodgersCount && errors.lodgersCount}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'roomsCount'}
                                    value={values.roomsCount}
                                    label={'Количество комнат'}
                                    onChange={(e) => {
                                        e.persist();
                                        setFieldValue('roomsCount', +e.target.value);
                                        setCounterStateStatus(false);
                                    }}
                                    error={errors.roomsCount && !!touched.roomsCount}
                                    helperText={touched.roomsCount && errors.roomsCount}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={2} style={{paddingRight: '15px',}}>
                                <Button
                                    variant={'outlined'}
                                    fullWidth
                                    onClick={() => {
                                        handleReset();
                                        setCounterStateStatus(true);
                                    }}
                                    className={'base_styled_btn'}
                                >
                                    Отменить изменения
                                </Button>
                            </Grid>
                            <Grid item xs={2} style={{paddingRight: '15px',}}>
                                <Button
                                    variant={'contained'}
                                    fullWidth
                                    onClick={() => handleSubmit()}
                                    className={'base_styled_btn'}
                                    disabled={counterStateStatus}
                                >
                                    Сохранить
                                </Button>
                            </Grid>
                        </Grid>
                    </Box>
                )}
            </Formik>
            <BackButtonComponent changed={!counterStateStatus}/>
        </>
    )
}