import React, {useEffect, useState} from "react";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import CounterEditCard from "./CounterEditCard";
import {SimpleFlashData} from "../../../interfaces/interfaces";
import {useDispatch, useSelector} from "react-redux";
import {useHistory, useParams} from "react-router";
import {AppAction} from "../../../store/actions/actions";
import {appStateInterface} from "../../../store/appState";
import FlashComponent from "../../../components/Flashes/FlashComponent";
import {
    deleteCounterRequest,
    editCounterRequest,
    getCompanyTransformersDataRequest,
    getCounterCardDataRequest
} from "../../../requests/Request";

export default function CounterEditPage() {
    const [counterData, setCounterData] = useState(null);
    const [companyTransformersData, setCompanyTransformersData] = useState([]);

    const [counterLoaded, setCounterLoaded] = useState(false);

    const [flashError, setFlashError] = useState<SimpleFlashData>({status: false})
    const [flashSuccess, setFlashSuccess] = useState({status: false, message: ''});

    const dispatch = useDispatch();
    const history = useHistory();

    let params: any = useParams();
    let id = params.id;
    let type = params.type;

    const breadcrumbsItems = [
        {
            itemTitle: type === 'ul' ? 'Юр. лица' : 'Физ. лица',
            itemSrc: `/subscribers/${type}`,
        },
        {
            itemTitle: 'Карточка абонента',
            itemSrc: `/subscriber/${type}/${counterData && counterData.subscriber.id}`,
        },
        {
            itemTitle: 'Карточка договора',
            itemSrc: `/agreement-page/${type}/${counterData && counterData.agreement.id}`,
        },
        {
            itemTitle: 'Карточка узла учёта',
            itemSrc: `/counter-page/${type}/${id}`,
        },
        {
            itemTitle: 'Редактирование узла учёта',
            // itemSrc: ``,
        },
    ]

    const onCloseErrorFlash = () => {
        setFlashError(prevState => {return {...prevState, status: false}})
    };

    const onCloseSuccessFlash = () => {
        setFlashSuccess(prevState => {return {...prevState, status: false}})
    };

    const showFlash = (type, message, messageHeading) => {
        switch (type) {
            case 'error':
                setFlashError({status: true, messageHeading: messageHeading, message: message});
                break;
            case 'success':
                setFlashSuccess({status: true, message: message});
                break;
            case 'disableFlashes':
                setFlashError({status: false,});
                setFlashSuccess({status: false, message: message});
        }
    }

    useEffect(() => {
        dispatch(AppAction.showLoader());
        getCounterCardDataRequest(id).then(
            result => {
                if (result.type === 'success') {
                    setCounterData(result.data[0]);
                } else {
                    console.log('Error: ', result.data);
                    showFlash('error', 'Обратитесь к администратору', 'Ошибка.');
                }
            }
        ).finally(() => {
            setCounterLoaded(true);
        })
    }, []);

    useEffect(() => {
        if (counterLoaded) {
            getCompanyTransformersDataRequest(counterData.agreement.company.id, {page: 1,}).then(result => {
                if (result.type === 'success') {
                    setCompanyTransformersData(result.data.transformers);
                }
            }).finally(() => {
                dispatch(AppAction.hideLoader())
            })
        }
    }, [counterLoaded]);

    const onSaveHandler = (values) => {
        dispatch(AppAction.showLoader());
        editCounterRequest(id, values).then(
            result => {
                if (result.type === 'success') {
                    history.push(`/counter-page/${type}/${id}`);
                }
            }
        ).finally(() => {
            dispatch(AppAction.hideLoader());
        })
    }

    const onDeleteHandler = () => {
        dispatch(AppAction.showLoader());
        deleteCounterRequest(id).then(
            result => {
                if (result.type === 'success') {
                    history.go(-2);
                } else {
                    showFlash('error', 'Не удалось удалить узел учёта. Пожалуйста, обратитесь к администратору', 'Ошибка.')
                }
            }
        ).finally(() => {
            dispatch(AppAction.hideLoader());
        })
    }

    const loading = useSelector((state: appStateInterface) => state.loading);

    return (
        <PageLayout
            breadcrumbsItems={breadcrumbsItems}
        >
            {!loading ?
                <CounterEditCard
                    counterData={counterData}
                    transformers={companyTransformersData}
                    onFlash={(type, message, messageHeading) => {showFlash(type, message, messageHeading)}}
                    onSaveHandler={onSaveHandler}
                    onDeleteHandler={onDeleteHandler}
                >
                    <FlashComponent
                        message={flashError.message}
                        messageHeading={flashError.messageHeading}
                        showFlash={flashError.status}
                        flashType={'error'}
                    />
                    <FlashComponent
                        message={flashSuccess.message}
                        showFlash={flashSuccess.status}
                        onCloseFlash={onCloseSuccessFlash}
                        flashType={'success'}
                    />
                </CounterEditCard> : <></>
            }
        </PageLayout>
    )
}