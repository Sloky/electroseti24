import React, {useEffect, useState} from "react";
import PageLayout from "../../../components/Layouts/PageLayout/PageLayout";
import NewCounterCard from "./NewCounterCard";
import {useDispatch} from "react-redux";
import {AppAction} from "../../../store/actions/actions";
import {
    getAgreementCardDataRequest,
    getCompanyTransformersDataRequest,
} from "../../../requests/Request";
import {useParams} from "react-router";

export default function NewCounterPage() {
    const [agreementData, setAgreementData] = useState(null);
    const [companyTransformersData, setCompanyTransformersData] = useState([]);
    const [agreementLoaded, setAgreementLoaded] = useState(false);

    let params: any = useParams();
    let id = params.id;
    let type = params.type;

    const dispatch = useDispatch();

    const breadcrumbsItems = [
        {
            itemTitle: type === 'ul' ? 'Юр. лица' : 'Физ. лица',
            itemSrc: `/subscribers/${type}`,
        },
        {
            itemTitle: 'Карточка абонента',
            itemSrc: `/subscriber/${type}/${agreementData && agreementData.subscriber.id}`,
        },
        {
            itemTitle: 'Карточка договора',
            itemSrc: `/agreement-page/${type}/${agreementData && agreementData.id}`,
        },
        {
            itemTitle: 'Добавление узла учёта',
            // itemSrc: ``,
        },
    ]

    useEffect(() => {
        dispatch(AppAction.showLoader());
        getAgreementCardDataRequest(id).then(
            result => {
                if (result.type === 'success') {
                    setAgreementData(result.data);
                }
            }
        ).finally(() => {
            setAgreementLoaded(true);
        })
    }, []);

    useEffect(() => {
        if (agreementLoaded) {
            getCompanyTransformersDataRequest(agreementData.company.id, {page: 1,}).then(result => {
                if (result.type === 'success') {
                    setCompanyTransformersData(result.data.transformers);
                }
            }).finally(() => {
                dispatch(AppAction.hideLoader());
            })
        }
    }, [agreementLoaded]);

    return (
        <PageLayout
            breadcrumbsItems={breadcrumbsItems}
        >
            <NewCounterCard
                transformers={companyTransformersData}
                agreementId={id}
            />
        </PageLayout>
    )
}