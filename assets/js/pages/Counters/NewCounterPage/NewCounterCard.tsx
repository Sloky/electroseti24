import React, {useState} from "react";
import {CompanyTransformersData, SimpleFlashData} from "../../../interfaces/interfaces";
import {useHistory} from "react-router";
import {useDispatch} from "react-redux";
import {AppAction} from "../../../store/actions/actions";
import {Formik} from "formik";
import * as Yup from "yup";
import {Box, Button, Grid, TextField} from "@material-ui/core";
import FlashComponent from "../../../components/Flashes/FlashComponent";
import SimpleSelectInput from "../../../components/FormInputs/SimpleSelectInput/SimpleSelectInput";
import MapComponent from "../../../components/Other/MapComponent/MapComponent";
import SimpleMapModal from "../../../components/Modal/SimpleMapModal/SimpleMapModal";
import SimpleTextInput from "../../../components/FormInputs/SimpleTextInput/SimpleTextInput";
import {addNewCounterRequest} from "../../../requests/Request";
import BasicDateTimePicker from "../../../components/DatePicker/BasicDateTimePicker";

interface NewCounterCardProps {
    transformers: CompanyTransformersData[]
    agreementId: string
}

const selectServiceability = [
    {title: 'Доступен / Исправен', value: 0},
    {title: 'Недоступен / Неисправен', value: 1},
];

export default function NewCounterCard(props: NewCounterCardProps) {
    const [flashError, setFlashError] : [SimpleFlashData, any] = useState({status: false});
    const [flashSuccess, setFlashSuccess] : [SimpleFlashData, any] = useState({status: false});

    const history = useHistory();
    const dispatch = useDispatch();

    const handleSubmit = (values) => {
        dispatch(AppAction.showLoader());
        addNewCounterRequest(props.agreementId, values).then(
            result => {
                if (result.type === 'success') {
                    history.goBack();
                } else {
                    if (result.type === 'error' && result.data.message.match(/An counter with number (\S+) already exists/).length) {
                        setFlashError({
                            status: true,
                            message: `Произошла ошибка. Узел учёта с номером ${result.data.message.match(/An counter with number (\S+) already exists/)[1]} уже существует.`,
                        })
                    } else {
                        setFlashError({
                            status: true,
                            message: 'Произошла ошибка. Обратитесь к администратору',
                        });
                    }
                }
            }
        ).finally(() => {
            dispatch(AppAction.hideLoader());
        })
    }

    const onCloseErrorFlash = () => {
        setFlashError({status: false});
    }

    const onCloseSuccessFlash = () => {
        setFlashSuccess({status: false});
    }

    const initialValues = {
        transformer: '',
        counterTitle: '',
        counterPhysicalStatus: 0,
        counterModel: '',
        terminalSeal: '',
        antimagneticSeal: '',
        sideSeal: '',
        address: '',
        coordinateX: null,
        coordinateY: null,
        initialCounterValue: null,
        initialCounterValueDate: null,
        lastCounterValue: null,
        lastCounterValueDate: null,
        electricPoleTitle: '',
        electricLineTitle: '',
        lodgersCount: null,
        roomsCount: null,
    }

    const counterSchema = Yup.object().shape({
        transformer: Yup.string(),
        counterTitle: Yup.string().required('Поле с номером узла учёта не может быть пустым'),
        counterPhysicalStatus: Yup.number(),
        counterModel: Yup.string(),
        terminalSeal: Yup.string(),
        antimagneticSeal: Yup.string(),
        sideSeal: Yup.string(),
        address: Yup.string(),
        coordinateX: Yup.number().nullable(),
        coordinateY: Yup.number().nullable(),
        initialCounterValue: Yup.number().nullable(),
        initialCounterValueDate: Yup.date().nullable(),
        lastCounterValue: Yup.number().nullable(),
        lastCounterValueDate: Yup.date().nullable(),
        electricPoleTitle: Yup.string(),
        electricLineTitle: Yup.string(),
        lodgersCount: Yup.number().nullable(),
        roomsCount: Yup.number().nullable(),
    })

    return (
        <>
            <Grid sx={{fontSize: '25px', fontWeight: '500', color: 'secondary.main', marginBottom: '30px',}}>
                Добавление узла учёта
            </Grid>
            <FlashComponent
                messageHeading={flashError.messageHeading}
                message={flashError.message}
                showFlash={flashError.status}
                onCloseFlash={onCloseErrorFlash}
                flashType={'error'}
            />
            <FlashComponent
                message={flashSuccess.message}
                showFlash={flashSuccess.status}
                onCloseFlash={onCloseSuccessFlash}
                flashType={'success'}
            />
            <Formik
                initialValues={initialValues}
                validationSchema={counterSchema}
                onSubmit={(values) => handleSubmit(values)}
            >
                {({
                    handleChange,
                    handleSubmit,
                    handleBlur,
                    dirty,
                    isValid,
                    errors,
                    values,
                    touched,
                    setFieldValue,
                }) => (
                    <Box
                        component={'form'}
                    >
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'counterTitle'}
                                    placeholder={'Введите номер узла учёта'}
                                    label={'Номер узла учёта'}
                                    onChange={handleChange}
                                    error={errors.counterTitle && touched.counterTitle}
                                    helperText={touched.counterTitle && errors.counterTitle}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'counterModel'}
                                    placeholder={'Введите модель узла учёта'}
                                    label={'Модель узла учёта'}
                                    onChange={handleChange}
                                    error={errors.counterModel && touched.counterModel}
                                    helperText={touched.counterModel && errors.counterModel}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleSelectInput
                                    label={'Состояние / Доступ узла учёта'}
                                    options={selectServiceability}
                                    defaultValue={values.counterPhysicalStatus || 0}
                                    onChange={(value) => {
                                        setFieldValue('counterPhysicalStatus', value);
                                    }}
                                    id={'counterPhysicalStatus'}
                                    formControlStyle={{width: '100%',}}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'address'}
                                    placeholder={'Введите адрес узла учёта'}
                                    label={'Адрес'}
                                    onChange={handleChange}
                                    error={errors.address && touched.address}
                                    helperText={touched.address && errors.address}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <Box sx={{color: 'secondary.main', fontSize: '18px', fontWeight: '500', marginBottom: '20px',}}>
                                    Координаты узла учёта
                                </Box>
                                <Box sx={{color: 'text.primary', fontSize: '18px', marginBottom: '30px',}}>
                                    {values.coordinateX && values.coordinateY
                                        ? '\[' + values.coordinateX + ', ' + values.coordinateY + '\]'
                                        : 'Нет данных'}
                                </Box>
                                {values.coordinateX && values.coordinateY  &&
                                    <MapComponent
                                        coordX={values.coordinateX}
                                        coordY={values.coordinateY}
                                        draggable={false}
                                        mapWidth={'100%'}
                                        mapHeight={'20vh'}
                                        enableSearch={false}
                                    />
                                }
                            </Grid>
                            <Grid item xs={2} style={{paddingRight: '15px',}}>
                                <SimpleMapModal
                                    coordX={values.coordinateX}
                                    coordY={values.coordinateY}
                                    modalName={'counterCoordinates'}
                                    modalTitle={'GPS координаты узла учёта'}
                                    startButtonTitle={'Изменить'}
                                    startButtonStyle={{width: '100%',}}
                                    // acceptButtonShow={true}
                                    onModalSubmit={(e) => {
                                        setFieldValue('coordinateX', e.x);
                                        setFieldValue('coordinateY', e.y);
                                    }}
                                    enableSearch={true}
                                    // disableStartButton={!task.new_coordinate_x && !task.new_coordinate_y}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'terminalSeal'}
                                    label={'Клеммная пломба'}
                                    onChange={handleChange}
                                    error={errors.terminalSeal && touched.terminalSeal}
                                    helperText={touched.terminalSeal && errors.terminalSeal}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'antimagneticSeal'}
                                    label={'Антимагнитная пломба'}
                                    onChange={handleChange}
                                    error={errors.antimagneticSeal && touched.antimagneticSeal}
                                    helperText={touched.antimagneticSeal && errors.antimagneticSeal}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'sideSeal'}
                                    label={'Боковая пломба'}
                                    onChange={handleChange}
                                    error={errors.sideSeal && touched.sideSeal}
                                    helperText={touched.sideSeal && errors.sideSeal}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleSelectInput
                                    label={'Трансформатор'}
                                    options={props.transformers ? props.transformers.map(item => {
                                        return {
                                            title: item.title,
                                            value: item.id,
                                        }
                                    }) : []}
                                    defaultValue={values.transformer}
                                    onChange={(value) => {
                                        setFieldValue('transformer', value);
                                    }}
                                    formControlStyle={{width: '100%',}}
                                    allowEmptyValue={true}
                                    emptyValueTitle={'Не выбрано'}
                                    error={!!touched.transformer && !!!values.transformer}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleTextInput
                                    name={'initialCounterValue'}
                                    label={'Показания счётчика при установке'}
                                    defaultValue={values.initialCounterValue}
                                    onBlur={(value) => {
                                        setFieldValue('initialCounterValue', +value);
                                    }}
                                    onChange={() => {}}
                                    id={'initialCounterValue'}
                                />
                            </Grid>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <BasicDateTimePicker
                                    id={'initialCounterValueDate'}
                                    label={'Дата установки'}
                                    defaultValue={values.initialCounterValueDate ? new Date(values.initialCounterValueDate) : null}
                                    maxDate={new Date()}
                                    onChange={(value) => {
                                        setFieldValue('initialCounterValueDate', value);
                                    }}
                                    inputVariant={'outlined'}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <SimpleTextInput
                                    name={'lastCounterValue'}
                                    label={'Текущее показание счётчика'}
                                    defaultValue={values.lastCounterValue}
                                    onBlur={(value) => {
                                        setFieldValue('lastCounterValue', +value);
                                    }}
                                    onChange={() => {}}
                                    id={'lastCounterValue'}
                                />
                            </Grid>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <BasicDateTimePicker
                                    id={'lastCounterValueDate'}
                                    label={'Дата снятия показания'}
                                    defaultValue={values.lastCounterValueDate ? new Date(values.lastCounterValueDate) : null}
                                    maxDate={new Date()}
                                    onChange={(value) => {
                                        setFieldValue('lastCounterValueDate', value);
                                    }}
                                    inputVariant={'outlined'}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'electricPoleTitle'}
                                    label={'Столб'}
                                    onChange={handleChange}
                                    error={errors.electricPoleTitle && touched.electricPoleTitle}
                                    helperText={touched.electricPoleTitle && errors.electricPoleTitle}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'electricLineTitle'}
                                    label={'Линия'}
                                    onChange={handleChange}
                                    error={errors.electricLineTitle && touched.electricLineTitle}
                                    helperText={touched.electricLineTitle && errors.electricLineTitle}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'lodgersCount'}
                                    label={'Количество проживающих'}
                                    onChange={(e) => {
                                        setFieldValue('lodgersCount', +e.target.value)
                                    }}
                                    error={errors.lodgersCount && !!touched.lodgersCount}
                                    helperText={touched.lodgersCount && errors.lodgersCount}
                                />
                            </Grid>
                        </Grid>
                        <Grid container style={{marginBottom: '30px',}}>
                            <Grid item xs={4} style={{paddingRight: '15px',}}>
                                <TextField
                                    fullWidth
                                    size={'small'}
                                    id={'roomsCount'}
                                    label={'Количество комнат'}
                                    onChange={(e) => {
                                        setFieldValue('roomsCount', +e.target.value)
                                    }}
                                    error={errors.roomsCount && !!touched.roomsCount}
                                    helperText={touched.roomsCount && errors.roomsCount}
                                />
                            </Grid>
                        </Grid>
                        <Grid container>
                            <Grid item xs={2} style={{paddingRight: '15px',}}>
                                <Button
                                    variant={'outlined'}
                                    fullWidth
                                    onClick={() => history.goBack()}
                                    className={'base_styled_btn'}
                                >
                                    Отменить
                                </Button>
                            </Grid>
                            <Grid item xs={2} style={{paddingRight: '15px',}}>
                                <Button
                                    variant={'contained'}
                                    fullWidth
                                    onClick={() => handleSubmit()}
                                    className={'base_styled_btn'}
                                >
                                    Создать
                                </Button>
                            </Grid>
                        </Grid>
                    </Box>
                )}
            </Formik>
        </>
    )
}