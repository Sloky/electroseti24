import React from "react";
import "./MainMenu.css";
import {Link, useHistory} from "react-router-dom"
import {Badge, Box, Grid, List, ListItem, ListItemText, Tooltip} from "@material-ui/core";
import {THEME} from "../../theme";
import {useDispatch} from "react-redux";
import {TasksAction} from "../../store/actions/actions";

export interface MainMenuProps {
    items?: MenuItem[];
    children: any
}

interface MenuItem {
    menuTitle: string
    defaultSource: string
    roles: string[]
    sources?: RegExp[]
    icon?: any
    tooltip?: boolean
    subMenu?: {
        subMenuTitle: string
        defaultSource: string
        roles: string[]
        sources?: RegExp[]
        icon?: any
        badgeCount?: number
        tooltip?: boolean
    }[]
}

export default function MainMenu(props: MainMenuProps) {
    const history = useHistory();
    const dispatch = useDispatch();

    const onClickNavItem = (route: string) => {
        dispatch(TasksAction.changeFilters({
            companyFilter: null,
            executorFilter: null,
            statusFilter: null,
            subscriberTypeFilter: null,
            filtersLocation: null,
            searchValue: '',
            periodFilter: null,
            searchField: null,
        }))
        history.push(route);
    }

    const menuTabHandler = (currentLocation: string, locations: RegExp[]) => {
        let result1 = locations.filter((item) => {
            let result2 = currentLocation.match(item);
            if (result2 !== null) {
                return true;
            }
        })
        return !!result1.length;
    }

    const getCurrentItemIndex = (): number => {
        let currentIndex = 0;
        props.items.map((item, index) => {
            item.sources.map(i => {
                let match = history.location.pathname.match(i);
                if (match) {
                    currentIndex = index;
                }
            })
        })
        return currentIndex;
    }

    const menuItem = (item, index) => (
        <ListItem
            style={{display: 'inline-block', width: 'auto',}}
            className={menuTabHandler(history.location.pathname, item.sources) ? 'main_menu_item_active' : 'menu_item'}
            key={index}
            button
            onClick={() => item.tooltip ? null : onClickNavItem(item.defaultSource)}
        >
            <Box
                className={menuTabHandler(history.location.pathname, item.sources) ? 'menu_icon_active' : 'menu_icon'}
            >
                {item.icon}
            </Box>
            <ListItemText
                sx={{color: THEME.PLACEHOLDER_TEXT, '& .MuiListItemText-primary': {fontWeight: 700, fontSize: 16,}}}
                primary={item.menuTitle}
            />
        </ListItem>
    )

    const subMenuItem = (item, index) => (
        <ListItem
            className={menuTabHandler(history.location.pathname, item.sources) ? 'menu_item_active' : 'menu_item'}
            key={index}
            button
            onClick={() => item.tooltip ? null : onClickNavItem(item.defaultSource)}
        >
            <Box
                sx={{marginRight: '20px',}}
                className={menuTabHandler(history.location.pathname, item.sources) ? 'menu_icon_active' : 'menu_icon'}
            >
                {item.badgeCount ? <Badge
                    anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}
                    max={99999} badgeContent={item.badgeCount}
                    sx={{
                        '& .MuiBadge-badge': {
                            backgroundColor: menuTabHandler(history.location.pathname, item.sources) ? THEME.PRIMARY_COLOR : THEME.PLACEHOLDER_TEXT,
                            color: THEME.BACKGROUND_COLOR,
                        },
                    }}
                >
                    {item.icon}
                </Badge> : item.icon}
            </Box>
            <ListItemText
                sx={{color: THEME.PLACEHOLDER_TEXT, '& .MuiListItemText-primary': {fontWeight: 500, fontSize: 16,}}}
                primary={item.menuTitle}
            />
        </ListItem>
    )

    return (
        <Box>
            <Grid container>
                <Grid item xs={2}>
                    <Link
                        to={'/'}
                    >
                        <img
                            style={{width: '280px', height: '57px', marginBottom: '12px',}}
                            src="/img/electroseti24-logo.svg"
                            alt="SmartAdmin WebApp"
                        />
                    </Link>
                </Grid>
                <Grid item xs={10}>
                    <List component={'nav'}>
                        <Grid container>
                            <Grid item xs={11}>
                                {Array.isArray(props.items) && props.items.length ?
                                    (<Box style={{display: 'flex',}}>
                                        {props.items.map((item, index) => {
                                            if (index < props.items.length - 1) {
                                                return item.tooltip ? <Tooltip key={index} title={'В разработке'} arrow>{menuItem(item, index)}</Tooltip> : menuItem(item, index)
                                            }
                                        })}
                                    </Box>) : ('')
                                }
                            </Grid>
                            <Grid item xs={1}>
                                {Array.isArray(props.items) && props.items.length ?
                                    (<Box style={{display: 'flex',}}>
                                        {props.items.map((item, index) => {
                                            if (index === props.items.length - 1) {
                                                return menuItem(item, index)
                                            }
                                        })}
                                    </Box>) : ('')
                                }
                            </Grid>
                        </Grid>
                    </List>
                </Grid>
            </Grid>
            <Grid container>
                <Grid item xs={2} style={{paddingTop: '12px',}}>
                    {Array.isArray(props.items) && props.items.length ?
                        (<List component="nav">
                            {props.items[getCurrentItemIndex()].subMenu.map((item, index) => (
                                item.tooltip ? <Tooltip key={index} title={'В разработке'} arrow>
                                    {subMenuItem(item, index)}
                                </Tooltip> : subMenuItem(item, index)
                            ))}
                        </List>) : ('')
                    }
                </Grid>
                <Grid item xs={10}>
                    {props.children}
                </Grid>
            </Grid>
        </Box>
    );
}