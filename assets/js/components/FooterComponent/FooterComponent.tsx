import React from "react";
import {Box, Grid} from "@material-ui/core";

interface FooterComponentProps {
    footerStyle?: object
    children: any
}

export default function FooterComponent(props: FooterComponentProps) {
    return (
        <Grid container style={{width: '100%', position: 'absolute', left: 0, bottom: 0,}}>
            <Grid item xs={12} style={{textAlign: 'center',}}>
                <Box component="span" sx={{color: 'background.default', fontSize: '12px', ...props.footerStyle}}>
                    {props.children}
                </Box>
            </Grid>
        </Grid>
    )
}