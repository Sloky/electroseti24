import React, {useState} from "react";
import {connect, ConnectedProps, useDispatch} from 'react-redux';
import './LoginForm.css';
import axios from 'axios';
import {useHistory} from "react-router-dom";
import {UserData} from "../../../interfaces/interfaces";
import {
    TextField,
    Box,
    Button,
    IconButton,
    Checkbox,
    FormControlLabel,
} from "@material-ui/core";
import {Formik} from "formik";
import {Visibility, VisibilityOff} from "@material-ui/icons";
import * as Yup from "yup";
import {AppAction} from "../../../store/actions/actions";
import {APP_CLEAR_STATE} from "../../../store/actions/actionTypes";

type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = PropsFromRedux & {
    usernameTitle: string,
    passwordTitle: string,
    rememberMeTitle: string,
    submitTitle: string,
};

interface authFormInterface {
    username: string,
    password: string,
    rememberMe?: boolean
}

function LoginForm(props: Props) {
    const [errorMes, setErrorMes] = useState('');
    const [showPassword, setShowPassword] = useState<boolean>(false);

    const history = useHistory();
    const dispatch = useDispatch();

    const handleSubmit = (values) => {
        dispatch(AppAction.rememberMe(values.rememberMe));
        dispatch(AppAction.showLoader());

        axios.post(
            '/api/login',
            {
                'username': values.username,
                'password': values.password,
            },
        ).then(
            function (response) {
                let authToken = response.data.token;
                let user: UserData = response.data.data.user;
                if (user.roles.includes('ROLE_OPERATOR') || user.roles.includes('ROLE_ADMIN')) {
                    props.onTokenChange(authToken);
                    props.onChangeUserData(response.data.data.user);
                    props.onChangeAuthStatus(true);
                    history.push('/');
                } else {
                    dispatch(AppAction.hideLoader());
                    setErrorMes(`Ошибка авторизации. Нет доступа`);
                    dispatch({type: APP_CLEAR_STATE});
                }

            }
        ).catch(function (error) {
            dispatch(AppAction.hideLoader());
            if (error.response.status == 401) {
                setErrorMes(`Ошибка авторизации. ${props.usernameTitle} или ${props.passwordTitle.toLowerCase()} указаны неверно.`);
            } else if (error.response.status == 403) {
                setErrorMes('Доступ запрещен.');
            } else if (error.response.status == 500) {
                setErrorMes('Что-то пошло не так.');
            }
            console.log('Response Error:', error.response);
        });

    };

    const handleClickShowPassword = () => {
        setShowPassword(prevState => !prevState);
    };

    const initialValues: authFormInterface = {
        username: '',
        password: '',
        rememberMe: false,
    };

    const loginSchema = Yup.object().shape({
        username: Yup.string().required('Поле логина не может быть пустым'),
        password: Yup.string(),
        rememberMe: Yup.boolean(),
    })

    return (
        <Formik
            initialValues={initialValues}
            validationSchema={loginSchema}
            onSubmit={(values) => handleSubmit(values)}
        >
            {({
                handleChange,
                handleSubmit,
                handleBlur,
                dirty,
                isValid,
                errors,
                values,
                touched,
                setFieldValue,
            }) => (
                <Box
                    component="form"
                    className="login_form_wrapper"
                    onKeyDown={(e) => {
                        if (e.key === 'Enter') {
                            handleSubmit();
                        }
                    }}
                >
                    <TextField
                        style={{margin: '8px 0'}}
                        fullWidth
                        id="username"
                        color="primary"
                        placeholder="Введите логин"
                        size="small"
                        label={props.usernameTitle}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        error={errors.username && touched.username}
                        helperText={touched.username && errors.username}
                        autoFocus
                    />
                    <Box className="password_input_wrapper">
                        <TextField
                            style={{margin: '8px 0'}}
                            fullWidth
                            id="password"
                            color="primary"
                            type={showPassword ? 'text' : 'password'}
                            placeholder="Введите пароль"
                            size="small"
                            label={props.passwordTitle}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            // error={errors.password && touched.password}
                            // helperText={touched.password && errors.password}
                        />
                        <IconButton
                            className="password_icon_btn"
                            style={{position: 'absolute',}}
                            onClick={handleClickShowPassword}
                        >
                            {showPassword ? <VisibilityOff/> : <Visibility/>}
                        </IconButton>
                    </Box>
                    <Box sx={{color: 'error.dark'}}>{errorMes}</Box>
                    <Box className="login_checkbox_wrapper">
                        <FormControlLabel
                            label="Запомнить"
                            control={
                                <Checkbox
                                    color="primary"
                                    id={'rememberMe'}
                                    onChange={(e) => {
                                        setFieldValue('rememberMe', e.target.checked)
                                    }}
                                    value={values.rememberMe}
                                />
                            }
                        />
                    </Box>
                    <Button
                        className="login_btn"
                        onClick={() => handleSubmit()}
                        color="primary"
                        variant="contained"
                        fullWidth
                    >
                        {props.submitTitle}
                    </Button>
                </Box>
            )}
        </Formik>
    );
}

function mapStateToProps(state) {
    return {
        isAuthenticated: state.isAuthenticated,
        token: state.token,
        user: state.user,
        state: state,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onTokenChange: (token) => dispatch({type: 'CHANGE_TOKEN', payload: token}),
        onChangeAuthStatus: (status) => dispatch({type: 'CHANGE_AUTH_STATUS', payload: status}),
        onChangeUserData: (userData) => dispatch({type: 'CHANGE_USER_DATA', payload: userData}),
    }
}

const connector = connect(mapStateToProps, mapDispatchToProps);

export default connector(LoginForm);