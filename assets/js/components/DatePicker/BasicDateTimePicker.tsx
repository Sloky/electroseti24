import React, {useEffect, useState} from "react";
import {DateTimePicker, LocalizationProvider} from "@material-ui/lab";
import AdapterDateFns from '@material-ui/lab/AdapterDateFns';
import ruLocale from 'date-fns/locale/ru';
import {TextField} from "@material-ui/core";

interface BasicDateTimePickerProps {
    label: string
    style?: object
    defaultValue?: Date
    onChange?: any
    inputVariant?: 'outlined' | 'standard' | 'filled'
    id?: string
    minDate?: Date
    maxDate?: Date
    disabled?: boolean
}

export default function BasicDateTimePicker(props: BasicDateTimePickerProps) {
    const [value, setValue] = useState<Date | null>(props.defaultValue);

    const handleOnChange = (value) => {
        setValue(value);
        props.onChange(value);
    }

    useEffect(() => {
        setValue(props.defaultValue);
    }, [props.defaultValue]);

    return (
        <LocalizationProvider
            dateAdapter={AdapterDateFns}
            locale={ruLocale}
        >
            <DateTimePicker
                label={props.label}
                value={value}
                onChange={handleOnChange}
                clearable={true}
                minDate={props.minDate}
                maxDate={props.maxDate}
                disabled={props.disabled}
                renderInput={(params) => (
                    <TextField
                        id={props.id}
                        size={'small'}
                        variant={props.inputVariant || 'standard'}
                        {...params}
                        style={{...props.style}}
                    />
                )}
            />
        </LocalizationProvider>
    )
}