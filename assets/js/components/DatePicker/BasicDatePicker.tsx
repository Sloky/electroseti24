import * as React from 'react';
import {DatePicker, LocalizationProvider} from "@material-ui/lab";
import AdapterDateFns from '@material-ui/lab/AdapterDateFns';
import {TextField} from "@material-ui/core";
import ruLocale from 'date-fns/locale/ru';
import {useEffect} from "react";

interface BasicDatePickerProps {
    label: string
    style?: object
    defaultValue?: Date
    onChange?: any
    inputVariant?: 'outlined' | 'standard' | 'filled'
    id?: string
    minDate?: Date
    maxDate?: Date
    disabled?: boolean
}

export default function BasicDatePicker(props: BasicDatePickerProps) {
    const [value, setValue] = React.useState<Date | null>(props.defaultValue);

    const handleOnChange = (value) => {
        setValue(value);
        props.onChange(value);
    };

    useEffect(
        () => {
            setValue(props.defaultValue);
        },
        [props.defaultValue]
    );

    return (
        <LocalizationProvider dateAdapter={AdapterDateFns} locale={ruLocale}>
            <DatePicker
                mask={'__.__.____'}
                label={props.label}
                value={value}
                onChange={handleOnChange}
                clearable={true}
                minDate={props.minDate}
                maxDate={props.maxDate}
                disabled={props.disabled}
                renderInput={(params) => (
                    <TextField
                        id={props.id}
                        size="small"
                        variant={props.inputVariant || 'standard'}
                        {...params}
                        style={{...props.style,}}
                    />
                )}
            />
        </LocalizationProvider>
    );
}