import React, {useEffect, useState} from "react";
import {Box, TextField} from "@material-ui/core";

interface SimpleTextInputProps {
    label?: string,
    name: string,
    placeholder?: string,
    defaultValue?: string|number,
    error?: boolean,
    controlled?: boolean
    inputVariant?: 'standard' | 'outlined' | 'filled'
    size?: 'small' | 'medium'
    inputStyle?: object
    onChange?(value: string|any): void
    onBlur?(value: string|any): void
    id?: string
    disabled?: boolean
    helperText?: string
}

const defaultProps = {
    controlled: true,
    error: false,
};


export default function SimpleTextInput(props: SimpleTextInputProps) {
    const [value, setValue] = useState(props.defaultValue);

    const handleOnChange = (e) => {
        setValue(e.target.value);
        props.onChange(e);
    };

    useEffect(
        () => {
            setValue(props.defaultValue);
        },
        [props.defaultValue]
    );

    return (
        <Box style={props.inputStyle || {}}>
            <TextField
                onBlur={(e) => props.onBlur(e.target.value)}
                name={props.name}
                label={props.label || ''}
                value={value || ''}
                onChange={handleOnChange}
                variant={props.inputVariant || 'outlined'}
                placeholder={props.placeholder || ''}
                size={props.size || 'small'}
                style={{width: '100%',}}
                id={props.id}
                disabled={props.disabled}
                error={props.error}
                helperText={props.helperText}
            />
        </Box>
    );
}

SimpleTextInput.defaultProps = defaultProps;