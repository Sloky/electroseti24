import * as React from 'react';
import { FormControl, FormLabel, RadioGroup, FormControlLabel, Radio } from '@material-ui/core';
import {useEffect} from "react";

interface SimpleRadioGroupProps {
    groupLabel?: string
    defaultValue?: number
    style?: object
    options: {
        value: string
        title: string
    }[],
    onChange: any
}


export default function SimpleRadioGroup(props: SimpleRadioGroupProps) {
    const [value, setValue] = React.useState(props.defaultValue.toString() || null);

    const handleOnChange = (e) => {
        setValue(e.target.value);
        props.onChange(e.target.value);
    };

    useEffect(
        () => {
            setValue(props.defaultValue.toString());
        },
        [props.defaultValue]
    );

    return (
        <FormControl component="fieldset" style={props.style}>
            <FormLabel component="legend">{props.groupLabel}</FormLabel>
            <RadioGroup value={value} onChange={handleOnChange} row style={{display: 'flex', justifyContent: 'space-between',}}>
                {props.options.map((item, index) => (
                    <FormControlLabel key={index} value={item.value} control={<Radio />} label={item.title} />
                ))}
            </RadioGroup>
        </FormControl>
    );
}