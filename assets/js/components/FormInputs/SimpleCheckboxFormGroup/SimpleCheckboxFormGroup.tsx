import React from "react";
import {Checkbox, FormControl, FormControlLabel, FormGroup, FormLabel} from "@material-ui/core";

interface SimpleCheckboxFormGroupProps {
    name?: string
    id?: string
    items: {
        value: string
        label: string
    }[]
    defaultValues?: string[]
    formGroupLabel?: string
    isRow?: boolean
    labelPlacement?: 'start'|'end'|'bottom'|'top'
    error?: boolean
    onChange(e): void
}

const defaultProps: SimpleCheckboxFormGroupProps = {
    items: [],
    defaultValues: [],
    formGroupLabel: undefined,
    isRow: false,
    labelPlacement: 'end',
    onChange: () => {}
}

function SimpleCheckboxFormGroup(props: SimpleCheckboxFormGroupProps) {
    const handleGroupChange = (e) => {
        props.onChange(e);
    }

    return (
        <FormControl
            component="fieldset"
            onChange={handleGroupChange}
        >
            {props.formGroupLabel &&
                <FormLabel component="legend" sx={{color: props.error ? 'error.main' : 'text.primary'}}>{props.formGroupLabel}</FormLabel>
            }
            <FormGroup aria-label="position" row={props.isRow}>
                {props.items.map(item =>{
                    return (<FormControlLabel
                        value={item.value}
                        key={item.label}
                        name={props.name}
                        checked={props.defaultValues?.includes(item.value)}
                        control={<Checkbox />}
                        label={item.label}
                        labelPlacement={props.labelPlacement}
                    />)
                })}
            </FormGroup>
        </FormControl>
    );
}

SimpleCheckboxFormGroup.defaultProps = defaultProps;

export default SimpleCheckboxFormGroup;