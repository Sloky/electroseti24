import React from "react";

interface SimplePasswordInputProps {
    label: string,
    name1?: string,
    name2?: string,
    placeholder1?: string,
    placeholder2?: string,
    error?: {
        status: boolean,
        text?: string,
    },
    onChange?: any
    // checkPasswordsSimilarity?: any
}

const defaultProps = {
    error: {
        status: false,
        text: 'Ошибка',
    }
};

export default function SimplePasswordInput(props: SimplePasswordInputProps) {

    const checkPasswordsSimilarity = (e) => {
        let similarity = false;
        // props.checkPasswordsSimilarity(e, similarity)
    };

    return (
        <div className="form-group">
            <label className="form-label" htmlFor="password">{props.label}</label>
            <input
                type="password"
                id="password"
                name={props.name1}
                className={"form-control " + (props.error.status ? 'is-invalid' : '')}
                style={{marginBottom: "10px"}}
                placeholder={props.placeholder1}
            />
            <input
                type="password"
                id="confirm-password"
                name={props.name2}
                className={"form-control " + (props.error.status ? 'is-invalid' : '')}
                placeholder={props.placeholder2}
                // onChange={}
            />
            <div className="invalid-feedback">
                {props.error.text}
            </div>
        </div>
    );
}

SimplePasswordInput.defaultProps = defaultProps;