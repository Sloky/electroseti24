import * as React from 'react';
import {OutlinedInput, InputLabel, MenuItem, FormControl, ListItemText, Select, Checkbox, Box} from '@material-ui/core';

interface MultiSelectInputProps {
    selectInputStyle?: object
    labelId?: string
    labelTitle?: string
    id?: string
    defaultValues?: string[]
    options: {
        id: string
        title: string
    }[]
    onChange?: any
}

export default function MultiSelectInput(props: MultiSelectInputProps) {
    const [values, setValues] = React.useState<string[]>(props.defaultValues || []);

    const handleChange = (event) => {
        const {
            target: { value },
        } = event;
        setValues(
            typeof value === 'string' ? value.split(',') : value,
        );
        let companies = [];
        props.options.map((item1) => {
            return value.some((item2) => {
                if (item1.title === item2) {
                    companies.push(item1.id);
                }
            })
        });
        props.onChange(companies);
    };

    return (
        <Box>
            <FormControl sx={{ width: 300, ...props.selectInputStyle }}>
                <InputLabel id={props.labelId}>{props.labelTitle}</InputLabel>
                <Select
                    labelId={props.labelId}
                    id={props.id}
                    multiple
                    value={values}
                    onChange={handleChange}
                    input={<OutlinedInput label="Tag" />}
                    // style={{height: '36px'}}
                    renderValue={(selected) => selected.join(', ')}
                    size={'small'}
                >
                    {props.options.map((option) => (
                        <MenuItem key={option.id} value={option.title}>
                            <Checkbox checked={values.indexOf(option.title) > -1} />
                            <ListItemText primary={option.title} />
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
        </Box>
    );
}
