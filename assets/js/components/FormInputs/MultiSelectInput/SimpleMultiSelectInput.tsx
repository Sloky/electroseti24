import * as React from 'react';
import {
    InputLabel,
    MenuItem,
    FormControl,
    ListItemText,
    Select,
    Checkbox,
    Box,
    Input
} from '@material-ui/core';
import {useEffect} from "react";

interface SimpleMultiSelectInputProps {
    selectInputStyle?: object
    labelId?: string
    labelTitle?: string
    id?: string
    defaultValues?: string[]
    error?: boolean
    options: {
        value: string
        title: string
    }[]
    onChange(e: any): void
    disabled?: boolean
}

export default function SimpleMultiSelectInput(props: SimpleMultiSelectInputProps) {
    const [values, setValues] = React.useState<string[]>(props.defaultValues);

    const handleChange = (e) => {
        setValues(e.target.value);
        props.onChange(e);
    };

    useEffect(() => {
        setValues(props.defaultValues);
    }, [props.defaultValues]);

    return (
        <Box>
            <FormControl sx={{ width: 300, ...props.selectInputStyle }}>
                <InputLabel id={props.labelId}>{props.labelTitle}</InputLabel>
                <Select
                    error={props.error}
                    labelId={props.labelId}
                    id={props.id}
                    multiple
                    value={values}
                    onChange={handleChange}
                    input={<Input />}
                    disabled={props.disabled}
                    renderValue={(selected) => {
                        let titles: string[] = props.options.filter(
                            option => selected.includes(option.value)).map(item => item.title);
                        return titles.join(', ');
                    }}
                    size={'small'}
                >
                    {props.options.map((option) => (
                        <MenuItem key={option.title} value={option.value}>
                            <Checkbox checked={values.indexOf(option.value) > -1} />
                            <ListItemText primary={option.title} />
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
        </Box>
    );
}
