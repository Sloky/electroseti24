import React from "react";
import {Checkbox, FormControl, FormControlLabel, FormGroup, FormLabel} from "@material-ui/core";

interface CheckboxGroupProps {
    name?: string
    id?: string
    commonLabel?: string
    defaultValues: string[]
    items: {
        value: string
        label: string
    }[]
    onChange?: any
}

export default function CheckboxGroup(props: CheckboxGroupProps) {
    let checked: {} = {};

    return (
        <FormControl>
            <FormLabel>{props.commonLabel}</FormLabel>
            <FormGroup style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between',}}>
                {props.items.map((item, index) => {
                    checked[item.value] = props.defaultValues.includes(item.value)
                    return (
                        <FormControlLabel
                            key={index}
                            control={
                                <Checkbox
                                    defaultChecked={props.defaultValues.includes(item.value)}
                                    onChange={props.onChange}
                                    name={props.name}
                                    value={item.value}
                                />
                            }
                            label={item.label}
                        />
                    )
                })}
            </FormGroup>
        </FormControl>
    )
}