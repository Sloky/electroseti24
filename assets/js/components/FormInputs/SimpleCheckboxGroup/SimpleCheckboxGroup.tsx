import React, {useEffect} from "react";

interface SimpleCheckboxGroupProps {
    name: string,
    commonLabel?: string,
    defaultValues: string[]
    items: {
        value: string,
        label: string
    }[],
    onChange?: any
}



export default function SimpleCheckboxGroup(props: SimpleCheckboxGroupProps) {
    let checked: {id:string, checked:boolean}[] = [];

    useEffect(() => {
        checked.map(value => {
            let checkbox: any = document.getElementById(value.id);
            checkbox.checked = value.checked;
        })

    });

    return (
        <div className={'form-group'} style={{display: 'flex',}}>
            <h5 className="frame-heading" style={{color: '#666666'}}>{props.commonLabel}</h5>
            {props.items.map((item, index) => {
                checked.push({
                    id: `customSwitch${item.label}`,
                    checked: props.defaultValues.includes(item.value)
                });
                return (
                <div className="custom-control custom-checkbox" key={index} style={{marginRight: '10px',}}>
                    <input
                        name={props.name}
                        value={item.value}
                        // defaultChecked={checked}
                        type="checkbox"

                        className="custom-control-input"
                        id={`customSwitch${item.label}`}
                        onChange={props.onChange}
                    />
                    <label
                        className="custom-control-label"
                        htmlFor={`customSwitch${item.label}`}
                    >
                        {item.label}
                    </label>
                </div>
            )})}
        </div>
    );
}