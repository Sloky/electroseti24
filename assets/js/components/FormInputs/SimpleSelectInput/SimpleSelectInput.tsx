import React, {useEffect, useState} from "react";
import {FormControl, InputLabel, MenuItem, Select} from "@material-ui/core";

interface SimpleSelectInputProps {
    label: string
    name?: string
    id?: string
    style?: object
    allowEmptyValue?: boolean
    emptyValueTitle?: string
    defaultValue?: string|number
    placeholder?: string
    options: {
            value: string|number
            title: string
        }[],
    error?: boolean,
    onChange?: any
    disabled?: boolean
    formControlStyle?: object
}

const defaultProps = {
    error: false,
    emptyValueTitle: 'Не выбрано',
    allowEmptyValue: false,
    disabled: false,
    formControlStyle: {},
};

export default function SimpleSelectInput(props: SimpleSelectInputProps) {
    const [value, setValue] = useState(props.defaultValue !== null ? props.defaultValue : props.allowEmptyValue && 'noValue');
    const [options, setOptions] = useState(props.options);

    useEffect(() => {
        if (props.allowEmptyValue) {
            setOptions( [...props.options, {value: 'noValue', title: props.emptyValueTitle}]);
        }
    }, [props.options]);

    useEffect(() => {
        setValue(props.defaultValue !== null ? props.defaultValue : props.allowEmptyValue && 'noValue');
    }, [props.defaultValue]);

    const handleOnChange = (e) => {
        setValue(e.target.value);
        props.onChange(e.target.value == 'noValue' ? null : e.target.value);
    };

    return (
        <FormControl style={{...props.formControlStyle}}>
            <InputLabel id={props.name}>{props.label}</InputLabel>
            <Select
                labelId={props.name}
                id={props.id}
                value={value}
                label={props.label}
                onChange={handleOnChange}
                style={{...props.style,}}
                size="small"
                disabled={props.disabled}
                error={props.error}
            >
                {options.map((option, index) => {
                    return (<MenuItem key={index} value={option.value}>{option.title}</MenuItem>);
                })}
            </Select>
        </FormControl>
    );
}

SimpleSelectInput.defaultProps = defaultProps;