import React, {useEffect, useState} from "react";
import {FormControl, InputLabel, TextField} from "@material-ui/core";

interface SimpleTextareaInputProps {
    label?: string
    defaultValue?: string
    rows?: number
    name: string
    placeholder?: string
    controlled?: boolean
    style?: object
    error?: {
        status: boolean
        text?: string
    },
    onChange?(value: string|any): void
    onBlur?(value: string|any): void
}

const defaultProps = {
    controlled: true,
    error: {
        status: false,
        text: 'Ошибка',
    }
};

export default function SimpleTextareaInput(props: SimpleTextareaInputProps) {
    const [value, setValue] = useState(props.defaultValue);

    const handleOnChange = (e) => {
        setValue(e.target.value);
        props.onChange(e.target.value);
    };

    useEffect(
        () => {
            setValue(props.defaultValue);
        },
        [props.defaultValue]
    );

    return (
        <FormControl>
            <InputLabel defaultValue={props.label} />
            <TextField
                label={props.label}
                value={value}
                name={props.name}
                style={props.style}
                multiline
                placeholder={props.placeholder}
                onChange={handleOnChange}
                onBlur={(e) => props.onBlur(e.target.value)}
            />
        </FormControl>
    );
}

SimpleTextareaInput.defaultProps = defaultProps;