import React from "react";
import {Box, IconButton} from "@material-ui/core";
import {FirstPage, KeyboardArrowLeft, KeyboardArrowRight, LastPage} from "@material-ui/icons";

interface TablePaginationProps {
    count: number
    page: number
    rowsPerPage: number
    onPageChange: (
        // event: React.MouseEvent<HTMLButtonElement>,
        newPage: number,
    ) => void
}

export default function TablePagination(props: TablePaginationProps) {
    const {count, page, rowsPerPage, onPageChange} = props;
    console.log('count', count, 'page', page, 'rowsPerPage', rowsPerPage);
    const handleFirstPageButtonClick = () => {
        onPageChange(1);
        console.log('handleFirstPageButtonClick', 'count', count, 'page', page, 'rowsPerPage', rowsPerPage);
    };

    const handleBackButtonClick = () => {
        onPageChange(page - 1);
        console.log('handleBackButtonClick', 'count', count, 'page', page, 'rowsPerPage', rowsPerPage);
    };

    const handleNextButtonClick = () => {
        onPageChange(page + 1);
        console.log('handleNextButtonClick', 'count', count, 'page', page, 'rowsPerPage', rowsPerPage);
    };

    const handleLastPageButtonClick = () => {
        onPageChange(Math.max(1, Math.ceil(count / rowsPerPage) - 1));
        console.log('handleLastPageButtonClick', 'count', count, 'page', page, 'rowsPerPage', rowsPerPage);
    };

    return (
        <Box>
            <IconButton
                onClick={handleFirstPageButtonClick}
                disabled={page === 1}
            >
                <FirstPage/>
            </IconButton>
            <IconButton
                onClick={handleBackButtonClick}
                disabled={page === 1}
            >
                <KeyboardArrowLeft />
            </IconButton>
            <IconButton
                onClick={handleNextButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
            >
                <KeyboardArrowRight/>
            </IconButton>
            <IconButton
                onClick={handleLastPageButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
            >
                <LastPage/>
            </IconButton>
        </Box>
    )
}