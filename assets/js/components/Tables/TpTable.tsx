import React from "react";
import {useHistory} from "react-router";
import {Box, Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@material-ui/core";
import {THEME} from "../../theme";
import {CounterPostData} from "../../interfaces/interfaces";

interface TpTableProps {
    headerItems: string[]
    tableDataRows: CounterPostData[]
}

export default function TpTable(props: TpTableProps) {
    const history = useHistory();

    const openCounter = (id: string) => {
        history.push(`/tp/counter/${id}`)
    }

    return (
        <TableContainer sx={{bgcolor: 'background.default',}}>
            <Table>
                <TableHead>
                    <TableRow>
                        {props.headerItems.map((value, index) => (
                            <TableCell key={index}>{value}</TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {props.tableDataRows && props.tableDataRows.length ? props.tableDataRows.map((counter, index) => {
                        return (
                            <TableRow
                                hover
                                key={index}
                                onClick={() => openCounter(counter.counter_id)}
                                style={{cursor: 'pointer'}}
                            >
                                <TableCell style={{fontSize: '15px',}}>
                                    {counter.agreement_number}
                                </TableCell>
                                <TableCell style={{fontSize: '15px',}}>
                                    {counter.subscriber_title}
                                </TableCell>
                                <TableCell style={{fontSize: '15px',}}>
                                    {counter.counter_title}
                                </TableCell>
                                <TableCell style={{fontSize: '15px',}}>
                                    {new Date(counter.last_counter_value_date).toLocaleDateString('ru')}
                                </TableCell>
                                <TableCell style={{fontSize: '15px',}}>
                                    {counter.last_counter_value}
                                </TableCell>
                            </TableRow>
                        )
                    }): (
                        <></>
                    )}
                </TableBody>
            </Table>
            {props.tableDataRows && !props.tableDataRows.length &&
            <Box style={{padding: '70px 0px', textAlign: 'center', width: '100%', color: THEME.PLACEHOLDER_TEXT, fontSize: '16px',}}>
                Нет данных
            </Box>
            }
        </TableContainer>
    )
}