import React from "react";
import {Box, Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@material-ui/core";
import {TaskTableData} from "../../interfaces/interfaces";
import {THEME} from "../../theme";
import {useHistory} from "react-router";
import {changeTrView} from "../../functions/functions";

interface WorkTableProps {
    headerItems: string[]
    tableDataRows: TaskTableData[]
}

export default function WorkTable(props: WorkTableProps) {
    const history = useHistory();

    const openTask = (id: string) => {
        history.push(`/task-to-work/edit/${id}`)
    }

    return (
        <TableContainer sx={{bgcolor: 'background.default'}}>
            <Table>
                <TableHead>
                    <TableRow>
                        {props.headerItems.map((value, index) => (
                            <TableCell key={index}>{value}</TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {props.tableDataRows.length ? props.tableDataRows.map((task, index) => {
                        return (
                            <TableRow
                                hover
                                key={index}
                                sx={changeTrView(task.trView)}
                                onClick={() => openTask(task.id)}
                                style={{ cursor: 'pointer',}}
                            >
                                <TableCell style={{fontSize: '15px',}}>
                                    {task.counterNumber}
                                </TableCell>
                                <TableCell style={{fontSize: '15px',}}>{task.companyTitle}</TableCell>
                                <TableCell style={{fontSize: '15px',}}>{task.address}</TableCell>
                                <TableCell style={{fontSize: '15px',}}>{task.deadline}</TableCell>
                                {task.userFullName ? (
                                    <TableCell style={{fontSize: '15px',}}>{task.userFullName}</TableCell>
                                ) : (
                                    <TableCell style={{minWidth: '140px',}}>
                                        <Box
                                            component="span"
                                            style={{
                                                color: THEME.WARNING_TEXT,
                                                backgroundColor: THEME.WARNING_BACKGROUND,
                                                borderRadius: '10px',
                                                padding: '2px 8px',
                                                fontSize: '15px',
                                            }}
                                        >
                                            Не назначен
                                        </Box>
                                    </TableCell>
                                )}
                            </TableRow>
                        )
                    }) : (
                        <></>
                    )}
                </TableBody>
            </Table>
            {!props.tableDataRows.length &&
            <Box style={{padding: '70px 0px', textAlign: 'center', width: '100%', color: THEME.PLACEHOLDER_TEXT, fontSize: '16px',}}>
                Нет данных
            </Box>
            }
        </TableContainer>
    );
}