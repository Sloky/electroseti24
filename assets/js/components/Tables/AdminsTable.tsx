import React from "react";
import {UserData} from "../../interfaces/interfaces";
import {Box, Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@material-ui/core";
import {useHistory, useParams} from "react-router";
import {THEME} from "../../theme";

const listHeader = [
    'ФИО',
    'Телефон',
];

interface AdminsTableProps {
    users: UserData[]
}

export default function AdminsTable(props: AdminsTableProps) {
    const history = useHistory();
    let params: any = useParams();
    let role = params.role;

    return (
        <>
            <TableContainer sx={{bgcolor: 'background.default'}}>
                <Table>
                    <TableHead>
                        <TableRow>
                            {listHeader.map((value, index) => (
                                <TableCell key={index}>{value}</TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {Array.isArray(props.users) && props.users.length ? props.users.map((user, index) => (
                            <TableRow hover key={index} style={{cursor: 'pointer',}} onClick={() => history.push(`/${role}/user/${user.id}`)}>
                                <TableCell style={{fontSize: '15px',}}>
                                    {user.surname + ' ' + user.name + ' ' + user.patronymic}
                                </TableCell>
                                <TableCell style={{fontSize: '15px',}}>
                                    {user.phone}
                                </TableCell>
                            </TableRow>
                        )) : (
                            <></>
                        )}
                    </TableBody>
                </Table>
            </TableContainer>
            {(!props.users || !props.users.length) && (
                <Box style={{padding: '70px 0px', textAlign: 'center', width: '100%', color: THEME.PLACEHOLDER_TEXT, fontSize: '16px',}}>
                    Нет данных
                </Box>
            )}
        </>
    )
}