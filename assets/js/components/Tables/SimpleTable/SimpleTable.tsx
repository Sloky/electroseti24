import React from "react";
import {NavLink} from "react-router-dom";
import {userStatuses} from "../../../pages/Users/UsersPage/UsersList";
import {Box, Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@material-ui/core";
import {TaskTableData} from "../../../interfaces/interfaces";
import {THEME} from "../../../theme";
import {useHistory} from "react-router";

interface SimpleTableProps {
    headerItems: string[]
    tableDataRows: TaskTableData[]
}

// /**
//  * @todo
//  * @param props
//  * @constructor
//  */
export default function SimpleTable(props: SimpleTableProps) {
    const history = useHistory()

    // const tableRowClick = (id: string) => {
    //     history.push(`/task-to-work/edit/${id}`)
    // }

    return (
        <TableContainer sx={{bgcolor: 'background.default'}}>
            <Table>
                <TableHead>
                    <TableRow>
                        {props.headerItems.map((value, index) => (
                            <TableCell key={index}>{value}</TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {props.tableDataRows.length ? props.tableDataRows.map((task, index) => {
                        return (
                            <TableRow hover key={index}>
                                {/*<TableCell style={{fontSize: '15px',}}>{task.id}</TableCell>*/}
                                <TableCell style={{fontSize: '15px',}}>
                                    <NavLink to={`/task-to-work/edit/${task.id}`}>
                                        {task.counterNumber}
                                    </NavLink>
                                </TableCell>
                                <TableCell style={{fontSize: '15px',}}>{task.companyTitle}</TableCell>
                                <TableCell style={{fontSize: '15px',}}>{task.address}</TableCell>
                                <TableCell style={{fontSize: '15px',}}>{task.deadline}</TableCell>
                                {task.userFullName ? (
                                    <TableCell style={{fontSize: '15px',}}>{task.userFullName}</TableCell>
                                ) : (
                                    <TableCell style={{minWidth: '140px',}}>
                                        <Box
                                            component="span"
                                            style={{
                                                color: THEME.WARNING_TEXT,
                                                backgroundColor: THEME.WARNING_BACKGROUND,
                                                borderRadius: '10px',
                                                padding: '2px 8px',
                                                fontSize: '15px',
                                            }}
                                        >
                                            Не назначен
                                        </Box>
                                    </TableCell>
                                )}
                            </TableRow>
                        )
                    }) : (
                        <TableRow><TableCell>Нет данных</TableCell></TableRow>
                    )}
                </TableBody>
            </Table>
        </TableContainer>


        // <table className="table table-hover table-bordered table-striped" id="table-example">
        //     <thead>
        //     <tr>
        //         {props.listHeader.map((value, index) =>
        //             (<th key={index}>{value}</th>)
        //         )}
        //     </tr>
        //     </thead>
        //     <tbody>
        //     {Array.isArray(props.listData) && props.listData.length ?
        //         props.listData.map((user, index) =>
        //             (<tr key={index}>
        //                 <td>
        //                     <NavLink to={`/user/edit/${user.id}`}>
        //                         {user.surname + ' ' + user.name + ' ' + user.patronymic}
        //                     </NavLink>
        //                 </td>
        //                 <td>{user.phone}</td>
        //                 <td>{user.groups.map((group, index) => {return user.groups.length == 1+index ? group.group_name: group.group_name+', '})}</td>
        //                 <td>{userStatuses[user.work_status]}</td>
        //                 <td>-</td>
        //             </tr>)
        //         )
        //         :
        //         (<tr><td colSpan={props.listHeader.length} align={"center"}>Нет данных</td></tr>)
        //     }
        //
        //     </tbody>
        // </table>
    );
}