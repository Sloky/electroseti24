import React from "react";
import {UserData} from "../../interfaces/interfaces";
import {useHistory, useParams} from "react-router";
import {Box, Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@material-ui/core";
import {THEME} from "../../theme";

const listHeader = [
    'ФИО',
    'Отделение',
    'Телефон',
    'Статус',
    'Количество задач к выполнению',
];

interface ControllersTableProps {
    users: UserData[]
}

const userStatuses = {
    1: 'В работе',
    2: 'Не в работе',
};

export default function ControllersTable(props: ControllersTableProps) {
    const history = useHistory();
    let params: any = useParams();
    let role = params.role;

    const companiesToString = (companies: {title: string}[]) => {
        let stringOfCompanies = companies.map((company, index) => {
            return company.title
        })
        return stringOfCompanies.join(', ');
    }

    return (
        <>
            <TableContainer sx={{bgcolor: 'background.default'}}>
                <Table>
                    <TableHead>
                        <TableRow>
                            {listHeader.map((value, index) => (
                                <TableCell key={index}>{value}</TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {Array.isArray(props.users) && props.users.length ? props.users.map((user, index) => (
                            <TableRow hover key={index} style={{cursor: 'pointer',}} onClick={() => history.push(`/${role}/user/${user.id}`)}>
                                <TableCell style={{fontSize: '15px',}}>
                                    {user.surname + ' ' + user.name + ' ' + user.patronymic}
                                </TableCell>
                                <TableCell style={{fontSize: '15px',}}>{companiesToString(user.companies)}</TableCell>
                                <TableCell style={{fontSize: '15px',}}>
                                    {user.phone}
                                </TableCell>
                                <TableCell style={{fontSize: '15px', minWidth: '132px',}}>
                                    <Box component="span" sx={user.work_status === 1
                                        ? ({backgroundColor: 'success.light', color: 'success.main', borderRadius: '12px', padding: '2px 8px',})
                                        : ({backgroundColor: 'warning.light', color: 'warning.dark', borderRadius: '12px', padding: '2px 8px',})}
                                    >
                                        {userStatuses[user.work_status]}
                                    </Box>
                                </TableCell>
                                <TableCell style={{fontSize: '15px',}}>
                                    {user.task_to_work_count}
                                </TableCell>
                            </TableRow>
                        )) : (
                            <></>
                        )}
                    </TableBody>
                </Table>
            </TableContainer>
            {(!props.users || !props.users.length) && (
                <Box style={{padding: '70px 0px', textAlign: 'center', width: '100%', color: THEME.PLACEHOLDER_TEXT, fontSize: '16px',}}>
                    Нет данных
                </Box>
            )}
        </>
    )
}