import React, {useEffect} from "react";
import {RecordTableData} from "../../interfaces/interfaces";
import {useHistory} from "react-router";
import {
    Box,
    Checkbox,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Tooltip
} from "@material-ui/core";
import {changeTrView} from "../../functions/functions";
import {THEME} from "../../theme";

interface BypassTableProps {
    headerItems: string[]
    tableDataRows: RecordTableData[]
    onRecordSelect(value): void
    selected?: string[]
    itemsCount?: number
    totalItems?: number
}

export default function BypassTable(props: BypassTableProps) {
    const [selected, setSelected] = React.useState<readonly string[]>(props.selected);

    const history = useHistory();

    const openRecord = (id: string) => {
        history.push(`/bypass/edit/${id}`)
    }

    const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
        if (event.target.checked) {
            const newSelected = props.tableDataRows.map((n) => n.id);
            props.onRecordSelect(newSelected)
            return;
        }
        props.onRecordSelect([]);
    };

    const handleClick = (event: React.MouseEvent<unknown>, id: string) => {
        const selectedIndex = selected.indexOf(id);
        let newSelected: readonly string[] = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, id);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }

        props.onRecordSelect(newSelected);
    };

    const isSelected = (id: string) => selected.indexOf(id) !== -1;

    useEffect(() => {
        setSelected(props.selected)
    }, [props.selected])

    return (
        <TableContainer sx={{bgcolor: 'background.default'}}>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell padding={'checkbox'}>
                            <Checkbox
                                color={'primary'}
                                // indeterminate={numSelected > 0 && numSelected < rowCount}
                                checked={props.itemsCount > 0 && (selected.length === props.itemsCount || selected.length === props.totalItems) && props.totalItems > 0}
                                onChange={handleSelectAllClick}
                            />
                        </TableCell>
                        {props.headerItems.map((value, index) => (
                            <TableCell key={index}>{value}</TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {props.tableDataRows.length ? props.tableDataRows.map((record, index) => {
                        const isItemSelected = isSelected(record.id);

                        return (
                            <Tooltip
                                disableHoverListener={!record.errors.length}
                                title={record.errors.map((item, index) => <Box key={index}>{item.title}</Box>)}
                                key={index}
                                arrow
                            >
                                <TableRow
                                    hover
                                    sx={changeTrView(record.trView)}
                                    style={{cursor: 'pointer', ...changeTrView(record.trView)}}
                                >
                                    <TableCell padding="checkbox" onClick={(event) => handleClick(event, record.id)}>
                                        <Checkbox
                                            color="primary"
                                            checked={isItemSelected}
                                        />
                                    </TableCell>
                                    <TableCell style={{fontSize: '15px',}} onClick={() => openRecord(record.id)}>
                                        {record.agreementNumber}
                                    </TableCell>
                                    <TableCell style={{fontSize: '15px',}} onClick={() => openRecord(record.id)}>
                                        {record.subscriberTitle}
                                    </TableCell>
                                    <TableCell style={{fontSize: '15px',}} onClick={() => openRecord(record.id)}>
                                        {record.counterNumber}
                                    </TableCell>
                                    <TableCell style={{fontSize: '15px',}} onClick={() => openRecord(record.id)}>
                                        {record.companyTitle}
                                    </TableCell>
                                    {record.userFullName ? (
                                        <TableCell style={{fontSize: '15px',}} onClick={() => openRecord(record.id)}>{record.userFullName}</TableCell>
                                    ) : (
                                        <TableCell style={{minWidth: '140px',}} onClick={() => openRecord(record.id)}>
                                            <Box
                                                component="span"
                                                style={{
                                                    color: THEME.WARNING_TEXT,
                                                    backgroundColor: THEME.WARNING_BACKGROUND,
                                                    borderRadius: '10px',
                                                    padding: '2px 8px',
                                                    fontSize: '15px',
                                                }}
                                            >
                                                Не назначен
                                            </Box>
                                        </TableCell>
                                    )}
                                    <TableCell style={{fontSize: '15px',}} onClick={() => openRecord(record.id)}>
                                        {(record.deadline === 'Invalid Date' || record.deadline === null) ? 'Нет даты' : record.deadline}
                                    </TableCell>
                                    <TableCell style={{fontSize: '15px',}} onClick={() => openRecord(record.id)}>
                                        {record.trView === 'task_danger' ? 'Критические ошибки' : (record.trView === 'task_warning' ? 'Требует дополнения' : 'Готово к постановке')}
                                    </TableCell>
                                </TableRow>
                            </Tooltip>
                        )
                    }): (
                        <></>
                    )}
                </TableBody>
            </Table>
            {!props.tableDataRows.length &&
            <Box style={{padding: '70px 0px', textAlign: 'center', width: '100%', color: THEME.PLACEHOLDER_TEXT, fontSize: '16px',}}>
                Нет данных
            </Box>
            }
        </TableContainer>
    )
}