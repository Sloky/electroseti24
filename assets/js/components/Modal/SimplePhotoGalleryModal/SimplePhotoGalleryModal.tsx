import React from "react";
import SimpleModal from "../SimpleModal/SimpleModal";
import './SimplePhotoGalleryModal.css';
import PhotoCard from "../../Other/PhotoCard/PhotoCard";
import {Box} from "@material-ui/core";

interface SimplePhotoGalleryModalProps {
    modalName: string
    modalTitle?: string
    photos: string[]
}

export default function SimplePhotoGalleryModal(props: SimplePhotoGalleryModalProps) {

    return (
        <>
            {props.photos.length ?
                <SimpleModal
                    modalName={props.modalName}
                    modalTitle={props.modalTitle}
                    startButtonTitle={'Фотографии'}
                    acceptButtonShow={false}
                >
                    <Box className={'photos_container'}>
                        {props.photos.map(photo => (
                                <PhotoCard
                                    uri={photo}
                                    key={photo}
                                />
                            ))}
                    </Box>
                </SimpleModal>
                : null
            }
        </>
    );
}