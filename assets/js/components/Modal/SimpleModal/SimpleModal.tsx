import React, {useState} from "react";
import {Box, Button, IconButton, Modal, Typography} from "@material-ui/core";
import {Close} from "@material-ui/icons";
// import "./SimpleModal.css";

export interface SimpleModalProps {
    modalName: string
    modalTitle?: string
    startButtonTitle?: string
    startButtonClass?: string
    startButtonStyle?: object
    startButtonVariant?: 'contained' | 'outlined' | 'text'
    closeButtonTitle?: string
    acceptButtonTitle?: string
    acceptButtonShow?: boolean
    disableAcceptButton?: boolean
    disableStartButton?: boolean
    onModalSubmit?: any
    startIcon?: any
    onModalClose?(): void
    children?: any
}

const defaultProps: SimpleModalProps = {
    modalName: '',
    modalTitle: 'Модальное окно',
    startButtonTitle: 'Изменить',
    acceptButtonTitle: 'Сохранить',
    acceptButtonShow: true,
    disableAcceptButton: false,
    startIcon: null,
    startButtonVariant: 'outlined',
    onModalClose(): void {

    }
};

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '558px',
    bgcolor: 'background.paper',
    boxShadow: 24,
    padding: '45px',
    borderRadius: '4px',
    color: 'primary.main',
};

export default function SimpleModal(props: SimpleModalProps) {
    const [open, setOpen] = useState<boolean>(false)
    const handleOpen = () => setOpen(true);
    const handleClose = () => {
        props.onModalClose()
        setOpen(false)
    };
    const handleSubmit = () => {
        setOpen(false);
        props.onModalSubmit();
    }

    return (
        <>
            <Button
                variant={props.startButtonVariant}
                className={'base_styled_btn'}
                style={{...props.startButtonStyle}}
                startIcon={props.startIcon}
                onClick={handleOpen}
                disabled={props.disableStartButton}
            >
                {props.startButtonTitle}
            </Button>

            <Modal
                open={open}
                onClose={handleClose}
            >
                <Box sx={style}>
                    <Typography component="h2" sx={{color: 'secondary.main', textAlign: 'center', marginBottom: '30px', fontSize: '25px',}}>
                        {props.modalTitle}
                    </Typography>
                    <IconButton
                        style={{position: 'absolute', top: 0, right: 0,}}
                        onClick={handleClose}
                        color='inherit'
                    >
                        <Close/>
                    </IconButton>
                    <Box sx={{textAlign: 'center', marginBottom: '30px', color: 'text.primary', fontSize: '18px',}}>
                        {props.children}
                    </Box>
                    <Box style={{display: 'flex',}}>
                        {props.closeButtonTitle && (
                            <Box style={{width: '100%', paddingRight: props.closeButtonTitle && props.acceptButtonShow ? '7.5px' : '0px',}}>
                                <Button
                                    onClick={handleClose}
                                    variant="outlined"
                                    className={'base_styled_btn'}
                                    style={{width: '100%',}}
                                >
                                    {props.closeButtonTitle}
                                </Button>
                            </Box>
                        )}
                        {props.acceptButtonShow && (
                            <Box style={{width: '100%', paddingLeft: props.closeButtonTitle && props.acceptButtonShow ? '7.5px' : '0px',}}>
                                <Button
                                    onClick={handleSubmit}
                                    variant="contained"
                                    className={'base_styled_btn'}
                                    style={{width: '100%',}}
                                    disabled={props.disableAcceptButton}
                                >
                                    {props.acceptButtonTitle}
                                </Button>
                            </Box>
                        )}
                    </Box>
                </Box>
            </Modal>
        </>
    );
}

SimpleModal.defaultProps = defaultProps;