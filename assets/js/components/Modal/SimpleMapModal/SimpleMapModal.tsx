import React, {useEffect, useState} from "react";
import SimpleModal from "../SimpleModal/SimpleModal";
import { Placemark, YMaps, Map, SearchControl, TypeSelector } from "react-yandex-maps";
import {Box} from "@material-ui/core";

interface SimpleMapModalProps {
    coordX?: number
    coordY?: number
    modalName: string
    modalTitle?: string
    startButtonTitle?: string
    startButtonClass?: string
    startButtonStyle?: object
    closeButtonTitle?: string
    acceptButtonTitle?: string
    acceptButtonShow?: boolean
    disableAcceptButton?: boolean
    disableStartButton?: boolean
    enableSearch?: boolean
    onModalSubmit(value): void
    children?: any
}


const defaultCoords = {
    x: 42.974386318699494,
    y: 47.49688625008533
};

export default function SimpleMapModal(props: SimpleMapModalProps) {
    const initialCoordinates = {
        x: props.coordX || defaultCoords.x,
        y: props.coordY || defaultCoords.y
    };

    const [coordinates, setCoordinates] = useState(initialCoordinates);
    const [acceptBtnDisabled, setAcceptBtnDisabled] = useState<boolean>(true);

    const onSubmit = () => {
        props.onModalSubmit(coordinates)
    };

    const onModalClose = () => {
        setCoordinates(initialCoordinates);
        setAcceptBtnDisabled(true);
    };

    const onMapClick = (e: any) => {
        if (props.enableSearch) {
            let coords = e.get('coords');
            setCoordinates({
                x: coords[0],
                y: coords[1]
            });
        }
        setAcceptBtnDisabled(false);
    };

    const dragHandler = (e: any) => {
        let coords = e.originalEvent.target.geometry.getCoordinates();
        setCoordinates({
            x: coords[0],
            y: coords[1]
        });
        setAcceptBtnDisabled(false);
    };

    useEffect(() => {
        setCoordinates(initialCoordinates);
    }, [props])

    return (
        <SimpleModal
            modalName={props.modalName}
            modalTitle={props.modalTitle}
            disableAcceptButton={acceptBtnDisabled}
            startButtonTitle={props.startButtonTitle}
            startButtonStyle={props.startButtonStyle}
            disableStartButton={props.disableStartButton}
            acceptButtonShow={props.acceptButtonShow}
            onModalClose={onModalClose}
            onModalSubmit={onSubmit}
        >
            <YMaps
                query={{
                    apikey: 'eff48c1b-de45-4d2f-a5fe-75870e7978e7'
                }}
            >
                <Box>
                    <Map state={{ center: [coordinates.x, coordinates.y], zoom: 18, type: 'yandex#hybrid'}} onClick={onMapClick} width={"100%"} height={"60vh"}>
                        <Placemark
                            geometry={[coordinates.x, coordinates.y]}
                            options={{
                                draggable: props.acceptButtonShow !== false,
                                iconColor: "red",
                                preset: 'islands#redCircleDotIcon'
                            }}
                            onDragend={dragHandler}
                        />
                        <TypeSelector options={{ float: 'right' }} mapTypes={['yandex#hybrid', 'yandex#map', 'yandex#satellite']}/>
                        {props.enableSearch ?
                            <SearchControl
                                options={{ float: 'right' }}
                            />
                            : null}
                    </Map>
                </Box>
            </YMaps>
        </SimpleModal>
    );
}