import React, {useState} from "react";
import SimpleModal from "../SimpleModal/SimpleModal";
import SimpleTextInput from "../../FormInputs/SimpleTextInput/SimpleTextInput";
import {isMatched} from "../../../functions/functions";

interface ChangeValueModalProps {
    initialValue: number
    modalName: string
    modalTitle?: string
    inputLabel?: string
    startButtonTitle?: string
    startButtonClass?: string
    startButtonStyle?: object
    closeButtonTitle?: string
    acceptButtonTitle?: string
    disableAcceptButton?: boolean
    onModalSubmit(value): void
    children?: any
}

export function SimpleNumberInputModal(props: ChangeValueModalProps) {
    const [value, setValue] = useState<string>(props.initialValue ? props.initialValue.toString() : '');
    const [isValid, setIsValid] = useState<boolean>(true);
    const [acceptBtnDisabled, setAcceptBtnDisabled] = useState<boolean>(true);

    const onSubmit = () => {
        props.onModalSubmit(value)
    };

    const onModalClose = () => {
        setValue(props.initialValue ? props.initialValue.toString() : '');
        setAcceptBtnDisabled(true);
        setIsValid(true);
    };

    const onItemChange = (value: string) => {
        setValue(value);
        if (isMatched(value, new RegExp(/^\d+(\.\d+)?$/))) {
            setIsValid(true);
            setAcceptBtnDisabled(false);
        } else {
            setIsValid(false);
            setAcceptBtnDisabled(true);
        }
    };

    return (
        <SimpleModal
            modalName={props.modalName}
            modalTitle={props.modalTitle}
            startButtonTitle={props.startButtonTitle}
            onModalSubmit={onSubmit}
            onModalClose={onModalClose}
            disableAcceptButton={acceptBtnDisabled}
        >
            <SimpleTextInput
                label={props.inputLabel}
                name={props.modalName}
                // error={{
                //     status: !isValid,
                //     text: 'Недопустимое значение'
                // }}
                defaultValue={value || ''}
                onChange={(value) => {onItemChange(value.target.value)}}
            />
        </SimpleModal>
    );
}