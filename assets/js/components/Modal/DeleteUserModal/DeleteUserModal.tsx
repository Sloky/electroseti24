import React, {useState} from "react";
import "./DeleteUserModal.css";

interface DeleteUserModalProps {
    modalName: string,
    modalTitle?: string,
    buttonStartTitle?: string,
    buttonCloseTitle?: string,
    buttonAcceptTitle?: string,
    onModalSubmit?: any,
    children?: any
}

const defaultProps: DeleteUserModalProps = {
    modalName: 'deleteUserModal',
    modalTitle: 'Удаление пользователя',
    buttonStartTitle: 'Удалить пользователя',
    buttonCloseTitle: 'Отмена',
    buttonAcceptTitle: 'Подтвердить',
    children: 'Удалить пользователя?'
};

export default function DeleteUserModal(props: DeleteUserModalProps) {

    const handleModalSubmit = (e) => {
        props.onModalSubmit(e);
    };

    return (
        <>
            <button type="button"
                    className="btn btn-lg btn-outline-danger waves-effect waves-themed"
                    data-toggle="modal"
                    data-target={'#'+props.modalName}
            >
                {props.buttonStartTitle}
            </button>
            <div className="modal fade" id={props.modalName} tabIndex={-1} role="dialog"
                 aria-hidden="true" style={{display: "none"}}>
                <div className="modal-dialog modal-sm modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">{props.modalTitle}</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true"><i className="fal fa-times"></i></span>
                            </button>
                        </div>
                        <div className="modal-body">
                            {props.children}
                        </div>
                        <div className="modal-footer delete_user_modal_footer">
                            <button
                                type="button"
                                    className="btn btn-secondary waves-effect waves-themed"
                                    data-dismiss="modal"
                            >
                                {props.buttonCloseTitle}
                            </button>
                            <button
                                type="button"
                                className="btn btn-primary waves-effect waves-themed"
                                data-dismiss={'modal'}
                                onClick={handleModalSubmit}
                            >
                                {props.buttonAcceptTitle}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

DeleteUserModal.defaultProps = defaultProps;