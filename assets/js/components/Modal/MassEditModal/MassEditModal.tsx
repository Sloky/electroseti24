import React, {useEffect, useState} from "react";
import SimpleModal from "../SimpleModal/SimpleModal";
import SimpleSelectInput from "../../FormInputs/SimpleSelectInput/SimpleSelectInput";
import {Box, Tooltip, Typography} from "@material-ui/core";
import BasicDatePicker from "../../DatePicker/BasicDatePicker";

interface selectOptionProps {
    title: string
    value: string
}

interface SimpleSelectModalProps {
    selectStyle?: object
    taskTypeOptions?: selectOptionProps[]
    executorOptions?: selectOptionProps[]
    modalName: string
    modalTitle?: string
    modalSubTitle?: string | JSX.Element
    startButtonTitle?: string
    startButtonClass?: string
    startButtonStyle?: object
    disableStartButton?: boolean
    closeButtonTitle?: string
    acceptButtonTitle?: string
    disableAcceptButton?: boolean
    startButtonVariant?: 'contained' | 'outlined' | 'text'
    onModalSubmit(values): void
    children?: any
}

export default function SimpleSelectModal(props: SimpleSelectModalProps) {
    const [taskTypeValue, setTaskTypeValue] = useState(null);
    const [executorValue, setExecutorValue] = useState(null);
    const [deadlineValue, setDeadlineValue] = useState(null);
    const [acceptBtnDisabled, setAcceptBtnDisabled] = useState<boolean>(true);

    const onSubmit = () => {
        props.onModalSubmit({
            taskType: taskTypeValue,
            executor: executorValue,
            deadline: deadlineValue,
        });
    };

    const onModalClose= () => {
        setTaskTypeValue(null);
        setExecutorValue(null);
        setDeadlineValue(null);
        setAcceptBtnDisabled(true);
    };

    const onTaskTypeChange = (value) => {
        setTaskTypeValue(value);
        setAcceptBtnDisabled(false);
    }

    const onExecutorChange = (value) => {
        setExecutorValue(value);
        setAcceptBtnDisabled(false);
    }

    const onDeadlineChange = (value) => {
        setDeadlineValue(value);
        setAcceptBtnDisabled(false);
    }

    useEffect(() => {
        if (taskTypeValue === null && executorValue === null && deadlineValue === null) {
            setAcceptBtnDisabled(true);
        } else {
            setAcceptBtnDisabled(false);
        }
    }, [taskTypeValue, executorValue, deadlineValue]);

    return (
        <SimpleModal
            modalName={props.modalName}
            modalTitle={props.modalTitle}
            startButtonTitle={props.startButtonTitle}
            startButtonClass={props.startButtonClass}
            startButtonStyle={props.startButtonStyle}
            acceptButtonTitle={props.acceptButtonTitle}
            startButtonVariant={props.startButtonVariant}
            onModalClose={onModalClose}
            onModalSubmit={onSubmit}
            disableAcceptButton={(props.disableAcceptButton !== false) && acceptBtnDisabled}
            disableStartButton={props.disableStartButton}
            closeButtonTitle={props.closeButtonTitle}
        >
            <Typography sx={{marginBottom: '30px', fontSize: '18px',}}>{props.modalSubTitle}</Typography>
            <Tooltip title={'Функционал пока недоступен'} arrow>
                <Box>
                    <SimpleSelectInput
                        formControlStyle={{width: '100%',}}
                        label={'Изменить тип задачи'}
                        options={props.taskTypeOptions || []}
                        allowEmptyValue={true}
                        emptyValueTitle={'Снятие показаний'}
                        defaultValue={taskTypeValue}
                        onChange={onTaskTypeChange}
                        disabled={true}
                    />
                </Box>
            </Tooltip>
            <Box style={{marginTop: '30px',}}>
                <SimpleSelectInput
                    formControlStyle={{width: '100%',}}
                    label={'Назначить исполнителя'}
                    options={props.executorOptions || []}
                    allowEmptyValue={true}
                    emptyValueTitle={'Не выбран'}
                    defaultValue={executorValue}
                    onChange={onExecutorChange}
                />
            </Box>
            <BasicDatePicker
                label={'Срок выполнения'}
                defaultValue={deadlineValue}
                onChange={(value) => onDeadlineChange(value)}
                minDate={new Date()}
                inputVariant={'outlined'}
                style={{width: '100%', marginTop: '30px',}}
            />
        </SimpleModal>
    );
}