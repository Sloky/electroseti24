import React, {useState} from "react";
import SimpleModal from "../SimpleModal/SimpleModal";
import SimpleTextareaInput from "../../FormInputs/SimpleTextareaInput/SimpleTextareaInput";

interface SimpleTextAreaModalProps {
    textareaLabel: string,
    textareaName?: string,
    // textareaStyle?: object
    defaultValue?: string,
    placeholder?: string,
    modalName: string
    modalTitle?: string
    startButtonTitle?: string
    startButtonClass?: string
    startButtonStyle?: object
    closeButtonTitle?: string
    acceptButtonTitle?: string
    disableAcceptButton?: boolean
    onModalSubmit(value): void
    children?: any
}

export default function SimpleTextAreaModal(props: SimpleTextAreaModalProps) {
    const [value, setValue] = useState(props.defaultValue);
    const [acceptBtnDisabled, setAcceptBtnDisabled] = useState<boolean>(true);

    const onSubmit = () => {
        props.onModalSubmit(value)
    };

    const onModalClose = () => {
        setValue(props.defaultValue);
        setAcceptBtnDisabled(true);
    };

    const onValueChange = (e) => {
        setValue(e.target.value);
        setAcceptBtnDisabled(false);
    };

    return (
        <SimpleModal
            modalName={props.modalName}
            modalTitle={props.modalTitle}
            onModalSubmit={onSubmit}
            onModalClose={onModalClose}
            disableAcceptButton={acceptBtnDisabled}
        >
            <SimpleTextareaInput
                label={props.textareaLabel}
                name={props.textareaName}
                defaultValue={value}
                placeholder={props.placeholder}
                onChange={onValueChange}
            />
        </SimpleModal>
    );
}