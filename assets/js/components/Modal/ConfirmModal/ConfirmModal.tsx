import React from "react";
import "./ConfirmModal.css";

interface ConfirmModalProps {
    modalName: string,
    modalTitle?: string,
    buttonStartTitle?: string,
    buttonStartClass?: string,
    buttonCloseTitle?: string,
    buttonAcceptTitle?: string,
    disabled?: boolean,
    onModalSubmit?: any,
    children?: any
}

const defaultProps: ConfirmModalProps = {
    modalName: 'confirmModal',
    modalTitle: 'Подтверждение',
    buttonStartTitle: 'Выполнить',
    buttonStartClass: 'btn btn-lg btn-outline-danger waves-effect waves-themed',
    buttonCloseTitle: 'Отмена',
    buttonAcceptTitle: 'Подтвердить',
    children: 'Вы подтверждаете действие?',
    disabled: false
};

export default function ConfirmModal(props: ConfirmModalProps) {

    const handleModalSubmit = (e) => {
        props.onModalSubmit(e);
    };

    return (
        <>
            <button
                type="button"
                className={props.buttonStartClass}
                data-toggle="modal"
                data-target={'#'+props.modalName}
                disabled={false}
            >
                {props.buttonStartTitle}
            </button>
            <div className="modal fade" id={props.modalName} tabIndex={-1} role="dialog"
                 aria-hidden="true" style={{display: "none"}}>
                <div className="modal-dialog modal-sm modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">{props.modalTitle}</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true"><i className="fal fa-times"></i></span>
                            </button>
                        </div>
                        <div className="modal-body">
                            {props.children}
                        </div>
                        <div className="modal-footer confirm_modal_footer">
                            <button
                                type="button"
                                className="btn btn-secondary waves-effect waves-themed"
                                data-dismiss="modal"
                            >
                                {props.buttonCloseTitle}
                            </button>
                            <button
                                type="button"
                                className="btn btn-primary waves-effect waves-themed"
                                data-dismiss={'modal'}
                                onClick={handleModalSubmit}
                            >
                                {props.buttonAcceptTitle}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

ConfirmModal.defaultProps = defaultProps;