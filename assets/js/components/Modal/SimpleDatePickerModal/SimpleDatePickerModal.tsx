import React, {useState} from "react";
import SimpleModal from "../SimpleModal/SimpleModal";
import DateFnsUtils from '@date-io/date-fns';
import ruLocale from "date-fns/locale/ru";
// import MuiPickersUtilsProvider from "@material-ui/pickers/MuiPickersUtilsProvider";
// import { DatePicker } from "@material-ui/pickers/DatePicker/DatePicker";

interface SimpleDatePickerModalProps {
    defaultValue?: string
    modalName: string
    modalTitle?: string
    startButtonTitle?: string
    startButtonClass?: string
    startButtonStyle?: object
    closeButtonTitle?: string
    acceptButtonTitle?: string
    disableAcceptButton?: boolean
    onModalSubmit(value): void
    children?: any
}

export default function SimpleDatePickerModal(props: SimpleDatePickerModalProps) {
    const initialDate = new Date(props.defaultValue);
    const [selectedDate, handleDateChange] = useState(initialDate);
    const [acceptBtnDisabled, setAcceptBtnDisabled] = useState<boolean>(true);

    const onSubmit = () => {
        props.onModalSubmit(selectedDate.toISOString());
    };

    const onChange = (value) => {
        setAcceptBtnDisabled(false);
        handleDateChange(value);
    };

    const onModalClose = () => {
        handleDateChange(initialDate);
        setAcceptBtnDisabled(true);
    };

    return (
        <SimpleModal
            modalName={props.modalName}
            modalTitle={props.modalTitle}
            onModalSubmit={onSubmit}
            onModalClose={onModalClose}
            disableAcceptButton={acceptBtnDisabled}
        >
            {/*<MuiPickersUtilsProvider*/}
            {/*    utils={DateFnsUtils}*/}
            {/*    locale={ruLocale}*/}
            {/*>*/}
            {/*    <DatePicker*/}
            {/*        autoOk*/}
            {/*        orientation="landscape"*/}
            {/*        variant="static"*/}
            {/*        openTo="date"*/}
            {/*        value={selectedDate}*/}
            {/*        onChange={onChange}*/}
            {/*    />*/}
            {/*</MuiPickersUtilsProvider>*/}
        </SimpleModal>
    );
}