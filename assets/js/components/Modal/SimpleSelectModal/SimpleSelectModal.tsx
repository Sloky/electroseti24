import React, {useEffect, useState} from "react";
import SimpleModal from "../SimpleModal/SimpleModal";
import SimpleSelectInput from "../../FormInputs/SimpleSelectInput/SimpleSelectInput";
import {TASK_TYPE} from "../../../constants/constants";

interface SimpleSelectModalProps {
    selectLabel: string,
    selectName?: string,
    selectStyle?: object
    defaultValue?: string|number,
    placeholder?: string,
    selectOptions: {
        value: string,
        title: string
    }[],
    allowEmptyValue?: boolean
    emptyValueTitle?: string
    modalName: string
    modalTitle?: string
    startButtonTitle?: string
    startButtonClass?: string
    startButtonStyle?: object
    disableStartButton?: boolean
    closeButtonTitle?: string
    acceptButtonTitle?: string
    disableAcceptButton?: boolean
    startButtonVariant?: 'contained' | 'outlined' | 'text'
    onModalSubmit(value): void
    children?: any
}

export default function SimpleSelectModal(props: SimpleSelectModalProps) {
    const [value, setValue] = useState(props.defaultValue);
    const [acceptBtnDisabled, setAcceptBtnDisabled] = useState<boolean>(true);

    const onSubmit = () => {
        props.onModalSubmit(value);
    };

    const onModalClose= () => {
        setValue(props.defaultValue);
        setAcceptBtnDisabled(true);
    };

    const onValueChange = (val) => {
        setValue(val);
        setAcceptBtnDisabled(false);
    };

    useEffect(() => {
        if (value === null) {
            setAcceptBtnDisabled(true);
        } else {
            setAcceptBtnDisabled(false);
        }
    }, [value]);

    useEffect(() => {
        setValue(props.defaultValue ?? null);
    }, [props.defaultValue]);

    return (
        <SimpleModal
            modalName={props.modalName}
            modalTitle={props.modalTitle}
            startButtonTitle={props.startButtonTitle}
            startButtonClass={props.startButtonClass}
            startButtonStyle={props.startButtonStyle}
            acceptButtonTitle={props.acceptButtonTitle}
            startButtonVariant={props.startButtonVariant}
            onModalClose={onModalClose}
            onModalSubmit={onSubmit}
            disableAcceptButton={(props.disableAcceptButton !== false) && acceptBtnDisabled}
            disableStartButton={props.disableStartButton}
        >
            <SimpleSelectInput
                style={{width: '406px',}}
                label={props.selectLabel}
                options={props.selectOptions}
                placeholder={props.placeholder}
                allowEmptyValue={props.allowEmptyValue}
                emptyValueTitle={props.emptyValueTitle}
                defaultValue={value}
                onChange={val => onValueChange(val)}
            />
        </SimpleModal>
    );
}