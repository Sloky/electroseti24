import React, {useEffect, useState} from "react";
import SimpleModal from "../SimpleModal/SimpleModal";
import {Grid, TextField} from "@material-ui/core";

interface ChangePasswordModalProps {
    onModalSubmit: any
}

const componentParameters = {
    passLabel: 'Пароль',
    invalidMessage: 'Пароли не совпадают',
    placeholder1: 'Введите пароль',
    placeholder2: 'Повторите ввод пароля'
};

export default function ChangePasswordModal(props: ChangePasswordModalProps) {
    const [pass1, setPass1] = useState<string>('');
    const [pass2, setPass2] = useState<string>('');
    const [isValid, setIsValid] = useState(true);

    useEffect(() => {
        if (pass1 === pass2) {
            setIsValid(true);
        } else {
            setIsValid(false);
        }
    }, [pass1, pass2])

    const handlePass1 = (e) => {
        setPass1(e.target.value)
    }

    const handlePass2 = (e) => {
        setPass2(e.target.value)
    }

    const changePassword = (e) => {
        if (pass1 === pass2 && pass1.trim().length > 0 && pass2.trim().length > 0) {
            setIsValid(true)
            props.onModalSubmit(e, pass2);
        } else {
            setIsValid(false)
        }
    }

    return (
        <SimpleModal
            modalName={'modalChangePassword'}
            startButtonTitle={'Изменить пароль'}
            startButtonStyle={{width: '100%', fontSize: '12px', height: '43px',}}
            modalTitle={'Изменение пароля'}
            disableAcceptButton={pass1.trim() === '' || pass2.trim() === '' || !isValid}
            onModalSubmit={changePassword}
        >
            <Grid container>
                <Grid item xs={6} style={{paddingRight: '15px',}}>
                    <TextField
                        label={'Введите новый пароль'}
                        type="password"
                        id="pass1"
                        value={pass1}
                        placeholder={componentParameters.placeholder1}
                        onChange={handlePass1}
                        size={'small'}
                        style={{width: '100%',}}
                    />
                </Grid>
                <Grid item xs={6}>
                    <TextField
                        label={'Повторите пароль'}
                        type="password"
                        id="pass2"
                        value={pass2}
                        placeholder={componentParameters.placeholder2}
                        onChange={handlePass2}
                        size={'small'}
                        style={{width: '100%',}}
                        error={!isValid}
                        helperText={!isValid ? componentParameters.invalidMessage : ''}
                    />
                </Grid>
            </Grid>
        </SimpleModal>
    );
}