import React, {useEffect, useRef, useState} from "react";
import SimpleModal from "../SimpleModal/SimpleModal";
import {Button, Typography} from "@material-ui/core";

interface SimpleLoadFileModalProps {
    modalName: string
    modalTitle?: string
    startButtonTitle?: string
    startButtonClass?: string
    startButtonStyle?: object
    closeButtonTitle?: string
    acceptButtonTitle?: string
    disableAcceptButton?: boolean
    onModalSubmit(value): void
    children?: any
    startButtonVariant?: 'contained' | 'outlined' | 'text'
}

export default function SimpleLoadFileModal(props: SimpleLoadFileModalProps) {
    const [file, setFile] = useState(null);
    const [acceptBtnDisabled, setAcceptBtnDisabled] = useState<boolean>(true);

    const fileInput = useRef(null);

    useEffect(() => {
        if (props.disableAcceptButton && file) {
            setAcceptBtnDisabled(false);
        } else {
            setAcceptBtnDisabled(true);
        }
    }, [props.disableAcceptButton, file])

    const onSubmit = () => {
        props.onModalSubmit(file)
    };

    const onModalClose = () => {
        setFile(null);
        setAcceptBtnDisabled(true);
    };

    const onValueChange = (e) => {
        setFile(e.target.files[0]);
        if (props.disableAcceptButton) {
            setAcceptBtnDisabled(false);
        }
    };

    return (
        <SimpleModal
            modalName={props.modalName}
            modalTitle={props.modalTitle}
            onModalSubmit={onSubmit}
            onModalClose={onModalClose}
            disableAcceptButton={acceptBtnDisabled}
            startButtonClass={props.startButtonClass}
            startButtonTitle={props.startButtonTitle}
            startButtonStyle={props.startButtonStyle}
            acceptButtonTitle={props.acceptButtonTitle}
            startButtonVariant={props.startButtonVariant}
        >
            {props.children}
            <input
                ref={fileInput}
                name={'bypassListForm'}
                type={'file'}
                accept={'.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel'}
                onChange={onValueChange}
                style={{display: 'none',}}
            />
            <Button
                variant='outlined'
                onClick={() => {
                    fileInput.current.click()
                }}
                className={'base_styled_btn'}
                style={{width: '100%', marginBottom: '15px',}}
            >
                Выбрать файл
            </Button>
            <Typography component="p" sx={{color: 'text.primary', fontSize: '18px',}}>
                {file !== null ? file.name : 'Файл не выбран'}
            </Typography>
        </SimpleModal>
    );
}