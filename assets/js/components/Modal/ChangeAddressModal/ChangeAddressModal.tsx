import React, {useState} from "react";
import SimpleModal from "../SimpleModal/SimpleModal";
import SimpleTextInput from "../../FormInputs/SimpleTextInput/SimpleTextInput";
import {Grid} from "@material-ui/core";
import {THEME} from "../../../theme";

interface ChangeAddressModalProps {
    defaultLocality?: string
    defaultStreet?: string
    defaultHouse?: string
    defaultPorch?: string
    defaultApartment?: string
    modalName: string
    modalTitle?: string
    startButtonTitle?: string
    startButtonClass?: string
    startButtonStyle?: object
    closeButtonTitle?: string
    acceptButtonTitle?: string
    disableAcceptButton?: boolean
    onModalSubmit(value): void
    children?: any
    fullAddress?: string
}

export default function ChangeAddressModal(props: ChangeAddressModalProps) {
    const initialAddress = {
        locality: props.defaultLocality,
        street: props.defaultStreet,
        house: props.defaultHouse,
        porch: props.defaultPorch,
        apartment: props.defaultApartment,
    };
    const [address, setAddress] = useState(initialAddress);
    const [acceptBtnDisabled, setAcceptBtnDisabled] = useState<boolean>(true);

    const onSubmit = () => {
        props.onModalSubmit(address)
    };

    const onModalClose = () => {
        setAddress(initialAddress);
        setAcceptBtnDisabled(true);
    };

    const onItemChange = (value, item: string) => {
        let newAddress = {...address};
        newAddress[item] = value;
        setAddress(newAddress);
        setAcceptBtnDisabled(false);
    };

    return (
        <SimpleModal
            modalName={props.modalName}
            modalTitle={props.modalTitle}
            onModalSubmit={onSubmit}
            onModalClose={onModalClose}
            disableAcceptButton={acceptBtnDisabled}
            startButtonStyle={props.startButtonStyle}
        >
            {props.fullAddress &&
                <Grid container style={{marginBottom: '30px',}}>
                    <Grid item xs={4} sx={{fontSize: '16px', fontWeight: '500', color: THEME.PLACEHOLDER_TEXT}}>Текущий адрес:</Grid>
                    <Grid item xs={8} sx={{fontSize: '16px',}}>{props.fullAddress}</Grid>
                </Grid>
            }
            <SimpleTextInput
                inputStyle={{marginBottom: '25px',}}
                label={'Населённый пункт'}
                name={'localityModal'}
                defaultValue={address.locality || ''}
                onBlur={() => {}}
                onChange={(value) => {onItemChange(value.target.value, 'locality')}}
            />
            <SimpleTextInput
                inputStyle={{marginBottom: '25px',}}
                label={'Улица'}
                name={'streetModal'}
                defaultValue={address.street || ''}
                onBlur={() => {}}
                onChange={(value) => {onItemChange(value.target.value, 'street')}}
            />
            <SimpleTextInput
                inputStyle={{marginBottom: '25px',}}
                label={'Дом'}
                name={'houseModal'}
                defaultValue={address.house || ''}
                onBlur={() => {}}
                onChange={(value) => {onItemChange(value.target.value, 'house')}}
            />
            <SimpleTextInput
                inputStyle={{marginBottom: '25px',}}
                label={'Подъезд'}
                name={'porchModal'}
                defaultValue={address.porch || ''}
                onBlur={() => {}}
                onChange={(value) => {onItemChange(value.target.value, 'porch')}}
            />
            <SimpleTextInput
                label={'Квартира'}
                name={'apartmentModal'}
                defaultValue={address.apartment || ''}
                onBlur={() => {}}
                onChange={(value) => {onItemChange(value.target.value, 'apartment')}}
            />
        </SimpleModal>
    );
}