import React from "react";
import MainMenu, {MainMenuProps} from "../../MainMenu/MainMenu";
import Store from "../../../store/store.";
import {useHistory, Link} from "react-router-dom";
import {isObjEmpty} from "../../../functions/functions";
import {BadgesData, UserGroupData} from "../../../interfaces/interfaces";
import {Box, CircularProgress} from "@material-ui/core";
import {THEME} from "../../../theme";
import FooterComponent from "../../FooterComponent/FooterComponent";
import {useSelector} from "react-redux";
import {appStateInterface} from "../../../store/appState";
import {
    InsertDriveFileOutlined,
    DescriptionOutlined,
    PlaylistAddCheckOutlined,
    PeopleOutline,
    AccountBoxOutlined,
    LocationCityOutlined,
    Assignment,
    PersonAddOutlined,
    GroupOutlined,
    OfflineBolt,
    PersonPinCircleOutlined, PeopleAltOutlined,
} from "@material-ui/icons";
import BreadcrumbsComponent from "../../Other/Breadcrumbs/BreadcrumbsComponent";
import TextLoader from "../../Loaders/TextLoaderComponent/TextLoader";

export interface PageLayoutProps {
    mainMenu?: MainMenuProps;
    dataLoading?: boolean
    children: any
    breadcrumbsItems?: {
        itemTitle: string
        itemScr?: string
    }[]
}

interface mainMenuInterface {
    menuTitle: string
    defaultSource: string
    roles: string[]
    sources?: RegExp[]
    icon?: any
    tooltip?: boolean
    subMenu?: {
        menuTitle: string
        defaultSource: string
        roles: string[]
        sources?: RegExp[]
        icon?: any
        badgeCount?: number
        tooltip?: boolean
    }[]
}

const defaultMainMenu = (badgesCount: BadgesData): mainMenuInterface[] => ([
    {
        menuTitle: 'Снятие показаний',
        defaultSource: '/tasks-to-work',
        roles: [],
        sources: [/\/bypass/, /\/$/, /\/tasks$/, /\/tasks-to-work/, /\/task-to-work/, /\/tasks-to-check/, /\/task-to-check/, /\/completed-task/,],
        subMenu: [
            {
                menuTitle: 'Обходной лист',
                defaultSource: '/bypass',
                roles: ['ROLE_ADMIN', 'ROLE_OPERATOR'],
                sources: [/\/bypass/],
                icon: <Assignment style={{fontSize: '30px',}}/>,
                badgeCount: badgesCount?.bypassRecordsCount || null,
            },
            {
                menuTitle: 'Задачи к выполнению',
                defaultSource: '/tasks-to-work',
                roles: [],
                sources: [/\/$/, /\/tasks$/, /\/tasks-to-work/, /\/task-to-work/,],
                icon: <InsertDriveFileOutlined style={{fontSize: '30px',}}/>,
                badgeCount: badgesCount?.tasksToWorkCount || null,
            },
            {
                menuTitle: 'Задачи к проверке',
                defaultSource: '/tasks-to-check',
                roles: [],
                sources: [/\/tasks-to-check/, /\/task-to-check/,],
                icon: <DescriptionOutlined style={{fontSize: '30px',}}/>,
                badgeCount: badgesCount?.tasksToCheckCount || null,
            },
            {
                menuTitle: 'Принятые задачи',
                defaultSource: '/completed-tasks',
                roles: [],
                sources: [/\/completed-task/,],
                icon: <PlaylistAddCheckOutlined style={{fontSize: '30px',}}/>,
                badgeCount: badgesCount?.completedTasksCount || null,
            },
        ],
    },
    // {
    //     menuTitle: 'Ведомость',
    //     defaultSource: '/task-statement',
    //     roles: [],
    //     sources: [/\/task-statement/,],
    //     subMenu: [],
    // },
    {
        menuTitle: 'Абоненты',
        defaultSource: '/subscribers/ul',
        roles: ['ROLE_ADMIN'],
        sources: [/\/subscriber/, /\/agreement/, /\/counter-/,],
        tooltip: false,
        subMenu: [
            {
                menuTitle: 'Юридические лица',
                defaultSource: '/subscribers/ul',
                roles: ['ROLE_ADMIN'],
                sources: [
                    /\/subscribers\/ul/,
                    /\/ul\//,
                    /\/ul-/,
                    /\/agreement\/counter-new\/ul/,
                ],
                icon: <PeopleAltOutlined style={{fontSize: '30px',}}/>,
            },
            {
                menuTitle: 'Физические лица',
                defaultSource: '/subscribers/fl',
                roles: ['ROLE_ADMIN'],
                sources: [
                    /\/subscribers\/fl/,
                    /\/fl\//,
                    /\/fl-/,
                    /\/agreement\/counter-new\/fl/,
                ],
                icon: <PeopleOutline style={{fontSize: '30px',}}/>,
            },
        ],
    },
    // {
    //     menuTitle: 'Аналитика',
    //     defaultSource: '/analytics',
    //     roles: ['ROLE_ADMIN'],
    //     sources: [/\/analytics/,],
    //     tooltip: true,
    //     subMenu: [
    //         {
    //             menuTitle: 'Конкуренты',
    //             defaultSource: '/analytics/competitors',
    //             roles: ['ROLE_ADMIN'],
    //             sources: [/\/analytics\/competitors/,],
    //             icon: <PeopleOutline style={{fontSize: '30px',}}/>,
    //         },
    //         {
    //             menuTitle: 'Юридические лица',
    //             defaultSource: '/analytics/ul',
    //             roles: ['ROLE_ADMIN'],
    //             sources: [/\/analytics\/ul/,],
    //             icon: <PeopleOutline style={{fontSize: '30px',}}/>,
    //         },
    //         {
    //             menuTitle: 'Физические лица',
    //             defaultSource: '/analytics/fl',
    //             roles: ['ROLE_ADMIN'],
    //             sources: [/\/analytics\/fl/,],
    //             icon: <PeopleOutline style={{fontSize: '30px',}}/>,
    //         },
    //     ],
    // },
    {
        menuTitle: 'Объекты сети',
        defaultSource: '/network',
        roles: ['ROLE_ADMIN'],
        sources: [/\/network/, /\/tp-new/, /\/tp-page/, /\/tp-edit/, /\/tp\/counter/],
        subMenu: [],
    },
    {
        menuTitle: 'Отделения',
        defaultSource: '/companies',
        roles: ['ROLE_ADMIN'],
        sources: [/\/companies/, /\/company/,],
        subMenu: [],
    },
    {
        menuTitle: 'Пользователи',
        defaultSource: '/users/controllers',
        roles: ['ROLE_ADMIN'],
        sources: [/\/user/,],
        subMenu: [
            {
                menuTitle: 'Все',
                defaultSource: '/users-all',
                roles: ['ROLE_SUPER_ADMIN'],
                sources: [/all/,],
                icon: <GroupOutlined style={{fontSize: '30px',}}/>,
            },
            {
                menuTitle: 'Руководители',
                defaultSource: '/users/admins',
                roles: ['ROLE_SUPER_ADMIN'],
                sources: [/admin/,],
                icon: <PersonPinCircleOutlined style={{fontSize: '30px',}}/>,
            },
            {
                menuTitle: 'Операторы',
                defaultSource: '/users/operators',
                roles: ['ROLE_ADMIN'],
                sources: [/operator/,],
                icon: <PeopleOutline style={{fontSize: '30px',}}/>,
            },
            {
                menuTitle: 'Контроллеры',
                defaultSource: '/users/controllers',
                roles: ['ROLE_ADMIN'],
                sources: [/controller/,],
                icon: <OfflineBolt style={{fontSize: '30px',}}/>,
            },
            {
                menuTitle: 'Создать пользователя',
                defaultSource: '/user/new',
                roles: ['ROLE_SUPER_ADMIN'],
                sources: [/\/user\/new/,],
                icon: <PersonAddOutlined style={{fontSize: '30px',}}/>,
            },
        ],
    },
    {
        menuTitle: '',
        defaultSource: '/profile',
        roles: [],
        sources: [/\/profile/,],
        icon: <AccountBoxOutlined style={{fontSize: '30px',}}/>,
        subMenu: [],
    },
])

const arrayRoleNamesHasItemOfArrayRoles = (roles: string[], userRoles: string[]) => {
    let isArraysHasCommonItems = userRoles.filter(userRole => roles.includes(userRole)).length !== 0;
    return isArraysHasCommonItems;
};

const getMenuForRole = (menu, roles: string[]) => {
    let newMenuItems = menu.filter((item) => {
        if (item.subMenu && item.subMenu.length) {
            let newSubMenu = getMenuForRole(item.subMenu, roles)
            item.subMenu = newSubMenu
        }
        return item.roles.length === 0 || arrayRoleNamesHasItemOfArrayRoles(item.roles, roles)
    });
    return newMenuItems;
};

export default function PageLayout(props: PageLayoutProps) {
    const user = Store.getState().user;
    const history = useHistory();
    const loading = useSelector((state: appStateInterface) => state.loading);
    const loadingText = useSelector((state: appStateInterface) => state.loadingText);
    const badgesCount = useSelector((state: appStateInterface) => state.badgesCount);

    if (isObjEmpty(user)) {
        history.push('/login');
    }

    const menuItems = getMenuForRole(defaultMainMenu(badgesCount), user.roles);

    return (
        <>
            <Box style={{
                position: 'fixed',
                width: '100vw',
                height: '100vh',
                justifyContent: 'center',
                alignItems: 'center',
                display: loading ? 'flex' : 'none',
                backgroundColor: THEME.LOADER_BACKGROUND,
                zIndex: 1000,
            }}>
                <CircularProgress/>
            </Box>
            {loadingText && <TextLoader/>}
            <Box style={{
                // width: '1386px',
                minHeight: '100vh',
                margin: '0 auto',
                padding: '20px 20px 90px 20px',
                display: 'flex',
                flexDirection: 'column',
                position: 'relative',
            }}>
                <MainMenu items={menuItems}>
                    <Box sx={{padding: '20px', backgroundColor: 'background.paper',}}>
                        {props.breadcrumbsItems &&
                            <BreadcrumbsComponent items={props.breadcrumbsItems}/>
                        }
                        {props.children}
                    </Box>
                </MainMenu>
                <FooterComponent
                    footerStyle={{color: THEME.PLACEHOLDER_TEXT,}}
                >
                    <Box style={{marginTop: '20px',}}>
                        2020 © Электросети24
                    </Box>
                    <Link
                        to={{pathname: 'https://cyberbelka.io/'}}
                        target='_blank'
                        style={{marginBottom: '10px', display: 'flex', justifyContent: 'center', alignItems: 'center',}}
                        className={'btn'}
                    >
                        <img
                            style={{width: '30px', height: '13px', marginRight: '10px', alignItems: 'center',}}
                            src="/img/cyberbelka.png"
                            alt="cyberbelka-logo"
                        />
                        <Box component={'span'} style={{color: THEME.PLACEHOLDER_TEXT,}}>
                            Сделано в Зеленограде
                        </Box>
                    </Link>
                </FooterComponent>
            </Box>
        </>
    );
}