import React from "react";
import {useHistory} from "react-router-dom";
import Store from "../../../store/store.";

export interface UserProfileProps {
    imgSource?: string;
    username?: string;
    userGroup?: string
}

const defaultProps: UserProfileProps = {
    imgSource: '/img/demo/avatars/avatar-admin.png',
    username: 'Иванов Иван Иванович',
    userGroup: 'Администраторы'
};

export default function UserProfile(props: UserProfileProps) {
    let history = useHistory();

    const handleLogoutClick = () => {
        Store.dispatch({type: 'CHANGE_TOKEN', payload: ''});
        Store.dispatch({type: 'CHANGE_USER_DATA', payload: {}});
        Store.dispatch({type: 'CHANGE_AUTH_STATUS', payload: false});
        localStorage.clear();
        history.push("/login")
    };

    return (
        <div className="ml-auto d-flex">
            {/*<!-- app user menu -->*/}
            <div>
                <a href="#" data-toggle="dropdown" title="drlantern@gotbootstrap.com"
                   className="header-icon d-flex align-items-center justify-content-center ml-2">
                    <span className={'fal fa-user'}/>
                    {/*<img
                        src={props.imgSource}
                        className="profile-image rounded-circle"
                        alt={props.username}
                        height={"35px"}
                        width={"35px"}
                    />*/}
                    {/*<!-- you can also add username next to the avatar with the codes below:
                                        <span class="ml-1 mr-1 text-truncate text-truncate-header hidden-xs-down">Me</span>
                                        <i class="ni ni-chevron-down hidden-xs-down"></i> -->*/}
                </a>
                <div className="dropdown-menu dropdown-menu-animated dropdown-lg">
                    <div
                        className="dropdown-header bg-trans-gradient d-flex flex-row py-4 rounded-top">
                        <div className="d-flex flex-row align-items-center mt-1 mb-1 color-white">
                                            <span className="mr-2">
                                                <img
                                                    src={props.imgSource}
                                                    className="rounded-circle profile-image"
                                                    alt={props.username}
                                                    height={"50px"}
                                                    width={"50px"}
                                                />
                                            </span>
                            <div className="info-card-text">
                                <div className="fs-lg text-truncate text-truncate-lg">
                                    {props.username}
                                </div>
                                <span className="text-truncate text-truncate-md opacity-80">
                                    Группы: {props.userGroup}
                                </span>
                            </div>
                        </div>
                    </div>

                    <div className="dropdown-divider m-0"></div>
                    <a href="#" className="dropdown-item" data-action="app-fullscreen">
                        <span data-i18n="drpdwn.fullscreen">Полный экран</span>
                        <i className="float-right text-muted fw-n">F11</i>
                    </a>

                    <div className="dropdown-divider m-0"></div>
                    <a className="dropdown-item fw-500 pt-3 pb-3" onClick={handleLogoutClick}>
                        <span data-i18n="drpdwn.page-logout">Выйти</span>
                    </a>
                </div>
            </div>
        </div>
    );
}

UserProfile.defaultProps = defaultProps;