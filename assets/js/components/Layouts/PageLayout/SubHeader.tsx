import React from "react";

export interface SubHeaderProps {
    iconClassName?: string;
    title?: string | any;
    subTitle?: string | any;
}

const defaultProps: SubHeaderProps  = {
    iconClassName: 'fal fa-info-circle',
    title: 'Заголовок страницы',
    subTitle: 'Подзаголовок'
};

export default function SubHeader(props: SubHeaderProps) {
    return (
        <div className="subheader">
            <h1 className="subheader-title">
                <i className={props.iconClassName}/> {props.title}
                <small>
                    {props.subTitle}
                </small>
            </h1>
        </div>
    );
}

SubHeader.defaultProps = defaultProps;