import React from "react";
import {BrowserRouter as Router, NavLink} from "react-router-dom"

export interface MainMenuProps {
    items?: MenuItem[];
}

interface MenuItem {
    itemClass?: itemClass
    itemIcon?: string
    itemTitle: string;
    itemSrc?: string;
    subItems?: MenuItem[];
}

enum itemClass {
    navTitle = 'nav-title',
}

const onClickItem = (e) => {
    let item = e.target.closest('li');
    let collapseSign = item.querySelectorAll('b.collapse-sign > em')[0];
    let openItemClass = 'active open';
    let closeItemClass = '';
    let signUp = 'fal fa-angle-up';
    let signDown = 'fal fa-angle-down';
    if (item.className == openItemClass) {
        item.className = closeItemClass;
        collapseSign.className =signDown;
    } else {
        item.className = openItemClass;
        collapseSign.className =signUp;
    }
};

const handleActiveLink = (e) => {
    e.stopPropagation();
};

export default function MainMenu(props: MainMenuProps) {
    return (
        <nav id="js-primary-nav" className="primary-nav js-list-filter" role="navigation">
            {Array.isArray(props.items) && props.items.length ?
                (<ul id="js-nav-menu" className="nav-menu js-nav-built">
                    {props.items.map((item, index1) => (
                        <li className="" key={item.itemTitle + index1} onClick={onClickItem}>
                            <a
                                href="#" title={item.itemTitle}
                                aria-expanded="true" className="waves-effect waves-themed"
                            >
                                <i className={item.itemIcon || ''}/>
                                <span className="nav-link-text">{item.itemTitle}</span>
                                <b className="collapse-sign"><em className="fal fa-angle-down"/></b>
                            </a>
                            {Array.isArray(item.subItems) && item.subItems.length ? (
                                <ul>
                                    {item.subItems.map((subItem, index2) => (
                                        <li key={subItem.itemTitle + index2} onClick={handleActiveLink}>
                                            {/*<a href={subItem.itemSrc} title={subItem.itemTitle}*/}
                                            {/*   className=" waves-effect waves-themed">*/}
                                            {/*            <span className="nav-link-text"*/}
                                            {/*                  data-i18n="nav.application_intel_marketing_dashboard">{subItem.itemTitle}</span>*/}
                                            {/*</a>*/}
                                            <NavLink to={subItem.itemSrc}>
                                                <span className="nav-link-text">
                                                {subItem.itemTitle}
                                                </span>
                                            </NavLink>
                                        </li>
                                    ))}
                                </ul>
                            ) : ('')}
                        </li>
                    ))}
                </ul>) : ('')
            }
        </nav>
    );
}