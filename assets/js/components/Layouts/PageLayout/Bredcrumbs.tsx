import React from "react";

export interface BreadcrumbsProps {
    items?: BreadcrumbsItem[];
}
interface BreadcrumbsItem {
    itemTitle: string | any;
    itemSrc?: string;
}

const breadcrumbsParams = {
    firstItemTitle: 'Счёт-Учёт',
    firstItemSrc: '/'
};


export default function Breadcrumbs(props: BreadcrumbsProps) {
    let date = new Date().toLocaleString('ru', {
        year: 'numeric',
        month: 'long',
        day: 'numeric'
    });

    return (
        <ol className="breadcrumb page-breadcrumb">
            <li className="breadcrumb-item">
                <a href={breadcrumbsParams.firstItemSrc}>{breadcrumbsParams.firstItemTitle}</a>
            </li>
            {Array.isArray(props.items) && props.items.length > 0 ? props.items.map((value, index) =>
                (
                    <li className="breadcrumb-item" key={index}>
                        {value.itemSrc ? (<a href={value.itemSrc} >{value.itemTitle}</a>) : value.itemTitle}
                    </li>
                )
            ) : ''}
            <li className="position-absolute  pos-right d-none d-sm-block" style={{marginRight: '12px'}}>
                <span>{date}</span>
            </li>
            {/*<li className="position-absolute pos-top pos-right d-none d-sm-block">*/}
            {/*    <span className="js-get-date"></span>*/}
            {/*</li>*/}
        </ol>
    );
}