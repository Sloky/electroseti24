import React from "react";
import './LoginLayout.css'
import {Box, CircularProgress, Link} from "@material-ui/core";
import FooterComponent from "../../FooterComponent/FooterComponent";
import {THEME} from "../../../theme";
import {useSelector} from "react-redux";
import {appStateInterface} from "../../../store/appState";

export default function LoginLayout(props) {
    const loading = useSelector((state: appStateInterface) => state.loading);

    return (
        <>
            <Box style={{
                position: 'absolute',
                width: '100vw',
                height: '100vh',
                justifyContent: 'center',
                alignItems: 'center',
                display: loading ? 'flex' : 'none',
                backgroundColor: THEME.LOADER_BACKGROUND,
                zIndex: 1000,
            }}>
                <CircularProgress/>
            </Box>
            <Box style={{
                backgroundImage: 'url("/img/login-bg.jpg")',
                backgroundSize: 'cover',
                backgroundRepeat: 'no-repeat',
                height: '100vh',
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'space-between',
            }}>
                <Box style={{height: '100%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center',}}>
                    <Box>
                        <img
                            style={{width: '275px', marginBottom: '15px',}}
                            src="/img/electroseti24-logo.svg"
                            alt="SmartAdmin WebApp"
                        />
                    </Box>
                    {props.children}
                </Box>
                <FooterComponent>
                    <Link
                        href={'http://support.electroline24.ru'}
                        style={{color: THEME.BACKGROUND_COLOR,}}
                        className={'btn footer-link'}
                        target={'_blank'}
                    >
                        Обратиться в техническую поддержку
                    </Link>
                </FooterComponent>
            </Box>
        </>
    );
}