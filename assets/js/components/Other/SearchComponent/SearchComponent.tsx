import React from "react";
import {Box, Button, Grid, IconButton, TextField} from "@material-ui/core";
import {THEME} from "../../../theme";
import {Close} from "@material-ui/icons";

interface SearchComponentProps {
    searchValue: string
    onSearchValueChange(value: string): void
    onFilterClear?(): void
    applyFilters(): void
    enableFilters?: boolean
    searchFieldXs: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11
    buttonXs: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11
}

export default function SearchComponent(props: SearchComponentProps) {
    return (
        <Grid container>
            <Grid item xs={props.searchFieldXs} style={{paddingRight: '15px',}}>
                <Box style={{position: 'relative'}}>
                    <TextField
                        label="Поиск"
                        variant="outlined"
                        placeholder="Найти"
                        size="small"
                        value={props.searchValue}
                        fullWidth
                        onChange={e => props.onSearchValueChange(e.target.value)}
                    />
                    {props.searchValue &&
                        <IconButton
                            style={{position: 'absolute', top: 0, right: 5, color: THEME.PLACEHOLDER_TEXT,}}
                            onClick={() => props.onSearchValueChange('')}
                            color='inherit'
                        >
                            <Close/>
                        </IconButton>
                    }
                </Box>
            </Grid>
            {props.enableFilters &&
                <Grid item xs={props.buttonXs} style={{paddingRight: '15px',}}>
                    <Button
                        onClick={props.onFilterClear}
                        variant="outlined"
                        className={'base_styled_btn'}
                        fullWidth
                    >
                        Сброс фильтра
                    </Button>
                </Grid>
            }
            <Grid item xs={props.buttonXs} style={{paddingRight: '15px',}}>
                <Button
                    onClick={props.applyFilters}
                    variant="outlined"
                    className={'base_styled_btn'}
                    fullWidth
                >
                    Найти
                </Button>
            </Grid>
        </Grid>
    )
}