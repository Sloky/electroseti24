import React from "react";
import "./TextDelimiter.css";

interface TextDelimiterProps {
    title?: string,
    style?: object,
}

export default function TextDelimiter(props: TextDelimiterProps) {
    return (
        <div className={'text_delimiter'} style={props.style}>{props.title}</div>
    );
}