import React from "react";
import {Breadcrumbs, Grid, Link} from "@material-ui/core";
import {NavigateNext} from "@material-ui/icons";
import {THEME} from "../../../theme";

interface BreadcrumbsComponentProps {
    items: {
        itemTitle: string
        itemSrc?: string
    }[]
}

export default function BreadcrumbsComponent(props: BreadcrumbsComponentProps) {
    return (
        <>
            {Array.isArray(props.items) && props.items.length > 0 ? (
                <Grid style={{marginBottom: '30px',}}>
                    <Breadcrumbs
                        separator={<NavigateNext fontSize="small" />}
                    >
                        {props.items.map((item, index) => (
                            <Link sx={{fontWeight: item.itemSrc ? '400' : '700', color: item.itemSrc ? THEME.PLACEHOLDER_TEXT : '',}} underline="hover" key="1" color="inherit" href={item.itemSrc}>
                                {item.itemTitle}
                            </Link>
                        ))}
                    </Breadcrumbs>
                </Grid>
            ) : null}
        </>
    )
}