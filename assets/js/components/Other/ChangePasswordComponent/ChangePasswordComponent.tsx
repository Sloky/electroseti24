import React, {useState} from "react";
import {Box, Button, Grid, TextField} from "@material-ui/core";

interface ChangePasswordComponentProps {
    onChange: any
}

export default function ChangePasswordComponent(props: ChangePasswordComponentProps) {
    const [pass1, setPass1] = useState<string>('');
    const [pass2, setPass2] = useState<string>('');
    const [isValid, setIsValid] = useState<boolean>(true);

    const handlePass1 = (e) => {
        setIsValid(true);
        setPass1(e.target.value)
    }

    const handlePass2 = (e) => {
        setIsValid(true);
        setPass2(e.target.value)
    }

    const changePassword = (e) => {
        if (pass1 === pass2 && pass1.trim().length > 0 && pass2.trim().length > 0) {
            setIsValid(true)
            props.onChange(e, pass2);
        } else {
            setIsValid(false)
        }
    }

    return (
        <Grid container style={{display: 'flex', justifyContent: 'space-between',}}>
            <Grid item xs={4} style={{paddingRight: '15px',}}>
                <TextField
                    label={'Введите пароль'}
                    type={'password'}
                    value={pass1}
                    onChange={handlePass1}
                    size={'small'}
                    style={{width: '100%',}}
                />
            </Grid>
            <Grid item xs={4} style={{paddingRight: '15px',}}>
                <TextField
                    label={'Повторите пароль'}
                    type={'password'}
                    value={pass2}
                    onChange={handlePass2}
                    size={'small'}
                    style={{width: '100%',}}
                    error={!isValid}
                    helperText={!isValid ? 'Пароли не совпадают' : ''}
                />
            </Grid>
            <Grid item xs={4}>
                <Button
                    onClick={changePassword}
                    variant={'outlined'}
                    size={'medium'}
                    style={{width: '100%', height: '42.88px'}}
                >
                    Сохранить
                </Button>
            </Grid>
        </Grid>
    )
}