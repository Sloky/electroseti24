import React, {useEffect, useState} from "react";
import {Placemark, YMaps, Map, SearchControl, TypeSelector} from "react-yandex-maps";
import {Box} from "@material-ui/core";

interface MapComponentProps {
    coordX?: number
    coordY?: number
    draggable?: boolean
    mapWidth?: string
    mapHeight?: string
    enableSearch?: boolean
}

const defaultCoords = {
    x: 42.974386318699494,
    y: 47.49688625008533
};

export default function MapComponent(props: MapComponentProps) {
    const initialCoordinates = {
        x: props.coordX || defaultCoords.x,
        y: props.coordY || defaultCoords.y
    };

    const [coordinates, setCoordinates] = useState(initialCoordinates);

    const onMapClick = (e: any) => {
        if (props.enableSearch) {
            let coords = e.get('coords');
            setCoordinates({
                x: coords[0],
                y: coords[1]
            });
        }
    };

    const dragHandler = (e: any) => {
        let coords = e.originalEvent.target.geometry.getCoordinates();
        setCoordinates({
            x: coords[0],
            y: coords[1]
        });
    };

    useEffect(() => {
        setCoordinates({x: props.coordX, y: props.coordY});
    }, [props]);

    return (
        <YMaps
            query={{
                apikey: 'eff48c1b-de45-4d2f-a5fe-75870e7978e7'
            }}
        >
            <Box>
                <Map state={{ center: [coordinates.x, coordinates.y], zoom: 18, type: 'yandex#hybrid'}} onClick={onMapClick} width={props.mapWidth || '100%'} height={props.mapHeight || '60vh'}>
                    <Placemark
                        geometry={[coordinates.x, coordinates.y]}
                        options={{
                            draggable: props.draggable,
                            iconColor: "red",
                            preset: 'islands#redCircleDotIcon'
                        }}
                        onDragend={dragHandler}
                    />
                    <TypeSelector options={{ float: 'right' }} mapTypes={['yandex#hybrid', 'yandex#map', 'yandex#satellite']}/>
                    {props.enableSearch ?
                        <SearchControl
                            options={{ float: 'right' }}
                        />
                        : null}
                </Map>
            </Box>
        </YMaps>
    );
}