import React from "react";
import SimpleModal from "../../Modal/SimpleModal/SimpleModal";
import {ArrowBackIos} from "@material-ui/icons";
import {Button} from "@material-ui/core";
import {useHistory} from "react-router";

interface BackButtonComponentProps {
    changed: boolean
}

export default function BackButtonComponent(props: BackButtonComponentProps) {
    const history = useHistory();

    return (
        <>
            {props.changed ?
                <SimpleModal
                    startButtonTitle={'Назад'}
                    modalName={'goBackModal'}
                    startIcon={<ArrowBackIos fontSizeAdjust={'20'}/>}
                    startButtonVariant={'text'}
                    closeButtonTitle={'Отменить'}
                    modalTitle={'Переход на другой экран'}
                    acceptButtonTitle={'Подтвердить'}
                    onModalSubmit={() => history.goBack()}
                    startButtonStyle={{fontSize: '12px', fontWeight: '500', width: '100px', height: '43px', padding: '6px 8px 6px 8px',}}
                >
                    Все несохраненные данные будут утеряны. Продолжить?
                </SimpleModal> :
                <Button
                    onClick={() => history.goBack()}
                    startIcon={<ArrowBackIos fontSizeAdjust={'20'}/>}
                    sx={{fontSize: '12px', fontWeight: '500', width: '100px', height: '43px', padding: '6px 8px 6px 8px',}}
                    size="small"
                >
                    Назад
                </Button>
            }
        </>
    )
}