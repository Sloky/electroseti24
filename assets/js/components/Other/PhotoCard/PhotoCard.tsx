import React, {useState} from "react";
import ReactDOM from "react-dom";
import './PhotoCard.css';
import Lightbox from "react-awesome-lightbox";
import "react-awesome-lightbox/build/style.css";
import {Box} from "@material-ui/core";

interface PhotoCardProps {
    uri: string
}

interface BigModalPhotoProps {
    uri: string
    showBigPhoto: boolean
    onBackdropClick(): void
}

const appRoot = document.getElementById('root');

function BigModalPhoto(props: BigModalPhotoProps) {

    const onBackdropClick = () => {
        props.onBackdropClick();
    }

    return ReactDOM.createPortal(
        <Box
            style={{display: (props.showBigPhoto ? 'flex' : 'none')}}
        >
            <Lightbox
                image={props.uri}
                title={"Image Title"}
                onClose={onBackdropClick}
            />
        </Box>,
        appRoot
    );
}



export default function PhotoCard(props: PhotoCardProps) {
    const [showBigPhoto, setShowBigPhoto] = useState<boolean>(false);

    const onPhotoClick = () => {
        setShowBigPhoto(true);
    };

    const onCloseLightbox = () => {
        setShowBigPhoto(false);
    };

    return (
        <>
            <Box className={'small_photo_card'}>
                <img
                    onClick={onPhotoClick}
                    src={props.uri}
                />
            </Box>
            {showBigPhoto &&
                <BigModalPhoto uri={props.uri} showBigPhoto={showBigPhoto} onBackdropClick={onCloseLightbox}/>
            }
        </>
    );
}