import React, {useEffect, useState} from "react";
import {Grid, IconButton} from "@material-ui/core";
import SimpleTextInput from "../../FormInputs/SimpleTextInput/SimpleTextInput";
import {AddBoxOutlined} from "@material-ui/icons";

interface PhoneComponentProps {
    label: string
    phones: string[]
    onChange?(data: string[]): void
    componentXs?: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12
    lastElemMargin?: boolean
}

export default function PhoneComponent(props: PhoneComponentProps) {
    const [phones, setPhones] = useState(props.phones.length ? props.phones : ['']);

    useEffect(() => {
        setPhones(props.phones.length ? props.phones : ['']);
    }, [props]);

    return (
        <>
            {phones && phones.map((phone, index) => (
                <Grid container key={index} style={{marginBottom: props.lastElemMargin ? '30px' : phones.length - 1 !== index ? '30px' : '0px',}}>
                    <Grid item xs={props.componentXs ||  4} style={{paddingRight: '15px',}}>
                        <SimpleTextInput
                            name={'phones'}
                            label={`${props.label} ${index + 1}`}
                            defaultValue={phone}
                            onBlur={(value) => {
                                if (value.trim() === '' && phones.length > 1) {
                                    setPhones(prevState => {
                                        prevState.splice(index, 1)
                                        return prevState;
                                    });
                                } else {
                                    setPhones(prevState => prevState.map((item, i) => i === index ? value : item))
                                }
                                props.onChange(phones);
                            }}
                            onChange={(e) => {
                                setPhones(prevState => prevState.map((item, i) => i === index ? e.target.value : item));
                            }}
                            id={'phones'}
                        />
                    </Grid>
                    {index === phones.length - 1 &&
                        <Grid item xs={1}>
                            <IconButton
                                color={phones[phones.length - 1].trim() !== '' ? 'primary' : 'secondary'}
                                component={'span'}
                                onClick={(e) => {
                                    e.preventDefault();
                                    if (phones[phones.length - 1].trim() !== '') {
                                        setPhones(prevState => [...prevState, '']);
                                    }
                                }}
                            >
                                <AddBoxOutlined/>
                            </IconButton>
                        </Grid>
                    }
                </Grid>
            ))}
        </>
    )
}