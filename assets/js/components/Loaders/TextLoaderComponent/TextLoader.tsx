import React, {useEffect, useState} from "react";
import {Box, Card, CircularProgress, Typography} from "@material-ui/core";
import {THEME} from "../../../theme";

const loadingTextArray = [
    'Пожалуйста подождите',
    'Выполняется загрузка',
    'Идет обработка данных',
];

export default function TextLoader() {
    const [loadingText, setLoadingText] = useState<string>(loadingTextArray[Math.floor(Math.random() * loadingTextArray.length)]);

    useEffect(() => {
        const interval = setInterval(() => {
            setLoadingText(loadingTextArray[Math.floor(Math.random() * loadingTextArray.length)]);
        }, 3000);
        return () => {
            return clearInterval(interval)
        }
    }, []);

    return (
        <Box style={{
            position: 'fixed',
            width: '100vw',
            height: '100vh',
            justifyContent: 'center',
            alignItems: 'center',
            display: 'flex',
            backgroundColor: THEME.LOADER_BACKGROUND,
            zIndex: 1000,
        }}>
            <Card style={{display: 'flex', width: '350px', padding: '20px', borderRadius: '10px', alignItems: 'center',}}>
                <Box style={{marginRight: '15px',}}>
                    <CircularProgress/>
                </Box>
                <Typography style={{backgroundColor: '#ffffff',}}>
                    {loadingText}
                </Typography>
            </Card>
        </Box>
    )
}