import React from "react";
import "./CardSpinner.css";

interface CardSpinnerProps {
    show?: boolean,
    width?: string,
    height?: string,
    text?: string
}

const defaultProps: CardSpinnerProps = {
    show: true,
    text: 'Идет загрузка данных ...',
};

export default function CardSpinner(props: CardSpinnerProps) {
    return (
        <div
            className={'card_spinner'}
            style={{
                display: props.show ? "flex" : "none",
                width: props.width || "",
                height: props.height || "",
            }}
        >
            <h5>{props.text}</h5>
            <div
                className="spinner-border text-primary"
                style={{display: 'flex', width: '3rem', height: '3rem'}}
                role="status"
            >
                <span className="sr-only">Loading...</span>
            </div>
        </div>
    )
}

CardSpinner.defaultProps = defaultProps;