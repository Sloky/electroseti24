import React from "react";
import "./OverlaySpinner.css"
import {Box, CircularProgress} from "@material-ui/core";

interface OverlaySpinnerProps {
    show?: boolean
}

const defaultProps: OverlaySpinnerProps = {
    show: false,
};

export default function OverlaySpinner(props: OverlaySpinnerProps) {
    return (
        <Box style={{
            position: 'absolute',
            display: props.show ? 'flex' : 'none',
            justifyContent: 'center',
            margin: '0 auto',
            alignItems: 'center',
            height: '500px',
            width: '90%',
        }}>
            <CircularProgress/>
        </Box>

        // <div className={'overlay_spinner'} style={{display: props.show ? "flex" : "none"}}>
        //     <div className="spinner-border text-primary" style={{width: '5rem', height: '5rem'}} role="status">
        //         <span className="sr-only">Loading...</span>
        //     </div>
        // </div>
    );
}

OverlaySpinner.defaultProps = defaultProps;