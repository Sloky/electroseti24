import React from "react";
import {Alert, AlertTitle, Box, Collapse, IconButton} from "@material-ui/core";
import {Close} from "@material-ui/icons";

interface FlashComponentProps {
    showFlash?: boolean
    onCloseFlash?(): void
    flashType?: "success" | "info" | "warning" | "error"
    flashWidth?: string
    messageHeading?: string
    message?: string
    messages?: string[]
}

const FlashComponent = (props: FlashComponentProps) => {
    return (
        <Box sx={{width: props.flashWidth ?? '50%'}} style={props.showFlash ? {marginBottom: '30px',} : {}}>
            <Collapse in={props.showFlash}>
                <Alert
                    icon={<></>}
                    variant='outlined'
                    severity={props.flashType ?? 'warning'}
                    sx={props.flashType !== 'info' ? {borderColor: `${props.flashType}.main`} : {}}
                    style={{borderRadius: '5px',}}
                    action={
                        props.onCloseFlash
                            ? <IconButton
                                color="inherit"
                                size="small"
                                onClick={() => {
                                    props.onCloseFlash();
                                }}
                            >
                                <Close/>
                            </IconButton>
                            : null
                    }
                >
                    <AlertTitle>{props.messageHeading}</AlertTitle>
                    {props.message}
                    {props.messages &&
                    <>
                        {props.messages.map((mes, index) => (
                                <Box key={index}>{mes+';'}<br/></Box>
                            )
                        )}
                    </>
                    }
                </Alert>
            </Collapse>
        </Box>
    );
}

export default FlashComponent;