import React from "react";

interface InfoFlashProps {
    message: string,
    messageHeading?: string
    onCloseInfoFlash?(): void
}

export default function InfoFlash(props: InfoFlashProps) {
    return (

        <div className="alert alert-primary" role="alert" >
            <button type="button" className="close" onClick={() => props.onCloseInfoFlash()}>
                <span aria-hidden="true">×</span>
            </button>
            {props.messageHeading ? (<strong>{props.messageHeading}</strong>) : ''}&nbsp;{props.message}
        </div>
    );
}