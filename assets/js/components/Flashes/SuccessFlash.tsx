import React from "react";

interface SuccessFlashProps {
    message: string,
    messageHeading?: string
    onCloseSuccessFlash?(): void
}

export default function SuccessFlash(props: SuccessFlashProps) {
    return (

        <div className="alert alert-success" role="alert" style={{backgroundColor: "#f7fdfc"}}>
            <button type="button" className="close" onClick={() => props.onCloseSuccessFlash()}>
                <span aria-hidden="true">×</span>
            </button>
            {props.messageHeading ? (<strong>{props.messageHeading}</strong>) : ''}&nbsp;{props.message}
        </div>
    );
}