import React from "react";

interface ErrorFlashProps {
    message?: string,
    messages?: string[],
    messageHeading?: string
    onCloseErrorFlash?(): void
}

export default function ErrorFlash(props: ErrorFlashProps) {
    return (
        <div className="alert alert-danger" role="alert" style={{backgroundColor: "#ffe5f1"}}>
            <button type="button" className="close" onClick={() => props.onCloseErrorFlash()}>
                <span aria-hidden="true">×</span>
            </button>
            {props.messageHeading ? (<strong>{props.messageHeading}&nbsp;</strong>) : ''}
            {props.message}
            {props.messages &&
                <>
                    {props.messages.map((mes) => (
                            <>{mes+';'}<br/></>
                        )
                    )}
                </>

            }
        </div>
    );
}