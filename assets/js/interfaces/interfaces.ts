export interface IDictionary<TValue> {
    [id: string] : TValue
}

export interface UserData {
    id: string,
    address: string,
    companies: CompanyData[],
    groups: UserGroupData[],
    name: string,
    patronymic: string,
    phone: string,
    roles: string[],
    surname: string,
    username: string,
    work_status: number,
    password?: string,
    task_to_work_count: number,
    company_task_to_check_count: number,
}

export interface UserDataForSending {
    id: string,
    address: string,
    companies: string[],
    groups: string[],
    name: string,
    patronymic: string,
    phone: string,
    roles: string[],
    surname: string,
    username: string,
    work_status: number
}

export interface UserGroupData {
    id: string,
    description?: string,
    group_name?: string
    roles?: string[]
}

export interface SimpleFlashData {
    status: boolean,
    message?: string,
    messageHeading?: string
}

export interface CompanyData {
    id?: string,
    parent?: CompanyData|null
    children?: CompanyData[]|[]
    title: string
    phone: string
    address: string
    branch_code: string
}

interface FeederData {
    id: string
    title: string
    substation: SubstationData,
    transformer: TransformerData
}

interface SubstationData {
    id: string
    title: string
}

interface TransformerData {
    id: string
    title: string
    feeder: FeederData
}

export interface CounterTypeData {
    "id"?: string
    "title": string
    "electron_current_type": number
    "number_phases": number
    "number_tariffs": number|null
    "tariff_type": string
    "operating_mechanism": number|null
    "class_accuracy": number
    "indicator_device": number|null
    "capacity": number|null
}

interface FailureFactorData {
    id: string
    title: string
    message: string
}

export interface PhotoData {
    id: string
    file_name: string
}

export interface RecordData {
    id: string
    address: string
    agreement_number: string
    antimagnetic_seal: string
    company_title: string
    controller: string
    coords: string
    counter_initial_date: string
    counter_initial_value: string
    counter_model: string
    counter_number: string
    current_counter_value: string
    current_counter_value_date: string
    deadline: string
    electric_line: string
    electric_pole: string
    feeder: string
    file_name: string
    lodgers_count: string
    period: string
    phones: string
    rooms_count: string
    row_position: number
    serviceability: string
    side_seal: string
    status: number
    subscriber_title: string
    subscriber_type: string
    substation: string
    terminal_seal: string
    transformer: string
    errors: ErrorData[]
}

export interface TaskData {
    id: string
    task_type: number
    executor: UserData|null
    priority: number
    deadline: string
    status: number
    coordinate_x: number|null
    coordinate_y: number|null
    operator_comment: string|null
    controller_comment: string|null
    failure_factors: FailureFactorData[]
    actualized_fields: string[]
    changed_fields: string[]
    company: CompanyData
    locality: string|null
    street: string|null
    house_number: string|null
    porch_number: string|null
    apartment_number: string|null
    lodgers_count: number|null
    rooms_count: number|null
    subscriber_type: number
    subscriber_title: string
    subscriber_name: string|null
    subscriber_surname: string|null
    subscriber_patronymic: string|null
    subscriber_phones: string[]
    agreement_number: string
    counter_number: string
    last_counter_value: number[]
    current_counter_value: number[]|null
    consumption: number|null
    consumption_coefficient: number
    counter_physical_status: number
    substation: SubstationData
    feeder: FeederData
    transformer: TransformerData
    line_number: string
    electric_pole_number: string
    counter_type: CounterTypeData
    terminal_seal: string|null
    anti_magnetic_seal: string|null
    side_seal: string|null
    temporary_address: string|null
    temporary_counter_type: string|null
    temporary_substation: string|null
    temporary_transformer: string|null
    temporary_feeder: string|null
    period: string
    // photos: string[]
}

export interface TaskToCheckData extends TaskData {
    executed_date: string
    changed_counter_number?: string
    changed_counter_type?: CounterTypeData
    changed_counter_type_string?: string
    changed_terminal_seal?: string
    changed_antimagnetic_seal?: string
    changed_side_seal?: string
    changed_lodgers_count?: number
    changed_rooms_count?: number
    new_coordinate_x?: number
    new_coordinate_y?: number
    photos: PhotoData[]
    photos_public_url: string
}

export interface CompleteTaskData extends TaskToCheckData {
    acceptance_date: string
    operator: UserData
}

export interface BypassRecordsCollection {
    [id: string]: RecordData
}

export interface TasksToWorkCollection {
    [id: string]: TaskData
}

export interface TasksToCheckCollection {
    [id: string]: TaskToCheckData
}

export interface CompletedTasksCollection {
    [id: string]: CompleteTaskData
}

export interface CompanyBranchesCollection {
    [id: string]: CompanyData
}

export interface UsersCollection {
    [id: string]: UserData
}

export interface UserGroupsCollection {
    [id: string]: UserGroupData
}

export interface CounterTypesCollection {
    [id: string]: CounterTypeData
}

export interface SubstationsCollection {
    [id: string]: SubstationData
}

export interface TransformersCollection {
    [id: string]: TransformerData
}

export interface FeedersCollection {
    [id: string]: FeederData
}

export interface FailureFactorsCollection {
    [id: string]: FailureFactorData
}

export interface RecordTableData {
    id: string
    address: string
    agreementNumber: string
    companyTitle: string
    subscriberTitle: string
    subscriberType: number|string
    counterNumber: string
    userFullName: string|null
    trView?: string
    deadline: string
    errors: ErrorData[]
}

export interface TaskTableData {
    id: string
    address: string
    userFullName: string|null
    userId: string|null
    subscriberTitle: string
    subscriberType: number
    agreementNumber: string
    deadline: string
    executed_date?: string
    counterNumber: string
    companyTitle: string
    companyId: string
    acceptanceDate?: string
    operatorFullName?: string
    operatorId?: string|null
    trView?: string
    period: string
}
export interface BypassRecordData {
    subscriber_type?: string
    subscriber_title?: string
    agreement_number?: string
    address?: string
    counter_number?: string
    company_title?: string
    controller?: string
    coords?: string
    deadline?: string
    serviceability?: string
    counter_model?: string
    counter_initial_value?: string
    counter_initial_date?: string
    current_counter_value?: string
    current_counter_value_date?: string
    terminal_seal?: string
    antimagnetic_seal?: string
    side_seal?: string
    substation?: string
    feeder?: string
    id?: string
    transformer?: string
    electric_line?: string
    electric_pole?: string
    lodgers_count?: string | number
    rooms_count?: string | number
    phones?: string
}

export interface BypassRecordPostData {
    subscriberType?: string
    subscriberTitle?: string
    agreementNumber?: string
    address?: string
    counterNumber?: string
    companyTitle?: string
    controller?: string
    coords?: string
    serviceability?: string
    counterModel?: string
    counterInitialValue?: string
    counterInitialDate?: string
    currentCounterValue?: string
    currentCounterValueDate?: string
    terminalSeal?: string
    antimagneticSeal?: string
    sideSeal?: string
    substation?: string
    feeder?: string
    id?: string
    transformer?: string
    electricLine?: string
    electricPole?: string
    lodgersCount?: string | number
    roomsCount?: string | number
    phones?: string
}

export interface TaskToWorkPostData {
    taskType?: number
    deadline?: string
    executor?: string
    priority?: number
    status?: number
    coordinateX?: number
    coordinateY?: number
    operatorComment?: string
    locality?: string
    street?: string
    houseNumber?: string
    porchNumber?: string
    apartmentNumber?: string
    lodgersCount?: number
    roomsCount?: number
}

export interface TaskToCheckPostData {
    counterPhysicalStatus?: number
    newCounterValue?: number[]
    newCounterNumber?: string
    newCounterType?: string
    newTerminalSeal?: string
    newAntimagneticSeal?: string
    newSideSeal?: string
    locality?: string
    street?: string
    houseNumber?: string
    porchNumber?: string
    apartmentNumber?: string
    lodgersCount?: number
    roomsCount?: number
    operatorComment?: string
}

export interface AddressCompilation {
    locality: string
    street: string
    house: string
    porch: string
    apartment: string
}

export interface CompanyBranch {
    id: string
    parent: CompanyBranch
    title: string
}

export interface TaskToCheckEditForm {
    counterPhysicalStatus?: number
    newCounterValue?: number[]
    newCounterNumber?: string
    newCounterType?: string
    newTerminalSeal?: string
    newAntimagneticSeal?: string
    newSideSeal?: string
    lodgersCount?: number
    roomsCount?: number
}

export interface TaskWarning {
    type: "success" | "info" | "warning" | "error"
    title: string
    message: string
    status: boolean
}

export interface RecordTableRequestData {
    page?: number
    searchField?: string
    maxResult?: number
    search?: string
    companyTitle?: string
    status?: string
    executor?: string
    subscriberType?: string | number
    period?: string
    items?: string[]
}

export interface TaskTableRequestData {
    page?: number
    searchField?: string
    maxResult?: number
    search?: string
    company?: string
    status?: string
    user?: string
    subscriberType?: string | number
    period?: string
}

export interface UserTableRequestData {
    page?: number
    searchField?: string
    maxResult?: number
    search?: string
    company?: string
    group?: string
    status?: string
}

export interface ExcelAndPhotosUploadRequestData {
    search?: string
    searchField?: string
    company?: string
    user?: string
    subscriberType?: string | number
    period?: string
    page?: number
    maxResult?: number
}

export interface TaskElementInterface {
    title: string
    value: any
    modal: any|null
    show?: boolean
    taskBackgroundColor?: 'danger' | 'warning' | 'info'
    taskTextColor?: 'danger' | 'warning' | 'info'
    editable?: boolean
}

export interface ErrorData {
    title: string
    message: string
}

export interface BadgesData {
    tasksToWorkCount: number
    tasksToCheckCount: number
    completedTasksCount: number
    bypassRecordsCount: number
}

export interface FiltersData {
    companyFilter?: string | null
    executorFilter?: string | null
    subscriberTypeFilter?: string | null
    statusFilter?: string | null
    searchValue: string
    filtersLocation?: string | null
    searchField?: string | null
    periodFilter?: string | null
}

export interface ProfileProgressData {
    averageMonthlyExecutionRatio: number
    delta: number
    statistic: any[]
    user: UserData
}

export interface TransformerPostData {
    id?: string
    company?: CompanyData
    title?: string
    feeder?: FeederData
    users?: UserData[]
    coords?: string[]
}

export interface CounterPostData {
    agreement_number?: string
    antimagnetic_seal?: string
    coordinates?: string[]
    counter_id?: string
    counter_model?: string
    counter_physical_status?: number
    counter_title?: string
    initial_counter_value?: number
    initial_counter_value_date?: string
    initial_counter_value_id?: string
    last_counter_value?: number
    last_counter_value_date?: string
    last_counter_value_id?: string
    lodgers_count?: string | number
    phones?: string[]
    rooms_count?: string | number
    side_seal?: string
    subscriber_title?: string
    terminal_seal?: string
    transformer?: TransformerPostData
}

export interface CompanyTransformersData {
    id: string
    title: string
    counters_count: number
    users: UserData[]
    company: CompanyData
}

export interface TransformerCounterTableRequestData {
    page?: number
    maxResult?: number
    search?: string
}

export interface CompanyTransformerTableRequestData {
    page?: number
    maxResult?: number
    search?: string
    executor?: string | null
    company?: string | null
}

export interface SubscriberTableRequestData {
    page?: number
    maxResult?: number
    search?: string
}

export interface AgreementTableRequestData {
    page?: number
    maxResult?: number
    search?: string
}

export interface CounterTableRequestData {
    page?: number
    maxResult?: number
    search?: string
}

export interface SubscriberPostData {
    address?: string
    agreements?: SubscriberAgreementData[]
    coordinates?: number[]
    email?: string
    id?: string
    name?: string
    patronymic?: string
    phones?: string[]
    subscriber_type?: number
    subscriber_type_value?: string[]
    surname?: string
    title?: string
    website?: string
    _tin?: string
}

export interface SubscriberAgreementData {
    company?: CompanyData
    counters?: AgreementCounterData[]
    id?: string
    number?: string
    payment_number?: string
    subscriber?: SubscriberPostData
}

export interface AgreementCounterTableData {
    address?: string
    agreement?: SubscriberAgreementData
    anti_magnetic_seal?: string
    coordinates?: number[]
    counter_type?: string
    dismantling_date?: string
    electric_line?: string
    electric_pole_number?: string
    final_value?: number
    id?: string
    initial_value?: any
    install_date?: string
    is_installed_status?: number
    side_seal?: string
    temporary_address?: string
    temporary_counter_type?: string
    temporary_feeder?: string
    temporary_lodgers_count?: number
    temporary_rooms_count?: number
    temporary_substation?: string
    temporary_transformer?: string
    terminal_seal?: string
    title?: string
    transformer?: TransformerPostData
    work_status?: number
}

export interface AgreementCounterData {
    address?: string
    antimagnetic_seal?: string
    coordinate_x?: number
    coordinate_y?: number
    counter_id?: string
    counter_model?: string
    counter_physical_status?: number
    counter_title?: string
    electric_line?: string
    electric_pole?: string
    initial_counter_value?: number
    initial_counter_value_date?: string
    initial_counter_value_id?: string
    last_counter_value?: number
    last_counter_value_date?: string
    last_counter_value_id?: string
    lodgers_count?: number
    rooms_count?: number
    side_seal?: string
    terminal_seal?: string
    transformer?: TransformerPostData
    subscriber?: SubscriberPostData
}

export interface CounterValuesData {
    counter?: any
    date?: string
    id?: string
    inspector?: any
    value?: {value?: string, values_sum?: string}
}

export interface TransformersData {
    company: CompanyData
    coordinates: number[]
    counters_count: number
    feeder_title: string
    id: string
    substation_title: string
    title: string
    users?: UserData[]
}