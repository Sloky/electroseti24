import Store from "../store/store.";
import axios from "axios";
import {
    TaskToCheckPostData,
    TaskToWorkPostData,
    UserDataForSending,
    TaskTableRequestData,
    ExcelAndPhotosUploadRequestData,
    UserTableRequestData,
    BypassRecordPostData,
    RecordTableRequestData,
    CounterPostData,
    SubscriberTableRequestData,
    CounterTableRequestData,
    CompanyTransformerTableRequestData,
} from "../interfaces/interfaces";

function clearStore() {
    Store.dispatch({type: 'APP_CLEAR_STATE'});
    localStorage.clear();
    sessionStorage.clear();
}

export async function getUsersRequest(data: UserTableRequestData) {
    let authToken = Store.getState().token;
    let request = axios.get(
        '/api/user',
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            },
            params: data,
        },
    ).then(
        users => {
            return {
                type: 'success',
                data: users.data,
            };
        }
    ).catch(
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function getUserRequest(id: string) {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/user/${id}`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        user => {
            return {
                type: 'success',
                data: user.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function editUserRequest(id: string, userData: UserDataForSending) {
    let authToken = Store.getState().token;
    let request = axios.post(
        `/api/user/${id}/edit`,
        userData,
 {
            headers: {
                'Authorization': "Bearer "+authToken,
            }
        },
    ).then(function (data) {
        return {
            type: 'success',
            data: data.data,
        };
      }
    ).catch(function (error) {
        if (error.response.data.code == 401) {
            clearStore();
        }
        return {
            type: 'error',
            data: error.response.data
        };
      }
    );
    return await request;
}

export async function getUserGroupsRequest() {
    let authToken = Store.getState().token;
    let request = axios.get(
        '/api/group',
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        groups => {
            return {
                type: 'success',
                data: groups.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function deleteUserRequest(userId: string) {
    let authToken = Store.getState().token;
    let request = axios.delete(
        `/api/user/${userId}`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        response => {
            return {
                type: 'success',
                data: response.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function getAllRolesRequest() {
    let authToken = Store.getState().token;
    let request = axios.get(
        '/api/group/roles/all',
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        roles => {
            return {
                type: 'success',
                data: roles.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function getNewUserCardRequest() {
    let authToken = Store.getState().token;
    let request =
        axios.all([
            axios.get(
                '/api/group',
                {
                    headers: {
                        'Authorization': "Bearer " + authToken,
                    }
                }
            ),
            axios.get(
                '/api/companies',
                {
                    headers: {
                        'Authorization': "Bearer " + authToken,
                    }
                }
            ),
        ]).then(
            result => {
                return {
                    type: 'success',
                    data: {
                        groups: result[0].data,
                        companies: result[1].data
                    }
                };
            },
            error => {
                if (error.response.data.code == 401) {
                    clearStore();
                }
                return {
                    type: 'error',
                    data: error.response.data,
                };
            }
        );
    return await request;
}

export async function addNewUserRequest(userData) {
    let authToken = Store.getState().token;
    let request = axios.post(
        `/api/user/new`,
        userData,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        result => {
            return {
                type: 'success',
                data: result.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function addNewGroupRequest(groupData) {
    let authToken = Store.getState().token;
    let request = axios.post(
        `/api/group/new`,
        groupData,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        result => {
            return {
                type: 'success',
                data: result.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function editGroupRequest(groupId: string, groupData) {
    let authToken = Store.getState().token;
    let request = axios.post(
        `/api/group/${groupId}/edit`,
        groupData,
        {
            headers: {
                'Authorization': "Bearer "+authToken,
            }
        },
    ).then(function (data) {
            return {
                type: 'success',
                data: data.data,
            };
        }
    ).catch(function (error) {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data
            };
        }
    );
    return await request;
}

export async function getGroupRequest(id: string) {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/group/${id}`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        group => {
            return {
                type: 'success',
                data: group.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function deleteGroupRequest(id: string) {
    let authToken = Store.getState().token;
    let request = axios.delete(
        `/api/group/${id}`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        response => {
            return {
                type: 'success',
                data: response.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function getTasksToWorkRequest(data: TaskTableRequestData) {
    let authToken = Store.getState().token;
    let request = axios.get(
        '/api/task-to-work-list',
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            },
            params: data,
        },
    ).then(
        tasks => {
            return {
                type: 'success',
                code: 200,
                data: tasks.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                code: error.response.status,
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function deleteTasksToWorkRequest(data: TaskTableRequestData) {
    let authToken = Store.getState().token;
    let request = axios.delete(
        `/api/tasks-to-work-delete`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            },
            params: data
        },
    ).then(
        response => {
            return {
                type: 'success',
                data: response.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function deleteTaskToWorkRequest(id: string) {
    let authToken = Store.getState().token;
    let request = axios.delete(
        `/api/task/delete/${id}`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            },
        },
    ).then(
        response => {
            return {
                type: 'success',
                data: response.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function getTasksToCheckRequest(data: TaskTableRequestData) {
    let authToken = Store.getState().token;
    let request = axios.get(
        '/api/task-to-check-list',
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            },
            params: data,
        },
    ).then(
        tasks => {
            return {
                type: 'success',
                code: 200,
                data: tasks.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                code: error.response.status,
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function getCompletedTasksRequest(data: TaskTableRequestData) {
    let authToken = Store.getState().token;
    let request = axios.get(
        '/api/completed-task-list',
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            },
            params: data,
        },
    ).then(
        tasks => {
            return {
                type: 'success',
                code: 200,
                data: tasks.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                code: error.response.status,
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function getPeriodsRequest(status: string) {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/tasks-periods/${status}`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        response => {
            return {
                type: 'success',
                code: 200,
                data: response.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                code: error.response.status,
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function getTaskRequest(taskId: string) {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/task/${taskId}`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        task => {
            return {
                type: 'success',
                data: task.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function getUserEditCardDataRequest(userId: string) {
    let authToken = Store.getState().token;
    let request =
        axios.all([
            axios.get(
                `/api/user/${userId}`,
                {
                    headers: {
                        'Authorization': "Bearer " + authToken,
                    }
                }
            ),
            axios.get(
                '/api/group',
                {
                    headers: {
                        'Authorization': "Bearer " + authToken,
                    }
                }
            ),
            axios.get(
                '/api/companies',
                {
                    headers: {
                        'Authorization': "Bearer " + authToken,
                    }
                }
            ),
        ]).then(
            result => {
                return {
                    type: 'success',
                    data: {
                        user: result[0].data,
                        groups: result[1].data,
                        companies: result[2].data
                    }
                };
            },
            error => {
                if (error.response.data.code == 401) {
                    clearStore();
                }
                return {
                    type: 'error',
                    data: error.response.data,
                };
            }
        );
    return await request;
}

export async function getControllerAndBranchesRequest() {
    let authToken = Store.getState().token;
    let request =
        axios.all([
            axios.get(
                '/api/users/Controllers',
                {
                    headers: {
                        'Authorization': "Bearer " + authToken,
                    }
                }
            ),
            axios.get(
                '/api/companies',
                {
                    headers: {
                        'Authorization': "Bearer " + authToken,
                    }
                }
            ),
        ]).then(
            result => {
                return {
                    type: 'success',
                    data: {
                        controllers: result[0].data,
                        branches: result[1].data
                    }
                };
            },
            error => {
                if (error.response.data.code == 401) {
                    clearStore();
                }
                return {
                    type: 'error',
                    data: error.response.data,
                };
            }
        );
    return await request;
}

export async function getCompaniesRequest(data: TaskTableRequestData) {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/companies`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            },
            params: data,
        },
    ).then(
        result => {
            return {
                type: 'success',
                code: 200,
                data: result.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                code: error.response.status,
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function getCompaniesForFilterRequest() {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/companies/for-filter`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        result => {
            return {
                type: 'success',
                code: 200,
                data: result.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                code: error.response.status,
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function getCompanyEditCardDataRequest(companyId: string) {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/company/${companyId}`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        }).then(result => {return {
            type: 'success',
            data: result.data,
        };}, error => {if (error.response.data.code == 401) {
            clearStore();
        }
            return {
                type: 'error',
                data: error.response.data,
            };}
    );
    return await request;
}

export async function addNewCompanyRequest(companyData) {
    let authToken = Store.getState().token;
    let request = axios.post(
        `/api/company/new`,
        companyData,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        result => {
            return {
                type: 'success',
                data: result.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function editCompanyRequest(companyId: string, companyData) {
    let authToken = Store.getState().token;
    let request = axios.post(
        `/api/company/${companyId}/edit`,
        companyData,
        {
            headers: {
                'Authorization': "Bearer "+authToken,
            }
        },
    ).then(function (data) {
            return {
                type: 'success',
                data: data.data,
            };
        }
    ).catch(function (error) {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data
            };
        }
    );
    return await request;
}

export async function deleteCompanyRequest(companyId: string) {
    let authToken = Store.getState().token;
    let request = axios.delete(
        `/api/company/${companyId}`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        response => {
            return {
                type: 'success',
                data: response.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function getControllersRequest() {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/users/Controllers`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        result => {
            return {
                type: 'success',
                code: 200,
                data: result.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                code: error.response.status,
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function getControllersForTasksRequest() {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/users/controllers-for-tasks`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        result => {
            return {
                type: 'success',
                code: 200,
                data: result.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                code: error.response.status,
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function getTaskToWorkRequest(taskId: string) {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/task-to-work/${taskId}`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        task => {
            return {
                type: 'success',
                code: 200,
                data: task.data,
            };
        },
        error => {
            if (error.response.data.status == 401) {
                clearStore();
            }
            return {
                type: 'error',
                code: error.response.status,
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function getTaskToCheckRequest(taskId: string) {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/task-to-check/${taskId}`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        task => {
            return {
                type: 'success',
                code: 200,
                data: task.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                code: error.response.status,
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function getCompletedTaskRequest(taskId: string) {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/completed-task/${taskId}`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        task => {
            return {
                type: 'success',
                code: 200,
                data: task.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                code: error.response.status,
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function editTaskToWorkRequest(id: string, taskData: TaskToWorkPostData) {
    let authToken = Store.getState().token;
    let request = axios.post(
        `/api/task-to-work/${id}/edit`,
        taskData,
        {
            headers: {
                'Authorization': "Bearer "+authToken,
            }
        },
    ).then(function (data) {
            return {
                type: 'success',
                data: data.data,
            };
        }
    ).catch(function (error) {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data
            };
        }
    );
    return await request;
}

export async function editTaskToCheckRequest(id: string, taskData: TaskToCheckPostData) {
    let authToken = Store.getState().token;
    let request = axios.post(
        `/api/task-to-check/${id}/edit`,
        taskData,
        {
            headers: {
                'Authorization': "Bearer "+authToken,
            }
        },
    ).then(function (data) {
            return {
                type: 'success',
                data: data.data,
            };
        }
    ).catch(function (error) {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data
            };
        }
    );
    return await request;
}

export async function acceptTaskToCheckRequest(id: string) {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/task-to-check/${id}/accept`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        result => {
            return {
                type: 'success',
                data: result.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function acceptAllTheTaskToCheckRequest() {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/tasks-to-check/accept`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        result => {
            return {
                type: 'success',
                data: result.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function returnTaskToCheckRequest(id: string) {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/task-to-check/${id}/return`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        result => {
            return {
                type: 'success',
                data: result.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function getCompletedTaskPhotosRequest(id: string) {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/task-upload/${id}/photos`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            },
            responseType: 'blob'
        },
    ).then(
        result => {
            return {
                type: 'success',
                data: result.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function getCompletedTasksPhotosRequest(data: ExcelAndPhotosUploadRequestData) {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/task-upload/photos`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            },
            responseType: 'blob',
            params: data
        },
    ).then(
        result => {
            return {
                type: 'success',
                data: result.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function synchronizeMod1DataRequest() {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/mod1-synchronize`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            },
        },
    ).then(
        result => {
            return {
                type: 'success',
                data: result.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function uploadTaskListRequest(taskListFile) {
    let formData = new FormData();
    formData.append('file', taskListFile);
    let authToken = Store.getState().token;
    let request = axios.post(
        `/api/task/upload`,
        formData,
        {
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': 'Bearer '+authToken,
            }
        },
    ).then(function (data) {
            return {
                type: 'success',
                data: data.data,
            };
        }
    ).catch(function (error) {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data
            };
        }
    );
    return await request;
}

export async function getCompletedTaskExcelRequest(id: string) {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/task-upload/${id}/excel`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            },
            responseType: 'blob'
        },
    ).then(
        result => {
            return {
                type: 'success',
                data: result.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function getCompletedTasksExcelRequest(data: ExcelAndPhotosUploadRequestData) {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/task-upload/excel`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            },
            responseType: 'blob',
            params: data,
        },
    ).then(
        result => {
            return {
                type: 'success',
                data: result.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function uploadBypassListRequest(taskListFile, company) {
    let formData = new FormData();
    formData.append('file', taskListFile);
    if (company) {
        formData.append('company', company);
    }
    let authToken = Store.getState().token;
    let request = axios.post(
        `/api/bypass-list/upload`,
        formData,
        {
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': 'Bearer '+authToken,
            },
        },
    ).then(function (data) {
            return {
                type: 'success',
                data: data.data,
            };
        }
    ).catch(function (error) {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data
            };
        }
    );
    return await request;
}

export async function getBypassRecordsRequest(data: RecordTableRequestData) {
    let authToken = Store.getState().token;
    let request = axios.get(
        '/api/bypass-list-records',
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            },
            params: data,
        },
    ).then(
        tasks => {
            return {
                type: 'success',
                code: 200,
                data: tasks.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                code: error.response.status,
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function getBypassRecordRequest(taskId: string) {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/bypass-list-record/${taskId}`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        task => {
            return {
                type: 'success',
                code: 200,
                data: task.data,
            };
        },
        error => {
            if (error.response.data.status == 401) {
                clearStore();
            }
            return {
                type: 'error',
                code: error.response.status,
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function deleteBypassRecordsRequest(data: TaskTableRequestData) {
    let authToken = Store.getState().token;
    let request = axios.delete(
        `/api/bypass-list-records`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            },
            data: data
        },
    ).then(
        response => {
            return {
                type: 'success',
                data: response.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function editBypassRecordRequest(id: string, taskData: BypassRecordPostData) {
    let authToken = Store.getState().token;
    let request = axios.post(
        `/api/bypass-list-record/${id}/edit`,
        taskData,
        {
            headers: {
                'Authorization': "Bearer "+authToken,
            }
        },
    ).then(function (data) {
            return {
                type: 'success',
                data: data.data,
            };
        }
    ).catch(function (error) {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data
            };
        }
    );
    return await request;
}

export async function deleteBypassRecordRequest(id: string) {
    let authToken = Store.getState().token;
    let request = axios.delete(
        `/api/bypass-list-record/${id}`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            },
        },
    ).then(
        response => {
            return {
                type: 'success',
                data: response.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function acceptRecordsToTasksRequest(data: RecordTableRequestData, status: string = '0') {
    let authToken = Store.getState().token;
    let request = axios.post(
        `/api/bypass-records-to-tasks/${status}`,
        data,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        tasks => {
            return {
                type: 'success',
                code: 200,
                data: tasks.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                code: error.response.status,
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function acceptRecordToTasksRequest(recordId: string) {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/bypass-record-to-task/${recordId}`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        result => {
            return {
                type: 'success',
                data: result.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function bypassMassEditRequest(params, data) {
    let authToken = Store.getState().token;
    let request = axios.post(
        '/api/bypass-list/multi-edit',
        data,
        {
            headers: {
                'Authorization': "Bearer "+authToken,
            },
            params: params,
        },
    ).then(function (data) {
            return {
                type: 'success',
                data: data.data,
            };
        }
    ).catch(function (error) {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data
            };
        }
    );
    return await request;
}

export async function getBadgesCount() {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/entities-count`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        result => {
            return {
                type: 'success',
                data: result.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function getProfileProgressDataRequest(userId: string) {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/user/profile/${userId}`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        result => {
            return {
                type: 'success',
                data: result.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function getProfileProgressExcelRequest(userId: string) {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/user/profile-upload/excel/${userId}`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            },
            responseType: 'blob',
        },
    ).then(
        result => {
            return {
                type: 'success',
                data: result.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function addNewTransformerRequest(transformerData) {
    let authToken = Store.getState().token;
    let request = axios.post(
        `/api/transformer/new`,
        transformerData,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        result => {
            return {
                type: 'success',
                data: result.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function editTransformerRequest(transformerId: string, transformerData) {
    let authToken = Store.getState().token;
    let request = axios.post(
        `/api/transformer/${transformerId}/edit`,
        transformerData,
        {
            headers: {
                'Authorization': "Bearer "+authToken,
            }
        },
    ).then(function (data) {
            return {
                type: 'success',
                data: data.data,
            };
        }
    ).catch(function (error) {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data
            };
        }
    );
    return await request;
}

export async function deleteTransformerRequest(transformerId: string) {
    let authToken = Store.getState().token;
    let request = axios.delete(
        `/api/transformer/${transformerId}`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        response => {
            return {
                type: 'success',
                data: response.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function getTransformerCardDataRequest(transformerId: string) {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/transformer/${transformerId}`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            },
        },
    ).then(
        tasks => {
            return {
                type: 'success',
                code: 200,
                data: tasks.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                code: error.response.status,
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function getCompanyTransformersDataRequest(companyId: string, data: RecordTableRequestData) {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/transformers-by-company/${companyId}`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            },
            params: data,
        }).then(result => {return {
            type: 'success',
            data: result.data,
        };}, error => {if (error.response.data.code == 401) {
            clearStore();
        }
            return {
                type: 'error',
                data: error.response.data,
            };}
    );
    return await request;
}

export async function getTransformerCounterRequest(counterId: string) {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/counter-card/${counterId}`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        task => {
            return {
                type: 'success',
                code: 200,
                data: task.data,
            };
        },
        error => {
            if (error.response.data.status == 401) {
                clearStore();
            }
            return {
                type: 'error',
                code: error.response.status,
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function editTransformerCounterRequest(id: string, data: CounterPostData) {
    let authToken = Store.getState().token;
    let request = axios.post(
        `/api/counter-card/${id}/edit`,
        data,
        {
            headers: {
                'Authorization': "Bearer "+authToken,
            }
        },
    ).then(function (data) {
            return {
                type: 'success',
                data: data.data,
            };
        }
    ).catch(function (error) {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data
            };
        }
    );
    return await request;
}

export async function getCompanyControllersDataRequest(companyId: string) {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/controllers-by-company/${companyId}`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        }).then(result => {return {
            type: 'success',
            data: result.data,
        };}, error => {if (error.response.data.code == 401) {
            clearStore();
        }
            return {
                type: 'error',
                data: error.response.data,
            };}
    );
    return await request;
}

export async function uploadCounterListRequest(taskListFile, id) {
    let formData = new FormData();
    formData.append('file', taskListFile);
    let authToken = Store.getState().token;
    let request = axios.post(
        `/api/counter-list-by-transformer/${id}/upload`,
        formData,
        {
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': 'Bearer '+authToken,
            },
        },
    ).then(function (data) {
            return {
                type: 'success',
                data: data.data,
            };
        }
    ).catch(function (error) {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data
            };
        }
    );
    return await request;
}

export async function getTransformerCountersDataRequest(transformerId: string, data: RecordTableRequestData) {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/counter-list-by-transformer/${transformerId}`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            },
            params: data,
        }).then(result => {return {
            type: 'success',
            data: result.data,
        };}, error => {if (error.response.data.code == 401) {
            clearStore();
        }
            return {
                type: 'error',
                data: error.response.data,
            };}
    );
    return await request;
}

export async function deleteTransformerCounterRequest(id: string) {
    let authToken = Store.getState().token;
    let request = axios.delete(
        `/api/counter/${id}`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            },
        },
    ).then(
        response => {
            return {
                type: 'success',
                data: response.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function getUlSubscribersRequest(data: SubscriberTableRequestData) {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/subscriber/legal-entities`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            },
            params: data,
        },
    ).then(
        result => {
            return {
                type: 'success',
                code: 200,
                data: result.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                code: error.response.status,
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function getFlSubscribersRequest(data: SubscriberTableRequestData) {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/subscriber/individuals`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            },
            params: data,
        },
    ).then(
        result => {
            return {
                type: 'success',
                code: 200,
                data: result.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                code: error.response.status,
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function addNewUlSubscriberRequest(subscriberData) {
    let authToken = Store.getState().token;
    let request = axios.post(
        `/api/subscriber-legal-entity/add`,
        subscriberData,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        result => {
            return {
                type: 'success',
                data: result.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function addNewFlSubscriberRequest(subscriberData) {
    let authToken = Store.getState().token;
    let request = axios.post(
        `/api/subscriber-individual/add`,
        subscriberData,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        result => {
            return {
                type: 'success',
                data: result.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function getSubscriberCardDataRequest(subscriberId: string) {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/subscriber/${subscriberId}`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            },
        }).then(result => {return {
            type: 'success',
            data: result.data,
        };}, error => {if (error.response.data.code == 401) {
            clearStore();
        }
            return {
                type: 'error',
                data: error.response.data,
            };}
    );
    return await request;
}

export async function getSubscriberAgreementsDataRequest(subscriberId: string, data: SubscriberTableRequestData) {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/subscriber/${subscriberId}/agreements`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            },
            params: data,
        }).then(result => {return {
            type: 'success',
            data: result.data,
        };}, error => {if (error.response.data.code == 401) {
            clearStore();
        }
            return {
                type: 'error',
                data: error.response.data,
            };}
    );
    return await request;
}

export async function editUlSubscriberRequest(subscriberId: string, subscriberData) {
    let authToken = Store.getState().token;
    let request = axios.post(
        `/api/subscriber-legal-entity/${subscriberId}/edit`,
        subscriberData,
        {
            headers: {
                'Authorization': "Bearer "+authToken,
            }
        },
    ).then(function (data) {
            return {
                type: 'success',
                data: data.data,
            };
        }
    ).catch(function (error) {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data
            };
        }
    );
    return await request;
}

export async function editFlSubscriberRequest(subscriberId: string, subscriberData) {
    let authToken = Store.getState().token;
    let request = axios.post(
        `/api/subscriber-individual/${subscriberId}/edit`,
        subscriberData,
        {
            headers: {
                'Authorization': "Bearer "+authToken,
            }
        },
    ).then(function (data) {
            return {
                type: 'success',
                data: data.data,
            };
        }
    ).catch(function (error) {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data
            };
        }
    );
    return await request;
}

export async function deleteSubscriberRequest(subscriberId: string) {
    let authToken = Store.getState().token;
    let request = axios.delete(
        `/api/subscriber/${subscriberId}`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        response => {
            return {
                type: 'success',
                data: response.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function addNewAgreementRequest(agreementData) {
    let authToken = Store.getState().token;
    let request = axios.post(
        `/api/agreement/add`,
        agreementData,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        result => {
            return {
                type: 'success',
                data: result.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function getAgreementCardDataRequest(agreementId: string) {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/agreement/${agreementId}`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            },
        }).then(result => {return {
            type: 'success',
            data: result.data,
        };}, error => {if (error.response.data.code == 401) {
            clearStore();
        }
            return {
                type: 'error',
                data: error.response.data,
            };}
    );
    return await request;
}

export async function getAgreementCountersDataRequest(agreementId: string, data: CounterTableRequestData) {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/counters-by-agreement/${agreementId}`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            },
            params: data,
        }).then(result => {return {
            type: 'success',
            data: result.data,
        };}, error => {if (error.response.data.code == 401) {
            clearStore();
        }
            return {
                type: 'error',
                data: error.response.data,
            };}
    );
    return await request;
}

export async function editAgreementRequest(agreementId: string, agreementData) {
    let authToken = Store.getState().token;
    let request = axios.post(
        `/api/agreement/${agreementId}/edit`,
        agreementData,
        {
            headers: {
                'Authorization': "Bearer "+authToken,
            }
        },
    ).then(function (data) {
            return {
                type: 'success',
                data: data.data,
            };
        }
    ).catch(function (error) {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data
            };
        }
    );
    return await request;
}

export async function deleteAgreementRequest(agreementId: string) {
    let authToken = Store.getState().token;
    let request = axios.delete(
        `/api/agreement/${agreementId}`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        response => {
            return {
                type: 'success',
                data: response.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function addNewCounterRequest(agreementId: string, counterData) {
    let authToken = Store.getState().token;
    let request = axios.post(
        `/api/agreement/${agreementId}/add-counter`,
        counterData,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        result => {
            return {
                type: 'success',
                data: result.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function getCounterCardDataRequest(counterId: string) {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/counter/${counterId}`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            },
        }).then(result => {return {
            type: 'success',
            data: result.data,
        };}, error => {if (error.response.data.code == 401) {
            clearStore();
        }
            return {
                type: 'error',
                data: error.response.data,
            };}
    );
    return await request;
}

export async function getCounterValuesDataRequest(counterId: string, data: CounterTableRequestData) {
    let authToken = Store.getState().token;
    let request = axios.get(
        `/api/counter/${counterId}/values`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            },
            params: data,
        }).then(result => {return {
            type: 'success',
            data: result.data,
        };}, error => {if (error.response.data.code == 401) {
            clearStore();
        }
            return {
                type: 'error',
                data: error.response.data,
            };}
    );
    return await request;
}

export async function editCounterRequest(counterId: string, counterData) {
    let authToken = Store.getState().token;
    let request = axios.post(
        `/api/counter/${counterId}/edit`,
        counterData,
        {
            headers: {
                'Authorization': "Bearer "+authToken,
            }
        },
    ).then(function (data) {
            return {
                type: 'success',
                data: data.data,
            };
        }
    ).catch(function (error) {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data
            };
        }
    );
    return await request;
}

export async function deleteCounterRequest(counterId: string) {
    let authToken = Store.getState().token;
    let request = axios.delete(
        `/api/counter/${counterId}`,
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            }
        },
    ).then(
        response => {
            return {
                type: 'success',
                data: response.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                data: error.response.data,
            };
        }
    );
    return await request;
}

export async function getTransformersDataRequest(data: CompanyTransformerTableRequestData) {
    let authToken = Store.getState().token;
    let request = axios.get(
        '/api/transformers',
        {
            headers: {
                'Authorization': "Bearer " + authToken,
            },
            params: data,
        },
    ).then(
        result => {
            return {
                type: 'success',
                code: 200,
                data: result.data,
            };
        },
        error => {
            if (error.response.data.code == 401) {
                clearStore();
            }
            return {
                type: 'error',
                code: error.response.status,
                data: error.response.data,
            };
        }
    );
    return await request;
}