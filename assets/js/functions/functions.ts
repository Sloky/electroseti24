import {
    BypassRecordsCollection,
    CompletedTasksCollection,
    CompleteTaskData,
    IDictionary,
    PhotoData,
    RecordData,
    RecordTableData,
    TaskData,
    TasksToCheckCollection,
    TasksToWorkCollection,
    TaskTableData,
    TaskToCheckData
} from "../interfaces/interfaces";

export function getFullAddress(task: TaskData, forTable: boolean = false) {
    let address = task ? task.temporary_address : 'Нет данных';
    if (task && task.locality && task.street) {
        address = task.locality + ', ' + task.street;
        if (task.house_number) {
            address = task.locality + ', ' + task.street + ', ' + task.house_number;
            if (task.apartment_number && !forTable) {
                address = task.locality + ', ' + task.street + ', ' + task.house_number + ', ' + task.apartment_number;
            }
        }
    }
    return address;
}

/**
 * Transform TaskData array to TaskTableData array
 * @param records
 */
export function getBypassRecordTableData(records: RecordData[]): RecordTableData[] {
    let recordTableData: RecordTableData[] = [];
    records.map(record => {
        let date = record.deadline ? new Date(record.deadline).toLocaleDateString('ru') : null;
        let trBypassRecordView: string = (() => {
            let trView = '';
            switch (record.status) {
                case 1 :
                    trView = 'task_warning';
                    break;
                case 2 :
                    trView = 'task_danger';
                    break;
                default :
                    trView = undefined;
            }
            return trView;
        })();
        recordTableData.push({
            address: record.address,
            agreementNumber: record.agreement_number,
            companyTitle: record.company_title,
            counterNumber: record.counter_number,
            deadline: date,
            id: record.id,
            subscriberTitle: record.subscriber_title,
            subscriberType: record.subscriber_type,
            userFullName: record.controller,
            trView: trBypassRecordView,
            errors: record.errors,
        });
    });
    return recordTableData;
}

/**
 * Transform TaskData array to TaskTableData array
 * @param tasks
 */
export function getTaskToWorkTableData(tasks: TaskData[]): TaskTableData[] {
    let taskTableData: TaskTableData[] = [];
    tasks.map(task => {
        let address = getFullAddress(task, true);
        let date = new Date(task.deadline).toLocaleDateString('ru');
        let executorFullName = task.executor !== null
            && task.executor.surname + ' ' + task.executor.name + ' ' + task.executor.patronymic;
        let userId = task.executor !== null ? task.executor.id : null;
        // let subscriberTitle = task.subscriber_surname + ' ' + task.subscriber_name + ' ' + task.subscriber_patronymic;
        let trTaskToWorkView: string = (() => {
            let trView = '';
            switch (task.priority) {
                case 1 : trView = 'task_priority_1'; break;
                case 2 : trView = 'task_priority_2'; break;
                default : trView = undefined;
            }
            return trView;
        })();
        taskTableData.push({
            address: address,
            agreementNumber: task.agreement_number,
            companyTitle: task.company.title,
            companyId: task.company.id,
            counterNumber: task.counter_number,
            deadline: date,
            id: task.id,
            subscriberTitle: task.subscriber_title,
            subscriberType: task.subscriber_type,
            userFullName: executorFullName,
            userId: userId,
            trView: trTaskToWorkView,
            // trClassName: trTaskToWorkClassName,
            period: task.period
        });
    });
    return taskTableData;
}

/**
 * Transform TaskToCheckData array to TaskTableData array
 * @param tasks
 */
export function getTaskToCheckTableData(tasks: TaskToCheckData[]): TaskTableData[] {
    let taskTableData: TaskTableData[] = [];
    tasks.map(task => {
        let address = getFullAddress(task, true);
        let deadline = new Date(task.deadline).toLocaleDateString();
        let executedDate = new Date(task.executed_date).toLocaleString('ru-RU');
        let executorFullName = task.executor !== null
            && task.executor.surname + ' ' + task.executor.name + ' ' + task.executor.patronymic;
        let userId = task.executor !== null ? task.executor.id : null;
        // let subscriberTitle = task.subscriber_surname + ' ' + task.subscriber_name + ' ' + task.subscriber_patronymic;
        let counterValueDifferenceCheck = () => {
            let result = 'task_danger';
            if (task.last_counter_value && task.current_counter_value) {
                if (task.last_counter_value.length && task.current_counter_value.length) {
                    result = task.last_counter_value[0] < task.current_counter_value[0] ? undefined : 'task_negative';
                }
            }
            return result;
        };
        let trTaskToCheckView: string = (task.counter_physical_status == 1 && 'task_danger')
            || counterValueDifferenceCheck()
            || ((task.changed_counter_type_string
                    || task.changed_counter_number
                    || task.changed_terminal_seal
                    || task.changed_antimagnetic_seal
                    || task.changed_side_seal
                    || task.changed_lodgers_count
                    || task.changed_rooms_count
                ) ? 'task_warning' : undefined);
        taskTableData.push({
            address: address,
            agreementNumber: task.agreement_number,
            companyTitle: task.company.title,
            companyId: task.company.id,
            counterNumber: task.counter_number,
            deadline: deadline,
            executed_date: executedDate,
            id: task.id,
            subscriberTitle: task.subscriber_title,
            subscriberType: task.subscriber_type,
            userFullName: executorFullName,
            userId: userId,
            // trClassName: task.status == 1 && trTaskToCheckClassName,
            trView: (task.status == 1) && trTaskToCheckView,
            period: task.period
        });
    });
    return taskTableData;
}

export function getCompletedTaskTableData(tasks: CompleteTaskData[]): TaskTableData[] {
    let taskTableData: TaskTableData[] = [];
    tasks.map(task => {
        let address = getFullAddress(task, true);
        let deadline = new Date(task.deadline).toLocaleDateString();
        let executedDate = new Date(task.executed_date).toLocaleString('ru-RU');
        let executorFullName = task.executor !== null
            && task.executor.surname + ' ' + task.executor.name + ' ' + task.executor.patronymic;
        let operatorFullName = task.operator !== null
            && task.operator.surname + ' ' + task.operator.name + ' ' + task.operator.patronymic;
        let userId = task.executor !== null ? task.executor.id : null;
        let operatorId = task.operator !== null ? task.operator.id : null;
        // let subscriberTitle = task.subscriber_surname + ' ' + task.subscriber_name + ' ' + task.subscriber_patronymic;
        let trCompletedTaskView: string = (task.counter_physical_status == 1 && 'task_danger')
            || ((task.changed_counter_type_string
                || task.changed_counter_number
                || task.changed_terminal_seal
                || task.changed_antimagnetic_seal
                || task.changed_side_seal
                || task.changed_lodgers_count
                || task.changed_rooms_count
            ) ? 'task_warning' : undefined);
        taskTableData.push({
            address: address,
            agreementNumber: task.agreement_number,
            companyTitle: task.company.title,
            companyId: task.company.id,
            counterNumber: task.counter_number,
            deadline: deadline,
            executed_date: executedDate,
            id: task.id,
            subscriberTitle: task.subscriber_title,
            subscriberType: task.subscriber_type,
            userFullName: executorFullName,
            userId: userId,
            // trClassName: task.status == 2 && trCompletedTaskClassName,
            trView: task.status == 2 && trCompletedTaskView,
            acceptanceDate: task.acceptance_date,
            operatorFullName: operatorFullName,
            operatorId: operatorId,
            period: task.period
        });
    });
    return taskTableData;
}

export function isObjEmpty(obj: object) {
    for (let key in obj) {
        return false;
    }
    return true;
}

export function getBypassRecordsCollection(records: RecordData[]) {
    let recordsCollection: BypassRecordsCollection = {};
    records && records.map(record => {
        recordsCollection[record.id] = record;
    });
    return recordsCollection;
}

export function getTasksToWorkCollection(tasks: TaskData[]) {
    let tasksCollection: TasksToWorkCollection = {};
    tasks.map(task => {
        tasksCollection[task.id] = task;
    });
    return tasksCollection;
}

export function getTasksToCheckCollection(tasks: TaskToCheckData[]) {
    let tasksCollection: TasksToCheckCollection = {};
    tasks.map(task => {
        tasksCollection[task.id] = task;
    });
    return tasksCollection;
}

export function getCompletedTasksCollection(tasks: CompleteTaskData[]) {
    let tasksCollection: CompletedTasksCollection = {};
    tasks.map(task => {
        tasksCollection[task.id] = task;
    });
    return tasksCollection;
}

export function getEntityCollection<EntityType>(entities: EntityType[]) {
    let collection: IDictionary<EntityType> = {};
    entities.map(entity => {
        // @ts-ignore
        collection[entity.id] = entity;
    });
    return collection;
}

export function getItemsByPrefix(items: string[], prefix: string): string[]|undefined {
    let regexp = new RegExp(prefix);
    let filteredItems = items.filter(photo => regexp.test(photo));
    return filteredItems.length > 0 ? filteredItems : undefined;
}

export function getTaskCheckPhotos(task: TaskToCheckData, prefix?: string) {
    let periodDate = new Date(task.period);
    let month = periodDate.getMonth() + 1;
    let period = periodDate.getFullYear() + '-' + (month < 10 ? '0' + month : month);
    let photos: string [] = [];
    task.photos.map((photo: PhotoData) => {
        if (prefix) {
            let regexp = new RegExp(prefix);
            regexp.test(photo.file_name) && photos.push(`${task.photos_public_url}/photos/${period}/${task.id}/${photo.file_name}`);
        } else {
            photos.push(`${task.photos_public_url}/photos/${period}/${task.id}/${photo.file_name}`);
        }
    });
    return photos;
}

export function isMatched(str: string, regexp: RegExp): boolean {
    return regexp.test(str);
}

export const changeTrView = (trView: string) => {
    let trStyle = {};
    switch (trView) {
        case 'task_priority_1' : trStyle = {backgroundColor: 'success.light',}; break;
        case 'task_priority_2' : trStyle = {backgroundColor: 'secondary.light',}; break;
        case 'task_warning' : trStyle = {backgroundColor: 'warning.light',}; break;
        case 'task_danger' : trStyle = {backgroundColor: 'error.light',}; break;
        default : trStyle = {};
    }
    return trStyle;
}

export const changeStatusView = (trView: string) => {
    let status = '';
    switch (trView) {
        case 'task_warning' : status = 'Есть исправления'; break;
        case 'task_danger' : status = 'Нет показаний счётчика'; break;
        case 'task_negative' : status = 'Показания меньше предыдущих'; break;
        default : status = 'Показания сняты';
    }
    return status;
}

export const changeView = (textColor: string, bgColor: string) => {
    let style = {};
    switch (textColor) {
        case 'task_warning' : style['color'] = 'warning.dark'; break;
        case 'task_danger' : style['color'] = 'error.dark'; break;
        case 'danger' : style['color'] = 'error.dark'; break;
        case 'warning' : style['color'] = 'warning.dark'; break;
        case 'info' : style['color'] = 'secondary.main'; break;
        case 'task_negative' : style['color'] = 'error.dark'; break;
        default : style = {...style};
    }
    switch (bgColor) {
        case 'task_warning' : style['backgroundColor'] = 'warning.light'; break;
        case 'task_danger' : style['backgroundColor'] = 'error.light'; break;
        case 'danger' : style['backgroundColor'] = 'error.light'; break;
        case 'warning' : style['backgroundColor'] = 'warning.light'; break;
        case 'info' : style['backgroundColor'] = 'secondary.light'; break;
        case 'task_negative' : style['backgroundColor'] = 'error.light'; break;
        default : style = {...style};
    }
    return style;
}

export const changeGroupNames = (groups) => {
    return groups.map(group => {
        switch (group.group_name) {
            case 'Admins':
                return {...group, group_name: 'Руководитель',}
            case 'Operators':
                return {...group, group_name: 'Оператор',}
            case 'Controllers':
                return {...group, group_name: 'Контроллер',}
        }
    });
}