import * as React from 'react';
import {connect} from 'react-redux';
import {BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom';
import TasksToWorkPage from "../pages/Tasks/TasksToWorkPage";
import LoginPage from "../pages/LoginPage/LoginPage";
import UsersPage from "../pages/Users/UsersPage/UsersPage";
import Error_404 from "../pages/Error/Error_404";
import UserPage from "../pages/Users/UserPage/UserPage";
import UserEditPage from "../pages/Users/UserEditPage/UserEditPage";
import NewUserPage from "../pages/Users/NewUserPage/NewUserPage";
import GroupsPage from "../pages/Groups/GroupsPage/GroupsPage";
import NewGroupPage from "../pages/Groups/NewGroupPage/NewGroupPage";
import EditGroupPage from "../pages/Groups/EditGroupPage/EditGroupPage";
import TaskToWorkPage from "../pages/Task/TaskToWorkPage/TaskToWorkPage";
import {isObjEmpty} from "../functions/functions";
import {appStateInterface} from "../store/appState";
import TasksToCheckPage from "../pages/Tasks/TasksToCheckPage";
import TaskToCheckPage from "../pages/Task/TaskToCheckPage/TaskToCheckPage";
import CompletedTasksPage from "../pages/Tasks/CompletedTasksPage";
import CompletedTaskPage from "../pages/Task/CompletedTaskPage/CompletedTaskPage";
import ProfilePage from "../pages/Profile/ProfilePage";
import CompaniesPage from "../pages/Companies/CompaniesPage/CompaniesPage";
import NewCompanyPage from "../pages/Companies/NewCompanyPage/NewCompanyPage";
import CompanyEditPage from "../pages/Companies/CompanyEditPage/CompanyEditPage";
import BypassPage from "../pages/Bypass/BypassPage";
import BypassRecordPage from "../pages/Task/BypassRecordPage/BypassRecordPage";
import ScrollToTop from "./ScrollToTop";
import CompanyPage from "../pages/Companies/CompanyPage/CompanyPage";
import NewTpPage from "../pages/Tp/NewTpPage/NewTpPage";
import TpEditPage from "../pages/Tp/TpEditPage/TpEditPage";
import TpPage from "../pages/Tp/TpPage/TpPage";
import TpCounterPage from "../pages/Tp/TpCounterPage/TpCounterPage";
import UlSubscribersPage from "../pages/Subscribers/UlSubscribersPage/UlSubscribersPage";
import FlSubscribersPage from "../pages/Subscribers/FlSubscribersPage/FlSubscribersPage";
import NewUlSubscriberPage from "../pages/Subscribers/NewUlSubscriberPage/NewUlSubscriberPage";
import NewFlSubscriberPage from "../pages/Subscribers/NewFlSubscriberPage/NewFlSubscriberPage";
import UlSubscriberPage from "../pages/Subscribers/UlSubscriberPage/UlSubscriberPage";
import FlSubscriberPage from "../pages/Subscribers/FlSubscriberPage/FlSubscriberPage";
import NewAgreementPage from "../pages/Agreement/NewAgreementPage/NewAgreementPage";
import UlSubscriberEditPage from "../pages/Subscribers/UlSubscriberEditPage/UlSubscriberEditPage";
import FlSubscriberEditPage from "../pages/Subscribers/FlSubscriberEditPage/FlSubscriberEditPage";
import AgreementPage from "../pages/Agreement/AgreementPage/AgreementPage";
import AgreementEditPage from "../pages/Agreement/AgreementEditPage/AgreementEditPage";
import NewCounterPage from "../pages/Counters/NewCounterPage/NewCounterPage";
import CounterPage from "../pages/Counters/CounterPage/CounterPage";
import CounterEditPage from "../pages/Counters/CounterEditPage/CounterEditPage";
import UsersRolePage from "../pages/Users/UsersRolePage/UsersRolePage";
import TaskStatementPage from "../pages/Statement/TaskStatementPage";
import NetworkTpPage from "../pages/Network/NetworkTpPage/NetworkTpPage";


function MasterRoute(props) {
    return (
        <Router>
            <ScrollToTop/>
            <Switch>
                <Route path={"/login"} render={()=> !props.isAuthenticated ?  <LoginPage/> : <Redirect to={"/"}/>} />
                <Route path={"/"} exact render={()=> props.isAuthenticated ?  <TasksToWorkPage/> : <Redirect to={"/login"}/>}/>
                <Route path={"/tasks"} render={()=> props.isAuthenticated ?  <TasksToWorkPage/> : <Redirect to={"/login"}/>}/>
                <Route path={"/tasks-to-work"} render={()=> props.isAuthenticated ?  <TasksToWorkPage/> : <Redirect to={"/login"}/>}/>
                <Route path={"/tasks-to-check"} render={()=> props.isAuthenticated ?  <TasksToCheckPage/> : <Redirect to={"/login"}/>}/>
                <Route path={"/completed-tasks"} render={()=> props.isAuthenticated ?  <CompletedTasksPage/> : <Redirect to={"/login"}/>}/>
                {/*<Route path={"/archive"} render={() => props.isAuthenticated ? <ArchiveComponent/> : <Redirect to={"/login"}/>}/>*/}
                <Route path={"/task-to-work/edit/:id"} exact render={()=> props.isAuthenticated ?  <TaskToWorkPage/> : <Redirect to={"/login"}/>}/>
                <Route path={"/task-to-check/edit/:id"} exact render={()=> props.isAuthenticated ?  <TaskToCheckPage/> : <Redirect to={"/login"}/>}/>
                <Route path={"/completed-task/:id"} exact render={()=> props.isAuthenticated ?  <CompletedTaskPage/> : <Redirect to={"/login"}/>}/>
                <Route path={"/profile"} exact render={() => props.isAuthenticated ? <ProfilePage/> : <Redirect to={"/login"}/>}/>
                {!isObjEmpty(props.user) && (props.user.groups.find(group => group.group_name == 'Admins') || (props.user.groups.find(group => group.group_name == 'Operators'))) && (
                    <Route path={"/bypass"} exact render={()=> props.isAuthenticated ?  <BypassPage/> : <Redirect to={"/login"}/>}/>
                )}
                {!isObjEmpty(props.user) && (props.user.groups.find(group => group.group_name == 'Admins') || (props.user.groups.find(group => group.group_name == 'Operators'))) && (
                    <Route path={"/bypass/edit/:id"} exact render={()=> props.isAuthenticated ?  <BypassRecordPage/> : <Redirect to={"/login"}/>}/>
                )}
                {!isObjEmpty(props.user) && props.user.roles.includes('ROLE_SUPER_ADMIN') && (
                    <Route path={"/user/new"} render={()=> props.isAuthenticated ?  <NewUserPage/> : <Redirect to={"/login"}/>}/>
                )}
                {!isObjEmpty(props.user) && props.user.roles.includes('ROLE_SUPER_ADMIN') && (
                    <Route path={"/users-all"} exact render={()=> props.isAuthenticated ?  <UsersPage/> : <Redirect to={"/login"}/>}/>
                )}
                {!isObjEmpty(props.user) && props.user.groups.find(group => group.group_name == 'Admins') && (
                    <Route path={"/users/:role"} exact render={()=> props.isAuthenticated ?  <UsersRolePage/> : <Redirect to={"/login"}/>}/>
                )}
                {!isObjEmpty(props.user) && props.user.groups.find(group => group.group_name == 'Admins') && (
                    <Route path={"/:role/user/:id"} exact render={()=> props.isAuthenticated ?  <UserPage/> : <Redirect to={"/login"}/>}/>
                )}
                {!isObjEmpty(props.user) && props.user.groups.find(group => group.group_name == 'Admins') && (
                    <Route path={"/:role/user-edit/:id"} exact render={()=> props.isAuthenticated ?  <UserEditPage/> : <Redirect to={"/login"}/>}/>
                )}
                {!isObjEmpty(props.user) && props.user.groups.find(group => group.group_name == 'Admins') && (
                    <Route path={"/groups"} render={()=> props.isAuthenticated ?  <GroupsPage/> : <Redirect to={"/login"}/>}/>
                )}
                {!isObjEmpty(props.user) && props.user.groups.find(group => group.group_name == 'Admins') && (
                    <Route path={"/group/new"} render={()=> props.isAuthenticated ?  <NewGroupPage/> : <Redirect to={"/login"}/>}/>
                )}
                {!isObjEmpty(props.user) && props.user.groups.find(group => group.group_name == 'Admins') && (
                    <Route path={"/group/edit/:id"} render={()=> props.isAuthenticated ?  <EditGroupPage/> : <Redirect to={"/login"}/>}/>
                )}
                {!isObjEmpty(props.user) && props.user.groups.find(group => group.group_name == 'Admins') && (
                    <Route path={"/companies"} render={() => props.isAuthenticated ? <CompaniesPage/> : <Redirect to={"/login"}/>}/>
                )}
                {!isObjEmpty(props.user) && props.user.groups.find(group => group.group_name == 'Admins') && (
                    <Route path={"/company-page/:id"} render={() => props.isAuthenticated ? <CompanyPage/> : <Redirect to={"/login"}/>}/>
                )}
                {!isObjEmpty(props.user) && props.user.groups.find(group => group.group_name == 'Admins') && (
                    <Route path={"/company/new"} render={() => props.isAuthenticated ? <NewCompanyPage/> : <Redirect to={"/login"}/>}/>
                )}
                {!isObjEmpty(props.user) && props.user.groups.find(group => group.group_name == 'Admins') && (
                    <Route path={"/company/edit/:id"} render={() => props.isAuthenticated ? <CompanyEditPage/> : <Redirect to={"/login"}/>}/>
                )}
                {!isObjEmpty(props.user) && props.user.groups.find(group => group.group_name == 'Admins') && (
                    <Route path={"/tp-page/:id"} render={() => props.isAuthenticated ? <TpPage/> : <Redirect to={"/login"}/>}/>
                )}
                {!isObjEmpty(props.user) && props.user.groups.find(group => group.group_name == 'Admins') && (
                    <Route path={"/tp-new"} render={() => props.isAuthenticated ? <NewTpPage/> : <Redirect to={"/login"}/>}/>
                )}
                {!isObjEmpty(props.user) && props.user.groups.find(group => group.group_name == 'Admins') && (
                    <Route path={"/tp-edit/:id"} render={() => props.isAuthenticated ? <TpEditPage/> : <Redirect to={"/login"}/>}/>
                )}
                {!isObjEmpty(props.user) && props.user.groups.find(group => group.group_name == 'Admins') && (
                    <Route path={"/tp/counter/:id"} render={() => props.isAuthenticated ? <TpCounterPage/> : <Redirect to={"/login"}/>}/>
                )}
                {!isObjEmpty(props.user) && props.user.groups.find(group => group.group_name == 'Admins') && (
                    <Route path={"/subscribers/ul"} render={() => props.isAuthenticated ? <UlSubscribersPage/> : <Redirect to={"/login"}/>}/>
                )}
                {!isObjEmpty(props.user) && props.user.groups.find(group => group.group_name == 'Admins') && (
                    <Route path={"/subscribers/fl"} render={() => props.isAuthenticated ? <FlSubscribersPage/> : <Redirect to={"/login"}/>}/>
                )}
                {!isObjEmpty(props.user) && props.user.groups.find(group => group.group_name == 'Admins') && (
                    <Route path={"/subscriber/ul-new"} render={() => props.isAuthenticated ? <NewUlSubscriberPage/> : <Redirect to={"/login"}/>}/>
                )}
                {!isObjEmpty(props.user) && props.user.groups.find(group => group.group_name == 'Admins') && (
                    <Route path={"/subscriber/fl-new"} render={() => props.isAuthenticated ? <NewFlSubscriberPage/> : <Redirect to={"/login"}/>}/>
                )}
                {!isObjEmpty(props.user) && props.user.groups.find(group => group.group_name == 'Admins') && (
                    <Route path={"/subscriber/ul/:id"} render={() => props.isAuthenticated ? <UlSubscriberPage/> : <Redirect to={"/login"}/>}/>
                )}
                {!isObjEmpty(props.user) && props.user.groups.find(group => group.group_name == 'Admins') && (
                    <Route path={"/subscriber/fl/:id"} render={() => props.isAuthenticated ? <FlSubscriberPage/> : <Redirect to={"/login"}/>}/>
                )}
                {!isObjEmpty(props.user) && props.user.groups.find(group => group.group_name == 'Admins') && (
                    <Route path={"/subscriber-edit/ul/:id"} render={() => props.isAuthenticated ? <UlSubscriberEditPage/> : <Redirect to={"/login"}/>}/>
                )}
                {!isObjEmpty(props.user) && props.user.groups.find(group => group.group_name == 'Admins') && (
                    <Route path={"/subscriber-edit/fl/:id"} render={() => props.isAuthenticated ? <FlSubscriberEditPage/> : <Redirect to={"/login"}/>}/>
                )}
                {!isObjEmpty(props.user) && props.user.groups.find(group => group.group_name == 'Admins') && (
                    <Route path={"/agreement-new/:type/:id"} render={() => props.isAuthenticated ? <NewAgreementPage/> : <Redirect to={"/login"}/>}/>
                )}
                {!isObjEmpty(props.user) && props.user.groups.find(group => group.group_name == 'Admins') && (
                    <Route path={"/agreement-page/:type/:id"} render={() => props.isAuthenticated ? <AgreementPage/> : <Redirect to={"/login"}/>}/>
                )}
                {!isObjEmpty(props.user) && props.user.groups.find(group => group.group_name == 'Admins') && (
                    <Route path={"/agreement-edit/:type/:id"} render={() => props.isAuthenticated ? <AgreementEditPage/> : <Redirect to={"/login"}/>}/>
                )}
                {!isObjEmpty(props.user) && props.user.groups.find(group => group.group_name == 'Admins') && (
                    <Route path={"/counter-new/:type/:id"} render={() => props.isAuthenticated ? <NewCounterPage/> : <Redirect to={"/login"}/>}/>
                )}
                {!isObjEmpty(props.user) && props.user.groups.find(group => group.group_name == 'Admins') && (
                    <Route path={"/counter-page/:type/:id"} render={() => props.isAuthenticated ? <CounterPage/> : <Redirect to={"/login"}/>}/>
                )}
                {!isObjEmpty(props.user) && props.user.groups.find(group => group.group_name == 'Admins') && (
                    <Route path={"/counter-edit/:type/:id"} render={() => props.isAuthenticated ? <CounterEditPage/> : <Redirect to={"/login"}/>}/>
                )}
                {!isObjEmpty(props.user) && props.user.groups.find(group => group.group_name == 'Admins') && (
                    <Route path={"/network"} render={() => props.isAuthenticated ? <NetworkTpPage/> : <Redirect to={"/login"}/>}/>
                )}
                <Route path="*" render={()=> props.isAuthenticated ?  <Error_404/> : <Redirect to={"/login"}/>}/>
            </Switch>
        </Router>
    );
}

function mapStateToProps(state: appStateInterface) {
    return {
        isAuthenticated: state.isAuthenticated,
        token: state.token,
        user: state.user,
    }
}

export default connect(mapStateToProps)(MasterRoute);