export const themeObj = {
    palette: {
        primary: {
            main: '#1e72ad',
            dark: '#1f6597',
        },
        secondary: {
            main: '#619ac1',
            light: '#8ac1e9',
        },
        background: {
            default: '#ffffff',
            paper: '#F9FCFF',
        },
        text: {
            primary: '#383838',
        },
        error: {
            main: '#E02626',
            light: '#ffebeb',
            dark: '#a92e2e',
        },
        warning: {
            main: '#f0ac27',
            light: '#fff3dc',
            dark: '#b68423',
        },
        success: {
            main: '#4d8b30',
            light: '#e8ffdd',
        },
    },
    typography: {
        fontSize: 16,
        fontFamily: 'Roboto',
        htmlFontSize: 18,
        h1: {
            fontFamily: 'Roboto',
            fontSize: 25,
            fontWeight: 500,
        },
        h2: {
            fontFamily: 'Roboto',
            fontSize: 22,
            fontWeight: 500,
        },
        body1: {
            fontFamily: 'Roboto',
            fontSize: 18,
        },
        body2: {
            fontFamily: 'Roboto',
            fontSize: 16,
        },
        button: {
            fontFamily: 'Roboto',
            fontSize: 16,
        },
    },
};

export const THEME = {
    PRIMARY_COLOR: '#1e72ad',
    HOVER: '#1F6597',
    ACTIVE_BUTTON: '#8AC1E9',
    ACTIVE_BACKGROUND: '#E7F5FF',
    OUTLINE: '#C8E0F0',
    PLACEHOLDER_TEXT: '#898989',
    INACTIVE_TEXT: '#B3B7C2',
    INACTIVE_BACKGROUND: '#E4E7EE',
    WARNING_TEXT: '#b68423',
    WARNING_BACKGROUND: '#FFFBE7',
    BACKGROUND_COLOR: '#FFFFFF',
    LOADER_BACKGROUND: 'rgb(0, 0, 0, 0.3)',
};