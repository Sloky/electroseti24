import '../css/app.css';
import React from 'react';
import MasterRoute from './routes/MasterRoute'
import { Provider } from 'react-redux';
import Store from "./store/store.";
import {ThemeProvider} from "@material-ui/core/styles";
import {createTheme} from "@material-ui/core/styles";
import {themeObj} from "./theme";

const store = Store;
const theme = createTheme(themeObj)

export default function App() {
    return (
        <Provider store={store}>
            <ThemeProvider theme={theme}>
                <MasterRoute/>
            </ThemeProvider>
        </Provider>
    );
}