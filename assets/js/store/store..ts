import {compose, createStore} from 'redux';
import rootReducer from "./reducers/rootReducer";

const saveState = (state, rememberMe: boolean) => {
    try {
        // Convert the state to a JSON string
        const serialisedState = JSON.stringify(state);

        if (rememberMe) {
            // Save the serialised state to localStorage against the key 'app_state'
            localStorage.setItem('app_state', serialisedState);
        } else {
            sessionStorage.setItem('app_state', serialisedState);
        }
    } catch (error) {
        // Log errors here, or ignore
        console.log(error);
    }
};

const loadState = () => {
    try {
        // Load the data saved in localStorage, against the key 'app_state'
        let serialisedState = window.localStorage.getItem('app_state');

        // Passing undefined to createStore will result in our app getting the default state
        // If no data is saved, return undefined
        if (!serialisedState) {
            serialisedState = window.sessionStorage.getItem('app_state');
            if (!serialisedState) {
                return undefined;
            }
        }

        // De-serialise the saved state, and return it.
        return JSON.parse(serialisedState);
    } catch (err) {
        // Return undefined if localStorage is not available,
        // or data could not be de-serialised,
        // or there was some other error
        return undefined;
    }
};

/**
 * This is where you create the app store
 */
const oldState = loadState();
// const Store = createStore(rootReducer, oldState);

const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const Store = createStore(rootReducer, oldState, composeEnhancers());

Store.subscribe(() => {
    let store = Store.getState();
    // console.log('State before saving to localStorage: ', Store.getState());
    saveState(Store.getState(), store.rememberMe);
});

export default Store;