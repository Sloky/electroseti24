import {
    CHANGE_BADGES_COUNT,
    CHANGE_BYPASS_TASK_DATA,
    CHANGE_BYPASS_TASKS_DATA,
    CHANGE_COMPANY_BRANCHES_DATA,
    CHANGE_COMPLETED_TASKS_DATA,
    CHANGE_COUNTER_TYPES_DATA,
    CHANGE_FAILURE_FACTORS_DATA,
    CHANGE_FEEDERS_DATA,
    CHANGE_FILTERS,
    CHANGE_SUBSTATIONS_DATA,
    CHANGE_TASK_TO_CHECK_DATA,
    CHANGE_TASK_TO_WORK_DATA,
    CHANGE_TASKS_TO_CHECK_DATA,
    CHANGE_TASKS_TO_WORK_DATA,
    CHANGE_TRANSFORMERS_DATA,
    CHANGE_USER_GROUPS_DATA,
    CHANGE_USERS_DATA,
    HIDE_LOADER,
    HIDE_TEXT_LOADER,
    REMEMBER_ME,
    SHOW_LOADER,
    SHOW_TEXT_LOADER,
} from "./actionTypes";
import {
    BadgesData,
    BypassRecordsCollection,
    CompanyBranchesCollection,
    CompletedTasksCollection,
    CounterTypesCollection,
    FailureFactorsCollection,
    FeedersCollection,
    FiltersData,
    SubstationsCollection,
    TaskData,
    TasksToCheckCollection,
    TasksToWorkCollection,
    TaskToCheckData,
    TransformersCollection,
    UserGroupsCollection,
    UsersCollection,
} from "../../interfaces/interfaces";

interface changeBypassRecordsActionInterface {
    type: typeof CHANGE_BYPASS_TASKS_DATA,
    payload: BypassRecordsCollection
}

interface changeBypassRecordActionInterface {
    type: typeof CHANGE_BYPASS_TASK_DATA,
    payload: TaskData
}

interface changeTasksToWorkActionInterface {
    type: typeof CHANGE_TASKS_TO_WORK_DATA,
    payload: TasksToWorkCollection
}

interface changeTaskToWorkActionInterface {
    type: typeof CHANGE_TASK_TO_WORK_DATA,
    payload: TaskData
}

interface changeTasksToCheckActionInterface {
    type: typeof CHANGE_TASKS_TO_CHECK_DATA,
    payload: TasksToCheckCollection
}

interface changeTaskToCheckActionInterface {
    type: typeof CHANGE_TASK_TO_CHECK_DATA,
    payload: TaskToCheckData
}

interface changeCompletedTasksActionInterface {
    type: typeof CHANGE_COMPLETED_TASKS_DATA,
    payload: TasksToCheckCollection
}

interface changeCompletedTaskActionInterface {
    type: typeof CHANGE_TASK_TO_CHECK_DATA,
    payload: TaskToCheckData
}

interface changeUsersActionInterface {
    type: typeof CHANGE_USERS_DATA,
    payload: UsersCollection
}

interface changeUserGroupsTasksActionInterface {
    type: typeof CHANGE_USER_GROUPS_DATA,
    payload: UserGroupsCollection
}

interface changeCompanyBranchesActionInterface {
    type: typeof CHANGE_COMPANY_BRANCHES_DATA,
    payload: CompanyBranchesCollection
}

interface changeCounterTypesActionInterface {
    type: typeof CHANGE_COUNTER_TYPES_DATA,
    payload: CounterTypesCollection
}

interface changeSubstationsActionInterface {
    type: typeof CHANGE_SUBSTATIONS_DATA,
    payload: SubstationsCollection
}

interface changeTransformersActionInterface {
    type: typeof CHANGE_TRANSFORMERS_DATA,
    payload: TransformersCollection
}

interface changeFeedersActionInterface {
    type: typeof CHANGE_FEEDERS_DATA,
    payload: FeedersCollection
}

interface changeFailureFactorsActionInterface {
    type: typeof CHANGE_FAILURE_FACTORS_DATA,
    payload: FailureFactorsCollection
}

interface showLoaderInterface {
    type: typeof SHOW_LOADER,
}

interface hideLoaderInterface {
    type: typeof HIDE_LOADER,
}

interface showTextLoaderInterface {
    type: typeof SHOW_TEXT_LOADER,
}

interface hideTextLoaderInterface {
    type: typeof HIDE_TEXT_LOADER,
}

interface rememberMeInterface {
    type: typeof REMEMBER_ME,
    payload: boolean,
}

const changeBypassRecordsAction = (bypassRecords: BypassRecordsCollection): changeBypassRecordsActionInterface => ({
    type: CHANGE_BYPASS_TASKS_DATA,
    payload: bypassRecords,
});

const changeBypassRecordAction = (task: TaskData): changeBypassRecordActionInterface => ({
    type: CHANGE_BYPASS_TASK_DATA,
    payload: task
});

const changeTasksToWorkAction = (tasksToWork: TasksToWorkCollection): changeTasksToWorkActionInterface => ({
    type: CHANGE_TASKS_TO_WORK_DATA,
    payload: tasksToWork
});

const changeTaskToWorkAction = (task: TaskData): changeTaskToWorkActionInterface => ({
    type: CHANGE_TASK_TO_WORK_DATA,
    payload: task
});

const changeTasksToCheckAction = (tasksToCheck: TasksToCheckCollection): changeTasksToCheckActionInterface => ({
    type: CHANGE_TASKS_TO_CHECK_DATA,
    payload: tasksToCheck
});

const changeTaskToCheckAction = (task: TaskToCheckData): changeTaskToCheckActionInterface => ({
    type: CHANGE_TASK_TO_CHECK_DATA,
    payload: task
});

const changeCompletedTasksAction = (completedTasks: CompletedTasksCollection): changeCompletedTasksActionInterface => ({
    type: CHANGE_COMPLETED_TASKS_DATA,
    payload: completedTasks
});

const changeUsersAction = (users: UsersCollection): changeUsersActionInterface => ({
    type: CHANGE_USERS_DATA,
    payload: users
});

const changeUserGroupsTasksAction = (userGroups: UserGroupsCollection): changeUserGroupsTasksActionInterface => ({
    type: CHANGE_USER_GROUPS_DATA,
    payload: userGroups
});

const changeCompanyBranchesAction = (branches: CompanyBranchesCollection): changeCompanyBranchesActionInterface => ({
    type: CHANGE_COMPANY_BRANCHES_DATA,
    payload: branches
});

const changeCounterTypesAction = (counterTypes: CounterTypesCollection): changeCounterTypesActionInterface => ({
    type: CHANGE_COUNTER_TYPES_DATA,
    payload: counterTypes
});

const changeSubstationsAction = (substations: SubstationsCollection): changeSubstationsActionInterface => ({
    type: CHANGE_SUBSTATIONS_DATA,
    payload: substations
});

const changeTransformersAction = (transformers: TransformersCollection): changeTransformersActionInterface => ({
    type: CHANGE_TRANSFORMERS_DATA,
    payload: transformers
});

const changeFeedersAction = (feeders: FeedersCollection): changeFeedersActionInterface => ({
    type: CHANGE_FEEDERS_DATA,
    payload: feeders
});

const changeFailureFactorsAction = (failureFactors: FailureFactorsCollection): changeFailureFactorsActionInterface => ({
    type: CHANGE_FAILURE_FACTORS_DATA,
    payload: failureFactors
});

const showLoader = (): showLoaderInterface => ({
    type: SHOW_LOADER,
});

const hideLoader = (): hideLoaderInterface => ({
    type: HIDE_LOADER,
});

const showTextLoader = (): showTextLoaderInterface => ({
    type: SHOW_TEXT_LOADER,
});

const hideTextLoader = (): hideTextLoaderInterface => ({
    type: HIDE_TEXT_LOADER,
});

const rememberMe = (rememberMeStatus: boolean): rememberMeInterface => ({
    type: REMEMBER_ME,
    payload: rememberMeStatus,
});

const changeBadgesCount = (badgesCountData: BadgesData) => ({
    type: CHANGE_BADGES_COUNT,
    payload: badgesCountData,
});

const changeFilters = (filtersData: FiltersData) => ({
    type: CHANGE_FILTERS,
    payload: filtersData,
});

export const TasksAction = {
    changeTasksToWorkAction,
    changeTasksToCheckAction,
    changeCompletedTasksAction,
    changeBypassRecordsAction: changeBypassRecordsAction,

    changeBypassRecordAction: changeBypassRecordAction,
    changeTaskToWorkAction,
    changeTaskToCheckAction,

    changeBadgesCount,
    changeFilters,
}

export const EntitiesAction = {
    changeUsersAction,
    changeUserGroupsTasksAction,
    changeCompanyBranchesAction,
    changeCounterTypesAction,
    changeSubstationsAction,
    changeTransformersAction,
    changeFeedersAction,
    changeFailureFactorsAction
}

export const AppAction = {
    showLoader,
    hideLoader,
    showTextLoader,
    hideTextLoader,
    rememberMe,
}