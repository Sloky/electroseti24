import {
    APP_CLEAR_AUTH_STATE,
    APP_CLEAR_STATE,
    CHANGE_AUTH_STATUS,
    CHANGE_COMPANY_BRANCHES_DATA,
    CHANGE_COMPLETED_TASKS_DATA,
    CHANGE_COUNTER_TYPES_DATA, CHANGE_FAILURE_FACTORS_DATA, CHANGE_FEEDERS_DATA,
    CHANGE_SUBSTATIONS_DATA, CHANGE_TASK_TO_CHECK_DATA, CHANGE_TASK_TO_WORK_DATA,
    CHANGE_TASKS_TO_CHECK_DATA,
    CHANGE_TASKS_TO_WORK_DATA,
    CHANGE_TOKEN, CHANGE_TRANSFORMERS_DATA,
    CHANGE_USER_DATA,
    CHANGE_USER_GROUPS_DATA,
    CHANGE_USERS_DATA,
    HIDE_LOADER,
    SHOW_LOADER,
    REMEMBER_ME,
    CHANGE_BYPASS_TASKS_DATA,
    CHANGE_BADGES_COUNT,
    CHANGE_FILTERS,
    SHOW_TEXT_LOADER,
    HIDE_TEXT_LOADER,
} from "../actions/actionTypes";
import {appState} from "../appState";

const initialState = appState;

export default function rootReducer(state = initialState, action = null) {
    switch (action.type) {
        case CHANGE_TOKEN: return {
            ...state,
            token: action.payload,
        };
        case CHANGE_AUTH_STATUS: return {
            ...state,
            isAuthenticated: action.payload,
        };
        case CHANGE_USER_DATA: return {
            ...state,
            user: action.payload,
        };

        case CHANGE_BYPASS_TASKS_DATA: return {
            ...state,
            bypassRecords: action.payload,
        };

        case CHANGE_TASKS_TO_WORK_DATA: return {
            ...state,
            tasksToWork: action.payload,
        };
        case CHANGE_TASK_TO_WORK_DATA: return {
            ...state,
            tasksToWork: {...state.tasksToWork, [action.payload.id]: action.payload}
        }
        case CHANGE_TASKS_TO_CHECK_DATA: return {
            ...state,
            tasksToCheck: action.payload,
        };
        case CHANGE_TASK_TO_CHECK_DATA: return {
            ...state,
            tasksToCheck: {...state.tasksToCheck, [action.payload.id]: action.payload}
        }
        case CHANGE_COMPLETED_TASKS_DATA: return {
            ...state,
            completedTasks: action.payload,
        }

        case CHANGE_COMPANY_BRANCHES_DATA: return {
            ...state,
            companyBranches: action.payload,
        }
        case CHANGE_USERS_DATA: return {
            ...state,
            users: action.payload
        }
        case CHANGE_USER_GROUPS_DATA: return {
            ...state,
            userGroups: action.payload
        }
        case CHANGE_COUNTER_TYPES_DATA: return {
            ...state,
            counterTypes: action.payload
        }
        case CHANGE_SUBSTATIONS_DATA: return {
            ...state,
            substations: action.payload
        }
        case CHANGE_TRANSFORMERS_DATA: return {
            ...state,
            transformers: action.payload
        }
        case CHANGE_FEEDERS_DATA: return {
            ...state,
            feeders: action.payload
        }
        case CHANGE_FAILURE_FACTORS_DATA: return {
            ...state,
            failureFactors: action.payload
        }

        case APP_CLEAR_STATE: return {
            token: '',
            user: undefined,
            isAuthenticated: false,
            tasksToWork: {},
            tasksToCheck: {},
            completedTasks: {},
            companyBranches: {},
            users: {},
            userGroups: {},
            counterTypes: {},
            substations: {},
            transformers: {},
            feeders: {},
            failureFactors: {},
            loading: false,
            rememberMe: false,
        }
        case APP_CLEAR_AUTH_STATE: return {
            token: '',
            user: undefined,
            isAuthenticated: false,
            loading: false,
            rememberMe: false,
        }
        case SHOW_LOADER: return {
            ...state,
            loading: true,
        }
        case HIDE_LOADER: return {
            ...state,
            loading: false,
        }
        case SHOW_TEXT_LOADER: return {
            ...state,
            loadingText: true,
        }
        case HIDE_TEXT_LOADER: return {
            ...state,
            loadingText: false,
        }
        case REMEMBER_ME: return {
            ...state,
            rememberMe: action.payload,
        }
        case CHANGE_BADGES_COUNT: return {
            ...state,
            badgesCount: action.payload,
        }
        case CHANGE_FILTERS: return {
            ...state,
            filtersData: action.payload,
        }
        default : return state;
    }
}