import {
    BadgesData,
    BypassRecordsCollection,
    CompanyBranchesCollection,
    CompletedTasksCollection,
    CounterTypesCollection,
    FailureFactorsCollection,
    FeedersCollection,
    FiltersData,
    SubstationsCollection,
    TasksToCheckCollection,
    TasksToWorkCollection,
    TransformersCollection,
    UserData,
    UserGroupsCollection,
    UsersCollection
} from "../interfaces/interfaces";

export interface appStateInterface {
    isAuthenticated: boolean,
    token: string,
    user: UserData,
    bypassRecords: BypassRecordsCollection
    tasksToWork: TasksToWorkCollection
    tasksToCheck: TasksToCheckCollection
    completedTasks: CompletedTasksCollection
    companyBranches: CompanyBranchesCollection
    users: UsersCollection
    userGroups: UserGroupsCollection
    counterTypes: CounterTypesCollection
    substations: SubstationsCollection
    transformers: TransformersCollection
    feeders: FeedersCollection
    failureFactors: FailureFactorsCollection
    loading: boolean
    loadingText: boolean
    rememberMe: boolean
    badgesCount: BadgesData
    filtersData: FiltersData
}

export const appState: appStateInterface = {
        isAuthenticated: false,
        token: '',
        user: <UserData> {},
        bypassRecords: <BypassRecordsCollection> {},
        tasksToWork: <TasksToWorkCollection> {},
        tasksToCheck: <TasksToCheckCollection> {},
        completedTasks: <CompletedTasksCollection> {},
        companyBranches: <CompanyBranchesCollection> {},
        users: <UsersCollection> {},
        userGroups: <UserGroupsCollection> {},
        counterTypes: <CounterTypesCollection> {},
        substations: <SubstationsCollection> {},
        transformers: <TransformersCollection> {},
        feeders: <FeedersCollection> {},
        failureFactors: <FailureFactorsCollection> {},
        loading: false,
        loadingText: false,
        rememberMe: false,
        badgesCount: <BadgesData> {},
        filtersData: <FiltersData> {},
};