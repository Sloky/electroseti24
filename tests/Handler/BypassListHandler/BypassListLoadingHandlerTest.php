<?php

namespace App\Tests\Handler\BypassListHandler;

use App\Handler\BypassListHandler\BypassListLoadingHandler;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BypassListLoadingHandlerTest extends KernelTestCase
{

    private $excelPath;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->excelPath = realpath(__DIR__.'/счет учет даг огни физ.л.xls');
    }

    public function testParseBypassList()
    {
        self::bootKernel();

        $container = static::$container;

        $excelTaskHandler = $container->get(BypassListLoadingHandler::class);

        $result = $excelTaskHandler->parseBypassList($this->excelPath);

        $this->assertIsArray($result);
    }
}
