<?php

namespace App\Tests\Handler\S3Handler;

use App\Handler\S3Handler\UploadHandler;
use Exception;
use League\Flysystem\FilesystemException;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\File\File;

class UploadHandlerTest extends KernelTestCase
{
    /**
     * File path to the photo
     * @var false|string
     */
    private $photo;

    /**
     * @var UploadHandler|object|null
     */
    private $uploadHandler;

    const DIRECTORY = '/photos/task_test/test_photos/';
    private static string $savedFile;

    protected function setUp(): void
    {
        parent::setUp();
//        $this->photo = realpath(__DIR__.'./photo_2021-09-21_15-36-06.jpg');
    }


    public function __construct($name = null, array $data = [], $dataName = '')
    {
        self::bootKernel();
        parent::__construct($name, $data, $dataName);
        $projectDir = static::$container->getParameter('project_dir');
//        $this->photo = realpath($projectDir.'/tests/Handler/S3Handler/photo_2021-09-21_15-36-06.jpg');
        $dir = __DIR__;
        $this->photo = realpath($dir.'/photo_2021-09-21_15-36-06.jpg');
        $this->uploadHandler = static::$container->get(UploadHandler::class);
    }

    /**
     * @throws FilesystemException
     */
    public function testUploadPhoto()
    {
        $file = new File($this->photo, true);
        $file_name = uniqid('counter_value_photo_') . '.' . $file->guessExtension();

        $result = $this->uploadHandler->uploadPhoto($file, self::DIRECTORY, $file_name, true);

        $this->assertIsString($result);
        self::$savedFile = $result;
        var_dump(self::$savedFile);
    }

    /**
     * @throws FilesystemException
     */
    public function testGetPublicPath()
    {
        $error = null;
        $file = self::DIRECTORY.self::$savedFile;
        try {
            $result = $this->uploadHandler->getPublicPath($file);
            $this->assertIsString($result);
            var_dump($result);
        } catch (Exception $exception) {
            $error = $exception->getMessage();
        }
        $this->assertNull($error);
    }

    /**
     * @throws FilesystemException
     */
    public function testDeleteFile()
    {
        $error = null;
        $file = self::DIRECTORY.self::$savedFile;
        try {
            $this->uploadHandler->deleteFile($file);
        } catch (Exception $exception) {
            $error = $exception->getMessage();
        }

        $this->assertNull($error);
    }
}
