<?php

namespace App\Tests\HandlerTest\TaskHandlers;

use App\Handler\TaskHandler\ExcelTaskListHandler;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ExcelTaskListHandlerTest extends KernelTestCase
{
    private $excelPath;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->excelPath = realpath(__DIR__.'/vedomost_mod3_2.xls');
//        $this->excelPath = realpath(__DIR__.'/vedomost_mod3_2021_08_01.xls');
    }

    public function testParseBypassList()
    {
        self::bootKernel();

        $container = static::$container;

        $excelTaskHandler = $container->get(ExcelTaskListHandler::class);

        $result = $excelTaskHandler->parseBypassList($this->excelPath);

        $this->assertIsArray($result);
    }
}
