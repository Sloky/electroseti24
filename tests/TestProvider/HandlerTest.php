<?php

namespace App\Tests\TestProvider;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TestHandler extends WebTestCase
{
    /**
     * Generates an error string containing data for the test
     * @param $testDescription "Test description"
     * @param $message "Error message"
     * @param array $testData "Data for the test"
     * @param $response
     * @return string
     */
    protected function getErrorMessage($testDescription, $message, array $testData = [], $response = null)
    {
        $msg = "Тест $testDescription.\n"
            .$message."\n"
            ."Входящие параметры: \n"
            .$this->arrToReadableStr($testData)
            .$response;
        return $msg;
    }

    /**
     * Converts the $array to a readable format
     * @param $array
     * @return string
     */
    protected function arrToReadableStr($array): string
    {
        $str = "";
        foreach ($array as $key => $value)
        {
            if (is_array($value))
            {
                $str .= "\n\t".$key." = [".$this->arrToReadableStr($value)."]";
            }
            else
            {
                $str .= "\n\t" . $key . " = " . $value;
            }
        }
        return $str;
    }

    /**
     * Get the random value for type 3 cost value
     * @return array
     */
    /*protected function getRandomCostWeightedAverageType3()
    {
        $arr = [];
        for ($d = 1; $d <= rand(30, 31); $d++) {
            for ($h = 0; $h <= 23; $h++) {
                $arr[] = floatval(rand(100, 999)/100);
            }
        }
        return $arr;
    }*/
}