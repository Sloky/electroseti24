<?php


namespace App\Tests\HandlerTest;


use App\Handler\UserHandler\UserHandler;
use Exception;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserHandlerTest extends WebTestCase
{
    /**
     * @var UserHandler
     */
    protected $userHandler;

    /**
     * Setup
     */
//    public function setUp()
//    {
//        $client = static::createClient();
//        $this->userHandler = $client->getContainer()->get('app.handler.user_handler');
//    }

    /**
     * @dataProvider providerGetUsersByGroupNameMethod
     * @param $description
     * @param array $inputData
     * @param array $expectedData
     */
    public function testGetUsersByGroupNameMethod($description, $inputData, array $expectedData)
    {
        $client = static::createClient();
        $this->userHandler = $client->getContainer()->get('app.handler.user_handler');
        $exceptions = null;
        try {
            $this->userHandler->getUsersByGroupName($inputData);
        } catch (Exception $e) {
            $exceptions = ['statusCode' => $e->getCode(), 'message' => $e->getMessage()];
        }
        $this->assertEquals($expectedData['exceptions']['message'], $exceptions['message'], $description.'|'.$exceptions['message']);
    }

    /**
     * @dataProvider providerGetUsersByGroupNameMethod
     * @param $description
     * @param $taskId
     * @param $inputData
     * @param array $expectedData
     */
    public function testEditTaskMethod($description, $taskId, $inputData, array $expectedData)
    {
        $client = static::createClient();
        $this->userHandler = $client->getContainer()->get('app.handler.user_handler');
        $exceptions = null;
        try {
            $this->userHandler->editUser($taskId, $inputData);
        } catch (Exception $e) {
            $exceptions = ['statusCode' => $e->getCode(), 'message' => $e->getMessage()];
        }
        $this->assertEquals($expectedData['exceptions']['message'], $exceptions['message'], $description.'|'.$exceptions['message']);
    }

    /**
     * Add tariff method data provider
     * @return array
     * @throws Exception
     */
    public function providerGetUsersByGroupNameMethod()
    {
        return [
            'Success test 1' => [
                $description = 'Сохранение тариффа 1 ЦК в БД (Все парамеры формы верны)',
                'Admins',
                [
                    'exceptions' => null
                ]
            ],
        ];
    }
}