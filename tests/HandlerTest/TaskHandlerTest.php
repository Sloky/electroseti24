<?php


namespace App\Tests\HandlerTest;


use App\Entity\Main\Task;
use App\Model\Assembler\TaskAssembler;
use Exception;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TaskHandlerTest extends WebTestCase
{
    protected $taskHandler;

    protected $em;

//    protected function setUp(): void
//    {
//        $client = static::createClient();
//        $this->taskHandler = $client->getContainer()->get('app.handler.task_handler');
//        $this->em = $client->getContainer()->get('doctrine.orm.entity_manager');
//    }

    /**
     * @dataProvider providerEditTaskMethod
     * @param $description
     * @param $taskId
     * @param $inputData
     * @param array $expectedData
     */
    public function testEditTaskMethod($description, $taskId, $inputData, array $expectedData)
    {
        $client = static::createClient();
        $this->taskHandler = $client->getContainer()->get('app.handler.task_handler');
        $exceptions = null;
        try {
            $this->taskHandler->editTask($taskId, $inputData);
        } catch (Exception $e) {
            $exceptions = ['statusCode' => $e->getCode(), 'message' => $e->getMessage()];
        }
        $this->assertEquals($expectedData['exceptions']['message'], $exceptions['message'], $description.'|'.$exceptions['message']);
    }

    public function providerEditTaskMethod()
    {
        $client = static::createClient();
        $this->em = $client->getContainer()->get('doctrine.orm.entity_manager');
        /** @var Task $task Задача к выполнению*/
        $task = $this->em->getRepository(Task::class)->findOneBy(['taskType' => 0]);
        $taskDTO = TaskAssembler::writeDTO($task);
        return [
            'Success test 1' => [
                $description = 'Сохранение тариффа 1 ЦК в БД (Все парамеры формы верны)',
                $taskDTO->getId(),
                [
                    'taskType' => $taskDTO->getTaskType(),
                    'executor' => $taskDTO->getExecutor()->getId(),
                    'priority' => $taskDTO->getPriority(),
                    'deadline' => $taskDTO->getDeadline()->format('c'),
                    'status' => $taskDTO->getStatus(),
                    'coordinateX' => $taskDTO->getCoordinateX(),
                    'coordinateY' => $taskDTO->getCoordinateY(),
                    'operatorComment' => $taskDTO->getOperatorComment(),
                    'controllerComment' => $taskDTO->getControllerComment(),
                    'failureFactors' => $taskDTO->getFailureFactors()->getValues(),
                    'actualizedFields' => $taskDTO->getActualizedFields(),
                    'changedFields' => $taskDTO->getChangedFields(),
                    'company' => $taskDTO->getCompany()->getId(),
                    'locality' => $taskDTO->getLocality(),
                    'street' => $taskDTO->getStreet(),
                    'houseNumber' => $taskDTO->getHouseNumber(),
                    'porchNumber' => $taskDTO->getPorchNumber(),
                    'apartmentNumber' => $taskDTO->getApartmentNumber(),
                    'lodgersCount' => $taskDTO->getLodgersCount(),
                    'roomsCount' => $taskDTO->getRoomsCount(),
                    'subscriberType' => $taskDTO->getSubscriberType(),
                    'subscriberTitle' => $taskDTO->getSubscriberTitle(),
                    'subscriberName' => $taskDTO->getSubscriberName(),
                    'subscriberSurname' => $taskDTO->getSubscriberSurname(),
                    'subscriberPatronymic' => $taskDTO->getSubscriberPatronymic(),
                    'subscriberPhones' => $taskDTO->getSubscriberPhones(),
                    'agreementNumber' => $taskDTO->getAgreementNumber(),
                    'counterNumber' => $taskDTO->getCounterNumber(),
                    'lastCounterValue' => $taskDTO->getLastCounterValue(),
                    'currentCounterValue' => $taskDTO->getCurrentCounterValue(),
                    'consumption' => $taskDTO->getConsumption(),
                    'consumptionCoefficient' => $taskDTO->getConsumptionCoefficient(),
                    'counterPhysicalStatus' => $taskDTO->getCounterPhysicalStatus(),
                    'substation' => $taskDTO->getSubstation()->getId(),
                    'feeder' => $taskDTO->getFeeder()->getId(),
                    'transformer' => $taskDTO->getTransformer()->getId(),
                    'lineNumber' => $taskDTO->getLineNumber(),
                    'electricPoleNumber' => $taskDTO->getElectricPoleNumber(),
                    'counterType' => $taskDTO->getCounterType()->getId(),
                    'terminalSeal' => $taskDTO->getTerminalSeal(),
                    'antiMagneticSeal' => $taskDTO->getAntiMagneticSeal(),
                    'sideSeal' => $taskDTO->getSideSeal(),
//                    'photos' => $taskDTO->getPhotos(),
                    'photos' => ['photo1'],
                ],
                [
                    'exceptions' => null
                ]
            ],
        ];
    }

}