<?php

namespace App\Security;

use App\Entity\Main\User;
use App\Entity\Main\User as AppUser;
use App\Security\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

//use Symfony\Component\Security\Core\Exception\CustomUserMessageAccountStatusException;

class UserChecker implements UserCheckerInterface
{
    public function checkPreAuth(UserInterface $user): void
    {
        if (!$user instanceof AppUser) {
            return;
        }

        /** @var $user User */
//        if ($user->getWorkStatus() !== 1) {
//            throw new AccessDeniedException();
//        }
    }

    public function checkPostAuth(UserInterface $user): void
    {
        if (!$user instanceof AppUser) {
            return;
        }

        /** @var $user User */
        if ($user->getWorkStatus() !== 1) {
            throw new HttpException(403, 'Access denied');
        }
    }
}