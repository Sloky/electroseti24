<?php

namespace App\DataFixtures;

use App\Entity\Main\Company;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class CompanyFixtures extends Fixture implements FixtureGroupInterface
{
    public static $refs = [
        'company-1',
        'company-2',
        'company-3',
        'company-4',
        'company-5',
        'company-6',
    ];

    public function provider()
    {
        return [
            'company-1' => ($mainCompany = new Company())
                ->setTitle('Электрон')
            ,
//            'company-2' => (new Company())
//                ->setTitle('Даогинское отделение')
//                ->setParent($mainCompany)
//            ,
//            'company-3' => (new Company())
//                ->setTitle('Махачкалинское отделение')
//                ->setParent($mainCompany)
//            ,
//            'company-4' => (new Company())
//                ->setTitle('Каспийское отделение')
//                ->setParent($mainCompany)
//            ,
//            'company-5' => (new Company())
//                ->setTitle('Кизилюртовское отделение')
//                ->setParent($mainCompany)
//            ,
//            'company-6' => (new Company())
//                ->setTitle('Кизлярское отделение')
//                ->setParent($mainCompany)
//            ,
        ];
    }

    public function load(ObjectManager $manager)
    {
        $messages = $this->provider();

        foreach ($messages as $key => $message) {
            $manager->persist($message);

            $this->addReference($key, $message);
        }
        $manager->flush();
    }

    /**
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['userGroup'];
    }
}
