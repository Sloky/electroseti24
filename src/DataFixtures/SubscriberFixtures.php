<?php

namespace App\DataFixtures;

use App\Entity\Main\Subscriber;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class SubscriberFixtures extends Fixture
{
    public static function getSubscriberRefs(): array
    {
        $refs = [];
        for ($i=1; $i<=15; $i++) {
            $refs[] = "subscriber-$i";
        }
        return $refs;
    }

    public function provider()
    {
        return [
            'subscriber-1' => (new Subscriber())
                ->setSubscriberType(0)
                ->setSurname('Иванов')
                ->setName('Иван')
                ->setPatronymic('Иванович')
                ->setPhones(['+79851234567'])
            ,
            'subscriber-2' => (new Subscriber())
                ->setSubscriberType(0)
                ->setSurname('Сергеева')
                ->setName('Елена')
                ->setPatronymic('Сергеевна')
                ->setPhones(['+79851487631'])
            ,
            'subscriber-3' => (new Subscriber())
                ->setSubscriberType(0)
                ->setSurname('Гаджиев')
                ->setName('Иван')
                ->setPatronymic('Петрович')
                ->setPhones(['+79163241562'])
            ,
            'subscriber-4' => (new Subscriber())
                ->setSubscriberType(0)
                ->setSurname('Никольский')
                ->setName('Петр')
                ->setPatronymic('Михайлович')
                ->setPhones(['+79096679044', '+79037882210', '+79859642333'])
            ,
            'subscriber-5' => (new Subscriber())
                ->setSubscriberType(0)
                ->setSurname('Лаврентьев')
                ->setName('Павел')
                ->setPatronymic('Казимирович')
                ->setPhones(['+79165902138'])
            ,
            'subscriber-6' => (new Subscriber())
                ->setSubscriberType(0)
                ->setSurname('Железняков')
                ->setName('Николай')
                ->setPatronymic('Павлович')
                ->setPhones(['+79097845621'])
            ,
            'subscriber-7' => (new Subscriber())
                ->setSubscriberType(0)
                ->setSurname('Абдулов')
                ->setName('Георгий')
                ->setPatronymic('Петрович')
                ->setPhones(['+79997422311'])
            ,
            'subscriber-8' => (new Subscriber())
                ->setSubscriberType(0)
                ->setSurname('Хасанов')
                ->setName('Али')
                ->setPatronymic('Русланович')
                ->setPhones(['+79075642001', '+79012287539'])
            ,
            'subscriber-9' => (new Subscriber())
                ->setSubscriberType(0)
                ->setSurname('Мамедов')
                ->setName('Иса')
                ->setPatronymic('Рустамович')
                ->setPhones(['+79157092445'])
            ,
            'subscriber-10' => (new Subscriber())
                ->setSubscriberType(0)
                ->setSurname('Улицкий')
                ->setName('Михаил')
                ->setPatronymic('Николаевич')
                ->setPhones(['+79856409211'])
            ,
            'subscriber-11' => (new Subscriber())
                ->setSubscriberType(0)
                ->setSurname('Мострюков')
                ->setName('Иван')
                ->setPatronymic('Сергеевич')
                ->setPhones(['+79090896722'])
            ,
            'subscriber-12' => (new Subscriber())
                ->setSubscriberType(0)
                ->setSurname('Самедов')
                ->setName('Руслан')
                ->setPatronymic('Магомедович')
                ->setPhones(['+79163387519', '+79094673207'])
            ,
            'subscriber-13' => (new Subscriber())
                ->setTitle('ИП Алексашенко Сергей Ильич')
                ->setSubscriberType(1)
                ->setPhones(['+7 872230649', '+79178884210'])
            ,
            'subscriber-14' => (new Subscriber())
                ->setTitle('ООО Вектор')
                ->setSubscriberType(1)
                ->setPhones(['+78723938894', '+79039842126'])
            ,
            'subscriber-15' => (new Subscriber())
                ->setTitle('ОАО Энергоресурс')
                ->setSubscriberType(1)
                ->setPhones(['+78723940018', '+79851328947'])
            ,
        ];
    }

    public function load(ObjectManager $manager)
    {
        $messages = $this->provider();

        foreach ($messages as $key => $message) {
            $manager->persist($message);

            $this->addReference($key, $message);
        }
        $manager->flush();
    }
}
