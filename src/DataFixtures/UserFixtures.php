<?php

namespace App\DataFixtures;

use App\Entity\Main\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function provider()
    {
        return [
            'user-admin' => ($userAdmin = new User())
                ->setUsername('admin')
                ->setName('Админ')
                ->setPatronymic('Админович')
                ->setSurname('Админский')
                ->setRoles(['ROLE_USER', 'ROLE_SUPER_ADMIN'])
                ->setPassword($this->passwordEncoder->encodePassword($userAdmin, 'Energoline_24'))
                ->setWorkStatus(1)
                ->setCompanies(new ArrayCollection([$this->getReference('company-1')]))
                ->setAddress('г. Москва, ул. Админская, д. 1а')
                ->setPhone('+79876543210')
                ->setGroups(new ArrayCollection([$this->getReference('group-admins')]))
            ,
            'user-konev' => ($userKonev = new User())
                ->setUsername('konev')
                ->setName('Михаил')
                ->setPatronymic('Николаевич')
                ->setSurname('Конев')
                ->setRoles(['ROLE_USER', 'ROLE_SUPER_ADMIN'])
                ->setPassword($this->passwordEncoder->encodePassword($userKonev, 'h^GEDgVgKQyE'))
                ->setWorkStatus(1)
                ->setCompanies(new ArrayCollection([$this->getReference('company-1')]))
                ->setAddress('г. Москва')
                ->setPhone('+79999999999')
                ->setGroups(new ArrayCollection([$this->getReference('group-admins')]))
            ,
//            'user-super-operator' => ($userSuperOperator = new User())
//                ->setUsername('super_operator')
//                ->setName('Опер')
//                ->setPatronymic('Оперович')
//                ->setSurname('Старший')
//                ->setRoles(['ROLE_USER'])
//                ->setPassword($this->passwordEncoder->encodePassword($userSuperOperator, 'super_operator'))
//                ->setWorkStatus(1)
//                ->setCompany($this->getReference('company-2'))
//                ->setAddress('г. Москва, ул. Операторская, д. 2')
//                ->setPhone('+79786543210')
//                ->setGroups(new ArrayCollection([$this->getReference('group-operators')]))
//            ,
//            'user-operator' => ($userOperator = new User())
//                ->setUsername('operator')
//                ->setName('Оператор')
//                ->setPatronymic('Операторович')
//                ->setSurname('Операторский')
//                ->setRoles(['ROLE_USER'])
//                ->setPassword($this->passwordEncoder->encodePassword($userOperator, 'operator'))
//                ->setWorkStatus(1)
//                ->setCompany($this->getReference('company-3'))
//                ->setAddress('г. Москва, ул. Операторская, д. 3')
//                ->setPhone('+79167843218')
//                ->setGroups(new ArrayCollection([$this->getReference('group-operators')]))
//            ,
//            'user-controller1' => ($userOperator = new User())
//                ->setUsername('controller1')
//                ->setName('Семен')
//                ->setPatronymic('Семенович')
//                ->setSurname('Обходский')
//                ->setRoles(['ROLE_USER'])
//                ->setPassword($this->passwordEncoder->encodePassword($userOperator, 'controller1'))
//                ->setWorkStatus(1)
//                ->setCompany($this->getReference('company-4'))
//                ->setAddress('г. Москва, ул. Контролёрская, д. 5')
//                ->setPhone('+7979954381')
//                ->setGroups(new ArrayCollection([$this->getReference('group-controllers')]))
//            ,
//            'user-controller2' => ($userOperator = new User())
//                ->setUsername('controller2')
//                ->setName('Алексей')
//                ->setPatronymic('Иванович')
//                ->setSurname('Пешеходов')
//                ->setRoles(['ROLE_USER'])
//                ->setPassword($this->passwordEncoder->encodePassword($userOperator, 'controller2'))
//                ->setWorkStatus(1)
//                ->setCompany($this->getReference('company-5'))
//                ->setAddress('г. Москва, ул. Контролёрская, д. 6')
//                ->setPhone('+79786569002')
//                ->setGroups(new ArrayCollection([$this->getReference('group-controllers')]))
//            ,
        ];
    }

    public function load(ObjectManager $manager)
    {
        $messages = $this->provider();

        foreach ($messages as $key => $message) {
            $manager->persist($message);

            $this->addReference($key, $message);
        }
        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array class-string[]
     */
    public function getDependencies()
    {
        return array(
            UserGroupFixtures::class,
            CompanyFixtures::class
        );
    }

    /**
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['userGroup'];
    }
}
