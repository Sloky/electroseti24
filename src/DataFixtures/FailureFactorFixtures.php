<?php

namespace App\DataFixtures;

use App\Entity\Main\FailureFactor;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class FailureFactorFixtures extends Fixture
{
    public static $refs = [
        'no_access',
        'broken_counter',
        'broken_seal_1',
        'broken_seal_2',
        'broken_seal_3',
        'incorrect_model',
        'incorrect_number',
    ];

    public function provider()
    {
        return [
            'no_access' => (new FailureFactor())
                ->setTitle('Нет доступа')
                ->setMessage('Нет доступа к узлу учёта')
            ,
            'broken_counter' => (new FailureFactor())
                ->setTitle('УУ неисправен')
                ->setMessage('Неисправен узел учёта')
            ,
            'broken_seal_1' => (new FailureFactor())
                ->setTitle('Неисправна клеммная пломба')
                ->setMessage('Нет доступа к узлу учёта')
            ,
            'broken_seal_2' => (new FailureFactor())
                ->setTitle('Неисправна антимагнитная пломба')
                ->setMessage('Нет доступа к узлу учёта')
            ,
            'broken_seal_3' => (new FailureFactor())
                ->setTitle('Неисправна боковая пломба')
                ->setMessage('Нет доступа к узлу учёта')
            ,
            'incorrect_model' => (new FailureFactor())
                ->setTitle('Неверная модель УУ')
                ->setMessage('Модель УУ абонента не соответствует данным договора')
            ,
            'incorrect_number' => (new FailureFactor())
                ->setTitle('Неверный номер УУ')
                ->setMessage('Номер УУ абонента не соответствует данным договора')
            ,
        ];
    }

    public function load(ObjectManager $manager)
    {
        $messages = $this->provider();

        foreach ($messages as $key => $message) {
            $manager->persist($message);
            $this->addReference($key, $message);
        }
        $manager->flush();
    }
}
