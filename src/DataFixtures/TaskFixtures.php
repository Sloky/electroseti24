<?php

namespace App\DataFixtures;

use App\Entity\Main\Task;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\HttpKernel\Exception\HttpException;

class TaskFixtures extends Fixture implements DependentFixtureInterface
{
    public static function getTaskRefs(): array
    {
        $taskRefs = [];
        for ($i=1; $i<=20; $i++) {
            $taskRefs[] = "task_$i";
        }
        return $taskRefs;
    }

    public function provider()
    {
        $tasks = [];
        $executors = ['user-controller1', 'user-controller2'];
        $subscribers = SubscriberFixtures::getSubscriberRefs();
        $agreements = AgreementFixtures::$refs;

        for ($i=1; $i<=20; $i++) {
            try {
                $task = new Task();
                $counter = $this->getReference("counter-$i");
                $agreement = $counter->getAgreement();
                $company = $agreement->getCompany();
                $subscriber = $agreement->getSubscriber();
                $counterValue = $this->getReference("count_val_$i");
                $task
                    ->setTaskType(0)
                    ->setCompany($company)
                    ->setCounter($counter)
                    ->setPeriod((new DateTimeImmutable())->modify('first day of this month'))
                    ->setDeadline((new DateTimeImmutable())->modify('last day of this month'))
                    ->setExecutor($i%3 !== 0 ? $this->getReference($executors[array_rand($executors)]) : null)
                    ->setPriority(0)
                    ->setStatus(0)
                    ->setCoordinates([rand(46000000, 48000000) / 1000000, rand(42000000, 43000000) / 1000000])
                    ->setOperatorComment(null)
                    ->setControllerComment(null)
//                ->setFailureFactors()
                    ->setActualizedFields([])
                    ->setChangedFields([])
                    ->setLastCounterValue($counterValue)
//                ->setCurrentCounterValue()
//                ->setConsumption()
//                ->setConsumptionCoefficient()
                    ->setCounterPhysicalStatus(0)
                    ->setSubscriber($subscriber)
                    ->setAgreement($counter->getAgreement())//
//                ->setPhotos()
                ;
                $tasks["task_$i"] = $task;
            } catch (\Exception $exception) {
                throw new HttpException(400,"Итерация:$i;".$exception->getMessage());
            }
        }
        return $tasks;
    }

    public function load(ObjectManager $manager)
    {
        $messages = $this->provider();

        foreach ($messages as $key => $message) {
            $manager->persist($message);
            $this->addReference($key, $message);
        }
        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array class-string[]
     */
    public function getDependencies()
    {
        return array(
            CounterFixtures::class,
            SubscriberFixtures::class,
            CompanyFixtures::class,
            AgreementFixtures::class,
            CounterTypeFixtures::class,
            UserFixtures::class
        );
    }
}
