<?php

namespace App\DataFixtures;

use App\Entity\Main\Address;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AddressFixtures extends Fixture
{
    public static $refs = [
        'address_1',
        'address_2',
        'address_3',
        'address_4',
        'address_5',
        'address_6',
        'address_7',
        'address_8',
        'address_9',
        'address_10',
        'address_11',
        'address_12',
        'address_13',
        'address_14',
        'address_15',
        'address_16',
        'address_17',
        'address_18',
        'address_19',
        'address_20',
    ];

    public function provider()
    {
        return [
            'address_1' => (new Address())
                ->setLocality('г. Махачкала')
                ->setStreet('ул. Ленина')
                ->setHouseNumber('1')
                ->setPorchNumber('1')
                ->setApartmentNumber('3')
                ->setLodgersCount(3)
                ->setRoomsCount(2)
            ,
            'address_2' => (new Address())
                ->setLocality('г. Махачкала')
                ->setStreet('ул. Ленина')
                ->setHouseNumber('1')
                ->setPorchNumber('1')
                ->setApartmentNumber('5')
                ->setLodgersCount(3)
                ->setRoomsCount(2)
            ,
            'address_3' => (new Address())
                ->setLocality('г. Махачкала')
                ->setStreet('ул. Ленина')
                ->setHouseNumber('2')
                ->setPorchNumber('3')
                ->setApartmentNumber('25')
                ->setLodgersCount(3)
                ->setRoomsCount(2)
            ,
            'address_4' => (new Address())
                ->setLocality('г. Махачкала')
                ->setStreet('ул. Ленина')
                ->setHouseNumber('3')
                ->setPorchNumber('4')
                ->setApartmentNumber('37')
                ->setLodgersCount(3)
                ->setRoomsCount(2)
            ,
            'address_5' => (new Address())
                ->setLocality('г. Махачкала')
                ->setStreet('ул. Исаева')
                ->setHouseNumber('5')
                ->setPorchNumber('1')
                ->setApartmentNumber('7')
                ->setLodgersCount(3)
                ->setRoomsCount(2)
            ,
            'address_6' => (new Address())
                ->setLocality('г. Махачкала')
                ->setStreet('ул. Приморская')
                ->setHouseNumber('7')
                ->setPorchNumber('1')
                ->setApartmentNumber('12')
                ->setLodgersCount(3)
                ->setRoomsCount(2)
            ,
            'address_7' => (new Address())
                ->setLocality('г. Махачкала')
                ->setStreet('ул. Приморская')
                ->setHouseNumber('7')
                ->setPorchNumber('4')
                ->setApartmentNumber('83')
                ->setLodgersCount(3)
                ->setRoomsCount(2)
            ,
            'address_8' => (new Address())
                ->setLocality('г. Махачкала')
                ->setStreet('ул. Катерная')
                ->setHouseNumber('12')
                ->setPorchNumber('1')
                ->setApartmentNumber('33')
                ->setLodgersCount(3)
                ->setRoomsCount(2)
            ,
            'address_9' => (new Address())
                ->setLocality('г. Каспийск')
                ->setStreet('ул. Моторная')
                ->setHouseNumber('15')
                ->setPorchNumber('2')
                ->setApartmentNumber('27')
                ->setLodgersCount(3)
                ->setRoomsCount(2)
            ,
            'address_10' => (new Address())
                ->setLocality('г. Каспийск')
                ->setStreet('ул. Мичурина')
                ->setHouseNumber('7')
                ->setPorchNumber('1')
                ->setApartmentNumber('21')
                ->setLodgersCount(3)
                ->setRoomsCount(2)
            ,
            'address_11' => (new Address())
                ->setLocality('г. Каспийск')
                ->setStreet('ул. Мичурина')
                ->setHouseNumber('2')
                ->setPorchNumber('4')
                ->setApartmentNumber('38')
                ->setLodgersCount(3)
                ->setRoomsCount(2)
            ,
            'address_12' => (new Address())
                ->setLocality('г. Каспийск')
                ->setStreet('ул. Колхозная')
                ->setHouseNumber('2')
                ->setPorchNumber('2')
                ->setApartmentNumber('48')
                ->setLodgersCount(3)
                ->setRoomsCount(2)
            ,
            'address_13' => (new Address())
                ->setLocality('г. Каспийск')
                ->setStreet('ул. Ленина')
                ->setHouseNumber('1')
                ->setPorchNumber('1')
                ->setApartmentNumber('3')
                ->setLodgersCount(3)
                ->setRoomsCount(2)
            ,
            'address_14' => (new Address())
                ->setLocality('г. Махачкала')
                ->setStreet('ул. Горная')
                ->setHouseNumber('6')
                ->setPorchNumber('3')
                ->setApartmentNumber('73')
                ->setLodgersCount(3)
                ->setRoomsCount(2)
            ,
            'address_15' => (new Address())
                ->setLocality('г. Дагогни')
                ->setStreet('ул. Шамиля')
                ->setHouseNumber('2')
                ->setPorchNumber('3')
                ->setApartmentNumber('39')
                ->setLodgersCount(3)
                ->setRoomsCount(2)
            ,
            'address_16' => (new Address())
                ->setLocality('г. Дагогни')
                ->setStreet('ул. Шамиля')
                ->setHouseNumber('13')
                ->setPorchNumber('1')
                ->setApartmentNumber('11')
                ->setLodgersCount(3)
                ->setRoomsCount(2)
            ,
            'address_17' => (new Address())
                ->setLocality('г. Дагогни')
                ->setStreet('ул. Летная')
                ->setHouseNumber('2')
                ->setPorchNumber('4')
                ->setApartmentNumber('97')
                ->setLodgersCount(3)
                ->setRoomsCount(2)
            ,
            'address_18' => (new Address())
                ->setLocality('г. Дагогни')
                ->setStreet('ул. Летная')
                ->setHouseNumber('2')
                ->setPorchNumber('3')
                ->setApartmentNumber('67')
                ->setLodgersCount(3)
                ->setRoomsCount(2)
            ,
            'address_19' => (new Address())
                ->setLocality('г. Кизляр')
                ->setStreet('ул. Речная')
                ->setHouseNumber('8')
                ->setPorchNumber('2')
                ->setApartmentNumber('37')
                ->setLodgersCount(3)
                ->setRoomsCount(2)
            ,
            'address_20' => (new Address())
                ->setLocality('г. Кизляр')
                ->setStreet('ул. Речная')
                ->setHouseNumber('14')
                ->setPorchNumber('3')
                ->setApartmentNumber('78')
                ->setLodgersCount(3)
                ->setRoomsCount(2)
            ,
        ];
    }

    public function load(ObjectManager $manager)
    {
        $messages = $this->provider();

        foreach ($messages as $key => $message) {
            $manager->persist($message);
            $this->addReference($key, $message);
        }
        $manager->flush();
    }
}
