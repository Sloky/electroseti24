<?php

namespace App\DataFixtures;

use App\Entity\Main\Transformer;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TransformerFixtures extends Fixture implements DependentFixtureInterface
{
    public static $refs = [
        'transformer-11',
        'transformer-12',
        'transformer-13',
        'transformer-21',
        'transformer-22',
        'transformer-31',
        'transformer-32',
        'transformer-33',
        'transformer-34',
    ];

    public function provider()
    {
        return [
            'transformer-11' => (new Transformer())
                ->setTitle('ТП 1341/1000 кВА')
                ->setFeeder($this->getReference('feeder-11'))
            ,
            'transformer-12' => (new Transformer())
                ->setTitle('ТП 1378/630 кВА')
                ->setFeeder($this->getReference('feeder-12'))
            ,
            'transformer-13' => (new Transformer())
                ->setTitle('КТП 45/630 кВА')
                ->setFeeder($this->getReference('feeder-13'))
            ,
            'transformer-21' => (new Transformer())
                ->setTitle('ТМ 160 кВА')
                ->setFeeder($this->getReference('feeder-21'))
            ,
            'transformer-22' => (new Transformer())
                ->setTitle('ЗТП 925/160 кВА')
                ->setFeeder($this->getReference('feeder-22'))
            ,
            'transformer-31' => (new Transformer())
                ->setTitle('ТП "Горянка" 630 кВА')
                ->setFeeder($this->getReference('feeder-31'))
            ,
            'transformer-32' => (new Transformer())
                ->setTitle('КТП 18/630 кВА')
                ->setFeeder($this->getReference('feeder-32'))
            ,
            'transformer-33' => (new Transformer())
                ->setTitle('КТП "Пальмира" 400кВА')
                ->setFeeder($this->getReference('feeder-33'))
            ,
            'transformer-34' => (new Transformer())
                ->setTitle('ТСГЛ 250 кВА')
                ->setFeeder($this->getReference('feeder-34'))
            ,
        ];
    }

    public function load(ObjectManager $manager)
    {
        $messages = $this->provider();

        /**
         * @var string $key
         * @var \App\Entity\Main\Transformer $message
         */
        foreach ($messages as $key => $message) {

            $manager->persist($message);
            $feeder = $message->getFeeder();
            $feeder->setTransformer($message);
            $manager->persist($feeder);

            $this->addReference($key, $message);
        }

        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array class-string[]
     */
    public function getDependencies()
    {
        return array(
            FeederFixtures::class
        );
    }
}
