<?php

namespace App\DataFixtures;

use App\Entity\Main\CounterType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CounterTypeFixtures extends Fixture
{
    public static $refs = [
        'counterType-1',
        'counterType-2',
        'counterType-3',
        'counterType-4',
        'counterType-5',
        'counterType-6',
    ];

    public function provider()
    {
        return [
            'counterType-1' => (new CounterType())
                ->setTitle('Меркурий 201.8')
                ->setTariffType(CounterType::$tariffs_type_value[1])
                ->setClassAccuracy(1)
                ->setElectronCurrentType(1)
                ->setNumberPhases(1)
            ,
            'counterType-2' => (new CounterType())
                ->setTitle('CE 303 S31 543')
                ->setTariffType(CounterType::$tariffs_type_value[1])
                ->setClassAccuracy(2)
                ->setElectronCurrentType(1)
                ->setNumberPhases(1)
            ,
            'counterType-3' => (new CounterType())
                ->setTitle('NP73E.3-14-1')
                ->setTariffType(CounterType::$tariffs_type_value[1])
                ->setClassAccuracy(1)
                ->setElectronCurrentType(1)
                ->setNumberPhases(1)
            ,
            'counterType-4' => (new CounterType())
                ->setTitle('STAR 302/1C4-5(7,5)ЭТ')
                ->setTariffType(CounterType::$tariffs_type_value[1])
                ->setClassAccuracy(3)
                ->setElectronCurrentType(1)
                ->setNumberPhases(1)
            ,
            'counterType-5' => (new CounterType())
                ->setTitle('Альфа A1805RALQ-P4GB-DW-4')
                ->setTariffType(CounterType::$tariffs_type_value[1])
                ->setClassAccuracy(1)
                ->setElectronCurrentType(1)
                ->setNumberPhases(1)
            ,
            'counterType-6' => (new CounterType())
                ->setTitle('Каскад 3МТWV 31-AO')
                ->setTariffType(CounterType::$tariffs_type_value[1])
                ->setClassAccuracy(1)
                ->setElectronCurrentType(1)
                ->setNumberPhases(1)
            ,
        ];
    }

    public function load(ObjectManager $manager)
    {
        $messages = $this->provider();

        foreach ($messages as $key => $message) {
            $manager->persist($message);

            $this->addReference($key, $message);
        }
        $manager->flush();
    }
}
