<?php

namespace App\DataFixtures;

use App\Entity\Main\UserGroup;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class UserGroupFixtures extends Fixture implements FixtureGroupInterface
{
    public function provider()
    {
        return [
            'group-admins' => ($userAdmin = new UserGroup())
                ->setGroupName('Admins')
                ->setRoles(['ROLE_ADMIN'])
                ->setDescription('Admins description')
            ,
            'group-operators' => ($userAdmin = new UserGroup())
                ->setGroupName('Operators')
                ->setRoles(['ROLE_OPERATOR'])
                ->setDescription('Operators description')
            ,
            'group-controllers' => ($userAdmin = new UserGroup())
                ->setGroupName('Controllers')
                ->setRoles(['ROLE_CONTROLLER'])
                ->setDescription('Controllers description')
            ,
        ];
    }

    public function load(ObjectManager $manager)
    {
        $messages = $this->provider();

        foreach ($messages as $key => $message) {
            $manager->persist($message);

            $this->addReference($key, $message);
        }
        $manager->flush();
    }

    /**
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['userGroup'];
    }
}