<?php

namespace App\DataFixtures;

use App\Entity\Main\CounterValue;
use App\Model\CounterValueType1;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CounterValueFixtures extends Fixture implements DependentFixtureInterface
{
    public static $refs = [
        'count_val_1',
        'count_val_2',
        'count_val_3',
        'count_val_4',
        'count_val_5',
        'count_val_6',
        'count_val_7',
        'count_val_8',
        'count_val_9',
        'count_val_10',
        'count_val_11',
        'count_val_12',
        'count_val_13',
        'count_val_14',
        'count_val_15',
        'count_val_16',
        'count_val_17',
        'count_val_18',
        'count_val_19',
        'count_val_20',
    ];

    public function provider()
    {
        $counterValues = [];

        for ($i=1; $i<=20; $i++) {
            $counterValue = new CounterValue();
            $number = ($i+10)%7;
            $counterValue
                ->setCounter($this->getReference("counter-$i"))
                ->setDate((new DateTimeImmutable())->modify('first day of this month')->modify("+ $number days"))
                ->setInspector($this->getReference("user-controller".(($i%2)+1)))
                ->setValue(new CounterValueType1([floatval(rand(10000, 20000)/1000)]))
                ;
            $counterValues["count_val_$i"] = $counterValue;
        }

        return $counterValues;
    }

    public function load(ObjectManager $manager)
    {
        $messages = $this->provider();

        foreach ($messages as $key => $message) {
            $manager->persist($message);
            $this->addReference($key, $message);
        }
        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array class-string[]
     */
    public function getDependencies()
    {
        return array(
            CounterFixtures::class,
            UserFixtures::class,
        );
    }
}
