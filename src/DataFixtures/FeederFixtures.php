<?php

namespace App\DataFixtures;

use App\Entity\Main\Feeder;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class FeederFixtures extends Fixture implements DependentFixtureInterface
{
    public function provider()
    {
        return [
            'feeder-11' => (new Feeder())
                ->setTitle('Ф-11')
                ->setSubstation($this->getReference('substation-1'))
            ,
            'feeder-12' => (new Feeder())
                ->setTitle('Ф-12')
                ->setSubstation($this->getReference('substation-1'))
            ,
            'feeder-13' => (new Feeder())
                ->setTitle('Ф-14')
                ->setSubstation($this->getReference('substation-1'))
            ,
            'feeder-21' => (new Feeder())
                ->setTitle('Ф-21')
                ->setSubstation($this->getReference('substation-2'))
            ,
            'feeder-22' => (new Feeder())
                ->setTitle('Ф-22')
                ->setSubstation($this->getReference('substation-2'))
            ,
            'feeder-31' => (new Feeder())
                ->setTitle('Ф-31')
                ->setSubstation($this->getReference('substation-3'))
            ,
            'feeder-32' => (new Feeder())
                ->setTitle('Ф-32')
                ->setSubstation($this->getReference('substation-3'))
            ,
            'feeder-33' => (new Feeder())
                ->setTitle('Ф-33')
                ->setSubstation($this->getReference('substation-3'))
            ,
            'feeder-34' => (new Feeder())
                ->setTitle('Ф-34')
                ->setSubstation($this->getReference('substation-3'))
            ,
        ];
    }

    public function load(ObjectManager $manager)
    {
        $messages = $this->provider();

        foreach ($messages as $key => $message) {
            $manager->persist($message);

            $this->addReference($key, $message);
        }
        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array class-string[]
     */
    public function getDependencies()
    {
        return array(
            SubstationFixtures::class
        );
    }
}
