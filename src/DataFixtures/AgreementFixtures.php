<?php

namespace App\DataFixtures;

use App\Entity\Main\Agreement;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class AgreementFixtures extends Fixture implements DependentFixtureInterface
{
    public static $refs = [
        'agreement-1-1',
        'agreement-1-2',
        'agreement-2',
        'agreement-3',
        'agreement-4',
        'agreement-5',
        'agreement-6',
        'agreement-7',
        'agreement-8-1',
        'agreement-8-2',
        'agreement-9',
        'agreement-10',
        'agreement-11',
        'agreement-12',
        'agreement-13',
        'agreement-14-1',
        'agreement-14-2',
        'agreement-14-3',
        'agreement-15',
    ];

    public function provider()
    {
        $companyRefs = CompanyFixtures::$refs;
        return [
            'agreement-1-1' => (new Agreement())
                ->setSubscriber($this->getReference('subscriber-1'))
                ->setNumber('5003289011')
                ->setCompany($this->getReference($companyRefs[array_rand($companyRefs)]))
            ,
            'agreement-1-2' => (new Agreement())
                ->setSubscriber($this->getReference('subscriber-1'))
                ->setNumber('5003289012')
                ->setCompany($this->getReference($companyRefs[array_rand($companyRefs)]))
            ,
            'agreement-2' => (new Agreement())
                ->setSubscriber($this->getReference('subscriber-2'))
                ->setNumber('5003289020')
                ->setCompany($this->getReference($companyRefs[array_rand($companyRefs)]))
            ,
            'agreement-3' => (new Agreement())
                ->setSubscriber($this->getReference('subscriber-3'))
                ->setNumber('5003289030')
                ->setCompany($this->getReference($companyRefs[array_rand($companyRefs)]))
            ,
            'agreement-4' => (new Agreement())
                ->setSubscriber($this->getReference('subscriber-4'))
                ->setNumber('5003289040')
                ->setCompany($this->getReference($companyRefs[array_rand($companyRefs)]))
            ,
            'agreement-5' => (new Agreement())
                ->setSubscriber($this->getReference('subscriber-5'))
                ->setNumber('5003289050')
                ->setCompany($this->getReference($companyRefs[array_rand($companyRefs)]))
            ,
            'agreement-6' => (new Agreement())
                ->setSubscriber($this->getReference('subscriber-6'))
                ->setNumber('5003289060')
                ->setCompany($this->getReference($companyRefs[array_rand($companyRefs)]))
            ,
            'agreement-7' => (new Agreement())
                ->setSubscriber($this->getReference('subscriber-7'))
                ->setNumber('5003289070')
                ->setCompany($this->getReference($companyRefs[array_rand($companyRefs)]))
            ,
            'agreement-8-1' => (new Agreement())
                ->setSubscriber($this->getReference('subscriber-8'))
                ->setNumber('5003289081')
                ->setCompany($this->getReference($companyRefs[array_rand($companyRefs)]))
            ,
            'agreement-8-2' => (new Agreement())
                ->setSubscriber($this->getReference('subscriber-8'))
                ->setNumber('5003289082')
                ->setCompany($this->getReference($companyRefs[array_rand($companyRefs)]))
            ,
            'agreement-9' => (new Agreement())
                ->setSubscriber($this->getReference('subscriber-9'))
                ->setNumber('5003289090')
                ->setCompany($this->getReference($companyRefs[array_rand($companyRefs)]))
            ,
            'agreement-10' => (new Agreement())
                ->setSubscriber($this->getReference('subscriber-10'))
                ->setNumber('5003289100')
                ->setCompany($this->getReference($companyRefs[array_rand($companyRefs)]))
            ,
            'agreement-11' => (new Agreement())
                ->setSubscriber($this->getReference('subscriber-11'))
                ->setNumber('5003289110')
                ->setCompany($this->getReference($companyRefs[array_rand($companyRefs)]))
            ,
            'agreement-12' => (new Agreement())
                ->setSubscriber($this->getReference('subscriber-12'))
                ->setNumber('5003289120')
                ->setCompany($this->getReference($companyRefs[array_rand($companyRefs)]))
            ,
            'agreement-13' => (new Agreement())
                ->setSubscriber($this->getReference('subscriber-13'))
                ->setNumber('5003289130')
                ->setCompany($this->getReference($companyRefs[array_rand($companyRefs)]))
            ,
            'agreement-14-1' => (new Agreement())
                ->setSubscriber($this->getReference('subscriber-14'))
                ->setNumber('5003289141')
                ->setCompany($this->getReference($companyRefs[array_rand($companyRefs)]))
            ,
            'agreement-14-2' => (new Agreement())
                ->setSubscriber($this->getReference('subscriber-14'))
                ->setNumber('5003289142')
                ->setCompany($this->getReference($companyRefs[array_rand($companyRefs)]))
            ,
            'agreement-14-3' => (new Agreement())
                ->setSubscriber($this->getReference('subscriber-14'))
                ->setNumber('5003289143')
                ->setCompany($this->getReference($companyRefs[array_rand($companyRefs)]))
            ,
            'agreement-15' => (new Agreement())
                ->setSubscriber($this->getReference('subscriber-15'))
                ->setNumber('5003289150')
                ->setCompany($this->getReference($companyRefs[array_rand($companyRefs)]))
            ,
        ];
    }

    public function load(ObjectManager $manager)
    {
        $messages = $this->provider();

        /**
         * @var string $key
         * @var \App\Entity\Main\Agreement $message
         */
        foreach ($messages as $key => $message) {
            $manager->persist($message);

            $subscriber = $message->getSubscriber();
            $subscriber->addAgreement($message);
            $manager->persist($subscriber);

            $this->addReference($key, $message);
        }
        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array class-string[]
     */
    public function getDependencies()
    {
        return array(
            SubscriberFixtures::class,
            CompanyFixtures::class
        );
    }
}
