<?php

namespace App\DataFixtures;

use App\Entity\Main\Address;
use App\Entity\Main\Agreement;
use App\Entity\Main\Counter;
use App\Entity\Main\CounterType;
use App\Entity\Main\CounterValue;
use App\Entity\Main\Transformer;
use App\Model\CounterValueType1;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CounterFixtures extends Fixture implements DependentFixtureInterface
{
    private $agreements = [];

    private $counterTypes = [];

    private $transformers = [];

    private $address = [];

    public function __construct()
    {}

    private function getAgreementFixtures()
    {
        $agreementFixturesRefs = AgreementFixtures::$refs;
        foreach ($agreementFixturesRefs as $ref) {
            $this->agreements[] = $this->getReference($ref);
        }
    }

    private function getCounterTypeFixtures()
    {
        $counterTypeFixturesRefs = CounterTypeFixtures::$refs;
        foreach ($counterTypeFixturesRefs as $ref) {
            $this->counterTypes[] = $this->getReference($ref);
        }
    }

    private function getTransformerFixtures()
    {
        $transformerFixturesRefs = TransformerFixtures::$refs;
        foreach ($transformerFixturesRefs as $ref) {
            $this->transformers[] = $this->getReference($ref);
        }
    }

    private function getAddressFixtures()
    {
        $addressFixturesRefs = AddressFixtures::$refs;
        foreach ($addressFixturesRefs as $ref) {
            $this->address[] = $this->getReference($ref);
        }
    }

    private function generateCounter(
        string $title,
        CounterType $counterType,
        Agreement $agreement,
        Transformer $transformer,
        string $electricLine,
        string $electricPoleNumber,
        string $terminalSeal,
        string $antiMagneticSeal,
        string $sideSeal,
        ?Address $address = null
    )
    {
        $counter = new Counter();
        $counter->setTitle($title);
        $counter->setCounterType($counterType);
        $counter->setAgreement($agreement);
        $counter->setTransformer($transformer);
        $counter->setElectricLine($electricLine);
        $counter->setElectricPoleNumber($electricPoleNumber);
        $counter->setTerminalSeal($terminalSeal);
        $counter->setAntiMagneticSeal($antiMagneticSeal);
        $counter->setSideSeal($sideSeal);
        $counter->setAddress($address);
        $counter->setInstallDate((new DateTimeImmutable())->modify('-1 month'));
        return $counter;
    }

    public function provider()
    {
        $this->getAgreementFixtures();
        $this->getCounterTypeFixtures();
        $this->getTransformerFixtures();
        $this->getAddressFixtures();

        $counters = [];

        for ($i = 1; $i <= 20; $i++) {
            $title = "5059837".(99-$i);
            $counterType = $this->counterTypes[rand(0, count($this->counterTypes)-1)];
//            $agreement = $this->agreements[rand(0, count($this->agreements)-1)];
            $agreement = $this->agreements[$i%4];
            $transformer = $this->transformers[rand(0, count($this->transformers)-1)];
            $address = $this->address[$i%4];
            $electricLine = $i;
            $electricPoleNumber = 273 + (3*$i);
            $terminalSeal = "5437".(99-2*$i);
            $antiMagneticSeal = "9542".(99-2*$i);
            $sideSeal = "129475".(99-2*$i);
//            $inVal = floatval(rand(1000, 10000)/1000);
//            $initialValue = new CounterValueType1([$inVal]);
            $counters["counter-$i"] = $this->generateCounter(
                $title,
                $counterType,
                $agreement,
                $transformer,
                $electricLine,
                $electricPoleNumber,
                $terminalSeal,
                $antiMagneticSeal,
                $sideSeal,
                $address
            );
        }


        return $counters;
    }

    public function load(ObjectManager $manager)
    {
        $messages = $this->provider();

        /**
         * @var string $key
         * @var Counter $message
         */
        foreach ($messages as $key => $message) {
            $inVal = floatval(rand(1000, 10000)/1000);
            $initialValue = new CounterValue();
            $initialValue->setValue(new CounterValueType1([$inVal]));
            $initialValue->setDate((new DateTimeImmutable())->modify('-1 month'));
            $initialValue->setCounter($message);
            $message->setInitialValue($initialValue);

            $manager->persist($initialValue);
            $manager->persist($message);

            $this->addReference($key, $message);
        }
        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array class-string[]
     */
    public function getDependencies()
    {
        return array(
            TransformerFixtures::class,
            CounterTypeFixtures::class,
            AgreementFixtures::class,
            AddressFixtures::class,
        );
    }
}
