<?php

namespace App\DataFixtures;

use App\Entity\Main\Substation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class SubstationFixtures extends Fixture
{
    public function provider()
    {
        return [
            'substation-1' => (new Substation())
                ->setTitle('ПС 110/10 кВ "Восточная"')
            ,
            'substation-2' => (new Substation())
                ->setTitle('ПС 35/10 кВ "Тепличный комбинат"')
            ,
            'substation-3' => (new Substation())
                ->setTitle('ПС 110/6 кВ "ЦПП"')
            ,
        ];
    }

    public function load(ObjectManager $manager)
    {
        $messages = $this->provider();

        foreach ($messages as $key => $message) {
            $manager->persist($message);

            $this->addReference($key, $message);
        }
        $manager->flush();
    }
}
