<?php


namespace App\EventListener;

use App\Model\Assembler\UserAssembler;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\Security\Core\User\UserInterface;
use JMS\Serializer\SerializerBuilder;

class AuthenticationSuccessListener
{
    /**
     * @param AuthenticationSuccessEvent $event
     */
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        $data = $event->getData();
        $user = $event->getUser();

        if (!$user instanceof UserInterface) {
            return;
        }

        $userDto = UserAssembler::writeDTO($user);

        $serializedUser = SerializerBuilder::create()->build()->serialize($userDto, 'json');
        $serUser = json_decode(trim($serializedUser, '\"'), true);

        $data['data'] = array(
            'user' => $serUser,
        );

        $event->setData($data);
    }
}