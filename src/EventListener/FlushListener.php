<?php

namespace App\EventListener;

use App\Entity\Main\BypassListRecord;
use App\Entity\Main\Company;
use App\Entity\Main\User;
use App\Handler\BypassListHandler\BypassListHandler;
use App\Repository\BypassListRecordRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\OnFlushEventArgs;

class FlushListener
{
    /**
     * @var BypassListHandler
     */
    private BypassListHandler $bypassListHandler;

    /**
     * @var BypassListRecordRepository
     */
    private BypassListRecordRepository $bypassListRecordRepository;

    public function __construct(
        BypassListHandler $bypassListHandler,
        BypassListRecordRepository $bypassListRecordRepository
    )
    {
        $this->bypassListHandler = $bypassListHandler;
        $this->bypassListRecordRepository = $bypassListRecordRepository;
    }

    /**
     * onFlush event handler
     * @param OnFlushEventArgs $eventArgs
     */
    public function onFlush(OnFlushEventArgs $eventArgs)
    {
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            if ($entity instanceof User) {
                $this->createUser($entity, $em);
            }
            if ($entity instanceof Company) {
                $this->createCompany($entity, $em);
            }
        }

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if ($entity instanceof User) {
                $changes = $uow->getEntityChangeSet($entity);
                $this->updateUser($entity, $changes, $em);
            }
            if ($entity instanceof Company) {
                $changes = $uow->getEntityChangeSet($entity);
                $this->updateCompany($entity, $changes, $em);
            }
        }

        foreach ($uow->getScheduledEntityDeletions() as $entity) {
            if ($entity instanceof User) {
                $this->deleteUser($entity, $em);
            }
            if ($entity instanceof Company) {
                $this->deleteCompany($entity, $em);
            }
        }
    }

    /**
     * User creation handler
     * @param User $user
     * @param EntityManagerInterface $em
     */
    private function createUser(User $user, EntityManagerInterface $em)
    {
        $currentUserName = $user->getSurname() . ' ' . $user->getName() . ' ' . $user->getPatronymic();

        /** @var \App\Entity\Main\BypassListRecord[] $bypassRecords */
        $bypassRecords = $this->bypassListRecordRepository->getBypassListRecordsByExecutors(
            [$currentUserName]
        );

        if (!empty($bypassRecords)) {
            $uow = $em->getUnitOfWork();

            foreach ($bypassRecords as $record) {
                $validatedRecord = $this->bypassListHandler->validateErrorsBypassListRecord(
                    $record,
                    $user,
                    null,
                    'userCreate'
                );
                $em->persist($validatedRecord);
                $uow->computeChangeSet($em->getClassMetadata(get_class($validatedRecord)), $validatedRecord);
            }
        }
    }

    /**
     * Company creation handler
     * @param \App\Entity\Main\Company $company
     * @param EntityManagerInterface $em
     */
    private function createCompany(Company $company, EntityManagerInterface $em)
    {
        /** @var \App\Entity\Main\BypassListRecord[] $bypassRecords */
        $bypassRecords = $this->bypassListRecordRepository->getBypassListRecordsByCompanies(
            [$company->getTitle()]
        );
        if (!empty($bypassRecords)) {
            $uow = $em->getUnitOfWork();

            foreach ($bypassRecords as $record) {
                $validatedRecord = $this->bypassListHandler->validateErrorsBypassListRecord(
                    $record,
                    null,
                    $company,
                    'companyCreate'
                );
                $em->persist($validatedRecord);
                $uow->computeChangeSet($em->getClassMetadata(get_class($validatedRecord)), $validatedRecord);
            }
        }
    }

    /**
     * User update handler
     * @param \App\Entity\Main\User $user
     * @param array $changes
     * @param EntityManagerInterface $em
     */
    private function updateUser(User $user, array $changes, EntityManagerInterface $em)
    {
        if (isset($changes['name']) || isset($changes['surname']) || isset($changes['patronymic']) || isset($changes['companies'])) {
            $name = $changes['name'][0] ?? $user->getName();
            $surname = $changes['surname'][0] ?? $user->getSurname();
            $patronymic = $changes['patronymic'][0] ?? $user->getPatronymic();

            $lastFullUserName = $surname . ' ' . $name . ' ' . $patronymic;
            $currentUserName = $user->getSurname() . ' ' . $user->getName() . ' ' . $user->getPatronymic();

            /** @var \App\Entity\Main\BypassListRecord[] $bypassRecords */
            $bypassRecords = $this->bypassListRecordRepository->getBypassListRecordsByExecutors(
                [$lastFullUserName, $currentUserName]
            );

            if (!empty($bypassRecords)) {
                $uow = $em->getUnitOfWork();

                foreach ($bypassRecords as $record) {
                    $validatedRecord = $this->bypassListHandler->validateErrorsBypassListRecord(
                        $record,
                        $user,
                        null,
                        'userUpdate'
                    );
                    $em->persist($validatedRecord);
                    $uow->computeChangeSet($em->getClassMetadata(get_class($validatedRecord)), $validatedRecord);
                }
            }
        }
    }

    /**
     * Company update handler
     * @param Company $company
     * @param array $changes
     * @param EntityManagerInterface $em
     */
    private function updateCompany(Company $company, array $changes, EntityManagerInterface $em)
    {
        if (isset($changes['title'])) {
            $lastCompanyTitle = $changes['title'][0];
            $currentCompanyTitle = $company->getTitle();

            /** @var \App\Entity\Main\BypassListRecord[] $bypassRecords */
            $bypassRecords = $this->bypassListRecordRepository->getBypassListRecordsByCompanies(
                [$lastCompanyTitle, $currentCompanyTitle]
            );

            if (!empty($bypassRecords)) {
                $uow = $em->getUnitOfWork();

                foreach ($bypassRecords as $record) {
                    $validatedRecord = $this->bypassListHandler->validateErrorsBypassListRecord(
                        $record,
                        null,
                        $company,
                        'companyUpdate'
                    );
                    $em->persist($validatedRecord);
                    $uow->computeChangeSet($em->getClassMetadata(get_class($validatedRecord)), $validatedRecord);
                }
            }
        }
    }

    /**
     * User delete handler
     * @param User $user
     * @param EntityManagerInterface $em
     */
    private function deleteUser(User $user, EntityManagerInterface $em)
    {
        $currentUserName = $user->getSurname() . ' ' . $user->getName() . ' ' . $user->getPatronymic();

        /** @var BypassListRecord[] $bypassRecords */
        $bypassRecords = $this->bypassListRecordRepository->getBypassListRecordsByExecutors(
            [$currentUserName]
        );

        if (!empty($bypassRecords)) {
            $uow = $em->getUnitOfWork();

            foreach ($bypassRecords as $record) {
                $validatedRecord = $this->bypassListHandler->validateErrorsBypassListRecord(
                    $record,
                    null,
                    null,
                    'userDelete'
                );
                $em->persist($validatedRecord);
                $uow->computeChangeSet($em->getClassMetadata(get_class($validatedRecord)), $validatedRecord);
            }
        }
    }

    /**
     * Company delete handler
     * @param \App\Entity\Main\Company $company
     * @param EntityManagerInterface $em
     */
    private function deleteCompany(Company $company, EntityManagerInterface $em)
    {
        $currentCompanyTitle = $company->getTitle();

        /** @var \App\Entity\Main\BypassListRecord[] $bypassRecords */
        $bypassRecords = $this->bypassListRecordRepository->getBypassListRecordsByCompanies(
            [$currentCompanyTitle]
        );

        if (!empty($bypassRecords)) {
            $uow = $em->getUnitOfWork();

            foreach ($bypassRecords as $record) {
                $validatedRecord = $this->bypassListHandler->validateErrorsBypassListRecord(
                    $record,
                    null,
                    null,
                    'companyDelete'
                );
                $em->persist($validatedRecord);
                $uow->computeChangeSet($em->getClassMetadata(get_class($validatedRecord)), $validatedRecord);
            }
        }
    }
}