<?php

namespace App\Form;

use App\Entity\Main\Company;
use App\Entity\Main\CounterType;
use App\Entity\Main\FailureFactor;
use App\Entity\Main\Feeder;
use App\Entity\Main\Subscriber;
use App\Entity\Main\Substation;
use App\Entity\Main\Task;
use App\Entity\Main\Transformer;
use App\Entity\Main\User;
use App\Handler\UserHandler\UserHandler;
use App\Model\DTO\TaskDTO;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TaskDTOType extends AbstractType
{
    protected $userHandler;

    public function __construct(
        UserHandler $userHandler
    )
    {
        $this->userHandler = $userHandler;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', TextType::class, [
                'required' => false
            ])
            ->add('taskType', ChoiceType::class, [
                'required' => false,
                'choices' => array_flip(Task::$task_type_value)
            ])
            ->add('executor', EntityType::class, [
                'class' => User::class,
                'choices' => $this->userHandler->getUsersByGroupName('Controllers')
            ])
            ->add('priority', ChoiceType::class, [
                'choices' => array_flip(Task::$priority_value)
            ])
            ->add('deadline', DateTimeType::class, [
                'required' => false,
                'widget' => 'single_text'
            ])
            ->add('status', ChoiceType::class, [
                'choices' => array_flip(Task::$status_value)
            ])
            ->add('coordinateX', NumberType::class, [
                'required' => false,
            ])
            ->add('coordinateY', NumberType::class, [
                'required' => false,
            ])
            ->add('operatorComment', TextType::class, [
                'required' => false
            ])
            ->add('controllerComment', TextType::class, [
                'required' => false
            ])
            ->add('failureFactors', EntityType::class, [
                'class' => FailureFactor::class,
                'required' => false,
                'multiple' => true
            ])
            ->add('actualizedFields', CollectionType::class, [
                'entry_type' => TextType::class,
                'allow_add' => true,
                'allow_delete' => true
            ])
            ->add('changedFields', CollectionType::class, [
                'entry_type' => TextType::class,
                'allow_add' => true,
                'allow_delete' => true
            ])
            ->add('company', EntityType::class, [
                'class' => Company::class
            ])
            ->add('locality', TextType::class, [
                'required' => false
            ])
            ->add('street', TextType::class, [
                'required' => false
            ])
            ->add('houseNumber', TextType::class, [
                'required' => false
            ])
            ->add('porchNumber', TextType::class, [
                'required' => false
            ])
            ->add('apartmentNumber', TextType::class, [
                'required' => false
            ])
            ->add('lodgersCount', IntegerType::class, [
                'required' => false
            ])
            ->add('roomsCount', IntegerType::class, [
                'required' => false
            ])
            ->add('subscriberType', ChoiceType::class, [
//                'required' => false,
//                'empty_data' => 0,
                'choices' => array_flip(Subscriber::$subscriber_type_value),
            ])
            ->add('subscriberTitle', TextType::class, [
                'required' => false
            ])
            ->add('subscriberName', TextType::class, [
                'required' => false
            ])
            ->add('subscriberSurname', TextType::class, [
                'required' => false
            ])
            ->add('subscriberPatronymic', TextType::class, [
                'required' => false
            ])
            ->add('subscriberPhones', CollectionType::class, [
                'entry_type' => TextType::class,
                'allow_add' => true,
                'allow_delete' => true
            ])
            ->add('agreementNumber', TextType::class, [
                'required' => false
            ])
            ->add('counterNumber', TextType::class, [
                'required' => false
            ])
            ->add('lastCounterValue', CollectionType::class, [
                'entry_type' => NumberType::class,
                'allow_add' => true,
                'allow_delete' => true
            ])
            ->add('currentCounterValue', CollectionType::class, [
                'entry_type' => NumberType::class,
                'allow_add' => true,
                'allow_delete' => true
            ])
            ->add('consumption', NumberType::class, [
                'required' => false
            ])
            ->add('consumptionCoefficient', NumberType::class, [
                'required' => false
            ])
            ->add('counterPhysicalStatus', ChoiceType::class, [
//                'required' => false,
//                'empty_data' => 0,
                'choices' => array_flip(Task::$counter_physical_status_value)
            ])
            ->add('substation', EntityType::class, [
                'class' => Substation::class
            ])
            ->add('feeder', EntityType::class, [
                'class' => Feeder::class
            ])
            ->add('transformer', EntityType::class, [
                'class' => Transformer::class
            ])
            ->add('lineNumber', TextType::class, [
                'required' => false
            ])
            ->add('electricPoleNumber', TextType::class, [
                'required' => false
            ])
            ->add('counterType', EntityType::class, [
                'class' => CounterType::class
            ])
            ->add('terminalSeal', TextType::class, [
                'required' => false
            ])
            ->add('antiMagneticSeal', TextType::class, [
                'required' => false
            ])
            ->add('sideSeal', TextType::class, [
                'required' => false
            ])
            ->add('photos', CollectionType::class, [
                'entry_type' => TextType::class,
                'allow_add' => true,
                'allow_delete' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TaskDTO::class,
            'label_format' => 'form.row.%name%',
            'translation_domain' => 'task',
            'csrf_protection' => false,
            'allow_extra_fields' => true
        ]);
    }

    private function getUserControllers()
    {
        $this->userHandler->getUsersByGroupName('Admins');
    }
}
