<?php

namespace App\Form;

use App\Model\SubscriberIndividualForm;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SubscriberIndividualFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('surname', TextType::class, [
                'required' => true
            ])
            ->add('name', TextType::class, [
                'required' => true
            ])
            ->add('patronymic', TextType::class, [
                'required' => false
            ])
            ->add('address', TextType::class, [
                'required' => false
            ])
            ->add('phones', CollectionType::class, [
                'entry_type' => TextType::class,
                'allow_add' => true,
                'allow_delete' => true
            ])
            ->add('email', TextType::class, [
                'required' => false
            ])
            ->add('coordinateX', NumberType::class, [
                'required' => false
            ])
            ->add('coordinateY', NumberType::class, [
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SubscriberIndividualForm::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true
        ]);
    }
}
