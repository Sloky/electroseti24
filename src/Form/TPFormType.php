<?php

namespace App\Form;

use App\Entity\Main\Company;
use App\Entity\Main\User;
use App\Model\TPForm;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TPFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('transformer', TextType::class, [
                'required' => true
            ])
            ->add('substation', TextType::class)
            ->add('feeder', TextType::class)
            ->add('executors', EntityType::class, [
                'class' => User::class,
                'multiple' => true,
                'expanded' => true
            ])
            ->add('company', EntityType::class, [
                'class' => Company::class,
                'required' => true
            ])
            ->add('coordinateX', NumberType::class, [
                'required' => false,
            ])
            ->add('coordinateY', NumberType::class, [
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TPForm::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true
        ]);
    }
}
