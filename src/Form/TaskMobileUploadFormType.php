<?php

namespace App\Form;

use App\Entity\Main\Task;
use App\Model\TaskMobileUploadForm;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TaskMobileUploadFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('taskId', TextType::class, [
                'required' => false
            ])
            ->add('status', ChoiceType::class, [
                'choices' => array_flip(Task::$status_value)
            ])
            ->add('counterPhysicalStatus', ChoiceType::class, [
                'choices' => array_flip(Task::$counter_physical_status_value)
            ])
            ->add('counterValue', NumberType::class, [
                'required' => false
            ])
            ->add('counterValuePhoto', FileType::class, [
                'required' => false
            ])
            ->add('counterPhoto', FileType::class, [
                'required' => false
            ])
            ->add('counterNumber', TextType::class, [
                'required' => false
            ])
            ->add('counterTypeString', TextType::class, [
                'required' => false
            ])
            ->add('terminalSeal', TextType::class, [
                'required' => false
            ])
            ->add('terminalSealPhoto', FileType::class, [
                'required' => false
            ])
            ->add('antimagneticSeal', TextType::class, [
                'required' => false
            ])
            ->add('antimagneticSealPhoto', FileType::class, [
                'required' => false
            ])
            ->add('sideSeal', TextType::class, [
                'required' => false
            ])
            ->add('sideSealPhoto', FileType::class, [
                'required' => false
            ])
            ->add('lodgersCount', IntegerType::class, [
                'required' => false
            ])
            ->add('roomsCount', IntegerType::class, [
                'required' => false
            ])
            ->add('controllerComment', TextType::class, [
                'required' => false
            ])
            ->add('executedDateTime',DateTimeType::class, [
                'required' => false,
                'widget' => 'single_text'
            ])
            ->add('GPSLongitude', NumberType::class, [
                'required' => false
            ])
            ->add('GPSLatitude', NumberType::class, [
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TaskMobileUploadForm::class,
//            'csrf_protection' => false,
//            'allow_extra_fields' => true
        ]);
    }
}
