<?php

namespace App\Form;

use App\Entity\Main\Transformer;
use App\Model\CounterCardTPForm;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CounterCardTPFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('transformer', EntityType::class, [
                'required' => true,
                'class' => Transformer::class
            ])
            ->add('counterTitle', TextType::class, [
                'required' => true
            ])
            ->add('counterPhysicalStatus', IntegerType::class, [
                'required' => true
            ])
            ->add('counterModel', TextType::class, [
                'required' => true
            ])
            ->add('terminalSeal', TextType::class, [
                'required' => false
            ])
            ->add('antimagneticSeal', TextType::class, [
                'required' => false
            ])
            ->add('sideSeal', TextType::class, [
                'required' => false
            ])
            ->add('address', TextType::class, [
                'required' => false
            ])
            ->add('coordinateX', NumberType::class, [
                'required' => false
            ])
            ->add('coordinateY', NumberType::class, [
                'required' => false
            ])
            ->add('initialCounterValueId', TextType::class, [
                'required' => false
            ])
            ->add('initialCounterValue', NumberType::class, [
                'required' => false
            ])
            ->add('initialCounterValueDate', DateTimeType::class, [
                'required' => false,
                'widget' => 'single_text'
            ])
            ->add('lastCounterValueId', TextType::class, [
                'required' => false
            ])
            ->add('lastCounterValue', NumberType::class, [
                'required' => false
            ])
            ->add('lastCounterValueDate', DateTimeType::class, [
                'required' => false,
                'widget' => 'single_text'
            ])
            ->add('subscriberTitle', TextType::class, [
                'required' => false
            ])
            ->add('agreementNumber', TextType::class, [
                'required' => false
            ])
            ->add('lodgersCount', IntegerType::class, [
                'required' => false
            ])
            ->add('roomsCount', IntegerType::class, [
                'required' => false
            ])
            ->add('phones', CollectionType::class, [
                'entry_type' => TextType::class,
                'allow_add' => true,
                'allow_delete' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CounterCardTPForm::class,
            'allow_extra_fields' => true,
            'csrf_protection' => false,
        ]);
    }
}
