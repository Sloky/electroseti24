<?php

namespace App\Form;

use App\Entity\Main\Company;
use App\Entity\Main\UserGroup;
use App\Model\UserForm;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class)
            ->add('roles', CollectionType::class, [
                'entry_type' => TextType::class,
                'entry_options' => [
                    'label' => false
                ],
                'allow_add' => true,
                'allow_delete' => true,
            ])
            ->add('name', TextType::class)
            ->add('surname', TextType::class)
            ->add('patronymic', TextType::class)
            ->add('phone', TextType::class)
            ->add('address', TextType::class)
            ->add('work_status', NumberType::class)
            ->add('companies', EntityType::class, [
                'class' => Company::class,
                'choice_value' => "id",
//                'choice_label' => "title",
                'multiple' => true,
                'expanded' => true
            ])
            ->add('password', TextType::class)
            ->add('groups', EntityType::class, [
                'class' => UserGroup::class,
                'choice_value' => "id",
                'choice_label' => "group_name",
                'multiple' => true,
                'expanded' => true
            ])
//            ->add('groups', CollectionType::class, [
//                'empty_data' => null,
//                'required' => false,
//                'entry_type' => EntityType::class,
//                'entry_options' => [
//                    'class' => UserGroup::class
//                ],
//                'allow_add' => true,
//                'allow_delete' => true,
//            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserForm::class,
            'allow_extra_fields' => true
        ]);
    }
}
