<?php

namespace App\Form;

use App\Entity\Main\Company;
use App\Entity\Main\Subscriber;
use App\Model\AgreementForm;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AgreementFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('number', TextType::class, [
                'required' => true
            ])
            ->add('paymentCode', TextType::class, [
                'required' => false
            ])
            ->add('company', EntityType::class, [
                'class' => Company::class,
                'required' => true
            ])
            ->add('subscriber', EntityType::class, [
                'class' => Subscriber::class,
                'required' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AgreementForm::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true
        ]);
    }
}
