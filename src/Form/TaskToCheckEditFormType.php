<?php

namespace App\Form;

use App\Entity\Main\CounterType;
use App\Entity\Main\Task;
use App\Model\TaskToCheckEditForm;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TaskToCheckEditFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('counterPhysicalStatus', ChoiceType::class, [
                'required' => false,
                'empty_data' => null,
                'choices' => array_flip(Task::$counter_physical_status_value)
            ])
            ->add('newCounterValue', CollectionType::class, [
                'required' => false,
                'empty_data' => null,
                'entry_type' => NumberType::class,
                'allow_add' => true,
                'allow_delete' => true
            ])
            ->add('newCounterNumber', TextType::class, [
                'required' => false
            ])
            ->add('newCounterType', EntityType::class, [
                'required' => false,
                'empty_data' => null,
                'class' => CounterType::class
            ])
            ->add('newTerminalSeal', TextType::class, [
                'required' => false
            ])
            ->add('newAntimagneticSeal', TextType::class, [
                'required' => false
            ])
            ->add('newSideSeal', TextType::class, [
                'required' => false
            ])
            ->add('locality', TextType::class, [
                'required' => false
            ])
            ->add('street', TextType::class, [
                'required' => false
            ])
            ->add('houseNumber', TextType::class, [
                'required' => false
            ])
            ->add('porchNumber', TextType::class, [
                'required' => false
            ])
            ->add('apartmentNumber', TextType::class, [
                'required' => false
            ])
            ->add('lodgersCount', IntegerType::class, [
                'required' => false
            ])
            ->add('roomsCount', IntegerType::class, [
                'required' => false
            ])
            ->add('operatorComment', TextType::class, [
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TaskToCheckEditForm::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true
        ]);
    }
}
