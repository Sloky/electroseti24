<?php

namespace App\Form;

use App\Entity\Main\BypassListRecord;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BypassListEditFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//            ->add('rowPosition')
//            ->add('fileName')
//            ->add('status')
//            ->add('period')
            ->add('agreementNumber', TextType::class, [
                'required' => false
            ])
            ->add('address', TextType::class, [
                'required' => false
            ])
            ->add('subscriberTitle', TextType::class, [
                'required' => false
            ])
            ->add('subscriberType', TextType::class, [
                'required' => false
            ])
            ->add('phones', TextType::class, [
                'required' => false
            ])
            ->add('companyTitle', TextType::class, [
                'required' => false
            ])
            ->add('counterNumber', TextType::class, [
                'required' => false
            ])
            ->add('counterModel', TextType::class, [
                'required' => false
            ])
            ->add('serviceability', TextType::class, [
                'required' => false
            ])
            ->add('currentCounterValue', TextType::class, [
                'required' => false
            ])
            ->add('currentCounterValueDate', TextType::class, [
                'required' => false
            ])
            ->add('sideSeal', TextType::class, [
                'required' => false
            ])
            ->add('terminalSeal', TextType::class, [
                'required' => false
            ])
            ->add('antimagneticSeal', TextType::class, [
                'required' => false
            ])
            ->add('substation', TextType::class, [
                'required' => false
            ])
            ->add('feeder', TextType::class, [
                'required' => false
            ])
            ->add('transformer', TextType::class, [
                'required' => false
            ])
            ->add('electricLine', TextType::class, [
                'required' => false
            ])
            ->add('electricPole', TextType::class, [
                'required' => false
            ])
            ->add('coords', TextType::class, [
                'required' => false
            ])
            ->add('controller', TextType::class, [
                'required' => false
            ])
            ->add('deadline', TextType::class, [
                'required' => false
            ])
            ->add('roomsCount', TextType::class, [
                'required' => false
            ])
            ->add('lodgersCount', TextType::class, [
                'required' => false
            ])
            ->add('counterInitialDate', TextType::class, [
                'required' => false
            ])
            ->add('counterInitialValue', TextType::class, [
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BypassListRecord::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true
        ]);
    }
}
