<?php

namespace App\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class BypassRecordsMultiEditForm
{
    /**
     * @var Collection|null
     */
    private $items;

    /**
     * @var string|null
     */
    private $deadline;

    /**
     * @var string|null
     */
    private $executor;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    /**
     * @return Collection|null
     */
    public function getItems(): ?Collection
    {
        return $this->items;
    }

    /**
     * @param string $item
     */
    public function addItem(string $item)
    {
        $this->items->add($item);
    }

    /**
     * @param string $item
     */
    public function removeItem(string $item)
    {
        $this->items->removeElement($item);
    }

    /**
     * @param Collection|null $items
     * @return BypassRecordsMultiEditForm
     */
    public function setItems(?Collection $items): BypassRecordsMultiEditForm
    {
        $this->items = $items;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDeadline(): ?string
    {
        return $this->deadline;
    }

    /**
     * @param string|null $deadline
     * @return BypassRecordsMultiEditForm
     */
    public function setDeadline(?string $deadline): BypassRecordsMultiEditForm
    {
        $this->deadline = $deadline;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getExecutor(): ?string
    {
        return $this->executor;
    }

    /**
     * @param string|null $executor
     * @return BypassRecordsMultiEditForm
     */
    public function setExecutor(?string $executor): BypassRecordsMultiEditForm
    {
        $this->executor = $executor;
        return $this;
    }
}