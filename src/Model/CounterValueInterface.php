<?php


namespace App\Model;


interface CounterValueInterface
{
    /**
     * Установить показания в виде массива (ключи массива - тарифные зоны)
     * @param array $values
     */
    public function setValues(array $values);

    /**
     * Получить значения показаний всех зон
     * @return array
     */
    public function getValues(): array;

    /**
     * Получить сумму всех значений показаний
     * @return float
     */
    public function getValuesSum(): float;
}