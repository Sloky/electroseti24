<?php

namespace App\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class SubscriberIndividualForm
{
    /**
     * @var string|null
     */
    private ?string $surname = null;

    /**
     * @var string|null
     */
    private ?string $name = null;

    /**
     * @var string|null
     */
    private ?string $patronymic = null;

    /**
     * @var string|null
     */
    private ?string $address = null;

    /**
     * @var Collection|ArrayCollection
     */
    private Collection $phones;

    /**
     * @var string|null
     */
    private ?string $email = null;

    /**
     * @var float|null
     */
    private ?float $coordinateX = null;

    /**
     * @var float|null
     */
    private ?float $coordinateY = null;

    public function __construct()
    {
        $this->phones = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getSurname(): ?string
    {
        return $this->surname;
    }

    /**
     * @param string|null $surname
     * @return SubscriberIndividualForm
     */
    public function setSurname(?string $surname): SubscriberIndividualForm
    {
        $this->surname = $surname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return SubscriberIndividualForm
     */
    public function setName(?string $name): SubscriberIndividualForm
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPatronymic(): ?string
    {
        return $this->patronymic;
    }

    /**
     * @param string|null $patronymic
     * @return SubscriberIndividualForm
     */
    public function setPatronymic(?string $patronymic): SubscriberIndividualForm
    {
        $this->patronymic = $patronymic;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     * @return SubscriberIndividualForm
     */
    public function setAddress(?string $address): SubscriberIndividualForm
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getPhones(): Collection
    {
        return $this->phones;
    }

    /**
     * @param Collection $phones
     * @return SubscriberIndividualForm
     */
    public function setPhones(Collection $phones): SubscriberIndividualForm
    {
        $this->phones = $phones;
        return $this;
    }

    /**
     * @param string $phone
     * @return $this
     */
    public function addPhone(string $phone): SubscriberIndividualForm
    {
        $this->phones->add($phone);
        return $this;
    }

    /**
     * @param string $phone
     * @return $this
     */
    public function removePhone(string $phone): SubscriberIndividualForm
    {
        $this->phones->removeElement($phone);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return SubscriberIndividualForm
     */
    public function setEmail(?string $email): SubscriberIndividualForm
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getCoordinateX(): ?float
    {
        return $this->coordinateX;
    }

    /**
     * @param float|null $coordinateX
     * @return SubscriberIndividualForm
     */
    public function setCoordinateX(?float $coordinateX): SubscriberIndividualForm
    {
        $this->coordinateX = $coordinateX;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getCoordinateY(): ?float
    {
        return $this->coordinateY;
    }

    /**
     * @param float|null $coordinateY
     * @return SubscriberIndividualForm
     */
    public function setCoordinateY(?float $coordinateY): SubscriberIndividualForm
    {
        $this->coordinateY = $coordinateY;
        return $this;
    }
}