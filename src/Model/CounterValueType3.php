<?php


namespace App\Model;

use DateTimeImmutable;
use DateTimeInterface;
use Exception;

/**
 * Class CounterValueType3
 *
 * Этот класс описывает модель показаний расхода почасовок счетчиков 3 и 4 ценовых категорий.
 * Счетчики данной ценовой снимают показания каждого часа месяца.
 *
 * @package App\Model
 */
class CounterValueType3 implements CounterValueInterface
{
    /**
     * Показания[день месяца(число с 1 по 30/31)][номер часа(0-23)]
     * @var array
     */
    protected $values;

    /**
     * @var float
     */
    protected $valuesSum;

    /**
     * CounterValueConsumption constructor.
     * @param array $values
     * @param DateTimeInterface $date
     * @throws Exception
     */
    public function __construct(array $values = [], DateTimeInterface $date = null)
    {
        $this->values = $this->formattedValues($values, $date);
        $this->valuesSum = array_sum($values);
    }

    /**
     * @param array $values
     * @param DateTimeInterface $date
     * @return void
     * @throws Exception
     */
    public function setValues(array $values, DateTimeInterface $date = null)
    {
        $this->values = $this->formattedValues($values, $date);
        $this->valuesSum = array_sum($values);
    }

    /**
     * Получить значения показаний всех зон
     * @return array
     */
    public function getValues(): array
    {
        return $this->values;
    }

    /**
     * Получить сумму всех значений показаний
     * @return float
     */
    public function getValuesSum(): float
    {
        return $this->valuesSum;
    }

    /**
     * Преобразование массива с показаниями в массив Показания[день месяца(число с 1 по 30/31)][номер часа(0-23)]
     * @param array $input_array
     * @param DateTimeInterface $date
     * @return array
     * @throws Exception
     */
    public function formattedValues(array $input_array, DateTimeInterface $date = null): array
    {
        if (is_null($date)) {
            $date = new DateTimeImmutable();
        }
        $countDayOfMonth = date('t', $date->getTimestamp());
        $output_array = [];
        $count = count($input_array);
        $counter = 0;
        for ($d = 1; $d <= $countDayOfMonth; $d++) {
            for ($h = 0; $h <= 23; $h++) {
                $output_array[$d][$h] = $input_array[$counter++] ?? 0;
            }
        }
        return $output_array;
    }
}