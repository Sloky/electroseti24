<?php


namespace App\Model;

/**
 * Class CounterValueType2
 *
 * Этот класс описывает модель показаний счетчиков 2 ценовой категории.
 * Счетчики данной ценовой категории снимают показания 2-х (день/ночь)
 * или 3-х (пик/полупик/ночь) тарифных зон.
 *
 * @package App\Model
 */
class CounterValueType2 implements CounterValueInterface
{
    /**
     * Показания[номер зоны]
     * @var array
     */
    protected $values;

    /**
     * Сумма показаний
     * @var float
     */
    protected $valuesSum;

    /**
     * Показания день|пик
     * @var float
     */
    protected $value1;

    /**
     * Показания ночь
     * @var float
     */
    protected $value2;

    /**
     * Показания полупик
     * @var float
     */
    protected $value3;

    /**
     * CounterValueType2 constructor.
     * @param array $values
     */
    public function __construct(array $values = [])
    {
        $this->values = $values;
        $this->valuesSum = array_sum($values);
        $this->value1 = $values[0];
        $this->value2 = $values[1];
        $this->value3 = $values[2] ?? null;
    }

    /**
     * @param array $values
     * @return CounterValueType2
     */
    public function setValues(array $values): CounterValueType2
    {
        $this->values = $values;
        $this->valuesSum = array_sum($values);
        $this->value1 = $values[0];
        $this->value2 = $values[1];
        $this->value3 = $values[2] ?? null;

        return $this;
    }

    /**
     * Получить значения показаний всех зон
     * @return array
     */
    public function getValues(): array
    {
        return $this->values;
    }

    /**
     * @return float
     */
    public function getValue1(): float
    {
        return $this->value1;
    }

    /**
     * @param float $value1
     * @return CounterValueType2
     */
    public function setValue1(float $value1): CounterValueType2
    {
        $this->value1 = $value1;
        return $this;
    }

    /**
     * @return float
     */
    public function getValue2(): float
    {
        return $this->value2;
    }

    /**
     * @param float $value2
     * @return CounterValueType2
     */
    public function setValue2(float $value2): CounterValueType2
    {
        $this->value2 = $value2;
        return $this;
    }

    /**
     * @return float
     */
    public function getValue3(): float
    {
        return $this->value3;
    }

    /**
     * @param float $value3
     * @return CounterValueType2
     */
    public function setValue3(float $value3): CounterValueType2
    {
        $this->value3 = $value3;
        return $this;
    }

    /**
     * Получить сумму всех значений показаний
     * @return float
     */
    public function getValuesSum(): float
    {
        return $this->valuesSum;
    }
}