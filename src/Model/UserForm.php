<?php

namespace App\Model;

use App\Entity\Main\Company;
use App\Entity\Main\UserGroup;
use Doctrine\Common\Collections\ArrayCollection;

class UserForm
{
    /**
     * @var string
     */
    protected $username;

    /**
     * @var string|null
     */
    protected $password;

    /**
     * @var ArrayCollection
     */
    protected $roles;

    /**
     * @var ArrayCollection
     */
    protected $groups;

    /**
     * @var string|null
     */
    protected $name;

    /**
     * @var string|null
     */
    protected $surname;

    /**
     * @var string|null
     */
    protected $patronymic;

    /**
     * @var string|null
     */
    protected $phone;

    /**
     * @var string|null
     */
    protected $address;

    /**
     * @var integer|null
     */
    protected $workStatus;

    /**
     * @var ArrayCollection
     */
    protected $companies;

    public function __construct()
    {
        $this->roles = new ArrayCollection();
        $this->groups = new ArrayCollection();
        $this->companies = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return UserForm
     */
    public function setUsername(string $username): UserForm
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     * @return UserForm
     */
    public function setPassword(?string $password): UserForm
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getRoles(): ArrayCollection
    {
        return $this->roles;
    }

    /**
     * @param ArrayCollection $roles
     * @return UserForm
     */
//    public function setRoles(ArrayCollection $roles): UserForm
//    {
//        $this->roles = $roles;
//        return $this;
//    }

    /**
     * @param $roles
     * @return UserForm
     */
    public function removeRole($roles): UserForm
    {
        $this->roles->removeElement($roles);
        return $this;
    }

    /**
     * @param string $role
     * @return UserForm
     */
    public function addRole(string $role): UserForm
    {
        $this->roles->add($role);
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getGroups(): ArrayCollection
    {
        return $this->groups;
    }

    /**
     * @param ArrayCollection $groups
     * @return UserForm
     */
    public function setGroups(ArrayCollection $groups): UserForm
    {
        $this->groups = $groups;
        return $this;
    }

    /**
     * @param UserGroup $group
     * @return UserForm
     */
    public function addGroup(UserGroup $group): UserForm
    {
        $this->groups->add($group);
        return $this;
    }

    /**
     * @param UserGroup $group
     * @return UserForm
     */
    public function removeGroup(UserGroup $group): UserForm
    {
        $this->groups->removeElement($group);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return UserForm
     */
    public function setName(?string $name): UserForm
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSurname(): ?string
    {
        return $this->surname;
    }

    /**
     * @param string|null $surname
     * @return UserForm
     */
    public function setSurname(?string $surname): UserForm
    {
        $this->surname = $surname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPatronymic(): ?string
    {
        return $this->patronymic;
    }

    /**
     * @param string|null $patronymic
     * @return UserForm
     */
    public function setPatronymic(?string $patronymic): UserForm
    {
        $this->patronymic = $patronymic;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     * @return UserForm
     */
    public function setPhone(?string $phone): UserForm
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     * @return UserForm
     */
    public function setAddress(?string $address): UserForm
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getWorkStatus(): ?int
    {
        return $this->workStatus;
    }

    /**
     * @param int|null $workStatus
     * @return UserForm
     */
    public function setWorkStatus(?int $workStatus): UserForm
    {
        $this->workStatus = $workStatus;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getCompanies(): ArrayCollection
    {
        return $this->companies;
    }

    /**
     * @param ArrayCollection $companies
     * @return UserForm
     */
    public function setCompanies(ArrayCollection $companies): UserForm
    {
        $this->companies = $companies;
        return $this;
    }

    /**
     * @param \App\Entity\Main\Company $company
     * @return $this
     */
    public function addCompany(Company $company): UserForm
    {
        $this->companies->add($company);
        return $this;
    }

    /**
     * @param Company $company
     * @return $this
     */
    public function removeCompany(Company $company): UserForm
    {
        $this->companies->removeElement($company);
        return $this;
    }
}