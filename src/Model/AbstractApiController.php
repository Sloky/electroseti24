<?php

namespace App\Model;

use App\Handler\AgreementHandler;
use App\Handler\BypassListHandler\BypassListHandler;
use App\Handler\BypassListHandler\BypassListLoadingHandler;
use App\Handler\BypassListHandler\BypassListTaskCreationHandler;
use App\Handler\CompanyHandler;
use App\Handler\CounterHandler;
use App\Handler\PhotoHandler;
use App\Handler\SubscriberHandler;
use App\Handler\TaskHandler\CompletedTaskHandler;
use App\Handler\TaskHandler\DeleteTaskHandler;
use App\Handler\TaskHandler\ExcelTaskListHandler;
use App\Handler\TaskHandler\TaskHandler;
use App\Handler\TaskHandler\TaskToCheckHandler;
use App\Handler\TaskHandler\TaskToWorkHandler;
use App\Handler\TaskHandler\UploadPhotosZipHandler;
use App\Handler\TaskHandler\UploadTaskFromMobileAppHandler;
use App\Handler\TransformerHandler\TransformerHandler;
use App\Handler\UserGroupHandler;
use App\Handler\UserHandler\ExcelUserStatisticHandler;
use App\Handler\UserHandler\UserHandler;
use App\Handler\UserHandler\UserProfileHandler;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

abstract class AbstractApiController extends AbstractFOSRestController
{
    /**
     * @var EntityManagerInterface
     */
    protected EntityManagerInterface $em;

    /**
     * @var UserPasswordEncoderInterface
     */
    protected UserPasswordEncoderInterface $passwordEncoder;

    /**
     * @var UserHandler
     */
    protected UserHandler $userHandler;

    /**
     * @var UserProfileHandler
     */
    protected UserProfileHandler $userProfileHandler;

    /**
     * @var UserGroupHandler
     */
    protected UserGroupHandler $userGroupHandler;

    /**
     * @var TaskHandler
     */
    protected TaskHandler $taskHandler;

    /**
     * @var TaskToWorkHandler
     */
    protected TaskToWorkHandler $taskToWorkHandler;

    /**
     * @var TaskToCheckHandler
     */
    protected TaskToCheckHandler $taskToCheckHandler;

    /**
     * @var CompletedTaskHandler
     */
    protected CompletedTaskHandler $completedTaskHandler;

    /**
     * @var ExcelTaskListHandler
     */
    protected ExcelTaskListHandler $excelTaskListHandler;

    /**
     * @var DeleteTaskHandler
     */
    protected DeleteTaskHandler $deleteTaskHandler;

    /**
     * @var UploadTaskFromMobileAppHandler
     */
    protected UploadTaskFromMobileAppHandler $uploadTaskFromMobileAppHandler;

    /**
     * @var UploadPhotosZipHandler
     */
    protected UploadPhotosZipHandler $uploadPhotosZipHandler;

    /**
     * @var CompanyHandler
     */
    protected CompanyHandler $companyHandler;

    /**
     * @var CounterHandler
     */
    protected CounterHandler $counterHandler;

    /**
     * @var PhotoHandler
     */
    protected PhotoHandler $photoHandler;

    /**
     * @var BypassListHandler
     */
    protected BypassListHandler $bypassListHandler;

    /**
     * @var BypassListLoadingHandler
     */
    protected BypassListLoadingHandler $bypassListLoadingHandler;

    /**
     * @var BypassListTaskCreationHandler
     */
    protected BypassListTaskCreationHandler $bypassListTaskCreationHandler;

    /**
     * @var ExcelUserStatisticHandler
     */
    protected ExcelUserStatisticHandler $excelUserStatisticHandler;

    /**
     * @var TransformerHandler
     */
    protected TransformerHandler $transformerHandler;

    /**
     * @var SubscriberHandler
     */
    protected SubscriberHandler $subscriberHandler;

    /**
     * @var AgreementHandler
     */
    protected AgreementHandler $agreementHandler;

    /**
     * AbstractApiController constructor.
     * @param EntityManagerInterface $entityManager
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param UserHandler $userHandler
     * @param UserGroupHandler $userGroupHandler
     * @param TaskHandler $taskHandler
     * @param CompanyHandler $companyHandler
     * @param CounterHandler $counterHandler
     * @param PhotoHandler $photoHandler
     * @param TaskToWorkHandler $taskToWorkHandler
     * @param TaskToCheckHandler $taskToCheckHandler
     * @param CompletedTaskHandler $completedTaskHandler
     * @param ExcelTaskListHandler $excelTaskListHandler
     * @param DeleteTaskHandler $deleteTaskHandler
     * @param UploadTaskFromMobileAppHandler $uploadTaskFromMobileAppHandler
     * @param UploadPhotosZipHandler $uploadPhotosZipHandler
     * @param BypassListHandler $bypassListHandler
     * @param BypassListLoadingHandler $bypassListLoadingHandler
     * @param BypassListTaskCreationHandler $bypassListTaskCreationHandler
     * @param UserProfileHandler $userProfileHandler
     * @param ExcelUserStatisticHandler $excelUserStatisticHandler
     * @param TransformerHandler $transformerHandler
     * @param SubscriberHandler $subscriberHandler
     * @param AgreementHandler $agreementHandler
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $passwordEncoder,
        UserHandler $userHandler,
        UserGroupHandler $userGroupHandler,
        TaskHandler $taskHandler,
        CompanyHandler $companyHandler,
        CounterHandler $counterHandler,
        PhotoHandler $photoHandler,
        TaskToWorkHandler $taskToWorkHandler,
        TaskToCheckHandler $taskToCheckHandler,
        CompletedTaskHandler $completedTaskHandler,
        ExcelTaskListHandler $excelTaskListHandler,
        DeleteTaskHandler $deleteTaskHandler,
        UploadTaskFromMobileAppHandler $uploadTaskFromMobileAppHandler,
        UploadPhotosZipHandler $uploadPhotosZipHandler,
        BypassListHandler $bypassListHandler,
        BypassListLoadingHandler $bypassListLoadingHandler,
        BypassListTaskCreationHandler $bypassListTaskCreationHandler,
        UserProfileHandler $userProfileHandler,
        ExcelUserStatisticHandler $excelUserStatisticHandler,
        TransformerHandler $transformerHandler,
        SubscriberHandler $subscriberHandler,
        AgreementHandler $agreementHandler
    )
    {
        $this->em = $entityManager;
        $this->userHandler = $userHandler;
        $this->passwordEncoder = $passwordEncoder;
        $this->userGroupHandler = $userGroupHandler;
        $this->taskHandler = $taskHandler;
        $this->companyHandler = $companyHandler;
        $this->counterHandler = $counterHandler;
        $this->photoHandler = $photoHandler;
        $this->taskToWorkHandler = $taskToWorkHandler;
        $this->taskToCheckHandler = $taskToCheckHandler;
        $this->completedTaskHandler= $completedTaskHandler;
        $this->excelTaskListHandler = $excelTaskListHandler;
        $this->deleteTaskHandler = $deleteTaskHandler;
        $this->uploadTaskFromMobileAppHandler = $uploadTaskFromMobileAppHandler;
        $this->uploadPhotosZipHandler = $uploadPhotosZipHandler;
        $this->bypassListHandler = $bypassListHandler;
        $this->bypassListLoadingHandler = $bypassListLoadingHandler;
        $this->bypassListTaskCreationHandler = $bypassListTaskCreationHandler;
        $this->userProfileHandler = $userProfileHandler;
        $this->excelUserStatisticHandler = $excelUserStatisticHandler;
        $this->transformerHandler = $transformerHandler;
        $this->subscriberHandler = $subscriberHandler;
        $this->agreementHandler = $agreementHandler;
    }
}