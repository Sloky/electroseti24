<?php


namespace App\Model;


use App\Entity\Main\Company;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class TaskListUploadForm
{
    /**
     * @var UploadedFile
     */
    protected $file;

    /**
     * @var Company|null
     */
    protected $company;

    /**
     * @return UploadedFile|null
     */
    public function getFile(): ?UploadedFile
    {
        return $this->file;
    }

    /**
     * @param UploadedFile|null $file
     * @return TaskListUploadForm
     */
    public function setFile(?UploadedFile $file): TaskListUploadForm
    {
        $this->file = $file;
        return $this;
    }

    /**
     * @return Company|null
     */
    public function getCompany(): ?Company
    {
        return $this->company;
    }

    /**
     * @param Company|null $company
     * @return TaskListUploadForm
     */
    public function setCompany(?Company $company): TaskListUploadForm
    {
        $this->company = $company;
        return $this;
    }
}