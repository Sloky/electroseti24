<?php

namespace App\Model;

use App\Entity\Main\Company;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Security\Core\User\UserInterface;

class TPForm
{
    /**
     * @var string|null
     */
    private ?string $transformer = null;

    /**
     * @var string|null
     */
    private ?string $substation = null;

    /**
     * @var string|null
     */
    private ?string $feeder = null;

    /**
     * @var Company|null
     */
    private ?Company $company = null;

    /**
     * @var Collection|ArrayCollection
     */
    private Collection $executors;

    /**
     * @var float|null
     */
    private ?float $coordinateX = null;

    /**
     * @var float|null
     */
    private ?float $coordinateY = null;

    public function __construct()
    {
        $this->executors = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getTransformer(): ?string
    {
        return $this->transformer;
    }

    /**
     * @param string|null $transformer
     * @return TPForm
     */
    public function setTransformer(?string $transformer): TPForm
    {
        $this->transformer = $transformer;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubstation(): ?string
    {
        return $this->substation;
    }

    /**
     * @param string|null $substation
     * @return TPForm
     */
    public function setSubstation(?string $substation): TPForm
    {
        $this->substation = $substation;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFeeder(): ?string
    {
        return $this->feeder;
    }

    /**
     * @param string|null $feeder
     * @return TPForm
     */
    public function setFeeder(?string $feeder): TPForm
    {
        $this->feeder = $feeder;
        return $this;
    }

    /**
     * @return \App\Entity\Main\Company|null
     */
    public function getCompany(): ?Company
    {
        return $this->company;
    }

    /**
     * @param Company|null $company
     * @return TPForm
     */
    public function setCompany(?Company $company): TPForm
    {
        $this->company = $company;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getExecutors(): Collection
    {
        return $this->executors;
    }

    /**
     * @param Collection $executors
     * @return TPForm
     */
    public function setExecutors(Collection $executors): TPForm
    {
        $this->executors = $executors;
        return $this;
    }

    /**
     * @param UserInterface $user
     * @return $this
     */
    public function addExecutor(UserInterface $user): TPForm
    {
        $this->executors->add($user);
        return $this;
    }

    /**
     * @param UserInterface $user
     * @return $this
     */
    public function removeExecutor(UserInterface $user): TPForm
    {
        $this->executors->removeElement($user);
        return $this;
    }

    /**
     * @return float|null
     */
    public function getCoordinateX(): ?float
    {
        return $this->coordinateX;
    }

    /**
     * @param float|null $coordinateX
     * @return TPForm
     */
    public function setCoordinateX(?float $coordinateX): TPForm
    {
        $this->coordinateX = $coordinateX;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getCoordinateY(): ?float
    {
        return $this->coordinateY;
    }

    /**
     * @param float|null $coordinateY
     * @return TPForm
     */
    public function setCoordinateY(?float $coordinateY): TPForm
    {
        $this->coordinateY = $coordinateY;
        return $this;
    }
}