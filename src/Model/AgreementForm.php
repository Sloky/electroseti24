<?php

namespace App\Model;

use App\Entity\Main\Company;
use App\Entity\Main\Subscriber;

class AgreementForm
{
    /**
     * @var string|null
     */
    private ?string $number;

    /**
     * @var string|null
     */
    private ?string $paymentCode;

    /**
     * @var Company|null
     */
    private ?Company $company;

    /**
     * @var \App\Entity\Main\Subscriber|null
     */
    private ?Subscriber $subscriber;

    /**
     * @return string|null
     */
    public function getNumber(): ?string
    {
        return $this->number;
    }

    /**
     * @param string|null $number
     * @return AgreementForm
     */
    public function setNumber(?string $number): AgreementForm
    {
        $this->number = $number;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPaymentCode(): ?string
    {
        return $this->paymentCode;
    }

    /**
     * @param string|null $paymentCode
     * @return AgreementForm
     */
    public function setPaymentCode(?string $paymentCode): AgreementForm
    {
        $this->paymentCode = $paymentCode;
        return $this;
    }

    /**
     * @return \App\Entity\Main\Company|null
     */
    public function getCompany(): ?Company
    {
        return $this->company;
    }

    /**
     * @param \App\Entity\Main\Company|null $company
     * @return AgreementForm
     */
    public function setCompany(?Company $company): AgreementForm
    {
        $this->company = $company;
        return $this;
    }

    /**
     * @return \App\Entity\Main\Subscriber|null
     */
    public function getSubscriber(): ?Subscriber
    {
        return $this->subscriber;
    }

    /**
     * @param Subscriber|null $subscriber
     * @return AgreementForm
     */
    public function setSubscriber(?Subscriber $subscriber): AgreementForm
    {
        $this->subscriber = $subscriber;
        return $this;
    }
}