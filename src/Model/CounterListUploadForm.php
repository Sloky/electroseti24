<?php

namespace App\Model;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class CounterListUploadForm
{
    /**
     * @var UploadedFile|null
     */
    private ?UploadedFile $file;

    /**
     * @return UploadedFile|null
     */
    public function getFile(): ?UploadedFile
    {
        return $this->file;
    }

    /**
     * @param UploadedFile|null $file
     * @return CounterListUploadForm
     */
    public function setFile(?UploadedFile $file): CounterListUploadForm
    {
        $this->file = $file;
        return $this;
    }
}