<?php


namespace App\Model;


use Doctrine\Common\Collections\ArrayCollection;

class UserGroupForm
{
    /**
     * @var string
     */
    protected $groupName;

    /**
     * @var ArrayCollection|null
     */
    protected $roles;

    /**
     * @var string|null
     */
    protected $description;

    public function __construct()
    {
        $this->roles = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getGroupName(): ?string
    {
        return $this->groupName;
    }

    /**
     * @param string $groupName
     * @return UserGroupForm
     */
    public function setGroupName(string $groupName): UserGroupForm
    {
        $this->groupName = $groupName;
        return $this;
    }

    /**
     * @return ArrayCollection|null
     */
    public function getRoles(): ?ArrayCollection
    {
        return $this->roles;
    }

    /**
     * @param string $role
     * @return UserGroupForm
     */
    public function addRole(string $role): UserGroupForm
    {
        $this->roles->add($role);
        return $this;
    }

    /**
     * @param string $role
     * @return UserGroupForm
     */
    public function removeRole(string $role): UserGroupForm
    {
        $this->roles->removeElement($role);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return UserGroupForm
     */
    public function setDescription(?string $description): UserGroupForm
    {
        $this->description = $description;
        return $this;
    }
}