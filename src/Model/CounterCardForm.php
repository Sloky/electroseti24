<?php

namespace App\Model;

use App\Entity\Main\Transformer;
use DateTimeInterface;

class CounterCardForm
{
    /**
     * @var \App\Entity\Main\Transformer|null
     */
    private ?Transformer $transformer = null;

    /**
     * @var string|null
     */
    private ?string $counterTitle = null;

    /**
     * @var integer|null
     */
    private ?int $counterPhysicalStatus = null;

    /**
     * @var string|null
     */
    private ?string $counterModel = null;

    /**
     * @var string|null
     */
    private ?string $terminalSeal = null;

    /**
     * @var string|null
     */
    private ?string $antimagneticSeal = null;

    /**
     * @var string|null
     */
    private ?string $sideSeal = null;

    /**
     * @var string|null
     */
    private ?string $address = null;

    /**
     * @var float|null
     */
    private ?float $coordinateX = null;

    /**
     * @var float|null
     */
    private ?float $coordinateY = null;

    /**
     * @var string|null
     */
    private ?string $initialCounterValueId = null;

    /**
     * @var float|null
     */
    private ?float $initialCounterValue = null;

    /**
     * @var DateTimeInterface|null
     */
    private ?DateTimeInterface $initialCounterValueDate = null;

    /**
     * @var string|null
     */
    private ?string $lastCounterValueId = null;

    /**
     * @var float|null
     */
    private ?float $lastCounterValue = null;

    /**
     * @var DateTimeInterface|null
     */
    private ?DateTimeInterface $lastCounterValueDate = null;

    /**
     * @var string|null
     */
    private ?string $electricPoleTitle = null;

    /**
     * @var string|null
     */
    private ?string $electricLineTitle = null;

    /**
     * @var integer|null
     */
    private ?int $lodgersCount = null;

    /**
     * @var integer|null
     */
    private ?int $roomsCount = null;

    /**
     * @return \App\Entity\Main\Transformer|null
     */
    public function getTransformer(): ?Transformer
    {
        return $this->transformer;
    }

    /**
     * @param Transformer|null $transformer
     * @return CounterCardForm
     */
    public function setTransformer(?Transformer $transformer): CounterCardForm
    {
        $this->transformer = $transformer;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCounterTitle(): ?string
    {
        return $this->counterTitle;
    }

    /**
     * @param string|null $counterTitle
     * @return CounterCardForm
     */
    public function setCounterTitle(?string $counterTitle): CounterCardForm
    {
        $this->counterTitle = $counterTitle;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getCounterPhysicalStatus(): ?int
    {
        return $this->counterPhysicalStatus;
    }

    /**
     * @param int|null $counterPhysicalStatus
     * @return CounterCardForm
     */
    public function setCounterPhysicalStatus(?int $counterPhysicalStatus): CounterCardForm
    {
        $this->counterPhysicalStatus = $counterPhysicalStatus;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCounterModel(): ?string
    {
        return $this->counterModel;
    }

    /**
     * @param string|null $counterModel
     * @return CounterCardForm
     */
    public function setCounterModel(?string $counterModel): CounterCardForm
    {
        $this->counterModel = $counterModel;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTerminalSeal(): ?string
    {
        return $this->terminalSeal;
    }

    /**
     * @param string|null $terminalSeal
     * @return CounterCardForm
     */
    public function setTerminalSeal(?string $terminalSeal): CounterCardForm
    {
        $this->terminalSeal = $terminalSeal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAntimagneticSeal(): ?string
    {
        return $this->antimagneticSeal;
    }

    /**
     * @param string|null $antimagneticSeal
     * @return CounterCardForm
     */
    public function setAntimagneticSeal(?string $antimagneticSeal): CounterCardForm
    {
        $this->antimagneticSeal = $antimagneticSeal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSideSeal(): ?string
    {
        return $this->sideSeal;
    }

    /**
     * @param string|null $sideSeal
     * @return CounterCardForm
     */
    public function setSideSeal(?string $sideSeal): CounterCardForm
    {
        $this->sideSeal = $sideSeal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     * @return CounterCardForm
     */
    public function setAddress(?string $address): CounterCardForm
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getCoordinateX(): ?float
    {
        return $this->coordinateX;
    }

    /**
     * @param float|null $coordinateX
     * @return CounterCardForm
     */
    public function setCoordinateX(?float $coordinateX): CounterCardForm
    {
        $this->coordinateX = $coordinateX;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getCoordinateY(): ?float
    {
        return $this->coordinateY;
    }

    /**
     * @param float|null $coordinateY
     * @return CounterCardForm
     */
    public function setCoordinateY(?float $coordinateY): CounterCardForm
    {
        $this->coordinateY = $coordinateY;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getInitialCounterValueId(): ?string
    {
        return $this->initialCounterValueId;
    }

    /**
     * @param string|null $initialCounterValueId
     * @return CounterCardForm
     */
    public function setInitialCounterValueId(?string $initialCounterValueId): CounterCardForm
    {
        $this->initialCounterValueId = $initialCounterValueId;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getInitialCounterValue(): ?float
    {
        return $this->initialCounterValue;
    }

    /**
     * @param float|null $initialCounterValue
     * @return CounterCardForm
     */
    public function setInitialCounterValue(?float $initialCounterValue): CounterCardForm
    {
        $this->initialCounterValue = $initialCounterValue;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getInitialCounterValueDate(): ?DateTimeInterface
    {
        return $this->initialCounterValueDate;
    }

    /**
     * @param DateTimeInterface|null $initialCounterValueDate
     * @return CounterCardForm
     */
    public function setInitialCounterValueDate(?DateTimeInterface $initialCounterValueDate): CounterCardForm
    {
        $this->initialCounterValueDate = $initialCounterValueDate;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastCounterValueId(): ?string
    {
        return $this->lastCounterValueId;
    }

    /**
     * @param string|null $lastCounterValueId
     * @return CounterCardForm
     */
    public function setLastCounterValueId(?string $lastCounterValueId): CounterCardForm
    {
        $this->lastCounterValueId = $lastCounterValueId;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getLastCounterValue(): ?float
    {
        return $this->lastCounterValue;
    }

    /**
     * @param float|null $lastCounterValue
     * @return CounterCardForm
     */
    public function setLastCounterValue(?float $lastCounterValue): CounterCardForm
    {
        $this->lastCounterValue = $lastCounterValue;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getLastCounterValueDate(): ?DateTimeInterface
    {
        return $this->lastCounterValueDate;
    }

    /**
     * @param DateTimeInterface|null $lastCounterValueDate
     * @return CounterCardForm
     */
    public function setLastCounterValueDate(?DateTimeInterface $lastCounterValueDate): CounterCardForm
    {
        $this->lastCounterValueDate = $lastCounterValueDate;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getElectricPoleTitle(): ?string
    {
        return $this->electricPoleTitle;
    }

    /**
     * @param string|null $electricPoleTitle
     * @return CounterCardForm
     */
    public function setElectricPoleTitle(?string $electricPoleTitle): CounterCardForm
    {
        $this->electricPoleTitle = $electricPoleTitle;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getElectricLineTitle(): ?string
    {
        return $this->electricLineTitle;
    }

    /**
     * @param string|null $electricLineTitle
     * @return CounterCardForm
     */
    public function setElectricLineTitle(?string $electricLineTitle): CounterCardForm
    {
        $this->electricLineTitle = $electricLineTitle;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getLodgersCount(): ?int
    {
        return $this->lodgersCount;
    }

    /**
     * @param int|null $lodgersCount
     * @return CounterCardForm
     */
    public function setLodgersCount(?int $lodgersCount): CounterCardForm
    {
        $this->lodgersCount = $lodgersCount;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getRoomsCount(): ?int
    {
        return $this->roomsCount;
    }

    /**
     * @param int|null $roomsCount
     * @return CounterCardForm
     */
    public function setRoomsCount(?int $roomsCount): CounterCardForm
    {
        $this->roomsCount = $roomsCount;
        return $this;
    }
}