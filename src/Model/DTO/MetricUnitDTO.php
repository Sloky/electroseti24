<?php

namespace App\Model\DTO;

use App\Entity\Main\Transformer;
use DateTimeInterface;

class MetricUnitDTO
{
    /**
     * @var string|null
     */
    private ?string $counterId;

    /**
     * @var string|null
     */
    private ?string $counterTitle;

    /**
     * @var integer|null
     */
    private ?int $counterPhysicalStatus;

    /**
     * @var string|null
     */
    private ?string $counterModel;

    /**
     * @var string|null
     */
    private ?string $terminalSeal;

    /**
     * @var string|null
     */
    private ?string $antimagneticSeal;

    /**
     * @var string|null
     */
    private ?string $sideSeal;

    /**
     * @var float[]|null
     */
    private ?array $coordinates;

    /**
     * @var string|null
     */
    private ?string $initialCounterValueId;

    /**
     * @var float|null
     */
    private ?float $initialCounterValue;

    /**
     * @var DateTimeInterface|null
     */
    private ?DateTimeInterface $initialCounterValueDate;

    /**
     * @var string|null
     */
    private ?string $lastCounterValueId;

    /**
     * @var float|null
     */
    private ?float $lastCounterValue = null;

    /**
     * @var DateTimeInterface|null
     */
    private ?DateTimeInterface $lastCounterValueDate = null;

    /**
     * @var string|null
     */
    private ?string $subscriberTitle;

    /**
     * @var string|null
     */
    private ?string $agreementNumber;

    /**
     * @var integer|null
     */
    private ?int $lodgersCount;

    /**
     * @var integer|null
     */
    private ?int $roomsCount;

    /**
     * @var string[]|null
     */
    private ?array $phones;

    /**
     * @var string|null
     */
    private ?string $address;

    /**
     * @var \App\Entity\Main\Transformer|null
     */
    private ?Transformer $transformer = null;

    /**
     * @return string|null
     */
    public function getCounterId(): ?string
    {
        return $this->counterId;
    }

    /**
     * @param string|null $counterId
     * @return MetricUnitDTO
     */
    public function setCounterId(?string $counterId): MetricUnitDTO
    {
        $this->counterId = $counterId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCounterTitle(): ?string
    {
        return $this->counterTitle;
    }

    /**
     * @param string|null $counterTitle
     * @return MetricUnitDTO
     */
    public function setCounterTitle(?string $counterTitle): MetricUnitDTO
    {
        $this->counterTitle = $counterTitle;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getCounterPhysicalStatus(): ?int
    {
        return $this->counterPhysicalStatus;
    }

    /**
     * @param int|null $counterPhysicalStatus
     * @return MetricUnitDTO
     */
    public function setCounterPhysicalStatus(?int $counterPhysicalStatus): MetricUnitDTO
    {
        $this->counterPhysicalStatus = $counterPhysicalStatus;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCounterModel(): ?string
    {
        return $this->counterModel;
    }

    /**
     * @param string|null $counterModel
     * @return MetricUnitDTO
     */
    public function setCounterModel(?string $counterModel): MetricUnitDTO
    {
        $this->counterModel = $counterModel;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTerminalSeal(): ?string
    {
        return $this->terminalSeal;
    }

    /**
     * @param string|null $terminalSeal
     * @return MetricUnitDTO
     */
    public function setTerminalSeal(?string $terminalSeal): MetricUnitDTO
    {
        $this->terminalSeal = $terminalSeal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAntimagneticSeal(): ?string
    {
        return $this->antimagneticSeal;
    }

    /**
     * @param string|null $antimagneticSeal
     * @return MetricUnitDTO
     */
    public function setAntimagneticSeal(?string $antimagneticSeal): MetricUnitDTO
    {
        $this->antimagneticSeal = $antimagneticSeal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSideSeal(): ?string
    {
        return $this->sideSeal;
    }

    /**
     * @param string|null $sideSeal
     * @return MetricUnitDTO
     */
    public function setSideSeal(?string $sideSeal): MetricUnitDTO
    {
        $this->sideSeal = $sideSeal;
        return $this;
    }

    /**
     * @return float[]|null
     */
    public function getCoordinates(): ?array
    {
        return $this->coordinates;
    }

    /**
     * @param float[]|null $coordinates
     * @return MetricUnitDTO
     */
    public function setCoordinates(?array $coordinates): MetricUnitDTO
    {
        $this->coordinates = $coordinates;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getInitialCounterValueId(): ?string
    {
        return $this->initialCounterValueId;
    }

    /**
     * @param string|null $initialCounterValueId
     * @return MetricUnitDTO
     */
    public function setInitialCounterValueId(?string $initialCounterValueId): MetricUnitDTO
    {
        $this->initialCounterValueId = $initialCounterValueId;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getInitialCounterValue(): ?float
    {
        return $this->initialCounterValue;
    }

    /**
     * @param float|null $initialCounterValue
     * @return MetricUnitDTO
     */
    public function setInitialCounterValue(?float $initialCounterValue): MetricUnitDTO
    {
        $this->initialCounterValue = $initialCounterValue;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getInitialCounterValueDate(): ?DateTimeInterface
    {
        return $this->initialCounterValueDate;
    }

    /**
     * @param DateTimeInterface|null $initialCounterValueDate
     * @return MetricUnitDTO
     */
    public function setInitialCounterValueDate(?DateTimeInterface $initialCounterValueDate): MetricUnitDTO
    {
        $this->initialCounterValueDate = $initialCounterValueDate;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastCounterValueId(): ?string
    {
        return $this->lastCounterValueId;
    }

    /**
     * @param string|null $lastCounterValueId
     * @return MetricUnitDTO
     */
    public function setLastCounterValueId(?string $lastCounterValueId): MetricUnitDTO
    {
        $this->lastCounterValueId = $lastCounterValueId;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getLastCounterValue(): ?float
    {
        return $this->lastCounterValue;
    }

    /**
     * @param float|null $lastCounterValue
     * @return MetricUnitDTO
     */
    public function setLastCounterValue(?float $lastCounterValue): MetricUnitDTO
    {
        $this->lastCounterValue = $lastCounterValue;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getLastCounterValueDate(): ?DateTimeInterface
    {
        return $this->lastCounterValueDate;
    }

    /**
     * @param DateTimeInterface|null $lastCounterValueDate
     * @return MetricUnitDTO
     */
    public function setLastCounterValueDate(?DateTimeInterface $lastCounterValueDate): MetricUnitDTO
    {
        $this->lastCounterValueDate = $lastCounterValueDate;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubscriberTitle(): ?string
    {
        return $this->subscriberTitle;
    }

    /**
     * @param string|null $subscriberTitle
     * @return MetricUnitDTO
     */
    public function setSubscriberTitle(?string $subscriberTitle): MetricUnitDTO
    {
        $this->subscriberTitle = $subscriberTitle;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAgreementNumber(): ?string
    {
        return $this->agreementNumber;
    }

    /**
     * @param string|null $agreementNumber
     * @return MetricUnitDTO
     */
    public function setAgreementNumber(?string $agreementNumber): MetricUnitDTO
    {
        $this->agreementNumber = $agreementNumber;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getLodgersCount(): ?int
    {
        return $this->lodgersCount;
    }

    /**
     * @param int|null $lodgersCount
     * @return MetricUnitDTO
     */
    public function setLodgersCount(?int $lodgersCount): MetricUnitDTO
    {
        $this->lodgersCount = $lodgersCount;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getRoomsCount(): ?int
    {
        return $this->roomsCount;
    }

    /**
     * @param int|null $roomsCount
     * @return MetricUnitDTO
     */
    public function setRoomsCount(?int $roomsCount): MetricUnitDTO
    {
        $this->roomsCount = $roomsCount;
        return $this;
    }

    /**
     * @return string[]|null
     */
    public function getPhones(): ?array
    {
        return $this->phones;
    }

    /**
     * @param string[]|null $phones
     * @return MetricUnitDTO
     */
    public function setPhones(?array $phones): MetricUnitDTO
    {
        $this->phones = $phones;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     * @return MetricUnitDTO
     */
    public function setAddress(?string $address): MetricUnitDTO
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return \App\Entity\Main\Transformer|null
     */
    public function getTransformer(): ?Transformer
    {
        return $this->transformer;
    }

    /**
     * @param \App\Entity\Main\Transformer|null $transformer
     * @return MetricUnitDTO
     */
    public function setTransformer(?Transformer $transformer): MetricUnitDTO
    {
        $this->transformer = $transformer;
        return $this;
    }
}