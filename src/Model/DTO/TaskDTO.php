<?php


namespace App\Model\DTO;


use App\Entity\Main\Company;
use App\Entity\Main\CounterType;
use App\Entity\Main\Feeder;
use App\Entity\Main\Substation;
use App\Entity\Main\Transformer;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Security\Core\User\UserInterface;

class TaskDTO
{
    /**
     * ID
     * @var string|null
     */
    private $id;

    /**
     * Тип задачи
     * @var integer
     */
    private $taskType = 0;

    /**
     * Исполнитель (контролёр)
     * @var UserInterface|null
     */
    private $executor;

    /**
     * Приоритет
     * @var integer
     */
    private $priority = 0;

    /**
     * Срок выполнения
     * @var DateTimeInterface|null
     */
    private $deadline;

    /**
     * Период действия задачи (отчетный период)
     * @var DateTimeInterface|null
     */
    private $period;

    /**
     * Статус задачи (Не выполнено, к проверке, принято)
     * @var integer
     */
    private $status = 0;

    /**
     * Координаты оси X
     * @var float|null
     */
    private $coordinateX;

    /**
     * Координаты оси Y
     * @var float|null
     */
    private $coordinateY;

    /**
     * Примечание от оператора
     * @var string|null
     */
    private $operatorComment;

    /**
     * Примечание от контролёра
     * @var string|null
     */
    private $controllerComment;

    /**
     * Фактор невыполнения задачи
     * @var Collection
     */
    private $failureFactors;

    /**
     * Поля для актуализации
     * @var array
     */
    private $actualizedFields = [];

    /**
     * Изменённые поля (в мобильном приложении)
     * @var array
     */
    private $changedFields = [];

    /**
     * Отделение
     * @var \App\Entity\Main\Company|null
     */
    private $company;

    /**
     * Населенный пункт
     * @var string|null
     */
    private $locality;

    /**
     * Улица
     * @var string|null
     */
    private $street;

    /**
     * Номер дома
     * @var string|null
     */
    private $houseNumber;

    /**
     * Номер подъезда
     * @var string|null
     */
    private $porchNumber;

    /**
     * Номер квартиры
     * @var string|null
     */
    private $apartmentNumber;

    /**
     * Количество жильцов
     * @var integer|null
     */
    private $lodgersCount;

    /**
     * Количество комнат
     * @var integer|null
     */
    private $roomsCount;

    /**
     * Тип абонента
     * @var integer|null
     */
    private $subscriberType = 0;

    /**
     * Наименование абонента
     * @var string|null
     */
    private $subscriberTitle;

    /**
     * Имя абонента (для физ. лиц)
     * @var string|null
     */
    private $subscriberName;

    /**
     * Фамилия абонента
     * @var string|null
     */
    private $subscriberSurname;

    /**
     * Отчество абонента
     * @var string|null
     */
    private $subscriberPatronymic;

    /**
     * Телефоны абонента
     * @var array
     */
    private $subscriberPhones = [];

    /**
     * Номер договора
     * @var string|null
     */
    private $agreementNumber;

    /**
     * Номер узла учета
     * @var string|null
     */
    private $counterNumber;

    /**
     * Последнее значении показаний УУ
     * @var float[]|null
     */
    private $lastCounterValue;

    /**
     * Текущее значение показаний УУ
     * @var float[]|null
     */
    private $currentCounterValue;

    /**
     * Расход потребления
     * @var float|null
     */
    private $consumption;

    /**
     * Коэффициент потребления
     * @var float|null
     */
    private $consumptionCoefficient;

    /**
     * Физическое состояние УУ (Возможность снять показания)
     * @var integer|null
     */
    private $counterPhysicalStatus = 0;

    /**
     * Подстанция
     * @var Substation|null
     */
    private $substation;

    /**
     * Фидер
     * @var Feeder|null
     */
    private $feeder;

    /**
     * Трансформатор
     * @var \App\Entity\Main\Transformer|null
     */
    private $transformer;

    /**
     * Номер линии
     * @var string|null
     */
    private $lineNumber;

    /**
     * Номер столба
     * @var string|null
     */
    private $electricPoleNumber;

    /**
     * Тип счетчика
     * @var CounterType|null
     */
    private $counterType;

    /**
     * Клеммная пломба
     * @var string|null
     */
    private $terminalSeal;

    /**
     * Антимагнитная пломба
     * @var string|null
     */
    private $antiMagneticSeal;

    /**
     * Боковая пломба
     * @var string|null
     */
    private $sideSeal;

    /**
     * Фотографии
     * @var Collection
     */
    private $photos = [];

    /**
     * Временный вариант адреса
     * @var string|null
     */
    private $temporaryAddress;

    /**
     * Временный вариант модели счётчика
     * @var string|null
     */
    private $temporaryCounterType;

    /**
     * Временный вариант подстанции
     * @var string|null
     */
    private $temporarySubstation;

    /**
     * Временный вариант трансформатора
     * @var string|null
     */
    private $temporaryTransformer;

    /**
     * Временный вариант фидера
     * @var string|null
     */
    private $temporaryFeeder;

    /**
     * TaskDTO constructor.
     */
    public function __construct()
    {
        $this->failureFactors = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return TaskDTO
     */
    public function setId(?string $id): TaskDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getTaskType(): int
    {
        return $this->taskType;
    }

    /**
     * @param int $taskType
     * @return TaskDTO
     */
    public function setTaskType(int $taskType): TaskDTO
    {
        $this->taskType = $taskType;
        return $this;
    }

    /**
     * @return UserInterface|null
     */
    public function getExecutor(): ?UserInterface
    {
        return $this->executor;
    }

    /**
     * @param UserInterface|null $executor
     * @return TaskDTO
     */
    public function setExecutor(?UserInterface $executor): TaskDTO
    {
        $this->executor = $executor;
        return $this;
    }

    /**
     * @return int
     */
    public function getPriority(): int
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     * @return TaskDTO
     */
    public function setPriority(int $priority): TaskDTO
    {
        $this->priority = $priority;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getDeadline(): ?DateTimeInterface
    {
        return $this->deadline;
    }

    /**
     * @param DateTimeInterface|null $deadline
     * @return TaskDTO
     */
    public function setDeadline(?DateTimeInterface $deadline): TaskDTO
    {
        $this->deadline = $deadline;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getPeriod(): ?DateTimeInterface
    {
        return $this->period;
    }

    /**
     * @param DateTimeInterface|null $period
     * @return TaskDTO
     */
    public function setPeriod(?DateTimeInterface $period): TaskDTO
    {
        $this->period = $period;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return TaskDTO
     */
    public function setStatus(int $status): TaskDTO
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getCoordinateX(): ?float
    {
        return $this->coordinateX;
    }

    /**
     * @param float|null $coordinateX
     * @return TaskDTO
     */
    public function setCoordinateX(?float $coordinateX): TaskDTO
    {
        $this->coordinateX = $coordinateX;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getCoordinateY(): ?float
    {
        return $this->coordinateY;
    }

    /**
     * @param float|null $coordinateY
     * @return TaskDTO
     */
    public function setCoordinateY(?float $coordinateY): TaskDTO
    {
        $this->coordinateY = $coordinateY;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOperatorComment(): ?string
    {
        return $this->operatorComment;
    }

    /**
     * @param string|null $operatorComment
     * @return TaskDTO
     */
    public function setOperatorComment(?string $operatorComment): TaskDTO
    {
        $this->operatorComment = $operatorComment;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getControllerComment(): ?string
    {
        return $this->controllerComment;
    }

    /**
     * @param string|null $controllerComment
     * @return TaskDTO
     */
    public function setControllerComment(?string $controllerComment): TaskDTO
    {
        $this->controllerComment = $controllerComment;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getFailureFactors(): Collection
    {
        return $this->failureFactors;
    }

    /**
     * @param Collection $failureFactors
     * @return TaskDTO
     */
    public function setFailureFactors(Collection $failureFactors): TaskDTO
    {
        $this->failureFactors = $failureFactors;
        return $this;
    }

    /**
     * @return array
     */
    public function getActualizedFields(): array
    {
        return $this->actualizedFields;
    }

    /**
     * @param array $actualizedFields
     * @return TaskDTO
     */
    public function setActualizedFields(array $actualizedFields): TaskDTO
    {
        $this->actualizedFields = $actualizedFields;
        return $this;
    }

    /**
     * @return array
     */
    public function getChangedFields(): array
    {
        return $this->changedFields;
    }

    /**
     * @param array $changedFields
     * @return TaskDTO
     */
    public function setChangedFields(array $changedFields): TaskDTO
    {
        $this->changedFields = $changedFields;
        return $this;
    }

    /**
     * @return Company|null
     */
    public function getCompany(): ?Company
    {
        return $this->company;
    }

    /**
     * @param Company|null $company
     * @return TaskDTO
     */
    public function setCompany(?Company $company): TaskDTO
    {
        $this->company = $company;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLocality(): ?string
    {
        return $this->locality;
    }

    /**
     * @param string|null $locality
     * @return TaskDTO
     */
    public function setLocality(?string $locality): TaskDTO
    {
        $this->locality = $locality;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStreet(): ?string
    {
        return $this->street;
    }

    /**
     * @param string|null $street
     * @return TaskDTO
     */
    public function setStreet(?string $street): TaskDTO
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getHouseNumber(): ?string
    {
        return $this->houseNumber;
    }

    /**
     * @param string|null $houseNumber
     * @return TaskDTO
     */
    public function setHouseNumber(?string $houseNumber): TaskDTO
    {
        $this->houseNumber = $houseNumber;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPorchNumber(): ?string
    {
        return $this->porchNumber;
    }

    /**
     * @param string|null $porchNumber
     * @return TaskDTO
     */
    public function setPorchNumber(?string $porchNumber): TaskDTO
    {
        $this->porchNumber = $porchNumber;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getApartmentNumber(): ?string
    {
        return $this->apartmentNumber;
    }

    /**
     * @param string|null $apartmentNumber
     * @return TaskDTO
     */
    public function setApartmentNumber(?string $apartmentNumber): TaskDTO
    {
        $this->apartmentNumber = $apartmentNumber;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getLodgersCount(): ?int
    {
        return $this->lodgersCount;
    }

    /**
     * @param int|null $lodgersCount
     * @return TaskDTO
     */
    public function setLodgersCount(?int $lodgersCount): TaskDTO
    {
        $this->lodgersCount = $lodgersCount;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getRoomsCount(): ?int
    {
        return $this->roomsCount;
    }

    /**
     * @param int|null $roomsCount
     * @return TaskDTO
     */
    public function setRoomsCount(?int $roomsCount): TaskDTO
    {
        $this->roomsCount = $roomsCount;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getSubscriberType(): ?int
    {
        return $this->subscriberType;
    }

    /**
     * @param int|null $subscriberType
     * @return TaskDTO
     */
    public function setSubscriberType(?int $subscriberType): TaskDTO
    {
        $this->subscriberType = $subscriberType;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubscriberTitle(): ?string
    {
        return $this->subscriberTitle;
    }

    /**
     * @param string $subscriberTitle
     * @return TaskDTO
     */
    public function setSubscriberTitle(?string $subscriberTitle): TaskDTO
    {
        $this->subscriberTitle = $subscriberTitle;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubscriberName(): ?string
    {
        return $this->subscriberName;
    }

    /**
     * @param string|null $subscriberName
     * @return TaskDTO
     */
    public function setSubscriberName(?string $subscriberName): TaskDTO
    {
        $this->subscriberName = $subscriberName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubscriberSurname(): ?string
    {
        return $this->subscriberSurname;
    }

    /**
     * @param string|null $subscriberSurname
     * @return TaskDTO
     */
    public function setSubscriberSurname(?string $subscriberSurname): TaskDTO
    {
        $this->subscriberSurname = $subscriberSurname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubscriberPatronymic(): ?string
    {
        return $this->subscriberPatronymic;
    }

    /**
     * @param string|null $subscriberPatronymic
     * @return TaskDTO
     */
    public function setSubscriberPatronymic(?string $subscriberPatronymic): TaskDTO
    {
        $this->subscriberPatronymic = $subscriberPatronymic;
        return $this;
    }

    /**
     * @return array
     */
    public function getSubscriberPhones(): array
    {
        return $this->subscriberPhones;
    }

    /**
     * @param array $subscriberPhones
     * @return TaskDTO
     */
    public function setSubscriberPhones(array $subscriberPhones): TaskDTO
    {
        $this->subscriberPhones = $subscriberPhones;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAgreementNumber(): ?string
    {
        return $this->agreementNumber;
    }

    /**
     * @param string|null $agreementNumber
     * @return TaskDTO
     */
    public function setAgreementNumber(?string $agreementNumber): TaskDTO
    {
        $this->agreementNumber = $agreementNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getCounterNumber(): ?string
    {
        return $this->counterNumber;
    }

    /**
     * @param string $counterNumber
     * @return TaskDTO
     */
    public function setCounterNumber(?string $counterNumber): TaskDTO
    {
        $this->counterNumber = $counterNumber;
        return $this;
    }

    /**
     * @return float[]|null
     */
    public function getLastCounterValue(): ?array
    {
        return $this->lastCounterValue;
    }

    /**
     * @param float[]|null $lastCounterValue
     * @return TaskDTO
     */
    public function setLastCounterValue(?array $lastCounterValue): TaskDTO
    {
        $this->lastCounterValue = $lastCounterValue;
        return $this;
    }

    /**
     * @return float[]|null
     */
    public function getCurrentCounterValue(): ?array
    {
        return $this->currentCounterValue;
    }

    /**
     * @param float[]|null $currentCounterValue
     * @return TaskDTO
     */
    public function setCurrentCounterValue(?array $currentCounterValue): TaskDTO
    {
        $this->currentCounterValue = $currentCounterValue;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getConsumption(): ?float
    {
        return $this->consumption;
    }

    /**
     * @param float|null $consumption
     * @return TaskDTO
     */
    public function setConsumption(?float $consumption): TaskDTO
    {
        $this->consumption = $consumption;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getConsumptionCoefficient(): ?float
    {
        return $this->consumptionCoefficient;
    }

    /**
     * @param float|null $consumptionCoefficient
     * @return TaskDTO
     */
    public function setConsumptionCoefficient(?float $consumptionCoefficient): TaskDTO
    {
        $this->consumptionCoefficient = $consumptionCoefficient;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getCounterPhysicalStatus(): ?int
    {
        return $this->counterPhysicalStatus;
    }

    /**
     * @param int|null $counterPhysicalStatus
     * @return TaskDTO
     */
    public function setCounterPhysicalStatus(?int $counterPhysicalStatus): TaskDTO
    {
        $this->counterPhysicalStatus = $counterPhysicalStatus;
        return $this;
    }

    /**
     * @return Substation|null
     */
    public function getSubstation(): ?Substation
    {
        return $this->substation;
    }

    /**
     * @param \App\Entity\Main\Substation|null $substation
     * @return TaskDTO
     */
    public function setSubstation(?Substation $substation): TaskDTO
    {
        $this->substation = $substation;
        return $this;
    }

    /**
     * @return Feeder|null
     */
    public function getFeeder(): ?Feeder
    {
        return $this->feeder;
    }

    /**
     * @param Feeder|null $feeder
     * @return TaskDTO
     */
    public function setFeeder(?Feeder $feeder): TaskDTO
    {
        $this->feeder = $feeder;
        return $this;
    }

    /**
     * @return \App\Entity\Main\Transformer|null
     */
    public function getTransformer(): ?Transformer
    {
        return $this->transformer;
    }

    /**
     * @param \App\Entity\Main\Transformer|null $transformer
     * @return TaskDTO
     */
    public function setTransformer(?Transformer $transformer): TaskDTO
    {
        $this->transformer = $transformer;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLineNumber(): ?string
    {
        return $this->lineNumber;
    }

    /**
     * @param string|null $lineNumber
     * @return TaskDTO
     */
    public function setLineNumber(?string $lineNumber): TaskDTO
    {
        $this->lineNumber = $lineNumber;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getElectricPoleNumber(): ?string
    {
        return $this->electricPoleNumber;
    }

    /**
     * @param string|null $electricPoleNumber
     * @return TaskDTO
     */
    public function setElectricPoleNumber(?string $electricPoleNumber): TaskDTO
    {
        $this->electricPoleNumber = $electricPoleNumber;
        return $this;
    }

    /**
     * @return CounterType|null
     */
    public function getCounterType(): ?CounterType
    {
        return $this->counterType;
    }

    /**
     * @param \App\Entity\Main\CounterType|null $counterType
     * @return TaskDTO
     */
    public function setCounterType(?CounterType $counterType): TaskDTO
    {
        $this->counterType = $counterType;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTerminalSeal(): ?string
    {
        return $this->terminalSeal;
    }

    /**
     * @param string|null $terminalSeal
     * @return TaskDTO
     */
    public function setTerminalSeal(?string $terminalSeal): TaskDTO
    {
        $this->terminalSeal = $terminalSeal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAntiMagneticSeal(): ?string
    {
        return $this->antiMagneticSeal;
    }

    /**
     * @param string|null $antiMagneticSeal
     * @return TaskDTO
     */
    public function setAntiMagneticSeal(?string $antiMagneticSeal): TaskDTO
    {
        $this->antiMagneticSeal = $antiMagneticSeal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSideSeal(): ?string
    {
        return $this->sideSeal;
    }

    /**
     * @param string|null $sideSeal
     * @return TaskDTO
     */
    public function setSideSeal(?string $sideSeal): TaskDTO
    {
        $this->sideSeal = $sideSeal;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getPhotos(): array
    {
        return $this->photos;
    }

    /**
     * @param string[] $photos
     * @return TaskDTO
     */
    public function setPhotos(array $photos): TaskDTO
    {
        $this->photos = $photos;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTemporaryAddress(): ?string
    {
        return $this->temporaryAddress;
    }

    /**
     * @param string|null $temporaryAddress
     * @return TaskDTO
     */
    public function setTemporaryAddress(?string $temporaryAddress): TaskDTO
    {
        $this->temporaryAddress = $temporaryAddress;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTemporaryCounterType(): ?string
    {
        return $this->temporaryCounterType;
    }

    /**
     * @param string|null $temporaryCounterType
     * @return TaskDTO
     */
    public function setTemporaryCounterType(?string $temporaryCounterType): TaskDTO
    {
        $this->temporaryCounterType = $temporaryCounterType;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTemporarySubstation(): ?string
    {
        return $this->temporarySubstation;
    }

    /**
     * @param string|null $temporarySubstation
     * @return TaskDTO
     */
    public function setTemporarySubstation(?string $temporarySubstation): TaskDTO
    {
        $this->temporarySubstation = $temporarySubstation;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTemporaryTransformer(): ?string
    {
        return $this->temporaryTransformer;
    }

    /**
     * @param string|null $temporaryTransformer
     * @return TaskDTO
     */
    public function setTemporaryTransformer(?string $temporaryTransformer): TaskDTO
    {
        $this->temporaryTransformer = $temporaryTransformer;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTemporaryFeeder(): ?string
    {
        return $this->temporaryFeeder;
    }

    /**
     * @param string|null $temporaryFeeder
     * @return TaskDTO
     */
    public function setTemporaryFeeder(?string $temporaryFeeder): TaskDTO
    {
        $this->temporaryFeeder = $temporaryFeeder;
        return $this;
    }
}