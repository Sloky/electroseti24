<?php

namespace App\Model\DTO;

use App\Entity\Main\Agreement;
use App\Entity\Main\Subscriber;
use App\Entity\Main\Transformer;
use DateTimeInterface;

class CounterCardDTO
{
    /**
     * @var string|null
     */
    private ?string $counterId = null;

    /**
     * @var string|null
     */
    private ?string $counterTitle = null;

    /**
     * @var integer|null
     */
    private ?int $counterPhysicalStatus = null;

    /**
     * @var string|null
     */
    private ?string $counterModel = null;

    /**
     * @var string|null
     */
    private ?string $terminalSeal = null;

    /**
     * @var string|null
     */
    private ?string $antimagneticSeal = null;

    /**
     * @var string|null
     */
    private ?string $sideSeal = null;

    /**
     * @var string|null
     */
    private ?string $address = null;

    /**
     * @var float|null
     */
    private ?float $coordinateX = null;

    /**
     * @var float|null
     */
    private ?float $coordinateY = null;

    /**
     * @var \App\Entity\Main\Transformer|null
     */
    private ?Transformer $transformer = null;

    /**
     * @var string|null
     */
    private ?string $initialCounterValueId = null;

    /**
     * @var float|null
     */
    private ?float $initialCounterValue = null;

    /**
     * @var DateTimeInterface|null
     */
    private ?DateTimeInterface $initialCounterValueDate = null;

    /**
     * @var string|null
     */
    private ?string $lastCounterValueId = null;

    /**
     * @var float|null
     */
    private ?float $lastCounterValue = null;

    /**
     * @var DateTimeInterface|null
     */
    private ?DateTimeInterface $lastCounterValueDate = null;

    /**
     * @var string|null
     */
    private ?string $electricPole = null;

    /**
     * @var string|null
     */
    private ?string $electricLine = null;

    /**
     * @var integer|null
     */
    private ?int $lodgersCount = null;

    /**
     * @var integer|null
     */
    private ?int $roomsCount = null;

    /**
     * @var Subscriber|null
     */
    private ?Subscriber $subscriber = null;

    /**
     * @var \App\Entity\Main\Agreement|null
     */
    private ?Agreement $agreement = null;

    /**
     * @return string|null
     */
    public function getCounterId(): ?string
    {
        return $this->counterId;
    }

    /**
     * @param string|null $counterId
     * @return CounterCardDTO
     */
    public function setCounterId(?string $counterId): CounterCardDTO
    {
        $this->counterId = $counterId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCounterTitle(): ?string
    {
        return $this->counterTitle;
    }

    /**
     * @param string|null $counterTitle
     * @return CounterCardDTO
     */
    public function setCounterTitle(?string $counterTitle): CounterCardDTO
    {
        $this->counterTitle = $counterTitle;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getCounterPhysicalStatus(): ?int
    {
        return $this->counterPhysicalStatus;
    }

    /**
     * @param int|null $counterPhysicalStatus
     * @return CounterCardDTO
     */
    public function setCounterPhysicalStatus(?int $counterPhysicalStatus): CounterCardDTO
    {
        $this->counterPhysicalStatus = $counterPhysicalStatus;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCounterModel(): ?string
    {
        return $this->counterModel;
    }

    /**
     * @param string|null $counterModel
     * @return CounterCardDTO
     */
    public function setCounterModel(?string $counterModel): CounterCardDTO
    {
        $this->counterModel = $counterModel;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTerminalSeal(): ?string
    {
        return $this->terminalSeal;
    }

    /**
     * @param string|null $terminalSeal
     * @return CounterCardDTO
     */
    public function setTerminalSeal(?string $terminalSeal): CounterCardDTO
    {
        $this->terminalSeal = $terminalSeal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAntimagneticSeal(): ?string
    {
        return $this->antimagneticSeal;
    }

    /**
     * @param string|null $antimagneticSeal
     * @return CounterCardDTO
     */
    public function setAntimagneticSeal(?string $antimagneticSeal): CounterCardDTO
    {
        $this->antimagneticSeal = $antimagneticSeal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSideSeal(): ?string
    {
        return $this->sideSeal;
    }

    /**
     * @param string|null $sideSeal
     * @return CounterCardDTO
     */
    public function setSideSeal(?string $sideSeal): CounterCardDTO
    {
        $this->sideSeal = $sideSeal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     * @return CounterCardDTO
     */
    public function setAddress(?string $address): CounterCardDTO
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getCoordinateX(): ?float
    {
        return $this->coordinateX;
    }

    /**
     * @param float|null $coordinateX
     * @return CounterCardDTO
     */
    public function setCoordinateX(?float $coordinateX): CounterCardDTO
    {
        $this->coordinateX = $coordinateX;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getCoordinateY(): ?float
    {
        return $this->coordinateY;
    }

    /**
     * @param float|null $coordinateY
     * @return CounterCardDTO
     */
    public function setCoordinateY(?float $coordinateY): CounterCardDTO
    {
        $this->coordinateY = $coordinateY;
        return $this;
    }

    /**
     * @return \App\Entity\Main\Transformer|null
     */
    public function getTransformer(): ?Transformer
    {
        return $this->transformer;
    }

    /**
     * @param \App\Entity\Main\Transformer|null $transformer
     * @return CounterCardDTO
     */
    public function setTransformer(?Transformer $transformer): CounterCardDTO
    {
        $this->transformer = $transformer;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getInitialCounterValueId(): ?string
    {
        return $this->initialCounterValueId;
    }

    /**
     * @param string|null $initialCounterValueId
     * @return CounterCardDTO
     */
    public function setInitialCounterValueId(?string $initialCounterValueId): CounterCardDTO
    {
        $this->initialCounterValueId = $initialCounterValueId;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getInitialCounterValue(): ?float
    {
        return $this->initialCounterValue;
    }

    /**
     * @param float|null $initialCounterValue
     * @return CounterCardDTO
     */
    public function setInitialCounterValue(?float $initialCounterValue): CounterCardDTO
    {
        $this->initialCounterValue = $initialCounterValue;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getInitialCounterValueDate(): ?DateTimeInterface
    {
        return $this->initialCounterValueDate;
    }

    /**
     * @param DateTimeInterface|null $initialCounterValueDate
     * @return CounterCardDTO
     */
    public function setInitialCounterValueDate(?DateTimeInterface $initialCounterValueDate): CounterCardDTO
    {
        $this->initialCounterValueDate = $initialCounterValueDate;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastCounterValueId(): ?string
    {
        return $this->lastCounterValueId;
    }

    /**
     * @param string|null $lastCounterValueId
     * @return CounterCardDTO
     */
    public function setLastCounterValueId(?string $lastCounterValueId): CounterCardDTO
    {
        $this->lastCounterValueId = $lastCounterValueId;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getLastCounterValue(): ?float
    {
        return $this->lastCounterValue;
    }

    /**
     * @param float|null $lastCounterValue
     * @return CounterCardDTO
     */
    public function setLastCounterValue(?float $lastCounterValue): CounterCardDTO
    {
        $this->lastCounterValue = $lastCounterValue;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getLastCounterValueDate(): ?DateTimeInterface
    {
        return $this->lastCounterValueDate;
    }

    /**
     * @param DateTimeInterface|null $lastCounterValueDate
     * @return CounterCardDTO
     */
    public function setLastCounterValueDate(?DateTimeInterface $lastCounterValueDate): CounterCardDTO
    {
        $this->lastCounterValueDate = $lastCounterValueDate;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getElectricPole(): ?string
    {
        return $this->electricPole;
    }

    /**
     * @param string|null $electricPole
     * @return CounterCardDTO
     */
    public function setElectricPole(?string $electricPole): CounterCardDTO
    {
        $this->electricPole = $electricPole;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getElectricLine(): ?string
    {
        return $this->electricLine;
    }

    /**
     * @param string|null $electricLine
     * @return CounterCardDTO
     */
    public function setElectricLine(?string $electricLine): CounterCardDTO
    {
        $this->electricLine = $electricLine;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getLodgersCount(): ?int
    {
        return $this->lodgersCount;
    }

    /**
     * @param int|null $lodgersCount
     * @return CounterCardDTO
     */
    public function setLodgersCount(?int $lodgersCount): CounterCardDTO
    {
        $this->lodgersCount = $lodgersCount;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getRoomsCount(): ?int
    {
        return $this->roomsCount;
    }

    /**
     * @param int|null $roomsCount
     * @return CounterCardDTO
     */
    public function setRoomsCount(?int $roomsCount): CounterCardDTO
    {
        $this->roomsCount = $roomsCount;
        return $this;
    }

    /**
     * @return Subscriber|null
     */
    public function getSubscriber(): ?Subscriber
    {
        return $this->subscriber;
    }

    /**
     * @param \App\Entity\Main\Subscriber|null $subscriber
     * @return CounterCardDTO
     */
    public function setSubscriber(?Subscriber $subscriber): CounterCardDTO
    {
        $this->subscriber = $subscriber;
        return $this;
    }

    /**
     * @return Agreement|null
     */
    public function getAgreement(): ?Agreement
    {
        return $this->agreement;
    }

    /**
     * @param Agreement|null $agreement
     * @return CounterCardDTO
     */
    public function setAgreement(?Agreement $agreement): CounterCardDTO
    {
        $this->agreement = $agreement;
        return $this;
    }
}