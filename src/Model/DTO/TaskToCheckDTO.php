<?php


namespace App\Model\DTO;


use App\Entity\Main\Company;
use App\Entity\Main\CounterType;
use App\Entity\Main\Feeder;
use App\Entity\Main\Substation;
use App\Entity\Main\Transformer;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Security\Core\User\UserInterface;

class TaskToCheckDTO
{
    /**
     * ID
     * @var string|null
     */
    private $id;

    /**
     * Тип задачи
     * @var integer
     */
    private $taskType = 0;

    /**
     * Исполнитель (контролёр)
     * @var UserInterface|null
     */
    private $executor;

    /**
     * Приоритет
     * @var integer
     */
    private $priority = 0;

    /**
     * Срок выполнения
     * @var DateTimeInterface|null
     */
    private $deadline;

    /**
     * Дата и время выполнения задачи
     * @var DateTimeInterface|null
     */
    private $executedDate;

    /**
     * Период действия задачи (отчетный период)
     * @var DateTimeInterface|null
     */
    private $period;

    /**
     * Статус задачи (Не выполнено, к проверке, принято)
     * @var integer
     */
    private $status = 0;

    /**
     * Координаты оси X
     * @var float|null
     */
    private $coordinateX;

    /**
     * Координаты оси Y
     * @var float|null
     */
    private $coordinateY;

    /**
     * Примечание от оператора
     * @var string|null
     */
    private $operatorComment;

    /**
     * Примечание от контролёра
     * @var string|null
     */
    private $controllerComment;

    /**
     * Фактор невыполнения задачи
     * @var Collection
     */
    private $failureFactors;

    /**
     * Поля для актуализации
     * @var array
     */
    private $actualizedFields = [];

    /**
     * Изменённые поля (в мобильном приложении)
     * @var array
     */
    private $changedFields = [];

    /**
     * Отделение
     * @var \App\Entity\Main\Company|null
     */
    private $company;

    /**
     * Населенный пункт
     * @var string|null
     */
    private $locality;

    /**
     * Улица
     * @var string|null
     */
    private $street;

    /**
     * Номер дома
     * @var string|null
     */
    private $houseNumber;

    /**
     * Номер подъезда
     * @var string|null
     */
    private $porchNumber;

    /**
     * Номер квартиры
     * @var string|null
     */
    private $apartmentNumber;

    /**
     * Количество жильцов
     * @var integer|null
     */
    private $lodgersCount;

    /**
     * Количество комнат
     * @var integer|null
     */
    private $roomsCount;

    /**
     * Тип абонента
     * @var integer|null
     */
    private $subscriberType = 0;

    /**
     * Наименование абонента
     * @var string|null
     */
    private $subscriberTitle;

    /**
     * Имя абонента (для физ. лиц)
     * @var string|null
     */
    private $subscriberName;

    /**
     * Фамилия абонента
     * @var string|null
     */
    private $subscriberSurname;

    /**
     * Отчество абонента
     * @var string|null
     */
    private $subscriberPatronymic;

    /**
     * Телефоны абонента
     * @var array
     */
    private $subscriberPhones = [];

    /**
     * Номер договора
     * @var string|null
     */
    private $agreementNumber;

    /**
     * Номер узла учета
     * @var string|null
     */
    private $counterNumber;

    /**
     * Последнее значении показаний УУ
     * @var float[]|null
     */
    private $lastCounterValue;

    /**
     * Текущее значение показаний УУ
     * @var float[]|null
     */
    private $currentCounterValue;

    /**
     * Расход потребления
     * @var float|null
     */
    private $consumption;

    /**
     * Коэффициент потребления
     * @var float|null
     */
    private $consumptionCoefficient;

    /**
     * Физическое состояние УУ (Возможность снять показания)
     * @var integer|null
     */
    private $counterPhysicalStatus = 0;

    /**
     * Подстанция
     * @var Substation|null
     */
    private $substation;

    /**
     * Фидер
     * @var Feeder|null
     */
    private $feeder;

    /**
     * Трансформатор
     * @var Transformer|null
     */
    private $transformer;

    /**
     * Номер линии
     * @var string|null
     */
    private $lineNumber;

    /**
     * Номер столба
     * @var string|null
     */
    private $electricPoleNumber;

    /**
     * Тип счетчика
     * @var CounterType|null
     */
    private $counterType;

    /**
     * Клеммная пломба
     * @var string|null
     */
    private $terminalSeal;

    /**
     * Антимагнитная пломба
     * @var string|null
     */
    private $antiMagneticSeal;

    /**
     * Боковая пломба
     * @var string|null
     */
    private $sideSeal;

    /**
     * Фотографии
     * @var Collection
     */
    private $photos = [];

    /**
     * Временный вариант адреса
     * @var string|null
     */
    private $temporaryAddress;

    /**
     * Временный вариант модели счётчика
     * @var string|null
     */
    private $temporaryCounterType;

    /**
     * Временный вариант подстанции
     * @var string|null
     */
    private $temporarySubstation;

    /**
     * Временный вариант трансформатора
     * @var string|null
     */
    private $temporaryTransformer;

    /**
     * Временный вариант фидера
     * @var string|null
     */
    private $temporaryFeeder;

    /**
     * Измененный номер УУ
     * @var string|null
     */
    private $changedCounterNumber;

    /**
     * Новый тип счётчика
     * @var CounterType|null
     */
    private $changedCounterType;

    /**
     * Измененный тип (модель) УУ
     * @var string|null
     */
    private $changedCounterTypeString;

    /**
     * Измененная клеммная пломба
     * @var string|null
     */
    private $changedTerminalSeal;

    /**
     * Измененная антимагнитная пломба
     * @var string|null
     */
    private $changedAntimagneticSeal;

    /**
     * Измененная боковая пломба
     * @var string|null
     */
    private $changedSideSeal;

    /**
     * Измененное количество жильцов
     * @var integer|null
     */
    private $changedLodgersCount;

    /**
     * Измененное количество комнат
     * @var integer|null
     */
    private $changedRoomsCount;

    /**
     * Координата X места выполнения задачи (фотофиксации счетчика)
     * @var float|null
     */
    private $newCoordinateX;

    /**
     * Координата Y места выполнения задачи
     * @var float|null
     */
    private $newCoordinateY;

    /**
     * Публичный адрес для фотографий
     * @var string|null
     */
    private $photosPublicUrl;

    /**
     * TaskToCheckDTO constructor.
     */
    public function __construct()
    {
        $this->failureFactors = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     * @return TaskToCheckDTO
     */
    public function setId(?string $id): TaskToCheckDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getTaskType(): int
    {
        return $this->taskType;
    }

    /**
     * @param int $taskType
     * @return TaskToCheckDTO
     */
    public function setTaskType(int $taskType): TaskToCheckDTO
    {
        $this->taskType = $taskType;
        return $this;
    }

    /**
     * @return UserInterface|null
     */
    public function getExecutor(): ?UserInterface
    {
        return $this->executor;
    }

    /**
     * @param UserInterface|null $executor
     * @return TaskToCheckDTO
     */
    public function setExecutor(?UserInterface $executor): TaskToCheckDTO
    {
        $this->executor = $executor;
        return $this;
    }

    /**
     * @return int
     */
    public function getPriority(): int
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     * @return TaskToCheckDTO
     */
    public function setPriority(int $priority): TaskToCheckDTO
    {
        $this->priority = $priority;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getDeadline(): ?DateTimeInterface
    {
        return $this->deadline;
    }

    /**
     * @param DateTimeInterface|null $deadline
     * @return TaskToCheckDTO
     */
    public function setDeadline(?DateTimeInterface $deadline): TaskToCheckDTO
    {
        $this->deadline = $deadline;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getExecutedDate(): ?DateTimeInterface
    {
        return $this->executedDate;
    }

    /**
     * @param DateTimeInterface|null $executedDate
     * @return TaskToCheckDTO
     */
    public function setExecutedDate(?DateTimeInterface $executedDate): TaskToCheckDTO
    {
        $this->executedDate = $executedDate;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getPeriod(): ?DateTimeInterface
    {
        return $this->period;
    }

    /**
     * @param DateTimeInterface|null $period
     * @return TaskToCheckDTO
     */
    public function setPeriod(?DateTimeInterface $period): TaskToCheckDTO
    {
        $this->period = $period;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return TaskToCheckDTO
     */
    public function setStatus(int $status): TaskToCheckDTO
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getCoordinateX(): ?float
    {
        return $this->coordinateX;
    }

    /**
     * @param float|null $coordinateX
     * @return TaskToCheckDTO
     */
    public function setCoordinateX(?float $coordinateX): TaskToCheckDTO
    {
        $this->coordinateX = $coordinateX;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getCoordinateY(): ?float
    {
        return $this->coordinateY;
    }

    /**
     * @param float|null $coordinateY
     * @return TaskToCheckDTO
     */
    public function setCoordinateY(?float $coordinateY): TaskToCheckDTO
    {
        $this->coordinateY = $coordinateY;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOperatorComment(): ?string
    {
        return $this->operatorComment;
    }

    /**
     * @param string|null $operatorComment
     * @return TaskToCheckDTO
     */
    public function setOperatorComment(?string $operatorComment): TaskToCheckDTO
    {
        $this->operatorComment = $operatorComment;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getControllerComment(): ?string
    {
        return $this->controllerComment;
    }

    /**
     * @param string|null $controllerComment
     * @return TaskToCheckDTO
     */
    public function setControllerComment(?string $controllerComment): TaskToCheckDTO
    {
        $this->controllerComment = $controllerComment;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getFailureFactors(): Collection
    {
        return $this->failureFactors;
    }

    /**
     * @param Collection $failureFactors
     * @return TaskToCheckDTO
     */
    public function setFailureFactors(Collection $failureFactors): TaskToCheckDTO
    {
        $this->failureFactors = $failureFactors;
        return $this;
    }

    /**
     * @return array
     */
    public function getActualizedFields(): array
    {
        return $this->actualizedFields;
    }

    /**
     * @param array $actualizedFields
     * @return TaskToCheckDTO
     */
    public function setActualizedFields(array $actualizedFields): TaskToCheckDTO
    {
        $this->actualizedFields = $actualizedFields;
        return $this;
    }

    /**
     * @return array
     */
    public function getChangedFields(): array
    {
        return $this->changedFields;
    }

    /**
     * @param array $changedFields
     * @return TaskToCheckDTO
     */
    public function setChangedFields(array $changedFields): TaskToCheckDTO
    {
        $this->changedFields = $changedFields;
        return $this;
    }

    /**
     * @return \App\Entity\Main\Company|null
     */
    public function getCompany(): ?Company
    {
        return $this->company;
    }

    /**
     * @param \App\Entity\Main\Company|null $company
     * @return TaskToCheckDTO
     */
    public function setCompany(?Company $company): TaskToCheckDTO
    {
        $this->company = $company;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLocality(): ?string
    {
        return $this->locality;
    }

    /**
     * @param string|null $locality
     * @return TaskToCheckDTO
     */
    public function setLocality(?string $locality): TaskToCheckDTO
    {
        $this->locality = $locality;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStreet(): ?string
    {
        return $this->street;
    }

    /**
     * @param string|null $street
     * @return TaskToCheckDTO
     */
    public function setStreet(?string $street): TaskToCheckDTO
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getHouseNumber(): ?string
    {
        return $this->houseNumber;
    }

    /**
     * @param string|null $houseNumber
     * @return TaskToCheckDTO
     */
    public function setHouseNumber(?string $houseNumber): TaskToCheckDTO
    {
        $this->houseNumber = $houseNumber;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPorchNumber(): ?string
    {
        return $this->porchNumber;
    }

    /**
     * @param string|null $porchNumber
     * @return TaskToCheckDTO
     */
    public function setPorchNumber(?string $porchNumber): TaskToCheckDTO
    {
        $this->porchNumber = $porchNumber;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getApartmentNumber(): ?string
    {
        return $this->apartmentNumber;
    }

    /**
     * @param string|null $apartmentNumber
     * @return TaskToCheckDTO
     */
    public function setApartmentNumber(?string $apartmentNumber): TaskToCheckDTO
    {
        $this->apartmentNumber = $apartmentNumber;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getLodgersCount(): ?int
    {
        return $this->lodgersCount;
    }

    /**
     * @param int|null $lodgersCount
     * @return TaskToCheckDTO
     */
    public function setLodgersCount(?int $lodgersCount): TaskToCheckDTO
    {
        $this->lodgersCount = $lodgersCount;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getRoomsCount(): ?int
    {
        return $this->roomsCount;
    }

    /**
     * @param int|null $roomsCount
     * @return TaskToCheckDTO
     */
    public function setRoomsCount(?int $roomsCount): TaskToCheckDTO
    {
        $this->roomsCount = $roomsCount;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getSubscriberType(): ?int
    {
        return $this->subscriberType;
    }

    /**
     * @param int|null $subscriberType
     * @return TaskToCheckDTO
     */
    public function setSubscriberType(?int $subscriberType): TaskToCheckDTO
    {
        $this->subscriberType = $subscriberType;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubscriberTitle(): ?string
    {
        return $this->subscriberTitle;
    }

    /**
     * @param string $subscriberTitle
     * @return TaskToCheckDTO
     */
    public function setSubscriberTitle(?string $subscriberTitle): TaskToCheckDTO
    {
        $this->subscriberTitle = $subscriberTitle;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubscriberName(): ?string
    {
        return $this->subscriberName;
    }

    /**
     * @param string|null $subscriberName
     * @return TaskToCheckDTO
     */
    public function setSubscriberName(?string $subscriberName): TaskToCheckDTO
    {
        $this->subscriberName = $subscriberName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubscriberSurname(): ?string
    {
        return $this->subscriberSurname;
    }

    /**
     * @param string|null $subscriberSurname
     * @return TaskToCheckDTO
     */
    public function setSubscriberSurname(?string $subscriberSurname): TaskToCheckDTO
    {
        $this->subscriberSurname = $subscriberSurname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubscriberPatronymic(): ?string
    {
        return $this->subscriberPatronymic;
    }

    /**
     * @param string|null $subscriberPatronymic
     * @return TaskToCheckDTO
     */
    public function setSubscriberPatronymic(?string $subscriberPatronymic): TaskToCheckDTO
    {
        $this->subscriberPatronymic = $subscriberPatronymic;
        return $this;
    }

    /**
     * @return array
     */
    public function getSubscriberPhones(): array
    {
        return $this->subscriberPhones;
    }

    /**
     * @param array $subscriberPhones
     * @return TaskToCheckDTO
     */
    public function setSubscriberPhones(array $subscriberPhones): TaskToCheckDTO
    {
        $this->subscriberPhones = $subscriberPhones;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAgreementNumber(): ?string
    {
        return $this->agreementNumber;
    }

    /**
     * @param string|null $agreementNumber
     * @return TaskToCheckDTO
     */
    public function setAgreementNumber(?string $agreementNumber): TaskToCheckDTO
    {
        $this->agreementNumber = $agreementNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getCounterNumber(): ?string
    {
        return $this->counterNumber;
    }

    /**
     * @param string $counterNumber
     * @return TaskToCheckDTO
     */
    public function setCounterNumber(?string $counterNumber): TaskToCheckDTO
    {
        $this->counterNumber = $counterNumber;
        return $this;
    }

    /**
     * @return float[]|null
     */
    public function getLastCounterValue(): ?array
    {
        return $this->lastCounterValue;
    }

    /**
     * @param float[]|null $lastCounterValue
     * @return TaskToCheckDTO
     */
    public function setLastCounterValue(?array $lastCounterValue): TaskToCheckDTO
    {
        $this->lastCounterValue = $lastCounterValue;
        return $this;
    }

    /**
     * @return float[]|null
     */
    public function getCurrentCounterValue(): ?array
    {
        return $this->currentCounterValue;
    }

    /**
     * @param float[]|null $currentCounterValue
     * @return TaskToCheckDTO
     */
    public function setCurrentCounterValue(?array $currentCounterValue): TaskToCheckDTO
    {
        $this->currentCounterValue = $currentCounterValue;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getConsumption(): ?float
    {
        return $this->consumption;
    }

    /**
     * @param float|null $consumption
     * @return TaskToCheckDTO
     */
    public function setConsumption(?float $consumption): TaskToCheckDTO
    {
        $this->consumption = $consumption;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getConsumptionCoefficient(): ?float
    {
        return $this->consumptionCoefficient;
    }

    /**
     * @param float|null $consumptionCoefficient
     * @return TaskToCheckDTO
     */
    public function setConsumptionCoefficient(?float $consumptionCoefficient): TaskToCheckDTO
    {
        $this->consumptionCoefficient = $consumptionCoefficient;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getCounterPhysicalStatus(): ?int
    {
        return $this->counterPhysicalStatus;
    }

    /**
     * @param int|null $counterPhysicalStatus
     * @return TaskToCheckDTO
     */
    public function setCounterPhysicalStatus(?int $counterPhysicalStatus): TaskToCheckDTO
    {
        $this->counterPhysicalStatus = $counterPhysicalStatus;
        return $this;
    }

    /**
     * @return Substation|null
     */
    public function getSubstation(): ?Substation
    {
        return $this->substation;
    }

    /**
     * @param Substation|null $substation
     * @return TaskToCheckDTO
     */
    public function setSubstation(?Substation $substation): TaskToCheckDTO
    {
        $this->substation = $substation;
        return $this;
    }

    /**
     * @return Feeder|null
     */
    public function getFeeder(): ?Feeder
    {
        return $this->feeder;
    }

    /**
     * @param Feeder|null $feeder
     * @return TaskToCheckDTO
     */
    public function setFeeder(?Feeder $feeder): TaskToCheckDTO
    {
        $this->feeder = $feeder;
        return $this;
    }

    /**
     * @return \App\Entity\Main\Transformer|null
     */
    public function getTransformer(): ?Transformer
    {
        return $this->transformer;
    }

    /**
     * @param \App\Entity\Main\Transformer|null $transformer
     * @return TaskToCheckDTO
     */
    public function setTransformer(?Transformer $transformer): TaskToCheckDTO
    {
        $this->transformer = $transformer;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLineNumber(): ?string
    {
        return $this->lineNumber;
    }

    /**
     * @param string|null $lineNumber
     * @return TaskToCheckDTO
     */
    public function setLineNumber(?string $lineNumber): TaskToCheckDTO
    {
        $this->lineNumber = $lineNumber;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getElectricPoleNumber(): ?string
    {
        return $this->electricPoleNumber;
    }

    /**
     * @param string|null $electricPoleNumber
     * @return TaskToCheckDTO
     */
    public function setElectricPoleNumber(?string $electricPoleNumber): TaskToCheckDTO
    {
        $this->electricPoleNumber = $electricPoleNumber;
        return $this;
    }

    /**
     * @return CounterType|null
     */
    public function getCounterType(): ?CounterType
    {
        return $this->counterType;
    }

    /**
     * @param CounterType|null $counterType
     * @return TaskToCheckDTO
     */
    public function setCounterType(?CounterType $counterType): TaskToCheckDTO
    {
        $this->counterType = $counterType;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTerminalSeal(): ?string
    {
        return $this->terminalSeal;
    }

    /**
     * @param string|null $terminalSeal
     * @return TaskToCheckDTO
     */
    public function setTerminalSeal(?string $terminalSeal): TaskToCheckDTO
    {
        $this->terminalSeal = $terminalSeal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAntiMagneticSeal(): ?string
    {
        return $this->antiMagneticSeal;
    }

    /**
     * @param string|null $antiMagneticSeal
     * @return TaskToCheckDTO
     */
    public function setAntiMagneticSeal(?string $antiMagneticSeal): TaskToCheckDTO
    {
        $this->antiMagneticSeal = $antiMagneticSeal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSideSeal(): ?string
    {
        return $this->sideSeal;
    }

    /**
     * @param string|null $sideSeal
     * @return TaskToCheckDTO
     */
    public function setSideSeal(?string $sideSeal): TaskToCheckDTO
    {
        $this->sideSeal = $sideSeal;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getPhotos(): array
    {
        return $this->photos;
    }

    /**
     * @param string[] $photos
     * @return TaskToCheckDTO
     */
    public function setPhotos(array $photos): TaskToCheckDTO
    {
        $this->photos = $photos;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTemporaryAddress(): ?string
    {
        return $this->temporaryAddress;
    }

    /**
     * @param string|null $temporaryAddress
     * @return TaskToCheckDTO
     */
    public function setTemporaryAddress(?string $temporaryAddress): TaskToCheckDTO
    {
        $this->temporaryAddress = $temporaryAddress;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTemporaryCounterType(): ?string
    {
        return $this->temporaryCounterType;
    }

    /**
     * @param string|null $temporaryCounterType
     * @return TaskToCheckDTO
     */
    public function setTemporaryCounterType(?string $temporaryCounterType): TaskToCheckDTO
    {
        $this->temporaryCounterType = $temporaryCounterType;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTemporarySubstation(): ?string
    {
        return $this->temporarySubstation;
    }

    /**
     * @param string|null $temporarySubstation
     * @return TaskToCheckDTO
     */
    public function setTemporarySubstation(?string $temporarySubstation): TaskToCheckDTO
    {
        $this->temporarySubstation = $temporarySubstation;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTemporaryTransformer(): ?string
    {
        return $this->temporaryTransformer;
    }

    /**
     * @param string|null $temporaryTransformer
     * @return TaskToCheckDTO
     */
    public function setTemporaryTransformer(?string $temporaryTransformer): TaskToCheckDTO
    {
        $this->temporaryTransformer = $temporaryTransformer;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTemporaryFeeder(): ?string
    {
        return $this->temporaryFeeder;
    }

    /**
     * @param string|null $temporaryFeeder
     * @return TaskToCheckDTO
     */
    public function setTemporaryFeeder(?string $temporaryFeeder): TaskToCheckDTO
    {
        $this->temporaryFeeder = $temporaryFeeder;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getChangedCounterNumber(): ?string
    {
        return $this->changedCounterNumber;
    }

    /**
     * @param string|null $changedCounterNumber
     * @return TaskToCheckDTO
     */
    public function setChangedCounterNumber(?string $changedCounterNumber): TaskToCheckDTO
    {
        $this->changedCounterNumber = $changedCounterNumber;
        return $this;
    }

    /**
     * @return \App\Entity\Main\CounterType|null
     */
    public function getChangedCounterType(): ?CounterType
    {
        return $this->changedCounterType;
    }

    /**
     * @param CounterType|null $changedCounterType
     * @return TaskToCheckDTO
     */
    public function setChangedCounterType(?CounterType $changedCounterType): TaskToCheckDTO
    {
        $this->changedCounterType = $changedCounterType;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getChangedCounterTypeString(): ?string
    {
        return $this->changedCounterTypeString;
    }

    /**
     * @param string|null $changedCounterTypeString
     * @return TaskToCheckDTO
     */
    public function setChangedCounterTypeString(?string $changedCounterTypeString): TaskToCheckDTO
    {
        $this->changedCounterTypeString = $changedCounterTypeString;
        return $this;
    }
    /**
     * @return string|null
     */
    public function getChangedTerminalSeal(): ?string
    {
        return $this->changedTerminalSeal;
    }

    /**
     * @param string|null $changedTerminalSeal
     * @return TaskToCheckDTO
     */
    public function setChangedTerminalSeal(?string $changedTerminalSeal): TaskToCheckDTO
    {
        $this->changedTerminalSeal = $changedTerminalSeal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getChangedAntimagneticSeal(): ?string
    {
        return $this->changedAntimagneticSeal;
    }

    /**
     * @param string|null $changedAntimagneticSeal
     * @return TaskToCheckDTO
     */
    public function setChangedAntimagneticSeal(?string $changedAntimagneticSeal): TaskToCheckDTO
    {
        $this->changedAntimagneticSeal = $changedAntimagneticSeal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getChangedSideSeal(): ?string
    {
        return $this->changedSideSeal;
    }

    /**
     * @param string|null $changedSideSeal
     * @return TaskToCheckDTO
     */
    public function setChangedSideSeal(?string $changedSideSeal): TaskToCheckDTO
    {
        $this->changedSideSeal = $changedSideSeal;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getChangedLodgersCount(): ?int
    {
        return $this->changedLodgersCount;
    }

    /**
     * @param int|null $changedLodgersCount
     * @return TaskToCheckDTO
     */
    public function setChangedLodgersCount(?int $changedLodgersCount): TaskToCheckDTO
    {
        $this->changedLodgersCount = $changedLodgersCount;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getChangedRoomsCount(): ?int
    {
        return $this->changedRoomsCount;
    }

    /**
     * @param int|null $changedRoomsCount
     * @return TaskToCheckDTO
     */
    public function setChangedRoomsCount(?int $changedRoomsCount): TaskToCheckDTO
    {
        $this->changedRoomsCount = $changedRoomsCount;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getNewCoordinateX(): ?float
    {
        return $this->newCoordinateX;
    }

    /**
     * @param float|null $newCoordinateX
     * @return TaskToCheckDTO
     */
    public function setNewCoordinateX(?float $newCoordinateX): TaskToCheckDTO
    {
        $this->newCoordinateX = $newCoordinateX;
        return $this;
    }

    /**
     * @return float
     */
    public function getNewCoordinateY(): float
    {
        return $this->newCoordinateY;
    }

    /**
     * @param float|null $newCoordinateY
     * @return TaskToCheckDTO
     */
    public function setNewCoordinateY(?float $newCoordinateY): TaskToCheckDTO
    {
        $this->newCoordinateY = $newCoordinateY;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhotosPublicUrl(): ?string
    {
        return $this->photosPublicUrl;
    }

    /**
     * @param string|null $photosPublicUrl
     * @return TaskToCheckDTO
     */
    public function setPhotosPublicUrl(?string $photosPublicUrl): TaskToCheckDTO
    {
        $this->photosPublicUrl = $photosPublicUrl;
        return $this;
    }
}