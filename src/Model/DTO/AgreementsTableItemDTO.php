<?php

namespace App\Model\DTO;

use App\Entity\Main\Agreement;

class AgreementsTableItemDTO extends Agreement
{
    private ?int $counterCount = null;

    /**
     * @return int|null
     */
    public function getCounterCount(): ?int
    {
        return $this->counterCount;
    }

    /**
     * @param int|null $counterCount
     * @return AgreementsTableItemDTO
     */
    public function setCounterCount(?int $counterCount): AgreementsTableItemDTO
    {
        $this->counterCount = $counterCount;
        return $this;
    }
}