<?php


namespace App\Model\DTO;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class UserDTO extends AbstractDTO
{
    /**
     * ID
     * @var string
     */
    private $id;

    /**
     * Имя пользователя (никнейм)
     * @var string
     */
    private $username;

    /**
     * Роли
     * @var array
     */
    private $roles;

    /**
     * Группы
     * @var Collection
     */
    private $groups;

    /**
     * Имя
     * @var string|null
     */
    private $name;

    /**
     * Фамилия
     * @var string|null
     */
    private $surname;

    /**
     * Отчество
     * @var string|null
     */
    private $patronymic;

    /**
     * Телефон
     * @var string|null
     */
    private $phone;

    /**
     * Адрес
     * @var string|null
     */
    private $address;

    /**
     * Рабочий статус
     * @var int|null
     */
    private $workStatus;

    /**
     * Массив ID компаний (отделений)
     * @var string[]|null
     */
    private $companies;

    /**
     * Количество задач к выполнению
     * @var int|null
     */
    private $taskToWorkCount;

    /**
     * Количество задач к проверке (по компаниям, для операторов)
     * @var int|null
     */
    private $companyTaskToCheckCount;

    public function __construct()
    {
        $this->groups = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return UserDTO
     */
    public function setId(string $id): UserDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return UserDTO
     */
    public function setUsername(string $username): UserDTO
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @param array $roles
     * @return UserDTO
     */
    public function setRoles(array $roles): UserDTO
    {
        $this->roles = $roles;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getGroups(): Collection
    {
        return $this->groups;
    }

    /**
     * @param Collection $groups
     * @return UserDTO
     */
    public function setGroups(Collection $groups): UserDTO
    {
        $this->groups = $groups;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return UserDTO
     */
    public function setName(?string $name): UserDTO
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSurname(): ?string
    {
        return $this->surname;
    }

    /**
     * @param string|null $surname
     * @return UserDTO
     */
    public function setSurname(?string $surname): UserDTO
    {
        $this->surname = $surname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPatronymic(): ?string
    {
        return $this->patronymic;
    }

    /**
     * @param string|null $patronymic
     * @return UserDTO
     */
    public function setPatronymic(?string $patronymic): UserDTO
    {
        $this->patronymic = $patronymic;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     * @return UserDTO
     */
    public function setPhone(?string $phone): UserDTO
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     * @return UserDTO
     */
    public function setAddress(?string $address): UserDTO
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getWorkStatus(): ?int
    {
        return $this->workStatus;
    }

    /**
     * @param int|null $workStatus
     * @return UserDTO
     */
    public function setWorkStatus(?int $workStatus): UserDTO
    {
        $this->workStatus = $workStatus;
        return $this;
    }

    /**
     * @return string[]|null
     */
    public function getCompanies(): ?array
    {
        return $this->companies;
    }

    /**
     * @param string[]|null $companies
     * @return UserDTO
     */
    public function setCompanies(?array $companies): UserDTO
    {
        $this->companies = $companies;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getTaskToWorkCount(): ?int
    {
        return $this->taskToWorkCount;
    }

    /**
     * @param int|null $taskToWorkCount
     * @return UserDTO
     */
    public function setTaskToWorkCount(?int $taskToWorkCount): UserDTO
    {
        $this->taskToWorkCount = $taskToWorkCount;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getCompanyTaskToCheckCount(): ?int
    {
        return $this->companyTaskToCheckCount;
    }

    /**
     * @param int|null $companyTaskToCheckCount
     * @return UserDTO
     */
    public function setCompanyTaskToCheckCount(?int $companyTaskToCheckCount): UserDTO
    {
        $this->companyTaskToCheckCount = $companyTaskToCheckCount;
        return $this;
    }
}