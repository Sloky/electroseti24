<?php


namespace App\Model\DTO;


use App\Entity\Main\Company;
use App\Entity\Main\CounterType;
use App\Entity\Main\Feeder;
use App\Entity\Main\Substation;
use App\Entity\Main\Transformer;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Security\Core\User\UserInterface;

class CompletedTaskDTO
{
    /**
     * ID
     * @var string|null
     */
    private $id;

    /**
     * Тип задачи
     * @var integer
     */
    private $taskType = 0;

    /**
     * Исполнитель (контролёр)
     * @var UserInterface|null
     */
    private $executor;

    /**
     * Приоритет
     * @var integer
     */
    private $priority = 0;

    /**
     * Срок выполнения
     * @var DateTimeInterface|null
     */
    private $deadline;

    /**
     * Дата и время выполнения задачи
     * @var DateTimeInterface|null
     */
    private $executedDate;

    /**
     * Период действия задачи (отчетный период)
     * @var DateTimeInterface|null
     */
    private $period;

    /**
     * Статус задачи (Не выполнено, к проверке, принято)
     * @var integer
     */
    private $status = 0;

    /**
     * Координаты оси X
     * @var float|null
     */
    private $coordinateX;

    /**
     * Координаты оси Y
     * @var float|null
     */
    private $coordinateY;

    /**
     * Примечание от оператора
     * @var string|null
     */
    private $operatorComment;

    /**
     * Примечание от контролёра
     * @var string|null
     */
    private $controllerComment;

    /**
     * Фактор невыполнения задачи
     * @var Collection
     */
    private $failureFactors;

    /**
     * Поля для актуализации
     * @var array
     */
    private $actualizedFields = [];

    /**
     * Изменённые поля (в мобильном приложении)
     * @var array
     */
    private $changedFields = [];

    /**
     * Отделение
     * @var Company|null
     */
    private $company;

    /**
     * Населенный пункт
     * @var string|null
     */
    private $locality;

    /**
     * Улица
     * @var string|null
     */
    private $street;

    /**
     * Номер дома
     * @var string|null
     */
    private $houseNumber;

    /**
     * Номер подъезда
     * @var string|null
     */
    private $porchNumber;

    /**
     * Номер квартиры
     * @var string|null
     */
    private $apartmentNumber;

    /**
     * Количество жильцов
     * @var integer|null
     */
    private $lodgersCount;

    /**
     * Количество комнат
     * @var integer|null
     */
    private $roomsCount;

    /**
     * Тип абонента
     * @var integer|null
     */
    private $subscriberType = 0;

    /**
     * Наименование абонента
     * @var string|null
     */
    private $subscriberTitle;

    /**
     * Имя абонента (для физ. лиц)
     * @var string|null
     */
    private $subscriberName;

    /**
     * Фамилия абонента
     * @var string|null
     */
    private $subscriberSurname;

    /**
     * Отчество абонента
     * @var string|null
     */
    private $subscriberPatronymic;

    /**
     * Телефоны абонента
     * @var array
     */
    private $subscriberPhones = [];

    /**
     * Номер договора
     * @var string|null
     */
    private $agreementNumber;

    /**
     * Номер узла учета
     * @var string|null
     */
    private $counterNumber;

    /**
     * Последнее значении показаний УУ
     * @var float[]|null
     */
    private $lastCounterValue;

    /**
     * Текущее значение показаний УУ
     * @var float[]|null
     */
    private $currentCounterValue;

    /**
     * Расход потребления
     * @var float|null
     */
    private $consumption;

    /**
     * Коэффициент потребления
     * @var float|null
     */
    private $consumptionCoefficient;

    /**
     * Физическое состояние УУ (Возможность снять показания)
     * @var integer|null
     */
    private $counterPhysicalStatus = 0;

    /**
     * Подстанция
     * @var Substation|null
     */
    private $substation;

    /**
     * Фидер
     * @var \App\Entity\Main\Feeder|null
     */
    private $feeder;

    /**
     * Трансформатор
     * @var Transformer|null
     */
    private $transformer;

    /**
     * Номер линии
     * @var string|null
     */
    private $lineNumber;

    /**
     * Номер столба
     * @var string|null
     */
    private $electricPoleNumber;

    /**
     * Тип счетчика
     * @var \App\Entity\Main\CounterType|null
     */
    private $counterType;

    /**
     * Клеммная пломба
     * @var string|null
     */
    private $terminalSeal;

    /**
     * Антимагнитная пломба
     * @var string|null
     */
    private $antiMagneticSeal;

    /**
     * Боковая пломба
     * @var string|null
     */
    private $sideSeal;

    /**
     * Фотографии
     * @var Collection
     */
    private $photos = [];

    /**
     * Временный вариант адреса
     * @var string|null
     */
    private $temporaryAddress;

    /**
     * Временный вариант модели счётчика
     * @var string|null
     */
    private $temporaryCounterType;

    /**
     * Временный вариант подстанции
     * @var string|null
     */
    private $temporarySubstation;

    /**
     * Временный вариант трансформатора
     * @var string|null
     */
    private $temporaryTransformer;

    /**
     * Временный вариант фидера
     * @var string|null
     */
    private $temporaryFeeder;

    /**
     * Измененный номер УУ
     * @var string|null
     */
    private $changedCounterNumber;

    /**
     * Новый тип счётчика
     * @var \App\Entity\Main\CounterType|null
     */
    private $changedCounterType;

    /**
     * Измененный тип (модель) УУ
     * @var string|null
     */
    private $changedCounterTypeString;

    /**
     * Измененная клеммная пломба
     * @var string|null
     */
    private $changedTerminalSeal;

    /**
     * Измененная антимагнитная пломба
     * @var string|null
     */
    private $changedAntimagneticSeal;

    /**
     * Измененная боковая пломба
     * @var string|null
     */
    private $changedSideSeal;

    /**
     * Измененное количество жильцов
     * @var integer|null
     */
    private $changedLodgersCount;

    /**
     * Измененное количество комнат
     * @var integer|null
     */
    private $changedRoomsCount;

    /**
     * Координата X места выполнения задачи (фотофиксации счетчика)
     * @var float|null
     */
    private $newCoordinateX;

    /**
     * Координата Y места выполнения задачи
     * @var float|null
     */
    private $newCoordinateY;

    /**
     * Дата принятия задачи
     * @var DateTimeInterface|null
     */
    private $acceptanceDate;

    /**
     * Пользователь (оператор), который принял задачу
     * @var UserInterface|null
     */
    private $operator;

    /**
     * Публичный адрес для фотографий
     * @var string|null
     */
    private $photosPublicUrl;

    /**
     * CompletedTaskDTO constructor.
     */
    public function __construct()
    {
        $this->failureFactors = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     * @return CompletedTaskDTO
     */
    public function setId(?string $id): CompletedTaskDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getTaskType(): int
    {
        return $this->taskType;
    }

    /**
     * @param int $taskType
     * @return CompletedTaskDTO
     */
    public function setTaskType(int $taskType): CompletedTaskDTO
    {
        $this->taskType = $taskType;
        return $this;
    }

    /**
     * @return UserInterface|null
     */
    public function getExecutor(): ?UserInterface
    {
        return $this->executor;
    }

    /**
     * @param UserInterface|null $executor
     * @return CompletedTaskDTO
     */
    public function setExecutor(?UserInterface $executor): CompletedTaskDTO
    {
        $this->executor = $executor;
        return $this;
    }

    /**
     * @return int
     */
    public function getPriority(): int
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     * @return CompletedTaskDTO
     */
    public function setPriority(int $priority): CompletedTaskDTO
    {
        $this->priority = $priority;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getDeadline(): ?DateTimeInterface
    {
        return $this->deadline;
    }

    /**
     * @param DateTimeInterface|null $deadline
     * @return CompletedTaskDTO
     */
    public function setDeadline(?DateTimeInterface $deadline): CompletedTaskDTO
    {
        $this->deadline = $deadline;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getExecutedDate(): ?DateTimeInterface
    {
        return $this->executedDate;
    }

    /**
     * @param DateTimeInterface|null $executedDate
     * @return CompletedTaskDTO
     */
    public function setExecutedDate(?DateTimeInterface $executedDate): CompletedTaskDTO
    {
        $this->executedDate = $executedDate;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getPeriod(): ?DateTimeInterface
    {
        return $this->period;
    }

    /**
     * @param DateTimeInterface|null $period
     * @return CompletedTaskDTO
     */
    public function setPeriod(?DateTimeInterface $period): CompletedTaskDTO
    {
        $this->period = $period;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return CompletedTaskDTO
     */
    public function setStatus(int $status): CompletedTaskDTO
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getCoordinateX(): ?float
    {
        return $this->coordinateX;
    }

    /**
     * @param float|null $coordinateX
     * @return CompletedTaskDTO
     */
    public function setCoordinateX(?float $coordinateX): CompletedTaskDTO
    {
        $this->coordinateX = $coordinateX;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getCoordinateY(): ?float
    {
        return $this->coordinateY;
    }

    /**
     * @param float|null $coordinateY
     * @return CompletedTaskDTO
     */
    public function setCoordinateY(?float $coordinateY): CompletedTaskDTO
    {
        $this->coordinateY = $coordinateY;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOperatorComment(): ?string
    {
        return $this->operatorComment;
    }

    /**
     * @param string|null $operatorComment
     * @return CompletedTaskDTO
     */
    public function setOperatorComment(?string $operatorComment): CompletedTaskDTO
    {
        $this->operatorComment = $operatorComment;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getControllerComment(): ?string
    {
        return $this->controllerComment;
    }

    /**
     * @param string|null $controllerComment
     * @return CompletedTaskDTO
     */
    public function setControllerComment(?string $controllerComment): CompletedTaskDTO
    {
        $this->controllerComment = $controllerComment;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getFailureFactors(): Collection
    {
        return $this->failureFactors;
    }

    /**
     * @param Collection $failureFactors
     * @return CompletedTaskDTO
     */
    public function setFailureFactors(Collection $failureFactors): CompletedTaskDTO
    {
        $this->failureFactors = $failureFactors;
        return $this;
    }

    /**
     * @return array
     */
    public function getActualizedFields(): array
    {
        return $this->actualizedFields;
    }

    /**
     * @param array $actualizedFields
     * @return CompletedTaskDTO
     */
    public function setActualizedFields(array $actualizedFields): CompletedTaskDTO
    {
        $this->actualizedFields = $actualizedFields;
        return $this;
    }

    /**
     * @return array
     */
    public function getChangedFields(): array
    {
        return $this->changedFields;
    }

    /**
     * @param array $changedFields
     * @return CompletedTaskDTO
     */
    public function setChangedFields(array $changedFields): CompletedTaskDTO
    {
        $this->changedFields = $changedFields;
        return $this;
    }

    /**
     * @return \App\Entity\Main\Company|null
     */
    public function getCompany(): ?Company
    {
        return $this->company;
    }

    /**
     * @param \App\Entity\Main\Company|null $company
     * @return CompletedTaskDTO
     */
    public function setCompany(?Company $company): CompletedTaskDTO
    {
        $this->company = $company;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLocality(): ?string
    {
        return $this->locality;
    }

    /**
     * @param string|null $locality
     * @return CompletedTaskDTO
     */
    public function setLocality(?string $locality): CompletedTaskDTO
    {
        $this->locality = $locality;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStreet(): ?string
    {
        return $this->street;
    }

    /**
     * @param string|null $street
     * @return CompletedTaskDTO
     */
    public function setStreet(?string $street): CompletedTaskDTO
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getHouseNumber(): ?string
    {
        return $this->houseNumber;
    }

    /**
     * @param string|null $houseNumber
     * @return CompletedTaskDTO
     */
    public function setHouseNumber(?string $houseNumber): CompletedTaskDTO
    {
        $this->houseNumber = $houseNumber;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPorchNumber(): ?string
    {
        return $this->porchNumber;
    }

    /**
     * @param string|null $porchNumber
     * @return CompletedTaskDTO
     */
    public function setPorchNumber(?string $porchNumber): CompletedTaskDTO
    {
        $this->porchNumber = $porchNumber;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getApartmentNumber(): ?string
    {
        return $this->apartmentNumber;
    }

    /**
     * @param string|null $apartmentNumber
     * @return CompletedTaskDTO
     */
    public function setApartmentNumber(?string $apartmentNumber): CompletedTaskDTO
    {
        $this->apartmentNumber = $apartmentNumber;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getLodgersCount(): ?int
    {
        return $this->lodgersCount;
    }

    /**
     * @param int|null $lodgersCount
     * @return CompletedTaskDTO
     */
    public function setLodgersCount(?int $lodgersCount): CompletedTaskDTO
    {
        $this->lodgersCount = $lodgersCount;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getRoomsCount(): ?int
    {
        return $this->roomsCount;
    }

    /**
     * @param int|null $roomsCount
     * @return CompletedTaskDTO
     */
    public function setRoomsCount(?int $roomsCount): CompletedTaskDTO
    {
        $this->roomsCount = $roomsCount;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getSubscriberType(): ?int
    {
        return $this->subscriberType;
    }

    /**
     * @param int|null $subscriberType
     * @return CompletedTaskDTO
     */
    public function setSubscriberType(?int $subscriberType): CompletedTaskDTO
    {
        $this->subscriberType = $subscriberType;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubscriberTitle(): ?string
    {
        return $this->subscriberTitle;
    }

    /**
     * @param string $subscriberTitle
     * @return CompletedTaskDTO
     */
    public function setSubscriberTitle(?string $subscriberTitle): CompletedTaskDTO
    {
        $this->subscriberTitle = $subscriberTitle;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubscriberName(): ?string
    {
        return $this->subscriberName;
    }

    /**
     * @param string|null $subscriberName
     * @return CompletedTaskDTO
     */
    public function setSubscriberName(?string $subscriberName): CompletedTaskDTO
    {
        $this->subscriberName = $subscriberName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubscriberSurname(): ?string
    {
        return $this->subscriberSurname;
    }

    /**
     * @param string|null $subscriberSurname
     * @return CompletedTaskDTO
     */
    public function setSubscriberSurname(?string $subscriberSurname): CompletedTaskDTO
    {
        $this->subscriberSurname = $subscriberSurname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubscriberPatronymic(): ?string
    {
        return $this->subscriberPatronymic;
    }

    /**
     * @param string|null $subscriberPatronymic
     * @return CompletedTaskDTO
     */
    public function setSubscriberPatronymic(?string $subscriberPatronymic): CompletedTaskDTO
    {
        $this->subscriberPatronymic = $subscriberPatronymic;
        return $this;
    }

    /**
     * @return array
     */
    public function getSubscriberPhones(): array
    {
        return $this->subscriberPhones;
    }

    /**
     * @param array $subscriberPhones
     * @return CompletedTaskDTO
     */
    public function setSubscriberPhones(array $subscriberPhones): CompletedTaskDTO
    {
        $this->subscriberPhones = $subscriberPhones;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAgreementNumber(): ?string
    {
        return $this->agreementNumber;
    }

    /**
     * @param string|null $agreementNumber
     * @return CompletedTaskDTO
     */
    public function setAgreementNumber(?string $agreementNumber): CompletedTaskDTO
    {
        $this->agreementNumber = $agreementNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getCounterNumber(): ?string
    {
        return $this->counterNumber;
    }

    /**
     * @param string $counterNumber
     * @return CompletedTaskDTO
     */
    public function setCounterNumber(?string $counterNumber): CompletedTaskDTO
    {
        $this->counterNumber = $counterNumber;
        return $this;
    }

    /**
     * @return float[]|null
     */
    public function getLastCounterValue(): ?array
    {
        return $this->lastCounterValue;
    }

    /**
     * @param float[]|null $lastCounterValue
     * @return CompletedTaskDTO
     */
    public function setLastCounterValue(?array $lastCounterValue): CompletedTaskDTO
    {
        $this->lastCounterValue = $lastCounterValue;
        return $this;
    }

    /**
     * @return float[]|null
     */
    public function getCurrentCounterValue(): ?array
    {
        return $this->currentCounterValue;
    }

    /**
     * @param float[]|null $currentCounterValue
     * @return CompletedTaskDTO
     */
    public function setCurrentCounterValue(?array $currentCounterValue): CompletedTaskDTO
    {
        $this->currentCounterValue = $currentCounterValue;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getConsumption(): ?float
    {
        return $this->consumption;
    }

    /**
     * @param float|null $consumption
     * @return CompletedTaskDTO
     */
    public function setConsumption(?float $consumption): CompletedTaskDTO
    {
        $this->consumption = $consumption;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getConsumptionCoefficient(): ?float
    {
        return $this->consumptionCoefficient;
    }

    /**
     * @param float|null $consumptionCoefficient
     * @return CompletedTaskDTO
     */
    public function setConsumptionCoefficient(?float $consumptionCoefficient): CompletedTaskDTO
    {
        $this->consumptionCoefficient = $consumptionCoefficient;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getCounterPhysicalStatus(): ?int
    {
        return $this->counterPhysicalStatus;
    }

    /**
     * @param int|null $counterPhysicalStatus
     * @return CompletedTaskDTO
     */
    public function setCounterPhysicalStatus(?int $counterPhysicalStatus): CompletedTaskDTO
    {
        $this->counterPhysicalStatus = $counterPhysicalStatus;
        return $this;
    }

    /**
     * @return Substation|null
     */
    public function getSubstation(): ?Substation
    {
        return $this->substation;
    }

    /**
     * @param \App\Entity\Main\Substation|null $substation
     * @return CompletedTaskDTO
     */
    public function setSubstation(?Substation $substation): CompletedTaskDTO
    {
        $this->substation = $substation;
        return $this;
    }

    /**
     * @return \App\Entity\Main\Feeder|null
     */
    public function getFeeder(): ?Feeder
    {
        return $this->feeder;
    }

    /**
     * @param Feeder|null $feeder
     * @return CompletedTaskDTO
     */
    public function setFeeder(?Feeder $feeder): CompletedTaskDTO
    {
        $this->feeder = $feeder;
        return $this;
    }

    /**
     * @return Transformer|null
     */
    public function getTransformer(): ?Transformer
    {
        return $this->transformer;
    }

    /**
     * @param Transformer|null $transformer
     * @return CompletedTaskDTO
     */
    public function setTransformer(?Transformer $transformer): CompletedTaskDTO
    {
        $this->transformer = $transformer;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLineNumber(): ?string
    {
        return $this->lineNumber;
    }

    /**
     * @param string|null $lineNumber
     * @return CompletedTaskDTO
     */
    public function setLineNumber(?string $lineNumber): CompletedTaskDTO
    {
        $this->lineNumber = $lineNumber;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getElectricPoleNumber(): ?string
    {
        return $this->electricPoleNumber;
    }

    /**
     * @param string|null $electricPoleNumber
     * @return CompletedTaskDTO
     */
    public function setElectricPoleNumber(?string $electricPoleNumber): CompletedTaskDTO
    {
        $this->electricPoleNumber = $electricPoleNumber;
        return $this;
    }

    /**
     * @return \App\Entity\Main\CounterType|null
     */
    public function getCounterType(): ?CounterType
    {
        return $this->counterType;
    }

    /**
     * @param \App\Entity\Main\CounterType|null $counterType
     * @return CompletedTaskDTO
     */
    public function setCounterType(?CounterType $counterType): CompletedTaskDTO
    {
        $this->counterType = $counterType;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTerminalSeal(): ?string
    {
        return $this->terminalSeal;
    }

    /**
     * @param string|null $terminalSeal
     * @return CompletedTaskDTO
     */
    public function setTerminalSeal(?string $terminalSeal): CompletedTaskDTO
    {
        $this->terminalSeal = $terminalSeal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAntiMagneticSeal(): ?string
    {
        return $this->antiMagneticSeal;
    }

    /**
     * @param string|null $antiMagneticSeal
     * @return CompletedTaskDTO
     */
    public function setAntiMagneticSeal(?string $antiMagneticSeal): CompletedTaskDTO
    {
        $this->antiMagneticSeal = $antiMagneticSeal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSideSeal(): ?string
    {
        return $this->sideSeal;
    }

    /**
     * @param string|null $sideSeal
     * @return CompletedTaskDTO
     */
    public function setSideSeal(?string $sideSeal): CompletedTaskDTO
    {
        $this->sideSeal = $sideSeal;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getPhotos(): array
    {
        return $this->photos;
    }

    /**
     * @param string[] $photos
     * @return CompletedTaskDTO
     */
    public function setPhotos(array $photos): CompletedTaskDTO
    {
        $this->photos = $photos;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTemporaryAddress(): ?string
    {
        return $this->temporaryAddress;
    }

    /**
     * @param string|null $temporaryAddress
     * @return CompletedTaskDTO
     */
    public function setTemporaryAddress(?string $temporaryAddress): CompletedTaskDTO
    {
        $this->temporaryAddress = $temporaryAddress;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTemporaryCounterType(): ?string
    {
        return $this->temporaryCounterType;
    }

    /**
     * @param string|null $temporaryCounterType
     * @return CompletedTaskDTO
     */
    public function setTemporaryCounterType(?string $temporaryCounterType): CompletedTaskDTO
    {
        $this->temporaryCounterType = $temporaryCounterType;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTemporarySubstation(): ?string
    {
        return $this->temporarySubstation;
    }

    /**
     * @param string|null $temporarySubstation
     * @return CompletedTaskDTO
     */
    public function setTemporarySubstation(?string $temporarySubstation): CompletedTaskDTO
    {
        $this->temporarySubstation = $temporarySubstation;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTemporaryTransformer(): ?string
    {
        return $this->temporaryTransformer;
    }

    /**
     * @param string|null $temporaryTransformer
     * @return CompletedTaskDTO
     */
    public function setTemporaryTransformer(?string $temporaryTransformer): CompletedTaskDTO
    {
        $this->temporaryTransformer = $temporaryTransformer;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTemporaryFeeder(): ?string
    {
        return $this->temporaryFeeder;
    }

    /**
     * @param string|null $temporaryFeeder
     * @return CompletedTaskDTO
     */
    public function setTemporaryFeeder(?string $temporaryFeeder): CompletedTaskDTO
    {
        $this->temporaryFeeder = $temporaryFeeder;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getChangedCounterNumber(): ?string
    {
        return $this->changedCounterNumber;
    }

    /**
     * @param string|null $changedCounterNumber
     * @return CompletedTaskDTO
     */
    public function setChangedCounterNumber(?string $changedCounterNumber): CompletedTaskDTO
    {
        $this->changedCounterNumber = $changedCounterNumber;
        return $this;
    }

    /**
     * @return \App\Entity\Main\CounterType|null
     */
    public function getChangedCounterType(): ?CounterType
    {
        return $this->changedCounterType;
    }

    /**
     * @param CounterType|null $changedCounterType
     * @return CompletedTaskDTO
     */
    public function setChangedCounterType(?CounterType $changedCounterType): CompletedTaskDTO
    {
        $this->changedCounterType = $changedCounterType;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getChangedCounterTypeString(): ?string
    {
        return $this->changedCounterTypeString;
    }

    /**
     * @param string|null $changedCounterTypeString
     * @return CompletedTaskDTO
     */
    public function setChangedCounterTypeString(?string $changedCounterTypeString): CompletedTaskDTO
    {
        $this->changedCounterTypeString = $changedCounterTypeString;
        return $this;
    }
    /**
     * @return string|null
     */
    public function getChangedTerminalSeal(): ?string
    {
        return $this->changedTerminalSeal;
    }

    /**
     * @param string|null $changedTerminalSeal
     * @return CompletedTaskDTO
     */
    public function setChangedTerminalSeal(?string $changedTerminalSeal): CompletedTaskDTO
    {
        $this->changedTerminalSeal = $changedTerminalSeal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getChangedAntimagneticSeal(): ?string
    {
        return $this->changedAntimagneticSeal;
    }

    /**
     * @param string|null $changedAntimagneticSeal
     * @return CompletedTaskDTO
     */
    public function setChangedAntimagneticSeal(?string $changedAntimagneticSeal): CompletedTaskDTO
    {
        $this->changedAntimagneticSeal = $changedAntimagneticSeal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getChangedSideSeal(): ?string
    {
        return $this->changedSideSeal;
    }

    /**
     * @param string|null $changedSideSeal
     * @return CompletedTaskDTO
     */
    public function setChangedSideSeal(?string $changedSideSeal): CompletedTaskDTO
    {
        $this->changedSideSeal = $changedSideSeal;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getChangedLodgersCount(): ?int
    {
        return $this->changedLodgersCount;
    }

    /**
     * @param int|null $changedLodgersCount
     * @return CompletedTaskDTO
     */
    public function setChangedLodgersCount(?int $changedLodgersCount): CompletedTaskDTO
    {
        $this->changedLodgersCount = $changedLodgersCount;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getChangedRoomsCount(): ?int
    {
        return $this->changedRoomsCount;
    }

    /**
     * @param int|null $changedRoomsCount
     * @return CompletedTaskDTO
     */
    public function setChangedRoomsCount(?int $changedRoomsCount): CompletedTaskDTO
    {
        $this->changedRoomsCount = $changedRoomsCount;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getNewCoordinateX(): ?float
    {
        return $this->newCoordinateX;
    }

    /**
     * @param float|null $newCoordinateX
     * @return CompletedTaskDTO
     */
    public function setNewCoordinateX(?float $newCoordinateX): CompletedTaskDTO
    {
        $this->newCoordinateX = $newCoordinateX;
        return $this;
    }

    /**
     * @return float
     */
    public function getNewCoordinateY(): float
    {
        return $this->newCoordinateY;
    }

    /**
     * @param float|null $newCoordinateY
     * @return CompletedTaskDTO
     */
    public function setNewCoordinateY(?float $newCoordinateY): CompletedTaskDTO
    {
        $this->newCoordinateY = $newCoordinateY;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getAcceptanceDate(): ?DateTimeInterface
    {
        return $this->acceptanceDate;
    }

    /**
     * @param DateTimeInterface|null $acceptanceDate
     * @return CompletedTaskDTO
     */
    public function setAcceptanceDate(?DateTimeInterface $acceptanceDate): CompletedTaskDTO
    {
        $this->acceptanceDate = $acceptanceDate;
        return $this;
    }

    /**
     * @return UserInterface|null
     */
    public function getOperator(): ?UserInterface
    {
        return $this->operator;
    }

    /**
     * @param UserInterface|null $operator
     * @return CompletedTaskDTO
     */
    public function setOperator(?UserInterface $operator): CompletedTaskDTO
    {
        $this->operator = $operator;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhotosPublicUrl(): ?string
    {
        return $this->photosPublicUrl;
    }

    /**
     * @param string|null $photosPublicUrl
     * @return CompletedTaskDTO
     */
    public function setPhotosPublicUrl(?string $photosPublicUrl): CompletedTaskDTO
    {
        $this->photosPublicUrl = $photosPublicUrl;
        return $this;
    }
}