<?php

namespace App\Model\DTO;

use App\Entity\Main\Company;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class TPDTO
{
    /**
     * @var string|null
     */
    private ?string $id;

    /**
     * @var string|null
     */
    private ?string $title;

    /**
     * @var string|null
     */
    private ?string $feederTitle;

    /**
     * @var string|null
     */
    private ?string $substationTitle;

    /**
     * @var Collection
     */
    private Collection $users;

    /**
     * @var \App\Entity\Main\Company|null
     */
    private ?Company $company;

    /**
     * @var array|null
     */
    private ?array $coordinates;

    /**
     * @var int|null
     */
    private ?int $countersCount;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     * @return TPDTO
     */
    public function setId(?string $id): TPDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     * @return TPDTO
     */
    public function setTitle(?string $title): TPDTO
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFeederTitle(): ?string
    {
        return $this->feederTitle;
    }

    /**
     * @param string|null $feederTitle
     * @return TPDTO
     */
    public function setFeederTitle(?string $feederTitle): TPDTO
    {
        $this->feederTitle = $feederTitle;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubstationTitle(): ?string
    {
        return $this->substationTitle;
    }

    /**
     * @param string|null $substationTitle
     * @return TPDTO
     */
    public function setSubstationTitle(?string $substationTitle): TPDTO
    {
        $this->substationTitle = $substationTitle;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    /**
     * @param Collection $users
     * @return TPDTO
     */
    public function setUsers(Collection $users): TPDTO
    {
        $this->users = $users;
        return $this;
    }

    /**
     * @return Company|null
     */
    public function getCompany(): ?Company
    {
        return $this->company;
    }

    /**
     * @param Company|null $company
     * @return TPDTO
     */
    public function setCompany(?Company $company): TPDTO
    {
        $this->company = $company;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getCoordinates(): ?array
    {
        return $this->coordinates;
    }

    /**
     * @param array|null $coordinates
     * @return TPDTO
     */
    public function setCoordinates(?array $coordinates): TPDTO
    {
        $this->coordinates = $coordinates;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getCountersCount(): ?int
    {
        return $this->countersCount;
    }

    /**
     * @param int|null $countersCount
     * @return TPDTO
     */
    public function setCountersCount(?int $countersCount): TPDTO
    {
        $this->countersCount = $countersCount;
        return $this;
    }
}