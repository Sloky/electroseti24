<?php

namespace App\Model;

class BypassListRecordError implements BypassListErrorInterface
{
    /**
     * Error title
     * @var string|null
     */
    protected ?string $title;

    /**
     * Error message
     * @var string
     */
    protected string $message;

    /**
     * @param string|null $message
     * @param string|null $title
     */
    public function __construct(
        ?string $message = '',
        ?string $title = null
    )
    {
        $this->setMessage($message);
        $this->setTitle($title);
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->getTitle();
    }

    /**
     * @param string|null $title
     * @return BypassListRecordError
     */
    public function setTitle(?string $title): BypassListRecordError
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->getMessage();
    }

    /**
     * @param string $message
     * @return BypassListRecordError
     */
    public function setMessage(string $message): BypassListRecordError
    {
        $this->message = $message;
        return $this;
    }
}