<?php

namespace App\Model;

interface BypassListErrorInterface
{
    /**
     * Return error title or null
     * @return string|null
     */
    public function getTitle(): ?string;

    /**
     * Return error message
     * @return string
     */
    public function getMessage(): string;
}