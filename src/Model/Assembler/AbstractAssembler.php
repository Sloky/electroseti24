<?php

namespace App\Model\Assembler;

use App\Model\DTO\AbstractDTO;
use Doctrine\ORM\Mapping\Entity;

interface AbstractAssembler
{
    public static function create();

    public static function update();

    public static function readDTO();

    public static function writeDTO();
}