<?php

namespace App\Model\Assembler;

use App\Entity\Main\Counter;
use App\Entity\Main\CounterValue;
use App\Model\DTO\CounterCardDTO;

class CounterCardDTOAssembler
{
    /**
     * @return mixed
     */
    public static function create()
    {}

    /**
     * @return mixed
     */
    public static function update()
    {}

    /**
     * @return mixed
     */
    public static function readDTO()
    {}

    /**
     * @param \App\Entity\Main\Counter $counter
     * @param \App\Entity\Main\CounterValue|null $counterValue
     * @return CounterCardDTO
     */
    public static function writeDTO(Counter $counter, ?CounterValue $counterValue): CounterCardDTO
    {
        $counterCardDTO = new CounterCardDTO();

        $initialCounterValueId = $counter->getInitialValue() ? $counter->getInitialValue()->getId() : null;
        $initialCounterValue = $counter->getInitialValue() ? $counter->getInitialValue()->getValue()->getValues()[0] : null;
        $initialCounterValueDate = $counter->getInitialValue() ? $counter->getInitialValue()->getDate() : null;

//        $lodgersCount = $counter->getAddress() ? $counter->getAddress()->getLodgersCount() : null;
//        $roomsCount = $counter->getAddress() ? $counter->getAddress()->getRoomsCount() : null;
        $lodgersCount = $counter->getTemporaryLodgersCount();
        $roomsCount = $counter->getTemporaryRoomsCount();

        $counterCardDTO->setCounterId($counter->getId());
        $counterCardDTO->setCounterTitle($counter->getTitle());
        $counterCardDTO->setCounterPhysicalStatus($counter->getWorkStatus());
        $counterCardDTO->setCounterModel($counter->getTemporaryCounterType());
        $counterCardDTO->setTerminalSeal($counter->getTerminalSeal());
        $counterCardDTO->setAntimagneticSeal($counter->getAntiMagneticSeal());
        $counterCardDTO->setSideSeal($counter->getSideSeal());

        $counterCardDTO->setTransformer($counter->getTransformer());

        if (!empty($counter->getCoordinates()) && count($counter->getCoordinates()) > 1) {
            $counterCardDTO->setCoordinateX($counter->getCoordinates()[0]);
            $counterCardDTO->setCoordinateY($counter->getCoordinates()[1]);
        }

        $counterCardDTO->setInitialCounterValueId($initialCounterValueId);
        $counterCardDTO->setInitialCounterValue($initialCounterValue);
        $counterCardDTO->setInitialCounterValueDate($initialCounterValueDate);
        $counterCardDTO->setAddress($counter->getTemporaryAddress());
        $counterCardDTO->setTransformer($counter->getTransformer());
        $counterCardDTO->setElectricPole($counter->getElectricPoleNumber());
        $counterCardDTO->setElectricLine($counter->getElectricLine());
        $counterCardDTO->setAgreement($counter->getAgreement());
        $counterCardDTO->setSubscriber($counter->getAgreement()->getSubscriber());

        if ($counterValue) {
            $counterCardDTO->setLastCounterValueId($counterValue->getId());
            $counterCardDTO->setLastCounterValue($counterValue->getValue()->getValues()[0]);
            $counterCardDTO->setLastCounterValueDate($counterValue->getDate());
        } else {
            $counterCardDTO->setLastCounterValueId($initialCounterValueId);
            $counterCardDTO->setLastCounterValue($initialCounterValue);
            $counterCardDTO->setLastCounterValueDate($initialCounterValueDate);
        }

        $counterCardDTO->setLodgersCount($lodgersCount);
        $counterCardDTO->setRoomsCount($roomsCount);

        return $counterCardDTO;
    }
}