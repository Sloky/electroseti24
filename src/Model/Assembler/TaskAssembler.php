<?php


namespace App\Model\Assembler;


use App\Entity\Main\Task;
use App\Handler\CounterHandler;
use App\Model\DTO\TaskDTO;

class TaskAssembler
{
    /**
     * @var CounterHandler
     */
    private $counterHandler;

    public function __construct(
        CounterHandler $counterHandler
    )
    {
        $this->counterHandler = $counterHandler;
    }

    /**
     * Create task from TaskDTO
     * @param TaskDTO $taskDTO
     * @return Task
     */
    public static function create(TaskDTO $taskDTO): Task
    {
        $task = new Task();

//        $counter = static::$counterHandler->getCounterByNumber($taskDTO->getCounterNumber()) ?? new Counter();
//        $subscriber = '';
//        $agreement = '';
//
//        $counter
//            ->setAgreement()
//            ->setAddress()
//            ->setInstallDate()
//        ;
//
//        $task
//            ->setId($taskDTO->getId())
//            ->setTaskType($taskDTO->getTaskType())
//            ->setCompany($taskDTO->getCompany())
//            ->setCounter()
//            ->setDeadline()
//            ->setExecutor()
//            ->setPriority()
//            ->setStatus()
//            ->setCoordinates()
//            ->setOperatorComment()
//            ->setControllerComment()
//            ->setFailureFactors()
//            ->setActualizedFields()
//            ->setChangedFields()
//            ->setLastCounterValue()
//            ->setCurrentCounterValue()
//            ->setConsumption()
//            ->setConsumptionCoefficient()
//            ->setCounterPhysicalStatus()
//            ->setSubscriber()
//            ->setAgreement()
//            ->setPhotos()
//        ;
        return $task;
    }

    public static function update(TaskDTO $taskDTO, Task $task): Task
    {
        return self::readDTO($taskDTO, $task);
    }

    public static function readDTO(TaskDTO $taskDTO, ?Task $task = null): Task
    {
        $task = $task ?? new Task();
        /*$task
            ->setId($taskDTO->getId())
            ->setTaskType($taskDTO->getTaskType())
            ->setCompany($taskDTO->getCompany())
            ->setCounter()
            ->setDeadline()
            ->setExecutor()
            ->setPriority()
            ->setStatus()
            ->setCoordinates()
            ->setOperatorComment()
            ->setControllerComment()
            ->setFailureFactors()
            ->setActualizedFields()
            ->setChangedFields()
            ->setLastCounterValue()
            ->setCurrentCounterValue()
            ->setConsumption()
            ->setConsumptionCoefficient()
            ->setCounterPhysicalStatus()
            ->setSubscriber()
            ->setAgreement()
            ->setPhotos()
        ;*/
        return $task;
    }

    /**
     * @param \App\Entity\Main\Task $task
     * @return TaskDTO
     */
    public static function writeDTO(Task $task): TaskDTO
    {
        $dto = new TaskDTO();

        $coordinates = $task->getCoordinates();
        $address = $task->getCounter()->getAddress();
        $subscriber = $task->getSubscriber();
        $agreement = $task->getAgreement();
        $counter = $task->getCounter();
        $lastCounterValue = $task->getLastCounterValue() ? $task->getLastCounterValue()->getValue()->getValues() : null;
        $currentCounterValue = $task->getCurrentCounterValue() ? $task->getCurrentCounterValue()->getValue()->getValues() : null;
        $transformer = $counter->getTransformer();
        $feeder = is_null($transformer) ? null : $transformer->getFeeder();

        $dto
            ->setId($task->getId())
            ->setTaskType($task->getTaskType())
            ->setExecutor($task->getExecutor())
            ->setPriority($task->getPriority())
            ->setDeadline($task->getDeadline())
            ->setPeriod($task->getPeriod())
            ->setStatus($task->getStatus())
            ->setCoordinateX(is_null($coordinates) ? null : $coordinates[0])
            ->setCoordinateY(is_null($coordinates) ? null : $coordinates[1])
            ->setOperatorComment($task->getOperatorComment())
            ->setControllerComment($task->getControllerComment())
            ->setFailureFactors($task->getFailureFactors())
            ->setActualizedFields($task->getActualizedFields())
            ->setChangedFields($task->getChangedFields())
            ->setCompany($task->getCompany())
            ->setLocality(is_null($address) ? null : $address->getLocality())
            ->setStreet(is_null($address) ? null : $address->getStreet())
            ->setHouseNumber(is_null($address) ? null : $address->getHouseNumber())
            ->setPorchNumber(is_null($address) ? null : $address->getPorchNumber())
            ->setApartmentNumber(is_null($address) ? null : $address->getApartmentNumber())
            ->setLodgersCount(is_null($address) ? null : $address->getLodgersCount())
            ->setRoomsCount(is_null($address) ? null : $address->getRoomsCount())
            ->setSubscriberTitle($subscriber->getTitle())
            ->setSubscriberName($subscriber->getName())
            ->setSubscriberSurname($subscriber->getSurname())
            ->setSubscriberPatronymic($subscriber->getPatronymic())
            ->setSubscriberPhones($subscriber->getPhones())
            ->setSubscriberType($subscriber->getSubscriberType())
            ->setAgreementNumber($agreement->getNumber())
            ->setCounterNumber($counter->getTitle())
            ->setLastCounterValue($lastCounterValue)
            ->setCurrentCounterValue($currentCounterValue)
            ->setConsumption($task->getConsumption())
            ->setConsumptionCoefficient($task->getConsumptionCoefficient())
            ->setCounterPhysicalStatus($task->getCounterPhysicalStatus())
            ->setSubstation(is_null($feeder) ? null : $feeder->getSubstation())
            ->setFeeder($feeder)
            ->setTransformer($transformer)
            ->setLineNumber($counter->getElectricLine())
            ->setElectricPoleNumber($counter->getElectricPoleNumber())
            ->setCounterType($counter->getCounterType())
            ->setTerminalSeal($counter->getTerminalSeal())
            ->setAntiMagneticSeal($counter->getAntiMagneticSeal())
            ->setSideSeal($counter->getSideSeal())
            ->setPhotos($task->getPhotos()->getValues())
            ->setTemporaryAddress($counter->getTemporaryAddress())
            ->setTemporaryCounterType($counter->getTemporaryCounterType())
            ->setTemporarySubstation($counter->getTemporarySubstation())
            ->setTemporaryTransformer($counter->getTemporaryTransformer())
            ->setTemporaryFeeder($counter->getTemporaryFeeder())
        ;
        return $dto;
    }
}