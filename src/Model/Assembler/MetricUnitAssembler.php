<?php

namespace App\Model\Assembler;

use App\Entity\Main\Counter;
use App\Entity\Main\CounterValue;
use App\Model\DTO\MetricUnitDTO;

class MetricUnitAssembler
{

    /**
     * @return mixed
     */
    public static function create()
    {
        // TODO: Implement create() method.
    }

    /**
     * @return mixed
     */
    public static function update()
    {
        // TODO: Implement update() method.
    }

    /**
     * @return mixed
     */
    public static function readDTO()
    {
        // TODO: Implement readDTO() method.
    }

    /**
     * @param \App\Entity\Main\Counter $counter
     * @param CounterValue|null $counterValue
     * @return MetricUnitDTO
     */
    public static function writeDTO(Counter $counter, ?CounterValue $counterValue): MetricUnitDTO
    {
        $metricUnitDTO = new MetricUnitDTO();

        $initialCounterValueId = $counter->getInitialValue() ? $counter->getInitialValue()->getId() : null;
        $initialCounterValue = $counter->getInitialValue() ? $counter->getInitialValue()->getValue()->getValues()[0] : null;
        $initialCounterValueDate = $counter->getInitialValue() ? $counter->getInitialValue()->getDate() : null;

        $subscriberTitle = $counter->getAgreement()->getSubscriber()->getTitle();
        $agreementNumber = $counter->getAgreement()->getNumber();
//        $lodgersCount = $counter->getAddress() ? $counter->getAddress()->getLodgersCount() : null;
//        $roomsCount = $counter->getAddress() ? $counter->getAddress()->getRoomsCount() : null;
        $lodgersCount = $counter->getTemporaryLodgersCount();
        $roomsCount = $counter->getTemporaryRoomsCount();
        $phones = $counter->getAgreement()->getSubscriber()->getPhones();

        $metricUnitDTO->setCounterId($counter->getId());
        $metricUnitDTO->setCounterTitle($counter->getTitle());
        $metricUnitDTO->setCounterPhysicalStatus($counter->getWorkStatus());
        $metricUnitDTO->setCounterModel($counter->getTemporaryCounterType());
        $metricUnitDTO->setTerminalSeal($counter->getTerminalSeal());
        $metricUnitDTO->setAntimagneticSeal($counter->getAntiMagneticSeal());
        $metricUnitDTO->setSideSeal($counter->getSideSeal());
        $metricUnitDTO->setCoordinates($counter->getCoordinates());
        $metricUnitDTO->setInitialCounterValueId($initialCounterValueId);
        $metricUnitDTO->setInitialCounterValue($initialCounterValue);
        $metricUnitDTO->setInitialCounterValueDate($initialCounterValueDate);
        $metricUnitDTO->setAddress($counter->getTemporaryAddress());
        $metricUnitDTO->setTransformer($counter->getTransformer());

        if ($counterValue) {
            $metricUnitDTO->setLastCounterValueId($counterValue->getId());
            $metricUnitDTO->setLastCounterValue($counterValue->getValue()->getValues()[0]);
            $metricUnitDTO->setLastCounterValueDate($counterValue->getDate());
        } else {
            $metricUnitDTO->setLastCounterValueId($initialCounterValueId);
            $metricUnitDTO->setLastCounterValue($initialCounterValue);
            $metricUnitDTO->setLastCounterValueDate($initialCounterValueDate);
        }

        $metricUnitDTO->setSubscriberTitle($subscriberTitle);
        $metricUnitDTO->setAgreementNumber($agreementNumber);
        $metricUnitDTO->setLodgersCount($lodgersCount);
        $metricUnitDTO->setRoomsCount($roomsCount);
        $metricUnitDTO->setPhones($phones);

        return $metricUnitDTO;
    }
}