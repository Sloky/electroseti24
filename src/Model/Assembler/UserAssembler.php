<?php


namespace App\Model\Assembler;


use App\Entity\Main\User;
use App\Model\DTO\UserDTO;
use Doctrine\Common\Collections\ArrayCollection;

class UserAssembler
{
    public static function create(UserDTO $userDTO): User
    {
        return self::readDTO($userDTO);
    }

    public static function update(UserDTO $userDTO, User $user): User
    {
        return self::readDTO($userDTO, $user);
    }

    public static function readDTO(UserDTO $userDTO, ?User $user = null): User
    {
        $user = $user ?? new User();
        $user
            ->setId($userDTO->getId())
            ->setUsername($userDTO->getUsername())
            ->setName($userDTO->getName())
            ->setPatronymic($userDTO->getPatronymic())
            ->setSurname($userDTO->getSurname())
//            ->setRoles($userDTO->getRoles())
            ->setGroups($userDTO->getGroups())
            ->setPhone($userDTO->getPhone())
            ->setAddress($userDTO->getAddress())
            ->setCompanies(new ArrayCollection($userDTO->getCompanies()))
            ->setWorkStatus($userDTO->getWorkStatus())
        ;
        return $user;
    }

    public static function writeDTO(User $user, $taskToWorkCount = null, $companyTaskToCheckCount = null): UserDTO
    {
        $dto = new UserDTO();
        $dto
            ->setId($user->getId())
            ->setUsername($user->getUsername())
            ->setName($user->getName())
            ->setPatronymic($user->getPatronymic())
            ->setSurname($user->getSurname())
            ->setRoles($user->getRoles())
            ->setGroups($user->getGroups())
            ->setPhone($user->getPhone())
            ->setAddress($user->getAddress())
            ->setCompanies($user->getCompanies()->getValues())
            ->setWorkStatus($user->getWorkStatus())
            ->setTaskToWorkCount($taskToWorkCount)
            ->setCompanyTaskToCheckCount($companyTaskToCheckCount)
        ;
        return $dto;
    }
}