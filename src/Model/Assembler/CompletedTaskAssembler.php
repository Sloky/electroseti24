<?php


namespace App\Model\Assembler;

use App\Entity\Main\Task;
use App\Handler\CounterHandler;
use App\Model\DTO\CompletedTaskDTO;

class CompletedTaskAssembler
{
    /**
     * @var CounterHandler
     */
    private $counterHandler;

    public function __construct(
        CounterHandler $counterHandler
    )
    {
        $this->counterHandler = $counterHandler;
    }

    /**
     * Create task from CompletedTaskDTO
     * @param CompletedTaskDTO $CompletedTaskDTO
     * @return \App\Entity\Main\Task
     */
    public static function create(CompletedTaskDTO $CompletedTaskDTO): Task
    {
        $task = new Task();

//        $counter = static::$counterHandler->getCounterByNumber($CompletedTaskDTO->getCounterNumber()) ?? new Counter();
//        $subscriber = '';
//        $agreement = '';
//
//        $counter
//            ->setAgreement()
//            ->setAddress()
//            ->setInstallDate()
//        ;
//
//        $task
//            ->setId($CompletedTaskDTO->getId())
//            ->setTaskType($CompletedTaskDTO->getTaskType())
//            ->setCompany($CompletedTaskDTO->getCompany())
//            ->setCounter()
//            ->setDeadline()
//            ->setExecutor()
//            ->setPriority()
//            ->setStatus()
//            ->setCoordinates()
//            ->setOperatorComment()
//            ->setControllerComment()
//            ->setFailureFactors()
//            ->setActualizedFields()
//            ->setChangedFields()
//            ->setLastCounterValue()
//            ->setCurrentCounterValue()
//            ->setConsumption()
//            ->setConsumptionCoefficient()
//            ->setCounterPhysicalStatus()
//            ->setSubscriber()
//            ->setAgreement()
//            ->setPhotos()
//        ;
        return $task;
    }

    public static function update(CompletedTaskDTO $CompletedTaskDTO, Task $task): Task
    {
        return self::readDTO($CompletedTaskDTO, $task);
    }

    public static function readDTO(CompletedTaskDTO $CompletedTaskDTO, ?Task $task = null): Task
    {
        $task = $task ?? new Task();
        /*$task
            ->setId($CompletedTaskDTO->getId())
            ->setTaskType($CompletedTaskDTO->getTaskType())
            ->setCompany($CompletedTaskDTO->getCompany())
            ->setCounter()
            ->setDeadline()
            ->setExecutor()
            ->setPriority()
            ->setStatus()
            ->setCoordinates()
            ->setOperatorComment()
            ->setControllerComment()
            ->setFailureFactors()
            ->setActualizedFields()
            ->setChangedFields()
            ->setLastCounterValue()
            ->setCurrentCounterValue()
            ->setConsumption()
            ->setConsumptionCoefficient()
            ->setCounterPhysicalStatus()
            ->setSubscriber()
            ->setAgreement()
            ->setPhotos()
        ;*/
        return $task;
    }

    /**
     * @param Task $task
     * @param string|null $photosStorageHost
     * @return CompletedTaskDTO
     */
    public static function writeDTO(Task $task, ?string $photosStorageHost = null): CompletedTaskDTO
    {
        $dto = new CompletedTaskDTO();

        $coordinates = $task->getCoordinates();
        $newCoordinates =$task->getNewCoordinates();
        $address = $task->getCounter()->getAddress();
        $subscriber = $task->getSubscriber();
        $agreement = $task->getAgreement();
        $counter = $task->getCounter();
        $lastCounterValue = $task->getLastCounterValue() ? $task->getLastCounterValue()->getValue()->getValues() : null;
        $currentCounterValue = $task->getCurrentCounterValue() ? $task->getCurrentCounterValue()->getValue()->getValues() : null;
        $transformer = $counter->getTransformer();
        $feeder = is_null($transformer) ? null : $transformer->getFeeder();

        $dto
            ->setId($task->getId())
            ->setTaskType($task->getTaskType())
            ->setExecutor($task->getExecutor())
            ->setPriority($task->getPriority())
            ->setPeriod($task->getPeriod())
            ->setDeadline($task->getDeadline())
            ->setExecutedDate($task->getExecutedDate())
            ->setStatus($task->getStatus())
            ->setCoordinateX(is_null($coordinates) ? null : $coordinates[0])
            ->setCoordinateY(is_null($coordinates) ? null : $coordinates[1])
            ->setOperatorComment($task->getOperatorComment())
            ->setControllerComment($task->getControllerComment())
            ->setFailureFactors($task->getFailureFactors())
            ->setActualizedFields($task->getActualizedFields())
            ->setChangedFields($task->getChangedFields())
            ->setCompany($task->getCompany())
            ->setLocality(is_null($address) ? null : $address->getLocality())
            ->setStreet(is_null($address) ? null : $address->getStreet())
            ->setHouseNumber(is_null($address) ? null : $address->getHouseNumber())
            ->setPorchNumber(is_null($address) ? null : $address->getPorchNumber())
            ->setApartmentNumber(is_null($address) ? null : $address->getApartmentNumber())
            ->setLodgersCount(is_null($address) ? null : $address->getLodgersCount())
            ->setRoomsCount(is_null($address) ? null : $address->getRoomsCount())
            ->setSubscriberTitle($subscriber->getTitle())
            ->setSubscriberName($subscriber->getName())
            ->setSubscriberSurname($subscriber->getSurname())
            ->setSubscriberPatronymic($subscriber->getPatronymic())
            ->setSubscriberPhones($subscriber->getPhones())
            ->setSubscriberType($subscriber->getSubscriberType())
            ->setAgreementNumber($agreement->getNumber())
            ->setCounterNumber($counter->getTitle())
            ->setLastCounterValue($lastCounterValue)
            ->setCurrentCounterValue($currentCounterValue)
            ->setConsumption($task->getConsumption())
            ->setConsumptionCoefficient($task->getConsumptionCoefficient())
            ->setCounterPhysicalStatus($task->getCounterPhysicalStatus())
            ->setSubstation(is_null($feeder) ? null : $feeder->getSubstation())
            ->setFeeder($feeder)
            ->setTransformer($transformer)
            ->setLineNumber($counter->getElectricLine())
            ->setElectricPoleNumber($counter->getElectricPoleNumber())
            ->setCounterType($counter->getCounterType())
            ->setTerminalSeal($counter->getTerminalSeal())
            ->setAntiMagneticSeal($counter->getAntiMagneticSeal())
            ->setSideSeal($counter->getSideSeal())
            ->setPhotos($task->getPhotos()->getValues())
            ->setTemporaryAddress($counter->getTemporaryAddress())
            ->setTemporaryCounterType($counter->getTemporaryCounterType())
            ->setTemporarySubstation($counter->getTemporarySubstation())
            ->setTemporaryTransformer($counter->getTemporaryTransformer())
            ->setTemporaryFeeder($counter->getTemporaryFeeder())
            ->setChangedCounterNumber($task->getChangedCounterNumber())
            ->setChangedCounterType($task->getChangedCounterType())
            ->setChangedCounterTypeString($task->getChangedCounterTypeString())
            ->setChangedTerminalSeal($task->getChangedTerminalSeal())
            ->setChangedAntimagneticSeal($task->getChangedAntimagneticSeal())
            ->setChangedSideSeal($task->getChangedSideSeal())
            ->setChangedLodgersCount($task->getChangedLodgersCount())
            ->setChangedRoomsCount($task->getChangedRoomsCount())
            ->setNewCoordinateX(is_null($newCoordinates) ? null : $newCoordinates[0])
            ->setNewCoordinateY(is_null($newCoordinates) ? null : $newCoordinates[1])
            ->setAcceptanceDate($task->getAcceptanceDate())
            ->setOperator($task->getOperator())
            ->setPhotosPublicUrl($photosStorageHost)
        ;
        return $dto;
    }
}