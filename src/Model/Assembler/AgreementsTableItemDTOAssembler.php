<?php

namespace App\Model\Assembler;

use App\Entity\Main\Agreement;
use App\Model\DTO\AgreementsTableItemDTO;

class AgreementsTableItemDTOAssembler
{
    /**
     * @return mixed
     */
    public static function create()
    {}

    /**
     * @return mixed
     */
    public static function update()
    {}

    /**
     * @return mixed
     */
    public static function readDTO()
    {}

    /**
     * @return mixed
     */
    public static function writeDTO(Agreement $agreement): AgreementsTableItemDTO
    {
        $agreementsTableItemDTO = new AgreementsTableItemDTO();

        $agreementsTableItemDTO->setId($agreement->getId());
        $agreementsTableItemDTO->setNumber($agreement->getNumber());
        $agreementsTableItemDTO->setSubscriber($agreement->getSubscriber());
        $agreementsTableItemDTO->setPaymentNumber($agreement->getPaymentNumber());
        $agreementsTableItemDTO->setCompany($agreement->getCompany());
        $agreementsTableItemDTO->setCounterCount($agreement->getCounters()->count());

        return $agreementsTableItemDTO;
    }
}