<?php

namespace App\Model\Assembler;

use App\Entity\Main\Transformer;
use App\Model\DTO\TPDTO;

class TPAssembler
{
    /**
     * @return mixed
     */
    public static function create()
    {}

    /**
     * @return mixed
     */
    public static function update()
    {}

    /**
     * @return mixed
     */
    public static function readDTO()
    {}

    /**
     * @return mixed
     */
    public static function writeDTO(Transformer $transformer, ?int $counterCount = null): TPDTO
    {
        $TPdto = new TPDTO();

        $feeder = $transformer->getFeeder();
        $substation = $feeder ? $transformer->getFeeder() : null;
        $feederTitle = $feeder ? $feeder->getTitle() : null;
        $substationTitle = $substation ? $substation->getTitle() : null;

        $TPdto->setId($transformer->getId());
        $TPdto->setTitle($transformer->getTitle());
        $TPdto->setCoordinates($transformer->getCoords());
        $TPdto->setFeederTitle($feederTitle);
        $TPdto->setSubstationTitle($substationTitle);
        $TPdto->setCompany($transformer->getCompany());
        $TPdto->setUsers($transformer->getUsers());
        $TPdto->setCountersCount($counterCount);

        return $TPdto;
    }
}