<?php


namespace App\Model;


use DateTimeInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class TaskMobileUploadForm
{
    /**
     * @var string|null
     */
    protected $taskId;

    /**
     * @var int|null
     */
    protected $status;

    /**
     * @var int|null
     */
    protected $counterPhysicalStatus;

    /**
     * @var float|null
     */
    protected $counterValue;

    /**
     * @var UploadedFile|null
     */
    protected $counterValuePhoto;

    /**
     * @var UploadedFile|null
     */
    protected $counterPhoto;

    /**
     * @var string|null
     */
    protected $counterNumber;

    /**
     * @var string|null
     */
    protected $counterTypeString;

    /**
     * @var string|null
     */
    protected $terminalSeal;

    /**
     * @var UploadedFile|null
     */
    protected $terminalSealPhoto;

    /**
     * @var string|null
     */
    protected $antimagneticSeal;

    /**
     * @var UploadedFile|null
     */
    protected $antimagneticSealPhoto;

    /**
     * @var string|null
     */
    protected $sideSeal;

    /**
     * @var UploadedFile|null
     */
    protected $sideSealPhoto;

    /**
     * @var int|null
     */
    protected $lodgersCount;

    /**
     * @var int|null
     */
    protected $roomsCount;

    /**
     * @var string|null
     */
    protected $controllerComment;

    /**
     * @var DateTimeInterface|null
     */
    protected $executedDateTime;

    /**
     * @var float|null
     */
    protected $GPSLongitude;

    /**
     * @var float|null
     */
    protected $GPSLatitude;

    /**
     * @return string|null
     */
    public function getTaskId(): ?string
    {
        return $this->taskId;
    }

    /**
     * @param string|null $taskId
     * @return TaskMobileUploadForm
     */
    public function setTaskId(?string $taskId): TaskMobileUploadForm
    {
        $this->taskId = $taskId;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return TaskMobileUploadForm
     */
    public function setStatus(?int $status): TaskMobileUploadForm
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return int
     */
    public function getCounterPhysicalStatus(): ?int
    {
        return $this->counterPhysicalStatus;
    }

    /**
     * @param int $counterPhysicalStatus
     * @return TaskMobileUploadForm
     */
    public function setCounterPhysicalStatus(?int $counterPhysicalStatus): TaskMobileUploadForm
    {
        $this->counterPhysicalStatus = $counterPhysicalStatus;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getCounterValue(): ?float
    {
        return $this->counterValue;
    }

    /**
     * @param float|null $counterValue
     * @return TaskMobileUploadForm
     */
    public function setCounterValue(?float $counterValue): TaskMobileUploadForm
    {
        $this->counterValue = $counterValue;
        return $this;
    }

    /**
     * @return UploadedFile|null
     */
    public function getCounterValuePhoto(): ?UploadedFile
    {
        return $this->counterValuePhoto;
    }

    /**
     * @param UploadedFile|null $counterValuePhoto
     * @return TaskMobileUploadForm
     */
    public function setCounterValuePhoto(?UploadedFile $counterValuePhoto): TaskMobileUploadForm
    {
        $this->counterValuePhoto = $counterValuePhoto;
        return $this;
    }

    /**
     * @return UploadedFile|null
     */
    public function getCounterPhoto(): ?UploadedFile
    {
        return $this->counterPhoto;
    }

    /**
     * @param UploadedFile|null $counterPhoto
     * @return TaskMobileUploadForm
     */
    public function setCounterPhoto(?UploadedFile $counterPhoto): TaskMobileUploadForm
    {
        $this->counterPhoto = $counterPhoto;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCounterNumber(): ?string
    {
        return $this->counterNumber;
    }

    /**
     * @param string|null $counterNumber
     * @return TaskMobileUploadForm
     */
    public function setCounterNumber(?string $counterNumber): TaskMobileUploadForm
    {
        $this->counterNumber = $counterNumber;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCounterTypeString(): ?string
    {
        return $this->counterTypeString;
    }

    /**
     * @param string|null $counterTypeString
     * @return TaskMobileUploadForm
     */
    public function setCounterTypeString(?string $counterTypeString): TaskMobileUploadForm
    {
        $this->counterTypeString = $counterTypeString;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTerminalSeal(): ?string
    {
        return $this->terminalSeal;
    }

    /**
     * @param string|null $terminalSeal
     * @return TaskMobileUploadForm
     */
    public function setTerminalSeal(?string $terminalSeal): TaskMobileUploadForm
    {
        $this->terminalSeal = $terminalSeal;
        return $this;
    }

    /**
     * @return UploadedFile|null
     */
    public function getTerminalSealPhoto(): ?UploadedFile
    {
        return $this->terminalSealPhoto;
    }

    /**
     * @param UploadedFile|null $terminalSealPhoto
     * @return TaskMobileUploadForm
     */
    public function setTerminalSealPhoto(?UploadedFile $terminalSealPhoto): TaskMobileUploadForm
    {
        $this->terminalSealPhoto = $terminalSealPhoto;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAntimagneticSeal(): ?string
    {
        return $this->antimagneticSeal;
    }

    /**
     * @param string|null $antimagneticSeal
     * @return TaskMobileUploadForm
     */
    public function setAntimagneticSeal(?string $antimagneticSeal): TaskMobileUploadForm
    {
        $this->antimagneticSeal = $antimagneticSeal;
        return $this;
    }

    /**
     * @return UploadedFile|null
     */
    public function getAntimagneticSealPhoto(): ?UploadedFile
    {
        return $this->antimagneticSealPhoto;
    }

    /**
     * @param UploadedFile|null $antimagneticSealPhoto
     * @return TaskMobileUploadForm
     */
    public function setAntimagneticSealPhoto(?UploadedFile $antimagneticSealPhoto): TaskMobileUploadForm
    {
        $this->antimagneticSealPhoto = $antimagneticSealPhoto;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSideSeal(): ?string
    {
        return $this->sideSeal;
    }

    /**
     * @param string|null $sideSeal
     * @return TaskMobileUploadForm
     */
    public function setSideSeal(?string $sideSeal): TaskMobileUploadForm
    {
        $this->sideSeal = $sideSeal;
        return $this;
    }

    /**
     * @return UploadedFile|null
     */
    public function getSideSealPhoto(): ?UploadedFile
    {
        return $this->sideSealPhoto;
    }

    /**
     * @param UploadedFile|null $sideSealPhoto
     * @return TaskMobileUploadForm
     */
    public function setSideSealPhoto(?UploadedFile $sideSealPhoto): TaskMobileUploadForm
    {
        $this->sideSealPhoto = $sideSealPhoto;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getLodgersCount(): ?int
    {
        return $this->lodgersCount;
    }

    /**
     * @param int|null $lodgersCount
     * @return TaskMobileUploadForm
     */
    public function setLodgersCount(?int $lodgersCount): TaskMobileUploadForm
    {
        $this->lodgersCount = $lodgersCount;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getRoomsCount(): ?int
    {
        return $this->roomsCount;
    }

    /**
     * @param int|null $roomsCount
     * @return TaskMobileUploadForm
     */
    public function setRoomsCount(?int $roomsCount): TaskMobileUploadForm
    {
        $this->roomsCount = $roomsCount;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getControllerComment(): ?string
    {
        return $this->controllerComment;
    }

    /**
     * @param string|null $controllerComment
     * @return TaskMobileUploadForm
     */
    public function setControllerComment(?string $controllerComment): TaskMobileUploadForm
    {
        $this->controllerComment = $controllerComment;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getExecutedDateTime(): ?DateTimeInterface
    {
        return $this->executedDateTime;
    }

    /**
     * @param DateTimeInterface|null $executedDateTime
     * @return TaskMobileUploadForm
     */
    public function setExecutedDateTime(?DateTimeInterface $executedDateTime): TaskMobileUploadForm
    {
        $this->executedDateTime = $executedDateTime;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getGPSLongitude(): ?float
    {
        return $this->GPSLongitude;
    }

    /**
     * @param float|null $GPSLongitude
     * @return TaskMobileUploadForm
     */
    public function setGPSLongitude(?float $GPSLongitude): TaskMobileUploadForm
    {
        $this->GPSLongitude = $GPSLongitude;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getGPSLatitude(): ?float
    {
        return $this->GPSLatitude;
    }

    /**
     * @param float|null $GPSLatitude
     * @return TaskMobileUploadForm
     */
    public function setGPSLatitude(?float $GPSLatitude): TaskMobileUploadForm
    {
        $this->GPSLatitude = $GPSLatitude;
        return $this;
    }
}