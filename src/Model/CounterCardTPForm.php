<?php

namespace App\Model;

use App\Entity\Main\Transformer;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class CounterCardTPForm
{
    /**
     * @var \App\Entity\Main\Transformer|null
     */
    private ?Transformer $transformer = null;

    /**
     * @var string|null
     */
    private ?string $counterTitle = null;

    /**
     * @var integer|null
     */
    private ?int $counterPhysicalStatus = null;

    /**
     * @var string|null
     */
    private ?string $counterModel = null;

    /**
     * @var string|null
     */
    private ?string $terminalSeal = null;

    /**
     * @var string|null
     */
    private ?string $antimagneticSeal = null;

    /**
     * @var string|null
     */
    private ?string $sideSeal = null;

    /**
     * @var string|null
     */
    private ?string $address = null;

    /**
     * @var float|null
     */
    private ?float $coordinateX = null;

    /**
     * @var float|null
     */
    private ?float $coordinateY = null;

    /**
     * @var string|null
     */
    private ?string $initialCounterValueId = null;

    /**
     * @var float|null
     */
    private ?float $initialCounterValue = null;

    /**
     * @var DateTimeInterface|null
     */
    private ?DateTimeInterface $initialCounterValueDate = null;

    /**
     * @var string|null
     */
    private ?string $lastCounterValueId = null;

    /**
     * @var float|null
     */
    private ?float $lastCounterValue = null;

    /**
     * @var DateTimeInterface|null
     */
    private ?DateTimeInterface $lastCounterValueDate = null;

    /**
     * @var string|null
     */
    private ?string $subscriberTitle = null;

    /**
     * @var string|null
     */
    private ?string $agreementNumber = null;

    /**
     * @var integer|null
     */
    private ?int $lodgersCount = null;

    /**
     * @var integer|null
     */
    private ?int $roomsCount = null;

    /**
     * @var Collection
     */
    private Collection $phones;

    public function __construct()
    {
        $this->phones = new ArrayCollection();
    }

    /**
     * @return Transformer|null
     */
    public function getTransformer(): ?Transformer
    {
        return $this->transformer;
    }

    /**
     * @param Transformer|null $transformer
     * @return CounterCardTPForm
     */
    public function setTransformer(?Transformer $transformer): CounterCardTPForm
    {
        $this->transformer = $transformer;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCounterTitle(): ?string
    {
        return $this->counterTitle;
    }

    /**
     * @param string|null $counterTitle
     * @return CounterCardTPForm
     */
    public function setCounterTitle(?string $counterTitle): CounterCardTPForm
    {
        $this->counterTitle = $counterTitle;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getCounterPhysicalStatus(): ?int
    {
        return $this->counterPhysicalStatus;
    }

    /**
     * @param int|null $counterPhysicalStatus
     * @return CounterCardTPForm
     */
    public function setCounterPhysicalStatus(?int $counterPhysicalStatus): CounterCardTPForm
    {
        $this->counterPhysicalStatus = $counterPhysicalStatus;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCounterModel(): ?string
    {
        return $this->counterModel;
    }

    /**
     * @param string|null $counterModel
     * @return CounterCardTPForm
     */
    public function setCounterModel(?string $counterModel): CounterCardTPForm
    {
        $this->counterModel = $counterModel;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTerminalSeal(): ?string
    {
        return $this->terminalSeal;
    }

    /**
     * @param string|null $terminalSeal
     * @return CounterCardTPForm
     */
    public function setTerminalSeal(?string $terminalSeal): CounterCardTPForm
    {
        $this->terminalSeal = $terminalSeal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAntimagneticSeal(): ?string
    {
        return $this->antimagneticSeal;
    }

    /**
     * @param string|null $antimagneticSeal
     * @return CounterCardTPForm
     */
    public function setAntimagneticSeal(?string $antimagneticSeal): CounterCardTPForm
    {
        $this->antimagneticSeal = $antimagneticSeal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSideSeal(): ?string
    {
        return $this->sideSeal;
    }

    /**
     * @param string|null $sideSeal
     * @return CounterCardTPForm
     */
    public function setSideSeal(?string $sideSeal): CounterCardTPForm
    {
        $this->sideSeal = $sideSeal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     * @return CounterCardTPForm
     */
    public function setAddress(?string $address): CounterCardTPForm
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getCoordinateX(): ?float
    {
        return $this->coordinateX;
    }

    /**
     * @param float|null $coordinateX
     * @return CounterCardTPForm
     */
    public function setCoordinateX(?float $coordinateX): CounterCardTPForm
    {
        $this->coordinateX = $coordinateX;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getCoordinateY(): ?float
    {
        return $this->coordinateY;
    }

    /**
     * @param float|null $coordinateY
     * @return CounterCardTPForm
     */
    public function setCoordinateY(?float $coordinateY): CounterCardTPForm
    {
        $this->coordinateY = $coordinateY;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getInitialCounterValueId(): ?string
    {
        return $this->initialCounterValueId;
    }

    /**
     * @param string|null $initialCounterValueId
     * @return CounterCardTPForm
     */
    public function setInitialCounterValueId(?string $initialCounterValueId): CounterCardTPForm
    {
        $this->initialCounterValueId = $initialCounterValueId;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getInitialCounterValue(): ?float
    {
        return $this->initialCounterValue;
    }

    /**
     * @param float|null $initialCounterValue
     * @return CounterCardTPForm
     */
    public function setInitialCounterValue(?float $initialCounterValue): CounterCardTPForm
    {
        $this->initialCounterValue = $initialCounterValue;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getInitialCounterValueDate(): ?DateTimeInterface
    {
        return $this->initialCounterValueDate;
    }

    /**
     * @param DateTimeInterface|null $initialCounterValueDate
     * @return CounterCardTPForm
     */
    public function setInitialCounterValueDate(?DateTimeInterface $initialCounterValueDate): CounterCardTPForm
    {
        $this->initialCounterValueDate = $initialCounterValueDate;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastCounterValueId(): ?string
    {
        return $this->lastCounterValueId;
    }

    /**
     * @param string|null $lastCounterValueId
     * @return CounterCardTPForm
     */
    public function setLastCounterValueId(?string $lastCounterValueId): CounterCardTPForm
    {
        $this->lastCounterValueId = $lastCounterValueId;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getLastCounterValue(): ?float
    {
        return $this->lastCounterValue;
    }

    /**
     * @param float|null $lastCounterValue
     * @return CounterCardTPForm
     */
    public function setLastCounterValue(?float $lastCounterValue): CounterCardTPForm
    {
        $this->lastCounterValue = $lastCounterValue;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getLastCounterValueDate(): ?DateTimeInterface
    {
        return $this->lastCounterValueDate;
    }

    /**
     * @param DateTimeInterface|null $lastCounterValueDate
     * @return CounterCardTPForm
     */
    public function setLastCounterValueDate(?DateTimeInterface $lastCounterValueDate): CounterCardTPForm
    {
        $this->lastCounterValueDate = $lastCounterValueDate;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubscriberTitle(): ?string
    {
        return $this->subscriberTitle;
    }

    /**
     * @param string|null $subscriberTitle
     * @return CounterCardTPForm
     */
    public function setSubscriberTitle(?string $subscriberTitle): CounterCardTPForm
    {
        $this->subscriberTitle = $subscriberTitle;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAgreementNumber(): ?string
    {
        return $this->agreementNumber;
    }

    /**
     * @param string|null $agreementNumber
     * @return CounterCardTPForm
     */
    public function setAgreementNumber(?string $agreementNumber): CounterCardTPForm
    {
        $this->agreementNumber = $agreementNumber;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getLodgersCount(): ?int
    {
        return $this->lodgersCount;
    }

    /**
     * @param int|null $lodgersCount
     * @return CounterCardTPForm
     */
    public function setLodgersCount(?int $lodgersCount): CounterCardTPForm
    {
        $this->lodgersCount = $lodgersCount;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getRoomsCount(): ?int
    {
        return $this->roomsCount;
    }

    /**
     * @param int|null $roomsCount
     * @return CounterCardTPForm
     */
    public function setRoomsCount(?int $roomsCount): CounterCardTPForm
    {
        $this->roomsCount = $roomsCount;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getPhones()
    {
        return $this->phones;
    }

    /**
     * @param Collection $phones
     * @return CounterCardTPForm
     */
    public function setPhones(Collection $phones): CounterCardTPForm
    {
        $this->phones = $phones;
        return $this;
    }

    /**
     * @param string $phone
     * @return $this
     */
    public function addPhone(string $phone): CounterCardTPForm
    {
        $this->phones->add($phone);
        return $this;
    }

    /**
     * @param string $phone
     * @return $this
     */
    public function removePhone(string $phone): CounterCardTPForm
    {
        $this->phones->removeElement($phone);
        return $this;
    }
}