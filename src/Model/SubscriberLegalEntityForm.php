<?php

namespace App\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class SubscriberLegalEntityForm
{
    /**
     * @var string|null
     */
    private ?string $title = null;

    /**
     * @var string|null
     */
    private ?string $TIN = null;

    /**
     * @var string|null
     */
    private ?string $address = null;

    /**
     * @var float|null
     */
    private ?float $coordinateX = null;

    /**
     * @var float|null
     */
    private ?float $coordinateY = null;

    /**
     * @var Collection|ArrayCollection
     */
    private Collection $phones;

    /**
     * @var string|null
     */
    private ?string $email = null;

    /**
     * @var string|null
     */
    private ?string $webSite = null;

    public function __construct()
    {
        $this->phones = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     * @return SubscriberLegalEntityForm
     */
    public function setTitle(?string $title): SubscriberLegalEntityForm
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTIN(): ?string
    {
        return $this->TIN;
    }

    /**
     * @param string|null $TIN
     * @return SubscriberLegalEntityForm
     */
    public function setTIN(?string $TIN): SubscriberLegalEntityForm
    {
        $this->TIN = $TIN;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     * @return SubscriberLegalEntityForm
     */
    public function setAddress(?string $address): SubscriberLegalEntityForm
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getCoordinateX(): ?float
    {
        return $this->coordinateX;
    }

    /**
     * @param float|null $coordinateX
     * @return SubscriberLegalEntityForm
     */
    public function setCoordinateX(?float $coordinateX): SubscriberLegalEntityForm
    {
        $this->coordinateX = $coordinateX;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getCoordinateY(): ?float
    {
        return $this->coordinateY;
    }

    /**
     * @param float|null $coordinateY
     * @return SubscriberLegalEntityForm
     */
    public function setCoordinateY(?float $coordinateY): SubscriberLegalEntityForm
    {
        $this->coordinateY = $coordinateY;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getPhones(): Collection
    {
        return $this->phones;
    }

    /**
     * @param Collection $phones
     * @return SubscriberLegalEntityForm
     */
    public function setPhones(Collection $phones): SubscriberLegalEntityForm
    {
        $this->phones = $phones;
        return $this;
    }

    /**
     * @param string $phone
     * @return $this
     */
    public function addPhone(string $phone): SubscriberLegalEntityForm
    {
        $this->phones->add($phone);
        return $this;
    }

    /**
     * @param string $phone
     * @return $this
     */
    public function removePhone(string $phone): SubscriberLegalEntityForm
    {
        $this->phones->removeElement($phone);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return SubscriberLegalEntityForm
     */
    public function setEmail(?string $email): SubscriberLegalEntityForm
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getWebSite(): ?string
    {
        return $this->webSite;
    }

    /**
     * @param string|null $webSite
     * @return SubscriberLegalEntityForm
     */
    public function setWebSite(?string $webSite): SubscriberLegalEntityForm
    {
        $this->webSite = $webSite;
        return $this;
    }
}