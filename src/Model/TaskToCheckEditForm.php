<?php


namespace App\Model;


use App\Entity\Main\CounterType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class TaskToCheckEditForm
{
    /**
     * Физическое состояние УУ(Доступность/Исправность)
     * @var int|null
     */
    protected $counterPhysicalStatus;

    /**
     * Новые показания УУ
     * @var Collection|null
     */
    protected $newCounterValue;

    /**
     * Новый номер УУ
     * @var string|null
     */
    protected $newCounterNumber;

    /**
     * Новый тип УУ
     * @var CounterType|null
     */
    protected $newCounterType;

    /**
     * Новая клеммная пломба
     * @var string|null
     */
    protected $newTerminalSeal;

    /**
     * Новая антимагнитная пломба
     * @var string|null
     */
    protected $newAntimagneticSeal;

    /**
     * Новая боковая пломба
     * @var string|null
     */
    protected $newSideSeal;

    /**
     * Населенный пункт
     * @var string|null
     */
    protected $locality;

    /**
     * Улица
     * @var string|null
     */
    protected $street;

    /**
     * Номер дома
     * @var string|null
     */
    protected $houseNumber;

    /**
     * Номер подъезда
     * @var string|null
     */
    protected $porchNumber;

    /**
     * Номер квартиры
     * @var string|null
     */
    protected $apartmentNumber;

    /**
     * Количество жильцов
     * @var int|null
     */
    protected $lodgersCount;

    /**
     * Количество комнат
     * @var int|null
     */
    protected $roomsCount;

    /**
     * Комментарий контролёра
     * @var string|null
     */
    protected $operatorComment;

    public function __construct()
    {
        $this->newCounterValue = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getCounterPhysicalStatus(): ?int
    {
        return $this->counterPhysicalStatus;
    }

    /**
     * @param int|null $counterPhysicalStatus
     * @return TaskToCheckEditForm
     */
    public function setCounterPhysicalStatus(?int $counterPhysicalStatus): TaskToCheckEditForm
    {
        $this->counterPhysicalStatus = $counterPhysicalStatus;
        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getNewCounterValue(): ?Collection
    {
        return $this->newCounterValue;
    }

    /**
     * @param Collection|null $newCounterValue
     * @return TaskToCheckEditForm
     */
    public function setNewCounterValue(?Collection $newCounterValue): TaskToCheckEditForm
    {
        $this->newCounterValue = $newCounterValue;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNewCounterNumber(): ?string
    {
        return $this->newCounterNumber;
    }

    /**
     * @param string|null $newCounterNumber
     * @return TaskToCheckEditForm
     */
    public function setNewCounterNumber(?string $newCounterNumber): TaskToCheckEditForm
    {
        $this->newCounterNumber = $newCounterNumber;
        return $this;
    }

    /**
     * @return \App\Entity\Main\CounterType|null
     */
    public function getNewCounterType(): ?CounterType
    {
        return $this->newCounterType;
    }

    /**
     * @param \App\Entity\Main\CounterType|null $newCounterType
     * @return TaskToCheckEditForm
     */
    public function setNewCounterType(?CounterType $newCounterType): TaskToCheckEditForm
    {
        $this->newCounterType = $newCounterType;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNewTerminalSeal(): ?string
    {
        return $this->newTerminalSeal;
    }

    /**
     * @param string|null $newTerminalSeal
     * @return TaskToCheckEditForm
     */
    public function setNewTerminalSeal(?string $newTerminalSeal): TaskToCheckEditForm
    {
        $this->newTerminalSeal = $newTerminalSeal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNewAntimagneticSeal(): ?string
    {
        return $this->newAntimagneticSeal;
    }

    /**
     * @param string|null $newAntimagneticSeal
     * @return TaskToCheckEditForm
     */
    public function setNewAntimagneticSeal(?string $newAntimagneticSeal): TaskToCheckEditForm
    {
        $this->newAntimagneticSeal = $newAntimagneticSeal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNewSideSeal(): ?string
    {
        return $this->newSideSeal;
    }

    /**
     * @param string|null $newSideSeal
     * @return TaskToCheckEditForm
     */
    public function setNewSideSeal(?string $newSideSeal): TaskToCheckEditForm
    {
        $this->newSideSeal = $newSideSeal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLocality(): ?string
    {
        return $this->locality;
    }

    /**
     * @param string|null $locality
     * @return TaskToCheckEditForm
     */
    public function setLocality(?string $locality): TaskToCheckEditForm
    {
        $this->locality = $locality;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStreet(): ?string
    {
        return $this->street;
    }

    /**
     * @param string|null $street
     * @return TaskToCheckEditForm
     */
    public function setStreet(?string $street): TaskToCheckEditForm
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getHouseNumber(): ?string
    {
        return $this->houseNumber;
    }

    /**
     * @param string|null $houseNumber
     * @return TaskToCheckEditForm
     */
    public function setHouseNumber(?string $houseNumber): TaskToCheckEditForm
    {
        $this->houseNumber = $houseNumber;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPorchNumber(): ?string
    {
        return $this->porchNumber;
    }

    /**
     * @param string|null $porchNumber
     * @return TaskToCheckEditForm
     */
    public function setPorchNumber(?string $porchNumber): TaskToCheckEditForm
    {
        $this->porchNumber = $porchNumber;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getApartmentNumber(): ?string
    {
        return $this->apartmentNumber;
    }

    /**
     * @param string|null $apartmentNumber
     * @return TaskToCheckEditForm
     */
    public function setApartmentNumber(?string $apartmentNumber): TaskToCheckEditForm
    {
        $this->apartmentNumber = $apartmentNumber;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getLodgersCount(): ?int
    {
        return $this->lodgersCount;
    }

    /**
     * @param int|null $lodgersCount
     * @return TaskToCheckEditForm
     */
    public function setLodgersCount(?int $lodgersCount): TaskToCheckEditForm
    {
        $this->lodgersCount = $lodgersCount;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getRoomsCount(): ?int
    {
        return $this->roomsCount;
    }

    /**
     * @param int|null $roomsCount
     * @return TaskToCheckEditForm
     */
    public function setRoomsCount(?int $roomsCount): TaskToCheckEditForm
    {
        $this->roomsCount = $roomsCount;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOperatorComment(): ?string
    {
        return $this->operatorComment;
    }

    /**
     * @param string|null $operatorComment
     * @return TaskToCheckEditForm
     */
    public function setOperatorComment(?string $operatorComment): TaskToCheckEditForm
    {
        $this->operatorComment = $operatorComment;
        return $this;
    }
}