<?php

namespace App\Model;

use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Id\AbstractIdGenerator;

class CustomIdGenerator extends AbstractIdGenerator
{
    /**
     * {@inheritDoc}
     * @throws DBALException
     */
    public function generate(EntityManager $em, $entity)
    {
        $id = $entity->getId();
        if (null !== $id) {
            return $id;
        }

        $conn = $em->getConnection();
        $sql = 'SELECT ' . $conn->getDatabasePlatform()->getGuidExpression();
        return $conn->query($sql)->fetchColumn(0);
    }
}