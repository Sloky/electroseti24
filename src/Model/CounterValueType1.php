<?php


namespace App\Model;

/**
 * Class CounterValueType1
 *
 * Этот класс описывает модель показаний счетчиков 1 ценовой категории.
 * Счетчики данной ценовой категории снимают только одно показание(1 тарифна зона).
 *
 * @package App\Model
 */
class CounterValueType1 implements CounterValueInterface
{
    /**
     * Значение показаний
     * @var float
     */
    protected $value;

    /**
     * Сумма показаний
     * @var float
     */
    protected $valuesSum;

    /**
     * CounterValueType1 constructor.
     * @param array $value
     */
    public function __construct($value = [])
    {
        if (!empty($value)) {
            $this->value = $value[0];
            $this->valuesSum = $value[0];
        }
    }

    /**
     * @param array $value
     */
    public function setValues(array $value)
    {
        $this->value = $value[0];
        $this->valuesSum = $value[0];
    }

    /**
     * @param float $value
     * @return CounterValueType1
     */
    public function setValue(float $value): CounterValueType1
    {
        $this->value = $value;
        $this->valuesSum = $value;
        return $this;
    }

    /**
     * Получить значения показаний всех зон
     * @return array
     */
    public function getValues(): array
    {
        return [$this->value];
    }

    /**
     * Получить сумму всех значений показаний
     * @return float
     */
    public function getValuesSum(): float
    {
        return $this->valuesSum;
    }
}