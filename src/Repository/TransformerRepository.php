<?php

namespace App\Repository;

use App\Entity\Main\Company;
use App\Entity\Main\Transformer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Transformer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Transformer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Transformer[]    findAll()
 * @method Transformer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TransformerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Transformer::class);
    }

    /**
     * @param string|null $search
     * @param array|null $filter
     * @return QueryBuilder
     */
    public function getTransformerWithSearchQueryBuilder(?string $search, ?array $filter): QueryBuilder
    {
        $qb = $this->createQueryBuilder('t');
        if ($search || $filter) {
            $qb = $this->searchTransformersQuery($qb, $filter, $search);
        }
        return $qb;
    }

    /**
     * @param QueryBuilder $qb
     * @param array|null $filter
     * @param string|null $search
     * @return QueryBuilder
     */
    private function searchTransformersQuery(QueryBuilder $qb, ?array $filter, ?string $search): QueryBuilder
    {
        if ($search) {
            $qb->orWhere('t.title LIKE :search')
//                ->orWhere('feeder.title LIKE :search')
//                ->orWhere('substation.title LIKE :search')
            ;
            $qb->setParameter('search', '%' . $search . '%');
        }

        if (!empty($filter)) {
            if (array_key_exists('company', $filter)) {
                $qb->leftJoin('t.company', 'company')
                    ->andWhere('company.id = :companyFilter')
                    ->setParameter('companyFilter', $filter['company']);
            }
            if (array_key_exists('executor', $filter)) {
                $qb->leftJoin('t.users', 'users');
                if ($filter['executor'] === 'empty') {
                    $qb->andWhere('users.id IS NULL');
                } else {
                    $qb->andWhere('users.id  = :executor')
                        ->setParameter('executor', $filter['executor']);
                }
            }
        }

        return $qb;
    }
}
