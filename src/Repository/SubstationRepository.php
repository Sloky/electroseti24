<?php

namespace App\Repository;

use App\Entity\Main\Substation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Substation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Substation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Substation[]    findAll()
 * @method Substation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubstationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Substation::class);
    }

    // /**
    //  * @return Substation[] Returns an array of Substation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Substation
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
