<?php

namespace App\Repository;

use App\Entity\Main\FailureFactor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FailureFactor|null find($id, $lockMode = null, $lockVersion = null)
 * @method FailureFactor|null findOneBy(array $criteria, array $orderBy = null)
 * @method FailureFactor[]    findAll()
 * @method FailureFactor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FailureFactorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FailureFactor::class);
    }

    // /**
    //  * @return FailureFactor[] Returns an array of FailureFactor objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FailureFactor
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
