<?php

namespace App\Repository;

use App\Entity\Main\BypassListRecord;
use App\Entity\Main\Company;
use App\Entity\Main\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BypassListRecord|null find($id, $lockMode = null, $lockVersion = null)
 * @method BypassListRecord|null findOneBy(array $criteria, array $orderBy = null)
 * @method BypassListRecord[]    findAll()
 * @method BypassListRecord[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BypassListRecordRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BypassListRecord::class);
    }

    /**
     * Get bypass list records by search and filter
     * @param string|null $search
     * @param string|null $searchField
     * @param array|null $filter
     * @param User|null $user
     * @return QueryBuilder
     */
    public function getBypassListRecordsWithSearchQueryBuilder(
        ?string $search,
        ?string $searchField,
        ?array $filter,
        ?User $user = null
    ): QueryBuilder
    {
        $companyTitles = $user ? $this->getUserCompanyTitles($user) : null;

        $qb = $this->createQueryBuilder('b');
        if ($search || $filter || $companyTitles) {
            $qb = $this->searchAndFilterQuery($qb, $filter, $search, $searchField, $companyTitles);
        }
        return $qb
            ->orderBy('b.rowPosition', 'ASC');
    }

    /**
     * Get records by items array
     * @param array $items
     * @return int|mixed|string
     */
    public function getBypassRecordsByItems(array $items)
    {
        return $this->createQueryBuilder('b')
            ->where('b.id IN (:items)')
            ->setParameter('items', $items)
            ->orderBy('b.rowPosition', 'ASC')
            ->getQuery()
            ->execute();
    }

    /**
     * Get records by items array and status
     * @param array $items
     * @param int $status
     * @return int|mixed|string
     */
    public function getBypassRecordsByItemsAndStatus(array $items, int $status)
    {
        return $this->createQueryBuilder('b')
            ->where('b.id IN (:items)')
            ->andWhere('b.status = :status')
            ->setParameter('items', $items)
            ->setParameter('status', $status)
            ->orderBy('b.rowPosition', 'ASC')
            ->getQuery()
            ->execute();
    }

    /**
     * Get records by executors
     * @param array $executorsFullNames
     * @return int|mixed|string
     */
    public function getBypassListRecordsByExecutors(
        array $executorsFullNames
    )
    {
        return $this->createQueryBuilder('b')
            ->where('b.controller IN (:executors)')
            ->setParameter('executors', $executorsFullNames)
            ->orderBy('b.rowPosition', 'ASC')
            ->getQuery()
            ->execute();
    }

    /**
     * Get records by companies
     * @param array $companiesTitles
     * @return int|mixed|string
     */
    public function getBypassListRecordsByCompanies(
        array $companiesTitles
    )
    {
        return $this->createQueryBuilder('b')
            ->where('b.companyTitle IN (:companies)')
            ->setParameter('companies', $companiesTitles)
            ->orderBy('b.rowPosition', 'ASC')
            ->getQuery()
            ->execute();
    }

    /**
     * Get record by company title with company`s controllers
     * @param $companyTitle
     * @param array $controllersFullNames
     * @return int|mixed|string
     */
    public function getBypassListRecordsByCompaniesWithUsers(
        $companyTitle,
        array $controllersFullNames
    )
    {
        return $this->createQueryBuilder('b')
            ->leftJoin(User::class, 'user')
            ->orWhere('b.companyTitle = :company')
            ->setParameter('company', $companyTitle)
            ->orWhere('b.controller IN (:controllers)')
            ->setParameter('controllers', $controllersFullNames)
            ->orderBy('b.rowPosition', 'ASC')
            ->getQuery()
            ->execute();
    }

    /**
     * Delete the bypass list record
     * @param $id
     */
    public function deleteBypassListRecord($id)
    {
        $this->createQueryBuilder('b')
            ->where('b.id = :recordId')
            ->setParameter('recordId', $id)
            ->delete()
            ->getQuery()
            ->execute();
    }

    /**
     * Delete all the bypass list records by search and filter
     * @param array $items
     * @param string|null $search
     * @param string|null $searchField
     * @param array|null $filter
     */
    public function deleteBypassListRecords(array $items, ?string $search, ?string $searchField, ?array $filter)
    {
        $qb = $this->createQueryBuilder('b');

        $ids = $items;
        if (empty($items)) {
            if ($search || $filter) {
                $qb = $this->searchAndFilterQuery($qb, $filter, $search, $searchField);
            }
            $ids = $qb->getQuery()->getResult();
        }

        $deleteQuery = $this->createQueryBuilder('b')
            ->where('b.id in (:ids)')
            ->setParameter('ids', $ids);

        $deleteQuery
            ->delete()
            ->getQuery()
            ->execute();
    }

    /**
     * Apply search and filter to query
     * @param QueryBuilder $qb
     * @param array|null $filter
     * @param string|null $search
     * @param string|null $searchField
     * @param string[]|null $companyTitles
     * @return QueryBuilder
     */
    protected function searchAndFilterQuery(
        QueryBuilder $qb,
        ?array $filter,
        ?string $search,
        ?string $searchField,
        ?array $companyTitles = null
    ): QueryBuilder
    {
        if ($search) {
            if (is_null($searchField)) {
                $qb->orWhere('b.agreementNumber LIKE :search')
                    ->orWhere('b.address LIKE :search')
                    ->orWhere('b.subscriberTitle LIKE :search')
                    ->orWhere('b.phones LIKE :search')
                    ->orWhere('b.counterNumber LIKE :search')
                    ->orWhere('b.counterModel LIKE :search')
                    ->orWhere('b.sideSeal LIKE :search')
                    ->orWhere('b.antimagneticSeal LIKE :search')
                    ->orWhere('b.terminalSeal LIKE :search')
                    ->orWhere('b.transformer LIKE :search')
                ;
            } else {
                switch ($searchField) {
                    case 'agreementNumber' :
                        $qb->orWhere('b.agreementNumber LIKE :search');
                        break;
                    case 'address' :
                        $qb->orWhere('b.address LIKE :search');
                        break;
                    case 'subscriberTitle' :
                        $qb->orWhere('b.subscriberTitle LIKE :search');
                        break;
                    case 'phones' :
                        $qb->orWhere('b.phones LIKE :search');
                        break;
                    case 'counterNumber' :
                        $qb->orWhere('b.counterNumber LIKE :search');
                        break;
                    case 'counterModel':
                        $qb->orWhere('b.counterModel LIKE :search');
                        break;
                    case 'sideSeal' :
                        $qb->orWhere('b.sideSeal LIKE :search');
                        break;
                    case 'antimagneticSeal' :
                        $qb->orWhere('b.antimagneticSeal LIKE :search');
                        break;
                    case 'terminalSeal' :
                        $qb->orWhere('b.terminalSeal LIKE :search');
                        break;
                    case 'transformer' :
                        $qb->orWhere('b.transformer LIKE :search');
                        break;
                    default: break;
                }
            }

            $qb->setParameter('search', '%' . $search . '%');
        }

        if ($companyTitles) {
            $qb->andWhere('b.companyTitle in (:companyTitles)')
                ->setParameter('companyTitles', $companyTitles);
        }

        if (!empty($filter)) {
            if (array_key_exists('companyTitle', $filter)) {
                $qb->andWhere('b.companyTitle = :companyFilter')
                    ->setParameter('companyFilter', $filter['companyTitle']);
            }
            if (array_key_exists('subscriberType', $filter)) {
                $qb->andWhere('b.subscriberType = :subscriberTypeFilter')
                    ->setParameter('subscriberTypeFilter', $filter['subscriberType']);
            }
            if (array_key_exists('serviceability', $filter)) {
                $qb->andWhere('b.serviceability = :serviceabilityFilter')
                    ->setParameter('serviceabilityFilter', $filter['serviceability']);
            }
            if (array_key_exists('executor', $filter)) {
                if ($filter['executor'] === 'empty') {
                    $qb->andWhere('b.controller IS NULL OR b.controller = :emptyValue')
                        ->setParameter('emptyValue', '');
                } else {
                    $qb->andWhere('b.controller = :executorFilter')
                        ->setParameter('executorFilter', $filter['executor']);
                }
            }
            if (array_key_exists('status', $filter)) {
                $qb->andWhere('b.status = :statusFilter')
                    ->setParameter('statusFilter', $filter['status']);
            }
            if (array_key_exists('period', $filter)) {
                $qb->andWhere('b.period = :periodFilter')
                    ->setParameter('periodFilter', $filter['period']);
            }
        }
        return $qb;
    }

    /**
     * Get bypass records count for operators
     * @param User $user
     * @return float|int|mixed|string
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getBypassRecordsCount(User $user)
    {
        $qb = $this->createQueryBuilder('b');

        $qb
            ->select('count(b.id)')
            ->andWhere('b.companyTitle in (:companyTitles)')
            ->setParameter('companyTitles', $this->getUserCompanyTitles($user))
        ;

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * Get User company titles
     * @param User|null $user
     * @return array|null
     */
    private function getUserCompanyTitles(?User $user): ?array
    {
        $companyTitles = null;
        /** @var Company[] $companies */
        $companies = $user->getCompanies()->getValues();
        foreach ($companies as $company) {
            $companyTitles[] = $company->getTitle();
        }
        return $companyTitles;
    }
}
