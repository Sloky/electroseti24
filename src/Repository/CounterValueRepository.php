<?php

namespace App\Repository;

use App\Entity\Main\CounterValue;
use DateTimeImmutable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CounterValue|null find($id, $lockMode = null, $lockVersion = null)
 * @method CounterValue|null findOneBy(array $criteria, array $orderBy = null)
 * @method CounterValue[]    findAll()
 * @method CounterValue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CounterValueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CounterValue::class);
    }

    /**
     * @param string $agreementId
     * @param string $counterId
     * @param DateTimeImmutable $date
     * @return \App\Entity\Main\CounterValue[]
     */
    public function findByAgreementAndCounter(string $counterId, string $agreementId, DateTimeImmutable $date): array
    {
        return $this->createQueryBuilder('cv')
            ->leftJoin('cv.counter', 'counter')
            ->andWhere('counter.agreement = :agreementId')
            ->setParameter('agreementId', $agreementId)
            ->andWhere('cv.counter = :counterId')
            ->setParameter('counterId', $counterId)
            ->andWhere('cv.date = :date')
            ->setParameter('date', $date)
            ->orderBy('cv.date', 'DESC')
            ->getQuery()
            ->execute()
        ;
    }

    /**
     * Get the last counterValues by counters
     * @param array $counterIds
     * @return float|int|mixed|string
     */
    public function getLastValuesByCountersSQLQuery(array $counterIds)
    {
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult(CounterValue::class, 'cv');
        $rsm->addFieldResult('cv', 'id', 'id');
        $rsm->addFieldResult('cv', 'date', 'date');
        $rsm->addFieldResult('cv', 'value', 'value');

        $rsm->addScalarResult('counter', 'counter');

        $sql = $this->getEntityManager()->createNativeQuery(
            'SELECT * FROM counter_value JOIN (SELECT MAX(date) AS mDate, counter FROM counter_value WHERE counter IN (?) GROUP BY counter) m ON counter_value.date = m.mDate AND counter_value.counter = m.counter;',
            $rsm
        );
        $sql->setParameter(1, $counterIds);
        return $sql->getResult();
    }
}
