<?php

namespace App\Repository;

use App\Entity\Main\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    /**
     * @param array $companies
     * @return int|mixed|string
     */
    public function getControllersByCompanies(array $companies)
    {
        return $this->createQueryBuilder('u')
            ->select('u')
            ->leftJoin('u.groups', 'groups')
            ->leftJoin('u.companies', 'companies')
            ->andWhere('groups.groupName = :grName')
            ->setParameter('grName', 'Controllers')
            ->andWhere('companies.id IN (:coms)')
            ->setParameter('coms', $companies)
            ->getQuery()
            ->execute();
    }

    public function getControllers()
    {
        return $this->createQueryBuilder('u')
            ->select('u')
            ->leftJoin('u.groups', 'groups')
            ->where('groups.groupName = :grName')
            ->setParameter('grName', 'Controllers')
            ->getQuery()
            ->execute();
    }

    public function getUsersByFullNameAndCompanyTitle(
        string $surname,
        string $name,
        string $patronymic,
        string $companyTitle
    )
    {
        return $this->createQueryBuilder('u')
            ->select('u')
            ->leftJoin('u.companies', 'company')
            ->andWhere('u.surname = :surname')
            ->andWhere('u.name = :name')
            ->andWhere('u.patronymic = :patronymic')
            ->andWhere('company.title = :companyTitle')
            ->setParameter('companyTitle', $companyTitle)
            ->setParameter('surname', $surname)
            ->setParameter('name', $name)
            ->setParameter('patronymic', $patronymic)
            ->getQuery()
            ->execute();
    }

    /**
     * @param string|null $search
     * @param array|null $filter
     * @return QueryBuilder
     */
    public function getUsersWithSearchQueryBuilder(?string $search, ?array $filter): QueryBuilder
    {
        $qb = $this->createQueryBuilder('u');
        if ($search || $filter) {
            $qb = $this->searchUsersQuery($qb, $filter, $search);
        }
        return $qb;
    }

    /**
     * @param QueryBuilder $qb
     * @param array|null $filter
     * @param string|null $search
     * @return QueryBuilder
     */
    private function searchUsersQuery(QueryBuilder $qb, ?array $filter, ?string $search): QueryBuilder
    {
        if ($search) {
            $qb->orWhere('u.phone LIKE :search');
            $qb->orWhere('CONCAT(u.name, \' \', u.patronymic, \' \', u.surname) LIKE :search');
            $qb->orWhere('CONCAT(u.surname, \' \',u.name, \' \', u.patronymic) LIKE :search');
            $qb->setParameter('search', '%' . $search . '%');
        }

        if (!empty($filter)) {
            if (array_key_exists('company', $filter)) {
                $qb->leftJoin('u.companies', 'companies')
                    ->andWhere('companies.id = :companyFilter')
                    ->setParameter('companyFilter', $filter['company']);
            }
            if (array_key_exists('group', $filter)) {
                $qb->leftJoin('u.groups', 'groups')
                    ->andWhere('groups.id = :groupFilter')
                    ->setParameter('groupFilter', $filter['group']);
            }
            if (array_key_exists('status', $filter)) {
                $qb->andWhere('u.workStatus = :statusFilter')
                    ->setParameter('statusFilter', $filter['status']);
            }
        }

        return $qb;
    }
}
