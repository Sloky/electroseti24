<?php

namespace App\Repository;

use App\Entity\Main\Agreement;
use App\Entity\Main\Counter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Counter|null find($id, $lockMode = null, $lockVersion = null)
 * @method Counter|null findOneBy(array $criteria, array $orderBy = null)
 * @method Counter[]    findAll()
 * @method Counter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CounterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Counter::class);
    }

    /**
     * @param $counterNumber
     * @param $agreementTitle
     * @return float|int|mixed|string
     */
    public function findCounterByNumberAndAgreementTitle($counterNumber, $agreementTitle)
    {
        return $this->createQueryBuilder('c')
            ->select('c')
            ->leftJoin('c.agreement', 'agreement')
            ->andWhere('agreement.number = :agTitle')
            ->andWhere('c.title = :counterTitle')
            ->setParameter('counterTitle', $counterNumber)
            ->setParameter('agTitle', $agreementTitle)
            ->getQuery()
            ->execute();
    }

    /**
     * @param string|null $search
     * @param array|null $filter
     * @return float|int|mixed|string
     */
    public function getCounterIdsWithSearchQueryBuilder(
        ?string $search,
        ?array $filter
    )
    {
        $qb = $this->createQueryBuilder('c');
        $qb->select('c.id', );
        if ($search || $filter) {
            $qb = $this->searchCounterQuery($qb, $filter, $search);
        }
        return $qb
            ->orderBy('c.title', 'DESC')
            ->getQuery()->execute();
    }

    /**
     * @param string|null $search
     * @param array|null $filter
     * @return QueryBuilder
     */
    public function getCountersWithSearchQueryBuilder(
        ?string $search,
        ?array $filter
    ): QueryBuilder
    {
        $qb = $this->createQueryBuilder('c');
        if ($search || $filter) {
            $qb = $this->searchCounterQuery($qb, $filter, $search);
        }
        return $qb
            ->orderBy('c.title', 'DESC');
    }

    /**
     * @param \App\Entity\Main\Agreement $agreement
     * @param string|null $search
     * @return QueryBuilder
     */
    public function getCountersByAgreementWithSearchQueryBuilder(
        Agreement $agreement,
        ?string $search
    ): QueryBuilder
    {
        $qb = $this->createQueryBuilder('c');
        if ($search) {
            $qb = $this->searchCounterQuery($qb, null, $search);
        }
        return $qb->andWhere('c.agreement = :agreement')
            ->setParameter('agreement', $agreement)
            ->orderBy('c.title', 'DESC');
    }

    /**
     * @param QueryBuilder $qb
     * @param array|null $filter
     * @param string|null $search
     * @return QueryBuilder
     */
    private function searchCounterQuery(
        QueryBuilder $qb,
        ?array $filter,
        ?string $search
    ): QueryBuilder
    {
        $qb->leftJoin('c.agreement', 'a')
            ->leftJoin('c.address', 'address')
            ->leftJoin('a.subscriber', 'subscriber')
        ;
        if ($search) {
            $qb->orWhere('a.number LIKE :search')
                ->orWhere('c.title LIKE :search')
                ->orWhere('subscriber.title LIKE :search')
                ->orWhere('subscriber.phones LIKE :search')
                ->orWhere('c.temporaryAddress LIKE :search')
                ->orWhere('address.locality LIKE :search')
                ->orWhere('address.street LIKE :search')
                ->orWhere('address.houseNumber LIKE :search')
                ->orWhere('address.apartmentNumber LIKE :search')
            ;
            $qb->setParameter('search', '%' . $search . '%');
        }

        if (!empty($filter)) {
            if (array_key_exists('transformer', $filter)) {
                $qb->leftJoin('c.transformer', 't')
                    ->andWhere('t.title = :transformerFilter')
                    ->setParameter('transformerFilter', $filter['transformer']);
            }
            if (array_key_exists('company', $filter)) {
                $qb->leftJoin('t.company', 'comp')
                    ->andWhere('comp.id = :companyFilter')
                    ->setParameter('companyFilter', $filter['company']);
            }
        }
        return $qb;
    }
}
