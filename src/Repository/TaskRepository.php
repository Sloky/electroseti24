<?php

namespace App\Repository;

use App\Entity\Main\Task;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Task|null find($id, $lockMode = null, $lockVersion = null)
 * @method Task|null findOneBy(array $criteria, array $orderBy = null)
 * @method Task[]    findAll()
 * @method Task[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Task::class);
    }

    /**
     * For Task list (if the user is Operator)
     * @param int $status
     * @param array $companies
     * @param string|null $search
     * @param string|null $searchField
     * @param array|null $filter
     * @return QueryBuilder
     */
    public function getTasksByCompaniesWithSearchQueryBuilder(
        int $status,
        array $companies,
        ?string $search,
        ?string $searchField,
        ?array $filter
    ): QueryBuilder
    {
        $qb = $this->createQueryBuilder('t');
        if ($search || $filter) {
            $qb = $this->searchTaskQuery($qb, $status, $filter, $search, $searchField);
        }
        return $qb
            ->andWhere('t.company in (:companies)')
            ->setParameter('companies', $companies)
            ->andWhere('t.status = :status')
            ->setParameter('status', $status)
            ->orderBy('t.priority', 'DESC');
    }

    /**
     * For Task list (if the user is Controller)
     * @param int $status
     * @param string $controllerId
     * @param array $companies
     * @param string|null $search
     * @param string|null $searchField
     * @param array|null $filter
     * @return QueryBuilder
     */
    public function getTasksByControllerWithSearchQueryBuilder(
        int $status,
        string $controllerId,
        array $companies,
        ?string $search,
        ?string $searchField,
        ?array $filter
    ): QueryBuilder
    {
        $qb = $this->createQueryBuilder('t');
        if ($search || $filter) {
            $qb = $this->searchTaskQuery($qb, $status, $filter, $search, $searchField);
        }
        return $qb
            ->andWhere('t.company in (:companies)')
            ->setParameter('companies', $companies)
            ->andWhere('t.executor = :controllerId OR t.executor is NULL')
            ->setParameter('controllerId', $controllerId)
            ->andWhere('t.status = :status')
            ->setParameter('status', $status)
            ->orderBy('t.priority', 'DESC');
    }

    /**
     * For Task list (if the user is Admin)
     * @param int $status
     * @param string|null $search
     * @param string|null $searchField
     * @param array|null $filter
     * @return QueryBuilder
     */
    public function getTasksWithSearchQueryBuilder(
        int $status,
        ?string $search,
        ?string $searchField,
        ?array $filter
    ): QueryBuilder
    {
        $qb = $this->createQueryBuilder('t');
        if ($search || $filter) {
            $qb = $this->searchTaskQuery($qb, $status, $filter, $search, $searchField);
        }
        return $qb
            ->andWhere('t.status = :status')
            ->setParameter('status', $status)
            ->orderBy('t.priority', 'DESC');
    }

    /**
     * Task searching
     * @param QueryBuilder $qb
     * @param string $status
     * @param array|null $filter
     * @param string|null $search
     * @param string|null $searchField
     * @return QueryBuilder
     * @throws \Exception
     */
    private function searchTaskQuery(
        QueryBuilder $qb,
        string $status,
        ?array $filter,
        ?string $search,
        ?string $searchField
    ): QueryBuilder
    {
        $qb->leftJoin('t.agreement', 'a')
            ->leftJoin('t.subscriber', 's')
            ->leftJoin('t.counter', 'c')
            ->leftJoin('c.address', 'address');
        if ($search) {
            if (is_null($searchField)) {
                $qb->orWhere('a.number LIKE :search')
                    ->orWhere('s.title LIKE :search')
                    ->orWhere('c.title LIKE :search')
                    ->orWhere('c.temporaryAddress LIKE :search')
                    ->orWhere('address.locality LIKE :search')
                    ->orWhere('address.street LIKE :search')
                    ->orWhere('address.houseNumber LIKE :search')
                    ->orWhere('address.apartmentNumber LIKE :search')
                    ->orWhere('t.deadline LIKE :search')
                ;
                if ($status == 1 || $status == 2) {
                    $qb->orWhere('t.executedDate LIKE :search');
                }
                if ($status == 2) {
                    $qb->orWhere('t.acceptanceDate LIKE :search');
                }
            } else {
                switch ($searchField) {
                    case 'subscriberTitle' :
                        $qb->orWhere('s.title LIKE :search');
                        break;
                    case 'address' :
                        $qb->orWhere('c.temporaryAddress LIKE :search')
                            ->orWhere('address.locality LIKE :search')
                            ->orWhere('address.street LIKE :search')
                            ->orWhere('address.houseNumber LIKE :search')
                            ->orWhere('address.apartmentNumber LIKE :search')
                        ;
                        break;
                    case 'agreementNumber' :
                        $qb->orWhere('a.number LIKE :search');
                        break;
                    case 'deadline' :
                        $search = $this->transformDateTimeSearchToISO($search);
                        $qb->orWhere('t.deadline LIKE :search');
                        break;
                    case 'executedDate' :
                        $search = $this->transformDateTimeSearchToISO($search);
                        $qb->orWhere('t.executedDate LIKE :search');
                        break;
                    case 'acceptanceDate':
                        $search = $this->transformDateTimeSearchToISO($search);
                        $qb->orWhere('t.acceptanceDate LIKE :search');
                        break;
                    case 'counterNumber' :
                        $qb->orWhere('c.title LIKE :search');
                        break;
                    default: break;
                }
            }
            if (preg_match('/(\d+)\.(\d+)\.(\d+)\,?\s?(\d+)?\:?(\d+)?\:?(\d+)?/', $search) === 1) {
                $search = $this->transformDateTimeSearchToISO($search);
            }

            $qb->setParameter('search', '%' . $search . '%');
        }

        if (!empty($filter)) {
            if (array_key_exists('company', $filter)) {
                $qb->leftJoin('t.company', 'comp')
                    ->andWhere('comp.id = :companyFilter')
                    ->setParameter('companyFilter', $filter['company']);
            }
            if (array_key_exists('user', $filter)) {
                $qb->leftJoin('t.executor', 'user')
                    ->andWhere('user.id = :userFilter')
                    ->setParameter('userFilter', $filter['user']);
            }
            if (array_key_exists('subscriberType', $filter)) {
                $qb->andWhere('s.subscriberType = :subscriberType')
                    ->setParameter('subscriberType', $filter['subscriberType']);
            }
            if (array_key_exists('period', $filter)) {
                $period = new DateTimeImmutable($filter['period']);
                $qb->andWhere('t.period = :period')
                    ->setParameter('period', $period);
            }
        }
        return $qb;
    }

    /**
     * Transform string date to ISO format
     * @param string $search
     * @return string
     */
    private function transformDateTimeSearchToISO(string $search): string
    {
        if (preg_match('/(\d+)\.(\d+)\.(\d+)\,/', $search) === 1) {
            $search = str_replace(',', '', $search);
        }


        $arr = explode(' ', $search);

        if (count($arr) > 1) {
            $ISOSearch = implode('-', array_reverse(explode('.', $arr[0]))).' '.$arr[1];
        } else {
            $ISOSearch = implode('-', array_reverse(explode('.', $arr[0])));
        }
        return $ISOSearch;
    }

    /**
     * Get all Task`s periods
     * @return int|mixed|string
     */
    public function getTasksPeriods()
    {
        return $this->createQueryBuilder('t')
            ->select('t.period')
            ->groupBy('t.period')
            ->getQuery()
            ->execute();
    }

    /**
     * Get all Task`s periods by the status
     * @param $taskStatus
     * @return int|mixed|string
     */
    public function getTasksPeriodsByStatus($taskStatus)
    {
        $query = $this->createQueryBuilder('t')
            ->select('t.period')
            ->groupBy('t.period');
        if ($taskStatus !== 'all') {
            $query->where('t.status = :type')
                ->setParameter('type', $taskStatus);
        }
        return $query->getQuery()->execute();
    }

    /**
     * Remove Tasks by status, filter and search
     * @param int|null $status
     * @param array|null $filter
     * @param string|null $search
     * @param string|null $searchField
     */
    public function removeTasks(?int $status, ?array $filter, ?string $search, ?string $searchField)
    {
        $qb = $this->createQueryBuilder('t');

        if ($search || $filter) {
            $qb = $this->searchTaskQuery($qb, $status, $filter, $search, $searchField);
        }

        if (!is_null($status)) {
            $qb->andWhere('t.status = :taskStatus')
                ->setParameter('taskStatus', $status);
        }

        $ids = $qb->getQuery()->getResult();

        $deleteQuery = $this->createQueryBuilder('t')
            ->where('t.id in (:ids)')
            ->setParameter('ids', $ids);

        $deleteQuery
            ->delete()
            ->getQuery()
            ->execute();
    }

    /**
     * Remove all Tasks
     * @return int|mixed|string
     */
    public function removeAllTheTasks()
    {
        return $this->createQueryBuilder('t')
            ->delete()
            ->getQuery()
            ->execute();
    }

    /**
     * Remove Task
     * @param string $taskId
     */
    public function removeTheTask(string $taskId): void
    {
        $this->createQueryBuilder('t')
            ->where('t.id = :taskId')
            ->setParameter('taskId', $taskId)
            ->delete()
            ->getQuery()
            ->execute();
    }

    /**
     * Get User's task count
     * @param string $userId
     * @param DateTimeInterface|null $period
     * @return float|int|mixed|string
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getUsersTaskCount(string $userId, ?DateTimeInterface $period = null)
    {
        $qb = $this->createQueryBuilder('t')
            ->select('count(t.id)')
            ->where('t.executor = :id')
            ->setParameter('id', $userId)
        ;
        if (!is_null($period)) {
            $qb->andWhere('t.period = :period')
                ->setParameter('period', $period);
        }
        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * Get User's task to work count
     * @param string|null $userId
     * @param DateTimeInterface|null $period
     * @return float|int|mixed|string
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getUsersTaskToWorkCount(string $userId, ?DateTimeInterface $period = null)
    {
        $qb = $this->createQueryBuilder('t')
            ->select('count(t.id)')
            ->where('t.executor = :id')
            ->setParameter('id', $userId)
            ->andWhere('t.status = :status')
            ->setParameter('status', 0)
        ;
        if (!is_null($period)) {
            $qb->andWhere('t.period = :period')
                ->setParameter('period', $period);
        }
        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * Get User's completed task count
     * @param string|null $userId
     * @param DateTimeInterface|null $period
     * @return float|int|mixed|string
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getUsersCompletedTaskCount(string $userId, ?DateTimeInterface $period = null)
    {
        $qb = $this->createQueryBuilder('t')
            ->select('count(t.id)')
            ->andWhere('t.executor = :id')
            ->setParameter('id', $userId)
            ->andWhere('t.status = :status')
            ->setParameter('status', 2)
        ;
        if (!is_null($period)) {
            $qb->andWhere('t.period = :period')
                ->setParameter('period', $period);
        }
        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * Get User's executed task count
     * @param string $userId
     * @param DateTimeInterface|null $period
     * @return float|int|mixed|string
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getExecutedTaskCount(string $userId, ?DateTimeInterface $period = null)
    {
        $qb = $this->createQueryBuilder('t')
            ->select('count(t.id)')
            ->andWhere('t.executor = :id')
            ->setParameter('id', $userId)
            ->andWhere('t.status <> :status')
            ->setParameter('status', 0)
        ;
        if (!is_null($period)) {
            $qb->andWhere('t.period = :period')
                ->setParameter('period', $period);
        }
        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * Get task execution statistics by the period
     * @param string $userId
     * @return float|int|mixed|string
     */
    public function getUserTaskStatistic(string $userId)
    {
        $qb = $this->createQueryBuilder('t')
            ->select('count(t.id) as taskCount, t.status as taskStatus, t.counterPhysicalStatus')
            ->where('t.executor = :id')
            ->setParameter('id', $userId)
            ->addSelect('t.period as period')
            ->groupBy('t.period')
            ->addGroupBy('t.status')
            ->addGroupBy('t.counterPhysicalStatus')
        ;
        return $qb->getQuery()->execute();
    }

    /**
     * Get Task count by status and companies
     * @param int $status
     * @param array $companies
     * @return float|int|mixed|string
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getTaskTCount(
        int $status,
        array $companies
    )
    {
        $qb = $this->createQueryBuilder('t');

        $qb
            ->select('count(t.id)')
            ->andWhere('t.company in (:companies)')
            ->setParameter('companies', $companies)
            ->andWhere('t.status = :status')
            ->setParameter('status', $status)
            ;

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * Get user's Task count by status
     * @param int $status
     * @return float|int|mixed|string
     */
    public function getUsersWithTasksCount(int $status = 0)
    {
        $qb = $this->createQueryBuilder('t');

        $qb
            ->leftJoin('t.executor', 'user')
            ->select('user.id, count(t.id) as taskCount')

            ->andWhere('t.status = :status')
            ->setParameter('status', $status)
            ->groupBy('user.id')
        ;
        return $qb->getQuery()->execute();
    }

    /**
     * @param array $statuses
     * @param array $companies
     * @return float|int|mixed|string
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getTaskByCompaniesAndStatuses(
        array $statuses,
        array $companies
    )
    {
        $qb = $this->createQueryBuilder('t');

        $qb
            ->select('count(t.id) as taskCount')
            ->andWhere('t.company in (:companies)')
            ->setParameter('companies', $companies)
            ->andWhere('t.status in (:statuses)')
            ->setParameter('statuses', $statuses)
        ;

        return $qb->getQuery()->getSingleScalarResult();
    }
}
