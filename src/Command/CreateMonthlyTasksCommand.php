<?php

namespace App\Command;

use App\Handler\TaskHandler\TaskToWorkHandler;
use MyBuilder\Bundle\CronosBundle\Annotation\Cron;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Cron(minute="/2", noLogs=true)
 */
class CreateMonthlyTasksCommand extends Command
{
    protected static $defaultName = 'app:create-monthly-tasks';

    private TaskToWorkHandler $taskToWorkHandler;

    public function __construct(TaskToWorkHandler $taskToWorkHandler, $name = null)
    {
        parent::__construct($name);
        $this->taskToWorkHandler = $taskToWorkHandler;
    }

    protected function configure()
    {
        $this
            ->setDescription('Tasks creation')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', '600');
        $message = 'Tasks have been created';
        $io = new SymfonyStyle($input, $output);

        $errors = $this->taskToWorkHandler->createTasksToWork();

        if (!empty($errors)) {
            $message = join('\n', $errors);
        }

        $io->success($message);

        return 0;
    }
}
