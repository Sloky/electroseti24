<?php

namespace App\Command;

use App\Handler\BypassListHandler\BypassListHandler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CreateBypassListCommand extends Command
{
    protected static $defaultName = 'app:create-bypass-list';

    private BypassListHandler $bypassListHandler;

    public function __construct(BypassListHandler $bypassListHandler, $name = null)
    {
        parent::__construct($name);
        $this->bypassListHandler = $bypassListHandler;
    }

    protected function configure()
    {
        $this
            ->setDescription('Bypass list creation')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', '600');
        $message = 'Tasks have been created';
        $io = new SymfonyStyle($input, $output);

        $result = $this->bypassListHandler->createBypassList();

        if ($result === 1) {
            $message = "Bypass list created";
        } else {
            $message = "Something went wrong";
        }

        $io->success($message);

        return 0;
    }
}