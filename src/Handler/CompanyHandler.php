<?php


namespace App\Handler;


use App\Entity\Main\Company;
use App\Entity\Main\User;
use App\Form\CompanyFormType;
use App\Handler\UserHandler\UserHandler;
use App\Repository\CompanyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CompanyHandler
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var CompanyRepository
     */
    public $companyRepository;

    /**
     * @var UserHandler
     */
    private $userHandler;

    /**
     * @var PaginatorInterface
     */
    private $paginator;

    public function __construct(
        FormFactoryInterface   $formFactory,
        EntityManagerInterface $entityManager,
        CompanyRepository      $companyRepository,
        UserHandler            $userHandler,
        PaginatorInterface     $paginator
    )
    {
        $this->formFactory = $formFactory;
        $this->em = $entityManager;
        $this->companyRepository = $companyRepository;
        $this->userHandler = $userHandler;
        $this->paginator = $paginator;
    }

    /**
     * Get all the companies
     * @return \App\Entity\Main\Company[]|null
     */
    public function getCompanies(int $page = 1, int $maxResult = null): ?array
    {
        $companiesQuery = $this->companyRepository->createQueryBuilder('c');

        if ($maxResult) {
            $pagination = $this->paginator->paginate($companiesQuery, $page, $maxResult);
            $companies = $pagination->getItems();
            $companiesCount = $pagination->getTotalItemCount();
        } else {
            /** @var \App\Entity\Main\Company[] $companies */
            $companies = $companiesQuery->getQuery()->execute();
            $companiesCount = count($companies);
        }

        return [
            'companies' => $companies,
            'companiesCount' => $companiesCount
        ];
    }

    /**
     * Get Company
     * @param string $id
     * @return \App\Entity\Main\Company|null
     */
    public function getCompany(string $id): ?Company
    {
        return $this->companyRepository->find($id);
    }

    /**
     * Get Company by title
     * @param string $title
     * @return \App\Entity\Main\Company|null
     */
    public function getCompanyByTitle(string $title): ?Company
    {
        return $this->companyRepository->findOneBy(['title' => $title]);
    }

    /**
     * @return mixed|null
     */
    public function getBranches()
    {
        /** @var \App\Entity\Main\Company[] $mainCompanies */
        $mainCompanies = $this->companyRepository->findMainCompanies();
        if (empty($mainCompanies)) {
            return null;
        }
        $branches = [];
        foreach ($mainCompanies as $company) {
            $branches = array_merge($branches, $company->getChildren()->getValues());
        }
        return $branches;
    }

    /**
     * Get Companies for the filter
     * @return \App\Entity\Main\Company[]
     */
    public function getCompaniesForFilter(): array
    {
        /** @var User $currentUser */
        $currentUser = $this->userHandler->getCurrentUser();
        if ($this->userHandler->isUserAdmin($currentUser)) {
            $companies = $this->companyRepository->findAll();
        } else {
            $companies = $currentUser->getCompanies()->getValues();
        }
        return $companies;
    }

    /**
     * Create a new company
     * @param Request $request
     * @return \App\Entity\Main\Company|null
     */
    public function createNewCompany(Request $request): ?Company
    {
        $company = new Company();
        $form = $this->formFactory->create(CompanyFormType::class, $company, ['csrf_protection' => false]);
        $form->submit($request->request->all());
        $form->handleRequest($request);

        if (!$form->isValid()) {
            throw new HttpException(Response::HTTP_BAD_REQUEST, 'Incorrect form data');
        }
        try {
            $this->em->persist($company);
            $this->em->flush();
            return $company;
        } catch (Exception $exception) {
            throw new HttpException(Response::HTTP_INTERNAL_SERVER_ERROR, $exception->getMessage());
        }
    }

    /**
     * Edit the company
     * @param Request $request
     * @param \App\Entity\Main\Company $company
     * @return \App\Entity\Main\Company|null
     */
    public function editCompany(Request $request, Company $company): ?Company
    {
        $form = $this->formFactory->create(CompanyFormType::class, $company);
        $form->submit($request->request->all());
        $form->handleRequest($request);

        if (!$form->isValid()) {
            return null;
        }
        try {
            $this->em->persist($company);
            $this->em->flush();
        } catch (Exception $exception) {
            return null;
        }
        return $company;
    }

    /**
     * Delete the company
     * @param \App\Entity\Main\Company $company
     * @return bool
     */
    public function deleteCompany(Company $company): bool
    {
        try {
            $this->em->remove($company);
            $this->em->flush();
        } catch (Exception $exception) {
            return false;
        }
        return true;
    }
}