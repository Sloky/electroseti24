<?php


namespace App\Handler;


use App\Entity\Main\Subscriber;
use App\Form\SubscriberIndividualFormType;
use App\Form\SubscriberLegalEntityFormType;
use App\Model\SubscriberIndividualForm;
use App\Model\SubscriberLegalEntityForm;
use App\Repository\SubscriberRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SubscriberHandler
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    /**
     * @var SubscriberRepository
     */
    protected $subscriberRepository;

    /**
     * @var PaginatorInterface
     */
    protected $paginator;

    public function __construct(
        EntityManagerInterface $entityManager,
        FormFactoryInterface $formFactory,
        SubscriberRepository $subscriberRepository,
        PaginatorInterface $paginator
    )
    {
        $this->em = $entityManager;
        $this->formFactory = $formFactory;
        $this->subscriberRepository = $subscriberRepository;
        $this->paginator = $paginator;
    }

    /**
     * Get the subscriber
     * @param $subscriberId
     * @return Subscriber|null
     */
    public function getSubscriber($subscriberId)
    {
        return $this->subscriberRepository->find($subscriberId);
    }

    /**
     * Get all the subscribers
     * @return \App\Entity\Main\Subscriber[]|null
     */
    public function getSubscribers()
    {
        $subscribers = $this->subscriberRepository->findAll();
        if (empty($subscribers)) {
            return null;
        }
        return $subscribers;
    }

    /**
     * Get subscriber by title
     * @param string $title
     * @return Subscriber|null
     */
    public function getSubscriberByTitle(string $title)
    {
        return $this->subscriberRepository->findOneBy(['title' => $title]);
    }

    /**
     * Get Subscribers by type
     * @param int $subscriberType
     * @param int $page
     * @param int|null $maxResult
     * @param string|null $search
     * @return array
     */
    public function getSubscribersWithSearch(
        int $subscriberType,
        int $page = 1,
        ?int $maxResult = 100,
        ?string $search = null
    ): array
    {
        $subscribersQB = $this->em->getRepository(Subscriber::class)
            ->createQueryBuilder('s');

        if ($search) {
            $subscribersQB
                ->leftJoin('s.agreements', 'agreements')
                ->orWhere('s.title LIKE :search')
                ->orWhere('s.name LIKE :search')
                ->orWhere('s.surname LIKE :search')
                ->orWhere('s.patronymic LIKE :search')
                ->orWhere('agreements.number LIKE :search')
                ->setParameter('search', '%' . $search . '%');
            ;
        }

        $subscribersQB->andWhere('s.subscriberType = :subscriberType')
            ->setParameter('subscriberType', $subscriberType)
            ->orderBy('s.title')
        ;

        if ($maxResult) {
            $pagination = $this->paginator->paginate($subscribersQB, $page, $maxResult);
            $subscribers = $pagination->getItems();
            $subscribersCount = $pagination->getTotalItemCount();
        } else {
            /** @var Subscriber[] $subscribers */
            $subscribers = $subscribersQB->getQuery()->execute();
            $subscribersCount = count($subscribers);
        }

        return [
            'subscribers' => $subscribers,
            'subscribersCount' => $subscribersCount
        ];
    }

    /**
     * Add new Subscriber (legal entity)
     * @param Request $request
     * @return void
     * @throws Exception
     */
    public function addSubscriberLegalEntity(Request $request)
    {
        $subscriberLegalEntity = new SubscriberLegalEntityForm();
        $form = $this->formFactory->create(SubscriberLegalEntityFormType::class, $subscriberLegalEntity);
        $form->submit($request->request->all());
        $form->handleRequest($request);

        if (!$form->isValid()) {
            throw new HttpException(
                Response::HTTP_BAD_REQUEST,
                'Invalid form data'
            );
        }

        try {
            $this->em->beginTransaction();

            $subscriber = new Subscriber();
            $subscriber->setSubscriberType(1);
            $subscriber->setTitle($subscriberLegalEntity->getTitle());
            $subscriber->setPhones($subscriberLegalEntity->getPhones()->getValues());
            $subscriber->setAddress($subscriberLegalEntity->getAddress());
            $subscriber->setTIN($subscriberLegalEntity->getTIN());
            $subscriber->setEmail($subscriberLegalEntity->getEmail());
            if ($subscriberLegalEntity->getCoordinateX() && $subscriberLegalEntity->getCoordinateY()) {
                $subscriber->setCoordinates([
                    $subscriberLegalEntity->getCoordinateX(),
                    $subscriberLegalEntity->getCoordinateY()
                ]);
            }
            $subscriber->setWebSite($subscriberLegalEntity->getWebSite());

            $this->em->persist($subscriber);
            $this->em->flush();
            $this->em->commit();
        } catch (Exception $exception) {
            $this->em->rollback();
            throw $exception;
        }
    }

    /**
     * Edit the Subscriber (legal entity)
     * @param Subscriber $subscriber
     * @param Request $request
     * @return Subscriber
     * @throws Exception
     */
    public function editSubscriberLegalEntity(Subscriber $subscriber, Request $request): Subscriber
    {
        $subscriberLegalEntity = new SubscriberLegalEntityForm();
        $form = $this->formFactory->create(SubscriberLegalEntityFormType::class, $subscriberLegalEntity);
        $form->submit($request->request->all());
        $form->handleRequest($request);

        if (!$form->isValid()) {
            throw new HttpException(
                Response::HTTP_BAD_REQUEST,
                'Invalid form data'
            );
        }

        try {
            $this->em->beginTransaction();

            $subscriber->setTitle($subscriberLegalEntity->getTitle());
            $subscriber->setPhones($subscriberLegalEntity->getPhones()->getValues());
            $subscriber->setAddress($subscriberLegalEntity->getAddress());
            $subscriber->setTIN($subscriberLegalEntity->getTIN());
            $subscriber->setEmail($subscriberLegalEntity->getEmail());
            if ($subscriberLegalEntity->getCoordinateX() && $subscriberLegalEntity->getCoordinateY()) {
                $subscriber->setCoordinates([
                    $subscriberLegalEntity->getCoordinateX(),
                    $subscriberLegalEntity->getCoordinateY()
                ]);
            }
            $subscriber->setWebSite($subscriberLegalEntity->getWebSite());

            $this->em->persist($subscriber);
            $this->em->flush();
            $this->em->commit();
        } catch (Exception $exception) {
            $this->em->rollback();
            throw $exception;
        }
        return $subscriber;
    }

    /**
     * Add new Subscriber (individual)
     * @param Request $request
     * @return void
     * @throws Exception
     */
    public function addSubscriberIndividual(Request $request)
    {
        $subscriberIndividual = new SubscriberIndividualForm();
        $form = $this->formFactory->create(SubscriberIndividualFormType::class, $subscriberIndividual);
        $form->submit($request->request->all());
        $form->handleRequest($request);

        if (!$form->isValid()) {
            throw new HttpException(
                Response::HTTP_BAD_REQUEST,
                'Invalid form data'
            );
        }

        try {
            $this->em->beginTransaction();

            $subscriber = new Subscriber();
            $subscriber->setSubscriberType(0);
            $subscriber->setSurname($subscriberIndividual->getSurname());
            $subscriber->setName($subscriberIndividual->getName());
            $subscriber->setPatronymic($subscriberIndividual->getPatronymic());
            $subscriber->setPhones($subscriberIndividual->getPhones()->getValues());
            $subscriber->setAddress($subscriberIndividual->getAddress());
            $subscriber->setEmail($subscriberIndividual->getEmail());
            if ($subscriberIndividual->getCoordinateX() && $subscriberIndividual->getCoordinateY()) {
                $subscriber->setCoordinates([
                    $subscriberIndividual->getCoordinateX(),
                    $subscriberIndividual->getCoordinateY()
                ]);
            }

            $this->em->persist($subscriber);
            $this->em->flush();
            $this->em->commit();
        } catch (Exception $exception) {
            $this->em->rollback();
            throw $exception;
        }
    }

    /**
     * Edit the Subscriber (individual)
     * @param Subscriber $subscriber
     * @param Request $request
     * @return Subscriber
     * @throws Exception
     */
    public function editSubscriberIndividual(Subscriber $subscriber, Request $request): Subscriber
    {
        $subscriberIndividual = new SubscriberIndividualForm();
        $form = $this->formFactory->create(SubscriberIndividualFormType::class, $subscriberIndividual);
        $form->submit($request->request->all());
        $form->handleRequest($request);

        if (!$form->isValid()) {
            throw new HttpException(
                Response::HTTP_BAD_REQUEST,
                'Invalid form data'
            );
        }

        try {
            $this->em->beginTransaction();

            $subscriber->setSurname($subscriberIndividual->getSurname());
            $subscriber->setName($subscriberIndividual->getName());
            $subscriber->setPatronymic($subscriberIndividual->getPatronymic());
            $subscriber->setPhones($subscriberIndividual->getPhones()->getValues());
            $subscriber->setAddress($subscriberIndividual->getAddress());
            $subscriber->setEmail($subscriberIndividual->getEmail());
            if ($subscriberIndividual->getCoordinateX() && $subscriberIndividual->getCoordinateY()) {
                $subscriber->setCoordinates([
                    $subscriberIndividual->getCoordinateX(),
                    $subscriberIndividual->getCoordinateY()
                ]);
            }

            $this->em->persist($subscriber);
            $this->em->flush();
            $this->em->commit();
        } catch (Exception $exception) {
            $this->em->rollback();
            throw $exception;
        }
        return $subscriber;
    }
}