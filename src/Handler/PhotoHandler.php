<?php


namespace App\Handler;


use App\Entity\Main\Photo;
use App\Entity\Main\Task;
use App\Handler\S3Handler\UploadHandler;
use App\Repository\PhotoRepository;
use App\Repository\TaskRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use League\Flysystem\FilesystemException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class PhotoHandler
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    /**
     * @var PhotoRepository
     */
    protected $photoRepository;

    /**
     * @var TaskRepository
     */
    protected $taskRepository;

    /**
     * @var ParameterBagInterface
     */
    protected $parametrBag;

    protected $s3UploadHandler;

    const PHOTOS_DIR = __DIR__ . "/../../public/photos/";

    /**
     * PhotoHandler constructor.
     * @param EntityManagerInterface $em
     * @param FormFactoryInterface $formFactory
     * @param PhotoRepository $photoRepository
     * @param TaskRepository $taskRepository
     */
    public function __construct(
        EntityManagerInterface $em,
        FormFactoryInterface $formFactory,
        PhotoRepository $photoRepository,
        TaskRepository $taskRepository,
        ParameterBagInterface $parameterBag,
        UploadHandler $uploadHandler
    )
    {
        $this->em = $em;
        $this->formFactory = $formFactory;
        $this->photoRepository = $photoRepository;
        $this->taskRepository = $taskRepository;
        $this->parametrBag = $parameterBag;
        $this->s3UploadHandler = $uploadHandler;
    }

    /**
     * Get Photos
     * @return \App\Entity\Main\Photo[]
     */
    public function getPhotos(): array
    {
        return $this->photoRepository->findAll();
    }

    /**
     * Get Photo
     * @param $id
     * @return \App\Entity\Main\Photo|null
     */
    public function getPhoto($id): ?Photo
    {
        return $this->photoRepository->find($id);
    }

    /**
     * Get Photos by Task
     * @param $taskId
     * @return \App\Entity\Main\Photo[]
     */
    public function getPhotosByTask($taskId): array
    {
        return $this->photoRepository->findBy(['counter' => $taskId]);
    }

    /**
     * Delete all Photos by Task
     * @param Task $task
     * @return bool
     */
    public function deletePhotosByTask(Task $task): bool
    {
        $photos = $this->photoRepository->findBy(['task' => $task->getId()]);
        try {
            foreach ($photos as $photo) {
                if (!($photo instanceof Photo)) {
                    throw new HttpException(Response::HTTP_BAD_REQUEST, 'Incorrect array data type');
                }
                $this->em->remove($photo);
                $this->em->flush();
            }
        } catch (Exception $exception) {
            return false;
        }
        return true;
    }

    /**
     *  Delete all photo files by Task
     * @param Task $task
     * @return bool
     * @throws FilesystemException
     */
    public function deletePhotosFilesByTask(Task $task): bool
    {
        $period = $task->getPeriod()->format('Y-m');
        $basePath = '/photos/' . $period . '/' . $task->getId() . '/';
        $photos = $this->photoRepository->findBy(['task' => $task->getId()]);
        try {
            foreach ($photos as $photo) {
                $fileName = $basePath.$photo->getFileName();
                $this->s3UploadHandler->deleteFile($fileName);
            }
        } catch (Exception $exception) {
            return false;
        }
        return true;
    }

    /**
     * Delete Photo
     * @param string $id
     * @param string|null $filePath
     * @return bool
     */
    public function deletePhoto(string $id, ?string $filePath): bool
    {
        try {
            if (!$photo = $this->getPhoto($id)) {
                throw new HttpException(Response::HTTP_NOT_FOUND, "There is no photo with ID=$id");
            }
            if (($filePath !== null) && unlink($filePath)) {
                throw new HttpException(Response::HTTP_BAD_REQUEST, "Failed to delete file");
            }
            $this->em->remove($photo);
            $this->em->flush();
        } catch (Exception $exception) {
            return false;
        }
        return true;
    }

    /**
     * Delete the photo file
     * @param string $filePath
     */
    public function deletePhotoFile(string $filePath)
    {
        unlink($filePath);
    }

    /**
     * Delete the photos dir
     * @param string $dirPath
     * @param bool $saveRootDir
     */
    public function deletePhotosFolder(string $dirPath, bool $saveRootDir = false)
    {
        $dir = opendir($dirPath);
        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                $full = $dirPath . '/' . $file;
                if ( is_dir($full) ) {
                    $this->deletePhotosFolder($full);
                }
                else {
                    unlink($full);
                }
            }
        }
        closedir($dir);
        if (!$saveRootDir) {
            rmdir($dirPath);
        }
    }

    /**
     * Delete the photos dir by period
     * @param string $period
     * @return bool
     */
    public function deletePhotosByPeriod(string $period): bool
    {
        $dirPath = realpath(self::PHOTOS_DIR).'/'.$period.'/';
        try {
            return rmdir($dirPath);
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Delete all the photos
     * @return bool
     */
    public function deleteAllThePhotos(): bool
    {
        $dirPath = realpath(self::PHOTOS_DIR).'/';
        try {
            $this->deletePhotosFolder($dirPath, true);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Get S3 server public url (for photos)
     * @return string
     */
    public function getS3DirPublicUrl(): string
    {
        return $this->parametrBag->get('public_photos_url');
    }
}