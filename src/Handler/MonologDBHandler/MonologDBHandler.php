<?php

namespace App\Handler\MonologDBHandler;

use App\Entity\Services\Log;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Monolog\Handler\AbstractProcessingHandler;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class MonologDBHandler extends AbstractProcessingHandler
{
    /**
     * @var ManagerRegistry
     */
    protected $managerRegistry;

    /**
     * MonologDBHandler constructor.
     * @param ManagerRegistry $managerRegistry
     */
    public function __construct(
        ManagerRegistry $managerRegistry
    )
    {
        parent::__construct();
        $this->managerRegistry = $managerRegistry;
    }

    /**
     * Called when writing to our database
     * @param array $record
     */
    protected function write(array $record)
    {
        /** @var EntityManager $em */
        $em = $this->managerRegistry->getManager('services');

        if (!$em->isOpen()) {
            $em = $this->managerRegistry->resetManager('services');
        }

        $logEntry = new Log();
        $logEntry->setMessage($record['message']);
        $logEntry->setLevel($record['level']);
        $logEntry->setLevelName($record['level_name']);
        $logEntry->setExtra($record['extra']);
//        $logEntry->setContext($record['context']);

        $em->persist($logEntry);
        $em->flush();
    }
}