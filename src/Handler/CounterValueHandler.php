<?php


namespace App\Handler;


use App\Entity\Main\CounterValue;
use App\Repository\CounterValueRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;

class CounterValueHandler
{
    /**
     * @var EntityManagerInterface
     */
    protected EntityManagerInterface $em;

    /**
     * @var FormFactoryInterface
     */
    protected FormFactoryInterface $formFactory;

    /**
     * @var CounterValueRepository
     */
    protected CounterValueRepository $counterValueRepository;

    /**
     * @param EntityManagerInterface $entityManager
     * @param FormFactoryInterface $formFactory
     * @param CounterValueRepository $counterValueRepository
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        FormFactoryInterface $formFactory,
        CounterValueRepository $counterValueRepository
    )
    {
        $this->em = $entityManager;
        $this->formFactory = $formFactory;
        $this->counterValueRepository = $counterValueRepository;
    }

    /**
     * Get the CounterValue
     * @param $counterValueId
     * @return CounterValue|null
     */
    public function getCounterValue($counterValueId): ?CounterValue
    {
        return $this->counterValueRepository->find($counterValueId);
    }

    /**
     * Get the last CounterValue
     * @param $counterId
     * @return \App\Entity\Main\CounterValue
     *@todo Проверить правильность работы
     */
    public function getLastCounterValueByCounter($counterId): ?CounterValue
    {
        $values = $this->counterValueRepository->findBy(['counter' => $counterId], ['date' => 'DESC']);
        if (empty($values)) {
            return null;
        }
        return $values[0];
    }

    /**
     * @param string $counterId
     * @param DateTimeImmutable|null $date
     * @return CounterValue|null
     */
    public function getCounterValueByCounterAndDate(string $counterId, ?DateTimeImmutable $date): ?CounterValue
    {
        if (!$date) {
            return null;
        }
        return $this->counterValueRepository
            ->findOneBy(['counter' => $counterId, 'date' => $date/*->format('Y-m-d H:i:s')*/]);
    }

    /**
     * @param string $counterId
     * @param string $agreementId
     * @param DateTimeImmutable|null $date
     * @return CounterValue|null
     */
    public function getCounterValueByCounterAndAgreement(
        string $counterId,
        string $agreementId,
        ?DateTimeImmutable $date
    ): ?CounterValue
    {
        if (!$date) {
            return null;
        }
        /** @var CounterValue $counterValues */
        $counterValues =  $this->counterValueRepository
            ->findByAgreementAndCounter($counterId, $agreementId, $date);
        return !empty($counterValues) ? $counterValues[0] : null;
    }
}