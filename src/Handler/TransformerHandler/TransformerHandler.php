<?php

namespace App\Handler\TransformerHandler;

use App\Entity\Main\Company;
use App\Entity\Main\Counter;
use App\Entity\Main\Feeder;
use App\Entity\Main\Substation;
use App\Entity\Main\Transformer;
use App\Entity\Main\User;
use App\Form\TPFormType;
use App\Model\Assembler\TPAssembler;
use App\Model\DTO\TPDTO;
use App\Model\TPForm;
use App\Repository\TransformerRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class TransformerHandler
{
    /**
     * @var EntityManager
     */
    private EntityManagerInterface $em;

    /**
     * @var TransformerRepository
     */
    public TransformerRepository $transformerRepository;

    /**
     * @var FormFactoryInterface
     */
    private FormFactoryInterface $formFactory;

    /**
     * @var PaginatorInterface
     */
    protected PaginatorInterface $paginator;

    /**
     * @param EntityManagerInterface $entityManager
     * @param FormFactoryInterface $formFactory
     * @param TransformerRepository $transformerRepository
     * @param PaginatorInterface $paginator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        FormFactoryInterface   $formFactory,
        TransformerRepository  $transformerRepository,
        PaginatorInterface     $paginator
    )
    {
        $this->em = $entityManager;
        $this->formFactory = $formFactory;
        $this->transformerRepository = $transformerRepository;
        $this->paginator = $paginator;
    }

    /**
     * Get transformer DTOs with filter and search
     * @param int $page
     * @param int|null $maxResult
     * @param string|null $search
     * @param array $filter
     * @return TPDTO[]
     */
    public function getTransformerList(int $page = 1, ?int $maxResult = null, string $search = null, array $filter = []): array
    {
        $transformerQuery = $this->transformerRepository->getTransformerWithSearchQueryBuilder($search, $filter);
        if ($maxResult) {
            $pagination = $this->paginator->paginate($transformerQuery, $page, $maxResult);
            $transformers = $pagination->getItems();
            $transformersCount = $pagination->getTotalItemCount();
        } else {
            /** @var Counter[] $transformers */
            $transformers = $transformerQuery->getQuery()->execute();
            $transformersCount = count($transformers);
        }

        $transformerDTOs = [];
        foreach ($transformers as $transformer) {
            $countersCount = $this->em->getRepository(Counter::class)->count(['transformer' => $transformer->getId()]);
            $transformerDTOs[] = TPAssembler::writeDTO($transformer, $countersCount);
        }

        return [
            'transformerList' => $transformerDTOs,
            'transformersCount' => $transformersCount
        ];
    }

    /**
     * Get Transformer by title if exists
     * @param string $title
     * @return Transformer|null
     */
    public function getTransformerByTitle(string $title): ?Transformer
    {
        return $this->transformerRepository->findOneBy(['title' => $title]);
    }

    /**
     * Get Feeder by title if exists
     * @param string $title
     * @return \App\Entity\Main\Feeder|null
     */
    public function getFeederByTitle(string $title): ?Feeder
    {
        return $this->em->getRepository(Feeder::class)->findOneBy(['title' => $title]);
    }

    /**
     * Get Substation by title if exists
     * @param string $title
     * @return Substation|null
     */
    public function getSubstationByTitle(string $title): ?Substation
    {
        return $this->em->getRepository(Substation::class)->findOneBy(['title' => $title]);
    }

    /**
     * Get transformers DTOs by company
     * @param \App\Entity\Main\Company $company
     * @param string|null $search
     * @param string|null $executor
     * @param int $page
     * @param int|null $maxResult
     * @return TPDTO[]
     */
    public function getTransformersByCompany(
        Company $company,
        ?string $search,
        ?string $executor,
        int     $page = 1,
        ?int    $maxResult = null
    ): array
    {
        $transformersQB = $this->transformerRepository->getTransformerWithSearchQueryBuilder(
            $company,
            $executor,
            $search
        );

        if ($maxResult) {
            $pagination = $this->paginator->paginate($transformersQB, $page, $maxResult);
            $transformers = $pagination->getItems();
            $transformersCount = $pagination->getTotalItemCount();
        } else {
            /** @var \App\Entity\Main\Counter[] $transformers */
            $transformers = $transformersQB->getQuery()->execute();
            $transformersCount = count($transformers);
        }

        $transformerDTOs = [];
        foreach ($transformers as $transformer) {
            $countersCount = $this->em->getRepository(Counter::class)->count(['transformer' => $transformer->getId()]);
            $transformerDTOs[] = TPAssembler::writeDTO($transformer, $countersCount);
        }
        return [
            'transformers' => $transformerDTOs,
            'transformersCount' => $transformersCount
        ];
    }

    /**
     * Add new transformer (with substation and feeder if they don't exist)
     * @param Request $request
     * @return void
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function addNewTransformer(Request $request)
    {
        $TPCreationForm = new TPForm();
        $form = $this->formFactory->create(TPFormType::class, $TPCreationForm);
        $form->submit($request->request->all());
        $form->handleRequest($request);

        if ($form->isValid()) {
            if ($this->getTransformerByTitle($TPCreationForm->getTransformer())) {
                throw new HttpException(
                    Response::HTTP_BAD_REQUEST,
                    "Transformer with title = {$TPCreationForm->getTransformer()} already exists"
                );
            }
            try {
                $this->em->beginTransaction();
                $substation = null;
                $feeder = null;
                if ($TPCreationForm->getSubstation() && !($substation = $this->getSubstationByTitle($TPCreationForm->getSubstation()))) {
                    $substation = new Substation();
                    $substation->setTitle($TPCreationForm->getSubstation());
                    $this->em->persist($substation);
                }
                if ($TPCreationForm->getFeeder() && !($feeder = $this->getFeederByTitle($TPCreationForm->getFeeder()))) {
                    $feeder = new Feeder();
                    $feeder->setTitle($TPCreationForm->getFeeder());
                    $feeder->setSubstation($substation);
                    $this->em->persist($substation);
                }
                $transformer = new Transformer();
                $transformer->setTitle($TPCreationForm->getTransformer());
                $transformer->setCompany($TPCreationForm->getCompany());
                $transformer->setFeeder($feeder);
                if (!empty($TPCreationForm->getExecutors()->getValues())) {
                    /** @var User $executor */
                    foreach ($TPCreationForm->getExecutors()->getValues() as $executor) {
                        $transformer->addUser($executor);
                    }
                }
                if ($TPCreationForm->getCoordinateX() && $TPCreationForm->getCoordinateY()) {
                    $transformer->setCoords([
                        $TPCreationForm->getCoordinateX(),
                        $TPCreationForm->getCoordinateY()
                    ]);
                }
                $this->em->persist($transformer);
                $this->em->flush();
                $this->em->commit();
                /*} catch (\Doctrine\DBAL\Exception\UniqueConstraintViolationException $uniqueException) {
                    $this->em->rollback();
                    throw new HttpException(
                        Response::HTTP_BAD_REQUEST,
                        "Фидер {$feeder->getTitle()} уже привязан к другому ТП"
                    );*/
            } catch (Exception $exception) {
                $this->em->rollback();
                throw $exception;
            }
        } else {
            throw new HttpException(
                Response::HTTP_BAD_REQUEST,
                'Incorrect form data'
            );
        }
    }

    /**
     * Edit the transformer
     * @param Transformer $transformer
     * @param Request $request
     * @return Transformer
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function editTransformer(Transformer $transformer, Request $request): Transformer
    {
        $TPCreationForm = new TPForm();
        $form = $this->formFactory->create(TPFormType::class, $TPCreationForm);
        $form->submit($request->request->all());
        $form->handleRequest($request);

        if ($form->isValid()) {
            if (
                ($transformer->getTitle() !== $TPCreationForm->getTransformer())
                && $this->getTransformerByTitle($TPCreationForm->getTransformer())
            ) {
                throw new HttpException(
                    Response::HTTP_BAD_REQUEST,
                    "Transformer with title = {$TPCreationForm->getTransformer()} already exists"
                );
            }
            try {
                $this->em->beginTransaction();
                $substation = null;
                $feeder = null;
                if ($TPCreationForm->getSubstation() && !($substation = $this->getSubstationByTitle($TPCreationForm->getSubstation()))) {
                    $substation = new Substation();
                    $substation->setTitle($TPCreationForm->getSubstation());
                    $this->em->persist($substation);
                }
                if ($TPCreationForm->getFeeder() && !($feeder = $this->getFeederByTitle($TPCreationForm->getFeeder()))) {
                    $feeder = new Feeder();
                    $feeder->setTitle($TPCreationForm->getFeeder());
                    $feeder->setSubstation($substation);
                    $this->em->persist($substation);
                }
                $transformer->setTitle($TPCreationForm->getTransformer());
                $transformer->setCompany($TPCreationForm->getCompany());
                $transformer->setFeeder($feeder);
                $transformer->setUsers($TPCreationForm->getExecutors());
                if ($TPCreationForm->getCoordinateX() && $TPCreationForm->getCoordinateY()) {
                    $transformer->setCoords([
                        $TPCreationForm->getCoordinateX(),
                        $TPCreationForm->getCoordinateY()
                    ]);
                }
                $this->em->persist($transformer);
                $this->em->flush();
                $this->em->commit();
            } catch (Exception $exception) {
                $this->em->rollback();
                throw $exception;
            }
        } else {
            throw new HttpException(
                Response::HTTP_BAD_REQUEST,
                'Incorrect form data'
            );
        }
        return $transformer;
    }

    /**
     * @param Transformer $transformer
     * @return void
     * @throws ORMException
     */
    public function deleteTransformer(Transformer $transformer)
    {
        $this->em->remove($transformer);
        $this->em->flush();
    }
}