<?php

namespace App\Handler\TaskHandler;

use App\Entity\Main\Agreement;
use App\Entity\Main\Company;
use App\Entity\Main\Counter;
use App\Entity\Main\CounterValue;
use App\Entity\Main\Photo;
use App\Entity\Main\Subscriber;
use App\Entity\Main\Task;
use App\Entity\Main\User;
use App\Form\TaskMobileUploadFormType;
use App\Model\CounterValueType1;
use App\Model\TaskMobileUploadForm;
use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use ZipArchive;

class TaskHandler extends AbstractTaskHandler
{
    /**
     * Get all the tasks
     * @return Task[]|null
     */
    public function getTasks(): ?array
    {
        $tasks = $this->taskRepository->findAll();
        if (empty($tasks)) {
            return null;
        }
        return $tasks;
    }

    /**
     * Get the task
     * @param $taskId
     * @return Task|null
     */
    public function getTask($taskId): ?Task
    {
        return $this->taskRepository->find($taskId);
    }

    /**
     * Get Task by Counter and period
     * @param $counterId
     * @param DateTimeInterface $period
     * @return Task|null
     */
    public function getTaskByCounterAndPeriod($counterId, DateTimeInterface $period): ?Task
    {
        return $this->taskRepository->findOneBy([
            'counter' => $counterId,
            'period' => $period
        ]);
    }

    /**
     * Get Tasks periods by Task type
     * @return mixed
     */
    public function getTasksPeriodsByStatus(string $status) {
        switch ($status) {
            case '0' :
            case '1' :
            case '2' :
            case 'all' : break;
            default : throw new HttpException(Response::HTTP_BAD_REQUEST, 'Incorrect Task status');
        }
        return $this->taskRepository->getTasksPeriodsByStatus($status);
    }

    /**
     * Create a new dir if not exist
     * @param string $dirName
     * @return bool
     */
    protected function createTaskDirIfNotExist(string $dirName): bool {
        if (!is_dir($dirName)) {
            if (!mkdir($dirName)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Make the task's photos folder dir if not exist
     * @throws Exception
     */
    protected function makeTaskDirIfNotExist(Task $task): string
    {
        $period = $task->getPeriod()->format('Y-m');
        $currentMonthDir = realpath(self::PHOTOS_DIR).'/'.$period;
        if (!$this->createTaskDirIfNotExist($currentMonthDir)) {
            throw new Exception('Error creating photo directory', 400);
        }
        $dirName = realpath(self::PHOTOS_DIR).'/'.$period.'/'.$task->getId();
        if (!$this->createTaskDirIfNotExist($dirName)) {
            throw new Exception('Error creating photo directory', 400);
        }
        return $dirName;
    }

    /**
     * Get the period dir name
     * @param Task $task
     * @return string
     */
    protected function getPeriodDir(Task $task): string
    {
        $currentMonth = $task->getPeriod()->format('Y-m');
        return realpath(self::PHOTOS_DIR).'/'.$currentMonth;
    }

    /**
     * Upload "task to check" from the mobile app
     * @param Request $request
     * @param string $taskId
     * @throws Exception
     */
    public function uploadTaskFromMobileApp(Request $request, string $taskId) {
        $formData = new TaskMobileUploadForm();
        $form = $this->formFactory->create(TaskMobileUploadFormType::class, $formData, ['csrf_protection' => false]);
        $submittedData = array_merge($request->request->all(), $request->files->all());
        $form->submit($submittedData);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($task = $this->getTask($taskId)) {
                $this->em->beginTransaction();

                try {
                    $status = $formData->getStatus();
                    $counterPhysicalStatus = $formData->getCounterPhysicalStatus();
                    $counterValue = $formData->getCounterValue();
                    $counterValuePhoto = $formData->getCounterValuePhoto();
                    $counterPhoto = $formData->getCounterPhoto();
                    $counterNumber = $formData->getCounterNumber();
                    $counterTypeString = $formData->getCounterTypeString();
                    $terminalSeal = $formData->getTerminalSeal();
                    $terminalSealPhoto = $formData->getTerminalSealPhoto();
                    $antimagneticSeal = $formData->getAntimagneticSeal();
                    $antimagneticSealPhoto = $formData->getAntimagneticSealPhoto();
                    $sideSeal = $formData->getSideSeal();
                    $sideSealPhoto = $formData->getSideSealPhoto();
                    $lodgersCount = $formData->getLodgersCount();
                    $roomsCount = $formData->getRoomsCount();
                    $controllerComment = $formData->getControllerComment();
                    $newCoordinateX = $formData->getGPSLatitude();
                    $newCoordinateY = $formData->getGPSLongitude();
                    $executedDateTime = $formData->getExecutedDateTime();

                    $photosDirName = $this->makeTaskDirIfNotExist($task);
                    $currentUser = $this->userHandler->getCurrentUser();

                    $task->setExecutor($currentUser);
                    $status && $task->setStatus($status);
                    $counterPhysicalStatus && $task->setCounterPhysicalStatus($counterPhysicalStatus);

                    //Сохранение показаний УУ
                    if ($counterValue && $counterPhysicalStatus == 0) {
                        $newCounterValue = new CounterValue();
                        $newCounterValue->setCounter($task->getCounter());
                        $newCounterValue->setInspector($currentUser);
                        $newCounterValue->setValue(new CounterValueType1([$counterValue]));
                        $newCounterValue->setDate(new DateTimeImmutable($executedDateTime->format('Y-m-d H:i:s')));
                        $this->em->persist($newCounterValue);

                        //Сохранение расхода (разница показаний)
                        if ($task->getLastCounterValue()) {
                            $consumption = $counterValue - $task->getLastCounterValue()->getValue()->getValues()[0];
                            $task->setConsumption($consumption);
                        }

                        //Сохранение фотографии показаний
                        if ($counterValuePhoto) {
                            $counterValueName = uniqid('counter_value_photo_') . '.' . $counterValuePhoto->guessExtension();
                            $counterValuePhoto->move(realpath($photosDirName), $counterValueName);
                            $newCounterValuePhoto = new Photo();
                            $newCounterValuePhoto->setFileName($counterValueName);
                            $newCounterValuePhoto->setTask($task);
                            $this->em->persist($newCounterValuePhoto);
                        }

                        $task->setCurrentCounterValue($newCounterValue);
                    }

                    //Сохранение фотографии неисправности/недоступности УУ
                    if ($counterPhoto) {
                        $counterPhotoName = uniqid('counter_photo_') . '.' . $counterPhoto->guessExtension();
                        $counterPhoto->move(realpath($photosDirName), $counterPhotoName);
                        $newCounterPhoto = new Photo();
                        $newCounterPhoto->setFileName($counterPhotoName);
                        $newCounterPhoto->setTask($task);
                        $this->em->persist($newCounterPhoto);
                    }

                    //Сохранение номера УУ
                    if ($counterNumber) {
                        $task->setChangedCounterNumber($counterNumber);
                    }

                    //Сохранение типа УУ
                    if ($counterTypeString) {
                        $task->setChangedCounterTypeString($counterTypeString);
                    }

                    //Сохранение клеммной пломбы
                    if ($terminalSeal) {
                        $task->setChangedTerminalSeal($terminalSeal);
                    }

                    //Сохранение фотографии клеммной пломбы
                    if ($terminalSealPhoto) {
                        $terminalSealPhotoName = uniqid('terminal_seal_photo_') . '.' . $terminalSealPhoto->guessExtension();
                        $terminalSealPhoto->move(realpath($photosDirName), $terminalSealPhotoName);
                        $newTerminalPhoto = new Photo();
                        $newTerminalPhoto->setFileName($terminalSealPhotoName);
                        $newTerminalPhoto->setTask($task);
                        $this->em->persist($newTerminalPhoto);
                    }

                    //Сохранение антимагнитной пломбы
                    if ($antimagneticSeal) {
                        $task->setChangedAntimagneticSeal($antimagneticSeal);
                    }

                    //Сохранение фотографии антимагнитной пломбы
                    if ($antimagneticSealPhoto) {
                        $antimagneticSealPhotoName = uniqid('antimagnetic_seal_photo_') . '.' . $antimagneticSealPhoto->guessExtension();
                        $antimagneticSealPhoto->move(realpath($photosDirName), $antimagneticSealPhotoName);
                        $newAntimagneticSealPhoto = new Photo();
                        $newAntimagneticSealPhoto->setFileName($antimagneticSealPhotoName);
                        $newAntimagneticSealPhoto->setTask($task);
                        $this->em->persist($newAntimagneticSealPhoto);
                    }

                    //Сохранение боковой пломбы
                    if ($sideSeal) {
                        $task->setChangedSideSeal($sideSeal);
                    }

                    //Сохранение фотографии боковой пломбы
                    if ($sideSealPhoto) {
                        $sideSealPhotoName = uniqid('side_seal_photo_') . '.' . $sideSealPhoto->guessExtension();
                        $sideSealPhoto->move(realpath($photosDirName), $sideSealPhotoName);
                        $newSideSealPhoto = new Photo();
                        $newSideSealPhoto->setFileName($sideSealPhotoName);
                        $newSideSealPhoto->setTask($task);
                        $this->em->persist($newSideSealPhoto);
                    }

                    //Сохранение числа проживающих
                    if ($lodgersCount) {
                        $task->setChangedLodgersCount($lodgersCount);
                    }

                    //Сохранение числа комнат
                    if ($roomsCount) {
                        $task->setChangedRoomsCount($roomsCount);
                    }

                    //Сохранение примечания контролёра
                    if ($controllerComment) {
                        $task->setControllerComment($controllerComment);
                    }

                    //Сохранение координат места выполнения задачи (места фотофиксации счётчика)
                    if ($newCoordinateX && $newCoordinateY) {
                        $task->setNewCoordinates([$newCoordinateX, $newCoordinateY]);
                    }

                    //Сохранение даты выполнения задачи
                    if ($executedDateTime) {
                        $task->setExecutedDate(new DateTimeImmutable($executedDateTime->format('Y-m-d H:i:s')));
                    }

                    $this->em->persist($task);
                    $this->em->flush();
                    $this->em->commit();
                } catch (Exception $e) {
                    $this->em->rollback();
                    throw $e;
                }
            }
        }
        $this->em->clear();
    }

    /**
     * Get the upload task photos response
     * @param $taskId
     * @return mixed|string
     * @throws Exception
     */
    public function getUploadTaskPhotosResponse($taskId): Response
    {
        if (!$task = $this->getTask($taskId)) {
            throw new HttpException(
                404,
                "There is no Task with ID=$taskId"
            );
        }

        $dirPath = $this->makeTaskDirIfNotExist($task);

        if (!$dir = scandir($dirPath)) {
            throw new HttpException(
                400,
                "There is no catalog of Task with ID=$taskId"
            );
        }

        $files = [];

        foreach ($dir as $fileName) {
            if ($fileName !== '.' && $fileName !== '..') {
                $filePath = $dirPath.'/'.$fileName;
                $files[] = $filePath;
            }
        }

        $zip = new ZipArchive();
        $zip_name = "../public/zipFileName.zip";

        if (!($zip->open($zip_name, ZipArchive::CREATE | ZipArchive::OVERWRITE) === TRUE)) {
            throw new HttpException(400, 'An error occurred while creating the file');
        }

        foreach ($files as $f) {
            $zip->addFromString(basename($f),  file_get_contents($f));
        }
        $zip->close();

        $response = new Response(
            file_get_contents($zip_name),
            Response::HTTP_OK,
            ['Content-Type' => 'application/zip',
                'Content-Disposition' => 'attachment; filename="' . basename($zip_name) . '"',
                'Content-Length' => filesize($zip_name)]);

        unlink($zip_name);

        return $response;
    }

    /**
     * Get all the tasks photos
     * @param \App\Entity\Main\User $user
     * @param array $filter
     * @param string|null $search
     * @param string|null $searchField
     * @param int $page
     * @param int|null $maxResult
     * @return Response
     */
    public function getUploadAllTheTasksPhotosResponse(
        User $user,
        array $filter = [],
        string $search = null,
        string $searchField = null,
        int $page = 1,
        int $maxResult = null
    ): Response
    {
        if ($this->userHandler->isUserAdmin($user)) {
            $tasksQuery = $this->taskRepository->getTasksWithSearchQueryBuilder(
                2,
                $search,
                $searchField,
                $filter
            );
        } else {
            $tasksQuery = $this->taskRepository->getTasksByCompaniesWithSearchQueryBuilder(
                2,
                $user->getCompanies()->getValues(),
                $search,
                $searchField,
                $filter
            );
        }

        if ($maxResult) {
            if ($page) {
                $tasksQuery->setFirstResult($page > 1 ? ($page-1)*$maxResult : 0);
            }
            $tasksQuery->setMaxResults($maxResult);
        }
        /** @var $tasks Task[] */
        $tasks = $tasksQuery->getQuery()->execute();

        if (empty($tasks)) {
            throw new HttpException(Response::HTTP_BAD_REQUEST, "No Tasks for Photos uploading");
        }

        $zip = new ZipArchive();
        $zip_name = "../public/zipFileName.zip";

        if (!($zip->open($zip_name, ZipArchive::CREATE | ZipArchive::OVERWRITE) === TRUE)) {
            throw new HttpException(400, 'An error occurred while creating the file');
        }

        foreach ($tasks as $task) {
            $periodStr = $task->getPeriod()->format('Y-m');
            $agreementNumber = $task->getAgreement()->getNumber();
            $counterNumber = $task->getCounter()->getTitle();

            // Название папки задачи в архиве
            $zipTaskDir = $periodStr.'_'.$agreementNumber.'_'.$counterNumber;

            // Получаем папку периода
            $periodDir = $this->getPeriodDir($task);

            // Получаем папку задачи
            $taskDir = $periodDir.'/'.$task->getId();

            if (is_dir($taskDir)) {
                $zip->addEmptyDir($zipTaskDir);
                $files = $this->getAttachmentsPaths(scandir($taskDir), $taskDir);
                foreach ($files as $file) {
                    $zip->addFile($file,  $zipTaskDir.'/'.basename($file));
                }
            }

        }
        $zip->close();

        $response = new Response(
            file_get_contents($zip_name),
            Response::HTTP_OK,
            ['Content-Type' => 'application/zip',
                'Content-Disposition' => 'attachment; filename="' . basename($zip_name) . '"',
                'Content-Length' => filesize($zip_name)]);

        unlink($zip_name);

        return $response;
    }

    /**
     * Get the dir attachments paths
     * @param $attachments
     * @param $dirPath
     * @return array
     */
    protected function getAttachmentsPaths($attachments, $dirPath): array
    {
        $attachmentsPaths = [];
        foreach ($attachments as $attachmentName) {
            if ($attachmentName !== '.' && $attachmentName !== '..') {
                $attachmentPath = $dirPath.'/'.$attachmentName;
                $attachmentsPaths[] = $attachmentPath;
            }
        }
        return $attachmentsPaths;
    }

    /**
     * Get json data from mod1
     * @throws Exception
     */
    public function synchronizeMod1Data()
    {
        $data = null;
        $url = $this->parameterBag->get('mod1_api_url');
        if($curl = curl_init()) {
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            $data = curl_exec($curl);
            if ($data === false) {
                throw new HttpException(Response::HTTP_BAD_REQUEST, 'Bad request');
            }
            curl_close($curl);
        }
        $dataArray = json_decode($data, true);

        if ($dataArray['code'] !== 200 && $dataArray['success'] !== true ) {
            throw new HttpException(Response::HTTP_BAD_REQUEST, 'Incorrect data');
        }

        $em = $this->em;
        $em->getConnection()->setAutoCommit(false);

        $vedomostPeriod = $dataArray['vedomost'];

        $vedomostData = $dataArray['data'];

        $vedomostDataLength = count($vedomostData);

        foreach ($vedomostData as $key => $subscriberData) {
            $em->getConnection()->beginTransaction();
            try {
                if (!$subscriber = $this->subscriberHandler->getSubscriber($subscriberData['id'])) {
//                    if (!$subscriber = $this->subscriberHandler->getSubscriberByTitle($subscriberData['title'])) {
                        $subscriber = new Subscriber();
                        $subscriber->setId($subscriberData['id']);
                        $subscriber->setSubscriberType($subscriberData['type']);
//                    }

                    $subscriber->setTitle($subscriberData['title']);
                    $phones = [];
                    foreach ($subscriberData['phones'] as $phone) {
                        if (!is_null($phone) && (preg_match("/[\+]?\d+[\-\(\)]*/", $phone) === 1)) {
                            $phones[] = $phone;
                        }
                    }
                    $subscriber->setPhones($phones);
                    $em->persist($subscriber);
                }
                foreach ($subscriberData['agreements'] as $agreementData) {
                    if (!$agreement = $this->agreementHandler->getAgreement($agreementData['id'])) {
                        if (!$agreement = $this->agreementHandler->getAgreementByNumber($agreementData['number'])) {
                            $agreement = new Agreement();
                            $agreement->setId($agreementData['id']);
                            $agreement->setNumber($agreementData['number']);
                            $agreement->setSubscriber($subscriber);
                        }

                        if (!$company = $this->companyHandler->getCompany($agreementData['company']['id'])) {
                            if (!$company = $this->companyHandler->getCompanyByTitle($agreementData['company']['title'])) {
                                $company = new Company();
                                $company->setTitle($agreementData['company']['title']);
                                $em->persist($company);
                            }
                        }
                        $agreement->setCompany($company);
                        $em->persist($agreement);
                    }
                    foreach ($agreementData['counters'] as $counterData) {
                        if (!$counter = $this->counterHandler->getCounter($counterData['id'])) {
                            if (!$counter = $this->counterHandler->getCounterByNumber($counterData['title'])) {
                                $counter = new Counter();
                                $counter->setId($counterData['id']);
                                $counter->setTitle($counterData['title']);
                                $counter->setInstallDate(new DateTimeImmutable($counterData['date_install']));
                                $counter->setAgreement($agreement);

                                $em->persist($counter);

                                $installValue = new CounterValue();
                                $installValue->setCounter($counter);
                                $installValue->setDate(new DateTimeImmutable($counterData['value_install']['date']));
                                $installValue->setValue(new CounterValueType1([$counterData['value_install']['value'][0]]));
                                $em->persist($installValue);
                                $counter->setInitialValue($installValue);
                            }

                            $counter->setTemporaryAddress($counterData['address']);
                            $counter->setTemporaryCounterType($counterData['counter_type_title']);
                            $counter->setTemporarySubstation($counterData['substation']);
                            $counter->setTemporaryTransformer($counterData['transformer']);
                            $counter->setTemporaryFeeder($counterData['feeder']);
                            $counter->setSideSeal($counterData['seals'][0]);
                            $em->persist($counter);
                        }

                        if (!$currentValue = $this->counterValueHandler
                            ->getCounterValueByCounterAndDate($counterData['id'], new DateTimeImmutable($counterData['value_current']['date']))) {
                            $currentValue = new CounterValue();
                            $currentValue->setCounter($counter);
                            $currentValue->setDate(
                                new DateTimeImmutable($counterData['value_current']['date'] ?? $vedomostPeriod)
                            );
                            $currentValue->setValue(new CounterValueType1([$counterData['value_current']['value'][0]]));
                            $em->persist($currentValue);
                        }
                        if (!$task = $this->getTaskByCounterAndPeriod($counter->getId(), new DateTimeImmutable($vedomostPeriod))) {
                            $task = new Task();
                            $task->setCounter($counter);
                            $task->setAgreement($agreement);
                            $task->setCompany($company);
                            $task->setSubscriber($subscriber);
                            $task->setLastCounterValue($currentValue);
                        }
                        $task->setPeriod(new DateTimeImmutable($vedomostPeriod));
                        $task->setStatus(0);
                        $task->setTaskType(0);
                        $task->setDeadline((new DateTimeImmutable($vedomostPeriod))->modify("first day of next month")->modify("+ 14 day"));
                        $em->persist($task);
                    }
                }
                if (($key%15 === 0) || ($key + 1 >= $vedomostDataLength)) {
                    $em->flush();
                    $em->getConnection()->commit();
                }
            } catch (Exception $exception) {
                $em->rollback();
                throw $exception;
            }
            $em->flush();
            $em->commit();
            $em->clear();
        }
    }
}