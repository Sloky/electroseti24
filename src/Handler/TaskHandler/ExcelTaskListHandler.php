<?php

namespace App\Handler\TaskHandler;

use App\Entity\Main\Agreement;
use App\Entity\Main\Company;
use App\Entity\Main\Counter;
use App\Entity\Main\CounterValue;
use App\Entity\Main\Subscriber;
use App\Entity\Main\Task;
use App\Entity\Main\User;
use App\Form\TaskListUploadFormType;
use App\Model\CounterValueType1;
use App\Model\TaskListUploadForm;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Exception;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ExcelTaskListHandler extends AbstractTaskHandler
{
    /**
     * Load task list file (new version)
     * @param $formFileData
     * @return array
     * @throws Exception
     */
    public function loadTaskList($formFileData): ?array
    {
        $form = $this->formFactory->create(TaskListUploadFormType::class, null, ['csrf_protection' => false]);
        $form->submit($formFileData);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var TaskListUploadForm $formData */
            $formData = $form->getData();
            $file = $formData->getFile();
            $fileName = $file->getPathname();
            return $this->parseBypassList($fileName);
        }
        $errors[] = 'Произошла ошибка при загрузке обходного листа';
        foreach ($form->getErrors(true) as $formErrorIterator) {
            $errors[] = $formErrorIterator->getMessage();
        }
        return $errors;
    }

    /**
     * @param $fileName
     * @return array
     * @throws Exception
     */
    public function parseBypassList($fileName): array
    {
        $rowErrors = [];

        $em = $this->em;
        $em->getConnection()->getConfiguration()->setSQLLogger(null);

        $oSpreadsheet = IOFactory::load($fileName);
        $activeSheet = $oSpreadsheet->getActiveSheet();
        $oCells = $oSpreadsheet->getActiveSheet()->getCellCollection();

        $vedomostPeriod = '01.'.$oCells->get('A2')->getFormattedValue();
        $vedomostPeriodDateTime = new DateTimeImmutable($vedomostPeriod);
        $deadline = $vedomostPeriodDateTime->modify('+1 month')->modify('+14 day');

        $highestRow = $oCells->getHighestRow();
        $columns = [];
        $columnsWithoutFormattedValue = ['B'];

        $columnIterator = $activeSheet->getColumnIterator();
        foreach ($columnIterator as $column) {
            $cellIterator = $column->getCellIterator();
            if (array_search($column->getColumnIndex(), $columnsWithoutFormattedValue) !== false) {
                foreach ($cellIterator as $cell) {
                    $columns[$cell->getColumn()][] = $cell->getValue();
                }
            } else {
                foreach ($cellIterator as $cell) {
                    $columns[$cell->getColumn()][] = trim($cell->getFormattedValue());
                }
            }
        }

        //Получаем все компании
        $companiesTitles = array_slice($columns['G'], 5, null);
        $uniqueCompanies = array_filter(array_unique($companiesTitles), function ($value) {return (bool)$value;});
        $companyEntities = $this->companyHandler->companyRepository->findByCompanyTitle($uniqueCompanies);
        $companyTitleCollection = $this->getCompaniesTitleCollection($companyEntities);
        $companies = [];

        foreach ($uniqueCompanies as $companyTitle) {
            if (!isset($companyTitleCollection[$companyTitle])) {
                $company = new Company();
                $company->setTitle($companyTitle);
                $em->persist($company);
                $companies[$companyTitle] = $company;
            } else {
                $companies[$companyTitle] = $companyTitleCollection[$companyTitle];
            }
        }

        //Получаем всех пользователей (контролёров)
        $executorsTitles = array_slice($columns['Z'], 5, null);
//        $executorWithCompany = array_filter(array_combine($executorsTitles, $companiesTitles), function ($value, $index) {return (bool)$index;}, ARRAY_FILTER_USE_BOTH );
        $executorWithCompany = $this->getExecutorWithCompanyArray($executorsTitles, $companiesTitles);
        $executorTitleCollection = $this->getUsersCollectionByFullName($executorWithCompany);
        $executors = [];

        $userCounter = 0;
        foreach ($executorWithCompany as $userFullName => $companiesTitles) {
            if (!isset($executorTitleCollection[$userFullName])) {
                $controllerGroup = $this->userGroupHandler->getUserGroupByName('Controllers');
                $fullNameArr = explode(' ', $userFullName);
                $user = new User();
                $user->setCompanies(new ArrayCollection($this->getCompaniesArrayByTitles($companiesTitles, $companies)));
                $user->setGroups(new ArrayCollection([$controllerGroup]));
                $user->setSurname($fullNameArr[0]);
                $user->setName($fullNameArr[1] ?? '');
                $user->setPatronymic($fullNameArr[2] ?? '');
                $user->setUsername('controller'.($this->userHandler->getControllersCount() + 1 + $userCounter++));
                $user->setPassword('');
                $em->persist($user);
                $executors[$userFullName] = $user;
            } else {
                $executors[$userFullName] = $executorTitleCollection[$userFullName];
            }
        }

        $em->flush();
        $em->clear();

        //Получаем существующие договора
        $agreementsTitles = array_slice($columns['B'], 5, null);
        $uniqueAgreementTitles = array_filter(array_unique($agreementsTitles), function ($value) {return (bool)$value;});
        $agreementEntities = $this->agreementHandler->agreementRepository->findByAgreementTitle($uniqueAgreementTitles);
        //Коллекция наименований абонентов с номером договора в качестве ключа
//        $subscribersTitleCollection = $this->getSubscriberTitleCollection($agreementEntities);
        $subscribersTitleCollection = [];

        //Многомерная коллекция наименований УУ с номером договора в качестве ключа
        $countersCollection = [];

        //Прогоняем список на наличие ошибок. Заполняем по необходимости массив $rowErrors
        foreach ($agreementsTitles as $key => $agreementTitle) {
            $agreementTitle = strval($agreementTitle);
            $rowIndex = $key + 5;

            //Проверка на наличие одинаковых договоров у разных абонентов
            if (isset($subscribersTitleCollection[$agreementTitle])) {
                if ($subscribersTitleCollection[$agreementTitle] !== trim($columns['D'][$rowIndex])) {
                    $rowErrors[] = "Ошибка в строке ".($rowIndex+1).". Несколько разных абонентов с одинаковым лицевым счётом/платежным кодом";
                    $subscribersTitleCollection[$agreementTitle] = "";
                }
            } else {
                $subscribersTitleCollection[$agreementTitle] = trim($columns['D'][$rowIndex]);
            }

            //Проверка на наличие одинаковых УУ в одном договоре (иначе ломается уникальность договор+УУ)
            if (isset($countersCollection[$agreementTitle])) {
                if (array_search($columns['H'][$rowIndex], $countersCollection[$agreementTitle]) !== false) {
                    $rowErrors[] = "Ошибка в строке ".($rowIndex+1).". Несколько одинаковых узлов учета с одинаковым лицевым счётом/платежным кодом";
                    $subscribersTitleCollection[$agreementTitle] = "";
                }
            } else {
                $countersCollection[$agreementTitle][] = $columns['H'][$rowIndex];
            }
        }

        $batchSize = 400;
        $iterationSize = count($agreementsTitles);

        /** @var \App\Entity\Main\Agreement[] $persistentAgreements */
        $persistentAgreements = [];

        foreach ($agreementsTitles as $key => $agreementTitle) {
            $agreementTitle = strval($agreementTitle);
            $rowIndex = $key + 5;

            $subscriberTitle = $subscribersTitleCollection[$agreementTitle];
            if ($subscriberTitle === "") {
                continue;
            }
            $subscriberType = $this->getSubscriberType($columns['E'][$rowIndex]);
            $subscriberPhones = (bool)$columns['F'][$rowIndex] ? explode(',', $columns['F'][$rowIndex]) : [];
            $counterTitle = $columns['H'][$rowIndex];
            $lastCounterValueString = (bool)$columns['K'][$rowIndex] ? $columns['K'][$rowIndex] : 0;
            $lastCounterValueDate = (bool)$columns['N'][$rowIndex] ? new DateTimeImmutable($columns['N'][$rowIndex]) : $vedomostPeriodDateTime;

            try {
                if (!(bool)$agreementTitle) {
                    $rowErrors[] = "Ошибка в строке ".($rowIndex+1).". Не указан номер договора";
                    continue;
                }
                $newCompany = isset($companies[$columns['G'][$rowIndex]])
                    ? $em->getRepository(Company::class)->find($companies[$columns['G'][$rowIndex]]->getId())
                    : null;

                $newExecutor= isset($executors[$columns['Z'][$rowIndex]])
                    ? $em->getRepository(User::class)->find($executors[$columns['Z'][$rowIndex]]->getId())
                    : null;

                $subscriber = null;

                if (!$agreement = $this->agreementHandler->agreementRepository->findOneBy(['number' => $agreementTitle])) {
                    if (!array_key_exists($agreementTitle, $persistentAgreements)) {
                        //Создаем абонента
                        $subscriber = new Subscriber();
                        $subscriber->setTitle($subscriberTitle);
                        $subscriber->setSubscriberType($subscriberType);
                        $subscriber->setPhones($subscriberPhones);

                        $em->persist($subscriber);

                        //Создаем договор
                        $agreement = new Agreement();
                        $agreement->setSubscriber($subscriber);
                        $agreement->setCompany($newCompany);
                        $agreement->setNumber($agreementTitle);

                        $em->persist($agreement);
                        $persistentAgreements[$agreementTitle] = $agreement;
                    } else {
                        $agreement = $persistentAgreements[$agreementTitle];
                        $subscriber = $agreement->getSubscriber();
                    }

                    //Создаем счётчик
                    $counter = new Counter();
                    $counter->setAgreement($agreement);

                } else {
                    $subscriber = $agreement->getSubscriber();
                    $subscriber->setTitle($subscriberTitle);
                    $subscriber->setSubscriberType($subscriberType);
                    $subscriber->setPhones($subscriberPhones);

                    if (!$counter = $this->getCounterByTitleFromCounters($counterTitle, $agreement->getCounters()->getValues())) {
                        //Создаем счётчик
                        $counter = new Counter();
                        $counter->setAgreement($agreement);
                    }
                }

                $counter->setTitle($counterTitle);
                $counter->setInstallDate(new DateTimeImmutable($columns['AH'][$rowIndex] ?? 'now'));
                $counter->setTemporaryAddress(empty($columns['C'][$rowIndex]) ? 'Нет данных' : $columns['C'][$rowIndex]);
                $counter->setTemporaryCounterType(empty($columns['I'][$rowIndex]) ? 'Нет данных' : $columns['I'][$rowIndex]);
                $counter->setTemporarySubstation(empty($columns['S'][$rowIndex]) ? 'Нет данных' : $columns['S'][$rowIndex]);
                $counter->setTemporaryTransformer(empty($columns['U'][$rowIndex]) ? 'Нет данных' : $columns['U'][$rowIndex]);
                $counter->setTemporaryFeeder(empty($columns['T'][$rowIndex]) ? 'Нет данных' : $columns['T'][$rowIndex]);
                $counter->setSideSeal(empty($columns['P'][$rowIndex]) ? 'Нет данных' : $columns['P'][$rowIndex]);
                $counter->setAntiMagneticSeal(empty($columns['R'][$rowIndex]) ? 'Нет данных' : $columns['R'][$rowIndex]);
                $counter->setTerminalSeal(empty($columns['Q'][$rowIndex]) ? 'Нет данных' : $columns['Q'][$rowIndex]);
                $em->persist($counter);

                //Поиск и создание значение показаний счётчика

                if (!$lastCounterValue = $this->counterValueHandler
                    ->getCounterValueByCounterAndDate($counter->getId(), $lastCounterValueDate)) {
//                    ->getCounterValueByCounterAndAgreement($counter->getId(), $agreement->getId(), $lastCounterValueDate)) {
                    $lastCounterValue = new CounterValue();
                    $lastCounterValue->setCounter($counter);
                    $lastCounterValue->setDate($lastCounterValueDate);
                }
                $lastCounterValue->setValue(new CounterValueType1([$lastCounterValueString]));
                $em->persist($lastCounterValue);

                if (!$task = $this->getTaskByCounterPeriodAndStatus($counter->getId(), new DateTimeImmutable($vedomostPeriod))) {
                    $task = new Task();
                    $task->setCounter($counter);
                    $task->setAgreement($agreement);
                    $task->setCompany($newCompany);

                }
                $task->setSubscriber($subscriber);
                $task->setLastCounterValue($lastCounterValue);
                $task->setSubscriber($subscriber);
                $task->setPeriod(new DateTimeImmutable($vedomostPeriod));
                $task->setStatus(0);
                $task->setTaskType(0);
                $task->setExecutor($newExecutor);
                $task->setDeadline($deadline);
                $em->persist($task);

                if ($key % $batchSize === 0 || $key === ($iterationSize - 1)) {
                    $em->flush();
                    $em->clear();
                }

            } catch(Exception $exception) {
                $rowErrorStart = $rowIndex + 1 - $batchSize;
                $rowErrorEnd = $rowIndex + 1;
                $rowErrors[] = "Ошибка! Не все задачи были загружены. Проверьте данные на наличие ошибок. $rowErrorStart - $rowErrorEnd";
            }
        }

        if (!empty($rowErrors)) {
            $rowErrors[] = "Не все задачи были загружены. Проверьте данные на наличие ошибок.";
        }
        return $rowErrors;
    }

    /**
     * Generate Task data excel file
     * @param $id
     * @return Response
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function getExcelCompletedTaskData($id): Response
    {
        if (!$task = $this->getTask($id)) {
            throw new HttpException(Response::HTTP_BAD_REQUEST, "There is no Task with ID=$id");
        }
        $period = $task->getPeriod();
        $counterNumber = $task->getCounter()->getTitle();

        $counterPhysicalStatus = $task->getCounterPhysicalStatus() === 0 ? 'Исправен/Доступен' : 'Неисправен/Недоступен';

        $agreementNumber = $task->getAgreement()->getNumber();

        $subscriberType = $this->my_mb_ucfirst(Subscriber::$subscriber_type_value[$task->getSubscriber()->getSubscriberType()]);
        $subscriberPhones = implode(',', $task->getSubscriber()->getPhones());

        $counterValue = $task->getCurrentCounterValue()
            ? $task->getCurrentCounterValue()->getValue()->getValues()[0]
            : '';
        $executedDate = $task->getExecutedDate()->format('Y-m-d H:i:s');
        $oldAddress = $task->getCounter()->getTemporaryAddress();

        /** @var User $executor */
        $executor = $task->getExecutor();

        $executorFullName = ($executor->getSurname() ? $executor->getSurname() : '')
            .' '.($executor->getName() ? $executor->getName() : '')
            .' '.($executor->getPatronymic() ? $executor->getPatronymic() : '')
        ;

        /** @var User $operator */
        $operator = $task->getOperator();

        $operatorFullName = ($operator->getSurname() ? $operator->getSurname() : '')
            .' '.($operator->getName() ? $operator->getName() : '')
            .' '.($operator->getPatronymic() ? $operator->getPatronymic() : '')
        ;

        $atPresentDate = (new DateTimeImmutable())->format('Y-m-d');

        $sOutFile = $period->format('Y-m').'_'."$counterNumber.xlsx";

        $fileName = $this->parameterBag->get('tasks_excel_template_path');
        $oSpreadsheet_Out = IOFactory::load($fileName);

        $oSpreadsheet_Out->getProperties()->setCreator('User')
            ->setLastModifiedBy($task->getOperator()->getUsername())
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Task data ')
        ;

        $oSpreadsheet_Out->setActiveSheetIndex(0)
            ->setCellValue("A2", $period->format('m.Y'));

        $activeSheet = $oSpreadsheet_Out->getActiveSheet();

        $columns = array(
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
            'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI',
            'AJ', 'AK', 'AL', 'AM'
        );

        foreach ($columns as $column) {
            $activeSheet->getColumnDimension($column)->setAutoSize(true);
        }

        $activeSheet
            ->setCellValue('A6', 1)
            ->setCellValue('B6', '="'.$agreementNumber.'"')
            ->setCellValue('C6', $oldAddress)
            ->setCellValue('D6', $task->getSubscriber()->getTitle())
            ->setCellValue('E6', $subscriberType)
            ->setCellValue('F6', $subscriberPhones)
            ->setCellValue('G6', $task->getCompany()->getTitle())
            ->setCellValue('H6', '="'.$counterNumber.'"')
            ->setCellValue('I6', $task->getCounter()->getTemporaryCounterType() ?? '')
            ->setCellValue('J6', $counterPhysicalStatus)
            ->setCellValue('K6', implode(',', $task->getLastCounterValue()->getValue()->getValues()))
            ->setCellValue('L6', $task->getLastCounterValue()
                ? $task->getLastCounterValue()->getDate()->format('Y-m-d H:i:s')
                : ''
            )
            ->setCellValue('M6', $counterValue)
            ->setCellValue('N6', $executedDate)
            ->setCellValue('O6', $task->getConsumption() ?? '')
            ->setCellValue('P6', $task->getCounter()->getTerminalSeal() ?? '')
            ->setCellValue('Q6', $task->getCounter()->getAntiMagneticSeal() ?? '')
            ->setCellValue('R6', $task->getCounter()->getSideSeal() ?? '')
            ->setCellValue('S6', $task->getCounter()->getTemporarySubstation() ?? '')
            ->setCellValue('T6', $task->getCounter()->getTemporaryFeeder() ?? '')
            ->setCellValue('U6', $task->getCounter()->getTemporaryTransformer() ?? '')
            ->setCellValue('V6', $task->getCounter()->getElectricLine() ?? '')
            ->setCellValue('W6', $task->getCounter()->getElectricPoleNumber() ?? '')
            ->setCellValue('X6', implode('/', $task->getCoordinates() ?? []))
            ->setCellValue('Y6', implode('/', $task->getNewCoordinates() ?? []))
            ->setCellValue('Z6', $executorFullName)
            ->setCellValue('AA6', $operatorFullName)
            ->setCellValue('AB6', $task->getAcceptanceDate())
            ->setCellValue('AC6', $task->getControllerComment())
            ->setCellValue('AD6', $task->getOperatorComment())
            ->setCellValue('AE6', $task->getDeadline())
            ->setCellValue('AF6', $task->getChangedRoomsCount())
            ->setCellValue('AG6', $task->getChangedLodgersCount())
            ->setCellValue('AH6', $task->getCounter()->getInstallDate()
                ? $task->getCounter()->getInstallDate()->format('Y-m-d H:i:s')
                : ''
            )
            ->setCellValue('AJ6', $task->getSubscriber()->getId())
            ->setCellValue('Ak6', $task->getAgreement()->getId())
            ->setCellValue('AL6', $task->getCompany()->getId())
            ->setCellValue('AM6', $task->getCounter()->getId());

        $activeSheet->getStyle('B6:C6')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_TEXT);
        $activeSheet->getStyle('K6')->getNumberFormat()->setFormatCode('0.0000000');
        $activeSheet->getStyle('M6')->getNumberFormat()->setFormatCode('0.0000000');
        $activeSheet->getStyle('O6')->getNumberFormat()->setFormatCode('0.0000000');

        $oWriter = IOFactory::createWriter($oSpreadsheet_Out, 'Xlsx');
        $oWriter->save($sOutFile);

        $response = new Response(
            file_get_contents($sOutFile),
            Response::HTTP_OK,
            ['Content-Type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'Content-Disposition' => 'attachment; filename="' . basename($sOutFile) . '"',
                'Content-Length' => filesize($sOutFile)]);

        unlink($sOutFile);

        return $response;
    }

    /**
     * Generate Tasks data excel file
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws Exception
     */
    public function getExcelCompletedTasksData(
        User $user,
        array $filter = [],
        string $search = null,
        string $searchField = null
    ): Response
    {
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', '600');

        if ($this->userHandler->isUserAdmin($user)) {
            $tasksQuery = $this->taskRepository->getTasksWithSearchQueryBuilder(
                2,
                $search,
                $searchField,
                $filter
            );
        } else {
            $tasksQuery = $this->taskRepository->getTasksByCompaniesWithSearchQueryBuilder(
                2,
                $user->getCompanies()->getValues(),
                $search,
                $searchField,
                $filter
            );
        }

        /** @var $tasks \App\Entity\Main\Task[] */
        $tasks = $tasksQuery->getQuery()->execute();

        if (empty($tasks)) {
            throw new HttpException(Response::HTTP_BAD_REQUEST, "No Tasks for Excel sheet formation");
        }


        /*if (empty($tasks = $this->taskRepository->getTasksWithSearchQueryBuilder(
            2,
            $search,
            $searchField,
            $filter
        )->getQuery()->execute())) {
            throw new HttpException(Response::HTTP_BAD_REQUEST, "No Tasks for Excel sheet formation");
        }*/

        $period = $tasks[0]->getPeriod();

        $sOutFile = $period->format('Y-m').".xlsx";

        $fileName = $this->parameterBag->get('tasks_excel_template_path');
        $oSpreadsheet_Out = IOFactory::load($fileName);

        $oSpreadsheet_Out->getProperties()->setCreator('TestUser')
            ->setLastModifiedBy('Tasks data')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Task data ')
        ;

        $oSpreadsheet_Out->setActiveSheetIndex(0)
            ->setCellValue("A2", $period->format('m.Y'));

        $activeSheet = $oSpreadsheet_Out->getActiveSheet();

        $columns = array(
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
            'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI',
            'AJ', 'AK', 'AL', 'AM'
        );

        foreach ($columns as $column) {
            $activeSheet->getColumnDimension($column)->setAutoSize(true);
        }

        $styleArray = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    'color' => ['argb' => '000000'],
                ],
            ],
        ];

        $tasksCount = count($tasks);

//        $activeSheet->getStyle("A6:I".($tasksCount+8))->applyFromArray($styleArray);

        /**
         * @var string $key
         * @var Task $task
         */
        foreach ($tasks as $key => $task) {
            $i = intval($key);
            $counterNumber = $task->getCounter()->getTitle();
            $agreementNumber = $task->getAgreement()->getNumber();
            $counterValue = $task->getCurrentCounterValue()
                ? $task->getCurrentCounterValue()->getValue()->getValues()[0]
                : '';
            $executedDate = $task->getExecutedDate()->format('Y-m-d H:i:s');
            $oldAddress = $task->getCounter()->getTemporaryAddress();
            $newAddress = $task->getCounter()->getAddress();

            $subscriberType = $this->my_mb_ucfirst(Subscriber::$subscriber_type_value[$task->getSubscriber()->getSubscriberType()]);
            $subscriberPhones = implode(',', $task->getSubscriber()->getPhones());

            $counterPhysicalStatus = $task->getCounterPhysicalStatus() === 0 ? 'Исправен/Доступен' : 'Неисправен/Недоступен';

            /** @var \App\Entity\Main\User $executor */
            $executor = $task->getExecutor();
            $executorFullName = "Неизвестный исполнитель";
            if ($executor) {
                $executorFullName = ($executor->getSurname() ? $executor->getSurname() : '')
                    .' '.($executor->getName() ? $executor->getName() : '')
                    .' '.($executor->getPatronymic() ? $executor->getPatronymic() : '')
                ;
            }

            /** @var \App\Entity\Main\User $operator */
            $operator = $task->getOperator();

            $operatorFullName = ($operator->getSurname() ? $operator->getSurname() : '')
                .' '.($operator->getName() ? $operator->getName() : '')
                .' '.($operator->getPatronymic() ? $operator->getPatronymic() : '')
            ;

            $activeSheet
                ->setCellValue('A'.($i+6), $i+1)
                ->setCellValue('B'.($i+6), '="'.$agreementNumber.'"')
                ->setCellValue('C'.($i+6), $oldAddress)
                ->setCellValue('D'.($i+6), $task->getSubscriber()->getTitle())
                ->setCellValue('E'.($i+6), $subscriberType)
                ->setCellValue('F'.($i+6), $subscriberPhones)
                ->setCellValue('G'.($i+6), $task->getCompany()->getTitle())
                ->setCellValue('H'.($i+6), '="'.$counterNumber.'"')
                ->setCellValue('I'.($i+6), $task->getCounter()->getTemporaryCounterType() ?? '')
                ->setCellValue('J'.($i+6), $counterPhysicalStatus)
                ->setCellValue('K'.($i+6), implode(',', $task->getLastCounterValue()->getValue()->getValues()))
                ->setCellValue('L'.($i+6), $task->getLastCounterValue()
                    ? $task->getLastCounterValue()->getDate()->format('Y-m-d H:i:s')
                    : ''
                )
                ->setCellValue('M'.($i+6), $counterValue)
                ->setCellValue('N'.($i+6), $executedDate)
                ->setCellValue('O'.($i+6), $task->getConsumption() ?? '')
                ->setCellValue('P'.($i+6), $task->getCounter()->getTerminalSeal() ?? '')
                ->setCellValue('Q'.($i+6), $task->getCounter()->getAntiMagneticSeal() ?? '')
                ->setCellValue('R'.($i+6), $task->getCounter()->getSideSeal() ?? '')
                ->setCellValue('S'.($i+6), $task->getCounter()->getTemporarySubstation() ?? '')
                ->setCellValue('T'.($i+6), $task->getCounter()->getTemporaryFeeder() ?? '')
                ->setCellValue('U'.($i+6), $task->getCounter()->getTemporaryTransformer() ?? '')
                ->setCellValue('V'.($i+6), $task->getCounter()->getElectricLine() ?? '')
                ->setCellValue('W'.($i+6), $task->getCounter()->getElectricPoleNumber() ?? '')
                ->setCellValue('X'.($i+6), implode('/', $task->getCoordinates() ?? []))
                ->setCellValue('Y'.($i+6), implode('/', $task->getNewCoordinates() ?? []))
                ->setCellValue('Z'.($i+6), $executorFullName)
                ->setCellValue('AA'.($i+6), $operatorFullName)
                ->setCellValue('AB'.($i+6), $task->getAcceptanceDate())
                ->setCellValue('AC'.($i+6), $task->getControllerComment())
                ->setCellValue('AD'.($i+6), $task->getOperatorComment())
                ->setCellValue('AE'.($i+6), $task->getDeadline())
                ->setCellValue('AF'.($i+6), $task->getChangedRoomsCount())
                ->setCellValue('AG'.($i+6), $task->getChangedLodgersCount())
                ->setCellValue('AH'.($i+6), $task->getCounter()->getInstallDate()
                    ? $task->getCounter()->getInstallDate()->format('Y-m-d H:i:s')
                    : ''
                )
//            ->setCellValue('AI6', implode(',', $task->getCounter()->getInitialValue()->getValue()->getValues()))
                ->setCellValue('AJ'.($i+6), $task->getSubscriber()->getId())
                ->setCellValue('Ak'.($i+6), $task->getAgreement()->getId())
                ->setCellValue('AL'.($i+6), $task->getCompany()->getId())
                ->setCellValue('AM'.($i+6), $task->getCounter()->getId());

            $activeSheet->getStyle('B'.($i+6).':C'.($i+6))->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_TEXT);
            $activeSheet->getStyle('K'.($i+6))->getNumberFormat()->setFormatCode('0.0000000');
            $activeSheet->getStyle('M'.($i+6))->getNumberFormat()->setFormatCode('0.0000000');
            $activeSheet->getStyle('O'.($i+6))->getNumberFormat()->setFormatCode('0.0000000');

            $activeSheet->getStyle('F'.($i+6))->getAlignment()->setHorizontal('right');
        }

        $oWriter = IOFactory::createWriter($oSpreadsheet_Out, 'Xlsx');
        $oWriter->save($sOutFile);

        $response = new Response(
            file_get_contents($sOutFile),
            Response::HTTP_OK,
            ['Content-Type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'Content-Disposition' => 'attachment; filename="' . basename($sOutFile) . '"',
                'Content-Length' => filesize($sOutFile)]);

        unlink($sOutFile);

        return $response;
    }

    /**
     * Get the task
     * @param $taskId
     * @return Task|null
     */
    private function getTask($taskId): ?Task
    {
        return $this->taskRepository->find($taskId);
    }

    /**
     * A collection of Subscriber titles where agreement number is the key
     * @param Agreement[] $agreements
     * @return array
     */
    private function getSubscriberTitleCollection(array $agreements): array
    {
        $agreementCollection = [];
        foreach ($agreements as $agreement) {
            $agreementCollection[$agreement->getNumber()] = $agreement->getSubscriber()->getTitle();
        }
        return $agreementCollection;
    }

    /**
     * A collection of Subscribes where subscriber title is the key
     * @param Agreement[] $agreements
     * @return \App\Entity\Main\Subscriber[]
     */
    private function getSubscribersTitleCollectionFromAgreements(array $agreements): array
    {
        $subscriberCollection = [];
        foreach ($agreements as $agreement) {
            $subscriber = $agreement->getSubscriber();
            $subscriberCollection[$subscriber->getTitle()] = $subscriber;
        }
        return $subscriberCollection;
    }

    /**
     * Get Counter by his title
     * @param string $counterTitle
     * @param Counter[] $counters
     * @return Counter|null
     */
    private function getCounterByTitleFromCounters(string $counterTitle, array $counters): ?Counter
    {
        $desiredCounter = null;
        foreach ($counters as $counter) {
            if ($counter->getTitle() == $counterTitle) {
                $desiredCounter = $counter;
                break;
            }
        }
        return $desiredCounter;
    }

    /**
     * User`s companies (User full name is a key, company array is a value)
     * @param array $executorsTitles
     * @param array $companiesTitles
     * @return array
     */
    private function getExecutorWithCompanyArray(array $executorsTitles, array $companiesTitles): array
    {
        $resultArray = [];
        foreach ($executorsTitles as $key => $title) {
            if (!!$title) {
                if (isset($resultArray[$title])) {
                    if (array_search($companiesTitles[$key], $resultArray[$title]) === false) {
                        $resultArray[$title][] = $companiesTitles[$key];
                    }
                } else {
                    $resultArray[$title][] = $companiesTitles[$key];
                }
            }
        }
        return $resultArray;
    }

    /**
     * @param array $companiesTitles
     * @param Company[] $companies
     * @return \App\Entity\Main\Company[]
     */
    private function getCompaniesArrayByTitles(array $companiesTitles, array $companies): array
    {
        $resultArray = [];
        foreach ($companiesTitles as $title) {
            if (array_key_exists($title, $companies)) {
                $resultArray[] = $companies[$title];
            }
        }
        return $resultArray;
    }

    /**
     * A collection of Companies where company name is the key
     * @param \App\Entity\Main\Company[] $companies
     * @return array
     */
    private function getCompaniesTitleCollection(array $companies): array
    {
        $companyCollection = [];
        foreach ($companies as $company) {
            $companyCollection[$company->getTitle()] = $company;
        }
        return $companyCollection;
    }

    /**
     * @param $str
     * @return string
     */
    private function my_mb_ucfirst($str): string
    {
        $fc = mb_strtoupper(mb_substr($str, 0, 1));
        return $fc.mb_substr($str, 1);
    }

    /**
     * A collection of Users where full name is the key
     * @param string[] $companyByUserName
     * @return array
     */
    private function getUsersCollectionByFullName(array $companyByUserName): array
    {
        $usersCollection = [];
        foreach ($companyByUserName as $userFullName => $companyTitle) {
            if ($userFullName) {
//               $user = $this->userHandler->getUserByFullNameAndCompanyTitle($userFullName, $companyTitle);
               $user = $this->userHandler->getUserByFullName($userFullName);
               $user && ($usersCollection[$userFullName] = $user);
            }
        }
        return $usersCollection;
    }

    /**
     * Get type of the subscriber
     * @param string $subscriberTypeString
     * @return int
     */
    private function getSubscriberType(string $subscriberTypeString): int
    {
        switch ($subscriberTypeString) {
            case 'физ. Лицо':
            case 'физ. лицо':
            case 'Физическое лицо':
            case 'fl': return 0;
            case 'Юридическое лицо': return 1;
            case 'bu':
            case 'Цент.ап':
            case 'Цент.ап.':
            case 'Центральный аппарат':
            case 'Цент. Ап.' : return 2;
            case 'Электрон' : return 3;
            default: return 1;
        }
    }

    /**
     * Get Task by Counter and period
     * @param $counterId
     * @param DateTimeInterface $period
     * @param int $status
     * @return Task|null
     */
    public function getTaskByCounterPeriodAndStatus($counterId, DateTimeInterface $period, int $status = 0): ?Task
    {
        return $this->taskRepository->findOneBy([
            'counter' => $counterId,
            'period' => $period,
            'status' => $status
        ]);
    }

    /**
     * Make new Counter
     * @param $rowIndex
     * @param \App\Entity\Main\Agreement $agreement
     * @param array $columns
     * @return \App\Entity\Main\Counter
     * @throws Exception
     */
    private function makeNewCounter($rowIndex, Agreement $agreement, array $columns): Counter
    {
        $counter = new Counter();
        $counter->setTitle($columns['H'][$rowIndex]);
        $counter->setInstallDate(new DateTimeImmutable($columns['AH'][$rowIndex] ?? 'now'));
        $counter->setAgreement($agreement);
        $counter->setTemporaryAddress(empty($columns['C'][$rowIndex]) ? 'Нет данных' : $columns['C'][$rowIndex]);
        $counter->setTemporaryCounterType(empty($columns['I'][$rowIndex]) ? 'Нет данных' : $columns['I'][$rowIndex]);
        $counter->setTemporarySubstation(empty($columns['S'][$rowIndex]) ? 'Нет данных' : $columns['S'][$rowIndex]);
        $counter->setTemporaryTransformer(empty($columns['U'][$rowIndex]) ? 'Нет данных' : $columns['U'][$rowIndex]);
        $counter->setTemporaryFeeder(empty($columns['T'][$rowIndex]) ? 'Нет данных' : $columns['T'][$rowIndex]);
        $counter->setSideSeal(empty($columns['P'][$rowIndex]) ? 'Нет данных' : $columns['P'][$rowIndex]);
        $counter->setAntiMagneticSeal(empty($columns['R'][$rowIndex]) ? 'Нет данных' : $columns['R'][$rowIndex]);
        $counter->setTerminalSeal(empty($columns['Q'][$rowIndex]) ? 'Нет данных' : $columns['Q'][$rowIndex]);
        return $counter;
    }

    /**
     * @param Counter $counter
     * @param $rowIndex
     * @param Agreement $agreement
     * @param array $columns
     * @return Counter
     * @throws Exception
     */
    private function editCounter(Counter $counter, $rowIndex, Agreement $agreement, array $columns): Counter
    {
//        $counter->setTitle($columns['H'][$rowIndex]);
        $counter->setInstallDate(new DateTimeImmutable($columns['AH'][$rowIndex] ?? 'now'));
        $counter->setAgreement($agreement);
        $counter->setTemporaryAddress(empty($columns['C'][$rowIndex]) ? 'Нет данных' : $columns['C'][$rowIndex]);
        $counter->setTemporaryCounterType(empty($columns['I'][$rowIndex]) ? 'Нет данных' : $columns['I'][$rowIndex]);
        $counter->setTemporarySubstation(empty($columns['S'][$rowIndex]) ? 'Нет данных' : $columns['S'][$rowIndex]);
        $counter->setTemporaryTransformer(empty($columns['U'][$rowIndex]) ? 'Нет данных' : $columns['U'][$rowIndex]);
        $counter->setTemporaryFeeder(empty($columns['T'][$rowIndex]) ? 'Нет данных' : $columns['T'][$rowIndex]);
        $counter->setSideSeal(empty($columns['P'][$rowIndex]) ? 'Нет данных' : $columns['P'][$rowIndex]);
        $counter->setAntiMagneticSeal(empty($columns['R'][$rowIndex]) ? 'Нет данных' : $columns['R'][$rowIndex]);
        $counter->setTerminalSeal(empty($columns['Q'][$rowIndex]) ? 'Нет данных' : $columns['Q'][$rowIndex]);
        return $counter;
    }

}