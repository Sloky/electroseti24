<?php

namespace App\Handler\TaskHandler;

use App\Entity\Main\Task;
use App\Entity\Main\User;
use App\Model\Assembler\CompletedTaskAssembler;

class CompletedTaskHandler extends AbstractTaskHandler
{
    /**
     * Get the tasks with status '2'
     * @param int $page
     * @param int|null $maxResult
     * @param string|null $search
     * @param string|null $searchField
     * @param array $filter
     * @return array
     */
    public function getCompletedTasksList(
        int $page = 1,
        ?int $maxResult = null,
        string $search = null,
        string $searchField = null,
        array $filter = []
    ): array
    {
        $tasksList = [];
        /** @var \App\Entity\Main\User $currentUser */
        $currentUser = $this->userHandler->getCurrentUser();
        if ($this->userHandler->isUserAdmin($currentUser)) {
            $tasksQuery = $this->taskRepository->getTasksWithSearchQueryBuilder(
                2,
                $search,
                $searchField,
                $filter
            );
        } else {
            if ($this->userHandler->isUserController($currentUser)) {
                $tasksQuery = $this->taskRepository->getTasksByControllerWithSearchQueryBuilder(
                    2,
                    $currentUser->getId(),
                    $currentUser->getCompanies()->getValues(),
                    $search,
                    $searchField,
                    $filter
                );
            } else {
                $tasksQuery = $this->taskRepository->getTasksByCompaniesWithSearchQueryBuilder(
                    2,
                    $currentUser->getCompanies()->getValues(),
                    $search,
                    $searchField,
                    $filter
                );
            }
        }

        if ($maxResult) {
            $pagination = $this->paginator->paginate($tasksQuery, $page, $maxResult);
            $tasks = $pagination->getItems();
            $taskCount = $pagination->getTotalItemCount();
        } else {
            /** @var \App\Entity\Main\Task[] $tasks */
            $tasks = $tasksQuery->getQuery()->execute();
            $taskCount = count($tasks);
        }


        if (!empty($tasks)) {
            foreach ($tasks as $task) {
                $tasksList[] = CompletedTaskAssembler::writeDTO($task);
            }
        }

        return [
            'tasksList' => $tasksList,
            'taskCount' => $taskCount
        ];
    }
}