<?php

namespace App\Handler\TaskHandler;

use App\Entity\Main\Address;
use App\Entity\Main\Counter;
use App\Entity\Main\CounterValue;
use App\Entity\Main\Task;
use App\Entity\Main\User;
use App\Form\TaskToCheckEditFormType;
use App\Model\Assembler\TaskToCheckAssembler;
use App\Model\CounterValueType1;
use App\Model\CounterValueType2;
use App\Model\CounterValueType3;
use App\Model\DTO\TaskToCheckDTO;
use App\Model\TaskToCheckEditForm;
use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use League\Flysystem\FilesystemException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\User\UserInterface;

class TaskToCheckHandler extends AbstractTaskHandler
{
    /**
     * Get the tasks with status '1'
     * @param int $page
     * @param int|null $maxResult
     * @param string|null $search
     * @param string|null $searchField
     * @param array $filter
     * @return array
     */
    public function getTasksToCheckList(
        int $page = 1,
        ?int $maxResult = null,
        string $search = null,
        string $searchField = null,
        array $filter = []
    ): array
    {
        $tasksList = [];
        /** @var User $currentUser */
        $currentUser = $this->userHandler->getCurrentUser();
        if ($this->userHandler->isUserAdmin($currentUser)) {
            $tasksQuery = $this->taskRepository->getTasksWithSearchQueryBuilder(1, $search, $searchField, $filter);
        } else {
            if ($this->userHandler->isUserController($currentUser)) {
                $tasksQuery = $this->taskRepository->getTasksByControllerWithSearchQueryBuilder(
                    1,
                    $currentUser->getId(),
                    $currentUser->getCompanies()->getValues(),
                    $search,
                    $searchField,
                    $filter
                );
            } else {
                $tasksQuery = $this->taskRepository->getTasksByCompaniesWithSearchQueryBuilder(
                    1,
                    $currentUser->getCompanies()->getValues(),
                    $search,
                    $searchField,
                    $filter
                );
            }
        }

        if ($maxResult) {
            $pagination = $this->paginator->paginate($tasksQuery, $page, $maxResult);
            $tasks = $pagination->getItems();
            $taskCount = $pagination->getTotalItemCount();
        } else {
            /** @var Task[] $tasks */
            $tasks = $tasksQuery->getQuery()->execute();
            $taskCount = count($tasks);
        }


        if (!empty($tasks)) {
            foreach ($tasks as $task) {
                $tasksList[] = TaskToCheckAssembler::writeDTO($task);
            }
        }

        return [
            'tasksList' => $tasksList,
            'taskCount' => $taskCount
        ];
    }

    /**
     * Edit the 'Task to check'
     * @param Request $request
     * @param $taskId
     * @return TaskToCheckDTO
     * @throws Exception
     */
    public function editTaskToCheck(Request $request, $taskId): TaskToCheckDTO
    {
        $formData = new TaskToCheckEditForm();
        $form = $this->formFactory->create(TaskToCheckEditFormType::class, $formData, ['csrf_protection' => false]);
        $submittedData = array_merge($request->request->all(), $request->files->all());
        $form->submit($submittedData);
        $form->handleRequest($request);

        if (!$form->isValid()) {
            throw new HttpException(Response::HTTP_BAD_REQUEST,'Incorrect form data');
        }

        if (!$task = $this->getTask($taskId)) {
            throw new HttpException(Response::HTTP_BAD_REQUEST,"There is no task with ID=$taskId");
        }

        $counter = $task->getCounter();

        $em = $this->em;
        $em->beginTransaction();

        try {
            if (!empty($newCounterValue = $formData->getNewCounterValue()->getValues())) {
                $counterValue = $this->makeCounterValue(
                    $newCounterValue,
                    $task->getCounter(),
                    $this->userHandler->getCurrentUser(),
                    new DateTimeImmutable()
                );
                $task->setConsumption($newCounterValue[0] - $task->getLastCounterValue()->getValue()->getValues()[0]);
                $em->persist($counterValue);
                $task->setCurrentCounterValue($counterValue);
            }

            if (!is_null($newCounterPhysStatus = $formData->getCounterPhysicalStatus())) {
                $task->setCounterPhysicalStatus($newCounterPhysStatus);
            }

            if ($newCounterNumber = $formData->getNewCounterNumber()) {
                $task->setChangedCounterNumber($newCounterNumber);
            }

            if ($newCounterType = $formData->getNewCounterType()) {
                $task->setChangedCounterType($newCounterType);
            }

            if ($newTerminalSeal = $formData->getNewTerminalSeal()) {
                $task->setChangedTerminalSeal($newTerminalSeal);
            }

            if ($newAntimagneticSeal = $formData->getNewAntimagneticSeal()) {
                $task->setChangedAntimagneticSeal($newAntimagneticSeal);
            }

            if ($newSideSeal = $formData->getNewSideSeal()) {
                $task->setChangedSideSeal($newSideSeal);
            }

            if (!is_null($newLodgersCount = $formData->getLodgersCount())) {
                $task->setChangedLodgersCount($newLodgersCount);
            }

            if (!is_null($newRoomsCount = $formData->getRoomsCount())) {
                $task->setChangedRoomsCount($newRoomsCount);
            }

            if ($operatorComment = $formData->getOperatorComment()) {
                $task->setOperatorComment($operatorComment);
            }

            $address = $formData->getLocality() && $formData->getStreet() && $formData->getHouseNumber()
                ? $this->getAndFillAddress(
                    $task->getCounter()->getAddress(),
                    $formData->getLocality(),
                    $formData->getStreet(),
                    $formData->getHouseNumber(),
                    $formData->getPorchNumber(),
                    $formData->getApartmentNumber(),
                    $formData->getLodgersCount(),
                    $formData->getRoomsCount()
                ) : null;

            $counter->setAddress($address);

            $em->persist($counter);

            $em->persist($task);
            $em->flush();
            $em->commit();
        } catch (Exception $exception) {
            $em->rollback();
            throw $exception;
        }
        return TaskToCheckAssembler::writeDTO($task);
    }

    /**
     * Accept the task to check
     * @param string $taskId
     * @throws Exception
     */
    public function acceptTaskToCheck(string $taskId)
    {
        if (!$task = $this->getTask($taskId)) {
            throw new HttpException(Response::HTTP_BAD_REQUEST,"There is no task with ID=$taskId");
        }
        //Проверка на то, что задача является "задачей к проверке"
        if ($task->getStatus() !== 1) {
            throw new HttpException(Response::HTTP_BAD_REQUEST,"Bad request");
        }

        $counter = $task->getCounter();
        $address = $counter->getAddress();

        $em = $this->em;
        $em->beginTransaction();

        try {
            //Дата-время принятия задачи
            $task->setAcceptanceDate(new DateTimeImmutable());

            //Тип задачи (Принято)
            $task->setStatus(2);

            //Оператор
            $task->setOperator($this->userHandler->getCurrentUser());

            //Координаты места выполнения задачи
            $counter->setCoordinates($task->getNewCoordinates());

            //Тип прибора учёта
            if ($counterType = $task->getChangedCounterType()) {
                $counter->setCounterType($counterType);
            }

            //Номер счётчика
            if ($counterNumber = $task->getChangedCounterNumber()) {
                $counter->setTitle($counterNumber);
            }

            //Клеммная пломба
            if ($terminalSeal = $task->getChangedTerminalSeal()) {
                $counter->setTerminalSeal($terminalSeal);
            }

            //Антимагнитная пломба
            if ($antimagneticSeal = $task->getChangedAntimagneticSeal()) {
                $counter->setAntiMagneticSeal($antimagneticSeal);
            }

            //Боковая пломба
            if ($sideSeal = $task->getChangedSideSeal()) {
                $counter->setSideSeal($sideSeal);
            }

            //Количество жильцов
            if ($lodgerCount = $task->getChangedLodgersCount()) {
                $address && $address->setLodgersCount($lodgerCount);
            }

            //Количество комнат
            if ($roomsCount = $task->getChangedRoomsCount()) {
                $address && $address->setRoomsCount($roomsCount);
            }

            $em->persist($counter);
            $em->persist($task);
            $em->flush();
            $em->commit();
        } catch (Exception $exception) {
            $em->rollback();
            throw $exception;
        }
    }

    /**
     * Accept the tasks to check
     * @return TaskToCheckDTO[]
     * @throws Exception
     */
    public function acceptTasksToCheck(): array
    {
        if (!$tasks = $this->taskRepository->findBy(['status' => 1])) {
            throw new HttpException(Response::HTTP_NOT_FOUND,"There are no tasks");
        }

        try {
            $em = $this->em;
            $em->beginTransaction();
            $count = 1;
            foreach ($tasks as $task) {
                if ($task->getCounterPhysicalStatus() !== 0) {
                    continue;
                }
                if ($this->isTaskHasChangedFields($task)) {
                    continue;
                }
                if ($this->doesTaskHaveNegativeDifference($task)) {
                    continue;
                }

                $counter = $task->getCounter();

                //Дата-время принятия задачи
                $task->setAcceptanceDate(new DateTimeImmutable());

                //Тип задачи (Принято)
                $task->setStatus(2);

                //Оператор
                $task->setOperator($this->userHandler->getCurrentUser());

                //Координаты места выполнения задачи
                $counter->setCoordinates($task->getNewCoordinates());

                //Тип прибора учёта
                if ($counterType = $task->getChangedCounterType()) {
                    $counter->setCounterType($counterType);
                }

                //Номер счётчика
                if ($counterNumber = $task->getChangedCounterNumber()) {
                    $counter->setTitle($counterNumber);
                }

                //Клеммная пломба
                if ($terminalSeal = $task->getChangedTerminalSeal()) {
                    $counter->setTerminalSeal($terminalSeal);
                }

                //Антимагнитная пломба
                if ($antimagneticSeal = $task->getChangedAntimagneticSeal()) {
                    $counter->setAntiMagneticSeal($antimagneticSeal);
                }

                //Боковая пломба
                if ($sideSeal = $task->getChangedSideSeal()) {
                    $counter->setSideSeal($sideSeal);
                }

                //Количество жильцов
                if ($lodgerCount = $task->getChangedLodgersCount()) {
                    $counter->getAddress()->setLodgersCount($lodgerCount);
                }

                //Количество комнат
                if ($roomsCount = $task->getChangedRoomsCount()) {
                    $counter->getAddress()->setRoomsCount($roomsCount);
                }

                $em->persist($counter);
                $em->persist($task);

                if ($count%10 === 0) {
                    $em->flush();
//                    $em->clear();
                }
                $count++;
            }
            $em->flush();
            $em->clear();
            $em->commit();
        } catch (Exception $exception) {
            $em->rollback();
            throw $exception;
        }
        return $this->getTasksToCheckList();
    }

    /**
     * Change the status of the task to check to task to work
     * @param string $taskId
     * @throws Exception|FilesystemException
     */
    public function returnTaskToCheck(string $taskId)
    {
        if (!$task = $this->getTask($taskId)) {
            throw new HttpException(Response::HTTP_BAD_REQUEST,"There is no task with ID=$taskId");
        }
        //Проверка на то, что задача является "задачей к проверке"
        if ($task->getStatus() !== 1) {
            throw new HttpException(Response::HTTP_BAD_REQUEST,"Bad request");
        }

        $em = $this->em;
        $em->beginTransaction();

        try {
            $this->photoHandler->deletePhotosFilesByTask($task);
            $this->photoHandler->deletePhotosByTask($task);

            $task->setChangedFields([]);
            $task->setChangedCounterTypeString(null);
            $task->setChangedCounterType(null);
            $task->setChangedCounterNumber(null);
            $task->setChangedTerminalSeal(null);
            $task->setChangedAntimagneticSeal(null);
            $task->setChangedSideSeal(null);
            $task->setCurrentCounterValue(null);
            $task->setExecutedDate(null);
            $task->setStatus(0);
            $task->setPriority(1);

            $em->persist($task);
            $em->flush();
            $em->commit();
        } catch (Exception $exception) {
            $em->rollback();
            throw $exception;
        }
    }

    /**
     * Get the task
     * @param $taskId
     * @return \App\Entity\Main\Task|null
     */
    private function getTask($taskId): ?Task
    {
        return $this->taskRepository->find($taskId);
    }

    /**
     * Get the task`s photos dir
     * @param Task $task
     * @return string|null
     */
    private function getTaskDir(Task $task): ?string
    {
        $period = $task->getPeriod()->format('Y-m');
//        if (!$dirName = realpath(self::PHOTOS_DIR).'/'.$period.'/'.$task->getId()) {
//            return null;
//        }
        return '/photos/' . $period . '/' . $task->getId() . '/';
    }

    /**
     * Does the task have modified fields?
     * @param \App\Entity\Main\Task $task
     * @return bool
     */
    private function isTaskHasChangedFields(Task $task): bool
    {
        return !(is_null($task->getChangedRoomsCount())
            && is_null($task->getChangedLodgersCount())
            && is_null($task->getChangedCounterNumber())
            && is_null($task->getChangedCounterTypeString())
            && is_null($task->getChangedTerminalSeal())
            && is_null($task->getChangedAntimagneticSeal())
            && is_null($task->getChangedSideSeal()))
            ;
    }

    /**
     * Does the task have negative difference?
     * @param \App\Entity\Main\Task $task
     * @return bool
     */
    private function doesTaskHaveNegativeDifference(Task $task): bool
    {
        $lastValue = !empty($task->getLastCounterValue()->getValue()->getValues())
            ? $task->getLastCounterValue()->getValue()->getValues()[0]
            : 0;
        if (!$task->getCurrentCounterValue()) {
            return true;
        }
        $currentValue = !empty($task->getCurrentCounterValue()->getValue()->getValues()) ? $task->getCurrentCounterValue()->getValue()->getValues()[0] : 0;
        return floatval($lastValue) > floatval($currentValue);
    }

    /**
     * Make a CounterValue
     * @param array $values
     * @param \App\Entity\Main\Counter $counter
     * @param UserInterface $user
     * @param DateTimeInterface $date
     * @return \App\Entity\Main\CounterValue
     * @throws Exception
     */
    private function makeCounterValue(array $values, Counter $counter, UserInterface $user, DateTimeInterface $date): CounterValue
    {
        $countValType = null;
        switch ($values) {
            case count($values) == 1 : $countValType = new CounterValueType1($values); break;
            case count($values) <= 3 : $countValType = new CounterValueType2($values); break;
            case count($values) > 3 : $countValType = new CounterValueType3($values); break;
        }
        $counterValue = new CounterValue();
        $counterValue->setCounter($counter)
            ->setInspector($user)
            ->setValue($countValType)
            ->setDate($date)
        ;
        return $counterValue;
    }

    /**
     * Fill the Address
     * @param Address|null $address
     * @param string $locality
     * @param string $street
     * @param string $houseNumber
     * @param string|null $porchNumber
     * @param string|null $apartmentNumber
     * @param int|null $lodgersCount
     * @param int|null $roomsCount
     * @return \App\Entity\Main\Address
     */
    private function getAndFillAddress(
        ?Address $address,
        string $locality,
        string $street,
        string $houseNumber,
        ?string $porchNumber,
        ?string $apartmentNumber,
        ?int $lodgersCount,
        ?int $roomsCount
    ): Address
    {
        if (!$address) {
            //Если не передан адрес
            $hash = $locality.'|'.$street.'|'.$houseNumber.'|'.$apartmentNumber;
            if (!$address = $this->addressHandler->getAddressByHash($hash)) {
                //Если по сформированному кешу нет адреса - создается новый адрес
                $address = new Address();
            }
        }
        $address->setLocality($locality)
            ->setStreet($street)
            ->setHouseNumber($houseNumber)
            ->setPorchNumber($porchNumber)
            ->setApartmentNumber($apartmentNumber)
            ->setLodgersCount($lodgersCount)
            ->setRoomsCount($roomsCount)
        ;
        return $address;
    }
}