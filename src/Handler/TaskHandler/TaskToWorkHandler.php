<?php

namespace App\Handler\TaskHandler;

use App\Entity\Main\Address;
use App\Entity\Main\CounterValue;
use App\Entity\Main\Task;
use App\Entity\Main\User;
use App\Form\TaskDTOType;
use App\Model\Assembler\TaskAssembler;
use App\Model\DTO\TaskDTO;
use DateTimeImmutable;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class TaskToWorkHandler extends AbstractTaskHandler
{
    /**
     * Get the tasks with status '0'
     * @param int $page
     * @param int|null $maxResult
     * @param string|null $search
     * @param string|null $searchField
     * @param array $filter
     * @return array
     */
    public function getTasksToWorkList(
        int $page = 1,
        int $maxResult = null,
        string $search = null,
        string $searchField = null,
        array $filter = []
    ): array
    {
        $tasksList = [];
        /** @var \App\Entity\Main\User $currentUser */
        $currentUser = $this->userHandler->getCurrentUser();
        if ($this->userHandler->isUserAdmin($currentUser)) {
            $tasksQuery = $this->taskRepository->getTasksWithSearchQueryBuilder(0, $search, $searchField, $filter);
        } else {
            if ($this->userHandler->isUserController($currentUser)) {
                $tasksQuery = $this->taskRepository->getTasksByControllerWithSearchQueryBuilder(
                    0,
                    $currentUser->getId(),
                    $currentUser->getCompanies()->getValues(),
                    $search,
                    $searchField,
                    $filter
                );
            } else {
                $tasksQuery = $this->taskRepository->getTasksByCompaniesWithSearchQueryBuilder(
                    0,
                    $currentUser->getCompanies()->getValues(),
                    $search,
                    $searchField,
                    $filter
                );
            }
        }

        if ($maxResult) {
            $pagination = $this->paginator->paginate($tasksQuery, $page, $maxResult);
            $tasks = $pagination->getItems();
            $taskCount = $pagination->getTotalItemCount();
        } else {
            /** @var Task[] $tasks */
            $tasks = $tasksQuery->getQuery()->execute();
            $taskCount = count($tasks);
        }


        if (!empty($tasks)) {
            foreach ($tasks as $task) {
                $tasksList[] = TaskAssembler::writeDTO($task);
            }
        }

        return [
            'tasksList' => $tasksList,
            'taskCount' => $taskCount
        ];
    }

    /**
     * Edit the 'Task to work'
     * @param $taskData
     * @param $taskId
     * @return TaskDTO
     */
    public function editTaskToWork($taskData, $taskId): TaskDTO
    {
        $form = $this->formFactory->create(TaskDTOType::class, null, ['csrf_protection' => false]);
        $form->submit($taskData);
        /** @var TaskDTO $formData */
        $formData = $form->getData();

        if (!$form->isValid()) {
            $errors = $form->getErrors(true);
            $messages = [];
            if (!empty($errors)) {
                foreach ($errors as $error) {
                    $cause = $error->getCause();
                    $messages[] = $cause->getPropertyPath().'|'.$cause->getCause()->getMessage();
                }
            }
            throw new HttpException(Response::HTTP_BAD_REQUEST,'Incorrect form data');
        }

        $em = $this->em;
        $em->beginTransaction();

        if (!$task = $this->getTask($taskId)) {
            throw new HttpException(Response::HTTP_NOT_FOUND,"Task with ID = $taskId not found");
        }

        try {
            $executor = $formData->getExecutor();
            $coordinates = [$formData->getCoordinateX(), $formData->getCoordinateY()];
            $counter = $task->getCounter();
            $address = $formData->getLocality() && $formData->getStreet() && $formData->getHouseNumber()
                ? $this->getAndFillAddress(
                    $task->getCounter()->getAddress(),
                    $formData->getLocality(),
                    $formData->getStreet(),
                    $formData->getHouseNumber(),
                    $formData->getPorchNumber(),
                    $formData->getApartmentNumber(),
                    $formData->getLodgersCount(),
                    $formData->getRoomsCount()
                ) : null;

            $task
                ->setTaskType($formData->getTaskType())
//                ->setCompany($formData->getCompany())
                ->setDeadline($formData->getDeadline())
                ->setExecutor($executor)
                ->setPriority($formData->getPriority())
                ->setStatus($formData->getStatus())
                ->setCoordinates($coordinates)
                ->setOperatorComment($formData->getOperatorComment())
//                ->setActualizedFields($formData->getActualizedFields())
            ;
            $counter->setAddress($address);

            $em->persist($counter);
            $em->persist($task);
            $em->flush();
            $em->commit();
        } catch (Exception $exception) {
            $em->rollback();
        }
        return TaskAssembler::writeDTO($task);
    }

    /**
     * Get the task
     * @param $taskId
     * @return Task|null
     */
    private function getTask($taskId): ?Task
    {
        return $this->taskRepository->find($taskId);
    }

    /**
     * Fill the Address
     * @param Address|null $address
     * @param string $locality
     * @param string $street
     * @param string $houseNumber
     * @param string|null $porchNumber
     * @param string|null $apartmentNumber
     * @param int|null $lodgersCount
     * @param int|null $roomsCount
     * @return Address
     */
    private function getAndFillAddress(
        ?Address $address,
        string $locality,
        string $street,
        string $houseNumber,
        ?string $porchNumber,
        ?string $apartmentNumber,
        ?int $lodgersCount,
        ?int $roomsCount
    ): Address
    {
        if (!$address) {
            //Если не передан адрес
            $hash = $locality.'|'.$street.'|'.$houseNumber.'|'.$apartmentNumber;
            if (!$address = $this->addressHandler->getAddressByHash($hash)) {
                //Если по сформированному кешу нет адреса - создается новый адрес
                $address = new Address();
            }
        }
        $address->setLocality($locality)
            ->setStreet($street)
            ->setHouseNumber($houseNumber)
            ->setPorchNumber($porchNumber)
            ->setApartmentNumber($apartmentNumber)
            ->setLodgersCount($lodgersCount)
            ->setRoomsCount($roomsCount)
        ;
        return $address;
    }

    /**
     * Create tasks to work for all the counters
     * @return array
     */
    public function createTasksToWork(): array
    {
        $errors = [];
        $counters = $this->counterHandler->counterRepository->findAll();

        foreach ($counters as $counter) {
            $agreement = $counter->getAgreement();
            $subscriber = $agreement->getSubscriber();
            $company = $agreement->getCompany();
            $executors = $counter->getTransformer()->getUsers()->getValues();
            $executor = empty($executors) ? null : $executors[0];
            $period = (new DateTimeImmutable())->modify('first day of this month')->setTime(0,0,0);
            $lastCounterValue = $this->em->getRepository(CounterValue::class)->findOneBy(
                ['counter' => $counter],
                ['date' => 'DESC']
            );
            $deadline = $period->modify('+1 month')->modify('+14 day');

            try {
                $this->em->beginTransaction();
                $task = new Task();

                $task->setCounter($counter);
                $task->setSubscriber($subscriber);
                $task->setAgreement($agreement);
                $task->setCompany($company);
                $task->setLastCounterValue($lastCounterValue);
                $task->setPeriod($period);
                $task->setStatus(0);
                $task->setTaskType(0);
                $task->setExecutor($executor);
                $task->setDeadline($deadline);

                $task->setCoordinates($counter->getCoordinates());
                $task->setCounterPhysicalStatus($counter->getWorkStatus());

                $this->em->persist($task);
                $this->em->flush();
                $this->em->commit();
            } catch (Exception $exception) {
                $this->em->rollback();
                $errors[] = $exception->getMessage();
            }
        }

        return $errors;
    }
}