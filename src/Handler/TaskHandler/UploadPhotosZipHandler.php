<?php

namespace App\Handler\TaskHandler;

use App\Entity\Main\Photo;
use App\Entity\Main\Task;
use App\Entity\Main\User;
use League\Flysystem\FilesystemException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use ZipArchive;

class UploadPhotosZipHandler extends AbstractTaskHandler
{
    /**
     * Get the upload task photos response
     * @param Task $task
     * @return mixed|string
     * @throws FilesystemException
     */
    public function getUploadTaskPhotosResponse(Task $task): Response
    {
        $period = $task->getPeriod()->format('Y-m');
        /** @var Photo[] $photos */
        $photos = $task->getPhotos()->getValues();
        $dirPath = '/photos/' . $period . '/' . $task->getId() . '/';

        $files = [];

        foreach ($photos as $photo) {
            $filePath = $dirPath . $photo->getFileName();
            $files[] = $filePath;
        }

        $zip = new ZipArchive();
        $zip_name = "../public/zipFileName.zip";

        if (!($zip->open($zip_name, ZipArchive::CREATE | ZipArchive::OVERWRITE) === TRUE)) {
            throw new HttpException(400, 'An error occurred while creating the file');
        }

        foreach ($files as $f) {
            $fileString = $this->s3UploadHandler->readFile($f);
            $zip->addFromString(basename($f), $fileString);
        }
        $zip->close();

        $response = new Response(
            file_get_contents($zip_name),
            Response::HTTP_OK,
            ['Content-Type' => 'application/zip',
                'Content-Disposition' => 'attachment; filename="' . basename($zip_name) . '"',
                'Content-Length' => filesize($zip_name)]);

        unlink($zip_name);

        return $response;
    }

    /**
     * Get all the tasks photos
     * @param User $user
     * @param array $filter
     * @param string|null $search
     * @param string|null $searchField
     * @param int $page
     * @param int|null $maxResult
     * @return Response
     */
    public function getUploadTAllTheTasksPhotosResponse(
        User $user,
        array $filter = [],
        string $search = null,
        string $searchField = null,
        int $page = 1,
        int $maxResult = null
    ): Response
    {
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', '600');

        // Получаем все принятые задачи

        if ($this->userHandler->isUserAdmin($user)) {
            $tasksQuery = $this->taskRepository->getTasksWithSearchQueryBuilder(
                2,
                $search,
                $searchField,
                $filter
            );
        } else {
            $tasksQuery = $this->taskRepository->getTasksByCompaniesWithSearchQueryBuilder(
                2,
                $user->getCompanies()->getValues(),
                $search,
                $searchField,
                $filter
            );
        }
        if ($maxResult) {
            if ($page) {
                $tasksQuery->setFirstResult($page > 1 ? ($page-1)*$maxResult : 0);
            }
            $tasksQuery->setMaxResults($maxResult);
        }
        /** @var $tasks Task[] */
        $tasks = $tasksQuery->getQuery()->execute();

        $zip = new ZipArchive();
        $zip_name = "../public/zipFileName.zip";

        if (!($zip->open($zip_name, ZipArchive::CREATE | ZipArchive::OVERWRITE) === TRUE)) {
            throw new HttpException(400, 'An error occurred while creating the file');
        }

        foreach ($tasks as $task) {
            $periodStr = $task->getPeriod()->format('Y-m');
            $agreementNumber = $task->getAgreement()->getNumber();
            $counterNumber = $task->getCounter()->getTitle();

            $photos = $task->getPhotos()->getValues();
            $dirPath = '/photos/' . $periodStr . '/' . $task->getId() . '/';

            // Название папки задачи в архиве
            $zipTaskDirName = $periodStr . '_' . $agreementNumber . '_' . $counterNumber;

            $files = [];

            foreach ($photos as $photo) {
                $filePath = $dirPath . $photo->getFileName();
                $files[] = $filePath;
            }


            $zip->addEmptyDir($zipTaskDirName);
            foreach ($files as $file) {
                try {
                    $fileString = $this->s3UploadHandler->readFile($file);
                    $zip->addFromString($zipTaskDirName . '/' . basename($file), $fileString);
                } catch (FilesystemException $e) {
                    continue;
                }
            }

        }
        $zip->close();

        $response = new Response(
            file_get_contents($zip_name),
            Response::HTTP_OK,
            ['Content-Type' => 'application/zip',
                'Content-Disposition' => 'attachment; filename="' . basename($zip_name) . '"',
                'Content-Length' => filesize($zip_name)]);

        unlink($zip_name);

        return $response;
    }

    /**
     * Get the task
     * @param $taskId
     * @return Task|null
     */
    private function getTask($taskId): ?Task
    {
        return $this->taskRepository->find($taskId);
    }
}