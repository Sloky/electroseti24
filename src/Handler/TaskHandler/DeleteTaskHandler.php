<?php

namespace App\Handler\TaskHandler;

class DeleteTaskHandler extends AbstractTaskHandler
{
    /**
     * Remove all the 'Tasks to work'
     */
    public function deleteTasksToWork(array $filter = [], ?string $search = null, ?string $searchField = null)
    {
        $this->taskRepository->removeTasks(0, $filter, $search, $searchField);
    }

    /**
     * Remove all the 'Tasks to check'
     */
    public function deleteTasksToCheck(array $filter = [], ?string $search = null, ?string $searchField = null)
    {
        $this->taskRepository->removeTasks(1, $filter, $search, $searchField);
    }

    /**
     * Remove all the 'completed Tasks'
     */
    public function deleteCompletedTasks(array $filter = [], ?string $search = null, ?string $searchField = null)
    {
        $this->taskRepository->removeTasks(2, $filter, $search, $searchField);
    }

    /**
     * Remove the Tasks by filter and search
     */
    public function deleteTasks(array $filter = [], ?string $search = null, ?string $searchField = null)
    {
        $this->taskRepository->removeTasks(null, $filter, $search, $searchField);
    }

    /**
     * Remove all the Tasks
     */
    public function deleteAllTheTasks()
    {
        $this->taskRepository->removeAllTheTasks();
    }

    /**
     * Remove the task
     */
    public function deleteTheTask(string $taskId)
    {
        $this->taskRepository->removeTheTask($taskId);
    }
}