<?php

namespace App\Handler\TaskHandler;

use App\Entity\Main\CounterValue;
use App\Entity\Main\Photo;
use App\Entity\Main\Task;
use App\Form\TaskMobileUploadFormType;
use App\Model\CounterValueType1;
use App\Model\TaskMobileUploadForm;
use DateTime;
use DateTimeImmutable;
use Exception;
use Symfony\Component\HttpFoundation\Request;

class UploadTaskFromMobileAppHandler extends AbstractTaskHandler
{
    /**
     * Upload "task to check" from the mobile app
     * @param Request $request
     * @param string $taskId
     * @throws Exception
     */
    public function uploadTaskFromMobileApp(Request $request, string $taskId) {
        $formData = new TaskMobileUploadForm();
        $form = $this->formFactory->create(TaskMobileUploadFormType::class, $formData, ['csrf_protection' => false]);
        $submittedData = array_merge($request->request->all(), $request->files->all());
        $form->submit($submittedData);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($task = $this->getTask($taskId)) {
                $this->em->beginTransaction();

                try {
                    $status = $formData->getStatus();
                    $counterPhysicalStatus = $formData->getCounterPhysicalStatus();
                    $counterValue = $formData->getCounterValue();
                    $counterValuePhoto = $formData->getCounterValuePhoto();
                    $counterPhoto = $formData->getCounterPhoto();
                    $counterNumber = $formData->getCounterNumber();
                    $counterTypeString = $formData->getCounterTypeString();
                    $terminalSeal = $formData->getTerminalSeal();
                    $terminalSealPhoto = $formData->getTerminalSealPhoto();
                    $antimagneticSeal = $formData->getAntimagneticSeal();
                    $antimagneticSealPhoto = $formData->getAntimagneticSealPhoto();
                    $sideSeal = $formData->getSideSeal();
                    $sideSealPhoto = $formData->getSideSealPhoto();
                    $lodgersCount = $formData->getLodgersCount();
                    $roomsCount = $formData->getRoomsCount();
                    $controllerComment = $formData->getControllerComment();
                    $newCoordinateX = $formData->getGPSLatitude();
                    $newCoordinateY = $formData->getGPSLongitude();
                    $executedDateTime = $formData->getExecutedDateTime();

                    //Переписываем период, в зависимости от даты исполнения или выгрузки задачи
                    $newPeriodDate = new DateTimeImmutable($executedDateTime->format('Y-m-d H:i:s')) ?? new DateTimeImmutable();
                    $newPeriod = $newPeriodDate->modify('first day of this month 00:00:00');
                    $task->setPeriod($newPeriod);

                    $photosDirName = $this->makeTaskPhotosDirName($task);
                    $currentUser = $this->userHandler->getCurrentUser();

                    $task->setExecutor($currentUser);
                    $status && $task->setStatus($status);
                    $counterPhysicalStatus && $task->setCounterPhysicalStatus($counterPhysicalStatus);

                    //Сохранение показаний УУ
                    if ($counterValue && $counterPhysicalStatus == 0) {
                        $newCounterValue = new CounterValue();
                        $newCounterValue->setCounter($task->getCounter());
                        $newCounterValue->setInspector($currentUser);
                        $newCounterValue->setValue(new CounterValueType1([$counterValue]));
                        $newCounterValue->setDate(new DateTimeImmutable($executedDateTime->format('Y-m-d H:i:s')));
                        $this->em->persist($newCounterValue);

                        //Сохранение расхода (разница показаний)
                        if ($task->getLastCounterValue()) {
                            $consumption = $counterValue - $task->getLastCounterValue()->getValue()->getValues()[0];
                            $task->setConsumption($consumption);
                        }

                        //Сохранение фотографии показаний
                        if ($counterValuePhoto) {
                            $counterValuePhotoName = uniqid('counter_value_photo_') . '.' . $counterValuePhoto->guessExtension();
                            $counterValuePhotoFullName = $this->s3UploadHandler
                                ->uploadPhoto(
                                    $counterValuePhoto,
                                    $photosDirName,
                                    $counterValuePhotoName,
                                    true
                                );
                            $newCounterValuePhoto = new Photo();
                            $newCounterValuePhoto->setFileName($counterValuePhotoFullName);
                            $newCounterValuePhoto->setTask($task);
                            $this->em->persist($newCounterValuePhoto);
                        }

                        $task->setCurrentCounterValue($newCounterValue);
                    }

                    //Сохранение фотографии неисправности/недоступности УУ
                    if ($counterPhoto) {
                        $counterPhotoName = uniqid('counter_photo_') . '.' . $counterPhoto->guessExtension();
                        $counterPhotoNameFullName = $this->s3UploadHandler
                            ->uploadPhoto(
                                $counterPhoto,
                                $photosDirName,
                                $counterPhotoName,
                                true
                            );
                        $newCounterPhoto = new Photo();
                        $newCounterPhoto->setFileName($counterPhotoNameFullName);
                        $newCounterPhoto->setTask($task);
                        $this->em->persist($newCounterPhoto);
                    }

                    //Сохранение номера УУ
                    if ($counterNumber) {
                        $task->setChangedCounterNumber($counterNumber);
                    }

                    //Сохранение типа УУ
                    if ($counterTypeString) {
                        $task->setChangedCounterTypeString($counterTypeString);
                    }

                    //Сохранение клеммной пломбы
                    if ($terminalSeal) {
                        $task->setChangedTerminalSeal($terminalSeal);
                    }

                    //Сохранение фотографии клеммной пломбы
                    if ($terminalSealPhoto) {
                        $terminalSealPhotoName = uniqid('terminal_seal_photo_') . '.' . $terminalSealPhoto->guessExtension();
                        $terminalSealPhotoNameFullName = $this->s3UploadHandler
                            ->uploadPhoto(
                                $terminalSealPhoto,
                                $photosDirName,
                                $terminalSealPhotoName,
                                true
                            );
                        $newTerminalPhoto = new Photo();
                        $newTerminalPhoto->setFileName($terminalSealPhotoNameFullName);
                        $newTerminalPhoto->setTask($task);
                        $this->em->persist($newTerminalPhoto);
                    }

                    //Сохранение антимагнитной пломбы
                    if ($antimagneticSeal) {
                        $task->setChangedAntimagneticSeal($antimagneticSeal);
                    }

                    //Сохранение фотографии антимагнитной пломбы
                    if ($antimagneticSealPhoto) {
                        $antimagneticSealPhotoName = uniqid('antimagnetic_seal_photo_') . '.' . $antimagneticSealPhoto->guessExtension();
                        $antimagneticSealPhotoNameFullName = $this->s3UploadHandler
                            ->uploadPhoto(
                                $antimagneticSealPhoto,
                                $photosDirName,
                                $antimagneticSealPhotoName,
                                true
                            );
                        $newAntimagneticSealPhoto = new Photo();
                        $newAntimagneticSealPhoto->setFileName($antimagneticSealPhotoNameFullName);
                        $newAntimagneticSealPhoto->setTask($task);
                        $this->em->persist($newAntimagneticSealPhoto);
                    }

                    //Сохранение боковой пломбы
                    if ($sideSeal) {
                        $task->setChangedSideSeal($sideSeal);
                    }

                    //Сохранение фотографии боковой пломбы
                    if ($sideSealPhoto) {
                        $sideSealPhotoName = uniqid('side_seal_photo_') . '.' . $sideSealPhoto->guessExtension();
                        $sideSealPhotoNameFullName = $this->s3UploadHandler
                            ->uploadPhoto(
                                $sideSealPhoto,
                                $photosDirName,
                                $sideSealPhotoName,
                                true
                            );
                        $newSideSealPhoto = new Photo();
                        $newSideSealPhoto->setFileName($sideSealPhotoNameFullName);
                        $newSideSealPhoto->setTask($task);
                        $this->em->persist($newSideSealPhoto);
                    }

                    //Сохранение числа проживающих
                    if ($lodgersCount) {
                        $task->setChangedLodgersCount($lodgersCount);
                    }

                    //Сохранение числа комнат
                    if ($roomsCount) {
                        $task->setChangedRoomsCount($roomsCount);
                    }

                    //Сохранение примечания контролёра
                    if ($controllerComment) {
                        $task->setControllerComment($controllerComment);
                    }

                    //Сохранение координат места выполнения задачи (места фотофиксации счётчика)
                    if ($newCoordinateX && $newCoordinateY) {
                        $task->setNewCoordinates([$newCoordinateX, $newCoordinateY]);
                    }

                    //Сохранение даты выполнения задачи
                    if ($executedDateTime) {
                        $task->setExecutedDate(new DateTimeImmutable($executedDateTime->format('Y-m-d H:i:s')));
                    }

                    $this->em->persist($task);
                    $this->em->flush();
                    $this->em->commit();
                } catch (Exception $e) {
                    $this->em->rollback();
                    throw $e;
                }
            }
        }
        $this->em->clear();
    }

    /**
     * Get the task
     * @param $taskId
     * @return \App\Entity\Main\Task|null
     */
    private function getTask($taskId): ?Task
    {
        return $this->taskRepository->find($taskId);
    }

    /**
     * Generate Task photos dir path
     * @param \App\Entity\Main\Task $task
     * @return string
     */
    private function makeTaskPhotosDirName(Task $task): string
    {
        $period = $task->getPeriod()->format('Y-m');
//        $agreementNumber = $task->getAgreement()->getNumber();
//        $counterNumber = $task->getCounter()->getTitle();
        return '/photos/'.$period.'/'.$task->getId().'/';
    }
}