<?php

namespace App\Handler\UserHandler;

use App\Entity\Main\Company;
use App\Entity\Main\Task;
use App\Entity\Main\User;
use App\Entity\Main\UserGroup;
use App\Form\UserFormType;
use App\Model\Assembler\UserAssembler;
use App\Model\UserForm;
use App\Repository\UserGroupRepository;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\User\UserInterface;

class UserHandler extends AbstractUserHandler
{
    /**
     * Get all the users
     * @return User[]|null
     */
    public function getUsers(): ?array
    {
        if (empty($users = $this->userRepository->findAll())) {
            return null;
        }
        $usersDTO = [];
        foreach ($users as $user) {
            $usersDTO[] = UserAssembler::writeDTO($user);
        }
        return $usersDTO;
    }

    /**
     * Get user list with filter and search
     * @param int $page
     * @param int|null $maxResult
     * @param string|null $search
     * @param array $filter
     * @return array
     */
    public function getUserList(int $page = 1, ?int $maxResult = null, string $search = null, array $filter = []): array
    {
        $userList = [];

        $userQuery = $this->userRepository->getUsersWithSearchQueryBuilder($search, $filter);
        if ($maxResult) {
            $pagination = $this->paginator->paginate($userQuery, $page, $maxResult);
            $users = $pagination->getItems();
            $usersCount = $pagination->getTotalItemCount();
        } else {
            /** @var User[] $users */
            $users = $userQuery->getQuery()->execute();
            $usersCount = count($users);
        }

        $userTaskData = $this->em->getRepository(Task::class)->getUsersWithTasksCount();

        if (!empty($users)) {
            foreach ($users as $user) {
                $userTaskToWorkCount = 0;
                foreach ($userTaskData as $item) {
                    if ($user->getId() === $item['id']) {
                        $userTaskToWorkCount = $item['taskCount'];
                        break;
                    }
                }

                $userCompaniesIds = array_map(function (Company $company) {
                    return $company->getId();
                }, $user->getCompanies()->getValues());

                $userCompaniesTasksToCheck = intval($this->em->getRepository(Task::class)
                    ->getTaskByCompaniesAndStatuses([1], $userCompaniesIds));

                $userList[] = UserAssembler::writeDTO($user, $userTaskToWorkCount, $userCompaniesTasksToCheck);
            }
        }

        return [
            'userList' => $userList,
            'usersCount' => $usersCount
        ];
    }

    /**
     * Get user by ID
     * @param $userId
     * @return User|null
     */
    public function getUser($userId): ?User
    {
        if ($user = $this->userRepository->find($userId)) {
            return $user;
        }
        return null;
    }

    /**
     * @param $fullName
     * @return User|null
     */
    public function getUserByFullName($fullName): ?User
    {
        $fullNameArr = explode(' ', $fullName);
        if (count($fullNameArr) === 3 && $user = $this->userRepository->findOneBy(
            [
                'surname' => $fullNameArr[0],
                'name' => $fullNameArr[1],
                'patronymic' => $fullNameArr[2],
            ]
        )) {
            return $user;
        }
        return null;
    }

    /**
     * Get the users by the full name and Company title
     * @param $fullName
     * @param $companyTitle
     * @return User|null
     */
    public function getUserByFullNameAndCompanyTitle($fullName, $companyTitle): ?User
    {
        $fullNameArr = explode(' ', $fullName);
        if (count($fullNameArr) === 3 && $users = $this->userRepository->getUsersByFullNameAndCompanyTitle(
                $fullNameArr[0],
                $fullNameArr[1],
                $fullNameArr[2],
                $companyTitle
            )
        )
        {
            return empty($users) ? null : $users[0];
        }
        return null;
    }

    /**
     * Get the current user
     * @return UserInterface|null
     */
    public function getCurrentUser(): ?UserInterface
    {
        return $this->security->getUser();
    }

    /**
     * Get user by username
     * @param $username
     * @return User|null
     */
    public function getUserByName($username): ?User
    {
        if ($user = $this->userRepository->findOneBy(['username' => $username])) {
            return $user;
        }
        return null;
    }

    /**
     * Get user by userCode
     * @param string $userCode
     * @return User|null
     */
    public function getUserByCode(string $userCode): ?User
    {
        if ($user = $this->userRepository->findOneBy(['userCode' => $userCode])) {
            return $user;
        }
        return null;
    }

    /**
     * Get users by group name
     * @param $groupName
     * @return User[]|null
     */
    public function getUsersByGroupName($groupName)
    {
        /** @var UserGroupRepository $userGroupRepository */
        $userGroupRepository = $this->em->getRepository(UserGroup::class);
        if ($group = $userGroupRepository->findOneBy(['groupName' => $groupName])) {
            /**@var \App\Entity\Main\UserGroup $group */
            $users = $group->getUsers()->getValues();
            return $users;
        }
        return null;
    }

    /**
     * Get Users by company (For TaskToWorkCard)
     * @param $taskId
     * @return User[]|null
     */
    public function getControllersForTask($taskId): ?array
    {
        if (!$task = $this->em->getRepository(Task::class)->find($taskId)) {
            return null;
        }
        try {
            $company = $task->getCompany();
            $users = $this->userRepository->getControllersByCompanies([$company->getId()]);
        } catch (Exception $exception) {
            return null;
        }
        return $users;
    }

    /**
     * Get Users by companies if User is operator (For TaskToWorkCard)
     * @return User[]|null
     */
    public function getControllersFromTasks(): ?array
    {
        $users = [];
        try {
            /** @var User $currentUser */
            $currentUser = $this->getCurrentUser();
            if ($this->isUserAdmin($currentUser)) {
                $group = $this->em->getRepository(UserGroup::class)->findOneBy(['groupName' => 'Controllers']);
                $users = $group->getUsers()->getValues();
            } else {
                $currentUserCompanies = $currentUser->getCompanies()->getValues();
                $users = $this->userRepository->getControllersByCompanies($currentUserCompanies);
            }
        } catch (Exception $exception) {
            return null;
        }
        return $users;
    }

    /**
     * Add new user
     * @param $userData
     */
    public function addNewUser($userData) {
        $form = $this->formFactory->create(UserFormType::class, null, ['csrf_protection' => false]);
        $form->submit($userData);
        /** @var UserForm $formData */
        $formData = $form->getData();

        if (!$form->isValid()) {
            throw new HttpException(Response::HTTP_BAD_REQUEST,'Incorrect form data');
        }

        $em = $this->em;
        $em->beginTransaction();
        try {
            $user = new User();
            if ($this->getUserByName($username = $formData->getUsername())) {
                throw new HttpException(Response::HTTP_BAD_REQUEST,"User with username=$username already exists");
            }
            $user->setUsername($username);
            $user->setPassword($this->passwordEncoder->encodePassword($user, $formData->getPassword()));
            if (!empty($roles = $formData->getRoles()->getValues())) {
                $user->addRoles($roles);
            }
            $user->setGroups($formData->getGroups());
            $user->setName($formData->getName());
            $user->setSurname($formData->getSurname());
            $user->setPatronymic($formData->getPatronymic());
            $user->setPhone($formData->getPhone());
            $user->setAddress($formData->getAddress());
            $user->setWorkStatus($formData->getWorkStatus());
            $user->setCompanies($formData->getCompanies());
            $em->persist($user);
            $em->flush();
            $em->commit();
        } catch (Exception $e) {
            $em->rollback();
            throw new HttpException($e->getStatusCode(), $e->getMessage());
        }
    }

    /**
     * Edit the user
     * @param $userData
     * @param $userId
     */
    public function editUser($userData, $userId) {
        $form = $this->formFactory->create(UserFormType::class, null, ['csrf_protection' => false]);
        $form->submit($userData);
        /** @var UserForm $formData */
        $formData = $form->getData();

        if (!$user = $this->getUser($userId)) {
            throw new HttpException(Response::HTTP_NOT_FOUND, 'User not found');
        }

        if ($user->getUsername() !== $formData->getUsername() && $this->getUserByName($username = $formData->getUsername())) {
            throw new HttpException(Response::HTTP_BAD_REQUEST,"User with username=$username already exists");
        }

        if (!$form->isValid()) {
            throw new HttpException(Response::HTTP_BAD_REQUEST,'Incorrect form data');
        }

        $em = $this->em;
        $em->beginTransaction();
        try {
            $user->setUsername($formData->getUsername());
            if (!is_null($formData->getPassword())) {
                $user->setPassword($this->passwordEncoder->encodePassword($user, $formData->getPassword()));
            }
            if (!empty($roles = $formData->getRoles()->getValues())) {
                $user->addRoles($roles);
            }
            $user->setGroups($formData->getGroups());
            $user->setName($formData->getName());
            $user->setSurname($formData->getSurname());
            $user->setPatronymic($formData->getPatronymic());
            $user->setPhone($formData->getPhone());
            $user->setAddress($formData->getAddress());
            $user->setWorkStatus($formData->getWorkStatus());
            $user->setCompanies($formData->getCompanies());
            $em->persist($user);
            $em->flush();
            $em->commit();
        } catch (Exception $e) {
            $em->rollback();
            throw new HttpException($e->getCode(), $e->getMessage());
        }
    }

    /**
     * Delete the User
     * @param $userId
     */
    public function deleteUser($userId)
    {
        if (!$user = $this->getUser($userId)) {
            throw new HttpException(Response::HTTP_NOT_FOUND, 'User not found');
        }
        $em = $this->em;
        $em->beginTransaction();
        try {
            $em->remove($user);
            $em->flush();
            $em->commit();
        } catch (Exception $e) {
            $em->rollback();
//            throw new HttpException($e->getStatusCode(), $e->getMessage());
            throw $e;
        }
    }

    /**
     * Get the User by the full name
     * @param string $fullName
     * @return User|null
     */
    public function isUserExistByFullName(string $fullName): ?User
    {
        $fullNameArr = explode(' ', $fullName);
        $user = null;
        if (!(count($fullNameArr) === 3 && $user = $this->userRepository->findOneBy([
            'surname' => $fullNameArr[0],
            'name' => $fullNameArr[1],
            'patronymic' => $fullNameArr[2]
        ]))) {
            return null;
        }
        return $user;
    }

    /**
     * Check for the admin user role
     * @param User $user
     * @return bool
     */
    public function isUserAdmin(User $user): bool
    {
        $roles = $user->getRoles();

        if (array_search('ROLE_ADMIN', $roles) === false) {
            return false;
        }
        return true;
    }

    /**
     * Check for the controller user role
     * @param User $user
     * @return bool
     */
    public function isUserController(User $user): bool
    {
        $roles = $user->getRoles();

        if (
            array_search('ROLE_ADMIN', $roles) === false
            && array_search('ROLE_OPERATOR', $roles) === false
            && array_search('ROLE_CONTROLLER', $roles) !== false
        ) {
            return true;
        }
        return false;
    }

    /**
     * Get Controllers count
     * @return int|mixed|string
     */
    public function getControllersCount()
    {
        $controllers = $this->userRepository->getControllers();
        return count($controllers);
    }

    /**
     * Get the full name of the user
     * @param User $user
     * @return string
     */
    public function getUserFullName(User $user): string
    {
        $surname = $user->getSurname() ?? '';
        $name = $user->getName() ?? '';
        $patronymic = $user->getPatronymic() ?? '';

        return $surname.' '.$name.' '.$patronymic;
    }
}