<?php

namespace App\Handler\UserHandler;

use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;

abstract class AbstractUserHandler
{
    /**
     * @var EntityManagerInterface
     */
    protected EntityManagerInterface $em;

    /**
     * @var UserRepository
     */
    public UserRepository $userRepository;

    /**
     * @var FormFactory
     */
    protected $formFactory;

    /**
     * @var UserPasswordEncoderInterface
     */
    protected UserPasswordEncoderInterface $passwordEncoder;

    /**
     * @var Security
     */
    protected Security $security;

    /**
     * @var PaginatorInterface
     */
    protected PaginatorInterface $paginator;

    /**
     * UserHandler constructor.
     * @param EntityManagerInterface $entityManager
     * @param UserRepository $userRepository
     * @param FormFactoryInterface $formFactory
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param Security $security
     * @param PaginatorInterface $paginator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        UserRepository $userRepository,
        FormFactoryInterface $formFactory,
        UserPasswordEncoderInterface $passwordEncoder,
        Security $security,
        PaginatorInterface $paginator
    )
    {
        $this->em = $entityManager;
        $this->userRepository = $userRepository;
        $this->formFactory = $formFactory;
        $this->passwordEncoder = $passwordEncoder;
        $this->security = $security;
        $this->paginator = $paginator;
    }
}