<?php

namespace App\Handler\UserHandler;

use App\Entity\Main\User;
use App\Repository\TaskRepository;
use App\Repository\UserRepository;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;

class UserProfileHandler extends AbstractUserHandler
{
    /**
     * @var TaskRepository
     */
    private TaskRepository $taskRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        UserRepository $userRepository,
        FormFactoryInterface $formFactory,
        UserPasswordEncoderInterface $passwordEncoder,
        Security $security,
        PaginatorInterface $paginator,
        TaskRepository $taskRepository
    )
    {
        parent::__construct($entityManager, $userRepository, $formFactory, $passwordEncoder, $security, $paginator);
        $this->taskRepository = $taskRepository;
    }

    /**
     * User profile data
     * @param \App\Entity\Main\User $user
     * @return array
     */
    public function getUserProfileData(User $user): array
    {
        $statistics = $this->taskRepository->getUserTaskStatistic($user->getId());
        $arrMonthStatistic = [];
        $averageMonthlyExecutionRatio = 0;
        $allExecutedTaskCount = 0;
        $diff = 0;
        if (!empty($statistics)) {
            foreach ($statistics as $key => $value) {
                /** @var DateTimeInterface $period */
                $period = $value['period'];
                $periodStr = $period->format('Y-m-d');

                isset($arrMonthStatistic[$periodStr]['taskToWorkCount']) ? : $arrMonthStatistic[$periodStr]['taskToWorkCount'] = 0;
                isset($arrMonthStatistic[$periodStr]['executedTaskCount']) ? : $arrMonthStatistic[$periodStr]['executedTaskCount'] = 0;
                isset($arrMonthStatistic[$periodStr]['acceptedTaskCount']) ? : $arrMonthStatistic[$periodStr]['acceptedTaskCount'] = 0;
                isset($arrMonthStatistic[$periodStr]['unavailableTaskCount']) ? : $arrMonthStatistic[$periodStr]['unavailableTaskCount'] = 0;

                $arrMonthStatistic[$periodStr]['taskToWorkCount']
                    += $value['taskStatus'] === 0 ? intval($value['taskCount']) : 0;
                $arrMonthStatistic[$periodStr]['executedTaskCount']
                    += $value['taskStatus'] !== 0 ? intval($value['taskCount']) : 0;
                $arrMonthStatistic[$periodStr]['acceptedTaskCount']
                    += $value['taskStatus'] === 2 ? intval($value['taskCount']) : 0;
                $arrMonthStatistic[$periodStr]['unavailableTaskCount']
                    += $value['counterPhysicalStatus'] === 1 ? intval($value['taskCount']) : 0;

                $allExecutedTaskCount += $value['taskStatus'] !== 0 ? intval($value['taskCount']) : 0;
            }
            if (count($arrMonthStatistic) > 1) {
                $allExecutedTaskCount -= $arrMonthStatistic[array_key_last($arrMonthStatistic)]['executedTaskCount'];
                $averageMonthlyExecutionRatio = intval(round($allExecutedTaskCount / (count($arrMonthStatistic) - 1)));
                $penultimateStatisticsItemKey = array_keys($arrMonthStatistic)[count($arrMonthStatistic) - 2];
                $penultimateStatisticsItem = $arrMonthStatistic[$penultimateStatisticsItemKey];
                $diff = intval($penultimateStatisticsItem['executedTaskCount'] - $averageMonthlyExecutionRatio);
            }
        }

        return [
            'user' => $user,
            'statistic' => $arrMonthStatistic,
            'averageMonthlyExecutionRatio' => $averageMonthlyExecutionRatio,
            'delta' => $diff
        ];
    }

    /**
     * User task execution statistics for mobile app
     * @param \App\Entity\Main\User $user
     * @return array
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getControllerProfileForMobileApp(User $user): array
    {
        $period = (new DateTimeImmutable())->modify('first day of this month 00:00:00');
        $executedTaskCount = intval($this->taskRepository->getExecutedTaskCount($user->getId(), $period));
        $taskToWorkCount = intval($this->taskRepository->getUsersTaskToWorkCount($user->getId(), $period));
        $totalTaskCount = intval($this->taskRepository->getUsersTaskCount($user->getId(), $period));

        $taskRatio = $totalTaskCount > 0
            ? round($executedTaskCount / $totalTaskCount, 2)
            : 0
        ;
        return compact(['executedTaskCount', 'taskToWorkCount', 'taskRatio']);
    }
}