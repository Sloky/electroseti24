<?php

namespace App\Handler\UserHandler;

use App\Entity\Main\Company;
use App\Entity\Main\User;
use App\Entity\Main\UserGroup;
use App\Repository\UserRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;

class ExcelUserStatisticHandler extends AbstractUserHandler
{
    /**
     * @var UserProfileHandler
     */
    private UserProfileHandler $userProfileHandler;

    /**
     * @var ParameterBagInterface
     */
    private ParameterBagInterface $parameterBag;

    /**
     * @var UserHandler
     */
    private UserHandler $userHandler;

    public function __construct(
        EntityManagerInterface $entityManager,
        UserRepository $userRepository,
        FormFactoryInterface $formFactory,
        UserPasswordEncoderInterface $passwordEncoder,
        Security $security,
        PaginatorInterface $paginator,
        UserProfileHandler $userProfileHandler,
        UserHandler $userHandler,
        ParameterBagInterface $parameterBag
    )
    {
        parent::__construct(
            $entityManager,
            $userRepository,
            $formFactory,
            $passwordEncoder,
            $security,
            $paginator
        );
        $this->userProfileHandler = $userProfileHandler;
        $this->parameterBag = $parameterBag;
        $this->userHandler = $userHandler;
    }

    /**
     * Generate excel profile data
     * @param \App\Entity\Main\User $user
     * @return Response
     * @throws Exception
     */
    public function getExcelProfileData(User $user): Response
    {
        $profileData = $this->userProfileHandler->getUserProfileData($user);
        /** @var \App\Entity\Main\User $currentUser */
        $currentUser = $this->userHandler->getCurrentUser();

        $userFullName = $user->getSurname()." ".$user->getName()." ".$user->getPatronymic();
        $userGroupNames = $this->getUserGroupNamesString($user);
        $companiesStr = $this->getUserCompaniesString($user);
        $userAddress = $user->getAddress();
        $userPhone = $user->getPhone();
        $userProfileUploadingDate = (new DateTimeImmutable())->format('d.m.Y H:i:s');
        $averageMonthlyExecutionRatio = $profileData['averageMonthlyExecutionRatio'];

        $sOutFile = $userFullName.".xlsx";

        $fileName = $this->parameterBag->get('controller_profile_excel_template_path');
        $oSpreadsheet_Out = IOFactory::load($fileName);

        $oSpreadsheet_Out->getProperties()
            ->setCreator($currentUser->getSurname()."_".$currentUser->getName()."_".$currentUser->getPatronymic())
            ->setLastModifiedBy('User profile data')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Task data ')
        ;

        $oSpreadsheet_Out->setActiveSheetIndex(0)
            ->setCellValue("D2", $userFullName)
            ->setCellValue("D3", $userGroupNames)
            ->setCellValue("D4", $companiesStr)
            ->setCellValue("D5", $userAddress)
            ->setCellValue("D6", $userPhone)
            ->setCellValue("D7", $userProfileUploadingDate)
            ->setCellValue("D9", $profileData['averageMonthlyExecutionRatio'])
            ->setCellValue("D10", $profileData['delta'])
        ;

        $activeSheet = $oSpreadsheet_Out->getActiveSheet();

        $activeSheet->getStyle("D6")->getAlignment()->setHorizontal('left');

        if (!empty($profileData['statistic'])) {
            $i = 0;
            foreach ($profileData['statistic'] as $period => $value) {
                $activeSheet
                    ->setCellValue('A'.($i+13), $period)
                    ->setCellValue('B'.($i+13), intval($value['taskToWorkCount']) + intval($value['executedTaskCount']))
                    ->setCellValue('C'.($i+13), intval($value['taskToWorkCount']))
                    ->setCellValue('D'.($i+13), intval($value['unavailableTaskCount']))
                    ->setCellValue('E'.($i+13), intval($value['executedTaskCount']))
                    ->setCellValue('F'.($i+13), intval($value['acceptedTaskCount']))
                ;
                $i++;
            }
        }

        $oWriter = IOFactory::createWriter($oSpreadsheet_Out, 'Xlsx');
        $oWriter->save($sOutFile);

        $response = new Response(
            file_get_contents($sOutFile),
            Response::HTTP_OK,
            ['Content-Type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'Content-Disposition' => 'attachment; filename="' . basename($sOutFile) . '"',
                'Content-Length' => filesize($sOutFile)]);

        unlink($sOutFile);

        return $response;
    }

    /**
     * Get user companies titles as string
     * @param \App\Entity\Main\User $user
     * @return string
     */
    private function getUserCompaniesString(User $user): string
    {
        $userCompaniesNamesString = '';
        /** @var \App\Entity\Main\Company $company */
        foreach ($user->getCompanies()->getValues() as $key => $company) {
            if ($key !== 0) {
                $userCompaniesNamesString .= ', ';
            }
            $userCompaniesNamesString .= $company->getTitle();
        }
        return $userCompaniesNamesString;
    }

    /**
     * Get user group names as string
     * @param \App\Entity\Main\User $user
     * @return string
     */
    private function getUserGroupNamesString(User $user): string
    {
        $userGroupNames = '';
        $userGroups = $user->getGroups()->getValues();
        /**
         * @var int $key
         * @var \App\Entity\Main\UserGroup $group
         */
        foreach ($userGroups as $key => $group) {
            if ($key !== 0) {
                $userGroupNames .= ', ';
            }
            $userGroupNames .= $this->getTranslateGroupName($group->getGroupName());
        }
        return $userGroupNames;
    }

    /**
     * Translate group name
     * @param string $groupName
     * @return string
     */
    private function getTranslateGroupName(string $groupName): string
    {
        $translatedGroupName = '';
        switch ($groupName) {
            case 'Controllers' : $translatedGroupName = 'Контролёры'; break;
            case 'Operators' : $translatedGroupName = 'Операторы'; break;
            case 'Admins' : $translatedGroupName = 'Администраторы'; break;
        }
        return $translatedGroupName;
    }
}