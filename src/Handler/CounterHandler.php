<?php


namespace App\Handler;


use App\Entity\Main\Agreement;
use App\Entity\Main\Company;
use App\Entity\Main\Counter;
use App\Entity\Main\CounterType;
use App\Entity\Main\CounterValue;
use App\Entity\Main\Feeder;
use App\Entity\Main\Subscriber;
use App\Entity\Main\Substation;
use App\Entity\Main\Transformer;
use App\Form\CounterCardFormType;
use App\Form\CounterCardTPFormType;
use App\Form\CounterListUploadFormType;
use App\Handler\UserHandler\UserHandler;
use App\Model\Assembler\MetricUnitAssembler;
use App\Model\BypassListRecordError;
use App\Model\CounterCardForm;
use App\Model\CounterCardTPForm;
use App\Model\CounterListUploadForm;
use App\Model\CounterValueType1;
use App\Model\DTO\MetricUnitDTO;
use App\Repository\CounterRepository;
use App\Repository\CounterTypeRepository;
use App\Repository\CounterValueRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CounterHandler
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    /**
     * @var CounterRepository
     */
    public $counterRepository;

    /**
     * @var CounterTypeRepository
     */
    protected $counterTypeRepository;

    /**
     * @var CounterValueRepository
     */
    protected CounterValueRepository $counterValueRepository;

    /**
     * @var PaginatorInterface
     */
    protected PaginatorInterface $paginator;

    /**
     * @var AgreementHandler
     */
    protected AgreementHandler $agreementHandler;

    /**
     * @var CounterValueHandler
     */
    protected CounterValueHandler $counterValueHandler;

    /**
     * @var UserHandler
     */
    protected UserHandler $userHandler;

    /**
     * CounterHandler constructor.
     * @param FormFactoryInterface $formFactory
     * @param EntityManagerInterface $entityManager
     * @param CounterRepository $counterRepository
     * @param CounterTypeRepository $counterTypeRepository
     * @param CounterValueRepository $counterValueRepository
     * @param PaginatorInterface $paginator
     * @param AgreementHandler $agreementHandler
     * @param CounterValueHandler $counterValueHandler
     * @param UserHandler $userHandler
     */
    public function __construct(
        FormFactoryInterface $formFactory,
        EntityManagerInterface $entityManager,
        CounterRepository $counterRepository,
        CounterTypeRepository $counterTypeRepository,
        CounterValueRepository $counterValueRepository,
        PaginatorInterface $paginator,
        AgreementHandler $agreementHandler,
        CounterValueHandler $counterValueHandler,
        UserHandler $userHandler
    )
    {
        $this->formFactory = $formFactory;
        $this->em = $entityManager;
        $this->counterRepository = $counterRepository;
        $this->counterTypeRepository = $counterTypeRepository;
        $this->counterValueRepository = $counterValueRepository;
        $this->paginator = $paginator;
        $this->agreementHandler = $agreementHandler;
        $this->counterValueHandler = $counterValueHandler;
        $this->userHandler = $userHandler;
    }

    /**
     * Get all the counters
     * @return Counter[]|null
     */
    public function getCounters(): ?array
    {
        if (empty($counters = $this->counterRepository->findAll())) {
            return null;
        }
        return $counters;
    }

    /**
     * Get the Counter
     * @param string $id
     * @return Counter|null
     */
    public function getCounter(string $id): ?Counter
    {
        if (!$counter = $this->counterRepository->find($id)) {
            return null;
        }
        return $counter;
    }

    /**
     * Get the Counter by number
     * @param string $counterNumber
     * @return \App\Entity\Main\Counter|null
     */
    public function getCounterByNumber(string $counterNumber): ?Counter
    {
        if (!$counter = $this->counterRepository->findOneBy(['title' => $counterNumber])) {
            return null;
        }
        return $counter;
    }

    /**
     * Get Counter by number and agreement id
     * @param string $counterNumber
     * @param string $agreement
     * @return \App\Entity\Main\Counter|null
     */
    public function getCounterByNumberAndAgreement(string $counterNumber, string $agreement): ?Counter
    {
        if (!$counter = $this->counterRepository->findOneBy(
            [
                'title' => $counterNumber,
                'agreement' => $agreement
            ]
        )) {
            return null;
        }
        return $counter;
    }

    /**
     * Get Counter by number and agreement title
     * @param string $counterNumber
     * @param string $agreementTitle
     * @return Counter|null
     * @throws Exception
     */
    public function getCounterByNumberAndAgreementTitle(string $counterNumber, string $agreementTitle): ?Counter
    {
        if (!$counters = $this->counterRepository->findCounterByNumberAndAgreementTitle($counterNumber, $agreementTitle)) {
            return null;
        }
        if (count($counters) > 1) {
            throw new Exception("ununical");
        }
        return $counters[0];
    }

    /**
     * Get all the counter types
     * @return \App\Entity\Main\CounterType[]|null
     */
    public function getCounterTypes(): ?array
    {
        if (!$counterTypes = $this->counterTypeRepository->findAll()) {
            return null;
        }
        return $counterTypes;
    }

    /**
     * Get Counters by Agreement with search and pagination
     * @param \App\Entity\Main\Agreement $agreement
     * @param string|null $search
     * @param int $page
     * @param int|null $maxResult
     * @return array
     */
    public function getCountersByAgreementWithSearch(
        Agreement $agreement,
        ?string $search,
        int $page = 1,
        ?int $maxResult = null
    ): array
    {
        $counterQB = $this->counterRepository->getCountersByAgreementWithSearchQueryBuilder($agreement, $search);

        if ($maxResult) {
            $pagination = $this->paginator->paginate($counterQB, $page, $maxResult);
            $counters = $pagination->getItems();
            $countersCount = $pagination->getTotalItemCount();
        } else {
            /** @var \App\Entity\Main\Counter[] $counters */
            $counters = $counterQB->getQuery()->execute();
            $countersCount = count($counters);
        }

        return [
            'counters' => $counters,
            'countersCount' => $countersCount
        ];
    }

    /**
     * Get Counters by TP with filters, search and pagination
     * @param \App\Entity\Main\Transformer $transformer
     * @param array|null $filter
     * @param string|null $search
     * @param int $page
     * @param int|null $maxResult
     * @return MetricUnitDTO[]
     * Get metric unit DTOs by transformer
     */
    public function getCounterByTPWithFilters(
        Transformer $transformer,
        ?array $filter,
        ?string $search,
        int $page = 1,
        ?int $maxResult = null
    ): array
    {
        $filter['transformer'] = $transformer->getTitle();
        $counterQB = $this->counterRepository->getCountersWithSearchQueryBuilder($search, $filter);

        if ($maxResult) {
            $pagination = $this->paginator->paginate($counterQB, $page, $maxResult);
            $counters = $pagination->getItems();
            $countersCount = $pagination->getTotalItemCount();
        } else {
            /** @var \App\Entity\Main\Counter[] $counters */
            $counters = $counterQB->getQuery()->execute();
            $countersCount = count($counters);
        }

        $counterIds = array_map(function ($value) {
            return $value->getId();
        }, $counters);

        $counterValues = $this->counterValueRepository->getLastValuesByCountersSQLQuery($counterIds);
        $metricDTO = [];
        foreach ($counters as $counter) {
            $counterValue = $this->getLastCounterValueFromResultArray($counterValues, $counter->getId());
//            $counterValue = $this->counterValueRepository->findOneBy(['counter' => $counter], ['date' => 'DESC']);
            $metricDTO[] = MetricUnitAssembler::writeDTO($counter, $counterValue);
        }

        return [
            'counters' => $metricDTO,
            'countersCount' => $countersCount
        ];
    }

    /**
     * Edit the counter card data
     * @param Request $request
     * @param Counter $counter
     * @return \App\Entity\Main\Counter
     * @throws Exception
     */
    public function editCounterCardData(Request $request, Counter $counter): Counter
    {
        $formData = new CounterCardTPForm();
        $form = $this->formFactory->create(CounterCardTPFormType::class, $formData);
        $formSendingData = $request->request->all();
        $form->submit($formSendingData);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->em->beginTransaction();
                $counter->setTransformer($formData->getTransformer());
                $counter->setTitle($formData->getCounterTitle());
                $counter->setWorkStatus($formData->getCounterPhysicalStatus());
                $counter->setTemporaryCounterType($formData->getCounterModel());
                $counter->setTerminalSeal($formData->getTerminalSeal());
                $counter->setAntiMagneticSeal($formData->getAntimagneticSeal());
                $counter->setSideSeal($formData->getSideSeal());
                $counter->setTemporaryAddress($formData->getAddress());
                if ($formData->getCoordinateX() && $formData->getCoordinateY()) {
                    $counter->setCoordinates([$formData->getCoordinateX(), $formData->getCoordinateY()]);
                }
                $counter->setTemporaryLodgersCount($formData->getLodgersCount());
                $counter->setTemporaryRoomsCount($formData->getRoomsCount());

                if ($counter->getInitialValue()) {
                    $initialValue = $counter->getInitialValue();
                    $initialValue->setDate($formData->getInitialCounterValueDate());
                    $initialValue->setValue(new CounterValueType1([$formData->getInitialCounterValue()]));
                    $this->em->persist($initialValue);
                } elseif ($formData->getInitialCounterValue()) {
                    $initialValue = new CounterValue();
                    $initialValue->setDate($formData->getInitialCounterValueDate() ?? new DateTimeImmutable());
                    $initialValue->setValue(new CounterValueType1([$formData->getInitialCounterValue()]));
                    $initialValue->setCounter($counter);
                    $this->em->persist($initialValue);
                    $counter->setInitialValue($initialValue);
                }

                if ($formData->getLastCounterValueId() && ($formData->getLastCounterValueId() !== $formData->getInitialCounterValueId())) {
                    if(!$lastCounterValue = $this->counterValueRepository->find($formData->getLastCounterValueId())) {
                        throw new HttpException(
                            400,
                            "Incorrect form data. There is no CounterValue by 'lastCounterValueId'"
                        );
                    }
                    $lastCounterValue->setDate($formData->getLastCounterValueDate() ?? new DateTimeImmutable());
                    $lastCounterValue->setValue(new CounterValueType1([$formData->getLastCounterValue()]));
                    $lastCounterValue->setCounter($counter);
                    $this->em->persist($lastCounterValue);
                } elseif ($formData->getLastCounterValue() && $formData->getLastCounterValueDate()) {
                    $lastCounterValue = new CounterValue();
                    $lastCounterValue->setDate($formData->getLastCounterValueDate());
                    $lastCounterValue->setValue(new CounterValueType1([$formData->getLastCounterValue()]));
                    $lastCounterValue->setCounter($counter);
                    $this->em->persist($lastCounterValue);
                }

                $agreement = $counter->getAgreement();
                $agreement->setNumber($formData->getAgreementNumber());
                $subscriber = $agreement->getSubscriber();
                $subscriber->setTitle($formData->getSubscriberTitle());
                $subscriber->setPhones($formData->getPhones()->getValues());

                $this->em->persist($counter);
                $this->em->persist($agreement);
                $this->em->persist($subscriber);
                $this->em->flush();
                $this->em->commit();
                return $counter;
            } catch (Exception $exception) {
                $this->em->rollback();
                throw $exception;
            }
        } else {
            throw new HttpException(400, "Incorrect form data");
        }
    }

    /**
     * Create a new Counter
     * @throws Exception
     */
    public function addCounter(Agreement $agreement, Request $request)
    {
        $counterCardForm = new CounterCardForm();
        $form = $this->formFactory->create(CounterCardFormType::class, $counterCardForm);
        $form->submit($request->request->all());
        $form->handleRequest($request);

        if (!$form->isValid()) {
            throw new HttpException(
                Response::HTTP_BAD_REQUEST,
                'Invalid form data'
            );
        }

        if ($this->getCounterByNumber($counterCardForm->getCounterTitle())) {
            throw new HttpException(
                Response::HTTP_BAD_REQUEST,
                "An counter with number \"{$counterCardForm->getCounterTitle()}\" already exists"
            );
        }

        try {
            $this->em->beginTransaction();

            $counter = new Counter();
            $counter->setTitle($counterCardForm->getCounterTitle());
            $counter->setTransformer($counterCardForm->getTransformer());
            $counter->setTemporaryCounterType($counterCardForm->getCounterModel());
            $counter->setTemporaryAddress($counterCardForm->getAddress());
            $counter->setWorkStatus($counterCardForm->getCounterPhysicalStatus());
            $counter->setTerminalSeal($counterCardForm->getTerminalSeal());
            $counter->setAntiMagneticSeal($counterCardForm->getAntimagneticSeal());
            $counter->setSideSeal($counterCardForm->getSideSeal());
            $counter->setAgreement($agreement);
            $counter->setInstallDate($counterCardForm->getInitialCounterValueDate() ?? new DateTimeImmutable());
            $counter->setElectricPoleNumber($counterCardForm->getElectricPoleTitle());
            $counter->setElectricLine($counterCardForm->getElectricLineTitle());
            $counter->setTemporaryLodgersCount($counterCardForm->getLodgersCount());
            $counter->setTemporaryRoomsCount($counterCardForm->getRoomsCount());
            if ($counterCardForm->getCoordinateX() && $counterCardForm->getCoordinateY()) {
                $counter->setCoordinates([
                    $counterCardForm->getCoordinateX(),
                    $counterCardForm->getCoordinateY()
                ]);
            }
            $this->em->persist($counter);

            $initialValue = new CounterValue();
            if ($counterCardForm->getInitialCounterValue()) {
                $initialValue->setValue(new CounterValueType1([$counterCardForm->getInitialCounterValue()]));
            } else {
                $initialValue->setValue(new CounterValueType1([0]));
            }
            $initialValue->setDate($counterCardForm->getInitialCounterValueDate() ?? new DateTimeImmutable());
            $initialValue->setInspector($this->userHandler->getCurrentUser());
            $initialValue->setCounter($counter);
            $this->em->persist($initialValue);

            $counter->setInitialValue($initialValue);
            $this->em->persist($counter);

            if ($counterCardForm->getLastCounterValue()) {
                $lastValue = new CounterValue();
                $lastValue->setValue(new CounterValueType1([$counterCardForm->getLastCounterValue()]));
                $lastValue->setDate($counterCardForm->getLastCounterValueDate() ?? new DateTimeImmutable());
                $lastValue->setInspector($this->userHandler->getCurrentUser());
                $lastValue->setCounter($counter);
                $this->em->persist($lastValue);
            }

            $this->em->flush();
            $this->em->commit();
        } catch (Exception $exception) {
            $this->em->rollback();
            throw $exception;
        }
    }

    /**
     * Edit the Counter
     * @param \App\Entity\Main\Counter $counter
     * @param Request $request
     * @return Counter
     * @throws Exception
     */
    public function editCounter(Counter $counter, Request $request): Counter
    {
        $counterCardForm = new CounterCardForm();
        $form = $this->formFactory->create(CounterCardFormType::class, $counterCardForm);
        $form->submit($request->request->all());
        $form->handleRequest($request);

        if (!$form->isValid()) {
            throw new HttpException(
                Response::HTTP_BAD_REQUEST,
                'Invalid form data'
            );
        }

        if (
            ($counter->getTitle() !== $counterCardForm->getCounterTitle())
            && $this->getCounterByNumber($counterCardForm->getCounterTitle())
        ) {
            throw new HttpException(
                Response::HTTP_BAD_REQUEST,
                "An counter with number \"{$counterCardForm->getCounterTitle()}\" already exists"
            );
        }

        try {
            $this->em->beginTransaction();

            $counter->setTitle($counterCardForm->getCounterTitle());
            $counter->setTransformer($counterCardForm->getTransformer());
            $counter->setTemporaryCounterType($counterCardForm->getCounterModel());
            $counter->setTemporaryAddress($counterCardForm->getAddress());
            $counter->setWorkStatus($counterCardForm->getCounterPhysicalStatus());
            $counter->setTerminalSeal($counterCardForm->getTerminalSeal());
            $counter->setAntiMagneticSeal($counterCardForm->getAntimagneticSeal());
            $counter->setSideSeal($counterCardForm->getSideSeal());
            $counter->setInstallDate($counterCardForm->getInitialCounterValueDate() ?? new DateTimeImmutable());
            $counter->setElectricPoleNumber($counterCardForm->getElectricPoleTitle());
            $counter->setElectricLine($counterCardForm->getElectricLineTitle());
            $counter->setTemporaryLodgersCount($counterCardForm->getLodgersCount());
            $counter->setTemporaryRoomsCount($counterCardForm->getRoomsCount());
            if ($counterCardForm->getCoordinateX() && $counterCardForm->getCoordinateY()) {
                $counter->setCoordinates([
                    $counterCardForm->getCoordinateX(),
                    $counterCardForm->getCoordinateY()
                ]);
            }
            $this->em->persist($counter);

            $initialValue = $counter->getInitialValue();
            $initialValue->setValue(new CounterValueType1([$counterCardForm->getInitialCounterValue()]));
            $initialValue->setDate($counterCardForm->getInitialCounterValueDate() ?? new DateTimeImmutable());
            $this->em->persist($initialValue);

            /*if ($counterCardForm->getLastCounterValueId()) {
                $lastValue = $this->counterValueRepository->find($counterCardForm->getLastCounterValueId());
                $lastValue->setValue(new CounterValueType1([$counterCardForm->getInitialCounterValue()]));
                $lastValue->setDate($counterCardForm->getInitialCounterValueDate() ?? new DateTimeImmutable());
                $this->em->persist($lastValue);
            } elseif ($counterCardForm->getLastCounterValue()) {
                $lastValue = new CounterValue();
                $lastValue->setValue(new CounterValueType1([$counterCardForm->getInitialCounterValue()]));
                $lastValue->setDate($counterCardForm->getInitialCounterValueDate() ?? new DateTimeImmutable());
                $lastValue->setInspector($this->userHandler->getCurrentUser());
                $lastValue->setCounter($counter);
                $this->em->persist($lastValue);
            }*/

            $this->em->flush();
            $this->em->commit();
        } catch (Exception $exception) {
            $this->em->rollback();
            throw $exception;
        }

        return $counter;
    }

    /**
     * Load counter Excel list file
     * @param Request $request
     * @param \App\Entity\Main\Transformer $transformer
     * @return array
     */
    public function loadCountersExcelList(Request $request, Transformer $transformer): array
    {
        $formData = new CounterListUploadForm();
        $form = $this->formFactory->create(CounterListUploadFormType::class, $formData, ['csrf_protection' => false]);
        $formSendingData = array_merge($request->request->all(), $request->files->all());
        $form->submit($formSendingData);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $formData->getFile();
            $fileName = $file->getPathname();
            return $this->parseCounterList($fileName, $transformer);
        }

        $errors[] = 'Произошла ошибка при загрузке списка узлов учёта';
        foreach ($form->getErrors(true) as $formErrorIterator) {
            $errors[] = $formErrorIterator->getMessage();
        }
        return $errors;
    }

    /**
     * @param string $fileName
     * @param Transformer $transformer
     * @return array
     */
    private function parseCounterList(string $fileName, Transformer $transformer): array
    {
        $counterListErrors = [];

        $em = $this->em;
        $em->getConnection()->getConfiguration()->setSQLLogger(null);

        $oSpreadsheet = IOFactory::load($fileName);
        $activeSheet = $oSpreadsheet->getActiveSheet();

        $columns = [];
        $columnsWithoutFormattedValue = ['B'];

        $columnIterator = $activeSheet->getColumnIterator();
        foreach ($columnIterator as $column) {
            $cellIterator = $column->getCellIterator();
            if (in_array($column->getColumnIndex(), $columnsWithoutFormattedValue)) {
                foreach ($cellIterator as $cell) {
                    $columns[$cell->getColumn()][] = $cell->getValue();
                }
            } else {
                foreach ($cellIterator as $cell) {
                    $columns[$cell->getColumn()][] = trim($cell->getFormattedValue());
                }
            }
        }

        //Все строки по которые будут записаны в БД
        $recordNumbersColl = array_slice($columns['A'], 3, null, true);
        $recordNumbers = array_filter($recordNumbersColl, function ($value) {
            return !!trim($value);
        });

        //Блок валидации -----------------------------------------------------------------------------------------------

        //Получаем все типы абонентов (для валидации)
        $subscriberTypes = array_slice($columns['E'], 3, null);
        $uniqueSubscriberTypes = array_filter(array_unique($subscriberTypes), function ($value) {
            return (bool)$value;
        });

        //Проверка на корректный тип абонента
        foreach ($uniqueSubscriberTypes as $subTypeTitle) {
            switch ($subTypeTitle) {
                case 'Юридическое лицо':
                case 'Центральный аппарат':
                case 'Физическое лицо':
                case 'Электрон': break;
                default : $bypassListErrors[] = new BypassListRecordError(
                    "Тип абонента $subTypeTitle не существует. Укажите корректный тип абонента. Допустимые типы абонента: 
                    \"Физическое лицо\", \"Юридическое лицо\", \"Центральный аппарат\", \"Электрон\"",
                    "Несуществующий тип абонента"
                );
            }
        }

        //Получаем существующие договора
        $agreementsTitles = array_slice($columns['B'], 3, null);
        $uniqueAgreementTitles = array_filter(array_unique($agreementsTitles), function ($value) {
            return (bool)$value;
        });
        $agreementEntities = $this->agreementHandler->agreementRepository->findByAgreementTitle($uniqueAgreementTitles);
        //Коллекция сущностей "Договор" с номером договора в качестве ключа
        $agreementsTitleCollection = $this->getAgreementsTitleCollection($agreementEntities);

        //Блок сохранения ----------------------------------------------------------------------------------------------

        foreach ($recordNumbers as $rowIndex => $value) {

            /** @var \App\Entity\Main\Company $company */
            $company = $transformer->getCompany();
            /** @var Feeder|null $feeder */
            $feeder = $transformer->getFeeder();
            /** @var Substation|null $substation */
            $substation = $feeder ? $feeder->getSubstation() : null;

            $agreementNumber = $columns['B'][$rowIndex];
            $address = $columns['C'][$rowIndex];
            $subscriberTitle = $columns['D'][$rowIndex];
            $subscriberType = $columns['E'][$rowIndex];
            $phones = $columns['F'][$rowIndex];
            $counterNumber = $columns['G'][$rowIndex];
            $counterModel = $columns['H'][$rowIndex];
            $serviceability = $columns['I'][$rowIndex];
            $lastCounterValueScalar = $columns['J'][$rowIndex];
            $lastCounterValueDate = $columns['K'][$rowIndex];
            $sideSeal = $columns['L'][$rowIndex];
            $terminalSeal = $columns['M'][$rowIndex];
            $antimagneticSeal = $columns['N'][$rowIndex];
            $coords = $columns['O'][$rowIndex];
            $electricLine = $columns['P'][$rowIndex];
            $electricPole = $columns['Q'][$rowIndex];
            $counterInstallationValue = $columns['R'][$rowIndex];
            $counterInstallationDate = $columns['S'][$rowIndex];

            $coordsArr = explode(',', $coords);
            $coordinates = count($coordsArr) < 2 ? null : $coordsArr;
            $counterPhysicalStatus = in_array($serviceability, [1, "1", null, ""]) ? 0 : 1;

            try {
                $em->beginTransaction();

                //Получаем договор и абонента и счётчик. При необходимости создаем
                if (!isset($agreementsTitleCollection[$agreementNumber])) {
                    $subscriber = new Subscriber();
                    $subscriber->setTitle($subscriberTitle);
                    $subscriber->setPhones(explode(',', $phones)?: []);
                    $subscriber->setSubscriberType($this->getSubscriberType($subscriberType));
                    $em->persist($subscriber);

                    $agreement = new Agreement();
                    $agreement->setNumber($agreementNumber);
                    $agreement->setCompany($company);
                    $agreement->setSubscriber($subscriber);
                    $em->persist($agreement);
                    $agreementsTitleCollection[$agreementNumber] = $agreement;

                    //Создаем счётчик
                    $counter = new Counter();
                    $counter->setAgreement($agreement);
                    $counter->setTitle($counterNumber);
                } else {
                    $agreement = $agreementsTitleCollection[$agreementNumber];

                    if (!$counter = $this->getCounterByTitleFromCounters(
                        $counterNumber,
                        $agreement->getCounters()->getValues()
                    )
                    ) {
                        //Создаем счётчик
                        $counter = new Counter();
                        $counter->setAgreement($agreement);
                        $counter->setTitle($counterNumber);
                    } /*else {
                        $counterListErrors[] = new BypassListRecordError(
                            "Счётчик с номером $counterNumber уже существует"
                        );
                        continue;
                    }*/
                }

                //Обновляем или заполняем счётчик
                $counter->setInstallDate($counterInstallationDate ? new DateTimeImmutable($counterInstallationDate): null);
                $counter->setTemporaryAddress($address ?? 'Нет данных');
                $counter->setTemporaryCounterType($counterModel ?? 'Нет данных');
                $counter->setTemporarySubstation($substation ? $substation->getTitle() : 'Нет данных');
                $counter->setTemporaryTransformer($transformer->getTitle());
                $counter->setTemporaryFeeder($feeder ? $feeder->getTitle() : 'Нет данных');
                $counter->setSideSeal($sideSeal ?? 'Нет данных');
                $counter->setAntiMagneticSeal($antimagneticSeal ?? 'Нет данных');
                $counter->setTerminalSeal($terminalSeal ?? 'Нет данных');
                $counter->setElectricLine($electricLine ?? 'Нет данных');
                $counter->setElectricPoleNumber($electricPole ?? 'Нет данных');
                $counter->setWorkStatus($counterPhysicalStatus);
                $counter->setTransformer($transformer);
                $counter->setCoordinates($coordinates);
                $em->persist($counter);

                //Получаем и обновляем значение первичных показаний. При необходимости создаем
                if ($counterInstallationValue) {
                    if (!$initialCounterValue = $this->counterValueHandler->getCounterValueByCounterAndDate(
                        $counter->getId(),
                        $counterInstallationDate ? new DateTimeImmutable($counterInstallationDate) : null
                    )) {
                        $initialCounterValue = new CounterValue();
                        $initialCounterValue->setCounter($counter);
                    }
                    $initialCounterValue->setDate($counterInstallationDate ? new DateTimeImmutable($counterInstallationDate) : null);
                    $initialCounterValue->setValue(new CounterValueType1([floatval($counterInstallationValue)]));
                    $em->persist($initialCounterValue);
                    $counter->setInitialValue($initialCounterValue);
                    $em->persist($counter);
                }

                //Получаем и обновляем значение последнего показания. При необходимости создаем
                $lastCounterValueDateTime = $lastCounterValueDate
                    ? new DateTimeImmutable($lastCounterValueDate)
                    : new DateTimeImmutable();
                if (!$lastCounterValue = $this->counterValueHandler->getCounterValueByCounterAndDate(
                    $counter->getId(),
                    $lastCounterValueDateTime
                )) {
                    $lastCounterValue = new CounterValue();
                    $lastCounterValue->setCounter($counter);
                    $lastCounterValue->setDate($lastCounterValueDateTime);
                }
                $lastCounterValue->setValue(new CounterValueType1([floatval($lastCounterValueScalar) ?? 0]));
                $em->persist($lastCounterValue);

                $em->flush();
                $em->commit();
            } catch (Exception $exception) {
                $em->rollback();
                $counterListErrors[] = new BypassListRecordError(
                    "Ошибка в строке $rowIndex. Данные этой строки не были внесены в систему"
                );
                unset($agreementsTitleCollection[$agreementNumber]);
                continue;
            }

        }


        return $counterListErrors;
    }

    /**
     * @param array $arr
     * @param $counterId
     * @return mixed|null
     */
    private function getLastCounterValueFromResultArray(array $arr, $counterId)
    {
        foreach ($arr as $value) {
            if ($counterId === $value['counter']) {
                return $value[0];
            }
        }
        return null;
    }

    /**
     * A collection of Agreements where agreement number is the key
     * @param \App\Entity\Main\Agreement[] $agreements
     * @return \App\Entity\Main\Agreement[]
     */
    private function getAgreementsTitleCollection(array $agreements): array
    {
        $agreementCollection = [];
        foreach ($agreements as $agreement) {
            $agreementCollection[$agreement->getNumber()] = $agreement;
        }
        return $agreementCollection;
    }

    /**
     * Get type of the subscriber
     * @param string $subscriberTypeString
     * @return int
     */
    private function getSubscriberType(string $subscriberTypeString): int
    {
        switch ($subscriberTypeString) {
            case 'Физическое лицо': return 0;
            case 'Юридическое лицо': return 1;
            case 'Центральный аппарат': return 2;
            case 'Электрон' : return 3;
            default: return 1;
        }
    }

    /**
     * Get Counter by his title
     * @param string $counterTitle
     * @param Counter[] $counters
     * @return Counter|null
     */
    private function getCounterByTitleFromCounters(string $counterTitle, array $counters): ?Counter
    {
        $desiredCounter = null;
        foreach ($counters as $counter) {
            if ($counter->getTitle() == $counterTitle) {
                $desiredCounter = $counter;
                break;
            }
        }
        return $desiredCounter;
    }
}