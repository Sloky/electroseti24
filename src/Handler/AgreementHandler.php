<?php


namespace App\Handler;

use App\Entity\Main\Agreement;
use App\Form\AgreementFormType;
use App\Model\AgreementForm;
use App\Repository\AgreementRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AgreementHandler
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    /**
     * @var AgreementRepository
     */
    public $agreementRepository;

    /**
     * @var PaginatorInterface
     */
    protected $paginator;

    public function __construct(
        FormFactoryInterface $formFactory,
        EntityManagerInterface $entityManager,
        AgreementRepository $agreementRepository,
        PaginatorInterface $paginator
    )
    {
        $this->formFactory = $formFactory;
        $this->em = $entityManager;
        $this->agreementRepository = $agreementRepository;
        $this->paginator = $paginator;
    }

    /**
     * Get all the agreements
     * @return \App\Entity\Main\Agreement[]|null
     */
    public function getAgreements(): ?array
    {
        $agreements = $this->agreementRepository->findAll();
        if (empty($agreements)) {
            return null;
        }
        return $agreements;
    }

    /**
     * @param $id
     * @return Agreement|null
     */
    public function getAgreement($id): ?Agreement
    {
        return $this->agreementRepository->find($id);
    }

    /**
     * Get agreement by number
     * @param $number
     * @return Agreement|null
     */
    public function getAgreementByNumber($number): ?Agreement
    {
        return $this->agreementRepository->findOneBy(['number' => $number]);
    }

    /**
     * Get Agreements with search
     * @param int $page
     * @param int|null $maxResult
     * @param string|null $search
     * @param string|null $subscriberId
     * @return array
     */
    public function getAgreementsWithSearch(
        int $page = 1,
        ?int $maxResult = 100,
        ?string $search = null,
        ?string $subscriberId = null
    ): array
    {
        $agreementsQB = $this->em->getRepository(Agreement::class)->createQueryBuilder('a');

        $agreementsQB
            ->orWhere('a.number LIKE :search')
            ->orWhere('a.paymentNumber LIKE :search')
            ->setParameter('search', '%' . $search . '%')
        ;

        if ($subscriberId) {
            $agreementsQB
                ->leftJoin('a.subscriber', 'subscriber')
                ->andWhere('subscriber.id = :subscriberId')
                ->setParameter('subscriberId', $subscriberId)
            ;
        }

        $agreementsQB->orderBy('a.number');

        if ($maxResult) {
            $pagination = $this->paginator->paginate($agreementsQB, $page, $maxResult);
            $agreements = $pagination->getItems();
            $agreementsCount = $pagination->getTotalItemCount();
        } else {
            /** @var Agreement[] $agreements */
            $agreements = $agreementsQB->getQuery()->execute();
            $agreementsCount = count($agreements);
        }

        return [
            'agreements' => $agreements,
            'agreementsCount' => $agreementsCount
        ];
    }

    /**
     * Add new Agreement
     * @param Request $request
     * @return void
     * @throws Exception
     */
    public function addAgreement(Request $request)
    {
        $agreementForm = new AgreementForm();
        $form = $this->formFactory->create(AgreementFormType::class, $agreementForm);
        $form->submit($request->request->all());
        $form->handleRequest($request);

        if (!$form->isValid()) {
            throw new HttpException(
                Response::HTTP_BAD_REQUEST,
                'Invalid form data'
            );
        }

        if ($this->agreementRepository->findOneBy(['number' => $agreementForm->getNumber()])) {
            throw new HttpException(
                Response::HTTP_BAD_REQUEST,
                "An agreement with number \"{$agreementForm->getNumber()}\" already exists"
            );
        }

        if ($agreementForm->getPaymentCode() && $this->agreementRepository->findOneBy([
            'paymentNumber' => $agreementForm->getPaymentCode()
            ])) {
            throw new HttpException(
                Response::HTTP_BAD_REQUEST,
                "An agreement with payment code \"{$agreementForm->getPaymentCode()}\" already exists"
            );
        }

        try {
            $this->em->beginTransaction();

            $agreement = new Agreement();
            $agreement->setSubscriber($agreementForm->getSubscriber());
            $agreement->setCompany($agreementForm->getCompany());
            $agreement->setNumber($agreementForm->getNumber());
            $agreement->setPaymentNumber($agreementForm->getPaymentCode());

            $this->em->persist($agreement);
            $this->em->flush();
            $this->em->commit();
        } catch (Exception $exception) {
            $this->em->rollback();
            throw $exception;
        }
    }

    /**
     * Edit the Agreement
     * @param Agreement $agreement
     * @param Request $request
     * @return Agreement
     * @throws Exception
     */
    public function editAgreement(Agreement $agreement, Request $request): Agreement
    {
        $agreementForm = new AgreementForm();
        $form = $this->formFactory->create(AgreementFormType::class, $agreementForm);
        $form->submit($request->request->all());
        $form->handleRequest($request);

        if (!$form->isValid()) {
            throw new HttpException(
                Response::HTTP_BAD_REQUEST,
                'Invalid form data'
            );
        }

        if (
            ($agreement->getNumber() !== $agreementForm->getNumber())
            && $this->agreementRepository->findOneBy(['number' => $agreementForm->getNumber()])
        ) {
            throw new HttpException(
                Response::HTTP_BAD_REQUEST,
                "An agreement with number \"{$agreementForm->getNumber()}\" already exists"
            );
        }

        if (
            $agreementForm->getPaymentCode()
            && ($agreement->getPaymentNumber() !== $agreementForm->getPaymentCode())
            && $this->agreementRepository->findOneBy([
            'paymentNumber' => $agreementForm->getPaymentCode()
            ])
        ) {
            throw new HttpException(
                Response::HTTP_BAD_REQUEST,
                "An agreement with payment code \"{$agreementForm->getPaymentCode()}\" already exists"
            );
        }

        try {
            $this->em->beginTransaction();

            $agreement->setCompany($agreementForm->getCompany());
            $agreement->setNumber($agreementForm->getNumber());
            $agreement->setPaymentNumber($agreementForm->getPaymentCode());

            $this->em->persist($agreement);
            $this->em->flush();
            $this->em->commit();
        } catch (Exception $exception) {
            $this->em->rollback();
            throw $exception;
        }
        return $agreement;
    }
}