<?php


namespace App\Handler;


use App\Entity\Main\Address;
use App\Repository\AddressRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;

class AddressHandler
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    /**
     * @var AddressRepository
     */
    protected $addressRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        FormFactoryInterface $formFactory,
        AddressRepository $addressRepository
    )
    {
        $this->em = $entityManager;
        $this->formFactory = $formFactory;
        $this->addressRepository = $addressRepository;
    }

    /**
     * Get the address
     * @param $addressId
     * @return Address|null
     */
    public function getAddress($addressId): ?Address
    {
        return $this->addressRepository->find($addressId);
    }

    /**
     * Get the address by hash
     * @param string $hash
     * @return \App\Entity\Main\Address|null
     */
    public function getAddressByHash(string $hash): ?Address
    {
        return $this->addressRepository->findOneBy(['addressHash' => $hash]);
    }
}