<?php

namespace App\Handler\S3Handler;

use Exception;
use League\Flysystem\FilesystemException;
use League\Flysystem\FilesystemOperator;
use League\Flysystem\FilesystemAdapter;
use League\Flysystem\Visibility;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RequestStack;

class UploadHandler
{
    private FilesystemOperator $filesystem;

    private ContainerInterface $container;

    public function __construct(
        FilesystemOperator $photoStorage,
        ContainerInterface $container
    )
    {
        $this->filesystem = $photoStorage;
        $this->container = $container;
    }

    /**
     * @throws Exception|FilesystemException
     */
    public function uploadPhoto(File $file, $directory, string $fileName, bool $isPublic): string
    {
        return $this->uploadFile($file, $directory, $fileName, $isPublic);
    }

    /**
     * @param string $path
     * @return string
     * @throws FilesystemException
     */
    public function getPublicPath(string $path): string
    {
        $basePath = $this->container->getParameter('uploads_photos_url');
        $fullPath = $basePath.$path;
        if (!$this->filesystem->fileExists($path)) {
            throw new FileNotFoundException();
        }
        return $fullPath;
    }

    /**
     * @return resource
     * @throws Exception
     * @throws FilesystemException
     */
    public function readStream(string $path)
    {
        return $this->filesystem->readStream($path);
    }

    /**
     * @param string $path
     * @return string
     * @throws FilesystemException
     */
    public function readFile(string $path): string
    {
        return $this->filesystem->read($path);
    }

    /**
     * @param string $path
     * @throws FilesystemException
     */
    public function deleteFile(string $path)
    {
        $this->filesystem->delete($path);
    }

    /**
     * @param File $file
     * @param string $directory
     * @param string $newFilename
     * @param bool $isPublic
     * @return string
     * @throws Exception
     * @throws FilesystemException
     */
    private function uploadFile(File $file, string $directory, string $newFilename, bool $isPublic = false): string
    {
        $stream = fopen($file->getPathname(), 'r');
        $this->filesystem->writeStream(
            $directory.'/'.$newFilename,
            $stream,
            [
                'visibility' => $isPublic ? Visibility::PUBLIC : Visibility::PRIVATE
            ]
        );
        if (is_resource($stream)) {
            fclose($stream);
        }
        return $newFilename;
    }
}