<?php

namespace App\Handler\BypassListHandler;

use App\Entity\Main\Agreement;
use App\Entity\Main\BypassListRecord;
use App\Entity\Main\Company;
use App\Entity\Main\Counter;
use App\Entity\Main\Subscriber;
use App\Entity\Main\Task;
use App\Entity\Main\User;
use App\Form\TaskListUploadFormType;
use App\Model\BypassListRecordError;
use App\Model\TaskListUploadForm;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Exception;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Component\HttpFoundation\Request;

class BypassListLoadingHandler extends AbstractBypassListHandler
{
    /**
     * Load bypass list file (new version)
     * @param Request $request
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function loadBypassList(Request $request): ?array
    {
        $form = $this->formFactory->create(TaskListUploadFormType::class, null, ['csrf_protection' => false]);
        $formSendingData = array_merge($request->request->all(), $request->files->all());
        $form->submit($formSendingData);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var TaskListUploadForm $formData */
            $formData = $form->getData();
            $file = $formData->getFile();
            $company = $formData->getCompany();
            $fileName = $file->getPathname();
            return $this->parseBypassList($fileName, $company);
        }
        $errors[] = 'Произошла ошибка при загрузке обходного листа';
        foreach ($form->getErrors(true) as $formErrorIterator) {
            $errors[] = $formErrorIterator->getMessage();
        }
        return $errors;
    }

    /**
     * Parsing and saving data of bypass list
     * @param string $fileName
     * @param Company|null $company
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function parseBypassList(string $fileName, ?Company $company = null): array
    {
        $bypassListErrors = [];
        $recordsCriticalErrors = [];

        $em = $this->em;
        $em->getConnection()->getConfiguration()->setSQLLogger(null);

        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', '600');

        $oSpreadsheet = IOFactory::load($fileName);
        $activeSheet = $oSpreadsheet->getActiveSheet();
        $oCells = $oSpreadsheet->getActiveSheet()->getCellCollection();

        $vedomostPeriod = '01.' . $oCells->get('A2')->getFormattedValue();
        $vedomostPeriodDateTime = (new DateTimeImmutable(trim($vedomostPeriod)))->modify('first day of this month 00:00:00');
//        $deadline = $vedomostPeriodDateTime->modify('+1 month')->modify('+14 day');

        $fileBaseName = basename($fileName);

        $highestRow = $oCells->getHighestRow();
        $columns = [];
        $columnsWithoutFormattedValue = ['B'];

        $columnIterator = $activeSheet->getColumnIterator();
        foreach ($columnIterator as $column) {
            $cellIterator = $column->getCellIterator();
            if (array_search($column->getColumnIndex(), $columnsWithoutFormattedValue) !== false) {
                foreach ($cellIterator as $cell) {
                    $columns[$cell->getColumn()][] = $cell->getValue();
                }
            } else {
                foreach ($cellIterator as $cell) {
                    $columns[$cell->getColumn()][] = trim($cell->getFormattedValue());
                }
            }
        }

        //Все строки по которые будут записаны в БД
        $recordNumbersColl = array_slice($columns['A'], 5, null, true);
        $recordNumbers = array_filter($recordNumbersColl, function ($value) {
            return !!trim($value);
        });

        //Блок валидации -----------------------------------------------------------------------------------------------

        $records = [];

        //Получаем все компании (для валидации)
        $companiesTitles = array_slice($columns['G'], 5, null);
        $uniqueCompanies = array_filter(array_unique($companiesTitles), function ($value) {
            return (bool)$value;
        });
        $companyEntities = $this->companyHandler->companyRepository->findByCompanyTitle($uniqueCompanies);
        $companyTitleCollection = $this->getCompaniesTitleCollection($companyEntities);

        //Проверка на несуществующие отделения (только если при загрузке ОЛ не было выбрано отделение)
        if (!$company) {
            foreach ($uniqueCompanies as $companyTitle) {
                if (!isset($companyTitleCollection[$companyTitle])) {
                    array_push($bypassListErrors, new BypassListRecordError(
                        "Отделение $companyTitle не существует. Вы можете его добавить через меню \"Отделения\"",
                        "Несуществующее отделение"
                    ));
                }
            }
        }

        //Получаем все типы абонентов (для валидации)
        $subscriberTypes = array_slice($columns['E'], 5, null);
        $uniqueSubscriberTypes = array_filter(array_unique($subscriberTypes), function ($value) {
            return (bool)$value;
        });

        //Проверка на корректный тип абонента
        foreach ($uniqueSubscriberTypes as $subTypeTitle) {
            switch ($subTypeTitle) {
                case 'Юридическое лицо':
                case 'Центральный аппарат':
                case 'Физическое лицо':
                case 'Электрон': break;
                default : array_push($bypassListErrors, new BypassListRecordError(
                    "Тип абонента $subTypeTitle не существует. Укажите корректный тип абонента. Допустимые типы абонента: 
                    \"Физическое лицо\", \"Юридическое лицо\", \"Центральный аппарат\", \"Электрон\"",
                    "Несуществующий тип абонента"
                ));
            }
        }

        //Получаем всех исполнителей (контролёров)
        $executorsTitles = array_slice($columns['Z'], 5, null);
        $executorWithCompany = $this->getExecutorWithCompanyArray($executorsTitles, $companiesTitles);
        /** @var \App\Entity\Main\User[] $executorTitleCollection */
        $executorTitleCollection = $this->getUsersCollectionByFullName($executorWithCompany);

        //Проверка на несуществующих исполнителей @todo
        foreach ($executorWithCompany as $executorTitle => $companyBranchesTitles) {
            if (!isset($executorTitleCollection[$executorTitle])) {
                array_push($bypassListErrors, new BypassListRecordError(
                    "Исполнитель $executorTitle не существует. Вы можете его добавить через меню \"Пользователи\"",
                    "Несуществующий исполнитель"
                ));
            }
        }

        //Получаем существующие договора
        $agreementsTitles = array_slice($columns['B'], 5, null);
        $uniqueAgreementTitles = array_filter(array_unique($agreementsTitles), function ($value) {
            return (bool)$value;
        });
        $agreementEntities = $this->agreementHandler->agreementRepository->findByAgreementTitle($uniqueAgreementTitles);
        //Коллекция наименований абонентов с номером договора в качестве ключа
        $subscribersTitleCollection = [];

        //Валидация отдельной записи (Добавляем в $records массив ошибок)
        foreach ($recordNumbers as $rowIndex => $value) {
            $records[$rowIndex]['errors'] = [];

            $agreementNumber = $columns['B'][$rowIndex];
            $address = $columns['C'][$rowIndex];
            $subscriberTitle = $columns['D'][$rowIndex];
            $counterNumber = $columns['H'][$rowIndex];
            $companyTitle = $company ? $company->getTitle() : $columns['G'][$rowIndex];
            $subscriberType = $columns['E'][$rowIndex];
            $counterModel = $columns['I'][$rowIndex];
            $controller = $columns['Z'][$rowIndex];

            //Проверка на несуществующие отделение
            if (!$company && !isset($companyTitleCollection[$companyTitle])) {
                array_push($records[$rowIndex]['errors'], new BypassListRecordError(
                    "Отделение $companyTitle не существует. Вы можете его добавить через меню \"Отделения\"",
                    "Несуществующее отделение"
                ));
            }

            //Проверка на корректный тип абонента
            switch ($subscriberType) {
                case 'Юридическое лицо':
                case 'Центральный аппарат':
                case 'Физическое лицо':
                case 'Электрон': break;
                default : array_push($records[$rowIndex]['errors'], new BypassListRecordError(
                    "Указан несуществующий тип абонента $subscriberType. Укажите корректный тип абонента. 
                    Допустимые типы абонента: 
                    \"Физическое лицо\", \"Юридическое лицо\", \"Центральный аппарат\", \"Электрон\"",
                    "Несуществующий тип абонента"
                ));
            }

            //Проверка на несуществующего исполнителя
            if (!isset($executorTitleCollection[$controller])) {
                $controller && array_push($records[$rowIndex]['errors'], new BypassListRecordError(
                    "Исполнитель $controller не существует. Вы можете его добавить через меню \"Пользователи\"",
                    "Несуществующий исполнитель"
                ));
            } elseif (array_search($companyTitle, $this->getExecutorCompanyTitles($executorTitleCollection[$controller])) === false) {
                array_push($records[$rowIndex]['errors'], new BypassListRecordError(
                    "Исполнитель \"$controller\" не привязан к отделению \"$companyTitle\". Вы можете его добавить через меню \"Пользователи\"",
                    "Несуществующий исполнитель"
                ));
            }

            //Проверка на наличие одинаковых договоров у разных абонентов
            if (isset($subscribersTitleCollection[$agreementNumber])) {
                if ($subscribersTitleCollection[$agreementNumber] !== trim($subscriberTitle)) {
                    $bypassListErrors[] = new BypassListRecordError(
                        "Ошибка в строке " . ($rowIndex + 1) . ". Несколько разных абонентов с одинаковым лицевым счётом/платежным кодом",
                        "Разные абоненты с одинаковым договором/лицевым счётом"
                    );
                    array_push($records[$rowIndex]['errors'], new BypassListRecordError(
                        "В этом обходном листе уже существует запись с таким же лицевым счётом/платежным кодом, но с другим абонентом",
                        "Разные абоненты с одинаковым договором/лицевым счётом"
                    ));
                    $subscribersTitleCollection[$agreementNumber] = "";
                }
            } else {
                $subscribersTitleCollection[$agreementNumber] = trim($subscriberTitle);
            }

            //Проверка на наличие одинаковых УУ в одном договоре (иначе ломается уникальность договор+УУ)
            if (isset($countersCollection[$agreementNumber])) {
                if (array_search($counterNumber, $countersCollection[$agreementNumber]) !== false) {
                    $bypassListErrors[] = new BypassListRecordError(
                        "Ошибка в строке " . ($rowIndex + 1) . ". Несколько одинаковых узлов учета с одинаковым лицевым счётом/платежным кодом",
                        "Одинаковый узел у нескольких записей"
                    );
                    array_push($records[$rowIndex]['errors'], new BypassListRecordError(
                        "В этом обходном листе уже существует запись с таким же узлом учета",
                        "Одинаковый узел у нескольких записей"
                    ));
                    $subscribersTitleCollection[$agreementNumber] = "";
                }
            } else {
                $countersCollection[$agreementNumber][] = $counterNumber;
            }

            //Проверка на пустые обязательные поля
            if (!$agreementNumber) {
                array_push($records[$rowIndex]['errors'], new BypassListRecordError(
                    'Поле "№ Лицевого счета/платежный код" не может быть пустым',
                    'Пустое значимое поле'
                ));
            }
            if (!$address) {
                array_push($records[$rowIndex]['errors'], new BypassListRecordError(
                    'Поле "Адрес объекта" не может быть пустым',
                    'Пустое значимое поле'
                ));
            }
            if (!$subscriberTitle) {
                array_push($records[$rowIndex]['errors'], new BypassListRecordError(
                    'Поле "Наименование абонента" не может быть пустым',
                    'Пустое значимое поле'
                ));
            }
            if (!$counterNumber) {
                array_push($records[$rowIndex]['errors'], new BypassListRecordError(
                    'Поле "Номер счётчика" не может быть пустым',
                    'Пустое значимое поле'
                ));
            }
            if (!$counterModel) {
                array_push($records[$rowIndex]['errors'], new BypassListRecordError(
                    'Поле "Тип (модель) счётчика" не может быть пустым',
                    'Пустое значимое поле'
                ));
            }
            if (!$companyTitle) {
                array_push($records[$rowIndex]['errors'], new BypassListRecordError(
                    'Поле "Наименование отделения" не может быть пустым',
                    'Пустое значимое поле'
                ));
            }
            if (!$subscriberType) {
                array_push($records[$rowIndex]['errors'], new BypassListRecordError(
                    'Поле "Тип абонента" не может быть пустым',
                    'Пустое значимое поле'
                ));
            }
        }

        //Блок сохранения записей --------------------------------------------------------------------------------------

        $batchSize = 400;
        $iterationSize = count($recordNumbers);

        try {
            foreach ($recordNumbers as $rowIndex => $value) {
                $key = $rowIndex - 4;

                $agreementNumber = $columns['B'][$rowIndex];
                $address = $columns['C'][$rowIndex];
                $subscriberTitle = $columns['D'][$rowIndex];
                $subscriberType = $columns['E'][$rowIndex];
                $phones = $columns['F'][$rowIndex];
                $companyTitle = $company ? $company->getTitle() : $columns['G'][$rowIndex];
                $counterNumber = $columns['H'][$rowIndex];
                $counterModel = $columns['I'][$rowIndex];
                $serviceability = $columns['J'][$rowIndex];
                $lastCounterValue = $columns['K'][$rowIndex];
                $lastCounterValueDate = $columns['L'][$rowIndex];
                $seal_1 = $columns['P'][$rowIndex];
                $seal_2 = $columns['Q'][$rowIndex];
                $seal_3 = $columns['R'][$rowIndex];
                $substation = $columns['S'][$rowIndex];
                $feeder = $columns['T'][$rowIndex];
                $transformer = $columns['U'][$rowIndex];
                $electricLine = $columns['V'][$rowIndex];
                $electricPole = $columns['W'][$rowIndex];
                $coords = $columns['X'][$rowIndex];
                $controller = $columns['Z'][$rowIndex];
                $deadline = $columns['AE'][$rowIndex];
                $roomsCount = $columns['AF'][$rowIndex];
                $lodgersCount = $columns['AG'][$rowIndex];
                $counterInstallationDate = $columns['AH'][$rowIndex];
                $counterInstallationValue = $columns['AI'][$rowIndex];

                $bypassListRecord = new BypassListRecord();
                $bypassListRecord->setRowPosition($rowIndex + 1);
                $bypassListRecord->setFileName($fileBaseName);
                $bypassListRecord->setPeriod($vedomostPeriodDateTime);
                $bypassListRecord->setAgreementNumber($agreementNumber);
                $bypassListRecord->setAddress($address);
                $bypassListRecord->setSubscriberTitle($subscriberTitle);
                $bypassListRecord->setSubscriberType($subscriberType);
                $bypassListRecord->setPhones($phones);
                $bypassListRecord->setCompanyTitle($companyTitle);
                $bypassListRecord->setCounterNumber($counterNumber);
                $bypassListRecord->setCounterModel($counterModel);
                $bypassListRecord->setServiceability($serviceability);
                $bypassListRecord->setCurrentCounterValue($lastCounterValue);
                $bypassListRecord->setCurrentCounterValueDate($lastCounterValueDate);
                $bypassListRecord->setTerminalSeal($seal_1);
                $bypassListRecord->setAntimagneticSeal($seal_2);
                $bypassListRecord->setSideSeal($seal_3);
                $bypassListRecord->setSubstation($substation);
                $bypassListRecord->setFeeder($feeder);
                $bypassListRecord->setTransformer($transformer);
                $bypassListRecord->setElectricLine($electricLine);
                $bypassListRecord->setElectricPole($electricPole);
                $bypassListRecord->setCoords($coords);
                $bypassListRecord->setController($controller);
                $bypassListRecord->setDeadline($deadline);
                $bypassListRecord->setRoomsCount($roomsCount);
                $bypassListRecord->setLodgersCount($lodgersCount);
                $bypassListRecord->setCounterInitialDate($counterInstallationDate);
                $bypassListRecord->setCounterInitialValue($counterInstallationValue);

                if (!empty($records[$rowIndex]['errors'])) {
                    $bypassListRecord->setErrors(new ArrayCollection($records[$rowIndex]['errors']));
                }

                $bypassListRecord->setStatus($this->getBypassListRecordStatus($records[$rowIndex]['errors']));

                $em->persist($bypassListRecord);

                if ($key % $batchSize === 0 || $key === $iterationSize) {
                    $em->flush();
                    $em->clear();
                }
            }
        } catch (Exception $exception) {
            $bypassListErrors[] = new BypassListRecordError(
                "При загрузке обходного листа произошла ошибка: {$exception->getMessage()}",
                "Ошибка загрузки обходного листа"
            );
        }

        return $bypassListErrors;
    }

    /**
     * A collection of Subscriber titles where agreement number is the key
     * @param Agreement[] $agreements
     * @return array
     */
    private function getSubscriberTitleCollection(array $agreements): array
    {
        $agreementCollection = [];
        foreach ($agreements as $agreement) {
            $agreementCollection[$agreement->getNumber()] = $agreement->getSubscriber()->getTitle();
        }
        return $agreementCollection;
    }

    /**
     * A collection of Subscribes where subscriber title is the key
     * @param Agreement[] $agreements
     * @return Subscriber[]
     */
    private function getSubscribersTitleCollectionFromAgreements(array $agreements): array
    {
        $subscriberCollection = [];
        foreach ($agreements as $agreement) {
            $subscriber = $agreement->getSubscriber();
            $subscriberCollection[$subscriber->getTitle()] = $subscriber;
        }
        return $subscriberCollection;
    }

    /**
     * Get Counter by his title
     * @param string $counterTitle
     * @param \App\Entity\Main\Counter[] $counters
     * @return \App\Entity\Main\Counter|null
     */
    private function getCounterByTitleFromCounters(string $counterTitle, array $counters): ?Counter
    {
        $desiredCounter = null;
        foreach ($counters as $counter) {
            if ($counter->getTitle() == $counterTitle) {
                $desiredCounter = $counter;
                break;
            }
        }
        return $desiredCounter;
    }

    /**
     * User`s companies (User full name is a key, company array is a value)
     * @param array $executorsTitles
     * @param array $companiesTitles
     * @return array
     */
    private function getExecutorWithCompanyArray(array $executorsTitles, array $companiesTitles): array
    {
        $resultArray = [];
        foreach ($executorsTitles as $key => $title) {
            if (!!$title) {
                if (isset($resultArray[$title])) {
                    if (array_search($companiesTitles[$key], $resultArray[$title]) === false) {
                        $resultArray[$title][] = $companiesTitles[$key];
                    }
                } else {
                    $resultArray[$title][] = $companiesTitles[$key];
                }
            }
        }
        return $resultArray;
    }

    /**
     * @param array $companiesTitles
     * @param Company[] $companies
     * @return \App\Entity\Main\Company[]
     */
    private function getCompaniesArrayByTitles(array $companiesTitles, array $companies): array
    {
        $resultArray = [];
        foreach ($companiesTitles as $title) {
            if (array_key_exists($title, $companies)) {
                $resultArray[] = $companies[$title];
            }
        }
        return $resultArray;
    }

    /**
     * A collection of Companies where company name is the key
     * @param \App\Entity\Main\Company[] $companies
     * @return array
     */
    private function getCompaniesTitleCollection(array $companies): array
    {
        $companyCollection = [];
        foreach ($companies as $company) {
            $companyCollection[$company->getTitle()] = $company;
        }
        return $companyCollection;
    }

    /**
     * A collection of Users where full name is the key
     * @param string[] $companyByUserName
     * @return array
     */
    private function getUsersCollectionByFullName(array $companyByUserName): array
    {
        $usersCollection = [];
        foreach ($companyByUserName as $userFullName => $companyTitle) {
            if ($userFullName) {
//               $user = $this->userHandler->getUserByFullNameAndCompanyTitle($userFullName, $companyTitle);
                $user = $this->userHandler->getUserByFullName($userFullName);
                $user && ($usersCollection[$userFullName] = $user);
            }
        }
        return $usersCollection;
    }

    /**
     * Get type of the subscriber
     * @param string $subscriberTypeString
     * @return int
     */
    private function getSubscriberType(string $subscriberTypeString): int
    {
        switch ($subscriberTypeString) {
            case 'физ. Лицо':
            case 'физ. лицо':
            case 'Физическое лицо':
            case 'fl':
                return 0;
            case 'Юридическое лицо':
                return 1;
            case 'bu':
            case 'Цент.ап':
            case 'Цент.ап.':
            case 'Центральный аппарат':
            case 'Цент. Ап.' :
                return 2;
            case 'Электрон' :
                return 3;
            default:
                return 1;
        }
    }

    /**
     * Get Task by Counter and period
     * @param $counterId
     * @param DateTimeInterface $period
     * @return Task|null
     */
    public function getTaskByCounterAndPeriod($counterId, DateTimeInterface $period): ?Task
    {
        return $this->taskRepository->findOneBy([
            'counter' => $counterId,
            'period' => $period
        ]);
    }

    /**
     * Get User companies titles
     * @param \App\Entity\Main\User $user
     * @return array
     */
    protected function getExecutorCompanyTitles(User $user): array
    {
        $companiesTitles = [];
        /** @var \App\Entity\Main\Company $company */
        foreach ($user->getCompanies()->getValues() as $company) {
            $companiesTitles[] = $company->getTitle();
        }
        return $companiesTitles;
    }

    /**
     * @param array $errors
     * @return int
     */
    protected function getBypassListRecordStatus(array $errors): int
    {
        $status = 0;
        if (!empty($errors)) {
            $status = 2;
        }
        return $status;
    }
}