<?php

namespace App\Handler\BypassListHandler;

use App\Entity\Main\Agreement;
use App\Entity\Main\BypassListRecord;
use App\Entity\Main\Company;
use App\Entity\Main\Counter;
use App\Entity\Main\CounterValue;
use App\Entity\Main\Feeder;
use App\Entity\Main\Subscriber;
use App\Entity\Main\Substation;
use App\Entity\Main\Task;
use App\Entity\Main\Transformer;
use App\Entity\Main\User;
use App\Handler\AddressHandler;
use App\Handler\AgreementHandler;
use App\Handler\CompanyHandler;
use App\Handler\CounterHandler;
use App\Handler\CounterValueHandler;
use App\Handler\PhotoHandler;
use App\Handler\SubscriberHandler;
use App\Handler\TransformerHandler\TransformerHandler;
use App\Handler\UserGroupHandler;
use App\Handler\UserHandler\UserHandler;
use App\Model\BypassListRecordError;
use App\Model\CounterValueType1;
use App\Repository\BypassListRecordRepository;
use App\Repository\TaskRepository;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class BypassListTaskCreationHandler extends AbstractBypassListHandler
{
    private TransformerHandler $transformerHandler;

    public function __construct(
        FormFactoryInterface $formFactory,
        EntityManagerInterface $entityManager,
        TaskRepository $taskRepository,
        BypassListRecordRepository $bypassListRecordRepository,
        CounterHandler $counterHandler,
        SubscriberHandler $subscriberHandler,
        AddressHandler $addressHandler,
        AgreementHandler $agreementHandler,
        UserHandler $userHandler,
        CounterValueHandler $counterValueHandler,
        CompanyHandler $companyHandler,
        PhotoHandler $photoHandler,
        UserGroupHandler $userGroupHandler,
        ParameterBagInterface $parameterBag,
        PaginatorInterface $paginator,
        TransformerHandler $transformerHandler
    )
    {
        parent::__construct(
            $formFactory,
            $entityManager,
            $taskRepository,
            $bypassListRecordRepository,
            $counterHandler,
            $subscriberHandler,
            $addressHandler,
            $agreementHandler,
            $userHandler,
            $counterValueHandler,
            $companyHandler,
            $photoHandler,
            $userGroupHandler,
            $parameterBag,
            $paginator
        );
        $this->transformerHandler = $transformerHandler;
    }

    public function BypassListTasksCreation(
        array $items,
        string $search = null,
        string $searchField = null,
        array  $filter = [],
        int    $status = 0
    ): array
    {
        $filter['status'] = $status;
        $errors = [];

        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', '600');

        /** @var BypassListRecord[] $bypassRecords */
        if (empty($items)) {
            $bypassRecords = $this->bypassListRecordRepository->getBypassListRecordsWithSearchQueryBuilder(
                $search,
                $searchField,
                $filter
            )->getQuery()->execute();
        } else {
            $bypassRecords = $this->bypassListRecordRepository->getBypassRecordsByItemsAndStatus($items, 0);
        }

        if (empty($bypassRecords)) {
            $errors[] = new BypassListRecordError(
                "Невозможно создать задачи, так как отсутствуют записи обходного листа",
                "Нет доступных записей обходного листа"
            );
            return $errors;
        }

        $companies = $this->companyHandler->getCompanies()['companies'] ?? [];
        $agreements = $this->agreementHandler->getAgreements() ?? [];
        $controllers = $this->userHandler->getUsersByGroupName('Controllers') ?? [];

        $companiesTitleCollection = $this->getCompaniesTitleCollection($companies);
        $agreementsTitleCollection = $this->getAgreementsTitleCollection($agreements);
        $controllersCollection = $this->getControllersByCompanyTitleCollection($controllers);

        $em = $this->em;

        //Обходим все записи и создаем задачи
        foreach ($bypassRecords as $record) {
            $em->beginTransaction();
            try {
                //Получаем отделение. При необходимости создаем.
                $company = $companiesTitleCollection[$record->getCompanyTitle()];

                //Получаем исполнителя. При необходимости создаем.
                $executor = null;
                if (isset($controllersCollection[$record->getCompanyTitle()][$record->getController()])) {
                    $executor = $controllersCollection[$record->getCompanyTitle()][$record->getController()];
                }

                $subscriber = null;
                $agreement = null;
                $counter = null;
                $lastCounterValue = null;


                //Получаем договор и абонента и счётчик. При необходимости создаем
                if (!isset($agreementsTitleCollection[$record->getAgreementNumber()])) {
                    $subscriber = $this->createSubscriberFromRecord($record);
                    $em->persist($subscriber);

                    $agreement = $this->createAgreementFromRecord($record, $subscriber, $company);
                    $em->persist($agreement);
                    $agreementsTitleCollection[$record->getAgreementNumber()] = $agreement;

                    //Создаем счётчик
                    $counter = new Counter();
                    $counter->setAgreement($agreement);
                    $counter->setTitle($record->getCounterNumber());
                } else {
                    $agreement = $agreementsTitleCollection[$record->getAgreementNumber()];
                    $subscriber = $agreement->getSubscriber();

                    if (!$counter = $this->getCounterByTitleFromCounters(
                        $record->getCounterNumber(),
                        $agreement->getCounters()->getValues()
                    )
                    ) {
                        //Создаем счётчик
                        $counter = new Counter();
                        $counter->setAgreement($agreement);
                        $counter->setTitle($record->getCounterNumber());
                    }
                }

                //Создаем сетевые объекты
                if (!$transformer = $this->transformerHandler->getTransformerByTitle($record->getTransformer())) {
                    if ((!$feeder = $this->transformerHandler->getFeederByTitle($record->getFeeder()))
                    && $record->getFeeder()) {
                        if ((!$substation = $this->transformerHandler->getSubstationByTitle($record->getSubstation()))
                        && $record->getSubstation()) {
                            $substation = new Substation();
                            $substation->setTitle($record->getSubstation());
                            $em->persist($substation);
                        }
                        $feeder = new Feeder();
                        $feeder->setTitle($record->getFeeder());
                        $feeder->setSubstation($substation);
                        $em->persist($feeder);
                    }
                    $transformer = new Transformer();
                    $transformer->setTitle($record->getTransformer());
                    $transformer->setFeeder($feeder);
                    $transformer->setCompany($company);
                    $em->persist($feeder);
                }


                    //Обновляем или заполняем счётчик
                    $counter->setInstallDate($record->getCounterInitialDate() ? new DateTimeImmutable($record->getCounterInitialDate()) : null);
                $counter->setTemporaryAddress($record->getAddress() ?? 'Нет данных');
                $counter->setTemporaryCounterType($record->getCounterModel() ?? 'Нет данных');
                $counter->setTemporarySubstation($record->getSubstation() ?? 'Нет данных');
                $counter->setTemporaryTransformer($record->getTransformer() ?? 'Нет данных');
                $counter->setTemporaryFeeder($record->getFeeder() ?? 'Нет данных');
                $counter->setTransformer($transformer);
                $counter->setSideSeal($record->getSideSeal() ?? 'Нет данных');
                $counter->setAntiMagneticSeal($record->getAntimagneticSeal() ?? 'Нет данных');
                $counter->setTerminalSeal($record->getTerminalSeal() ?? 'Нет данных');
                $em->persist($counter);

                //Получаем и обновляем значение последнего показания. При необходимости создаем
                $lastCounterValueDate = $record->getCurrentCounterValueDate()
                    ? new DateTimeImmutable($record->getCurrentCounterValueDate())
                    : new DateTimeImmutable();
                if (!$lastCounterValue = $this->counterValueHandler->getCounterValueByCounterAndDate(
                    $counter->getId(),
                    $lastCounterValueDate
                )) {
                    $lastCounterValue = new CounterValue();
                    $lastCounterValue->setCounter($counter);
                    $lastCounterValue->setDate($lastCounterValueDate);
                }
                $lastCounterValue->setValue(new CounterValueType1([$record->getCurrentCounterValue() ?? 0]));
                $em->persist($lastCounterValue);

                //Получаем и обновляем задачу. При необходимости создаем
                if (!$task = $this->getTaskByCounterAndPeriod($counter->getId(), $record->getPeriod())) {
                    $task = new Task();
                    $task->setCounter($counter);
                    $task->setAgreement($agreement);
                    $task->setCompany($company);

                }
                $task->setSubscriber($subscriber);
                $task->setLastCounterValue($lastCounterValue);
                $task->setSubscriber($subscriber);
                $task->setPeriod($record->getPeriod());
                $task->setStatus(0);
                $task->setTaskType(0);
                $task->setExecutor($executor);
                $task->setDeadline(new DateTimeImmutable($record->getDeadline()));
                $em->persist($task);

                //Удаляем запись ОЛ
                $em->remove($record);

                $em->flush();
                $em->commit();
            } catch (Exception $exception) {
                $em->rollback();
                $errors[] = new BypassListRecordError(
                    "Номер строки записи обходного листа {$record->getRowPosition()}. {$exception->getMessage()}",
                    "Не удалось создать задачу из записи"
                );
            }
        }

        return $errors;
    }

    /**
     * @param \App\Entity\Main\BypassListRecord $record
     * @throws Exception
     */
    public function bypassRecordTaskCreation(BypassListRecord $record)
    {
        if ($record->getStatus() === 2) {
            throw new HttpException(
                Response::HTTP_BAD_REQUEST,
                'Невозможно создать задачу, так как запись имеет критические ошибки'
            );
        }
        $em = $this->em;

        $em->beginTransaction();

        try {
            //Получаем отделение. При необходимости создаем.
            $company = $this->companyHandler->getCompanyByTitle($record->getCompanyTitle());

            //Получаем исполнителя.
            $executor = $this->userHandler->getUserByFullNameAndCompanyTitle(
                $record->getController(),
                $record->getCompanyTitle()
            );

            //Получаем договор и абонента и счётчик. При необходимости создаем
            if (!$agreement = $this->agreementHandler->getAgreementByNumber($record->getAgreementNumber())) {
                $subscriber = $this->createSubscriberFromRecord($record);
                $em->persist($subscriber);

                $agreement = $this->createAgreementFromRecord($record, $subscriber, $company);
                $em->persist($agreement);

                //Создаем счётчик
                $counter = new Counter();
                $counter->setAgreement($agreement);
                $counter->setTitle($record->getCounterNumber());
            } else {
                $subscriber = $agreement->getSubscriber();

                if (!$counter = $this->getCounterByTitleFromCounters(
                    $record->getCounterNumber(),
                    $agreement->getCounters()->getValues()
                )
                ) {
                    //Создаем счётчик
                    $counter = new Counter();
                    $counter->setAgreement($agreement);
                    $counter->setTitle($record->getCounterNumber());
                }
            }

            //Обновляем или заполняем счётчик
            $counter->setInstallDate($record->getCounterInitialDate() ? new DateTimeImmutable($record->getCounterInitialDate()) : null);
            $counter->setTemporaryAddress($record->getAddress() ?? 'Нет данных');
            $counter->setTemporaryCounterType($record->getCounterModel() ?? 'Нет данных');
            $counter->setTemporarySubstation($record->getSubstation() ?? 'Нет данных');
            $counter->setTemporaryTransformer($record->getTransformer() ?? 'Нет данных');
            $counter->setTemporaryFeeder($record->getFeeder() ?? 'Нет данных');
            $counter->setSideSeal($record->getSideSeal() ?? 'Нет данных');
            $counter->setAntiMagneticSeal($record->getAntimagneticSeal() ?? 'Нет данных');
            $counter->setTerminalSeal($record->getTerminalSeal() ?? 'Нет данных');
            $em->persist($counter);

            //Получаем и обновляем значение последнего показания. При необходимости создаем
            $lastCounterValueDate = $record->getCurrentCounterValueDate()
                ? new DateTimeImmutable($record->getCurrentCounterValueDate())
                : new DateTimeImmutable();
            if (!$lastCounterValue = $this->counterValueHandler->getCounterValueByCounterAndDate(
                $counter->getId(),
                $lastCounterValueDate
            )) {
                $lastCounterValue = new CounterValue();
                $lastCounterValue->setCounter($counter);
                $lastCounterValue->setDate($lastCounterValueDate);
            }
            $lastCounterValue->setValue(new CounterValueType1([$record->getCurrentCounterValue() ?? 0]));
            $em->persist($lastCounterValue);

            //Получаем и обновляем задачу. При необходимости создаем
            if (!$task = $this->getTaskByCounterAndPeriod($counter->getId(), $record->getPeriod())) {
                $task = new Task();
                $task->setCounter($counter);
                $task->setAgreement($agreement);
                $task->setCompany($company);

            }
            $task->setSubscriber($subscriber);
            $task->setLastCounterValue($lastCounterValue);
            $task->setSubscriber($subscriber);
            $task->setPeriod($record->getPeriod());
            $task->setStatus(0);
            $task->setTaskType(0);
            $task->setExecutor($executor);
            $task->setDeadline(new DateTimeImmutable($record->getDeadline()));
            $em->persist($task);

            //Удаляем запись ОЛ
            $em->remove($record);

            $em->flush();
            $em->commit();
        } catch (Exception $exception) {
            $em->rollback();
            throw $exception;
        }
    }

    /**
     * Get Task by Counter and period
     * @param $counterId
     * @param DateTimeInterface $period
     * @return Task|null
     */
    private function getTaskByCounterAndPeriod($counterId, DateTimeInterface $period): ?Task
    {
        return $this->taskRepository->findOneBy([
            'counter' => $counterId,
            'period' => $period
        ]);
    }

    /**
     * A collection of Companies where company name is the key
     * @param \App\Entity\Main\Company[] $companies
     * @return \App\Entity\Main\Company[]
     */
    private function getCompaniesTitleCollection(array $companies): array
    {
        $companyCollection = [];
        foreach ($companies as $company) {
            $companyCollection[$company->getTitle()] = $company;
        }
        return $companyCollection;
    }

    /**
     * A collection of Agreements where agreement number is the key
     * @param \App\Entity\Main\Agreement[] $agreements
     * @return \App\Entity\Main\Agreement[]
     */
    private function getAgreementsTitleCollection(array $agreements): array
    {
        $agreementCollection = [];
        foreach ($agreements as $agreement) {
            $agreementCollection[$agreement->getNumber()] = $agreement;
        }
        return $agreementCollection;
    }

    /**
     * A two level collection of Controllers, where first key is  company name and second key is Controller title
     * @param \App\Entity\Main\User[] $controllers
     * @return \App\Entity\Main\User[][]
     */
    private function getControllersByCompanyTitleCollection(array $controllers): array
    {
        $controllersCollection = [];
        foreach ($controllers as $controller) {
            $controllerFullName = $controller->getSurname() . ' ' . $controller->getName() . ' ' . $controller->getPatronymic();
            /** @var \App\Entity\Main\Company $company */
            foreach ($controller->getCompanies()->getValues() as $company) {
                $controllersCollection[$company->getTitle()][$controllerFullName] = $controller;
            }
        }
        return $controllersCollection;
    }

    /**
     * Get Counter by his title
     * @param string $counterTitle
     * @param \App\Entity\Main\Counter[] $counters
     * @return \App\Entity\Main\Counter|null
     */
    private function getCounterByTitleFromCounters(string $counterTitle, array $counters): ?Counter
    {
        $desiredCounter = null;
        foreach ($counters as $counter) {
            if ($counter->getTitle() == $counterTitle) {
                $desiredCounter = $counter;
                break;
            }
        }
        return $desiredCounter;
    }

    /**
     * Create Subscriber
     * @param BypassListRecord $record
     * @return Subscriber
     */
    private function createSubscriberFromRecord(BypassListRecord $record): Subscriber
    {
        $subscriberPhones = explode(',', $record->getPhones());
        $subscriber = new Subscriber();
        $subscriber->setTitle($record->getSubscriberTitle());
        $subscriber->setSubscriberType($this->getSubscriberType($record->getSubscriberType()));
        $subscriber->setPhones($subscriberPhones ?: []);
        return $subscriber;
    }

    /**
     * Create Agreement
     * @param \App\Entity\Main\BypassListRecord $record
     * @param Subscriber $subscriber
     * @param \App\Entity\Main\Company $company
     * @return Agreement
     */
    private function createAgreementFromRecord(BypassListRecord $record, Subscriber $subscriber, Company $company): Agreement
    {
        $agreement = new Agreement();
        $agreement->setSubscriber($subscriber);
        $agreement->setCompany($company);
        $agreement->setNumber($record->getAgreementNumber());
        return $agreement;
    }

    private function createCounterFromRecord(BypassListRecord $record): Counter
    {
        $counter = new Counter();
        return $counter;
    }

    private function createLastCounterValueFromRecord(BypassListRecord $record): CounterValue
    {
        $counterValue = new CounterValue();
        return $counterValue;
    }

    /**
     * Get type of the subscriber
     * @param string $subscriberTypeString
     * @return int
     */
    private function getSubscriberType(string $subscriberTypeString): int
    {
        switch ($subscriberTypeString) {
            case 'Физическое лицо':
                return 0;
            case 'Юридическое лицо':
                return 1;
            case 'Центральный аппарат':
                return 2;
            case 'Электрон' :
                return 3;
            default:
                return 1;
        }
    }
}