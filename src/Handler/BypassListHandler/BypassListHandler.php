<?php

namespace App\Handler\BypassListHandler;

use App\Entity\Main\BypassListRecord;
use App\Entity\Main\Company;
use App\Entity\Main\CounterValue;
use App\Entity\Main\Subscriber;
use App\Entity\Main\User;
use App\Form\BypassListEditFormType;
use App\Form\BypassRecordsMultiEditFormType;
use App\Model\BypassListRecordError;
use App\Model\BypassRecordsMultiEditForm;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Exception;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class BypassListHandler extends AbstractBypassListHandler
{
    /**
     * Get bypass list records
     * @param int $page
     * @param int|null $maxResult
     * @param string|null $search
     * @param string|null $searchField
     * @param array $filter
     * @return array
     */
    public function getBypassListRecordsList(
        int    $page = 1,
        int    $maxResult = null,
        string $search = null,
        string $searchField = null,
        array  $filter = []
    ): array
    {
        /** @var User $user */
        $user = $this->userHandler->getCurrentUser();
        $operator = $this->userHandler->isUserAdmin($user) ? null : $user;
        $bypassListRecordsQuery = $this->bypassListRecordRepository->getBypassListRecordsWithSearchQueryBuilder(
            $search,
            $searchField,
            $filter,
            $operator
        );

        if ($maxResult) {
            $pagination = $this->paginator->paginate($bypassListRecordsQuery, $page, $maxResult);
            $records = $pagination->getItems();
            $recordsCount = $pagination->getTotalItemCount();
        } else {
            /** @var BypassListRecord[] $bypassListRecords */
            $records = $bypassListRecordsQuery->getQuery()->execute();
            $recordsCount = count($records);
        }

        return [
            'bypassListRecordList' => $records,
            'bypassListRecordCount' => $recordsCount
        ];
    }

    /**
     * Get the bypass list record
     * @param $id
     * @return \App\Entity\Main\BypassListRecord|null
     */
    public function getBypassListRecord($id): ?BypassListRecord
    {
        return $this->bypassListRecordRepository->find($id);
    }

    /**
     * Edit the bypass list record
     * @param Request $request
     * @param \App\Entity\Main\BypassListRecord $bypassListRecord
     * @return \App\Entity\Main\BypassListRecord
     */
    public function editBypassListRecord(Request $request, BypassListRecord $bypassListRecord): BypassListRecord
    {
        $form = $this->formFactory->create(BypassListEditFormType::class, $bypassListRecord, ['csrf_protection' => false]);
        $form->submit($request->request->all());
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->em->persist($this->validateErrorsBypassListRecord($bypassListRecord));
            $this->em->flush();
        } else {
            throw new HttpException(Response::HTTP_BAD_REQUEST, 'Incorrect form data');
        }
        return $bypassListRecord;
    }

    /**
     * Delete the bypass list record
     * @param $id
     * @return bool
     */
    public function deleteBypassListRecord($id): bool
    {
        try {
            $this->bypassListRecordRepository->deleteBypassListRecord($id);
        } catch (Exception $exception) {
            return false;
        }
        return true;
    }

    /**
     * Delete all the bypass list records by filter and search
     * @param array $items
     * @param string|null $search
     * @param string|null $searchField
     * @param array $filter
     * @return bool
     */
    public function deleteBypassListRecords(
        array $items,
        string $search = null,
        string $searchField = null,
        array  $filter = []
    ): bool
    {
        try {
            $this->bypassListRecordRepository->deleteBypassListRecords($items, $search, $searchField, $filter);
        } catch (Exception $exception) {
            return false;
        }
        return true;
    }

    /**
     * @param \App\Entity\Main\BypassListRecord $bypassListRecord
     * @param \App\Entity\Main\User|null $persistentController
     * @param Company|null $persistentCompany
     * @param string $operationType
     * @return BypassListRecord
     */
    public function validateErrorsBypassListRecord(
        BypassListRecord $bypassListRecord,
        User             $persistentController = null,
        Company          $persistentCompany = null,
        string $operationType = 'userUpdate'
    ): BypassListRecord
    {
        $errors = [];

        $agreementNumber = $bypassListRecord->getAgreementNumber();
        $address = $bypassListRecord->getAddress();
        $subscriberTitle = $bypassListRecord->getSubscriberTitle();
        $counterNumber = $bypassListRecord->getCounterNumber();
        $companyTitle = $bypassListRecord->getCompanyTitle();
        $subscriberType = $bypassListRecord->getSubscriberType();
        $counterModel = $bypassListRecord->getCounterModel();
        $controller = $bypassListRecord->getController();

        $executor = null;
        $executorFullName = null;
        $company = null;

        if (!$persistentController) {
            $executor = $this->userHandler->getUserByFullName($controller);
        } else {
            $executor = $persistentController;
        }

        if ($executor) {
            $executorFullName = $executor->getSurname()
                . ' ' . $executor->getName()
                . ' ' . $executor->getPatronymic();
        }

        if (!$persistentCompany) {
            $company = $this->companyHandler->getCompanyByTitle($companyTitle);
        } else {
            $company = $persistentCompany;
        }

        //Проверка на несуществующие отделение
        if ($companyTitle) {
            if ($operationType === 'companyDelete' || is_null($company) || $company->getTitle() !== $companyTitle) {
                array_push($errors, new BypassListRecordError(
                    "Отделение $companyTitle не существует. Вы можете его добавить через меню \"Отделения\"",
                    "Несуществующее отделение"
                ));
            }
        }

        //Проверка на корректный тип абонента
        switch ($subscriberType) {
            case 'Юридическое лицо':
            case 'Центральный аппарат':
            case 'Физическое лицо':
            case 'Электрон':
                break;
            default :
                array_push($errors, new BypassListRecordError(
                    "Указан несуществующий тип абонента $subscriberType. Укажите корректный тип абонента.
                    Допустимые типы абонента: 
                    \"Физическое лицо\", \"Юридическое лицо\", \"Центральный аппарат\", \"Электрон\"",
                    "Несуществующий тип абонента"
                ));
        }


        //Проверка на несуществующего исполнителя
        if ($controller) {
            if ($operationType === 'userDelete' || (is_null($executor) || ($executorFullName !== $controller))) {
                array_push($errors, new BypassListRecordError(
                    "Исполнитель $controller не существует. Вы можете его добавить через меню \"Пользователи\"",
                    "Несуществующий исполнитель"
                ));
            } elseif (array_search(
                    $companyTitle,
                    $this->getCompaniesTitles($executor->getCompanies()->getValues()
                    )) === false) {
                array_push($errors, new BypassListRecordError(
                    "Исполнитель \"$controller\" не привязан к отделению \"$companyTitle\". Вы можете его добавить через меню \"Пользователи\"",
                    "Несуществующий исполнитель"
                ));
            }
        }

        //Проверка на наличие одинаковых договоров у разных абонентов
        /*if (isset($subscribersTitleCollection[$agreementNumber])) {
            if ($subscribersTitleCollection[$agreementNumber] !== trim($subscriberTitle)) {
                $bypassListErrors[] = new BypassListRecordError(
                    "Ошибка в строке " . ($rowIndex + 1) . ". Несколько разных абонентов с одинаковым лицевым счётом/платежным кодом",
                    "Разные абоненты с одинаковым договором/лицевым счётом"
                );
                array_push($records[$rowIndex]['errors'], new BypassListRecordError(
                    "В этом обходном листе уже существует запись с таким же лицевым счётом/платежным кодом, но с другим абонентом",
                    "Разные абоненты с одинаковым договором/лицевым счётом"
                ));
                $subscribersTitleCollection[$agreementNumber] = "";
            }
        } else {
            $subscribersTitleCollection[$agreementNumber] = trim($subscriberTitle);
        }

        //Проверка на наличие одинаковых УУ в одном договоре (иначе ломается уникальность договор+УУ)
        if (isset($countersCollection[$agreementNumber])) {
            if (array_search($counterNumber, $countersCollection[$agreementNumber]) !== false) {
                $bypassListErrors[] = new BypassListRecordError(
                    "Ошибка в строке " . ($rowIndex + 1) . ". Несколько одинаковых узлов учета с одинаковым лицевым счётом/платежным кодом",
                    "Одинаковый узел у нескольких записей"
                );
                array_push($records[$rowIndex]['errors'], new BypassListRecordError(
                    "В этом обходном листе уже существует запись с таким же узлом учета",
                    "Одинаковый узел у нескольких записей"
                ));
                $subscribersTitleCollection[$agreementNumber] = "";
            }
        } else {
            $countersCollection[$agreementNumber][] = $counterNumber;
        }*/

        //Проверка на пустые обязательные поля
        if (!$agreementNumber) {
            array_push($errors, new BypassListRecordError(
                'Поле "№ Лицевого счета/платежный код" не может быть пустым',
                'Пустое значимое поле'
            ));
        }
        if (!$address) {
            array_push($errors, new BypassListRecordError(
                'Поле "Адрес объекта" не может быть пустым',
                'Пустое значимое поле'
            ));
        }
        if (!$subscriberTitle) {
            array_push($errors, new BypassListRecordError(
                'Поле "Наименование абонента" не может быть пустым',
                'Пустое значимое поле'
            ));
        }
        if (!$counterNumber) {
            array_push($errors, new BypassListRecordError(
                'Поле "Номер счётчика" не может быть пустым',
                'Пустое значимое поле'
            ));
        }
        if (!$counterModel) {
            array_push($errors, new BypassListRecordError(
                'Поле "Тип (модель) счётчика" не может быть пустым',
                'Пустое значимое поле'
            ));
        }
        if (!$companyTitle) {
            array_push($errors, new BypassListRecordError(
                'Поле "Наименование отделения" не может быть пустым',
                'Пустое значимое поле'
            ));
        }
        if (!$subscriberType) {
            array_push($errors, new BypassListRecordError(
                'Поле "Тип абонента" не может быть пустым',
                'Пустое значимое поле'
            ));
        }

        if (empty($errors)) {
            $bypassListRecord->setErrors(new ArrayCollection());
            $bypassListRecord->setStatus(0);
        } else {
            $bypassListRecord->setErrors(new ArrayCollection($errors));
            $bypassListRecord->setStatus(2);
        }

        return $bypassListRecord;
    }

    /**
     * Multi edit handler
     * @param Request $request
     * @param string|null $search
     * @param string|null $searchField
     * @param array $filter
     * @param int $page
     * @param int|null $maxResult
     */
    public function recordsMultiEdit(
        Request $request,
        string $search = null,
        string $searchField = null,
        array  $filter = [],
        int $page = 1,
        int $maxResult = null
    ): void
    {
        $batchSize = 100;

        $bypassRecordsMultiEditForm = new BypassRecordsMultiEditForm();
        $form = $this->formFactory->create(
            BypassRecordsMultiEditFormType::class,
            $bypassRecordsMultiEditForm
        );
        $form->submit($request->request->all());
        $form->handleRequest($request);

        if ($form->isValid()) {
            if ($bypassRecordsMultiEditForm->getItems() && !empty($bypassRecordsMultiEditForm->getItems()->getValues())) {
                $records = $this->bypassListRecordRepository->getBypassRecordsByItems($bypassRecordsMultiEditForm->getItems()->getValues());
            } else {
                $bypassListRecordsQuery = $this->bypassListRecordRepository->getBypassListRecordsWithSearchQueryBuilder(
                    $search,
                    $searchField,
                    $filter
                );
                if ($maxResult) {
                    if ($page) {
                        $bypassListRecordsQuery->setFirstResult($page > 1 ? ($page-1)*$maxResult : 0);
                    }
                    $bypassListRecordsQuery->setMaxResults($maxResult);
                }
                $records = $bypassListRecordsQuery->getQuery()->execute();
            }

            /** @var \App\Entity\Main\BypassListRecord[] $records */
            if (empty($records))
            {
                throw new HttpException(
                    Response::HTTP_BAD_REQUEST,
                    'Records not found. Invalid search or filter or array of items'
                );
            }

            foreach ($records as $key => $record) {
                if (!is_null($bypassRecordsMultiEditForm->getDeadline())) {
                    $record->setDeadline($bypassRecordsMultiEditForm->getDeadline());
                }
                if (!is_null($bypassRecordsMultiEditForm->getExecutor())) {
                    $record->setController($bypassRecordsMultiEditForm->getExecutor());
                    $company = $this->companyHandler->getCompanyByTitle($record->getCompanyTitle());
                    $controller = $this->userHandler->getUserByFullNameAndCompanyTitle(
                        $bypassRecordsMultiEditForm->getExecutor(),
                        $record->getCompanyTitle()
                    );
                    $record = $this->validateErrorsBypassListRecord(
                        $record,
                        $controller,
                        $company
                    );
                }

                $this->em->persist($record);
                if (($key + 1)%$batchSize === 0) {
                    $this->em->flush();
                }
            }
            $this->em->flush();
        } else {
            throw new HttpException(
                Response::HTTP_BAD_REQUEST,
                'Incorrect form data'
            );
        }
    }

    public function createBypassList()
    {
        $counters = $this->counterHandler->getCounters();

        $period = new DateTimeImmutable();

        $sOutFile = ".\public\Bypass_list".$period->format('Y-m').".xlsx";

        $fileName = $this->parameterBag->get('tasks_excel_template_path');
        $oSpreadsheet_Out = IOFactory::load($fileName);

        $oSpreadsheet_Out->getProperties()->setCreator('TestUser')
            ->setLastModifiedBy('Tasks data')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Task data ')
        ;

        $oSpreadsheet_Out->setActiveSheetIndex(0)
            ->setCellValue("A2", $period->format('m.Y'));

        $activeSheet = $oSpreadsheet_Out->getActiveSheet();

        $columns = array(
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
            'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI',
            'AJ', 'AK', 'AL', 'AM'
        );

        foreach ($columns as $column) {
            $activeSheet->getColumnDimension($column)->setAutoSize(true);
        }

        foreach ($counters as $key => $counter) {
            $i = intval($key);
            $counterNumber = $counter->getTitle();
            $agreementNumber = $counter->getAgreement()->getNumber();
            $address = $counter->getTemporaryAddress();
            $subscriber = $counter->getAgreement()->getSubscriber();
            $subscriberTitle = $subscriber->getTitle();
            $subscriberType = $this->my_mb_ucfirst(Subscriber::$subscriber_type_value[$subscriber->getSubscriberType()]);
            $subscriberPhones = implode(',', $subscriber->getPhones());
            $counterPhysicalStatus = $counter->getWorkStatus() === 0 ? 'Исправен/Доступен' : 'Неисправен/Недоступен';

            $company = $counter->getAgreement()->getCompany();

            /** @var \App\Entity\Main\CounterValue|null $lastCounterValue */
            $lastCounterValue = $this->em->getRepository(CounterValue::class)->findOneBy(
                ['counter' => $counter],
                ['date' => 'DESC']
            );

            /** @var User|null $executor */
            $executor = $lastCounterValue->getInspector();
            $executorTitle = '';
            if ($executor) {
                $executorRoles = $executor->getRoles();

                if (in_array('ROLE_CONTROLLER', $executorRoles)) {
                    $executorTitle = $executor->getSurname() .' '.$executor->getName().' '.$executor->getPatronymic();
                }
            }

            $lastCounterValueStr = $lastCounterValue ? $lastCounterValue->getValue()->getValues()[0] : '';
            $lastCounterValueStrDate = $lastCounterValue ? $lastCounterValue->getDate()->format('Y-m-d H:i:s') : '';

            $counterModel = '';
            $currentValue = '';
            $currentValueDate = '';
            $terminalSeal = $counter->getTerminalSeal();
            $antimagneticSeal = $counter->getAntiMagneticSeal();
            $sideSeal = $counter->getSideSeal();
            $substation = $counter->getTemporarySubstation();
            $feeder = $counter->getTemporaryFeeder();
            $transformer = $counter->getTemporaryTransformer();
            $electricPole = $counter->getElectricPoleNumber();
            $electricLine = $counter->getElectricLine();

            $activeSheet
                ->setCellValue('A'.($i+6), $i+1)
                ->setCellValue('B'.($i+6), '="'.$agreementNumber.'"')
                ->setCellValue('C'.($i+6), $address ?? '')
                ->setCellValue('D'.($i+6), $subscriberTitle)
                ->setCellValue('E'.($i+6), $subscriberType)
                ->setCellValue('F'.($i+6), $subscriberPhones)
                ->setCellValue('G'.($i+6), $company)
                ->setCellValue('H'.($i+6), '="'.$counterNumber.'"')
                ->setCellValue('I'.($i+6), $counter->getTemporaryCounterType() ?? '')
                ->setCellValue('J'.($i+6), $counterPhysicalStatus)
                ->setCellValue('K'.($i+6), $lastCounterValueStr)
                ->setCellValue('L'.($i+6), $lastCounterValueStrDate)
                ->setCellValue('P'.($i+6), $terminalSeal ?? '')
                ->setCellValue('Q'.($i+6), $antimagneticSeal ?? '')
                ->setCellValue('R'.($i+6), $sideSeal ?? '')
                ->setCellValue('S'.($i+6), $substation ?? '')
                ->setCellValue('T'.($i+6), $feeder ?? '')
                ->setCellValue('U'.($i+6), $transformer ?? '')
                ->setCellValue('V'.($i+6), $electricLine ?? '')
                ->setCellValue('W'.($i+6), $electricPole ?? '')
                ->setCellValue('Z'.($i+6), $executorTitle ?? '')
            ;

            $activeSheet->getStyle('B'.($i+6).':C'.($i+6))->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_TEXT);
            $activeSheet->getStyle('K'.($i+6))->getNumberFormat()->setFormatCode('0.0000000');
            $activeSheet->getStyle('M'.($i+6))->getNumberFormat()->setFormatCode('0.0000000');
            $activeSheet->getStyle('O'.($i+6))->getNumberFormat()->setFormatCode('0.0000000');

            $activeSheet->getStyle('F'.($i+6))->getAlignment()->setHorizontal('right');
            $activeSheet->getStyle('P'.($i+6))->getAlignment()->setHorizontal('left');
            $activeSheet->getStyle('Q'.($i+6))->getAlignment()->setHorizontal('left');
            $activeSheet->getStyle('R'.($i+6))->getAlignment()->setHorizontal('left');
        }
        $oWriter = IOFactory::createWriter($oSpreadsheet_Out, 'Xlsx');
        $oWriter->save($sOutFile);

        return 1;
    }

    /**
     * @param $str
     * @return string
     */
    private function my_mb_ucfirst($str): string
    {
        $fc = mb_strtoupper(mb_substr($str, 0, 1));
        return $fc.mb_substr($str, 1);
    }

    /**
     * @param Company[] $companies
     * @return array
     */
    private function getCompaniesTitles(array $companies): array
    {
        return array_map(function (Company $company) {
            return $company->getTitle();
        }, $companies);
    }
}