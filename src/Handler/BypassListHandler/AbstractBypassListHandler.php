<?php

namespace App\Handler\BypassListHandler;

use App\Handler\AddressHandler;
use App\Handler\AgreementHandler;
use App\Handler\CompanyHandler;
use App\Handler\CounterHandler;
use App\Handler\CounterValueHandler;
use App\Handler\PhotoHandler;
use App\Handler\SubscriberHandler;
use App\Handler\UserGroupHandler;
use App\Handler\UserHandler\UserHandler;
use App\Repository\BypassListRecordRepository;
use App\Repository\TaskRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\FormFactoryInterface;

abstract class AbstractBypassListHandler
{
    /**
     * @var EntityManagerInterface
     */
    protected EntityManagerInterface $em;

    /**
     * @var FormFactoryInterface
     */
    protected FormFactoryInterface $formFactory;

    /**
     * @var TaskRepository
     */
    protected TaskRepository $taskRepository;

    /**
     * @var BypassListRecordRepository
     */
    protected BypassListRecordRepository $bypassListRecordRepository;

    /**
     * @var CounterHandler
     */
    protected CounterHandler $counterHandler;

    /**
     * @var SubscriberHandler
     */
    protected SubscriberHandler $subscriberHandler;

    /**
     * @var AddressHandler
     */
    protected AddressHandler $addressHandler;

    /**
     * @var CounterValueHandler
     */
    protected CounterValueHandler $counterValueHandler;

    /**
     * @var AgreementHandler
     */
    protected AgreementHandler $agreementHandler;

    /**
     * @var PhotoHandler
     */
    protected PhotoHandler $photoHandler;

    /**
     * @var UserHandler
     */
    protected UserHandler $userHandler;

    /**
     * @var CompanyHandler
     */
    protected CompanyHandler $companyHandler;

    /**
     * @var ParameterBagInterface
     */
    protected ParameterBagInterface $parameterBag;

    /**
     * @var UserGroupHandler
     */
    protected UserGroupHandler $userGroupHandler;

    /**
     * @var PaginatorInterface
     */
    protected PaginatorInterface $paginator;

    public function __construct(
        FormFactoryInterface $formFactory,
        EntityManagerInterface $entityManager,
        TaskRepository $taskRepository,
        BypassListRecordRepository $bypassListRecordRepository,
        CounterHandler $counterHandler,
        SubscriberHandler $subscriberHandler,
        AddressHandler $addressHandler,
        AgreementHandler $agreementHandler,
        UserHandler $userHandler,
        CounterValueHandler $counterValueHandler,
        CompanyHandler $companyHandler,
        PhotoHandler $photoHandler,
        UserGroupHandler $userGroupHandler,
        ParameterBagInterface $parameterBag,
        PaginatorInterface $paginator
    )
    {
        $this->formFactory = $formFactory;
        $this->em = $entityManager;
        $this->taskRepository = $taskRepository;
        $this->bypassListRecordRepository = $bypassListRecordRepository;
        $this->counterHandler = $counterHandler;
        $this->subscriberHandler = $subscriberHandler;
        $this->addressHandler = $addressHandler;
        $this->agreementHandler = $agreementHandler;
        $this->userHandler = $userHandler;
        $this->counterValueHandler = $counterValueHandler;
        $this->companyHandler = $companyHandler;
        $this->photoHandler = $photoHandler;
        $this->userGroupHandler = $userGroupHandler;
        $this->parameterBag = $parameterBag;
        $this->paginator = $paginator;
    }
}