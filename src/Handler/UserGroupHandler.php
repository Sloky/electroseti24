<?php

namespace App\Handler;

use App\Entity\Main\UserGroup;
use App\Form\UserGroupFormType;
use App\Handler\UserHandler\UserHandler;
use App\Model\UserGroupForm;
use App\Repository\UserGroupRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;

class UserGroupHandler
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var UserGroupRepository
     */
    protected $userGroupRepository;

    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    /**
     * @var UserHandler
     */
    protected $userHandler;

    /**
     * @var RoleHierarchyInterface
     */
    protected $roleHierarchy;

    /**
     * UserHandler constructor.
     * @param EntityManagerInterface $entityManager
     * @param UserGroupRepository $userGroupRepository
     * @param FormFactoryInterface $formFactory
     * @param UserHandler $userHandler
     * @param RoleHierarchyInterface $roleHierarchy
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        UserGroupRepository $userGroupRepository,
        FormFactoryInterface $formFactory,
        UserHandler $userHandler,
        RoleHierarchyInterface $roleHierarchy
    )
    {
        $this->em = $entityManager;
        $this->userGroupRepository = $userGroupRepository;
        $this->formFactory = $formFactory;
        $this->userHandler = $userHandler;
        $this->roleHierarchy = $roleHierarchy;
    }
    
    /**
     * Get all user groups
     * @return \App\Entity\Main\UserGroup[]|null
     */
    public function getUserGroups()
    {
        if (empty($groups = $this->userGroupRepository->findAll())) {
            return null;
        }
        return $groups;
    }

    /**
     * Get User group by ID
     * @param $userGroupId
     * @return \App\Entity\Main\UserGroup|null
     */
    public function getUserGroup($userGroupId): ?UserGroup
    {
        if ($userGroup = $this->userGroupRepository->find($userGroupId)) {
            return $userGroup;
        }
        return null;
    }

    /**
     * Get User group by groupName
     * @param $groupName
     * @return \App\Entity\Main\UserGroup|null
     */
    public function getUserGroupByName($groupName): ?UserGroup
    {
        if ($userGroup = $this->userGroupRepository->findOneBy(['groupName' => $groupName])) {
            return $userGroup;
        }
        return null;
    }

    /**
     * Add a new user group
     * @param $userGroupData
     */
    public function addNewUserGroup($userGroupData)
    {
        $form = $this->formFactory->create(UserGroupFormType::class, null, ['csrf_protection' => false]);
        $form->submit($userGroupData);
        /** @var UserGroupForm $formData */
        $formData = $form->getData();

        if (!$form->isValid()) {
            throw new HttpException(Response::HTTP_BAD_REQUEST,'Incorrect form data');
        }

        $em = $this->em;
        $em->beginTransaction();
        try {
            $userGroup = new UserGroup();
            if ($this->userHandler->getUserByName($groupName = $formData->getGroupName())) {
                throw new HttpException(Response::HTTP_BAD_REQUEST,"User group with groupName=$groupName already exists");
            }
            $userGroup->setGroupName($formData->getGroupName());
            $userGroup->setRoles($formData->getRoles()->getValues());
            $userGroup->setDescription($formData->getDescription());
            $em->persist($userGroup);
            $em->flush();
            $em->commit();
        } catch (Exception $e) {
            $em->rollback();
            throw new HttpException($e->getStatusCode(), $e->getMessage());
        }
    }

    /**
     * Edit the user group
     * @param $userGroupData
     * @param $userGroupId
     */
    public function editUserGroup($userGroupData, $userGroupId)
    {
        $form = $this->formFactory->create(UserGroupFormType::class, null, ['csrf_protection' => false]);
        $form->submit($userGroupData);
        /** @var UserGroupForm $formData */
        $formData = $form->getData();

        if (!$userGroup = $this->getUserGroup($userGroupId)) {
            throw new HttpException(Response::HTTP_NOT_FOUND, 'User group not found');
        }

        if ($userGroup->getGroupName() !== $formData->getGroupName() && $this->getUserGroupByName($groupName = $formData->getGroupName())) {
            throw new HttpException(Response::HTTP_NOT_FOUND, "User group with name=$groupName is already exist");
        }

        if (!$form->isValid()) {
            throw new HttpException(Response::HTTP_BAD_REQUEST,'Incorrect form data');
        }

        $em = $this->em;
        $em->beginTransaction();
        try {
            $userGroup->setGroupName($formData->getGroupName());
            $userGroup->setRoles($formData->getRoles()->getValues());
            $userGroup->setDescription($formData->getDescription());
            $em->persist($userGroup);
            $em->flush();
            $em->commit();
        } catch (Exception $e) {
            $em->rollback();
            throw new HttpException(500, $e->getMessage());
        }
    }

    /**
     * Delete the user group
     * @param $userGroupId
     */
    public function deleteUserGroup($userGroupId)
    {
        if (!$userGroup = $this->getUserGroup($userGroupId)) {
            throw new HttpException(Response::HTTP_NOT_FOUND, 'User group not found');
        }
        $em = $this->em;
        $em->beginTransaction();
        try {
            $em->remove($userGroup);
            $em->flush();
            $em->commit();
        } catch (Exception $e) {
            $em->rollback();
            throw new HttpException(500, $e->getMessage());
        }
    }

    /**
     * Get all the available user roles except 'ROLE_USER'
     * @return string[]
     */
    public function getAllTheRoles()
    {
        $roles = $this->roleHierarchy->getReachableRoleNames(['ROLE_ADMIN']);
        if ($key = array_search('ROLE_USER', $roles)) {
            unset($roles[$key]);
        }
        return $roles;
    }
}