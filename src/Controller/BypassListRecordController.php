<?php

namespace App\Controller;

use App\Model\AbstractApiController;
use Exception;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class BypassListRecordController extends AbstractApiController
{
    /**
     * Get all the bypass list records
     * @Route("/bypass-list-records", name="bypass_list_records_list", methods={"GET"})
     * @param Request $request
     * @return View
     */
    public function getBypassListRecordListAction(Request $request): View
    {
        $page = $request->query->get('page', 1);
        $maxResult = $request->query->get('maxResult', null);
        $search = $request->query->get('search', null);
        $searchField = $request->query->get('searchField', null);
        $filter = $this->getFilterFromRequest($request);

        try {
            $bypassListRecordsData = $this->bypassListHandler->getBypassListRecordsList(
                $page,
                $maxResult,
                $search,
                $searchField,
                $filter
            );

            $bypassListRecords = $bypassListRecordsData['bypassListRecordList'];
            $bypassListRecordCount = $bypassListRecordsData['bypassListRecordCount'];
            return $this->view(
                [
                    'records' => $bypassListRecords,
                    'recordsCount' => $bypassListRecordCount
                ],
                Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->view('Something went wrong...', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get the bypass list record
     * @Route("/bypass-list-record/{id}", name="bypass_list_record", methods={"GET"})
     * @param $id
     * @return View
     */
    public function getBypassListRecordAction($id): View
    {
        if (!$bypassListRecord = $this->bypassListHandler->getBypassListRecord($id)) {
            throw new HttpException(
                Response::HTTP_NOT_FOUND,
                "There is no bypass list record with ID = $id"
            );
        }
        return $this->view($bypassListRecord, Response::HTTP_OK);
    }

    /**
     * Edit the bypass list record
     * @Route("/bypass-list-record/{id}/edit", name="bypass_list_record_edit", methods={"POST"})
     * @param Request $request
     * @param $id
     * @return View
     */
    public function editBypassListRecordAction(Request $request, $id): View
    {
        if (!$bypassListRecord = $this->bypassListHandler->getBypassListRecord($id)) {
            throw new HttpException(
                Response::HTTP_NOT_FOUND,
                "There is no bypass list record with ID = $id"
            );
        }

        try {
            $bypassListRecord = $this->bypassListHandler->editBypassListRecord($request, $bypassListRecord);
        } catch (HttpException $httpException) {
            throw $httpException;
        } catch (Exception $exception) {
            $message = 'An error occurred while editing the bypass list records';
            throw new HttpException(Response::HTTP_INTERNAL_SERVER_ERROR, $message);
        }
        return $this->view($bypassListRecord, Response::HTTP_OK);
    }

    /**
     * Delete all the bypass list records by search and filter
     * @Route("/bypass-list-records", name="bypass_list_records_delete", methods={"DELETE"})
     * @param Request $request
     * @return View
     */
    public function deleteBypassListRecordsAction(Request $request): View
    {
        $search = $request->request->get('search', null);
        $searchField = $request->request->get('searchField', null);
        $items = $request->request->get('items', []);
        $filter = $this->getPostFilterFromRequest($request);

        $message = 'Bypass list record successfully deleted';
        if (!$this->bypassListHandler->deleteBypassListRecords($items, $search, $searchField, $filter)) {
            $message = 'An error occurred while deleting the bypass list records';
            throw new HttpException(Response::HTTP_INTERNAL_SERVER_ERROR, $message);
        }
        return $this->view($message, Response::HTTP_OK);
    }

    /**
     * Delete the bypass list record
     * @Route("/bypass-list-record/{id}", name="bypass_list_record_delete", methods={"DELETE"})
     * @param $id
     * @return View
     */
    public function deleteBypassListRecordAction($id): View
    {
        $message = 'Bypass list record successfully deleted';
        if (!$this->bypassListHandler->getBypassListRecord($id)) {
            $message = "There is no bypass list record with ID = $id";
            throw new HttpException(Response::HTTP_NOT_FOUND, $message);
        }
        if (!$this->bypassListHandler->deleteBypassListRecord($id)) {
            $message = 'An error occurred while deleting the bypass list record';
            throw new HttpException(Response::HTTP_INTERNAL_SERVER_ERROR, $message);
        }
        return $this->view($message, Response::HTTP_OK);
    }

    /**
     * Create the tasks from the bypass list records
     * @Route("/bypass-records-to-tasks/{status}", name="bypass_list_to_tasks", methods={"POST"})
     * @param Request $request
     * @param $status
     * @return View
     */
    public function bypassListToTasksAction(Request $request, $status): View
    {
        $search = $request->request->get('search', null);
        $searchField = $request->request->get('searchField', null);
        $maxResult = $request->request->get('maxResult', null);
        $items = $request->request->get('items', []);
        $filter = $this->getPostFilterFromRequest($request);

        $bypassListRecords = [];
        $bypassListRecordsCount = 0;

        $errors = $this->bypassListTaskCreationHandler->BypassListTasksCreation(
            $items,
            $search,
            $searchField,
            $filter,
            intval($status)
        );
        $bypassListRecordsData = $this->bypassListHandler->getBypassListRecordsList(
            1,
            $maxResult,
            $search,
            $searchField,
            $filter
        );
        $bypassListRecordsData && $bypassListRecords = $bypassListRecordsData['bypassListRecordList'];
        $bypassListRecordsData && $bypassListRecordsCount = $bypassListRecordsData['bypassListRecordCount'];

        $branches = $this->companyHandler->getCompaniesForFilter();
        $controllers = $this->userHandler->getControllersFromTasks();

        return $this->view(
            [
                'records' => $bypassListRecords,
                'recordsCount' => $bypassListRecordsCount,
                'branches' => $branches,
                'rowErrors' => $errors,
                'controllers' => $controllers
            ],
            Response::HTTP_OK);
    }

    /**
     * Create the task from the bypass record
     * @Route("/bypass-record-to-task/{recordId}", name="bypass_record_to_task", methods={"GET"})
     * @param $recordId
     * @return View
     */
    public function bypassRecordToTaskAction($recordId): View
    {
        $message = 'Task successfully created';
        if (!$record = $this->bypassListHandler->getBypassListRecord($recordId)) {
            $message = "There is no bypass list record with ID = recordId";
            throw new HttpException(Response::HTTP_NOT_FOUND, $message);
        }
        try {
            $this->bypassListTaskCreationHandler->bypassRecordTaskCreation($record);
        } catch (Exception $exception) {
            return $this->view($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        return $this->view($message, Response::HTTP_OK);
    }

    /**
     * Records multi edit
     * @Route("/bypass-list/multi-edit", name="bypass_records_multi_edit", methods={"POST"})
     * @param Request $request
     * @return View
     * @throws Exception
     */
    public function bypassRecordsMultiEditAction(Request $request): View
    {
        $page = $request->query->get('page', 1);
        $maxResult = $request->query->get('maxResult', null);
        $search = $request->query->get('search', null);
        $searchField = $request->query->get('searchField', null);
        $filter = $this->getFilterFromRequest($request);

        try {
            $this->bypassListHandler->recordsMultiEdit(
                $request,
                $search,
                $searchField,
                $filter,
                $page,
                $maxResult
            );
            $records = $this->bypassListHandler->getBypassListRecordsList(
                $page,
                $maxResult,
                $search,
                $searchField,
                $filter
            );
        } catch (Exception $exception) {
            throw $exception;
        }
        return $this->view($records, Response::HTTP_OK);
    }

    /**
     * Upload bypass list (excel loading)
     * @Route("/bypass-list/upload", name="bypass_list_upload", methods={"POST"})
     * @param Request $request
     * @return View
     */
    public function uploadBypassListAction(Request $request): View
    {
        $maxResult = $request->query->get('maxResult', 100);

        $message = 'Bypass list loaded';
        $statusCode = Response::HTTP_OK;

        $errors = [];
        $bypassListRecords = [];
        $bypassListRecordsCount = 0;
        $branches = [];
        $controllers = [];
        try {
            $errors = $this->bypassListLoadingHandler->loadBypassList($request);

            $bypassListRecordsData = $this->bypassListHandler->getBypassListRecordsList(1, $maxResult);
            $bypassListRecordsData && $bypassListRecords = $bypassListRecordsData['bypassListRecordList'];
            $bypassListRecordsData && $bypassListRecordsCount = $bypassListRecordsData['bypassListRecordCount'];

            $branches = $this->companyHandler->getCompaniesForFilter();
            $controllers = $this->userHandler->getControllersFromTasks();
        } catch (Exception $exception) {
            $message = $exception->getMessage();
            if ($exception instanceof HttpException) {
                $statusCode = $exception->getStatusCode();
            } else {
                $statusCode = ($exception->getCode() >= 100 ? $exception->getCode() : Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }
        return $this->view(
            [
                'records' => $bypassListRecords,
                'recordsCount' => $bypassListRecordsCount,
                'branches' => $branches,
                'message' => $message,
                'rowErrors' => $errors,
                'controllers' => $controllers
            ],
            $statusCode);
    }

    /**
     * @param Request $request
     * @return array
     */
    protected function getFilterFromRequest(Request $request): array
    {
        $companyTitleFilter = $request->query->get('companyTitle', null);
        $subscriberTypeFilter = $request->query->get('subscriberType', null);
        $serviceabilityFilter = $request->query->get('serviceability', null);
        $executorFilter = $request->query->get('executor', null);
        $statusFilter = $request->query->get('status', null);
        $periodFilter = $request->query->get('period', null);

        $filter = [];
        !is_null($companyTitleFilter) && $filter['companyTitle'] = $companyTitleFilter;
        !is_null($subscriberTypeFilter) && $filter['subscriberType'] = $subscriberTypeFilter;
        !is_null($serviceabilityFilter) && $filter['serviceability'] = $serviceabilityFilter;
        !is_null($executorFilter) && $filter['executor'] = $executorFilter;
        !is_null($statusFilter) && $filter['status'] = $statusFilter;
        !is_null($periodFilter) && $filter['period'] = $periodFilter;
        return $filter;
    }

    /**
     * @param Request $request
     * @return array
     */
    protected function getPostFilterFromRequest(Request $request): array
    {
        $companyTitleFilter = $request->request->get('companyTitle', null);
        $subscriberTypeFilter = $request->request->get('subscriberType', null);
        $serviceabilityFilter = $request->request->get('serviceability', null);
        $executorFilter = $request->request->get('executor', null);
        $statusFilter = $request->request->get('status', null);
        $periodFilter = $request->request->get('period', null);

        $filter = [];
        !is_null($companyTitleFilter) && $filter['companyTitle'] = $companyTitleFilter;
        !is_null($subscriberTypeFilter) && $filter['subscriberType'] = $subscriberTypeFilter;
        !is_null($serviceabilityFilter) && $filter['serviceability'] = $serviceabilityFilter;
        !is_null($executorFilter) && $filter['executor'] = $executorFilter;
        !is_null($statusFilter) && $filter['status'] = $statusFilter;
        !is_null($periodFilter) && $filter['period'] = $periodFilter;
        return $filter;
    }
}