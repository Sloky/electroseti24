<?php

namespace App\Controller;

use App\Entity\Main\Company;
use App\Entity\Main\User;
use App\Model\AbstractApiController;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Exception;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class UserController extends AbstractApiController
{
    /**
     * Get all the Users
     * @Route("/user", name="users_all", methods={"GET"})
     * @param Request $request
     * @return View
     */
    public function getUserListAction(Request $request): View
    {
        $page = $request->query->get('page', 1);
        $maxResult = $request->query->get('maxResult', null);
        $search = $request->query->get('search', null);
        $filter = $this->getFilterFromRequest($request);

        try {
            $userListData = $this->userHandler->getUserList($page, $maxResult, $search, $filter);
            $userList = $userListData['userList'];
            $usersTotalCount = $userListData['usersCount'];
            return $this->view(
                [
                    'users' => $userList,
                    'usersCount' => $usersTotalCount
                ],
                Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->view('Something went wrong...', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @IsGranted("ROLE_SUPER_ADMIN")
     * @Route("/user/{user}/set-role-super-admin", name="set_role_super_admin", methods={"GET"})
     * @param \App\Entity\Main\User $user
     * @return View
     */
    public function setRoleSuperAdminAction(User $user): View
    {
        $message = 'Rights changed successfully';
        $statusCode = Response::HTTP_OK;
        try {
            $user->addRoles(['ROLE_SUPER_ADMIN']);
            $this->em->persist($user);
            $this->em->flush();
        } catch (Exception $exception) {
            $message = 'Something went wrong';
            $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
        }
        return $this->view($message, $statusCode);
    }

    /**
     * @Route("/users/controllers-for-tasks", name="controllers_for_tasks", methods={"GET"})
     * @return View
     */
    public function getUsersForTaskListAction(): View
    {
        $users = $this->userHandler->getControllersFromTasks();
        return $this->view($users, Response::HTTP_OK);
    }

    /**
     * Get all the Users with Controller group
     * @Route("/users/{groupName}", name="users_by_group", methods={"GET"})
     * @param $groupName
     * @return View
     */
    public function getUsersByGroupsAction($groupName): View
    {
        if (!($users = $this->userHandler->getUsersByGroupName($groupName))) {
            return $this->view("There are no users in group $groupName or this group does not exist", Response::HTTP_NOT_FOUND);
        }
        return $this->view($users, Response::HTTP_OK);
    }

    /**
     * Get Controllers by company
     * @Route("/controllers-by-company/{company}", name="controllers_by_company", methods={"GET"})
     * @param Company $company
     * @return View
     */
    public function getControllersByCompanyAction(Company $company): View
    {
        $controllers = $this->userHandler->userRepository->getControllersByCompanies([$company->getId()]);
        return $this->view($controllers, Response::HTTP_OK);
    }

    /**
     * Create a new User
     * @Route("/user/new", name="user_new", methods={"POST"})
     * @param Request $request
     * @return View
     */
    public function newUserAction(Request $request): View
    {
        $message = 'User successfully added';
        $statusCode = Response::HTTP_CREATED;
        try {
            $this->userHandler->addNewUser($request->request->all());
        } catch (Exception $exception) {
            $message = $exception->getMessage();
            if ($exception instanceof HttpException) {
                $statusCode = $exception->getStatusCode();
            } else {
                $statusCode = ($exception->getCode() >= 100 ? $exception->getCode() : 400);
            }
        }
        return $this->view(['message' => $message], $statusCode);
    }

    /**
     * Get the current User
     * @Route("/user/current", name="user_get_current", methods={"GET"})
     * @return View
     */
    public function getCurrentUserAction(): View
    {
        $user = $this->getUser();
        return $this->view($user, Response::HTTP_OK);
    }

    /**
     * Get the User by ID
     * @Route("/user/{userId}", name="user_get", methods={"GET"})
     * @param $userId
     * @return View
     */
    public function getUserAction($userId): View
    {
        if (!$user = $this->userHandler->getUser($userId)) {
            return $this->view('User not found', Response::HTTP_NOT_FOUND);
        }
        return $this->view($user, Response::HTTP_OK);
    }

    /**
     * Edit the User
     * @Route("/user/{userId}/edit", name="user_edit", methods={"POST"})
     * @param Request $request
     * @param $userId
     * @return View
     */
    public function editAction(Request $request, $userId): View
    {
        $message = 'User successfully changed';
        $statusCode = Response::HTTP_OK;
        try {
            $this->userHandler->editUser($request->request->all(), $userId);
        } catch (Exception $exception) {
            $message = $exception->getMessage();
            if ($exception instanceof HttpException) {
                $statusCode = $exception->getStatusCode();
            } else {
                $statusCode = ($exception->getCode() >= 100 ? $exception->getCode() : 400);
            }
        }
        return $this->view(['message' => $message], $statusCode);
    }

    /**
     * Delete the User
     * @Route("/user/{userId}", name="user_delete", methods={"DELETE"})
     * @param $userId
     * @return View
     * @throws Exception
     */
    public function deleteAction($userId): View
    {
        $message = 'User successfully deleted';
        $statusCode = Response::HTTP_OK;
        try {
            $this->userHandler->deleteUser($userId);
        } catch (Exception $exception) {
            throw $exception;
            /*$message = $exception->getMessage();
            if ($exception instanceof HttpException) {
                $statusCode = $exception->getStatusCode();
            } else {
                $statusCode = $exception->getCode();
            }*/
        }
        return $this->view(['message' => $message], $statusCode);
    }

    /**
     * Get user profile data action
     * @Route("/user/mobile-profile/{user}", name="user_get_mobile_profile_data", methods={"GET"})
     * @param \App\Entity\Main\User $user
     * @return View
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getUserProfileDataForMobileAppAction(User $user): View
    {
        try {
            $userMobileProfileData = $this->userProfileHandler->getControllerProfileForMobileApp($user);
        } catch (Exception $e) {
            throw $e;
        }
        return $this->view($userMobileProfileData, Response::HTTP_OK);
    }

    /**
     * Get excel user data
     * @Route("/user/profile/{user}", name="user_get_profile_data", methods={"GET"})
     * @param \App\Entity\Main\User $user
     * @return View
     */
    public function getUserProfileAction(User $user): View
    {
        $userProfileData = $this->userProfileHandler->getUserProfileData($user);
        return $this->view($userProfileData, Response::HTTP_OK);
    }

    /**
     * Get excel user profile data
     * @Route("/user/profile-upload/excel/{user}", name="user_get_profile_upload_excel_data", methods={"GET"})
     * @param \App\Entity\Main\User $user
     * @return View|Response
     */
    public function getUserExcelProfile(User $user)
    {
        try {
            return $this->excelUserStatisticHandler->getExcelProfileData($user);
        } catch (Exception $exception) {
            $message = $exception->getMessage();
            if ($exception instanceof HttpException) {
                $statusCode = $exception->getStatusCode();
            } else {
                $statusCode = ($exception->getCode() >= 100 ? $exception->getCode() : 400);
            }
        }
        return $this->view(['message' => $message], $statusCode);
    }

    /**
     * @param Request $request
     * @return array
     */
    protected function getFilterFromRequest(Request $request): array
    {
        $groupFilter = $request->query->get('group', null);
        $statusFilter = $request->query->get('status', null);
        $companyFilter = $request->query->get('company', null);

        $filter = [];
        !is_null($groupFilter) && $filter['group'] = $groupFilter;
        !is_null($statusFilter) && $filter['status'] = $statusFilter;
        !is_null($companyFilter) && $filter['company'] = $companyFilter;
        return $filter;
    }
}
