<?php


namespace App\Controller;


use App\Model\AbstractApiController;
use Exception;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class PhotoController extends AbstractApiController
{
    /**
     * Delete all the photos
     * @Route("/photo/delete-photos", name="delete_all_the_photos", methods={"DELETE"})
     */
    public function deleteAllThePhotos(): View
    {
        $message = 'All the photos has been deleted successfully';
        $statusCode = Response::HTTP_OK;
        try {
            if (!$this->photoHandler->deleteAllThePhotos()) {
                throw new HttpException(Response::HTTP_BAD_REQUEST, 'Incorrect period');
            }
        } catch (Exception $exception) {
            $message = $exception->getMessage();
            if ($exception instanceof HttpException) {
                $statusCode = $exception->getStatusCode();
            } else {
                $statusCode = $exception->getCode() === 0 ? 400 : $exception->getCode();
            }
        }
        return $this->view(['message' => $message], $statusCode);
    }

    /**
     * Delete photos by period
     * @Route("/photo/delete-photos/{period}", name="delete_photos_by_period", methods={"DELETE"})
     */
    public function deletePhotosFolderByPeriod($period): View
    {
        $message = 'Photos folder has been deleted successfully';
        $statusCode = Response::HTTP_OK;
        try {
            if (!$this->photoHandler->deletePhotosByPeriod($period)) {
                throw new HttpException(Response::HTTP_BAD_REQUEST, 'Incorrect period');
            }
        } catch (Exception $exception) {
            $message = $exception->getMessage();
            if ($exception instanceof HttpException) {
                $statusCode = $exception->getStatusCode();
            } else {
                $statusCode = $exception->getCode() === 0 ? 400 : $exception->getCode();
            }
        }
        return $this->view(['message' => $message], $statusCode);
    }
}