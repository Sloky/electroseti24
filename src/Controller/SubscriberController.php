<?php

namespace App\Controller;

use App\Entity\Main\Subscriber;
use App\Model\AbstractApiController;
use Exception;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class SubscriberController extends AbstractApiController
{
    /**
     * @Route("/subscriber/legal-entities", name="legal_entities_subscribers", methods={"GET"})
     * @param Request $request
     * @return View
     */
    public function getLegalEntitiesAction(Request $request): View
    {
        $page = $request->query->get('page', 1);
        $maxResult = $request->query->get('maxResult', null);
        $search = $request->query->get('search', null);

        return $this->view(
            $this->subscriberHandler->getSubscribersWithSearch(1, $page, $maxResult, $search),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/subscriber/individuals", name="individuals_subscribers", methods={"GET"})
     * @param Request $request
     * @return View
     */
    public function getIndividualsAction(Request $request): View
    {
        $page = $request->query->get('page', 1);
        $maxResult = $request->query->get('maxResult', null);
        $search = $request->query->get('search', null);

        return $this->view(
            $this->subscriberHandler->getSubscribersWithSearch(0, $page, $maxResult, $search),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/subscriber/central-office-staff", name="central_office_staff_subscribers", methods={"GET"})
     * @param Request $request
     * @return View
     */
    public function getCentralOfficeStaffAction(Request $request): View
    {
        $page = $request->query->get('page', 1);
        $maxResult = $request->query->get('maxResult', null);
        $search = $request->query->get('search', null);

        return $this->view(
            $this->subscriberHandler->getSubscribersWithSearch(2, $page, $maxResult, $search),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/subscriber/{subscriber}", name="get_subscriber", methods={"GET"})
     * @param \App\Entity\Main\Subscriber $subscriber
     * @return View
     */
    public function getSubscriberAction(Subscriber $subscriber): View
    {
        return $this->view(
            $subscriber,
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/subscriber-legal-entity/add", name="add_legal_entity_subscriber", methods={"POST"})
     * @param Request $request
     * @return View
     * @throws Exception
     */
    public function addSubscriberLegalEntityAction(Request $request): View
    {
        $this->subscriberHandler->addSubscriberLegalEntity($request);
        return $this->view(
            "Subscriber added successfully",
            Response::HTTP_CREATED
        );
    }

    /**
     * @Route("/subscriber-legal-entity/{subscriber}/edit", name="edit_legal_entity_subscriber", methods={"POST"})
     * @param \App\Entity\Main\Subscriber $subscriber
     * @param Request $request
     * @return View
     * @throws Exception
     */
    public function editSubscriberLegalEntityAction(Subscriber $subscriber, Request $request): View
    {
        $subscriber = $this->subscriberHandler->editSubscriberLegalEntity($subscriber, $request);
        return $this->view(
            $subscriber,
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/subscriber-individual/add", name="add_individual_subscriber", methods={"POST"})
     * @param Request $request
     * @return View
     * @throws Exception
     */
    public function addSubscriberIndividualAction(Request $request): View
    {
        $this->subscriberHandler->addSubscriberIndividual($request);
        return $this->view(
            "Subscriber added successfully",
            Response::HTTP_CREATED
        );
    }

    /**
     * @Route("/subscriber-individual/{subscriber}/edit", name="edit_individual_subscriber", methods={"POST"})
     * @param \App\Entity\Main\Subscriber $subscriber
     * @param Request $request
     * @return View
     * @throws Exception
     */
    public function editSubscriberIndividualAction(Subscriber $subscriber, Request $request): View
    {
        $subscriber = $this->subscriberHandler->editSubscriberIndividual($subscriber, $request);
        return $this->view(
            $subscriber,
            Response::HTTP_OK
        );
    }

    /**
     *  @Route("/subscriber/{subscriber}", name="delete_subscriber", methods={"DELETE"})
     * @param \App\Entity\Main\Subscriber $subscriber
     * @return View
     */
    public function deleteSubscriber(Subscriber $subscriber): View
    {
        $this->em->remove($subscriber);
        $this->em->flush();
        return $this->view(
            "Subscriber deleted successfully",
            Response::HTTP_CREATED
        );
    }
}