<?php

namespace App\Controller;

use App\Entity\Main\Task;
use App\Entity\Main\User;
use App\Model\AbstractApiController;
use App\Model\Assembler\CompletedTaskAssembler;
use App\Model\Assembler\TaskAssembler;
use App\Model\Assembler\TaskToCheckAssembler;
use Exception;
use FOS\RestBundle\View\View;
use League\Flysystem\FilesystemException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class TaskController extends AbstractApiController
{
    /**
     * Get the task
     * @Route("/task/{taskId}", name="task", methods={"GET"})
     * @param $taskId
     * @return View
     */
    public function getTaskAction($taskId): View
    {
        if (!($task = $this->taskHandler->getTask($taskId))) {
            return $this->view("There are no task with ID = $taskId", Response::HTTP_NOT_FOUND);
        }
        $taskDTO = TaskAssembler::writeDTO($task);
        return $this->view($taskDTO, Response::HTTP_OK);
    }

    /**
     * Get all the tasks with status '0' (for tasks to work list)
     * @Route("/task-to-work-list", name="task_to_work_list", methods={"GET"})
     * @param Request $request
     * @return View
     */
    public function getTaskToWorkListAction(Request $request): View
    {
        $page = $request->query->get('page', 1);
        $maxResult = $request->query->get('maxResult', null);
        $search = $request->query->get('search', null);
        $searchField = $request->query->get('searchField', null);
        $filter = $this->getFilterFromRequest($request);

        try {
            $tasksListData = $this->taskToWorkHandler->getTasksToWorkList(
                $page,
                $maxResult,
                $search,
                $searchField,
                $filter
            );

            $tasksList = $tasksListData['tasksList'];
            $taskTotalCount = $tasksListData['taskCount'];
            return $this->view(
                [
                    'tasks' => $tasksList,
                    'tasksCount' => $taskTotalCount
                ],
                Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->view('Something went wrong...', Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Get 'task to work' data (for tasks to work card)
     * @Route("/task-to-work/{id}", name="task_to_work_item", methods={"GET"})
     * @param $id
     * @return View
     */
    public function getTaskToWorkAction($id): View
    {
        if (!($task = $this->taskHandler->getTask($id))) {
            return $this->view("There are no task with ID = $id", Response::HTTP_NOT_FOUND);
        }
        $taskDTO = TaskAssembler::writeDTO($task);
        $users = $this->userHandler->getControllersForTask($id);
        return $this->view([
            'task' => $taskDTO,
            'users' => $users,
        ], Response::HTTP_OK);
    }

    /**
     * Get all the tasks with status '1' (for tasks to check list)
     * @Route("/task-to-check-list", name="task_to_check_list", methods={"GET"})
     * @param Request $request
     * @return View
     */
    public function getTaskToCheckListAction(Request $request): View
    {
        $page = $request->query->get('page', 1);
        $maxResult = $request->query->get('maxResult', null);
        $search = $request->query->get('search', null);
        $searchField = $request->query->get('searchField', null);
        $filter = $this->getFilterFromRequest($request);

        try {
            $tasksListData = $this->taskToCheckHandler->getTasksToCheckList(
                $page,
                $maxResult,
                $search,
                $searchField,
                $filter
            );

            $tasksList = $tasksListData['tasksList'];
            $taskTotalCount = $tasksListData['taskCount'];
            return $this->view(
                [
                    'tasks' => $tasksList,
                    'tasksCount' => $taskTotalCount
                ],
                Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->view('Something went wrong...', Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Get 'task to check' data (for tasks to check card)
     * @Route("/task-to-check/{id}", name="task_to_check_item", methods={"GET"})
     * @param $id
     * @return View
     */
    public function getTaskToCheckAction($id): View
    {
        if (!($task = $this->taskHandler->getTask($id))) {
            return $this->view("There is no task with ID = $id", Response::HTTP_NOT_FOUND);
        }
        $photosStorageHost = $this->photoHandler->getS3DirPublicUrl();
        $taskDTO = TaskToCheckAssembler::writeDTO($task, $photosStorageHost);
        $counterModels = $this->counterHandler->getCounterTypes() ?? [];
        return $this->view([
            'task' => $taskDTO,
            'counterModels' => $counterModels,
        ], Response::HTTP_OK);
    }

    /**
     * Get all the tasks periods
     * @Route("/tasks-periods/{taskType}", name="tasks_periods", methods={"GET"})
     * @param $taskType
     * @return View
     * @throws Exception
     */
    public function getTasksPeriodAction($taskType): View
    {
        $tasksPeriods = $this->taskHandler->getTasksPeriodsByStatus($taskType);
        return $this->view($tasksPeriods, Response::HTTP_OK);
    }

    /**
     * Get all the tasks with status '2' (for completed tasks list)
     * @Route("/completed-task-list", name="completed_task_list", methods={"GET"})
     * @param Request $request
     * @return View
     */
    public function getCompletedTaskListAction(Request $request): View
    {
        $page = $request->query->get('page', 1);
        $maxResult = $request->query->get('maxResult', null);
        $search = $request->query->get('search', null);
        $searchField = $request->query->get('searchField', null);
        $filter = $this->getFilterFromRequest($request);

        try {
            $tasksListData = $this->completedTaskHandler->getCompletedTasksList(
                $page,
                $maxResult,
                $search,
                $searchField,
                $filter
            );

            $tasksList = $tasksListData['tasksList'];
            $taskTotalCount = $tasksListData['taskCount'];
            return $this->view(
                [
                    'tasks' => $tasksList,
                    'tasksCount' => $taskTotalCount
                ],
                Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->view('Something went wrong...', Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Get 'completed task' data (for completed task card)
     * @Route("/completed-task/{id}", name="completed_task_item", methods={"GET"})
     * @param $id
     * @return View
     */
    public function getCompletedTaskAction($id): View
    {
        if (!($task = $this->taskHandler->getTask($id))) {
            return $this->view("There is no task with ID = $id", Response::HTTP_NOT_FOUND);
        }
        $photosStorageHost = $this->photoHandler->getS3DirPublicUrl();
        $completedTaskDTO = CompletedTaskAssembler::writeDTO($task, $photosStorageHost);
        return $this->view([
            'task' => $completedTaskDTO,
        ], Response::HTTP_OK);
    }

    /**
     * Edit the task to work
     * @Route("/task-to-work/{taskId}/edit", name="task_to_work_edit", methods={"POST"})
     * @param Request $request
     * @param $taskId
     * @return View
     */
    public function editTaskToWorkAction(Request $request, $taskId): View
    {
        $message = 'Task successfully changed';
        $statusCode = Response::HTTP_OK;
        try {
            $task = $this->taskToWorkHandler->editTaskToWork($request->request->all(), $taskId);
            return $this->view(['message' => $message, 'task' => $task], $statusCode);
        } catch (Exception $exception) {
            $message = $exception->getMessage();
            if ($exception instanceof HttpException) {
                $statusCode = $exception->getStatusCode();
            } else {
                $statusCode = ($exception->getCode() >= 100 ? $exception->getCode() : 400);
            }
        }
        return $this->view(['message' => $message], $statusCode);
    }

    /**
     * Edit the task to check
     * @Route("/task-to-check/{taskId}/edit", name="task_to_check_edit", methods={"POST"})
     * @param Request $request
     * @param $taskId
     * @return View
     */
    public function editTaskToCheckAction(Request $request, $taskId): View
    {
        $message = 'Task successfully changed';
        $statusCode = Response::HTTP_OK;
        try {
            $task = $this->taskToCheckHandler->editTaskToCheck($request, $taskId);
            return $this->view(['message' => $message, 'task' => $task], $statusCode);
        } catch (Exception $exception) {
            $message = $exception->getMessage();
            if ($exception instanceof HttpException) {
                $statusCode = $exception->getStatusCode();
            } else {
                $statusCode = ($exception->getCode() >= 100 ? $exception->getCode() : 400);
            }
        }
        return $this->view(['message' => $message], $statusCode);
    }

    /**
     * Accept the task to check
     * @Route("/task-to-check/{taskId}/accept", name="task_to_check_accept", methods={"GET"})
     * @param $taskId
     * @return View
     */
    public function acceptTaskToCheckAction($taskId): View
    {
        $message = 'Task successfully accepted';
        $statusCode = Response::HTTP_OK;
        try {
            $this->taskToCheckHandler->acceptTaskToCheck($taskId);
        } catch (Exception $exception) {
            $message = $exception->getMessage();
            if ($exception instanceof HttpException) {
                $statusCode = $exception->getStatusCode();
            } else {
                $statusCode = ($exception->getCode() >= 100 ? $exception->getCode() : 400);
            }
        }
        return $this->view(['message' => $message], $statusCode);
    }

    /**
     * Accept the task to check
     * @Route("/tasks-to-check/accept", name="tasks_to_check_accept_all", methods={"GET"})
     * @return View
     * @throws Exception
     */
    public function acceptAllTheTasksToCheckAction(): View
    {
        $message = 'All the tasks successfully accepted';
        $statusCode = Response::HTTP_OK;
        $tasksToCheck = $this->taskToCheckHandler->acceptTasksToCheck();
        return $this->view(
            [
                'message' => $message,
                'tasks' => $tasksToCheck['tasksList'],
                'tasksCount' => $tasksToCheck['taskCount']
            ],
            $statusCode
        );
    }

    /**
     * Change the status of TaskToCheck to TaskToWork
     * @Route("/task-to-check/{taskId}/return", name="task_to_check_return", methods={"GET"})
     * @param $taskId
     * @return View
     */
    public function returnTaskToCheckAction($taskId): View
    {
        $message = 'Task successfully returned';
        $statusCode = Response::HTTP_OK;
        try {
            $this->taskToCheckHandler->returnTaskToCheck($taskId);
        } catch (Exception $exception) {
            $message = $exception->getMessage();
            if ($exception instanceof HttpException) {
                $statusCode = $exception->getStatusCode();
            } else {
                $statusCode = ($exception->getCode() >= 100 ? $exception->getCode() : 400);
            }
        }
        return $this->view(['message' => $message], $statusCode);
    }

    /**
     * Upload "task to check" from the mobile app
     * @param Request $request
     * @param $taskId
     * @return View
     * @throws Exception
     * @Route("/task/{taskId}/upload", name="task_mobile_app_upload", methods={"POST"})
     */
    public function uploadTaskFromMobileApp(Request $request, $taskId): View
    {
        $message = 'Task successfully loaded';
        $statusCode = Response::HTTP_OK;
        $this->uploadTaskFromMobileAppHandler->uploadTaskFromMobileApp($request, $taskId);
//        $this->taskHandler->uploadTaskFromMobileApp($request, $taskId);
        return $this->view(['message' => $message], $statusCode);
    }

    /**
     * Upload task list (excel loading)
     * @Route("/task/upload", name="task_upload", methods={"POST"})
     * @param Request $request
     * @return View
     */
    public function uploadTaskListAction(Request $request): View
    {
        $maxResult = $request->query->get('maxResult', 100);

        $message = 'Task list loaded';
        $statusCode = Response::HTTP_OK;
        $errors = [];
        $tasksList = [];
        $taskCount = 0;
        $periods = [];
        $branches = [];
        $controllers = [];
        try {
            $errors = $this->excelTaskListHandler->loadTaskList($request->files->all());

            $tasksListData = $this->taskToWorkHandler->getTasksToWorkList(1, $maxResult);
            $tasksListData && $tasksList = $tasksListData['tasksList'];
            $tasksListData && $taskCount = $tasksListData['taskCount'];

            $periods = $this->taskHandler->getTasksPeriodsByStatus(0);
            $branches = $this->companyHandler->getCompaniesForFilter();
            $controllers = $this->userHandler->getUsersByGroupName('Controllers');
        } catch (Exception $exception) {
            $message = $exception->getMessage();
            if ($exception instanceof HttpException) {
                $statusCode = $exception->getStatusCode();
            } else {
                $statusCode = ($exception->getCode() >= 100 ? $exception->getCode() : 400);
            }
        }
        return $this->view(
            [
                'tasks' => $tasksList,
                'tasksCount' => $taskCount,
                'periods' => $periods,
                'branches' => $branches,
                'message' => $message,
                'rowErrors' => $errors,
                'controllers' => $controllers
            ],
            $statusCode);
    }

    /**
     * Delete Tasks to work
     * @Route("/tasks-to-work-delete", name="task_to_work_delete", methods={"DELETE"})
     */
    public function deleteTasksToWorkAction(Request $request): View
    {
        $search = $request->query->get('search', null);
        $searchField = $request->query->get('searchField', null);
        $filter = $this->getFilterFromRequest($request);
        $maxResult = $request->query->get('maxResult', null);

        $message = 'Tasks to work has been deleted successfully';
        try {
            $this->deleteTaskHandler->deleteTasksToWork($filter, $search, $searchField);
            $tasksResult = $this->taskToWorkHandler->getTasksToWorkList(1, $maxResult);
            return $this->view([
                'tasks' => $tasksResult['tasksList'],
                'tasksCount' => $tasksResult['taskCount'],
                'message' => $message,
            ], Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->view('Something went wrong...', Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Delete Tasks by type
     * @Route("/task/tasks-by-type/delete/{type}", name="tasks_by_type_delete", methods={"DELETE"})
     */
    public function deleteTasksByTypeAction($type, Request $request): View
    {
        $search = $request->query->get('search', null);
        $searchField = $request->query->get('searchField', null);
        $filter = $this->getFilterFromRequest($request);

        $message = "Tasks with type = $type has been deleted successfully";
        $statusCode = Response::HTTP_OK;
        try {
            switch ($type) {
                case '0' : $this->deleteTaskHandler->deleteTasksToWork($filter, $search, $searchField); break;
                case '1' : $this->deleteTaskHandler->deleteTasksToCheck($filter, $search, $searchField); break;
                case '2' : $this->deleteTaskHandler->deleteCompletedTasks($filter, $search, $searchField); break;
                case 'all' : $this->deleteTaskHandler->deleteAllTheTasks(); break;
                default: throw new HttpException(Response::HTTP_BAD_REQUEST, 'Incorrect task type');
            }
        } catch (Exception $exception) {
            $message = $exception->getMessage();
            if ($exception instanceof HttpException) {
                $statusCode = $exception->getStatusCode();
            } else {
                $statusCode = ($exception->getCode() >= 100 ? $exception->getCode() : 400);
            }
        }
        return $this->view(['message' => $message], $statusCode);
    }

    /**
     * Delete the Task
     * @Route("/task/delete/{id}", name="tasks_by_id_delete", methods={"DELETE"})
     */
    public function deleteTaskAction(Task $task): View
    {
        $message = "Task with ID = {$task->getId()} has been deleted successfully";
        $statusCode = Response::HTTP_OK;
        try {
            $this->deleteTaskHandler->deleteTheTask($task->getId());
        } catch (Exception $exception) {
            $message = $exception->getMessage();
            if ($exception instanceof HttpException) {
                $statusCode = $exception->getStatusCode();
            } else {
                $statusCode = ($exception->getCode() >= 100 ? $exception->getCode() : 400);
            }
        }
        return $this->view(['message' => $message], $statusCode);
    }

    /**
     * Get all the tasks photos
     * @Route("/task-upload/photos", name="tasks_upload_photos", methods={"GET"})
     */
    public function getCompletedAllTheTasksPhotos(Request $request)
    {
        $search = $request->query->get('search', null);
        $searchField = $request->query->get('searchField', null);
        $filter = $this->getFilterFromRequest($request);
        $page = $request->query->get('page', 1);
        $maxResult = $request->query->get('maxResult', null);

        /** @var User $user */
        $user = $this->getUser();
        try {
            return $this->uploadPhotosZipHandler->getUploadTAllTheTasksPhotosResponse($user, $filter, $search, $searchField, $page, $maxResult);
        } catch (Exception $exception) {
            $message = $exception->getMessage();
            if ($exception instanceof HttpException) {
                $statusCode = $exception->getStatusCode();
            } else {
                $statusCode = ($exception->getCode() >= 100 ? $exception->getCode() : 400);
            }
        }
        return $this->view(['message' => $message], $statusCode);
    }

    /**
     * Get Task photos
     * @Route("/task-upload/{id}/photos", name="task_upload_photos", methods={"GET"})
     */
    public function getCompletedTaskPhotos(Task $task)
    {
        try {
            return $this->uploadPhotosZipHandler->getUploadTaskPhotosResponse($task);
        } catch (Exception $exception) {
            $message = $exception->getMessage();
            if ($exception instanceof HttpException) {
                $statusCode = $exception->getStatusCode();
            } else {
                $statusCode = ($exception->getCode() >= 100 ? $exception->getCode() : 400);
            }
        } catch (FilesystemException $e) {
            $message = $e->getMessage();
            $statusCode = ($e->getCode() >= 100 ? $e->getCode() : 400);
        }
        return $this->view(['message' => $message], $statusCode);
    }

    /**
     * Get completed Task Excel file
     * @Route("/task-upload/{id}/excel", name="task_upload_excel", methods={"GET"})
     */
    public function getExcelCompletedTaskData($id)
    {
        try {
            return $this->excelTaskListHandler->getExcelCompletedTaskData($id);
        } catch (Exception $exception) {
            $message = $exception->getMessage();
            if ($exception instanceof HttpException) {
                $statusCode = $exception->getStatusCode();
            } else {
                $statusCode = ($exception->getCode() >= 100 ? $exception->getCode() : 400);
            }
        }
        return $this->view(['message' => $message], $statusCode);
    }

    /**
     * Get completed Task Excel file
     * @Route("/task-upload/excel", name="tasks_upload_excel", methods={"GET"})
     */
    public function getExcelCompletedTasksData(Request $request)
    {
        $search = $request->query->get('search', null);
        $searchField = $request->query->get('searchField', null);
        $filter = $this->getFilterFromRequest($request);

        /** @var User $user */
        $user = $this->getUser();

        try {
            return $this->excelTaskListHandler->getExcelCompletedTasksData($user, $filter, $search, $searchField);
        } catch (Exception $exception) {
            $message = $exception->getMessage();
            if ($exception instanceof HttpException) {
                $statusCode = $exception->getStatusCode();
            } else {
                $statusCode = ($exception->getCode() >= 100 ? $exception->getCode() : 400);
            }
        }
        return $this->view(['message' => $message], $statusCode);
    }

    /**
     * @todo Temporarily unused functionality
     * Get json data from mod1
     * @Route("/mod1-synchronize", name="mod1_synchronize", methods={"GET"})
     */
    public function synchronizeMod1(): View
    {
        $message = 'Data to work has been synchronized successfully';
        $statusCode = Response::HTTP_OK;
        try {
            $this->taskHandler->synchronizeMod1Data();
            $tasksList = $this->taskToWorkHandler->getTasksToWorkList();
            $branches = $this->companyHandler->getCompanies();
        } catch (Exception $exception) {
            $message = $exception->getMessage();
            if ($exception instanceof HttpException) {
                $statusCode = $exception->getStatusCode();
            } else {
                $statusCode = ($exception->getCode() >= 100 ? $exception->getCode() : 400);
            }
        }
        return $this->view(
            [
                'message' => $message,
                'tasks' => $tasksList ?? [],
                'branches' => $branches ?? []
            ],
            $statusCode
        );
    }

    /**
     * @param Request $request
     * @return array
     */
    protected function getFilterFromRequest(Request $request): array
    {
        $companyFilter = $request->query->get('company', null);
        $executorFilter = $request->query->get('user', null);
        $subscriberTypeFilter = $request->query->get('subscriberType', null);
        $periodFilter = $request->query->get('period', null);

        $filter = [];
        !is_null($companyFilter) && $filter['company'] = $companyFilter;
        !is_null($executorFilter) && $filter['user'] = $executorFilter;
        !is_null($subscriberTypeFilter) && $filter['subscriberType'] = $subscriberTypeFilter;
        !is_null($periodFilter) && $filter['period'] = $periodFilter;
        return $filter;
    }
}
