<?php

namespace App\Controller;

use App\Entity\Main\BypassListRecord;
use App\Entity\Main\User;
use App\Handler\AgreementHandler;
use App\Handler\BypassListHandler\BypassListHandler;
use App\Handler\BypassListHandler\BypassListLoadingHandler;
use App\Handler\BypassListHandler\BypassListTaskCreationHandler;
use App\Handler\CompanyHandler;
use App\Handler\CounterHandler;
use App\Handler\PhotoHandler;
use App\Handler\SubscriberHandler;
use App\Handler\TaskHandler\CompletedTaskHandler;
use App\Handler\TaskHandler\DeleteTaskHandler;
use App\Handler\TaskHandler\ExcelTaskListHandler;
use App\Handler\TaskHandler\TaskHandler;
use App\Handler\TaskHandler\TaskToCheckHandler;
use App\Handler\TaskHandler\TaskToWorkHandler;
use App\Handler\TaskHandler\UploadPhotosZipHandler;
use App\Handler\TaskHandler\UploadTaskFromMobileAppHandler;
use App\Handler\TransformerHandler\TransformerHandler;
use App\Handler\UserGroupHandler;
use App\Handler\UserHandler\ExcelUserStatisticHandler;
use App\Handler\UserHandler\UserHandler;
use App\Handler\UserHandler\UserProfileHandler;
use App\Model\AbstractApiController;
use App\Repository\TaskRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/api")
 */
class DefaultApiController extends AbstractApiController
{
    /**
     * @var TaskRepository
     */
    private TaskRepository $taskRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $passwordEncoder,
        UserHandler $userHandler,
        UserGroupHandler $userGroupHandler,
        TaskHandler $taskHandler,
        CompanyHandler $companyHandler,
        CounterHandler $counterHandler,
        PhotoHandler $photoHandler,
        TaskToWorkHandler $taskToWorkHandler,
        TaskToCheckHandler $taskToCheckHandler,
        CompletedTaskHandler $completedTaskHandler,
        ExcelTaskListHandler $excelTaskListHandler,
        DeleteTaskHandler $deleteTaskHandler,
        UploadTaskFromMobileAppHandler $uploadTaskFromMobileAppHandler,
        UploadPhotosZipHandler $uploadPhotosZipHandler,
        BypassListHandler $bypassListHandler,
        BypassListLoadingHandler $bypassListLoadingHandler,
        BypassListTaskCreationHandler $bypassListTaskCreationHandler,
        UserProfileHandler $userProfileHandler,
        ExcelUserStatisticHandler $excelUserStatisticHandler,
        TransformerHandler $transformerHandler,
        TaskRepository $taskRepository,
        SubscriberHandler $subscriberHandler,
        AgreementHandler $agreementHandler
    )
    {
        parent::__construct(
            $entityManager,
            $passwordEncoder,
            $userHandler,
            $userGroupHandler,
            $taskHandler,
            $companyHandler,
            $counterHandler,
            $photoHandler,
            $taskToWorkHandler,
            $taskToCheckHandler,
            $completedTaskHandler,
            $excelTaskListHandler,
            $deleteTaskHandler,
            $uploadTaskFromMobileAppHandler,
            $uploadPhotosZipHandler,
            $bypassListHandler,
            $bypassListLoadingHandler,
            $bypassListTaskCreationHandler,
            $userProfileHandler,
            $excelUserStatisticHandler,
            $transformerHandler,
            $subscriberHandler,
            $agreementHandler
        );
        $this->taskRepository = $taskRepository;
    }

    /**
     * Get tasks and bypass list records count
     * @Route("/entities-count", name="default_api", methods={"GET"})
     */
    public function getEntitiesItemsCount(): View
    {
        /** @var \App\Entity\Main\User $user */
        $user = $this->getUser();
        $doctrine = $this->getDoctrine();

        $tasksToWorkCount = 0;
        $tasksToCheckCount = 0;
        $completedTasksCount = 0;
        $bypassRecordsCount = 0;

        if ($this->userHandler->isUserAdmin($user)) {
            $tasksToWorkCount = $this->taskRepository->count(['status' => 0]);
            $tasksToCheckCount = $this->taskRepository->count(['status' => 1]);
            $completedTasksCount = $this->taskRepository->count(['status' => 2]);
            $bypassRecordsCount = intval($doctrine->getRepository(BypassListRecord::class)->count([]));
        } else {
            $tasksToWorkCount = intval($this->taskRepository->getTaskTCount(0, $user->getCompanies()->getValues()));
            $tasksToCheckCount = intval($this->taskRepository->getTaskTCount(1, $user->getCompanies()->getValues()));
            $completedTasksCount = intval($this->taskRepository->getTaskTCount(2, $user->getCompanies()->getValues()));
            $bypassRecordsCount = intval($doctrine->getRepository(BypassListRecord::class)->getBypassRecordsCount($user));
        }

        return $this->view([
            'bypassRecordsCount' => $bypassRecordsCount,
            'tasksToWorkCount' => $tasksToWorkCount,
            'tasksToCheckCount' => $tasksToCheckCount,
            'completedTasksCount' => $completedTasksCount
        ], Response::HTTP_OK);
    }
}
