<?php

namespace App\Controller;

use App\Entity\Main\Agreement;
use App\Entity\Main\Subscriber;
use App\Model\AbstractApiController;
use App\Model\Assembler\AgreementsTableItemDTOAssembler;
use Exception;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class AgreementController extends AbstractApiController
{
    /**
     * Get agreements with search and pagination
     * @Route("/agreements", name="agreements", methods={"GET"})
     * @param Request $request
     * @return View
     */
    public function getAgreementsAction(Request $request): View
    {
        $page = $request->query->get('page', 1);
        $maxResult = $request->query->get('maxResult', null);
        $search = $request->query->get('search', null);

        return $this->view(
            $this->agreementHandler->getAgreementsWithSearch($page, $maxResult, $search),
            Response::HTTP_OK
        );
    }

    /**
     * Get subscriber`s agreements
     * @Route("/subscriber/{subscriber}/agreements", name="agreements_by_subscriber", methods={"GET"})
     * @param Request $request
     * @param Subscriber $subscriber
     * @return View
     */
    public function getAgreementsBySubscriberAction(Request $request, Subscriber $subscriber): View
    {
        $page = $request->query->get('page', 1);
        $maxResult = $request->query->get('maxResult', null);
        $search = $request->query->get('search', null);
        $agreements = $this->agreementHandler->getAgreementsWithSearch($page, $maxResult, $search, $subscriber->getId());
        $agreementTableItems = [];
        foreach ($agreements['agreements'] as $agreement) {
            $agreementTableItems[] = AgreementsTableItemDTOAssembler::writeDTO($agreement);
        }
        return $this->view(
            [
                'agreements' => $agreementTableItems,
                'agreementsCount' => $agreements['agreementsCount']
            ],
            Response::HTTP_OK
        );
    }

    /**
     * Get the Agreement
     * @Route("/agreement/{agreement}", name="agreement", methods={"GET"})
     * @param Agreement $agreement
     * @return View
     */
    public function getAgreement(Agreement $agreement): View
    {
        return $this->view(
            $agreement,
            Response::HTTP_OK
        );
    }

    /**
     * Add new Agreement
     * @Route("/agreement/add", name="add_new_agreement", methods={"POST"})
     * @param Request $request
     * @return View
     * @throws Exception
     */
    public function addAgreementAction(Request $request): View
    {
        $this->agreementHandler->addAgreement($request);
        return $this->view(
            "Agreement added successfully",
            Response::HTTP_CREATED
        );
    }

    /**
     * Edit the agreement
     * @Route("/agreement/{agreement}/edit", name="edit_agreement", methods={"POST"})
     * @param Agreement $agreement
     * @param Request $request
     * @return View
     * @throws Exception
     */
    public function editAgreementAction(Agreement $agreement, Request $request): View
    {
        $agreement = $this->agreementHandler->editAgreement($agreement, $request);
        return $this->view(
            $agreement,
            Response::HTTP_OK
        );
    }

    /**
     * Delete the Agreement
     * @Route("/agreement/{agreement}", name="delete_agreement", methods={"DELETE"})
     * @param Agreement $agreement
     * @return View
     */
    public function deleteAgreementAction(Agreement $agreement): View
    {
        $this->em->remove($agreement);
        $this->em->flush();
        return $this->view(
            'Agreement deleted successfully',
            Response::HTTP_OK
        );
    }
}