<?php


namespace App\Controller;


use App\Entity\Main\Agreement;
use App\Entity\Main\Counter;
use App\Entity\Main\CounterValue;
use App\Entity\Main\Transformer;
use App\Model\AbstractApiController;
use App\Model\Assembler\CounterCardDTOAssembler;
use App\Model\Assembler\MetricUnitAssembler;
use Exception;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class CounterController extends AbstractApiController
{
    /**
     * @Route("/counter/mapping", name="counter_mapping", methods={"GET"})
     * @return View
     */
    public function getCounterAndAgreementMappingAction()
    {
        $mapping = [];
        if (!empty($counters = $this->counterHandler->getCounters())) {
            foreach ($counters as $counter) {
                $mapping[] = [$counter->getAgreement()->getNumber(), $counter->getTitle()];
            }
        }
        return $this->view($mapping, Response::HTTP_OK);
    }

    /**
     * Get Counters by Agreement with search and pagination
     * @Route("/counters-by-agreement/{agreement}", name="counter_by_agreement", methods={"GET"})
     * @param Request $request
     * @param \App\Entity\Main\Agreement $agreement
     * @return View
     */
    public function getCountersByAgreementIdWithSearch(Request $request, Agreement $agreement): View
    {
        $page = $request->query->get('page', 1);
        $maxResult = $request->query->get('maxResult', null);
        $search = $request->query->get('search', null);

        $countersData = $this->counterHandler->getCountersByAgreementWithSearch(
            $agreement,
            $search,
            $page,
            $maxResult
        );

        return $this->view(
            [
                'counters' => $countersData['counters'],
                'countersCount' => $countersData['countersCount']
            ],
            Response::HTTP_OK
        );
    }

    /**
     * Get Counters by Transformer with search, filtering and pagination
     * @Route("/counter-list-by-transformer/{transformer}", name="counter_list_by_TP", methods={"GET"})
     * @param Request $request
     * @param \App\Entity\Main\Transformer $transformer
     * @return View
     */
    public function getCountersByTPAction(Request $request, Transformer $transformer): View
    {
        $page = $request->query->get('page', 1);
        $maxResult = $request->query->get('maxResult', null);
        $search = $request->query->get('search', null);
        $filter = $this->getFilterFromRequest($request);

        $metricUnitsData = $this->counterHandler->getCounterByTPWithFilters(
            $transformer,
            $filter,
            $search,
            $page,
            $maxResult
        );

        return $this->view(
            [
                'counters' => $metricUnitsData['counters'],
                'countersCount' => $metricUnitsData['countersCount']
            ],
            Response::HTTP_OK
        );
    }

    /**
     * Upload counter list (excel loading)
     * @Route("/counter-list-by-transformer/{transformer}/upload", name="counter_list_upload", methods={"POST"})
     * @param Request $request
     * @param Transformer $transformer
     * @return View
     */
    public function counterListUploadAction(Request $request, Transformer $transformer): View
    {
        $maxResult = $request->query->get('maxResult', 100);

        $message = 'Counter list loaded';
        $statusCode = Response::HTTP_OK;

        $errors = [];
        $counters = [];
        $countersCount = 0;

        try {
            $errors = $this->counterHandler->loadCountersExcelList($request, $transformer);

            $counterListData = $this->counterHandler->getCounterByTPWithFilters($transformer,null,null,1, $maxResult);
            $counterListData && $counters = $counterListData['counters'];
            $counterListData && $countersCount = $counterListData['countersCount'];

        } catch (Exception $exception) {
            $message = $exception->getMessage();
            if ($exception instanceof HttpException) {
                $statusCode = $exception->getStatusCode();
            } else {
                $statusCode = ($exception->getCode() >= 100 ? $exception->getCode() : Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }
        return $this->view(
            [
                'counters' => $counters,
                'countersCount' => $countersCount,
                'message' => $message,
                'rowErrors' => $errors
            ],
            $statusCode);
    }

    /**
     * Get counter card data for TP
     * @Route("/counter-card/{counter}", name="counter_card_TP", methods={"GET"})
     * @param \App\Entity\Main\Counter $counter
     * @return View
     */
    public function getMeteringUnitCardAction(Counter $counter): View
    {
        $counterValue = $this->getDoctrine()->getRepository(CounterValue::class)->findOneBy(
            ['counter' => $counter],
            ['date' => 'DESC']
        );
        $meteringUnit = MetricUnitAssembler::writeDTO($counter, $counterValue);
        return $this->view(
            [
                'counterCardData' => $meteringUnit,
            ],
            Response::HTTP_OK
        );
    }

    /**
     * Edit the counter card data for TP
     * @Route("/counter-card/{counter}/edit", name="counter_card_edit", methods={"POST"})
     * @param Request $request
     * @param Counter $counter
     * @return View
     * @throws Exception
     */
    public function editMeteringUnitAction(Request $request, Counter $counter): View
    {
        $counter = $this->counterHandler->editCounterCardData($request, $counter);
        $counterValue = $this->getDoctrine()->getRepository(CounterValue::class)->findOneBy(
            ['counter' => $counter],
            ['date' => 'DESC']
        );
        $meteringUnit = MetricUnitAssembler::writeDTO($counter, $counterValue);
        return $this->view(
            [
                'counterCardData' => $meteringUnit,
            ],
            Response::HTTP_OK
        );
    }

    /**
     * Get counter card data
     * @Route("/counter/{counter}", name="counter_card", methods={"GET"})
     * @param Counter $counter
     * @return View
     */
    public function getCounterCardDataAction(Counter $counter): View
    {
        $lastCounterValue = $this->em->getRepository(CounterValue::class)->findOneBy(
            ['counter' => $counter],
            ['date' => 'DESC']
        );
        $counterCardData = CounterCardDTOAssembler::writeDTO($counter, $lastCounterValue);
        return $this->view(
            [
                $counterCardData,
            ],
            Response::HTTP_OK
        );
    }

    /**
     * Get counter values
     * @Route("/counter/{counter}/values", name="counter_values", methods={"GET"})
     * @param Counter $counter
     * @return View
     */
    public function getCounterValuesByCounterAction(Counter $counter): View
    {
        $counterValues = $this->em->getRepository(CounterValue::class)->findBy(
            ['counter' => $counter],
            ['date' => 'DESC']
        );
        return $this->view(
            [
                $counterValues,
            ],
            Response::HTTP_OK
        );
    }

    /**
     * Add a new Counter
     * @Route("/agreement/{agreement}/add-counter", name="add_new_counter", methods={"POST"})
     * @param \App\Entity\Main\Agreement $agreement
     * @param Request $request
     * @return View
     * @throws Exception
     */
    public function addCounterAction(Agreement $agreement, Request $request): View
    {
        $this->counterHandler->addCounter($agreement, $request);
        return $this->view(
            "Counter added successfully",
            Response::HTTP_CREATED
        );
    }

    /**
     * Edit the Counter
     * @Route("/counter/{counter}/edit", name="edit_counter", methods={"POST"})
     * @param \App\Entity\Main\Counter $counter
     * @param Request $request
     * @return View
     * @throws Exception
     */
    public function editCounterAction(Counter $counter, Request $request): View
    {
        $counter = $this->counterHandler->editCounter($counter, $request);
        return $this->view(
            $counter,
            Response::HTTP_OK
        );
    }

    /**
     * Delete the counter
     * @Route("/counter/{counter}", name="counter_delete", methods={"DELETE"})
     * @param \App\Entity\Main\Counter $counter
     * @return View
     */
    public function deleteCounter(Counter $counter): View
    {
        $this->em->remove($counter);
        $this->em->flush();
        return $this->view(['message' => 'Counter deleted successfully'], Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return array
     */
    protected function getFilterFromRequest(Request $request): array
    {
        $companyFilter = $request->query->get('company', null);
        $executorFilter = $request->query->get('user', null);
        $subscriberTypeFilter = $request->query->get('subscriberType', null);
        $periodFilter = $request->query->get('period', null);

        $filter = [];
        !is_null($companyFilter) && $filter['company'] = $companyFilter;
        !is_null($executorFilter) && $filter['user'] = $executorFilter;
        !is_null($subscriberTypeFilter) && $filter['subscriberType'] = $subscriberTypeFilter;
        !is_null($periodFilter) && $filter['period'] = $periodFilter;
        return $filter;
    }
}