<?php

namespace App\Controller;

use App\Entity\Main\Company;
use App\Entity\Main\Transformer;
use App\Model\AbstractApiController;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class TransformerController extends AbstractApiController
{
    /**
     * Get all the transformers
     * @Route("/transformers", name="transformers", methods={"GET"})
     * @param Request $request
     * @return View
     */
    public function getTransformerListAction(Request $request): View
    {
        $page = $request->query->get('page', 1);
        $maxResult = $request->query->get('maxResult', null);
        $search = $request->query->get('search', null);
        $filter = $this->getFilterFromRequest($request);

        try {
            $transformerListData = $this->transformerHandler->getTransformerList($page, $maxResult, $search, $filter);
            $transformerList = $transformerListData['transformerList'];
            $transformersTotalCount = $transformerListData['transformersCount'];
            return $this->view(
                [
                    'transformers' => $transformerList,
                    'transformersCount' => $transformersTotalCount
                ],
                Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->view('Something went wrong...', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get the transformer
     * @Route("/transformer/{transformer}", name="transformer", methods={"GET"})
     * @param \App\Entity\Main\Transformer $transformer
     * @return View
     */
    public function transformerAction(Transformer $transformer): View
    {
        return $this->view($transformer, Response::HTTP_OK);
    }

    /**
     * Get the transformers by company
     * @Route("/transformers-by-company/{company}", name="transformers_by_company", methods={"GET"})
     * @param Request $request
     * @param Company $company
     * @return View
     */
    public function getTransformersByCompanyAction(Request $request, Company $company): View
    {
        $page = $request->query->get('page', 1);
        $maxResult = $request->query->get('maxResult', null);
        $search = $request->query->get('search', null);
        $executor = $request->query->get('executor', null);

        $TPDTOs = $this->transformerHandler->getTransformersByCompany(
            $company,
            $search,
            $executor,
            $page,
            $maxResult
        );
        return $this->view(
            $TPDTOs,
            Response::HTTP_OK
        );
    }

    /**
     * Add a new transformer
     * @Route("/transformer/new", name="transformer_new", methods={"POST"})
     * @param Request $request
     * @return View
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function addNewTransformer(Request $request): View
    {
        $message = 'The transformer has been successfully created';
        $this->transformerHandler->addNewTransformer($request);
        return $this->view(['message' => $message], Response::HTTP_CREATED);
    }

    /**
     * Edit the transformer
     * @Route("/transformer/{transformer}/edit", name="transformer_edit", methods={"POST"})
     * @param Transformer $transformer
     * @param Request $request
     * @return View
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function editTransformerAction(Transformer $transformer, Request $request): View
    {
        $transformer = $this->transformerHandler->editTransformer($transformer, $request);
        return $this->view($transformer, Response::HTTP_OK);
    }

    /**
     * Delete the transformer
     * @Route("/transformer/{transformer}", name="transformer_delete", methods={"DELETE"})
     * @param \App\Entity\Main\Transformer $transformer
     * @return View
     */
    public function deleteTransformerAction(Transformer $transformer): View
    {
        $message = 'The transformer has been successfully deleted';
        $this->em->remove($transformer);
        $this->em->flush();
        return $this->view(['message' => $message], Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return array
     */
    protected function getFilterFromRequest(Request $request): array
    {
        $companyFilter = $request->query->get('company', null);
        $executorFilter = $request->query->get('executor', null);

        $filter = [];
        !is_null($companyFilter) && $filter['company'] = $companyFilter;
        !is_null($executorFilter) && $filter['executor'] = $executorFilter;
        return $filter;
    }
}
