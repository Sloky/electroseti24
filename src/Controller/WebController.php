<?php

namespace App\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class WebController
 * @package App\Controller
 *
 * Route("/api")
 */
class WebController extends AbstractFOSRestController
{
    /**
     * @Route(
     *     "/{reactRouting}/{additionalRout1}/{additionalRout2}",
     *     name="home",
     *     defaults={"reactRouting": null, "additionalRout1": null, "additionalRout2": null}
     * )
     */
    public function index()
    {
        return $this->render('web/index.html.twig');
    }


}
