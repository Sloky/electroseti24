<?php

namespace App\Controller;

use App\Model\AbstractApiController;
use Exception;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class CompanyController extends AbstractApiController
{
    /**
     * Get all the companies
     * @Route("/companies", name="companies", methods={"GET"})
     * @param Request $request
     * @return View
     */
    public function getCompaniesAction(Request $request): View
    {
        $page = $request->query->get('page', 1);
        $maxResult = $request->query->get('maxResult', null);
        $companiesData = $this->companyHandler->getCompanies($page, $maxResult);

        return $this->view(
            [
                'companies' => $companiesData['companies'],
                'companiesCount' => $companiesData['companiesCount']
            ],
            Response::HTTP_OK);
    }

    /**
     * Get the companies for the filter
     * @Route("/companies/for-filter", name="companies_for_filter", methods={"GET"})
     * @return View
     */
    public function getCompaniesForFilterAction(): View
    {
        $companies = $this->companyHandler->getCompaniesForFilter();
        return $this->view($companies, Response::HTTP_OK);
    }

    /**
     * Get the company
     * @Route("/company/{id}", name="company", methods={"GET"})
     * @param $id
     * @return View
     */
    public function getCompanyAction($id): View
    {
        if (!$company = $this->companyHandler->getCompany($id)) {
            throw new HttpException(Response::HTTP_NOT_FOUND, "There is no company with ID = $id");
        }
        return $this->view($company, Response::HTTP_OK);
    }

    /**
     * Create a new company
     * @Route("/company/new", name="company_new", methods={"POST"})
     * @param Request $request
     * @return View
     */
    public function createNewCompanyAction(Request $request): View
    {
        $company = $this->companyHandler->createNewCompany($request);
        return $this->view($company, Response::HTTP_CREATED);
    }

    /**
     * Edit the company
     * @Route("/company/{id}/edit", name="company_edit", methods={"POST"})
     * @param Request $request
     * @param $id
     * @return View
     */
    public function editCompanyAction(Request $request, $id): View
    {
        if (!$company = $this->companyHandler->getCompany($id)) {
            throw new HttpException(Response::HTTP_NOT_FOUND, "There is no company with ID = $id");
        }
        if (!$company = $this->companyHandler->editCompany($request, $company)) {
            throw new HttpException(
                Response::HTTP_INTERNAL_SERVER_ERROR,
                'An error occurred while editing the company'
            );
        }
        return $this->view($company, Response::HTTP_OK);
    }

    /**
     * Delete the company
     * @Route("/company/{id}", name="company_delete", methods={"DELETE"})
     * @param $id
     * @return View
     */
    public function deleteCompanyAction($id): View
    {
        if (!$company = $this->companyHandler->getCompany($id)) {
            throw new HttpException(
                Response::HTTP_NOT_FOUND,
                'Company not found'
            );
        }
        if (!$this->companyHandler->deleteCompany($company)) {
            throw new HttpException(
                Response::HTTP_INTERNAL_SERVER_ERROR,
                'An error occurred while deleting the company'
            );
        }
        return $this->view('Company successfully deleted', Response::HTTP_OK);
    }
}