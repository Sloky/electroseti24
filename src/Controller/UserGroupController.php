<?php

namespace App\Controller;

use App\Model\AbstractApiController;
use Exception;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class UserGroupController extends AbstractApiController
{
    /**
     * Get all the UserGroups
     * @Route("/group", name="user_groups", methods={"GET"})
     * @return View
     */
    public function getUserGroupsAction(): View
    {
        $groups = $this->userGroupHandler->getUserGroups() ?? [];
        return $this->view($groups, Response::HTTP_OK);
    }

    /**
     * Add a new UserGroup
     * @Route("/group/new", name="user_group_new", methods={"POST"})
     * @param Request $request
     * @return View
     */
    public function newAction(Request $request): View
    {
        $message = 'User group successfully added';
        $statusCode = Response::HTTP_CREATED;
        try {
            $this->userGroupHandler->addNewUserGroup($request->request->all());
        } catch (Exception $exception) {
            $message = $exception->getMessage();
            if ($exception instanceof HttpException) {
                $statusCode = $exception->getStatusCode();
            } else {
                $statusCode = $exception->getCode();
            }
        }
        return $this->view(['message' => $message], $statusCode);
    }

    /**
     * Get the UserGroup by ID
     * @Route("/group/{groupId}", name="user_group_show", methods={"GET"})
     * @param $groupId
     * @return View
     */
    public function getGroupAction($groupId): View
    {
        if (!$userGroup = $this->userGroupHandler->getUserGroup($groupId)) {
            throw new HttpException( Response::HTTP_NOT_FOUND, 'User group not found');
        }
        return $this->view($userGroup, Response::HTTP_OK);
    }

    /**
     * Edit the UserGroup
     * @Route("/group/{userGroupId}/edit", name="user_group_edit", methods={"POST"})
     * @param Request $request
     * @param $userGroupId
     * @return View
     */
    public function editAction(Request $request, $userGroupId): View
    {
        $message = 'User group successfully changed';
        $statusCode = Response::HTTP_OK;
        try {
            $this->userGroupHandler->editUserGroup($request->request->all(), $userGroupId);
        } catch (Exception $exception) {
            $message = $exception->getMessage();
            if ($exception instanceof HttpException) {
                $statusCode = $exception->getStatusCode();
            } else {
                $statusCode = $exception->getCode();
            }
        }
        return $this->view(['message' => $message], $statusCode);
    }

    /**
     * Delete the UserGroup
     * @Route("/group/{userGroupId}", name="user_group_delete", methods={"DELETE"})
     * @param $userGroupId
     * @return View
     */
    public function deleteAction($userGroupId): View
    {
        $message = 'User group successfully deleted';
        $statusCode = Response::HTTP_OK;
        try {
            $this->userGroupHandler->deleteUserGroup($userGroupId);
        } catch (Exception $exception) {
            $message = $exception->getMessage();
            if ($exception instanceof HttpException) {
                $statusCode = $exception->getStatusCode();
            } else {
                $statusCode = $exception->getCode();
            }
        }
        return $this->view(['message' => $message], $statusCode);
    }

    /**
     * Get all the roles
     * @Route("/group/roles/all", name="all_roles", methods={"GET"})
     * @return View
     */
    public function getRoleHierarchy()
    {
        $roles = $this->userGroupHandler->getAllTheRoles();
        return $this->view($roles, Response::HTTP_OK);
    }
}
