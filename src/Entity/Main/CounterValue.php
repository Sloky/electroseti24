<?php

namespace App\Entity\Main;

use App\Model\CounterValueInterface;
use App\Repository\CounterValueRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Этот класс описывает показания узла учета
 * @ORM\Entity(repositoryClass=CounterValueRepository::class)
 */
class CounterValue
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\Column(name="id", type="guid")
     * @ORM\CustomIdGenerator(class="App\Model\CustomIdGenerator")
     */
    private $id;

    /**
     * Счетчик
     * @ORM\ManyToOne(targetEntity="App\Entity\Main\Counter")
     * @ORM\JoinColumn(name="counter", referencedColumnName="id", onDelete="SET NULL")
     * @var Counter
     */
    private $counter;

    /**
     * Дата снятия показаний
     * @ORM\Column(type="datetime")
     * @var DateTimeInterface
     */
    private $date;

    /**
     * Показания
     * @ORM\Column(type="object", nullable=true)
     * @var CounterValueInterface
     */
    private $value;

    /**
     * Пользователь снявший показания (контролёр)
     * @ORM\ManyToOne(targetEntity="App\Entity\Main\User")
     * @ORM\JoinColumn(name="inspector", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * @var UserInterface|null
     */
    private $inspector;

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @return Counter
     */
    public function getCounter(): Counter
    {
        return $this->counter;
    }

    /**
     * @param Counter $counter
     * @return CounterValue
     */
    public function setCounter(Counter $counter): CounterValue
    {
        $this->counter = $counter;
        return $this;
    }

    /**
     * @return DateTimeInterface
     */
    public function getDate(): DateTimeInterface
    {
        return $this->date;
    }

    /**
     * @param DateTimeInterface $date
     * @return CounterValue
     */
    public function setDate(DateTimeInterface $date): CounterValue
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return CounterValueInterface
     */
    public function getValue(): CounterValueInterface
    {
        return $this->value;
    }

    /**
     * @param CounterValueInterface $value
     * @return CounterValue
     */
    public function setValue(CounterValueInterface $value): CounterValue
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return UserInterface|null
     */
    public function getInspector(): ?UserInterface
    {
        return $this->inspector;
    }

    /**
     * @param UserInterface|null $inspector
     * @return CounterValue
     */
    public function setInspector(?UserInterface $inspector): CounterValue
    {
        $this->inspector = $inspector;
        return $this;
    }
}
