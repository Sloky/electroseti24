<?php

namespace App\Entity\Main;

use App\Repository\FeederRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FeederRepository::class)
 */
class Feeder
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\Column(name="id", type="guid")
     * @ORM\CustomIdGenerator(class="App\Model\CustomIdGenerator")
     */
    private $id;

    /**
     * Название фидера
     * @ORM\Column(type="string", length=100, unique=true)
     * @var string
     */
    private $title;

    /**
     * Подстанция
     * @ORM\ManyToOne(targetEntity="App\Entity\Main\Substation")
     * @ORM\JoinColumn(name="substation", referencedColumnName="id", nullable=true)
     * @var Substation|null
     */
    private $substation;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->title;
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Feeder
     */
    public function setTitle(string $title): Feeder
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return Substation|null
     */
    public function getSubstation(): ?Substation
    {
        return $this->substation;
    }

    /**
     * @param Substation|null $substation
     * @return Feeder
     */
    public function setSubstation(?Substation $substation): Feeder
    {
        $this->substation = $substation;
        return $this;
    }
}
