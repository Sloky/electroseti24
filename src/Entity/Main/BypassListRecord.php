<?php

namespace App\Entity\Main;

use App\Model\BypassListErrorInterface;
use App\Repository\BypassListRecordRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BypassListRecordRepository::class)
 */
class BypassListRecord
{
    /**
     * @var string|null
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\Column(name="id", type="guid")
     * @ORM\CustomIdGenerator(class="App\Model\CustomIdGenerator")
     */
    private $id;

    /**
     * Номер строки
     * @ORM\Column(type="integer", nullable=false)
     * @var int
     */
    private int $rowPosition;

    /**
     * Название файла
     * @ORM\Column(type="text", nullable=false)
     * @var string
     */
    private string $fileName;

    /**
     * Статус записи (
     *  0 => Готово к постановке,
     *  1 => Требует дополнения,
     *  2 => Критические ошибки
     * )
     * @ORM\Column(type="integer", nullable=false)
     * @var int
     */
    private int $status;

    /**
     * Период обходного листа
     * @ORM\Column(type="datetime", nullable=true)
     * @var DateTimeInterface|null
     */
    private ?DateTimeInterface $period;

    /**
     * Поле "№ Лицевого счета/платежный код"
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private ?string $agreementNumber;

    /**
     * Поле "Адрес объекта"
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private ?string $address;

    /**
     * Поле "Наименование абонента"
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private ?string $subscriberTitle;

    /**
     * Поле "Тип абонента"
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private ?string $subscriberType;

    /**
     * Поле "Телефоны абонента"
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private ?string $phones;

    /**
     * Поле "Наименование отделения"
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private ?string $companyTitle;

    /**
     * Поле "Номер счётчика"
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private ?string $counterNumber;

    /**
     * Поле "Тип (модель) счётчика"
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private ?string $counterModel;

    /**
     * Поле "Доступность / Исправность"
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private ?string $serviceability;

    /**
     * Поле "Текущее значение показаний"
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private ?string $currentCounterValue;

    /**
     * Поле "Дата снятия текущего значения показаний"
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private ?string $currentCounterValueDate;

    /**
     * Поле "Боковая пломба"
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private ?string $sideSeal;

    /**
     * Поле "Клеммная пломба"
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private ?string $terminalSeal;

    /**
     * Поле "Антимагнитная пломба"
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private ?string $antimagneticSeal;

    /**
     * Поле "Подстанция"
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private ?string $substation;

    /**
     * Поле "Фидер"
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private ?string $feeder;

    /**
     * Поле "Трансформатор"
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private ?string $transformer;

    /**
     * Поле "Линия"
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private ?string $electricLine;

    /**
     * Поле "Столб"
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private ?string $electricPole;

    /**
     * Поле "Координаты на момент постановки задачи"
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private ?string $coords;

    /**
     * Поле "Контролёр"
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private ?string $controller;

    /**
     * Поле "Крайний срок выполнения"
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private ?string $deadline;

    /**
     * Поле "Количество комнат"
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private ?string $roomsCount;

    /**
     * Поле "Количество проживающих"
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private ?string $lodgersCount;

    /**
     * Поле "Дата установки счётчика"
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private ?string $counterInitialDate;

    /**
     * Поле "Значение показаний счётчика при установке"
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private ?string $counterInitialValue;

    /**
     * Ошибки записи обходного листа
     * @ORM\Column(type="object", nullable=true)
     * @var Collection<BypassListErrorInterface>
     */
    private Collection $errors;

    public function __construct()
    {
        $this->errors = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     * @return BypassListRecord
     */
    public function setId(?string $id): BypassListRecord
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getRowPosition(): int
    {
        return $this->rowPosition;
    }

    /**
     * @param int $rowPosition
     * @return BypassListRecord
     */
    public function setRowPosition(int $rowPosition): BypassListRecord
    {
        $this->rowPosition = $rowPosition;
        return $this;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     * @return BypassListRecord
     */
    public function setFileName(string $fileName): BypassListRecord
    {
        $this->fileName = $fileName;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return BypassListRecord
     */
    public function setStatus(int $status): BypassListRecord
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getPeriod(): ?DateTimeInterface
    {
        return $this->period;
    }

    /**
     * @param DateTimeInterface|null $period
     * @return BypassListRecord
     */
    public function setPeriod(?DateTimeInterface $period): BypassListRecord
    {
        $this->period = $period;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAgreementNumber(): ?string
    {
        return $this->agreementNumber;
    }

    /**
     * @param string|null $agreementNumber
     * @return BypassListRecord
     */
    public function setAgreementNumber(?string $agreementNumber): BypassListRecord
    {
        $this->agreementNumber = $agreementNumber;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     * @return BypassListRecord
     */
    public function setAddress(?string $address): BypassListRecord
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubscriberTitle(): ?string
    {
        return $this->subscriberTitle;
    }

    /**
     * @param string|null $subscriberTitle
     * @return BypassListRecord
     */
    public function setSubscriberTitle(?string $subscriberTitle): BypassListRecord
    {
        $this->subscriberTitle = $subscriberTitle;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubscriberType(): ?string
    {
        return $this->subscriberType;
    }

    /**
     * @param string|null $subscriberType
     * @return BypassListRecord
     */
    public function setSubscriberType(?string $subscriberType): BypassListRecord
    {
        $this->subscriberType = $subscriberType;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhones(): ?string
    {
        return $this->phones;
    }

    /**
     * @param string|null $phones
     * @return BypassListRecord
     */
    public function setPhones(?string $phones): BypassListRecord
    {
        $this->phones = $phones;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCompanyTitle(): ?string
    {
        return $this->companyTitle;
    }

    /**
     * @param string|null $companyTitle
     * @return BypassListRecord
     */
    public function setCompanyTitle(?string $companyTitle): BypassListRecord
    {
        $this->companyTitle = $companyTitle;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCounterNumber(): ?string
    {
        return $this->counterNumber;
    }

    /**
     * @param string|null $counterNumber
     * @return BypassListRecord
     */
    public function setCounterNumber(?string $counterNumber): BypassListRecord
    {
        $this->counterNumber = $counterNumber;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCounterModel(): ?string
    {
        return $this->counterModel;
    }

    /**
     * @param string|null $counterModel
     * @return BypassListRecord
     */
    public function setCounterModel(?string $counterModel): BypassListRecord
    {
        $this->counterModel = $counterModel;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getServiceability(): ?string
    {
        return $this->serviceability;
    }

    /**
     * @param string|null $serviceability
     * @return BypassListRecord
     */
    public function setServiceability(?string $serviceability): BypassListRecord
    {
        $this->serviceability = $serviceability;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCurrentCounterValue(): ?string
    {
        return $this->currentCounterValue;
    }

    /**
     * @param string|null $currentCounterValue
     * @return BypassListRecord
     */
    public function setCurrentCounterValue(?string $currentCounterValue): BypassListRecord
    {
        $this->currentCounterValue = $currentCounterValue;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCurrentCounterValueDate(): ?string
    {
        return $this->currentCounterValueDate;
    }

    /**
     * @param string|null $currentCounterValueDate
     * @return BypassListRecord
     */
    public function setCurrentCounterValueDate(?string $currentCounterValueDate): BypassListRecord
    {
        $this->currentCounterValueDate = $currentCounterValueDate;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSideSeal(): ?string
    {
        return $this->sideSeal;
    }

    /**
     * @param string|null $sideSeal
     * @return BypassListRecord
     */
    public function setSideSeal(?string $sideSeal): BypassListRecord
    {
        $this->sideSeal = $sideSeal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTerminalSeal(): ?string
    {
        return $this->terminalSeal;
    }

    /**
     * @param string|null $terminalSeal
     * @return BypassListRecord
     */
    public function setTerminalSeal(?string $terminalSeal): BypassListRecord
    {
        $this->terminalSeal = $terminalSeal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAntimagneticSeal(): ?string
    {
        return $this->antimagneticSeal;
    }

    /**
     * @param string|null $antimagneticSeal
     * @return BypassListRecord
     */
    public function setAntimagneticSeal(?string $antimagneticSeal): BypassListRecord
    {
        $this->antimagneticSeal = $antimagneticSeal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubstation(): ?string
    {
        return $this->substation;
    }

    /**
     * @param string|null $substation
     * @return BypassListRecord
     */
    public function setSubstation(?string $substation): BypassListRecord
    {
        $this->substation = $substation;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFeeder(): ?string
    {
        return $this->feeder;
    }

    /**
     * @param string|null $feeder
     * @return BypassListRecord
     */
    public function setFeeder(?string $feeder): BypassListRecord
    {
        $this->feeder = $feeder;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTransformer(): ?string
    {
        return $this->transformer;
    }

    /**
     * @param string|null $transformer
     * @return BypassListRecord
     */
    public function setTransformer(?string $transformer): BypassListRecord
    {
        $this->transformer = $transformer;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getElectricLine(): ?string
    {
        return $this->electricLine;
    }

    /**
     * @param string|null $electricLine
     * @return BypassListRecord
     */
    public function setElectricLine(?string $electricLine): BypassListRecord
    {
        $this->electricLine = $electricLine;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getElectricPole(): ?string
    {
        return $this->electricPole;
    }

    /**
     * @param string|null $electricPole
     * @return BypassListRecord
     */
    public function setElectricPole(?string $electricPole): BypassListRecord
    {
        $this->electricPole = $electricPole;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCoords(): ?string
    {
        return $this->coords;
    }

    /**
     * @param string|null $coords
     * @return BypassListRecord
     */
    public function setCoords(?string $coords): BypassListRecord
    {
        $this->coords = $coords;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getController(): ?string
    {
        return $this->controller;
    }

    /**
     * @param string|null $controller
     * @return BypassListRecord
     */
    public function setController(?string $controller): BypassListRecord
    {
        $this->controller = $controller;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDeadline(): ?string
    {
        return $this->deadline;
    }

    /**
     * @param string|null $deadline
     * @return BypassListRecord
     */
    public function setDeadline(?string $deadline): BypassListRecord
    {
        $this->deadline = $deadline;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRoomsCount(): ?string
    {
        return $this->roomsCount;
    }

    /**
     * @param string|null $roomsCount
     * @return BypassListRecord
     */
    public function setRoomsCount(?string $roomsCount): BypassListRecord
    {
        $this->roomsCount = $roomsCount;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLodgersCount(): ?string
    {
        return $this->lodgersCount;
    }

    /**
     * @param string|null $lodgersCount
     * @return BypassListRecord
     */
    public function setLodgersCount(?string $lodgersCount): BypassListRecord
    {
        $this->lodgersCount = $lodgersCount;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCounterInitialDate(): ?string
    {
        return $this->counterInitialDate;
    }

    /**
     * @param string|null $counterInitialDate
     * @return BypassListRecord
     */
    public function setCounterInitialDate(?string $counterInitialDate): BypassListRecord
    {
        $this->counterInitialDate = $counterInitialDate;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCounterInitialValue(): ?string
    {
        return $this->counterInitialValue;
    }

    /**
     * @param string|null $counterInitialValue
     * @return BypassListRecord
     */
    public function setCounterInitialValue(?string $counterInitialValue): BypassListRecord
    {
        $this->counterInitialValue = $counterInitialValue;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getErrors(): Collection
    {
        return $this->errors;
    }

    /**
     * @param Collection $errors
     * @return BypassListRecord
     */
    public function setErrors(Collection $errors): BypassListRecord
    {
        $this->errors = $errors;
        return $this;
    }

    /**
     * @param BypassListErrorInterface $bypassListError
     * @return $this
     */
    public function addError(BypassListErrorInterface $bypassListError): BypassListRecord
    {
        $this->errors->add($bypassListError);
        return $this;
    }

    /**
     * @param BypassListErrorInterface $bypassListError
     * @return $this
     */
    public function removeError(BypassListErrorInterface $bypassListError): BypassListRecord
    {
        $this->errors->removeElement($bypassListError);
        return $this;
    }
}
