<?php

namespace App\Entity\Main;

use App\Repository\TransformerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=TransformerRepository::class)
 *
 * @Serializer\ExclusionPolicy("all")
 */
class Transformer
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\Column(name="id", type="guid")
     * @ORM\CustomIdGenerator(class="App\Model\CustomIdGenerator")
     *
     * @Serializer\Expose()
     */
    private $id;

    /**
     * Наименование ТП
     * @ORM\Column(type="string", length=180, unique=true)
     * @var string
     *
     * @Serializer\Expose()
     */
    private string $title;

    /**
     * Фидер
     * @ORM\ManyToOne(targetEntity="App\Entity\Main\Feeder", cascade="persist")
     * @ORM\JoinColumn(name="feeder", referencedColumnName="id", nullable=true)
     * @var Feeder|null
     *
     * @Serializer\Expose()
     */
    private ?Feeder $feeder;

    /**
     * Контролёры, прикреплённые к этому трансформатору
     * @ORM\ManyToMany(targetEntity="App\Entity\Main\User", inversedBy="transformers")
     * @ORM\JoinTable(name="users_transformers")
     * @var Collection
     *
     * @Serializer\Expose()
     */
    private $users;

    /**
     * Компания, которая обслуживает трансформатор
     * @ORM\ManyToOne(targetEntity="App\Entity\Main\Company", inversedBy="transformers")
     * @ORM\JoinColumn(name="company", referencedColumnName="id", nullable=true)
     * @var Company|null
     *
     * @Serializer\Expose()
     */
    private ?Company $company;

    /**
     * Координаты GPS ([X,Y])
     * @ORM\Column(type="json", nullable=true)
     * @var float[]|null
     *
     * @Serializer\Expose()
     */
    private ?array $coords;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->title;
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Transformer
     */
    public function setTitle(string $title): Transformer
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return Feeder|null
     */
    public function getFeeder(): ?Feeder
    {
        return $this->feeder;
    }

    /**
     * @param Feeder|null $feeder
     * @return Transformer
     */
    public function setFeeder(?Feeder $feeder): Transformer
    {
        $this->feeder = $feeder;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    /**
     * @param Collection $users
     * @return Transformer
     */
    public function setUsers(Collection $users): Transformer
    {
        $this->users = $users;
        return $this;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function addUser(User $user): Transformer
    {
        $this->users->add($user);
        return $this;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function removeUser(User $user): Transformer
    {
        $this->users->removeElement($user);
        return $this;
    }

    /**
     * @return Company|null
     */
    public function getCompany(): ?Company
    {
        return $this->company;
    }

    /**
     * @param Company|null $company
     * @return Transformer
     */
    public function setCompany(?Company $company): Transformer
    {
        $this->company = $company;
        return $this;
    }

    /**
     * @return float[]|null
     */
    public function getCoords(): ?array
    {
        return $this->coords;
    }

    /**
     * @param float[]|null $coords
     * @return Transformer
     */
    public function setCoords(?array $coords): Transformer
    {
        $this->coords = $coords;
        return $this;
    }
}
