<?php

namespace App\Entity\Main;

use App\Repository\AgreementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use JMS\Serializer\Annotation as Serializer;

/**
 * Этот класс описывает договор абонента
 * @ORM\Entity(repositoryClass=AgreementRepository::class)
 * @ORM\Table(name="agreement", indexes={@ORM\Index(name="search_idx", columns={"number", "payment_number"})})
 *
 * @Serializer\ExclusionPolicy("all")
 */
class Agreement
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\Column(name="id", type="guid")
     * @ORM\CustomIdGenerator(class="App\Model\CustomIdGenerator")
     *
     * @Serializer\Expose()
     */
    protected $id;

    /**
     * Номер договора
     * @ORM\Column(type="string", length=100, unique=true)
     * @var string
     *
     * @Serializer\Expose()
     */
    protected $number;

    /**
     * Счетчики
     * @OneToMany(targetEntity="App\Entity\Main\Counter", mappedBy="agreement")
     * @var Collection
     *
     * @Serializer\Exclude()
     */
    protected $counters;

    /**
     * Абонент
     * @ManyToOne(targetEntity="App\Entity\Main\Subscriber", inversedBy="agreements")
     * @JoinColumn(name="subscriber_id", referencedColumnName="id", onDelete="CASCADE")
     * @var Subscriber
     *
     * @Serializer\Expose()
     */
    protected $subscriber;

    /**
     * Отделение
     * @ManyToOne(targetEntity="App\Entity\Main\Company")
     * @JoinColumn(name="company", referencedColumnName="id", onDelete="SET NULL")
     * @var Company
     *
     * @Serializer\Expose()
     */
    protected $company;

    /**
     * Платежный код
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     *
     * @Serializer\Expose()
     */
    protected $paymentNumber;

    /**
     * Agreement constructor.
     */
    public function __construct()
    {
        $this->counters = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     * @return Agreement
     */
    public function setId(?string $id): Agreement
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumber(): string
    {
        return $this->number;
    }

    /**
     * @param string $number
     * @return Agreement
     */
    public function setNumber(string $number): Agreement
    {
        $this->number = $number;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getCounters(): Collection
    {
        return $this->counters;
    }

    /**
     * @param Collection $counters
     * @return Agreement
     */
    public function setCounters(Collection $counters): Agreement
    {
        $this->counters = $counters;
        return $this;
    }

    /**
     * @param Counter $counter
     * @return Agreement
     */
    public function addCounter(Counter $counter): Agreement
    {
        $this->counters->add($counter);
        return $this;
    }

    /**
     * @param Counter $counter
     * @return Agreement
     */
    public function removeCounter(Counter $counter): Agreement
    {
        $this->counters->removeElement($counter);
        return $this;
    }

    /**
     * @return Subscriber
     */
    public function getSubscriber(): Subscriber
    {
        return $this->subscriber;
    }

    /**
     * @param Subscriber $subscriber
     * @return Agreement
     */
    public function setSubscriber(Subscriber $subscriber): Agreement
    {
        $this->subscriber = $subscriber;
        return $this;
    }

    /**
     * @return Company
     */
    public function getCompany(): Company
    {
        return $this->company;
    }

    /**
     * @param Company $company
     * @return Agreement
     */
    public function setCompany(Company $company): Agreement
    {
        $this->company = $company;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPaymentNumber(): ?string
    {
        return $this->paymentNumber;
    }

    /**
     * @param string|null $paymentNumber
     * @return Agreement
     */
    public function setPaymentNumber(?string $paymentNumber): Agreement
    {
        $this->paymentNumber = $paymentNumber;
        return $this;
    }
}