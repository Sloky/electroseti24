<?php

namespace App\Entity\Main;

use App\Repository\PhotosRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=PhotoRepository::class)
 *
 * @Serializer\ExclusionPolicy("all")
 */
class Photo
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\Column(name="id", type="guid")
     * @ORM\CustomIdGenerator(class="App\Model\CustomIdGenerator")
     *
     * @Serializer\Expose()
     */
    private $id;

    /**
     * Задача
     * @ORM\ManyToOne(targetEntity="App\Entity\Main\Task", inversedBy="photos")
     * @ORM\JoinColumn(name="task", referencedColumnName="id", onDelete="CASCADE")
     * @var Task
     */
    private $task;

    /**
     * Имя файла
     * @ORM\Column(type="string")
     * @var string
     *
     * @Serializer\Expose()
     */
    private $fileName;

    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @return Task
     */
    public function getTask(): Task
    {
        return $this->task;
    }

    /**
     * @param Task $task
     * @return Photo
     */
    public function setTask(Task $task): Photo
    {
        $this->task = $task;
        return $this;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     * @return Photo
     */
    public function setFileName(string $fileName): Photo
    {
        $this->fileName = $fileName;
        return $this;
    }
}
