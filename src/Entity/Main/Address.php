<?php

namespace App\Entity\Main;

use App\Repository\AddressRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;

/**
 * @ORM\Entity(repositoryClass=AddressRepository::class)
 * @HasLifecycleCallbacks
 */
class Address
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\Column(name="id", type="guid")
     * @ORM\CustomIdGenerator(class="App\Model\CustomIdGenerator")
     */
    private $id;

    /**
     * Населенный пункт
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     */
    private $locality;

    public static $locality_prefix = [
        'город' => 'г.',
        'посёлок' => 'пос.',
        'село' => 'с.',
        'деревня' => 'дер.',
        'воинская часть' => 'в/ч',
    ];

    /**
     * Улица
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     */
    private $street;

    /**
     * Номер дома
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     */
    private $houseNumber;

    /**
     * Номер подъезда
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     */
    private $porchNumber;

    /**
     * Номер квартиры
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     */
    private $apartmentNumber;

    /**
     * Количество жильцов
     * @ORM\Column(type="integer", nullable=true)
     * @var int|null
     */
    private $lodgersCount;

    /**
     * Количество комнат
     * @ORM\Column(type="integer", nullable=true)
     * @var int|null
     */
    private $roomsCount;

    /**
     * Хеш адреса (для поиска)
     * @ORM\Column(type="string", length=180, unique=true)
     * @var string
     */
    private $addressHash;

    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getLocality(): ?string
    {
        return $this->locality;
    }

    /**
     * @param string|null $locality
     * @return Address
     */
    public function setLocality(?string $locality): Address
    {
        $this->locality = $locality;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStreet(): ?string
    {
        return $this->street;
    }

    /**
     * @param string|null $street
     * @return Address
     */
    public function setStreet(?string $street): Address
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getHouseNumber(): ?string
    {
        return $this->houseNumber;
    }

    /**
     * @param string|null $houseNumber
     * @return Address
     */
    public function setHouseNumber(?string $houseNumber): Address
    {
        $this->houseNumber = $houseNumber;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPorchNumber(): ?string
    {
        return $this->porchNumber;
    }

    /**
     * @param string|null $porchNumber
     * @return Address
     */
    public function setPorchNumber(?string $porchNumber): Address
    {
        $this->porchNumber = $porchNumber;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getApartmentNumber(): ?string
    {
        return $this->apartmentNumber;
    }

    /**
     * @param string|null $apartmentNumber
     * @return Address
     */
    public function setApartmentNumber(?string $apartmentNumber): Address
    {
        $this->apartmentNumber = $apartmentNumber;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getLodgersCount(): ?int
    {
        return $this->lodgersCount;
    }

    /**
     * @param int|null $lodgersCount
     * @return Address
     */
    public function setLodgersCount(?int $lodgersCount): Address
    {
        $this->lodgersCount = $lodgersCount;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getRoomsCount(): ?int
    {
        return $this->roomsCount;
    }

    /**
     * @param int|null $roomsCount
     * @return Address
     */
    public function setRoomsCount(?int $roomsCount): Address
    {
        $this->roomsCount = $roomsCount;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddressHash(): string
    {
        return $this->addressHash;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function setAddressHash()
    {
        $this->addressHash = $this->locality
            .'|'
            .($this->street ?? '')
            .'|'
            .($this->houseNumber ?? '')
            .'|'
            .($this->apartmentNumber ?? '');
    }
}
