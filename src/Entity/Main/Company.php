<?php

namespace App\Entity\Main;

use App\Repository\CompanyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=CompanyRepository::class)
 *
 * @Serializer\ExclusionPolicy("all")
 */
class Company
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\Column(name="id", type="guid")
     * @ORM\CustomIdGenerator(class="App\Model\CustomIdGenerator")
     *
     * @Serializer\Expose()
     */
    private $id;

    /**
     * Родительская компания (для отделений)
     * @ORM\ManyToOne(targetEntity="App\Entity\Main\Company", inversedBy="children")
     * @ORM\JoinColumn(name="parent", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * @var Company|null
     *
     * @Serializer\Expose()
     */
    private $parent;

    /**
     * Потомки
     * @ORM\OneToMany(targetEntity="App\Entity\Main\Company", mappedBy="parent")
     * @var Collection
     *
     * @Serializer\Exclude()
     */
    private $children;

    /**
     * Наименование компании
     * @ORM\Column(type="string", length=180, unique=true)
     * @var string|null
     *
     * @Serializer\Expose()
     */
    private $title;

    /**
     * Код отделения
     * @ORM\Column(type="string", length=255, unique=false, nullable=true)
     * @var string|null
     *
     * @Serializer\Expose()
     */
    private $branchCode;

    /**
     * Адрес отделения
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     *
     * @Serializer\Expose()
     */
    private $address;

    /**
     * Основной телефон отделения
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     *
     * @Serializer\Expose()
     */
    private $phone;

    /**
     * ТП которые обслуживает компания
     * @ORM\OneToMany(targetEntity="App\Entity\Main\Transformer", mappedBy="company")
     * @var Collection
     *
     * @Serializer\Exclude()
     */
    private $transformers;

    /**
     * Company constructor.
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->transformers = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->title;
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId(string $id): Company
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Company|null
     */
    public function getParent(): ?Company
    {
        return $this->parent;
    }

    /**
     * @param Company|null $parent
     * @return Company
     */
    public function setParent(?Company $parent): Company
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    /**
     * @param Collection $children
     * @return Company
     */
    public function setChildren(Collection $children): Company
    {
        $this->children = $children;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Company
     */
    public function setTitle(?string $title): Company
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBranchCode(): ?string
    {
        return $this->branchCode;
    }

    /**
     * @param string|null $branchCode
     * @return Company
     */
    public function setBranchCode(?string $branchCode): Company
    {
        $this->branchCode = $branchCode;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     * @return Company
     */
    public function setAddress(?string $address): Company
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     * @return Company
     */
    public function setPhone(?string $phone): Company
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getTransformers()
    {
        return $this->transformers;
    }

    /**
     * @param Collection $transformers
     * @return Company
     */
    public function setTransformers($transformers): Company
    {
        $this->transformers = $transformers;
        return $this;
    }

    /**
     * @param Transformer $transformer
     * @return $this
     */
    public function addTransformer(Transformer $transformer): Company
    {
        $this->transformers->add($transformer);
        return $this;
    }

    /**
     * @param Transformer $transformer
     * @return $this
     */
    public function removeTransformer(Transformer $transformer): Company
    {
        $this->transformers->removeElement($transformer);
        return $this;
    }
}
