<?php

namespace App\Entity\Main;

use App\Repository\UserGroupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=UserGroupRepository::class)
 */
class UserGroup
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\Column(name="id", type="guid")
     * @ORM\CustomIdGenerator(class="App\Model\CustomIdGenerator")
     */
    private $id;

    /**
     * Название группы
     * @var string
     *
     * @ORM\Column(type="string", length=180, unique=true)
     *
     * @Serializer\Expose()
     */
    private $groupName;

    /**
     * Роли
     * @var array
     *
     * @ORM\Column(type="json")
     *
     * @Serializer\Expose()
     */
    private $roles = array();

    /**
     * Описание
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     *
     * @Serializer\Expose()
     */
    private $description;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="User", mappedBy="groups")
     *
     * @Serializer\Exclude()
     */
    private $users;

    /**
     * UserGroup constructor.
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getGroupName(): string
    {
        return $this->groupName;
    }

    /**
     * @param string $groupName
     * @return UserGroup
     */
    public function setGroupName(string $groupName): UserGroup
    {
        $this->groupName = $groupName;
        return $this;
    }

    /**
     * @param array $roles
     */
    public function addRoles(array $roles)
    {
        $this->roles = array_unique(array_merge($this->roles, $roles));
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @param array $roles
     * @return UserGroup
     */
    public function setRoles(array $roles): UserGroup
    {
        $this->roles = $roles;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return UserGroup
     */
    public function setDescription(string $description): UserGroup
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    /**
     * @param Collection $users
     * @return UserGroup
     */
    public function setUsers(Collection $users): UserGroup
    {
        $this->users = $users;
        return $this;
    }
}
