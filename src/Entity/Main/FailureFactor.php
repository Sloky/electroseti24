<?php

namespace App\Entity\Main;

use App\Repository\FailureFactorRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Этот класс описывает фактор невозможности выполнения задачи
 * @ORM\Entity(repositoryClass=FailureFactorRepository::class)
 */
class FailureFactor
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\Column(name="id", type="guid")
     * @ORM\CustomIdGenerator(class="App\Model\CustomIdGenerator")
     */
    private $id;

    /**
     * Наименование фактора
     * @ORM\Column(type="string", length=180, unique=true)
     * @var string
     */
    private $title;

    /**
     * Текст сообщения
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     */
    private $message;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->title;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return FailureFactor
     */
    public function setTitle(string $title): FailureFactor
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string|null $message
     * @return FailureFactor
     */
    public function setMessage(?string $message): FailureFactor
    {
        $this->message = $message;
        return $this;
    }
}
