<?php

namespace App\Entity\Main;

use App\Repository\CounterRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Entity(repositoryClass=CounterRepository::class)
 */
class Counter
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\Column(name="id", type="guid")
     * @ORM\CustomIdGenerator(class="App\Model\CustomIdGenerator")
     */
    private $id;

    /**
     * Номер счетчика
     * @ORM\Column(type="string", length=100, nullable=false, unique=false)
     * @var string
     */
    private $title;

    /**
     * Договор
     * @ManyToOne(targetEntity="Agreement", inversedBy="counters")
     * @JoinColumn(name="agreement", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     * @var Agreement|null
     */
    private $agreement;

    /**
     * Адрес установки УУ
     * @ORM\ManyToOne(targetEntity="App\Entity\Main\Address", cascade="persist")
     * @JoinColumn(name="address", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * @var Address|null
     */
    private $address;

    /**
     * Трансформатор (ТП)
     * @ManyToOne(targetEntity="App\Entity\Main\Transformer", cascade={"persist"})
     * @JoinColumn(name="transformer", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * @var Transformer|null
     */
    private $transformer;

    /**
     * Тип счетчика
     * @ManyToOne(targetEntity="App\Entity\Main\CounterType")
     * @JoinColumn(name="counter_type", referencedColumnName="id", nullable=true)
     * @var CounterType|null
     */
    private $counterType;

    /**
     * Дата установки УУ
     * @ORM\Column(type="datetime", nullable=true)
     * @var DateTimeInterface|null
     */
    private $installDate;

    /**
     * Дата демонтажа УУ
     * @ORM\Column(type="datetime", nullable=true)
     * @var DateTimeInterface
     */
    private $dismantlingDate;

    /**
     * Начальные показания
     * @ORM\OneToOne(targetEntity="App\Entity\Main\CounterValue")
     * @JoinColumn(name="initial_value", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * @var CounterValue|null
     */
    private $initialValue;

    /**
     * Конечные показания
     * @ORM\OneToOne(targetEntity="App\Entity\Main\CounterValue")
     * @JoinColumn(name="final_value", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * @var CounterValue|null
     */
    private $finalValue;

    /**
     * Клеммная пломба
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     */
    private $terminalSeal;

    /**
     * Антимагнитная пломба
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     */
    private $antiMagneticSeal;

    /**
     * Боковая пломба
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     */
    private $sideSeal;

    /**
     * Электрический столб
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     */
    private $electricPoleNumber;

    /**
     * Линия
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     */
    private $electricLine;

    /**
     * Статус работоспособности (исправен, неисправен, истек срок МПИ)
     * 0 - исправен
     * 1 - неисправен
     * 2 - истек срок МПИ
     * @ORM\Column(type="integer", nullable=false, options={"default":0})
     * @var int
     */
    private $workStatus = 0;

    /**
     * Статус (установлен или снят)
     * @ORM\Column(type="integer", nullable=false, options={"default":0})
     * @var int
     */
    private $isInstalledStatus = 0;

    /**
     * Координаты GPS ([X,Y])
     * @ORM\Column(type="json", nullable=true)
     * @var float[]|null
     */
    private $coordinates;

    /**
     * Временный вариант адреса
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     */
    private $temporaryAddress;

    /**
     * Временный вариант модели УУ
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     */
    private $temporaryCounterType;

    /**
     * Временный вариант подстанции
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     */
    private $temporarySubstation;

    /**
     * Временный вариант трансформатора
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     */
    private $temporaryTransformer;

    /**
     * Временный вариант фидера
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     */
    private $temporaryFeeder;

    /**
     * Временный вариант количества проживающих
     * @ORM\Column(type="integer", nullable=true)
     * @var int|null
     */
    private $temporaryLodgersCount;

    /**
     * Временный вариант количества комнат
     * @ORM\Column(type="integer", nullable=true)
     * @var int|null
     */
    private $temporaryRoomsCount;

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return Counter
     */
    public function setId(string $id): Counter
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Counter
     */
    public function setTitle(string $title): Counter
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return Agreement|null
     */
    public function getAgreement(): ?Agreement
    {
        return $this->agreement;
    }

    /**
     * @param Agreement|null $agreement
     * @return Counter
     */
    public function setAgreement(?Agreement $agreement): Counter
    {
        $this->agreement = $agreement;
        return $this;
    }

    /**
     * @return Address|null
     */
    public function getAddress(): ?Address
    {
        return $this->address;
    }

    /**
     * @param Address|null $address
     * @return Counter
     */
    public function setAddress(?Address $address): Counter
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return Transformer|null
     */
    public function getTransformer(): ?Transformer
    {
        return $this->transformer;
    }

    /**
     * @param Transformer|null $transformer
     * @return Counter
     */
    public function setTransformer(?Transformer $transformer): Counter
    {
        $this->transformer = $transformer;
        return $this;
    }

    /**
     * @return CounterType|null
     */
    public function getCounterType(): ?CounterType
    {
        return $this->counterType;
    }

    /**
     * @param CounterType|null $counterType
     * @return Counter
     */
    public function setCounterType(?CounterType $counterType): Counter
    {
        $this->counterType = $counterType;
        return $this;
    }

    /**
     * @return CounterValue|null
     */
    public function getInitialValue(): ?CounterValue
    {
        return $this->initialValue;
    }

    /**
     * @param CounterValue|null $initialValue
     * @return Counter
     */
    public function setInitialValue(?CounterValue $initialValue): Counter
    {
        $this->initialValue = $initialValue;
        return $this;
    }

    /**
     * @return CounterValue|null
     */
    public function getFinalValue(): ?CounterValue
    {
        return $this->finalValue;
    }

    /**
     * @param CounterValue|null $finalValue
     * @return Counter
     */
    public function setFinalValue(?CounterValue $finalValue): Counter
    {
        $this->finalValue = $finalValue;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTerminalSeal(): ?string
    {
        return $this->terminalSeal;
    }

    /**
     * @param string|null $terminalSeal
     * @return Counter
     */
    public function setTerminalSeal(?string $terminalSeal): Counter
    {
        $this->terminalSeal = $terminalSeal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAntiMagneticSeal(): ?string
    {
        return $this->antiMagneticSeal;
    }

    /**
     * @param string|null $antiMagneticSeal
     * @return Counter
     */
    public function setAntiMagneticSeal(?string $antiMagneticSeal): Counter
    {
        $this->antiMagneticSeal = $antiMagneticSeal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSideSeal(): ?string
    {
        return $this->sideSeal;
    }

    /**
     * @param string|null $sideSeal
     * @return Counter
     */
    public function setSideSeal(?string $sideSeal): Counter
    {
        $this->sideSeal = $sideSeal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getElectricPoleNumber(): ?string
    {
        return $this->electricPoleNumber;
    }

    /**
     * @param string|null $electricPoleNumber
     * @return Counter
     */
    public function setElectricPoleNumber(?string $electricPoleNumber): Counter
    {
        $this->electricPoleNumber = $electricPoleNumber;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getElectricLine(): ?string
    {
        return $this->electricLine;
    }

    /**
     * @param string|null $electricLine
     * @return Counter
     */
    public function setElectricLine(?string $electricLine): Counter
    {
        $this->electricLine = $electricLine;
        return $this;
    }

    /**
     * @return int
     */
    public function getWorkStatus(): int
    {
        return $this->workStatus;
    }

    /**
     * @param int $workStatus
     * @return Counter
     */
    public function setWorkStatus(int $workStatus): Counter
    {
        $this->workStatus = $workStatus;
        return $this;
    }

    /**
     * @return int
     */
    public function getIsInstalledStatus(): int
    {
        return $this->isInstalledStatus;
    }

    /**
     * @param int $isInstalledStatus
     * @return Counter
     */
    public function setIsInstalledStatus(int $isInstalledStatus): Counter
    {
        $this->isInstalledStatus = $isInstalledStatus;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getInstallDate(): ?DateTimeInterface
    {
        return $this->installDate;
    }

    /**
     * @param DateTimeInterface|null $installDate
     * @return Counter
     */
    public function setInstallDate(?DateTimeInterface $installDate): Counter
    {
        $this->installDate = $installDate;
        return $this;
    }

    /**
     * @return DateTimeInterface
     */
    public function getDismantlingDate(): DateTimeInterface
    {
        return $this->dismantlingDate;
    }

    /**
     * @param DateTimeInterface $dismantlingDate
     * @return Counter
     */
    public function setDismantlingDate(DateTimeInterface $dismantlingDate): Counter
    {
        $this->dismantlingDate = $dismantlingDate;
        return $this;
    }

    /**
     * @return float[]|null
     */
    public function getCoordinates(): ?array
    {
        return $this->coordinates;
    }

    /**
     * @param float[]|null $coordinates
     * @return Counter
     */
    public function setCoordinates(?array $coordinates): Counter
    {
        $this->coordinates = $coordinates;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTemporaryAddress(): ?string
    {
        return $this->temporaryAddress;
    }

    /**
     * @param string|null $temporaryAddress
     * @return Counter
     */
    public function setTemporaryAddress(?string $temporaryAddress): Counter
    {
        $this->temporaryAddress = $temporaryAddress;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTemporaryCounterType(): ?string
    {
        return $this->temporaryCounterType;
    }

    /**
     * @param string|null $temporaryCounterType
     * @return Counter
     */
    public function setTemporaryCounterType(?string $temporaryCounterType): Counter
    {
        $this->temporaryCounterType = $temporaryCounterType;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTemporarySubstation(): ?string
    {
        return $this->temporarySubstation;
    }

    /**
     * @param string|null $temporarySubstation
     * @return Counter
     */
    public function setTemporarySubstation(?string $temporarySubstation): Counter
    {
        $this->temporarySubstation = $temporarySubstation;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTemporaryTransformer(): ?string
    {
        return $this->temporaryTransformer;
    }

    /**
     * @param string|null $temporaryTransformer
     * @return Counter
     */
    public function setTemporaryTransformer(?string $temporaryTransformer): Counter
    {
        $this->temporaryTransformer = $temporaryTransformer;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTemporaryFeeder(): ?string
    {
        return $this->temporaryFeeder;
    }

    /**
     * @param string|null $temporaryFeeder
     * @return Counter
     */
    public function setTemporaryFeeder(?string $temporaryFeeder): Counter
    {
        $this->temporaryFeeder = $temporaryFeeder;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getTemporaryLodgersCount(): ?int
    {
        return $this->temporaryLodgersCount;
    }

    /**
     * @param int|null $temporaryLodgersCount
     * @return Counter
     */
    public function setTemporaryLodgersCount(?int $temporaryLodgersCount): Counter
    {
        $this->temporaryLodgersCount = $temporaryLodgersCount;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getTemporaryRoomsCount(): ?int
    {
        return $this->temporaryRoomsCount;
    }

    /**
     * @param int|null $temporaryRoomsCount
     * @return Counter
     */
    public function setTemporaryRoomsCount(?int $temporaryRoomsCount): Counter
    {
        $this->temporaryRoomsCount = $temporaryRoomsCount;
        return $this;
    }
}
