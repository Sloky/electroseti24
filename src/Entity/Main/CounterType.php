<?php

namespace App\Entity\Main;

use App\Repository\CounterTypeRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=CounterTypeRepository::class)
 *
 * @Serializer\ExclusionPolicy("all")
 */
class CounterType
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\Column(name="id", type="guid")
     * @ORM\CustomIdGenerator(class="App\Model\CustomIdGenerator")
     *
     * @Serializer\Expose
     */
    private $id;

    /**
     * Название модели счетчика
     * @var string
     *
     * @ORM\Column(type="string", length=180, unique=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({
     *     "counter_type"
     * })
     */
    protected $title;

    /**
     * Тип тока
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true, name="electron_current_type")
     *
     * @Serializer\Expose
     * @Serializer\SerializedName("electron_current_type")
     * @Serializer\Groups({
     *     "counter_type"
     * })
     */
    protected $electronCurrentType;

    /**
     * @var array
     */
    static public $electron_current_type_value = [
        'constant', // постоянный
        'variable' // переменный
    ];

    /**
     * Численность фаз
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true, name="number_phases")
     *
     * @Serializer\Expose
     * @Serializer\SerializedName("number_phases")
     * @Serializer\Groups({
     *     "counter_type"
     * })
     */
    protected $numberPhases;

    static public $number_phases_value = [
        'single-phase', //однофазный
        'three-phase' //трехфазный
    ];

    /**
     * Численность тарифов
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true, name="number_tariffs")
     *
     * @Serializer\Expose
     * @Serializer\SerializedName("number_tariffs")
     * @Serializer\Groups({
     *     "counter_type"
     * })
     */
    protected $numberTariffs ;

    static public $number_tariffs_value = [
        "T1" => 1,
        "T2" => 2,
        "T3" => 3,
        "TH" => 0
    ];

    /**
     * Тип тарифа ("T1", "T2", "T3" или "TH")
     * @var string
     *
     * @ORM\Column(type="string", nullable=true, name="tariff_type")
     *
     * @Serializer\Expose
     * @Serializer\SerializedName("tariff_type")
     * @Serializer\Groups({
     *     "counter_type"
     * })
     */
    protected $tariffType;

    static public $tariffs_type_value = [
        0 => "TH",
        1 => "T1",
        2 => "T2",
        3 => "T3",

    ];

    /**
     * Тип рабочего механизма
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true, name="operating_mechanism")
     *
     * @Serializer\Expose
     * @Serializer\SerializedName("operating_mechanism")
     * @Serializer\Groups({
     *     "counter_type"
     * })
     */
    protected $operatingMechanism;

    static public $operating_mechanism_value = [
        'mechanical', // механический
        'electronic' // электронный
    ];

    /**
     * Класс точности
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true, name="class_accuracy")
     *
     * @Serializer\Expose
     * @Serializer\SerializedName("class_accuracy")
     * @Serializer\Groups({
     *     "counter_type"
     * })
     */
    protected $classAccuracy;

    static public $classAccuracyValue = [
        '0.5',
        '1.0',
        '1.5',
        '2.0',
    ];

    /**
     * Индикатор устройства
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true, name="indicator_device")
     *
     * @Serializer\Expose
     * @Serializer\SerializedName("indicator_device")
     * @Serializer\Groups({
     *     "counter_type"
     * })
     */
    protected $indicatorDevice;

    static public $indicatorDeviceValue = [
        'ОУ',
        'ЖКИ'
    ];

    /**
     * Максимальное значение счетчика Разрядность счетчика
     * @var integer|null
     *
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({
     *     "counter_type"
     * })
     */
    protected $capacity;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->title;
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return CounterType
     */
    public function setTitle(string $title): CounterType
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return int
     */
    public function getElectronCurrentType(): int
    {
        return $this->electronCurrentType;
    }

    /**
     * @param int $electronCurrentType
     * @return CounterType
     */
    public function setElectronCurrentType(int $electronCurrentType): CounterType
    {
        $this->electronCurrentType = $electronCurrentType;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumberPhases(): int
    {
        return $this->numberPhases;
    }

    /**
     * @param int $numberPhases
     * @return CounterType
     */
    public function setNumberPhases(int $numberPhases): CounterType
    {
        $this->numberPhases = $numberPhases;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumberTariffs(): int
    {
        return $this->numberTariffs;
    }

    /**
     * @param int $numberTariffs
     * @return CounterType
     */
    public function setNumberTariffs(int $numberTariffs): CounterType
    {
        $this->numberTariffs = $numberTariffs;
        return $this;
    }

    /**
     * @return string
     */
    public function getTariffType(): string
    {
        return $this->tariffType;
    }

    /**
     * @param string $tariffType
     * @return CounterType
     */
    public function setTariffType(string $tariffType): CounterType
    {
        $this->tariffType = $tariffType;
        return $this;
    }

    /**
     * @return int
     */
    public function getOperatingMechanism(): int
    {
        return $this->operatingMechanism;
    }

    /**
     * @param int $operatingMechanism
     * @return CounterType
     */
    public function setOperatingMechanism(int $operatingMechanism): CounterType
    {
        $this->operatingMechanism = $operatingMechanism;
        return $this;
    }

    /**
     * @return int
     */
    public function getClassAccuracy(): int
    {
        return $this->classAccuracy;
    }

    /**
     * @param int $classAccuracy
     * @return CounterType
     */
    public function setClassAccuracy(int $classAccuracy): CounterType
    {
        $this->classAccuracy = $classAccuracy;
        return $this;
    }

    /**
     * @return int
     */
    public function getIndicatorDevice(): int
    {
        return $this->indicatorDevice;
    }

    /**
     * @param int $indicatorDevice
     * @return CounterType
     */
    public function setIndicatorDevice(int $indicatorDevice): CounterType
    {
        $this->indicatorDevice = $indicatorDevice;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getCapacity(): ?int
    {
        return $this->capacity;
    }

    /**
     * @param int|null $capacity
     * @return CounterType
     */
    public function setCapacity(?int $capacity): CounterType
    {
        $this->capacity = $capacity;
        return $this;
    }
}
