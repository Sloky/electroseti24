<?php

namespace App\Entity\Main;

use App\Repository\TaskRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=TaskRepository::class)
 */
class Task
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\Column(name="id", type="guid")
     * @ORM\CustomIdGenerator(class="App\Model\CustomIdGenerator")
     */
    private $id;

    /**
     * Тип задачи
     * @ORM\Column(type="integer")
     * @var int
     */
    private $taskType = 0;

    /**
     * Варианты типов задач
     * @var array
     */
    public static $task_type_value = [
        0 => 'Снятие показаний', //Снятие показаний
        1 => 'Замена узла учета', //Замена узла учета
        2 => 'Отключение узла учета', //Отключение узла учета
        3 => 'Актуализация данных абонента или сети', //Актуализация данных абонента или сети
    ];

    /**
     * Компания обслуживающая узел учета (отделение)
     * @ORM\ManyToOne(targetEntity="App\Entity\Main\Company")
     * @ORM\JoinColumn(name="company", referencedColumnName="id", onDelete="SET NULL")
     * @var Company
     */
    private $company;

    /**
     * Узел учета (счетчик)
     * @ORM\ManyToOne(targetEntity="App\Entity\Main\Counter")
     * @ORM\JoinColumn(name="counter", referencedColumnName="id", onDelete="SET NULL")
     * @var Counter
     */
    private $counter;

    /**
     * Срок выполнения задачи
     * @ORM\Column(type="datetime", nullable=true)
     * @var DateTimeInterface|null
     */
    private $deadline;

    /**
     * Дата и время выполнения задачи
     * @ORM\Column(type="datetime", nullable=true)
     * @var DateTimeInterface|null
     */
    private $executedDate;

    /**
     * Дата и время принятия задачи
     * @ORM\Column(type="datetime", nullable=true)
     * @var DateTimeInterface|null
     */
    private $acceptanceDate;

    /**
     * Исполнитель задачи (контролёр)
     * @ORM\ManyToOne(targetEntity="App\Entity\Main\User")
     * @ORM\JoinColumn(name="executor", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * @var UserInterface|null
     */
    private $executor;

    /**
     * Проверяющий задачу (оператор)
     * @ORM\ManyToOne(targetEntity="App\Entity\Main\User")
     * @ORM\JoinColumn(name="operator", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * @var UserInterface|null
     */
    private $operator;

    /**
     * Приоритет
     * @ORM\Column(type="integer")
     * @var int
     */
    private $priority = 0;

    /**
     * Варианты приитета
     * @var array
     */
    public static $priority_value = [
        0 => 'Стандартный',
        1 => 'Высокий',
        2 => 'Особый'
    ];

    /**
     * Статус задачи
     * @ORM\Column(type="integer")
     * @var int
     */
    private $status = 0;

    /**
     * Варианты статусов задачи
     * @var array
     */
    public static $status_value = [
        0 => 'К выполнению', //Не выполнено
        1 => 'К проверке', //К проверке
        2 => 'Принято', //Принято
    ];


    /**
     * Координаты GPS ([X,Y])
     * @ORM\Column(type="json", nullable=true)
     * @var float[]|null
     */
    private $coordinates;

    /**
     * Примечание оператора
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private $operatorComment;

    /**
     * Примечание контролёра
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private $controllerComment;

    /**
     * Тип ошибки
     * @ORM\ManyToMany(targetEntity="App\Entity\Main\FailureFactor")
     * @ORM\JoinTable(name="task_failure_factors",
     *      joinColumns={@ORM\JoinColumn(name="task_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="failure_factor_id", referencedColumnName="id")}
     *      )
     * @var Collection
     */
    private $failureFactors;

    /**
     * Варианты типов ошибок
     * @var array
     */
    /*public static $error_type_value = [
        '0' => 0, //Нет доступа к узлу учета
        '1' => 1, //УУ неисправен
        '2' => 2, //Клеммная пломба неисправна
        '3' => 3, //Антимагнитная пломба неисправна
        '4' => 4, //Боковая пломба неисправна
        '5' => 5, //Неверная модель УУ
        '6' => 6, //Неверный номер УУ
    ];*/

    /**
     * Поля для актуализации. Массив состоящий из имен полей
     * @ORM\Column(type="json", nullable=true)
     * @var string[]|[]
     */
    private $actualizedFields = [];

    /**
     * Поля измененные в мобильном приложении. Массив состоящий из имен полей
     * @ORM\Column(type="json", nullable=true)
     * @var string[]|[]
     */
    private $changedFields = [];

    /**
     * Последние показания (на начало расчетного периода)
     * @ORM\ManyToOne(targetEntity="App\Entity\Main\CounterValue")
     * @ORM\JoinColumn(name="last_counter_value", referencedColumnName="id", onDelete="CASCADE")
     * @var CounterValue
     */
    private $lastCounterValue;

    /**
     * Текущие показания
     * @ORM\ManyToOne(targetEntity="App\Entity\Main\CounterValue")
     * @ORM\JoinColumn(name="current_counter_value", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     * @var CounterValue|null
     */
    private $currentCounterValue;

    /**
     * Расход (разница начальных и текущих показаний)
     * @ORM\Column(type="float", nullable=true)
     * @var float|null
     */
    private $consumption;

    /**
     * Коэффициент расхода
     * @ORM\Column(type="float", nullable=true)
     * @var float
     */
    private $consumptionCoefficient = 1.0;

    /**
     * Физическое состояние УУ (возможность снять показания)
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $counterPhysicalStatus = 0;

    /**
     * Варианты физического состояния УУ
     * @var array
     */
    public static $counter_physical_status_value = [
        0 => 'Исправен', //Доступен, исправен
        1 => 'Неисправен', //Недоступен или неисправен
    ];

    /**
     * Абонент
     * @ORM\ManyToOne(targetEntity="App\Entity\Main\Subscriber")
     * @ORM\JoinColumn(name="subscriber", referencedColumnName="id")
     * @var Subscriber
     */
    private $subscriber;

    /**
     * Договор
     * @ORM\ManyToOne(targetEntity="App\Entity\Main\Agreement")
     * @ORM\JoinColumn(name="agreement", referencedColumnName="id")
     * @var Agreement
     */
    private $agreement;

    /**
     * Фотографии
     * @ORM\OneToMany(targetEntity="App\Entity\Main\Photo", mappedBy="task")
     * @var Collection
     */
    private $photos;

    /**
     * Измененный номер УУ
     * @ORM\Column(type="string", length=255, nullable=true, unique=false)
     * @var string|null
     */
    private $changedCounterNumber;

    /**
     * Измененный тип (модель) УУ (Изменён контролёром в ввиде текста)
     * @ORM\Column(type="string", length=255, nullable=true, unique=false)
     * @var string|null
     */
    private $changedCounterTypeString;

    /**
     * Измененный тип (модель) УУ (Изменен оператором (выбрана конкретная модель))
     * @ORM\ManyToOne(targetEntity="App\Entity\Main\CounterType")
     * @ORM\JoinColumn(name="changed_counter_type", referencedColumnName="id", nullable=true)
     * @var CounterType|null
     */
    private $changedCounterType;

    /**
     * Измененная клеммная пломба
     * @ORM\Column(type="string", length=255, nullable=true, unique=false)
     * @var string|null
     */
    private $changedTerminalSeal;

    /**
     * Измененная антимагнитная пломба
     * @ORM\Column(type="string", length=255, nullable=true, unique=false)
     * @var string|null
     */
    private $changedAntimagneticSeal;

    /**
     * Измененная боковая пломба
     * @ORM\Column(type="string", length=255, nullable=true, unique=false)
     * @var string|null
     */
    private $changedSideSeal;

    /**
     * Измененное количество жильцов
     * @ORM\Column(type="integer", nullable=true)
     * @var integer|null
     */
    private $changedLodgersCount;

    /**
     * Измененное количество комнат
     * @ORM\Column(type="integer", nullable=true)
     * @var integer|null
     */
    private $changedRoomsCount;

    /**
     * Новые координаты GPS ([X,Y])
     * @ORM\Column(type="json", nullable=true)
     * @var float[]|null
     */
    private $newCoordinates;

    /**
     * Первый день периода (месяца расчетного периода)
     * @ORM\Column(type="datetime", nullable=true)
     * @var DateTimeInterface|null
     */
    private $period;

    public function __construct()
    {
        $this->photos = new ArrayCollection();
        $this->failureFactors = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return Task
     */
    public function setId(string $id): Task
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getTaskType(): int
    {
        return $this->taskType;
    }

    /**
     * @param int $taskType
     * @return Task
     */
    public function setTaskType(int $taskType): Task
    {
        $this->taskType = $taskType;
        return $this;
    }

    /**
     * @return Company
     */
    public function getCompany(): Company
    {
        return $this->company;
    }

    /**
     * @param Company $company
     * @return Task
     */
    public function setCompany(Company $company): Task
    {
        $this->company = $company;
        return $this;
    }

    /**
     * @return Counter
     */
    public function getCounter(): Counter
    {
        return $this->counter;
    }

    /**
     * @param Counter $counter
     * @return Task
     */
    public function setCounter(Counter $counter): Task
    {
        $this->counter = $counter;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getDeadline(): ?DateTimeInterface
    {
        return $this->deadline;
    }

    /**
     * @param DateTimeInterface|null $deadline
     * @return Task
     */
    public function setDeadline(?DateTimeInterface $deadline): Task
    {
        $this->deadline = $deadline;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getExecutedDate(): ?DateTimeInterface
    {
        return $this->executedDate;
    }

    /**
     * @param DateTimeInterface|null $executedDate
     * @return Task
     */
    public function setExecutedDate(?DateTimeInterface $executedDate): Task
    {
        $this->executedDate = $executedDate;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getAcceptanceDate(): ?DateTimeInterface
    {
        return $this->acceptanceDate;
    }

    /**
     * @param DateTimeInterface|null $acceptanceDate
     * @return Task
     */
    public function setAcceptanceDate(?DateTimeInterface $acceptanceDate): Task
    {
        $this->acceptanceDate = $acceptanceDate;
        return $this;
    }

    /**
     * @return UserInterface|null
     */
    public function getExecutor(): ?UserInterface
    {
        return $this->executor;
    }

    /**
     * @param UserInterface|null $executor
     * @return Task
     */
    public function setExecutor(?UserInterface $executor): Task
    {
        $this->executor = $executor;
        return $this;
    }

    /**
     * @return UserInterface|null
     */
    public function getOperator(): ?UserInterface
    {
        return $this->operator;
    }

    /**
     * @param UserInterface|null $operator
     * @return Task
     */
    public function setOperator(?UserInterface $operator): Task
    {
        $this->operator = $operator;
        return $this;
    }

    /**
     * @return int
     */
    public function getPriority(): int
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     * @return Task
     */
    public function setPriority(int $priority): Task
    {
        $this->priority = $priority;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Task
     */
    public function setStatus(int $status): Task
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return float[]|null
     */
    public function getCoordinates(): ?array
    {
        return $this->coordinates;
    }

    /**
     * @param float[]|null $coordinates
     * @return Task
     */
    public function setCoordinates(?array $coordinates): Task
    {
        $this->coordinates = $coordinates;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOperatorComment(): ?string
    {
        return $this->operatorComment;
    }

    /**
     * @param string|null $operatorComment
     * @return Task
     */
    public function setOperatorComment(?string $operatorComment): Task
    {
        $this->operatorComment = $operatorComment;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getControllerComment(): ?string
    {
        return $this->controllerComment;
    }

    /**
     * @param string|null $controllerComment
     * @return Task
     */
    public function setControllerComment(?string $controllerComment): Task
    {
        $this->controllerComment = $controllerComment;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getActualizedFields(): array
    {
        return $this->actualizedFields;
    }

    /**
     * @param string[] $actualizedFields
     * @return Task
     */
    public function setActualizedFields(array $actualizedFields): Task
    {
        $this->actualizedFields = $actualizedFields;
        return $this;
    }

    /**
     * @return CounterValue
     */
    public function getLastCounterValue(): CounterValue
    {
        return $this->lastCounterValue;
    }

    /**
     * @param CounterValue $lastCounterValue
     * @return Task
     */
    public function setLastCounterValue(CounterValue $lastCounterValue): Task
    {
        $this->lastCounterValue = $lastCounterValue;
        return $this;
    }

    /**
     * @return CounterValue|null
     */
    public function getCurrentCounterValue(): ?CounterValue
    {
        return $this->currentCounterValue;
    }

    /**
     * @param CounterValue|null $currentCounterValue
     * @return Task
     */
    public function setCurrentCounterValue(?CounterValue $currentCounterValue): Task
    {
        $this->currentCounterValue = $currentCounterValue;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getConsumption(): ?float
    {
        return $this->consumption;
    }

    /**
     * @param float|null $consumption
     * @return Task
     */
    public function setConsumption(?float $consumption): Task
    {
        $this->consumption = $consumption;
        return $this;
    }

    /**
     * @return float
     */
    public function getConsumptionCoefficient(): float
    {
        return $this->consumptionCoefficient;
    }

    /**
     * @param float $consumptionCoefficient
     * @return Task
     */
    public function setConsumptionCoefficient(float $consumptionCoefficient): Task
    {
        $this->consumptionCoefficient = $consumptionCoefficient;
        return $this;
    }

    /**
     * @return int
     */
    public function getCounterPhysicalStatus(): int
    {
        return $this->counterPhysicalStatus;
    }

    /**
     * @param int $counterPhysicalStatus
     * @return Task
     */
    public function setCounterPhysicalStatus(int $counterPhysicalStatus): Task
    {
        $this->counterPhysicalStatus = $counterPhysicalStatus;
        return $this;
    }

    /**
     * @return Subscriber
     */
    public function getSubscriber(): Subscriber
    {
        return $this->subscriber;
    }

    /**
     * @param Subscriber $subscriber
     * @return Task
     */
    public function setSubscriber(Subscriber $subscriber): Task
    {
        $this->subscriber = $subscriber;
        return $this;
    }

    /**
     * @return Agreement
     */
    public function getAgreement(): Agreement
    {
        return $this->agreement;
    }

    /**
     * @param Agreement $agreement
     * @return Task
     */
    public function setAgreement(Agreement $agreement): Task
    {
        $this->agreement = $agreement;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getFailureFactors(): Collection
    {
        return $this->failureFactors;
    }

    /**
     * @param FailureFactor $failureFactor
     * @return Task
     */
    public function addFailureFactors(FailureFactor $failureFactor): Task
    {
        $this->failureFactors->add($failureFactor);
        return $this;
    }

    /**
     * @param FailureFactor $failureFactor
     * @return Task
     */
    public function removeFailureFactors(FailureFactor $failureFactor): Task
    {
        $this->failureFactors->removeElement($failureFactor);
        return $this;
    }

    /**
     * @param Collection $failureFactors
     * @return Task
     */
    public function setFailureFactors(Collection $failureFactors): Task
    {
        $this->failureFactors = $failureFactors;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getChangedFields(): array
    {
        return $this->changedFields;
    }

    /**
     * @param string[] $changedFields
     * @return Task
     */
    public function setChangedFields(array $changedFields): Task
    {
        $this->changedFields = $changedFields;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getPhotos(): Collection
    {
        return $this->photos;
    }

    /**
     * @param Collection $photos
     * @return Task
     */
    public function setPhotos(Collection $photos): Task
    {
        $this->photos = $photos;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getChangedCounterNumber(): ?string
    {
        return $this->changedCounterNumber;
    }

    /**
     * @param string|null $changedCounterNumber
     * @return Task
     */
    public function setChangedCounterNumber(?string $changedCounterNumber): Task
    {
        $this->changedCounterNumber = $changedCounterNumber;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getChangedCounterTypeString(): ?string
    {
        return $this->changedCounterTypeString;
    }

    /**
     * @param string|null $changedCounterTypeString
     * @return Task
     */
    public function setChangedCounterTypeString(?string $changedCounterTypeString): Task
    {
        $this->changedCounterTypeString = $changedCounterTypeString;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getChangedTerminalSeal(): ?string
    {
        return $this->changedTerminalSeal;
    }

    /**
     * @return CounterType|null
     */
    public function getChangedCounterType(): ?CounterType
    {
        return $this->changedCounterType;
    }

    /**
     * @param CounterType|null $changedCounterType
     * @return Task
     */
    public function setChangedCounterType(?CounterType $changedCounterType): Task
    {
        $this->changedCounterType = $changedCounterType;
        return $this;
    }

    /**
     * @param string|null $changedTerminalSeal
     * @return Task
     */
    public function setChangedTerminalSeal(?string $changedTerminalSeal): Task
    {
        $this->changedTerminalSeal = $changedTerminalSeal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getChangedAntimagneticSeal(): ?string
    {
        return $this->changedAntimagneticSeal;
    }

    /**
     * @param string|null $changedAntimagneticSeal
     * @return Task
     */
    public function setChangedAntimagneticSeal(?string $changedAntimagneticSeal): Task
    {
        $this->changedAntimagneticSeal = $changedAntimagneticSeal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getChangedSideSeal(): ?string
    {
        return $this->changedSideSeal;
    }

    /**
     * @param string|null $changedSideSeal
     * @return Task
     */
    public function setChangedSideSeal(?string $changedSideSeal): Task
    {
        $this->changedSideSeal = $changedSideSeal;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getChangedLodgersCount(): ?int
    {
        return $this->changedLodgersCount;
    }

    /**
     * @param int|null $changedLodgersCount
     * @return Task
     */
    public function setChangedLodgersCount(?int $changedLodgersCount): Task
    {
        $this->changedLodgersCount = $changedLodgersCount;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getChangedRoomsCount(): ?int
    {
        return $this->changedRoomsCount;
    }

    /**
     * @param int|null $changedRoomsCount
     * @return Task
     */
    public function setChangedRoomsCount(?int $changedRoomsCount): Task
    {
        $this->changedRoomsCount = $changedRoomsCount;
        return $this;
    }

    /**
     * @return float[]|null
     */
    public function getNewCoordinates(): ?array
    {
        return $this->newCoordinates;
    }

    /**
     * @param float[]|null $newCoordinates
     * @return Task
     */
    public function setNewCoordinates(?array $newCoordinates): Task
    {
        $this->newCoordinates = $newCoordinates;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getPeriod(): ?DateTimeInterface
    {
        return $this->period;
    }

    /**
     * @param DateTimeInterface|null $period
     * @return Task
     */
    public function setPeriod(?DateTimeInterface $period): Task
    {
        $this->period = $period;
        return $this;
    }
}
