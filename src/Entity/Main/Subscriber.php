<?php

namespace App\Entity\Main;

use App\Repository\SubscriberRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=SubscriberRepository::class)
 * @ORM\HasLifecycleCallbacks()
 *
 * @Serializer\ExclusionPolicy("all")
 */
class Subscriber
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\Column(name="id", type="guid")
     * @ORM\CustomIdGenerator(class="App\Model\CustomIdGenerator")
     *
     * @Serializer\Expose()
     */
    private $id;

    /**
     * Наименование абонента
     * @ORM\Column(type="string", length=255, nullable=false, unique=false)
     * @var string
     *
     * @Serializer\Expose()
     */
    private $title;

    /**
     * Имя абонента
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     *
     * @Serializer\Expose()
     */
    private $name;

    /**
     * Фамилия абонента
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     *
     * @Serializer\Expose()
     */
    private $surname;

    /**
     * Отчество абонента
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     *
     * @Serializer\Expose()
     */
    private $patronymic;

    /**
     * Тип абонента (юр. лицо, физ. лицо и т.д.)
     * @ORM\Column(type="integer")
     * @var integer
     *
     * @Serializer\Expose()
     */
    private $subscriberType = 0;

    /**
     * Вырианты типов абонента
     * @var array
     */
    public static $subscriber_type_value = [
        0 => 'физическое лицо',
        1 => 'юридическое лицо',
        2 => 'центральный аппарат',
        3 => 'Электрон',
    ];

    /**
     * Телефоны
     * @ORM\Column(type="json")
     * @var string[]|[]
     *
     * @Serializer\Expose()
     */
    private $phones = [];

    /**
     * Договоры
     * @OneToMany(targetEntity="App\Entity\Main\Agreement", mappedBy="subscriber")
     * @var Collection
     *
     * @Serializer\Exclude()
     */
    private $agreements;

    /**
     * Адрес регистрации абонента
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     *
     * @Serializer\Expose()
     */
    private $address;

    /**
     * ИНН (для Юридических лиц)
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     *
     * @Serializer\Expose()
     */
    private $TIN;

    /**
     * E-mail
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     *
     * @Serializer\Expose()
     */
    private $email;

    /**
     * Координаты офиса (для Юридических лиц)
     * @ORM\Column(type="json", nullable=true)
     * @var string[]|null
     *
     * @Serializer\Expose()
     */
    private $coordinates;

    /**
     * Сайт компании (для Юридических лиц)
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     *
     * @Serializer\Expose()
     */
    private $webSite;

    /**
     * Subscriber constructor.
     */
    public function __construct()
    {
        $this->agreements = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return Subscriber
     */
    public function setId(string $id): Subscriber
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Subscriber
     */
    public function setTitle(string $title): Subscriber
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return Subscriber
     */
    public function setName(?string $name): Subscriber
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSurname(): ?string
    {
        return $this->surname;
    }

    /**
     * @param string|null $surname
     * @return Subscriber
     */
    public function setSurname(?string $surname): Subscriber
    {
        $this->surname = $surname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPatronymic(): ?string
    {
        return $this->patronymic;
    }

    /**
     * @param string|null $patronymic
     * @return Subscriber
     */
    public function setPatronymic(?string $patronymic): Subscriber
    {
        $this->patronymic = $patronymic;
        return $this;
    }

    /**
     * @return int
     */
    public function getSubscriberType(): int
    {
        return $this->subscriberType;
    }

    /**
     * @param int $subscriberType
     * @return Subscriber
     */
    public function setSubscriberType(int $subscriberType): Subscriber
    {
        $this->subscriberType = $subscriberType;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getPhones(): array
    {
        return $this->phones;
    }

    /**
     * @param string[] $phones
     * @return Subscriber
     */
    public function setPhones(array $phones): Subscriber
    {
        $this->phones = $phones;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getAgreements(): Collection
    {
        return $this->agreements;
    }

    /**
     * @param Collection $agreements
     * @return Subscriber
     */
    public function setAgreements(Collection $agreements): Subscriber
    {
        $this->agreements = $agreements;
        return $this;
    }

    /**
     * @param Agreement $agreement
     * @return Subscriber
     */
    public function addAgreement(Agreement $agreement): Subscriber
    {
        $this->agreements->add($agreement);
        return $this;
    }

    /**
     * @param Agreement $agreement
     * @return Subscriber
     */
    public function removeAgreement(Agreement $agreement): Subscriber
    {
        $this->agreements->removeElement($agreement);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     * @return Subscriber
     */
    public function setAddress(?string $address): Subscriber
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTIN(): ?string
    {
        return $this->TIN;
    }

    /**
     * @param string|null $TIN
     * @return Subscriber
     */
    public function setTIN(?string $TIN): Subscriber
    {
        $this->TIN = $TIN;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return Subscriber
     */
    public function setEmail(?string $email): Subscriber
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string[]|null
     */
    public function getCoordinates(): ?array
    {
        return $this->coordinates;
    }

    /**
     * @param string[]|null $coordinates
     * @return Subscriber
     */
    public function setCoordinates(?array $coordinates): Subscriber
    {
        $this->coordinates = $coordinates;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getWebSite(): ?string
    {
        return $this->webSite;
    }

    /**
     * @param string|null $webSite
     * @return Subscriber
     */
    public function setWebSite(?string $webSite): Subscriber
    {
        $this->webSite = $webSite;
        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setNaturalPersonTitle()
    {
        if ($this->subscriberType == 0 && is_null($this->title)) {
            $this->title =
                ($this->surname ? $this->surname.' ' : '')
                .($this->name ? $this->name.' ' : '')
                .($this->patronymic ?? '');
        }
    }
}
