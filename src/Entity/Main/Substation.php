<?php

namespace App\Entity\Main;

use App\Repository\SubstationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SubstationRepository::class)
 */
class Substation
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\Column(name="id", type="guid")
     * @ORM\CustomIdGenerator(class="App\Model\CustomIdGenerator")
     */
    private $id;

    /**
     * Название подстанции
     * @ORM\Column(type="string", length=180, unique=true)
     * @var string
     */
    private $title;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->title;
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Substation
     */
    public function setTitle(string $title): Substation
    {
        $this->title = $title;
        return $this;
    }
}
