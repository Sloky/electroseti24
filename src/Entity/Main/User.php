<?php

namespace App\Entity\Main;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 *
 * @ORM\Table(name="user", indexes={@ORM\Index(name="search_idx", columns={"username", "user_code"})})
 *
 * @Serializer\ExclusionPolicy("all")
 */
class User implements UserInterface
{
    /**
     * ID
     * @var string
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\Column(name="id", type="guid")
     * @ORM\CustomIdGenerator(class="App\Model\CustomIdGenerator")
     *
     * @Serializer\Expose()
     */
    private $id;

    /**
     * Имя пользователя (никнейм)
     * @var string
     *
     * @ORM\Column(type="string", length=180, unique=true)
     *
     * @Serializer\Expose()
     */
    private $username;

    /**
     * Роли
     * @var array
     *
     * @ORM\Column(type="json")
     *
     * @Serializer\Expose()
     */
    private $roles;

    /**
     * Группы
     * @var Collection
     *
     * ORM\ManyToMany(targetEntity="App\Entity\Main\Main\UserGroup")
     * ORM\JoinTable(name="users_groups",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")},
     *      )
     * @ORM\ManyToMany(targetEntity="App\Entity\Main\UserGroup", inversedBy="users")
     * @ORM\JoinTable(name="users_groups")
     *
     * @Serializer\Expose()
     */
    private $groups;

    /**
     * Имя
     * @var string|null
     *
     * @ORM\Column(type="string", length=255, unique=false, nullable=true)
     *
     * @Serializer\Expose()
     */
    private $name;

    /**
     * Фамилия
     * @var string|null
     *
     * @ORM\Column(type="string", length=255, unique=false, nullable=true)
     *
     * @Serializer\Expose()
     */
    private $surname;

    /**
     * Отчество
     * @var string|null
     *
     * @ORM\Column(type="string", length=255, unique=false, nullable=true)
     *
     * @Serializer\Expose()
     */
    private $patronymic;

    /**
     * Телефон
     * @var string|null
     *
     * @ORM\Column(type="string", length=255, unique=false, nullable=true)
     *
     * @Serializer\Expose()
     */
    private $phone;

    /**
     * Адрес
     * @var string|null
     *
     * @ORM\Column(type="string", length=255, unique=false, nullable=true)
     *
     * @Serializer\Expose()
     */
    private $address;

    /**
     * Рабочий статус
     * @var int|null
     *
     * @ORM\Column(type="integer", nullable=false)
     *
     * @Serializer\Expose()
     */
    private $workStatus;

    /**
     * Компания (отделение)
     * @var Collection
     *
     * ORM\ManyToOne(targetEntity="App\Entity\Main\Main\Company")
     * ORM\JoinColumn(name="company", referencedColumnName="id")
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Main\Company")
     * @ORM\JoinTable(name="company_users",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="company_id", referencedColumnName="id", onDelete="CASCADE")}
     *      )
     *
     * @Serializer\Expose()
     */
    private Collection $companies;

    /**
     * Пароль
     * @var string The hashed password
     * @ORM\Column(type="string")
     *
     * @Serializer\Exclude()
     */
    private $password;

    /**
     * Код пользователя
     * @var string|null
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Serializer\Expose()
     */
    private $userCode;

    /**
     * ТП к которым привязан контролёр
     * @ORM\ManyToMany(targetEntity="App\Entity\Main\Transformer", mappedBy="users")
     * @var Collection
     */
    private $transformers;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->setWorkStatus(1);
        $this->roles[] = 'ROLE_USER';
        $this->groups = new ArrayCollection();
        $this->transformers = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->username;
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     * @return User
     */
    public function setId(?string $id): User
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return User
     */
    public function setUsername(string $username): User
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return $this->getAllRoles();
    }

    /**
     * Get all the roles of User
     * @return array
     */
    protected function getAllRoles(): array
    {
        if (!$this->groups->isEmpty())
        {
            $roles = array();
            /** @var UserGroup $group */
            foreach ($this->groups->getValues() as $group)
            {
                $roles = array_merge($roles, $group->getRoles());
            }
            return array_unique(array_merge($this->roles, $roles));
        }
        return $this->roles;
    }

    /**
     * @param array $roles
     * @return $this
     */
    public function addRoles(array $roles): User
    {
        $this->roles = array_unique(array_merge($this->roles, $roles));
        return $this;
    }

    /**
     * @param array $roles
     * @return User
     */
    public function setRoles(array $roles): User
    {
        $this->roles = array_unique(array_merge($this->roles, $roles));
        return $this;
    }

    /**
     * @param $roles
     */
    public function removeRoles($roles)
    {
        foreach ($roles as $role) {
            if ($index = array_search($role, $this->roles)) {
                unset($this->roles[$index]);
            }
        }
    }

    /**
     * @return Collection
     */
    public function getGroups(): Collection
    {
        return $this->groups;
    }

    /**
     * @param Collection $groups
     * @return User
     */
    public function setGroups(Collection $groups): User
    {
        $this->groups = $groups;
        /** @var UserGroup $group */
//        foreach ($groups->getValues() as $group) {
//            $this->roles = array_unique(array_merge($this->roles, $group->getRoles()));
//        }
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return User
     */
    public function setName(?string $name): User
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSurname(): ?string
    {
        return $this->surname;
    }

    /**
     * @param string|null $surname
     * @return User
     */
    public function setSurname(?string $surname): User
    {
        $this->surname = $surname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPatronymic(): ?string
    {
        return $this->patronymic;
    }

    /**
     * @param string|null $patronymic
     * @return User
     */
    public function setPatronymic(?string $patronymic): User
    {
        $this->patronymic = $patronymic;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     * @return User
     */
    public function setPhone(?string $phone): User
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     * @return User
     */
    public function setAddress(?string $address): User
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getWorkStatus(): ?int
    {
        return $this->workStatus;
    }

    /**
     * @param int|null $workStatus
     * @return User
     */
    public function setWorkStatus(?int $workStatus): User
    {
        $this->workStatus = $workStatus;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getCompanies(): Collection
    {
        return $this->companies;
    }

    /**
     * @param Collection $companies
     * @return User
     */
    public function setCompanies(Collection $companies): User
    {
        $this->companies = $companies;
        return $this;
    }

    /**
     * @param Company $company
     * @return $this
     */
    public function addCompany(Company $company): User
    {
        $this->companies->add($company);
        return $this;
    }

    /**
     * @param Company $company
     * @return $this
     */
    public function removeCompany(Company $company): User
    {
        $this->companies->removeElement($company);
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): User
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUserCode(): ?string
    {
        return $this->userCode;
    }

    /**
     * @param string|null $userCode
     * @return User
     */
    public function setUserCode(?string $userCode): User
    {
        $this->userCode = $userCode;
        return $this;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @return Collection
     */
    public function getTransformers()
    {
        return $this->transformers;
    }

    /**
     * @param Collection $transformers
     * @return User
     */
    public function setTransformers(Collection $transformers)
    {
        $this->transformers = $transformers;
        return $this;
    }

    /**
     * @param Transformer $transformer
     * @return $this
     */
    public function addTransformer(Transformer $transformer): User
    {
        $this->transformers->add($transformer);
        return $this;
    }

    /**
     * @param Transformer $transformer
     * @return $this
     */
    public function removeTransformer(Transformer $transformer): User
    {
        $this->transformers->removeElement($transformer);
        return $this;
    }
}
