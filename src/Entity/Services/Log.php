<?php

namespace App\Entity\Services;

use App\Repository\Services\LogRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LogRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class Log
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\Column(name="id", type="guid")
     * @ORM\CustomIdGenerator(class="App\Model\CustomIdGenerator")
     */
    private $id;

    /**
     * @var string|null
     * @ORM\Column(name="message", type="text")
     */
    private $message;

    /**
     * @var array|null
     * @ORM\Column(name="context", type="array")
     */
    private $context;

    /**
     * @var int|null
     * @ORM\Column(name="level", type="smallint")
     */
    private $level;

    /**
     * @var string|null
     * @ORM\Column(name="level_name", type="string", length=50)
     */
    private $levelName;

    /**
     * @var array|null
     * @ORM\Column(name="extra", type="array")
     */
    private $extra;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    public function __construct()
    {
        $this->createdAt = new DateTimeImmutable();
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string|null $message
     * @return Log
     */
    public function setMessage(?string $message): Log
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getContext(): ?array
    {
        return $this->context;
    }

    /**
     * @param array|null $context
     * @return Log
     */
    public function setContext(?array $context): Log
    {
        $this->context = $context;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getLevel(): ?int
    {
        return $this->level;
    }

    /**
     * @param int|null $level
     * @return Log
     */
    public function setLevel(?int $level): Log
    {
        $this->level = $level;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLevelName(): ?string
    {
        return $this->levelName;
    }

    /**
     * @param string|null $levelName
     * @return Log
     */
    public function setLevelName(?string $levelName): Log
    {
        $this->levelName = $levelName;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getExtra(): ?array
    {
        return $this->extra;
    }

    /**
     * @param array|null $extra
     * @return Log
     */
    public function setExtra(?array $extra): Log
    {
        $this->extra = $extra;
        return $this;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @param DateTimeImmutable $createdAt
     * @return Log
     */
    public function setCreatedAt(DateTimeImmutable $createdAt): Log
    {
        $this->createdAt = $createdAt;
        return $this;
    }
}
